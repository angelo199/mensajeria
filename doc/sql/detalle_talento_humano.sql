ALTER TABLE gestion_humana.experiencia_laboral ADD COLUMN condicion_nominal_id bigint;

ALTER TABLE gestion_humana.experiencia_laboral ALTER COLUMN condicion_nominal_id SET NOT NULL;

ALTER TABLE gestion_humana.estudio_realizado
  ADD CONSTRAINT estudio_realizado_condicion_nominal_fk6 FOREIGN KEY (condicion_nominal_id)
      REFERENCES gestion_humana.condicion_nominal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE gestion_humana.experiencia_laboral
  ADD CONSTRAINT experiencia_laboral_condicion_nominal_fk6 FOREIGN KEY (condicion_nominal_id)
      REFERENCES gestion_humana.condicion_nominal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE gestion_humana.experiencia_laboral
  ADD CONSTRAINT experiencia_laboral_publica_check CHECK (publica::text = ANY (ARRAY['S'::character varying::text, 'N'::character varying::text]));

ALTER TABLE gestion_humana.experiencia_laboral
  ADD CONSTRAINT experiencia_laboral_activo_actualmente_check CHECK (activo_actualmente::text = ANY (ARRAY['S'::character varying::text, 'N'::character varying::text]));

ALTER TABLE gestion_humana.experiencia_laboral
   ALTER COLUMN observacion DROP NOT NULL;

ALTER TABLE gestion_humana.referencia_personal
  ADD COLUMN funcionario_mppe character varying(1) NOT NULL DEFAULT 'N';
ALTER TABLE gestion_humana.referencia_personal
  ADD CONSTRAINT referencia_personal_funcinario_mppe_chck2 CHECK (funcionario_mppe IN ('S', 'N'));
COMMENT ON COLUMN gestion_humana.referencia_personal.funcionario_mppe IS 'S=Sí;
N=No;';
-- Table: gestion_humana.referencia_personal

-- DROP TABLE gestion_humana.referencia_personal;

CREATE TABLE gestion_humana.referencia_personal
(
  id bigserial NOT NULL,
  talento_humano_id bigint NOT NULL,
  nombre_referencia character varying(150) NOT NULL,
  telefono_referencia character varying(15) NOT NULL,
  observacion text,
  usuario_ini_id integer NOT NULL,
  fecha_ini timestamp(6) without time zone DEFAULT (now())::timestamp(0) without time zone,
  usuario_act_id integer,
  fecha_act timestamp(6) without time zone DEFAULT (now())::timestamp(0) without time zone,
  fecha_elim timestamp(6) without time zone,
  estatus character varying(1) DEFAULT 'A'::character varying,
  funcionario_mppe character varying(1) NOT NULL DEFAULT 'N'::character varying, -- S=Sí;...
  condicion_nominal_id integer NOT NULL,
  email_referencia character varying(180),
  cedula_referencia character varying(15) NOT NULL,
  CONSTRAINT referencia_personal_pk PRIMARY KEY (id),
  CONSTRAINT referencia_personal_condicion_nominal_fk4 FOREIGN KEY (condicion_nominal_id)
      REFERENCES gestion_humana.condicion_nominal (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT referencia_personal_nivel_curso_fk3 FOREIGN KEY (talento_humano_id)
      REFERENCES gestion_humana.talento_humano (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT referencia_personal_user_act_fk2 FOREIGN KEY (usuario_act_id)
      REFERENCES seguridad.usergroups_user (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT referencia_personal_user_ini_fk1 FOREIGN KEY (usuario_ini_id)
      REFERENCES seguridad.usergroups_user (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT referencia_personal_estatus_check CHECK (estatus::text = ANY (ARRAY['A'::character varying::text, 'P'::character varying::text, 'T'::character varying::text, 'C'::character varying::text])),
  CONSTRAINT referencia_personal_funcinario_mppe_chck2 CHECK (funcionario_mppe::text = ANY (ARRAY['S'::character varying, 'N'::character varying]::text[]))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE gestion_humana.referencia_personal
  OWNER TO divsistemas;
COMMENT ON COLUMN gestion_humana.referencia_personal.funcionario_mppe IS 'S=Sí;
N=No;';


-- Index: gestion_humana.gh_referencia_personal_estatus_idx

-- DROP INDEX gestion_humana.gh_referencia_personal_estatus_idx;

CREATE INDEX gh_referencia_personal_estatus_idx
  ON gestion_humana.referencia_personal
  USING btree
  (estatus COLLATE pg_catalog."default");

-- Index: gestion_humana.gh_referencia_personal_talento_humano_idx

-- DROP INDEX gestion_humana.gh_referencia_personal_talento_humano_idx;

CREATE INDEX gh_referencia_personal_talento_humano_idx
  ON gestion_humana.referencia_personal
  USING btree
  (talento_humano_id);
