﻿SELECT 'AAA-'||ce.nombre AS orden_estado, ce.id AS estadoid, ce.nombre AS estado,
  (SUM(CASE WHEN (th.estado_id IS NOT NULL AND th.user_id IS NOT NULL) THEN 1 ELSE 0 END)) AS total_registrados_en_sistema,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.estado_id=th.estado_id AND po.apertura_periodo_id = 2) AS postulados, 
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.estado_id=th.estado_id AND po.apertura_periodo_id = 2 AND po.estatus_aprobado=0) AS en_espera_por_consignacion_de_documentos,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.estado_id=th.estado_id AND po.apertura_periodo_id = 2 AND (po.estatus_aprobado IN (1, 3, 4, 6, 7)) AS postulacion_aprobada,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.estado_id=th.estado_id AND po.apertura_periodo_id = 2 AND po.estatus_aprobado IN (3, 4, 6, 7)) AS req_minimos_aprobados,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.estado_id=th.estado_id AND po.apertura_periodo_id = 2 AND po.estatus_aprobado=4) AS beneficiados,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.estado_id=th.estado_id AND po.apertura_periodo_id = 2 AND po.estatus_aprobado=5) AS no_beneficiados,
  2 AS periodoid
  FROM catastro.estado ce
  LEFT JOIN gestion_humana.talento_humano AS th ON th.estado_id = ce.id
  GROUP BY orden_estado,ce.id, ce.nombre, th.estado_id 

UNION 

SELECT 'ZZZ-TOTAL' AS orden_estado, 0 AS estadoid, 'TOTAL' AS estado, 
  (SUM(CASE WHEN (gth.estado_id IS NOT NULL AND gth.user_id IS NOT NULL) THEN 1 ELSE 0 END)) AS total_registrados_en_sistema,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.apertura_periodo_id = 2) AS postulados, 
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.apertura_periodo_id = 2 AND po.estatus_aprobado=0) AS en_espera_por_consignacion_de_documentos,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.apertura_periodo_id = 2 AND po.estatus_aprobado=1) AS postulacion_aprobada,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.apertura_periodo_id = 2 AND po.estatus_aprobado=3) AS req_minimos_aprobados,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.apertura_periodo_id = 2 AND po.estatus_aprobado=4) AS beneficiados,
  (SELECT COUNT(po.id) FROM gestion_humana.postulacion po WHERE po.apertura_periodo_id = 2 AND po.estatus_aprobado=5) AS no_beneficiados,
  2 AS periodoid
  FROM gestion_humana.talento_humano gth
  WHERE gth.user_id IS NOT NULL
  ORDER BY orden_estado