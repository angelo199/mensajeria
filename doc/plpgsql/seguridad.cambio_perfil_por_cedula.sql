CREATE INDEX seguridad_usr_cedula_idx
ON seguridad.usergroups_user
USING btree(cedula);

CREATE INDEX seguridad_usr_cedula_status_idx
ON seguridad.usergroups_user
USING btree(cedula, status);

CREATE OR REPLACE FUNCTION seguridad.cambio_perfil_por_cedula(group_id_vi integer, cedula_vi integer, ip_address_vi character varying, user_level_vi integer, username_vi character varying, user_act_id_vi bigint, data_vi text)
  RETURNS record AS
  $BODY$
  DECLARE

    --  Variables:
    --      Informacion del sistema:
    modulo_v VARCHAR := 'UserGroups.CambiarPerfil.Ejecutar'; --log auditoria.traza

    --      Debug:
    seccion_v SMALLINT := 0; --Seccion del error
    fecha_v TIMESTAMP WITHOUT TIME ZONE;

--      Variables de Existencia:
    existencia_usuario SMALLINT := 0;
    existencia_usuario_activo SMALLINT := 0;
    existencia_permiso_grupo SMALLINT := 0;
    existencia_usuario_root SMALLINT := 0;
    -- Validar si existe un un tipo de persona y si es externo validar que no este en talento humano
    -- Variables de Captura:
    captura_id_usuario_v BIGINT := 0;

    --      Variables de Mensaje:
    mensaje_v VARCHAR := '';
    codigo_v VARCHAR := '';
    estado_resultado_v VARCHAR := '';

    --      Variables de Resultado:
    resultado record;

  BEGIN
    
    fecha_v = (NOW())::timestamp(0);

    seccion_v := 1;
    SELECT COUNT(1) INTO existencia_usuario FROM seguridad.usergroups_user WHERE cedula = cedula_vi;
    IF existencia_usuario = 0 THEN
      mensaje_v := 'La Cédula de Identidad '||cedula_vi||' no se encuentra registrada en el Sistema. El usuario debe registrarse primeramente.';
      codigo_v := 'EDB000';
      estado_resultado_v :='error';
      RAISE EXCEPTION 'La Cédula de Identidad % no se encuentra registrada en el Sistema. El usuario debe registrarse primeramente.', cedula_vi;
    END IF;

    seccion_v := 2;
    IF existencia_usuario > 1 THEN
      mensaje_v := 'El Número de Cédula de Identidad '||cedula_vi||' aparece más de una vez en la base de datos. Puede que existan con orígenes distintos (V, E, P). Instamos a que en este caso haga uso del Módulo de Administración de Usuarios para cambiar el perfil de este usuario.';
      codigo_v := 'EDB001';
      estado_resultado_v :='error';
      RAISE EXCEPTION 'El Número de Cédula de Identidad % aparece más de una vez en la base de datos. Puede que existan con orígenes distintos (V, E, P). Instamos a que en este caso haga uso del Módulo de Administración de Usuarios para cambiar el perfil de este usuario.', cedula_vi;
    END IF;

    seccion_v := 3;
    SELECT COUNT(1) INTO existencia_usuario FROM seguridad.usergroups_user WHERE cedula = cedula_vi AND status != 0;
    IF existencia_usuario = 0 THEN
      mensaje_v := 'La persona con la Cédula de Identidad '||cedula_vi||' no se encuentra Activa en el Sistema.';
      codigo_v := 'EDB002';
      estado_resultado_v :='error';
      RAISE EXCEPTION 'La persona con la Cédula de Identidad % no se encuentra Activa en el Sistema.', cedula_vi;
    END IF;

    seccion_v := 4;
    SELECT COUNT(1) INTO existencia_usuario_root FROM seguridad.usergroups_user WHERE cedula = cedula_vi AND group_id = 1;
    SELECT COUNT(1) INTO existencia_permiso_grupo FROM seguridad.usergroups_group WHERE groupname != 'root' AND estatus = 'A' AND level <= user_level_vi;
    IF existencia_permiso_grupo = 0 OR existencia_usuario_root = 1 THEN
      mensaje_v := 'Puede que el grupo de usuarios seleccionado no exista o usted no tenga permisos para otorgar este perfil a otros usuarios.';
      codigo_v := 'EDB003';
      estado_resultado_v :='error';
      RAISE EXCEPTION 'Puede que el grupo de usuarios seleccionado no exista o usted no tenga permisos para otorgar este perfil a otros usuarios.';
    END IF;

    seccion_v := 5;
    UPDATE seguridad.usergroups_user SET group_id = group_id_vi, user_act_id = user_act_id_vi, date_act = fecha_v WHERE cedula = cedula_vi;

    mensaje_v := 'Perfil Actualizado exitosamente al usuario con la Cédula de Identidad Número '||cedula_vi||'.';
    codigo_v := 'S00000';
    estado_resultado_v :='exito';

    SELECT codigo_v, estado_resultado_v, mensaje_v, seccion_v INTO resultado;
    --  Ingresa los resultados a la traza de la auditoria.

    seccion_v := 6;
    INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (fecha_v, ip_address_vi, 'ACTUALIZACION', modulo_v, estado_resultado_v, 'ACTUALIZACIÓN MASIVA DE PERFILES DE USUARIO: '||mensaje_v, user_act_id_vi, username_vi, data_vi);

    RETURN resultado;

  EXCEPTION

    WHEN OTHERS THEN
        codigo_v := SQLSTATE;
        estado_resultado_v := 'error';
        SELECT codigo_v, estado_resultado_v, mensaje_v, seccion_v INTO resultado;
        mensaje_v := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||estado_resultado_v||')';
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (Seccion: %)', SQLERRM, SQLSTATE, seccion_v;
        --  Ingresa los resultados a la traza de la auditoria.
        INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (fecha_v, ip_address_vi, 'ERROR', modulo_v, estado_resultado_v, 'ACTUALIZACIÓN MASIVA DE PERFILES DE USUARIO: '||mensaje_v, user_act_id_vi, username_vi, data_vi);
        RETURN resultado;

  END;$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
