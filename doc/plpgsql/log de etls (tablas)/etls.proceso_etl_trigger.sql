-- Table: etls.proceso_etl

-- DROP TABLE etls.proceso_etl;

CREATE TABLE etls.proceso_etl_trigger
(
  id bigserial NOT NULL,
  codigo character varying(20),
  descripcion text, --Descripcion del ETL que se ejecuto.
  datos_origen character varying(100), --
  datos_destino character varying(100),
  servidor_ip character varying(30),
  nombre_ktr character varying(150),
  fecha_ultima_ejecucion timestamp(6) without time zone DEFAULT (now())::timestamp(0) without time zone,
  fecha_ejecucion_exitosa timestamp(6) without time zone,
  fecha_ultimo_error timestamp(6) without time zone,
  estatus character varying(1) NOT NULL DEFAULT 'A'::character varying,
  observaciones text,
  nombre_kjb character varying(150),
  CONSTRAINT proceso_etl_id_pk PRIMARY KEY (id),
  CONSTRAINT proceso_etl_codigo_key UNIQUE (codigo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE etls.proceso_etl
  OWNER TO postgres;
GRANT ALL ON TABLE etls.proceso_etl TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE etls.proceso_etl TO divsistemas WITH GRANT OPTION;

-- Index: etls.etls_proceso_etl_codigo_idx

-- DROP INDEX etls.etls_proceso_etl_codigo_idx;

CREATE INDEX etls_proceso_etl_codigo_idx
  ON etls.proceso_etl
  USING btree
  (codigo COLLATE pg_catalog."default");

-- Index: etls.etls_proceso_etl_estatus_idx

-- DROP INDEX etls.etls_proceso_etl_estatus_idx;

CREATE INDEX etls_proceso_etl_estatus_idx
  ON etls.proceso_etl
  USING btree
  (estatus COLLATE pg_catalog."default");

-- Index: etls.etls_proceso_etl_fecha_ejecucion_exitosa_idx

-- DROP INDEX etls.etls_proceso_etl_fecha_ejecucion_exitosa_idx;

CREATE INDEX etls_proceso_etl_fecha_ejecucion_exitosa_idx
  ON etls.proceso_etl
  USING btree
  (fecha_ejecucion_exitosa);

-- Index: etls.etls_proceso_etl_fecha_ultima_ejecucion_idx

-- DROP INDEX etls.etls_proceso_etl_fecha_ultima_ejecucion_idx;

CREATE INDEX etls_proceso_etl_fecha_ultima_ejecucion_idx
  ON etls.proceso_etl
  USING btree
  (fecha_ultima_ejecucion);

-- Index: etls.etls_proceso_etl_fecha_ultimo_error_idx

-- DROP INDEX etls.etls_proceso_etl_fecha_ultimo_error_idx;

CREATE INDEX etls_proceso_etl_fecha_ultimo_error_idx
  ON etls.proceso_etl
  USING btree
  (fecha_ultimo_error);

