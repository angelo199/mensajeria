CREATE TABLE  etls.captura_log_errores(
id serial not null primary key,
captura_log_estado_id int not null,
descripcion varchar(255) not null,
nombre_ktr character varying(150) not null
);


CREATE INDEX etls_captura_log_errores_id_idx
ON etls.captura_log_errores
USING btree
(id);

CREATE INDEX etls_captura_log_errores_id_captura_log_estado_idx
ON etls.captura_log_errores
USING btree
(captura_log_estado_id);

CREATE INDEX etls_captura_log_errores_nombre_ktr_idx
ON etls.captura_log_errores
USING btree
(nombre_ktr);



CREATE TABLE etls.captura_log_errores_trans_mirror_contratados_tcargo
(
)
INHERITS (etls.captura_log_errores);

CREATE TABLE etls.captura_log_errores_trans_mirror_contratados_tpersona
(
)
INHERITS (etls.captura_log_errores);

CREATE TABLE etls.captura_log_errores_trans_mirror_contratados_tdependencia
(
)
INHERITS (etls.captura_log_errores);

CREATE TABLE etls.captura_log_errores_trans_mirror_fijos_tcargo
(
)
INHERITS (etls.captura_log_errores);

CREATE TABLE etls.captura_log_errores_trans_mirror_fijos_tdependencia
(
)
INHERITS (etls.captura_log_errores);

CREATE TABLE etls.captura_log_errores_trans_mirror_fijos_tpersona
(
)
INHERITS (etls.captura_log_errores);

CREATE TABLE etls.captura_log_errores_trans_mirror_jubilados_tpersona
(
)
INHERITS (etls.captura_log_errores);

ALTER TABLE etls.captura_log_errores_trans_mirror_contratados_tcargo
  ADD CONSTRAINT etls_contratados_tcargo_captura_log_estado_id_fkey FOREIGN KEY (captura_log_estado_id) REFERENCES etls.proceso_etl_trigger (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE etls.captura_log_errores_trans_mirror_contratados_tpersona
  ADD CONSTRAINT etls_contratados_tpersona_captura_log_estado_id_fkey FOREIGN KEY (captura_log_estado_id) REFERENCES etls.proceso_etl_trigger (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE etls.captura_log_errores_trans_mirror_contratados_tdependencia
  ADD CONSTRAINT etls_contratados_tdependencia_captura_log_estado_id_fkey FOREIGN KEY (captura_log_estado_id) REFERENCES etls.proceso_etl_trigger (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE etls.captura_log_errores_trans_mirror_fijos_tcargo
  ADD CONSTRAINT etls_fijos_tcargo_captura_log_estado_id_fkey FOREIGN KEY (captura_log_estado_id) REFERENCES etls.proceso_etl_trigger (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE etls.captura_log_errores_trans_mirror_fijos_tdependencia
  ADD CONSTRAINT etls_fijos_tdependencia_captura_log_estado_id_fkey FOREIGN KEY (captura_log_estado_id) REFERENCES etls.proceso_etl_trigger (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE etls.captura_log_errores_trans_mirror_fijos_tpersona
  ADD CONSTRAINT etls_fijos_tpersona_captura_log_estado_id_fkey FOREIGN KEY (captura_log_estado_id) REFERENCES etls.proceso_etl_trigger (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE etls.captura_log_errores_trans_mirror_jubilados_tpersona
  ADD CONSTRAINT etls_jubilados_tpersona_captura_log_estado_id_fkey FOREIGN KEY (captura_log_estado_id) REFERENCES etls.proceso_etl_trigger (id) ON UPDATE NO ACTION ON DELETE NO ACTION;



CREATE INDEX captura_log_errores_trans_mirror_contratados_tcargo_idx
  ON etls.captura_log_errores_trans_mirror_contratados_tcargo
  USING btree
  (nombre_ktr COLLATE pg_catalog."default");

CREATE INDEX captura_log_errores_trans_mirror_contratados_tpersona_idx
  ON etls.captura_log_errores_trans_mirror_contratados_tpersona
  USING btree
  (nombre_ktr COLLATE pg_catalog."default");


CREATE INDEX captura_log_errores_trans_mirror_contratados_tdependencia_idx
  ON etls.captura_log_errores_trans_mirror_contratados_tdependencia
  USING btree
  (nombre_ktr COLLATE pg_catalog."default");


CREATE INDEX captura_log_errores_trans_mirror_fijos_tcargo_idx
  ON etls.captura_log_errores_trans_mirror_fijos_tcargo
  USING btree
  (nombre_ktr COLLATE pg_catalog."default");


CREATE INDEX captura_log_errores_trans_mirror_fijos_tdependencia_idx
  ON etls.captura_log_errores_trans_mirror_fijos_tdependencia
  USING btree
  (nombre_ktr COLLATE pg_catalog."default");


CREATE INDEX captura_log_errores_trans_mirror_fijos_tpersona_idx
  ON etls.captura_log_errores_trans_mirror_fijos_tpersona
  USING btree
  (nombre_ktr COLLATE pg_catalog."default");


CREATE INDEX captura_log_errores_trans_mirror_jubilados_tpersona_idx
  ON etls.captura_log_errores_trans_mirror_jubilados_tpersona
  USING btree
  (nombre_ktr COLLATE pg_catalog."default");
