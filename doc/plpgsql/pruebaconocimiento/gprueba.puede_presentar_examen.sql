CREATE OR REPLACE FUNCTION gprueba.puede_presentar_examen(
    postulacion_id_vi BIGINT,
    usuario_id_vi BIGINT,
    username_vi CHARACTER VARYING,
    modulo_vi CHARACTER VARYING,
    ipaddress_vi CHARACTER VARYING,
    action_vi  CHARACTER VARYING, 
    horaActual_vi TIME, 
    horaFinalPrueba_vi TIME
)
RETURNS RECORD AS
$BODY$
--Declarando las variables
DECLARE

--  Variables de existencia
    existe_talento_humano_v INT := 0;
    existe_postulacion_v INT := 0;
    existe_seccion_prueba_v INT := 0;
    presentado_prueba_v INT :=0;

--  Variables de captura
    talento_humano_r RECORD;
    postulacion_r RECORD;
    fecha_inicio_v DATE;
    fecha_final_v  DATE;
    fecha_v TIMESTAMP;

--  Variables de resultado:
    resultado record;
    id_prueba_v BIGINT; -- id de la prueba
    aplicacion_prueba_id_v BIGINT:=null;  -- id aplicacion_prueba

--  Variables de Bucle
    indice_v INT := 1;
    length_v INt := 0;

--  Variables de exception
--      Seccion que permite localizar hasta que punto corrio el codigo hasta que se genero el error.
    seccion_v SMALLINT := 0;
    mensaje_v TEXT:= 'Aun no ha generado el mensaje para esta respuesta.'; --SQLERRM
    codigo_v VARCHAR := 'ES000'; --SXXXX, ESXXX, EDBXX, ESVXX.
    /* Legenda de codigos de 'codigo_v':
        SXXXX: Proceso completado correctamente.
        EDBXX: Error en el resultado de la Base de Datos.
        ESXXX: Error general del procedimiento almacenado.
        ESVXX: Error de Variable, generalmente son condiciones que no se cumplen con las variables.
    */
    estado_resultado_v VARCHAR := 'error';

--  Variables de auditoria
    fecha_hora_v timestamp without time zone := NOW();
    ipaddress_v VARCHAR := ''; -- Si es necesario capturar la ip del servidor donde se aloja la base de datos se usa: 'inet_client_addr();'
    tipo_transaccion_v VARCHAR := 'ESCRITURA'; --Si es ESCRITURA, ELIMINACION, EDICION o LECTURA
    modulo_v VARCHAR := 'gestionHumana.Postulacion.entrevista'; --Modulo.Controlador.Accion
    transaccion_v VARCHAR := null;
    mensaje_auditoria_v VARCHAR:=null;
    username_v VARCHAR := '';
    data_v TEXT := '{}';

BEGIN
    fecha_v := (NOW())::TIMESTAMP(0);
    seccion_v := 1;

    --  Validar si existe el talento humano dado el id del usuario en el sistema.
    SELECT COUNT(1) INTO existe_talento_humano_v FROM gestion_humana.talento_humano WHERE user_id  = usuario_id_vi LIMIT 1;
    IF existe_talento_humano_v = 0 THEN
        codigo_v := 'EDB301';
        mensaje_v:= 'No se ha encontrado el talento humano relacionado al usuario actual.';
        RAISE EXCEPTION 'No se ha encontrado el talento humano relacionado al usuario actual.';
    END IF;

    SELECT id, condicion_actual_id, estatus INTO talento_humano_r FROM gestion_humana.talento_humano WHERE user_id  = usuario_id_vi LIMIT 1;

    -- Validar que la persona sea Aspirante o Contratado los cuales son los que puede presentar la Prueba de Concocimiento para el Concurso Público
    IF talento_humano_r.condicion_actual_id NOT IN (0, 2) THEN -- 0 = Aspirante, 2 = Contratado
        codigo_v := 'EDB302';
        mensaje_v:= 'Usted no cumple con las condiciones nominales para realizar este proceso.';
        RAISE EXCEPTION 'Usted no cumple con las condiciones nominales para realizar este proceso.';
    END IF;

    -- Validar que el estatus del talento humano no esté en Nómina Pasiva, Inactivo u Otro Estatus
    IF talento_humano_r.estatus IN ('P', 'O', 'I') THEN -- P = pasivo el usuario esta en nomina pasiva , O = otro, I = inactivo
        codigo_v := 'EDB303';
        mensaje_v:='Usted no posee el estatus necesario en el sistema para realizar este proceso.';
        RAISE EXCEPTION 'Usted no posee el estatus necesario en el sistema para realizar este proceso.';
    END IF;

    -- Validar si existe la postulacion
    SELECT COUNT(1) INTO existe_postulacion_v FROM gestion_humana.postulacion WHERE id = postulacion_id_vi LIMIT 1;
    IF existe_postulacion_v = 0 THEN
        codigo_v := 'EDB304';
        mensaje_v:= 'No existe la postulación indicada.';
        RAISE EXCEPTION 'No existe la postulación indicada.';
    END IF;



    -- Traigo los dato necesarios de la postulación para hacer las validaciones pertinentes
    SELECT p.talento_humano_id, p.clase_cargo_id, p.estatus_aprobado, p.apertura_periodo_id, p.puntuacion_prueba, cc.nivel_cargo_id INTO postulacion_r FROM gestion_humana.postulacion p LEFT JOIN gestion_humana.clase_cargo cc ON p.clase_cargo_id = cc.id WHERE p.id = postulacion_id_vi LIMIT 1;
    -- Validar que la persona que está registrada a la postulación indicada pertenece al usuario indicado
    IF talento_humano_r.id != postulacion_r.talento_humano_id THEN
        codigo_v := 'EDB305';
        mensaje_v:= 'La postulación indicada no pertenece a este usuario.';
        RAISE EXCEPTION 'La postulación indicada no pertenece a este usuario.';
    END IF;

-- validar que la fecha actual este dentro del periodo activo
    SELECT fecha_inicio, fecha_final  INTO  fecha_inicio_v, fecha_final_v FROM gestion_humana.apertura_periodo  WHERE id= postulacion_r.apertura_periodo_id AND estatus='A';
    IF fecha_hora_v  NOT BETWEEN fecha_inicio_v AND fecha_final_v THEN
        codigo_v := 'EDB306';
        mensaje_v:= 'No existe un período actualmente';
        RAISE EXCEPTION 'No existe un período actualmente';
    END IF;

    -- Validar que la persona no haya presentado ya la prueba de conocimiento
    IF postulacion_r.puntuacion_prueba IS NOT NULL THEN
        codigo_v := 'EDB307';
        mensaje_v:= 'Este usuario ha presentado la Prueba de Conocimiento en este Período y ha obtenido una puntuación de '||postulacion_r.puntuacion_prueba::VARCHAR||'.';
        RAISE EXCEPTION 'Este usuario ha presentado la Prueba de Conocimiento en este Período y ha obtenido una puntuación de %.', postulacion_r.puntuacion_prueba::VARCHAR;
    END IF;

    -- Validar la existencia de una prueba de conocimiento registrada para el periodo en el que se aperturó el proceso de concurso público y la postulación así como el nivel
    SELECT COUNT(1) INTO existe_seccion_prueba_v FROM gprueba.seccion_prueba sp WHERE sp.prueba_id = postulacion_r.apertura_periodo_id AND sp.nivel_cargo_id = postulacion_r.nivel_cargo_id AND estatus = 'A' LIMIT 1;
    IF existe_seccion_prueba_v=0 THEN
        codigo_v := 'EDB308';
        mensaje_v:= 'No existe ninguna Prueba de Conocimiento registrada y activa que cumpla con los parámetros de Período Aperturado de Concurso Público y Nivel de Cargo al cual se ha postulado.';
        RAISE EXCEPTION 'No existe ninguna Prueba de Conocimiento registrada y activa que cumpla con los parámetros de Período Aperturado de Concurso Público y Nivel de Cargo al cual se ha postulado.';
    END IF;

    -- Obteniendo el id de la prueba de acuerdo al periodo  activo y al nivel de cargo postulado
    SELECT id INTO id_prueba_v FROM gprueba.seccion_prueba sp WHERE sp.prueba_id = postulacion_r.apertura_periodo_id AND sp.nivel_cargo_id = postulacion_r.nivel_cargo_id AND estatus = 'A' LIMIT 1;

    --validar que esta persona no está presentando la prueba
    IF  (action_vi='VALIDAR') OR (action_vi='PRESENTACION') THEN
        SELECT COUNT(1) INTO presentado_prueba_v FROM gprueba.aplicacion_prueba  ap WHERE ap.seccion_prueba_id= id_prueba_v AND ap.aspirante_id= talento_humano_r.id  AND  ap.estatus_prueba_aplicada!=0 limit 1;
        IF presentado_prueba_v!=0 THEN
            codigo_v := 'EDB309';
            mensaje_v:= 'Este usuario ya iniciado la prueba en alguna oportunidad. Si considera que esto es una anomalia comuniquese con el personal de Ingreso y Clasificación del MPPE proporcionandole sus datos de identificación.';
            RAISE EXCEPTION 'Este usuario ya iniciado la prueba en alguna oportunidad. Si considera que esto es una anomalia comuniquese con el personal de Ingreso y Clasificación del MPPE proporcionandole sus datos de identificación.';
        END IF;
    END IF;

    IF (action_vi='PRESENTACION') THEN
       INSERT INTO
        gprueba.aplicacion_prueba(
            responsable_aplicar_prueba_id,
            aspirante_id,
            prueba_id,
            usuario_ini_id,
            fecha_ini,
            usuario_act_id,
            fecha_act, 
            estatus,
            estatus_prueba_aplicada,
            hora_inicio_prueba, 
            hora_final_prueba, 
            seccion_prueba_id
           
        ) VALUES (
            1,
            talento_humano_r.id,
            postulacion_r.apertura_periodo_id,
            usuario_id_vi,
            fecha_v,
            usuario_id_vi,
            fecha_v,
            'A',
            1,
            horaActual_vi, 
            horaFinalPrueba_vi,
            id_prueba_v
        ) RETURNING id INTO aplicacion_prueba_id_v;
    END IF; 

     IF  (action_vi='EVALUACION') THEN

        
        SELECT COUNT(1) INTO presentado_prueba_v FROM gprueba.aplicacion_prueba  ap WHERE ap.seccion_prueba_id= id_prueba_v AND ap.aspirante_id= talento_humano_r.id  AND  ap.estatus_prueba_aplicada!=1 limit 1;
        IF presentado_prueba_v!=0 THEN

            codigo_v := 'EDB309';
            mensaje_v:= 'Este usuario ya iniciado la prueba en alguna oportunidad. Si considera que esto es una anomalia comuniquese con el personal de Ingreso y Clasificación del MPPE proporcionandole sus datos de identificación.';
            RAISE EXCEPTION 'Este usuario ya iniciado la prueba anteriormente. Si considera que esto es una anomalia comuniquese con el personal de Ingreso y Clasificación del MPPE proporcionandole sus datos de identificación.';
        END IF;
    END IF;

    mensaje_v := 'Bienvenido al Proceso de Ejecución de la Prueba de Conocimiento para el Concurso Público del MPPE.';
    codigo_v := 'S0000';
    estado_resultado_v := 'success';



    SELECT
        mensaje_v,
        estado_resultado_v,
        codigo_v,
        id_prueba_v,
        aplicacion_prueba_id_v,
        talento_humano_r.id
    INTO
        resultado;

   data_v := '{ talento_humano: { id: '||talento_humano_r.id::TEXT||', condicion_actual_id: '||talento_humano_r.condicion_actual_id ::TEXT||', estaus: '||talento_humano_r.estatus::TEXT||', postulacion_id: '||postulacion_id_vi::TEXT||', apertura_periodo_id: '||postulacion_r.apertura_periodo_id::TEXT||', nivel_cargo_id: '||postulacion_r.nivel_cargo_id ::TEXT||'} }';
--  AUDITORIA
    INSERT INTO
        auditoria.traza(
            fecha_hora,
            ip_maquina,
            tipo_transaccion,
            modulo,
            resultado_transaccion,
            descripcion,
            user_id,
            username,
            data
        ) VALUES (
            fecha_hora_v,
            ipaddress_vi,
            tipo_transaccion_v,
            modulo_vi,
            estado_resultado_v,
            mensaje_v,
            usuario_id_vi,
            username_vi,
            data_v
        );
    RETURN resultado;

EXCEPTION

    WHEN OTHERS THEN
    transaccion_v := SQLSTATE;
    mensaje_auditoria_v := 'VALIDACION PRUEBA: '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||transaccion_v;
    mensaje_v := SQLERRM;
    data_v := '{postulacion_id: '||postulacion_id_vi::TEXT||', usuario_id: '||usuario_id_vi::TEXT||', username: '||username_vi ::TEXT||', modulo: '||modulo_vi::TEXT||', ipaddress_vi: '||ipaddress_vi::TEXT||'} }';

    SELECT
        mensaje_v,
        estado_resultado_v,
        codigo_v,
        null::BIGINT, 
        null::BIGINT, 
        null::BIGINT
    INTO
        resultado;

--  AUDITORIA
    INSERT INTO
        auditoria.traza(
            fecha_hora,
            ip_maquina,
            tipo_transaccion,
            modulo,
            resultado_transaccion,
            descripcion,
            user_id,
            username,
            data
        ) VALUES (
            fecha_hora_v,
            ipaddress_vi,
            tipo_transaccion_v,
            modulo_vi,
            estado_resultado_v,
            mensaje_auditoria_v,
            usuario_id_vi,
            username_vi,
            data_v
        );

    RETURN resultado;

END;$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
/*

SELECT * FROM  gprueba.puede_presentar_examen(
    3 ::BIGINT,
    65 ::BIGINT,
    'ple':: CHARACTER VARYING,
    'gestionHumana.postulacion.registro':: CHARACTER VARYING,
    '172.24.61.12':: CHARACTER VARYING
) AS f(
    mensaje text,
    resultado character varying,
    codigo_resultado character varying,
    id_prueba BIGINT

)
*/