-- Function: gestion_humana.cancelar_evaluacion_postulacion(smallint, bigint, bigint, bigint, smallint, character varying, character varying)

-- DROP FUNCTION gestion_humana.cancelar_evaluacion_postulacion(smallint, bigint, bigint, bigint, smallint, character varying, character varying);

CREATE OR REPLACE FUNCTION gestion_humana.cancelar_evaluacion_postulacion(
    tipo_periodo_id_vi smallint,
    postulacion_id_vi bigint,
    talento_humano_id_vi bigint,
    proceso_a_cancelar_vi character varying,
    observacion_vi text,
    usuario_id_vi bigint,
    ipaddress_vi character varying)
  RETURNS record AS
$BODY$
  --Declarando las variables
  DECLARE

    --  Variables de Resultado
    resultado record;

    --  Variables de exception
    --      Seccion que permite localizar hasta que punto corrio el codigo hasta que se genero el error.
    seccion_v SMALLINT := 0;
    mensaje_v VARCHAR := 'Aun no ha generado el mensaje para esta respuesta.'; --SQLERRM

--  Variables de auditoria
    fecha_hora_v DATE := NOW();
    tipo_transaccion_v VARCHAR := 'ESCRITURA'; --Si es ESCRITURA, ELIMINACION, EDICION o LECTURA
    modulo_v VARCHAR := 'gestionHumana.Postulacion.cancelarPostulacion'; --Modulo.Controlador.Accion
    transaccion_v VARCHAR := null;
    resultado_transaccion_v VARCHAR := null;
    mensaje_auditoria_v VARCHAR := null;
    username_v VARCHAR := '';
    data_v TEXT := '';

    postulacion_r RECORD;

    --  existencia
    existencia_talento_humano_id_v INT := 0;
    existencia_postulacion_id_v INT := 0;
    existencia_postulacion_eliminada_v INT := 0;
    existencia_usuario_ini_id_v INT := 0;
    existencia_motivo_eliminacion_id_v INT := 0;
    existencia_tipo_periodo_id_v INT := 0;
    captura_apertura_periodo_id_v INT := 0;

    --constantes
    const_consign_document INT := 0;
    const_postulacion_aprobada INT := 1;
    const_prueba_conocimiento_aprob INT := 2;
    const_req_minimos_aprobados INT := 3;
    const_no_beneficiado INT := 5;

    const_concurso INT := 1;
    const_merito INT := 2;

    const_tpond_requisitos_minimos_id SMALLINT := 2; -- Indica el Id del Tipo de Ponderación "Evaluación de Requisitos Mínimos"
    const_tpond_entrevista_id SMALLINT := 4; -- Indica el Id del Tipo de Ponderación "Proceso de Entrevista"

  BEGIN

    IF proceso_a_cancelar_vi NOT IN ('consignacion_de_documentos', 'requisitos_minimos', 'entrevista') THEN
      mensaje_v := 'El proceso indicado ('||proceso_a_cancelar_vi||') no se encuentra registrado para poder realizar esta acción.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'El proceso indicado % no se encuentra registrado para poder realizar esta acción.',proceso_a_cancelar_vi;
    END IF;

    seccion_v := 1;

    SELECT COUNT(1) INTO existencia_talento_humano_id_v FROM gestion_humana.talento_humano WHERE id = talento_humano_id_vi;
    IF existencia_talento_humano_id_v = 0 THEN
      mensaje_v := 'El Talento Humano no se encuentra en la Base de Datos.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'El Talento Humano no se encuentra registrado en la Base de Datos.';
    END IF;

    seccion_v := 2;

    SELECT COUNT(1) INTO existencia_tipo_periodo_id_v FROM gestion_humana.apertura_periodo WHERE tipo_apertura_id = tipo_periodo_id_vi AND estatus = 'A';
    IF existencia_tipo_periodo_id_v = 0 THEN
      mensaje_v := 'El tipo de proceso no se encuentra aperturado.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'El tipo de proceso no se encuentra aperturado.';
    END IF;

    seccion_v := 3;
    SELECT id INTO captura_apertura_periodo_id_v FROM gestion_humana.apertura_periodo WHERE tipo_apertura_id = tipo_periodo_id_vi AND estatus = 'A';

    seccion_v := 4;

    SELECT COUNT(1) INTO existencia_postulacion_id_v FROM gestion_humana.postulacion WHERE id = postulacion_id_vi AND talento_humano_id = talento_humano_id_vi;
    IF existencia_postulacion_id_v = 0 THEN
      mensaje_v := 'La Postulación no se encuentra registrada en la Base de Datos.';
      resultado_transaccion_v := 'alert';
      RAISE EXCEPTION 'La Postulación no se encuentra registrada en la Base de Datos.';
    END IF;

    SELECT estatus_aprobado, estatus_anterior_aprobado INTO postulacion_r FROM gestion_humana.postulacion WHERE id = postulacion_id_vi AND talento_humano_id = talento_humano_id_vi AND apertura_periodo_id = captura_apertura_periodo_id_v;

    seccion_v := 6;

    IF proceso_a_cancelar_vi = 'consignacion_de_documentos' THEN
      IF postulacion_r.estatus_aprobado IN (const_postulacion_aprobada, const_no_beneficiado) AND postulacion_r.estatus_anterior_aprobado = const_consign_document THEN
        DELETE FROM baremo.entrevista_postulacion WHERE talento_humano_id = talento_humano_id_vi  AND postulacion_id = postulacion_id_vi;
        UPDATE gestion_humana.postulacion SET estatus_aprobado = const_consign_document, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
        UPDATE gestion_humana.postulacion SET estatus_aprobado = const_consign_document, estatus_anterior_aprobado = const_consign_document, observacion = observacion || '. Rectificación: ' ||observacion_vi, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
        mensaje_v := 'Se ha Cancelado de forma exitosa la Evaluación en el Proceso de Consignación de Documentos. La Postulación ha vuelto a su estatus anterior, por lo que podrá hacer de nuevo dicha evaluación.';
      ELSE
        mensaje_v := 'La postulación se encuentra en un estatus en el cual no es posible Cancelar la Consignación de Documentos ('||postulacion_r.estatus_aprobado||':'||postulacion_r.estatus_anterior_aprobado||').';
        resultado_transaccion_v := 'alert';
        RAISE EXCEPTION 'La postulación se encuentra en un estatus en el cual no es posible Cancelar la Consignación de Documentos (%:%).', postulacion_r.estatus_aprobado, postulacion_r.estatus_anterior_aprobado;
      END IF;
    END IF;

    seccion_v := 7;

    IF proceso_a_cancelar_vi = 'requisitos_minimos' THEN
      IF postulacion_r.estatus_aprobado IN (const_req_minimos_aprobados, const_no_beneficiado) AND postulacion_r.estatus_anterior_aprobado = const_postulacion_aprobada THEN
        DELETE FROM baremo.entrevista_ponderacion WHERE talento_humano_id = talento_humano_id_vi  AND postulacion_id = postulacion_id_vi;
        UPDATE baremo.entrevista_postulacion SET finalizada = 0, puntuacion_total_obtenida = 0, puntuacion_req_minimos = 0, puntuacion_entrevista = 0, codigo_constancia_req_minimos = null, observacion_analista = 'Rectificación: '||observacion_vi, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE talento_humano_id = talento_humano_id_vi  AND postulacion_id = postulacion_id_vi;
        IF const_concurso = tipo_periodo_id_vi THEN -- CONCURSO PÚBLICO
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_postulacion_aprobada, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_prueba_conocimiento_aprob, observacion = observacion || '. Rectificación: ' ||observacion_vi, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
        ELSE                                        -- SISTEMA DE MERITO
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_consign_document, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_postulacion_aprobada, observacion = observacion || '. Rectificación: ' ||observacion_vi, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
        END IF;
        mensaje_v := 'Se ha Cancelado de forma exitosa la Evaluación de Requisitos Mínimos. La Postulación ha vuelto a su estatus anterior, por lo que podrá hacer de nuevo dicha evaluación.';
      ELSE
        mensaje_v := 'La postulación se encuentra en un estatus en el cual no es posible Cancelar la Evaluación de Requisitos Mínimos ('||postulacion_r.estatus_aprobado||':'||postulacion_r.estatus_anterior_aprobado||').';
        resultado_transaccion_v := 'alert';
        RAISE EXCEPTION 'La postulación se encuentra en un estatus en el cual no es posible Cancelar la Evaluación de Requisitos Mínimos(%:%).', postulacion_r.estatus_aprobado, postulacion_r.estatus_anterior_aprobado;
      END IF;
    END IF;

    seccion_v := 8;

    IF proceso_a_cancelar_vi = 'entrevista' THEN
      IF postulacion_r.estatus_aprobado IN (const_req_minimos_aprobados, const_no_beneficiado) AND postulacion_r.estatus_anterior_aprobado = const_postulacion_aprobada THEN
        DELETE FROM baremo.entrevista_ponderacion WHERE talento_humano_id = talento_humano_id_vi  AND postulacion_id = postulacion_id_vi AND tipo_ponderacion_id = const_tpond_entrevista_id;
        UPDATE baremo.entrevista_postulacion SET finalizada = 1, puntuacion_total_obtenida = puntuacion_req_minimos, puntuacion_entrevista = 0, codigo_constancia_req_minimos = null, observacion_analista = 'Rectificación: '||observacion_vi, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE talento_humano_id = talento_humano_id_vi  AND postulacion_id = postulacion_id_vi;
        IF const_concurso = tipo_periodo_id_vi THEN -- CONCURSO PÚBLICO
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_prueba_conocimiento_aprob, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_req_minimos_aprobados, observacion = observacion || '. Rectificación: ' ||observacion_vi, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
        ELSE                                        -- SISTEMA DE MERITO
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_postulacion_aprobada, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
          UPDATE gestion_humana.postulacion SET estatus_aprobado = const_req_minimos_aprobados, observacion = observacion || '. Rectificación: ' ||observacion_vi, usuario_act_id = usuario_id_vi, fecha_act = (NOW())::TIMESTAMP(0) WHERE id = postulacion_id_vi;
        END IF;
        mensaje_v := 'Se ha Cancelado de forma exitosa el Registro de la Entrevista. La Postulación ha vuelto a su estatus anterior, por lo que podrá hacer de nuevo dicha evaluación.';
      ELSE
        mensaje_v := 'La postulación se encuentra en un estatus en el cual no es posible Cancelar el Registro de la Entrevista ('||postulacion_r.estatus_aprobado||':'||postulacion_r.estatus_anterior_aprobado||').';
        resultado_transaccion_v := 'alert';
        RAISE EXCEPTION 'La postulación se encuentra en un estatus en el cual no es posible Cancelar la Registro de la Entrevista (%:%).', postulacion_r.estatus_aprobado, postulacion_r.estatus_anterior_aprobado;
      END IF;
    END IF;

    INSERT INTO gestion_humana.postulacion_evaluacion_cancelada(
          proceso_cancelado,
          postulacion_id,
          talento_humano_id,
          observacion,
          usuario_ini_id,
          usuario_act_id
    ) VALUES (
          proceso_a_cancelar_vi,
          postulacion_id_vi,
          talento_humano_id_vi,
          observacion_vi,
          usuario_id_vi,
          usuario_id_vi
    );

    SELECT '{tipo_periodo_id:'||COALESCE(tipo_periodo_id_vi::VARCHAR, '')||
    ', postulacion_id:'||COALESCE(postulacion_id_vi::VARCHAR, '')||
    ', proceso_a_cancelar:'||COALESCE(proceso_a_cancelar_vi::VARCHAR, '')||
    ', observacion:"'||COALESCE(observacion_vi::TEXT, '')||
    ', analista_user_id:"'||COALESCE(usuario_id_vi::VARCHAR, '')||'" }' INTO data_v;

    resultado_transaccion_v := 'success';
    mensaje_auditoria_v := mensaje_v;
    seccion_v := 9;
    SELECT
      mensaje_v,
      seccion_v,
      resultado_transaccion_v
    --Campos
    INTO
      resultado;

    seccion_v := 9;
    --  AUDITORIA
    INSERT INTO
      auditoria.traza(
        fecha_hora,
        ip_maquina,
        tipo_transaccion,
        modulo,
        resultado_transaccion,
        descripcion,
        user_id,
        username,
        data
      ) VALUES (
      fecha_hora_v,
      ipaddress_vi,
      tipo_transaccion_v,
      modulo_v,
      resultado_transaccion_v,
      mensaje_auditoria_v,
      usuario_id_vi,
      username_v,
      data_v
    );
    RETURN resultado;

    EXCEPTION

    WHEN OTHERS THEN
      transaccion_v := SQLSTATE;
      IF resultado_transaccion_v is null OR length(resultado_transaccion_v) = 0 THEN
        resultado_transaccion_v := 'error';
      END IF;
      mensaje_auditoria_v := 'CANCELAR DE EVALUACIÓN PARA CORRECCIÓN POSTERIOR: '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||transaccion_v||')';
      mensaje_v := 'Ha ocurrido un error. No se ha culminado el proceso motivado a lo siguiente: '||SQLERRM;
      SELECT '{tipo_periodo_id:'||COALESCE(tipo_periodo_id_vi::VARCHAR, '')||
    ', postulacion_id:'||COALESCE(postulacion_id_vi::VARCHAR, '')||
    ', proceso_a_cancelar:'||COALESCE(proceso_a_cancelar_vi::VARCHAR, '')||
    ', observacion:"'||COALESCE(observacion_vi::TEXT, '')||
    ', analista_user_id:"'||COALESCE(usuario_id_vi::VARCHAR, '')||'" }' INTO data_v;
      --  AUDITORIA
      INSERT INTO
        auditoria.traza(
          fecha_hora,
          ip_maquina,
          tipo_transaccion,
          modulo,
          resultado_transaccion,
          descripcion,
          user_id,
          username,
          data
        ) VALUES (
        fecha_hora_v,
        ipaddress_vi,
        tipo_transaccion_v,
        modulo_v,
        resultado_transaccion_v,
        mensaje_auditoria_v,
        usuario_id_vi,
        username_v,
        data_v
      );

      SELECT
        mensaje_v,
        seccion_v,
        resultado_transaccion_v
      --Campos
      INTO
        resultado;

      RETURN resultado;

  END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;