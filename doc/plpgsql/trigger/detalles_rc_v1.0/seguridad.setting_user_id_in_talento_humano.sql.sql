-- Función que será ejecutada por el trigger
CREATE OR REPLACE FUNCTION seguridad.setting_user_id_in_talento_humano()
  RETURNS trigger AS
$BODY$
DECLARE
BEGIN
    IF (TG_OP = 'INSERT') THEN
       UPDATE gestion_humana.talento_humano SET user_id = NEW.id, fecha_act = (NOW()::timestamp(0)), usuario_act_id = NEW.user_ini_id WHERE origen = NEW.origen AND cedula = NEW.cedula;
    END IF;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- Definición del Trigger
CREATE TRIGGER seguridad_setting_users_talento_humano
  AFTER INSERT
  ON seguridad.usergroups_user
  FOR EACH ROW
  EXECUTE PROCEDURE seguridad.setting_users_talento_humano();
