-- Function: gestion_humana.horas_curso_realizado()

DROP TRIGGER gestion_humana_actualiza_datos_postulacion ON gestion_humana.postulacion;

DROP FUNCTION gestion_humana.actualiza_datos_postulacion();

CREATE OR REPLACE FUNCTION gestion_humana.actualiza_datos_postulacion()
  RETURNS trigger AS
$BODY$
DECLARE

    horas_curso_totales_v INT := 0;

    cant_autoridad_vigente_ingreso_v INT;
    cant_autoridad_vigente_oficgesthumana_v INT;
    cant_autoridad_vigente_rrhh_v INT;

    autoridad_vigente_ingreso_v INT;
    autoridad_vigente_oficgesthumana_v INT;
    autoridad_vigente_rrhh_v INT;

BEGIN

    -- RAISE NOTICE 'TG: TG_OP: %', TG_OP;

    IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN

	-- RAISE NOTICE 'TG: ESTATUS_APROBADO: %', NEW.estatus_aprobado;

        IF (NEW.estatus_aprobado IN (0, 1, 2)) OR TG_OP = 'INSERT' THEN

	    SELECT SUM(horas) INTO horas_curso_totales_v FROM gestion_humana.curso_realizado
             WHERE talento_humano_id = NEW.talento_humano_id AND estatus = 'C';

	    -- RAISE NOTICE 'TG: HORAS CURSO: %', horas_curso_totales_v;

            NEW.horas_totales_cursos := horas_curso_totales_v;

            -- RAISE NOTICE 'TG: HORAS CURSO POSTULACIÓN: %', NEW.horas_totales_cursos;

        END IF;
    END IF;

    IF (TG_OP = 'INSERT') THEN -- ESTE SÓLO PARA INSERTS

        SELECT COUNT(1) INTO cant_autoridad_vigente_ingreso_v FROM gestion_humana.autoridad WHERE dependencia_id = 3210 AND estatus = 'A' LIMIT 1; -- LA DEPENDENCIA CON ID 3210 ES INGRESO Y CLASIFICACIÓN
        SELECT COUNT(1) INTO cant_autoridad_vigente_oficgesthumana_v FROM gestion_humana.autoridad WHERE dependencia_id = 531 AND estatus = 'A' LIMIT 1; -- LA DEPENDENCIA CON ID 531 ES OFICINA DE GESTION HUMANA
        SELECT COUNT(1) INTO cant_autoridad_vigente_rrhh_v FROM gestion_humana.autoridad WHERE dependencia_id = 74 AND estatus = 'A' LIMIT 1; -- LA DEPENDENCIA CON ID 74 ES DIR GENERAL DE RRHH

        IF (cant_autoridad_vigente_ingreso_v>0) THEN
            -- Selecciono la autoridad actual de Ingreso y Clasificación
            SELECT id INTO autoridad_vigente_ingreso_v FROM gestion_humana.autoridad WHERE dependencia_id = 3210 AND estatus = 'A' LIMIT 1;
            NEW.autoridad_vigente_deingreso_id := autoridad_vigente_ingreso_v;
        END IF;

        IF (cant_autoridad_vigente_ingreso_v>0) THEN
            -- Selecciono la autoridad actual de la Oficina de Gestión Humana
            SELECT id INTO autoridad_vigente_oficgesthumana_v FROM gestion_humana.autoridad WHERE dependencia_id = 531 AND estatus = 'A' LIMIT 1;
            NEW.autoridad_vigente_degestionhumana_id := autoridad_vigente_oficgesthumana_v;
        END IF;

        IF (cant_autoridad_vigente_rrhh_v>0) THEN
            -- Selecciono la autoridad actual de la Dir. General de RRHH
            SELECT id INTO autoridad_vigente_rrhh_v FROM gestion_humana.autoridad WHERE dependencia_id = 74 AND estatus = 'A' LIMIT 1;
            NEW.autoridad_vigente_derrhh_id := autoridad_vigente_rrhh_v;
        END IF;

        -- RAISE NOTICE 'TG: AUTORIDAD INGRESO: %', NEW.autoridad_vigente_deingreso_id;
        -- RAISE NOTICE 'TG: AUTORIDAD GESTHUM: %', NEW.autoridad_vigente_degestionhumana_id;
        -- RAISE NOTICE 'TG: AUTORIDAD DG-RRHH: %', NEW.autoridad_vigente_derrhh_id;

    END IF;

    IF (TG_OP = 'UPDATE') THEN
	-- RAISE NOTICE 'TG: ESTATUS ANTERIOR APROBADO NEW: %', NEW.estatus_aprobado;
	-- RAISE NOTICE 'TG: ESTATUS ANTERIOR APROBADO OLD: %', OLD.estatus_aprobado;
	-- RAISE NOTICE 'TG: ESTATUS REG: %', NEW.estatus;
        IF (NEW.estatus = 'A' AND (NEW.estatus_aprobado != OLD.estatus_aprobado)) THEN
            NEW.estatus_anterior_aprobado := OLD.estatus_aprobado;
        END IF;
    END IF;

    --
    RETURN NEW;

EXCEPTION
    WHEN OTHERS THEN
	-- RAISE EXCEPTION '%', SQLERRM;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER gestion_humana_actualiza_datos_postulacion
BEFORE INSERT OR UPDATE
ON gestion_humana.postulacion
FOR EACH ROW
EXECUTE PROCEDURE gestion_humana.actualiza_datos_postulacion();

/*
INSERT INTO gestion_humana.postulacion(
            talento_humano_id, condicion_nominal_id, periodo_evaluacion,
            cargo_evaluacion_id, dependencia_evaluacion_id, dependencia_evaluacion,
            supervisor_inmediato, rango_actuacion_id, puntuacion_obtenida,
            apertura_periodo_id, estado_id, dependencia_postulado_id, dependencia_general_postulado,
            clase_cargo_id, cargo_postulado_id, estatus, usuario_ini_id,
            fecha_ini, usuario_act_id, fecha_act, fecha_eli, puntuacion_desempeno,
            estatus_aprobado, codigo_constancia, documentos_consignado
            )
    VALUES (231718,
1,
'2015-1 : 01/01/2015 al 30/06/2015',
366,
83,
'DIV DE ARCHIVO DE PERSONAL',
'JOSÉ GABRIEL GONZÁLEZ',
4,
410,
2,
21,
99,
'DIV DE ARCHIVO DE PERSONAL',
5,
34431,
'A',
1,
'2015-09-27 05:37:56',
1,
'2015-09-27 05:37:56',
null, 410, 0, null, null);
*/
