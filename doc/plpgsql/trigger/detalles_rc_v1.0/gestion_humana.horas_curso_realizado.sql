-- Function: gestion_humana.horas_curso_realizado()

-- DROP FUNCTION gestion_humana.horas_curso_realizado();

--CREATE OR REPLACE FUNCTION gestion_humana.horas_postulacion()
CREATE OR REPLACE FUNCTION gestion_humana.update_horas_curso_realizado()
  RETURNS trigger AS
$BODY$
DECLARE

    horas_curso_totales_v INT := 0;

BEGIN 

    IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
        SELECT SUM(horas) INTO horas_curso_totales_v FROM gestion_humana.curso_realizado WHERE talento_humano_id = NEW.talento_humano_id AND estatus = 'C';
        UPDATE gestion_humana.postulacion SET horas_totales_cursos = horas_curso_totales_v WHERE apertura_periodo_id IN (SELECT id FROM gestion_humana.apertura_periodo WHERE estatus = 'A') AND talento_humano_id = NEW.talento_humano_id AND estatus_aprobado IN (0, 1, 2, 4);
    ELSIF (TG_OP = 'DELETE') THEN
        SELECT SUM(horas) INTO horas_curso_totales_v FROM gestion_humana.curso_realizado WHERE talento_humano_id = OLD.talento_humano_id AND estatus = 'C';
        UPDATE gestion_humana.postulacion SET horas_totales_cursos = horas_curso_totales_v WHERE apertura_periodo_id IN (SELECT id FROM gestion_humana.apertura_periodo WHERE estatus = 'A') AND talento_humano_id = OLD.talento_humano_id AND estatus_aprobado IN (0, 1, 2, 4);
    END IF;

    --
    RETURN NEW;

EXCEPTION
    WHEN OTHERS THEN

    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER gestion_humana_update_horas_curso_realizado
AFTER INSERT OR UPDATE OR BEFORE DELETE
ON gestion_humana.curso_realizado
FOR EACH ROW
EXECUTE PROCEDURE gestion_humana.update_horas_curso_realizado();

