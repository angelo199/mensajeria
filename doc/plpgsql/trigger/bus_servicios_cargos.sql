-- Function: mirror.bus_servicios_cargos()

-- DROP FUNCTION mirror.bus_servicios_cargos();

CREATE OR REPLACE FUNCTION mirror.bus_servicios_cargos()
  RETURNS trigger AS
$BODY$
DECLARE

--Variables de captura.
  ccargo_old_v varchar;
  dcargo_old_v varchar;
  tpersona_v varchar;
  categoria_cargo_nominal_id_v INT := null;
  cgrado_v smallint := null;
  captura_id_cargo_v INT := null;

--Variables de EXCEPTION
  descripcion_v TEXT := 'Bus de servicio con Nómina que permite la transferencia de los datos entre la tabla dbcontrato.tcargos y dbfija.tcargos de la base de datos de nomina con las tablas mirror.nm_contratados_tcargo y mirror.nm_fijos_tcargo de app_seguros';
  nombre_ktr_v VARCHAR := null;
  nombre_kjb_v VARCHAR := 'JOB_MIRROR_APP_CONCURSO_MERITO';
  observaciones_v TEXT := null;
  seccion_v TEXT := 'EXITO';  


--Variables de existencia.
  existencia_ccn_v INT := 0;
  existencia_tpersona_v INT := 0;
  existencia_tpersona_tipo_v INT := 0;

--Variables 
  fecha_actual_v TIMESTAMP WITH TIME ZONE := NOW();
  codigo_v varchar;
  tabla_v text;

   
BEGIN

--Ejecutar cuando se realiza un INSERT o un UPDATE.
  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
--Si es un INSERT, el codigo a comparar es el nuevo codigo que se ingresa
    IF (TG_OP = 'INSERT') THEN
      codigo_v := NEW.ccargo;
      tpersona_v := NEW.tpersona;
      cgrado_v := NULLIF(TRIM(NEW.cgrado::VARCHAR), '')::SMALLINT;
  --Si es un UPDATE, el codigo a comparar es el codigo que se encuentra registrado
    ELSEIF (TG_OP = 'UPDATE') THEN
      codigo_v := OLD.ccargo;
      tpersona_v := OLD.tpersona;
      cgrado_v := NULLIF(TRIM(OLD.cgrado::VARCHAR), '')::SMALLINT;
    END IF;


    IF (TG_TABLE_NAME = 'nm_contratados_tcargo') THEN
      nombre_ktr_v := 'TRANS_CONTRATADOS_TCARGO';
    ELSEIF(TG_TABLE_NAME = 'nm_fijos_tcargo') THEN
      nombre_ktr_v := 'TRANS_FIJOS_TCARGO';
    END IF;


    IF (tpersona_v is null) THEN
      SELECT COUNT(1) INTO existencia_tpersona_tipo_v FROM mirror.nm_contratados_tpersona WHERE ccargo = codigo_v limit 1;
      IF (existencia_tpersona_tipo_v > 0) THEN
        SELECT tpersona INTO tpersona_v FROM mirror.nm_contratados_tpersona WHERE ccargo = codigo_v limit 1;
      ELSE
        IF(substring(NEW.dcargo , '....')) = 'DOC.' THEN 
          tpersona_v := 'D';
        END IF;
      END IF;
    END IF;


    SELECT count(1) INTO existencia_tpersona_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = tpersona_v;
    IF existencia_tpersona_v = 1 THEN
  --  Captura el id del tipo de persona en categoria cargo nominal
      SELECT id INTO categoria_cargo_nominal_id_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = tpersona_v;
    ELSE
      RAISE EXCEPTION 'El tipo de persona: % No se pudo encontrar en el cargo: %', tpersona_v, NEW.ccargo;
    END IF;


--  Validar si ya existe un registro en categoria_cargo_nominal con el codigo de ccargo.
    SELECT count(1) INTO existencia_ccn_v FROM gestion_humana.cargo_nominal WHERE codigo = codigo_v;

--  Primero en caso de que no exista registro en la tabla categoria_cargo_nominal se inserta el registro.
    IF existencia_ccn_v = 0 THEN
    /* LOS REGISTROS SE ESTAN DUPLICANDO. Agregar UNIC a codigo en cargo_nomina de gestion humana, verificar por que se duplican los registros en el trigger, OJO cuando ingresa los datos en distintas tablas,  puede estar ahi el fallo*/

      IF (TG_TABLE_NAME = 'nm_contratados_tcargo') THEN
        INSERT INTO gestion_humana.cargo_nominal (codigo, nombre, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, categoria_cargo_id) VALUES (NEW.ccargo, NEW.dcargo, 1, 1, fecha_actual_v, fecha_actual_v, categoria_cargo_nominal_id_v)  RETURNING id INTO captura_id_cargo_v;
      ELSEIF(TG_TABLE_NAME = 'nm_fijos_tcargo') THEN
        INSERT INTO gestion_humana.cargo_nominal (codigo, nombre, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, categoria_cargo_id, cgrado) VALUES (NEW.ccargo, NEW.dcargo, 1, 1, fecha_actual_v, fecha_actual_v, categoria_cargo_nominal_id_v, cgrado_v)  RETURNING id INTO captura_id_cargo_v;
      END IF;
--  Si ya posee un registro, Actualiza los datos codigo, nombre y siglas del registro.
    ELSE 
       IF (TG_TABLE_NAME = 'nm_contratados_tcargo') THEN
        UPDATE gestion_humana.cargo_nominal SET codigo = NEW.ccargo, nombre = NEW.dcargo, categoria_cargo_id = categoria_cargo_nominal_id_v WHERE codigo = codigo_v RETURNING id INTO captura_id_cargo_v;
      ELSEIF(TG_TABLE_NAME = 'nm_fijos_tcargo') THEN
        UPDATE gestion_humana.cargo_nominal SET codigo = NEW.ccargo, nombre = NEW.dcargo, categoria_cargo_id = categoria_cargo_nominal_id_v, cgrado = cgrado_v WHERE codigo = codigo_v RETURNING id INTO captura_id_cargo_v;
      END IF;
    END IF;
  END IF;
  IF captura_id_cargo_v is not null THEN 
    SELECT 1 INTO NEW.estado_carga;
  END IF;
  PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);
  RETURN NEW;
EXCEPTION
    WHEN OTHERS THEN

    IF (TG_TABLE_NAME = 'nm_contratados_tcargo') THEN
      nombre_ktr_v := 'TRANS_CONTRATADOS_TCARGO';
    ELSEIF(TG_TABLE_NAME = 'nm_fijos_tcargo') THEN
      nombre_ktr_v := 'TRANS_FIJOS_TCARGO';
    END IF;

    seccion_v := 'EXCEPTION';
    observaciones_v := SQLERRM;
    PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);

    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


/*

CREATE TRIGGER mirror_bus_servicios_contratados_tcargo
BEFORE INSERT OR UPDATE
ON mirror.nm_contratados_tcargo
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_cargos();


CREATE TRIGGER mirror_bus_servicios_fijos_tcargo
BEFORE INSERT OR UPDATE
ON mirror.nm_fijos_tcargo
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_cargos();



*/