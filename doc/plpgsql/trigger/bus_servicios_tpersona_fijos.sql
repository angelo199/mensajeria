CREATE OR REPLACE FUNCTION mirror.bus_servicios_tpersona_tpercardep()
  RETURNS trigger AS
$BODY$
DECLARE
--	Variables 
	verificado_saime_v VARCHAR;
	nombre_v VARCHAR := '';
	apellido_v VARCHAR := '';
	fecha_ingreso_v DATE; 
	condicion_actual_id_v INT := 1;
	tipo_nomina_id_v INT := 1;
	condicion_v VARCHAR := 'Fijo';
	usuario_id_v INT := 2;
	fecha_actual_v TIMESTAMP WITH TIME ZONE := NOW();
	fecha_nacimiento_errada_v SMALLINT := 0;
	tiempo_inicio TIMESTAMP := clock_timestamp();
	tiempo varchar := null;


--	Constantes
	const_institucion_mppe_v VARCHAR := 'MPPE';
	const_es_institucion_publica_v  VARCHAR := 'S';
	const_actualmente_activo_v VARCHAR := 'S';
	conts_condicion_nominal_id_v INT := 1;
	conts_condicion_v VARCHAR := 'Contratado';
	const_usuario_ini_id_v INT := 2;
	const_tipo_nomina_fijo_id_v INT := 1;
	const_fecha_actual_v TIMESTAMP WITH TIME ZONE := NOW();
	const_experiencia_laboral_mppe_registrada_v INT := 1;


--	Variables de EXCEPTION
	descripcion_v TEXT := 'Bus de servicio con Nómina que permite la transferencia de los datos entre la tabla dbfijas.tpersona de la base de datos de nomina y mirror.nm_fijos_tpersona de app_seguros';
	nombre_ktr_v VARCHAR := 'TRANS_FIJOS_TPERSONA';
	nombre_kjb_v VARCHAR := 'JOB_MIRROR_APP_CONCURSO_MERITO';
	observaciones_v TEXT := null;
	seccion_v SMALLINT := 0 ;
	estatus_seccion_v TEXT := 'EXITO';


--	Variables Array
	array_tpersona_v VARCHAR[];

--	Variables Indices
	indice_tpersona_v INT := 1;
	indice_final_tpersona_v INT := 0;

--	Variables de estado
	estado_cobertura_v VARCHAR := '';

--	Variables de captura
	captura_ccedula_v INT := null;
	captura_tnacionalidad_v VARCHAR := null;
	captura_primer_nombre_v VARCHAR;
	captura_segundo_nombre_v VARCHAR;
	captura_primer_apellido_v VARCHAR;
	captura_segundo_apellido_v VARCHAR;
	captura_centidad_v VARCHAR;
	captura_cmunicipio_v VARCHAR;
	captura_id_estado_v INT;
	captura_id_municipio_v INT := null;
	captura_cargo_nominal_id_v INT := null;
	captura_tipo_cuenta_id_v INT := null;
	captura_ccargo_v VARCHAR := null;
	captura_cdependencia_v VARCHAR := null;
	captura_cbanco_v VARCHAR := null;
	captura_cuenta_banco_v VARCHAR := null;
	captura_istatus_v VARCHAR := null;
	captura_tpersona_v VARCHAR := '';
	captura_cargo_v VARCHAR := '';
	captura_dependencia_v VARCHAR := '';
	captura_zona_educativa_v VARCHAR := '';
	captura_dependencia_id_v INT := null;
	captura_categoria_cargo_actual_id_v INT := null;
	captura_tipo_personal_v VARCHAR := '';
	captura_horas_v DOUBLE PRECISION := 0;
	captura_cobertura_id_v INT := null;
	captura_categoria_cargo_id_v INT := null;
	captura_fecha_nacimiento_v DATE;
	captura_fecha_nacimiento_saime_v DATE;
	captura_sexo_saime_v VARCHAR := '';
	captura_sexo_v VARCHAR;
	captura_estado_dependencia_id_v INT := null;
	captura_nombre_dependencia_estado_v VARCHAR := null;
	captura_id_talento_humano_v INT := null;
	captura_nombre_cargo_v VARCHAR := null;
	captura_id_experiencia_laboral_v INT := null;

--	Variables de existencia
	existencia_cedula_saime_v INT := 0;
	existencia_tpercardep_v INT := 0;
	existencia_talento_humano_v INT := 0;
	existencia_codigo_nomina_v INT := 0;
	existencia_co_munc_asap_v INT := 0;
	existencia_cargo_nominal_v INT := 0;
	existencia_tipo_cuenta_id_v INT := 0;
	existencia_ccargo_v INT := 0;
	existencia_cdependencia_v INT := 0;
	existencia_dependencia_v INT := 0;
	existencia_tpersona_v INT := 0;
	existencia_nhora_doc_v INT := 0;
	existencia_nhora_adm_v INT := 0;
	existencia_nhora_obr_v INT := 0;
	existencia_etl_v INT := 0;
	existencia_estado_id_v INT := 0;
	existencia_experiencia_laboral_mppe_v INT := 0;

--	Variables de cantidad
	cantidad_nhora_doc_v DOUBLE PRECISION := 0;
	cantidad_nhora_adm_v DOUBLE PRECISION := 0;
	cantidad_nhora_obr_v DOUBLE PRECISION := 0;

BEGIN

seccion_v := 1;


SELECT  (clock_timestamp() - tiempo_inicio ) INTO tiempo;
RAISE NOTICE 'tiempo transcurrido: %',tiempo;
RAISE NOTICE 'Inicio';

--Ejecutar cuando se realiza un INSERT o un UPDATE.
	IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  	Seteando variables que se usaran para buscar los datos cuando sea un INSERT
	    IF (TG_OP = 'INSERT') THEN
	    RAISE NOTICE 'Es un insert';
	    	captura_ccedula_v := NEW.ccedula;
	    	captura_tnacionalidad_v := NEW.tnacionalidad;
--  	Seteando variables que se usaran para buscar los datos cuando sea un UPDATE
	    ELSEIF (TG_OP = 'UPDATE') THEN
	    RAISE NOTICE 'Es un update';
	    	captura_ccedula_v := OLD.ccedula;
	    	captura_tnacionalidad_v := OLD.tnacionalidad;
	    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------
		seccion_v := 2;
--		Validar la existencia de la Cedula en la tabla tpercardep
--(----------------------------------------------------------------------------------------------------------------------------------------------------
		SELECT COUNT(1) INTO existencia_tpercardep_v FROM mirror.nm_fijos_tpercardep WHERE ccedula = captura_ccedula_v AND tnacionalidad = captura_tnacionalidad_v;
		IF (existencia_tpercardep_v > 0) THEN
		RAISE NOTICE 'existencia_tpercardep_v > 0';
			
--			Valida si solo existe un registro en tpercardep de la cedula, dado el caso se seleccionan los valores de ese unico registro.
			IF (existencia_tpercardep_v = 1) THEN
			RAISE NOTICE 'existencia_tpercardep_v = 1';
				SELECT 
					centidad, cmunicipio, tpersona, ccargo, cdependencia, cbanco, ccta_banco, istatus
				INTO
					captura_centidad_v, captura_cmunicipio_v, captura_tpersona_v, captura_ccargo_v, captura_cdependencia_v, captura_cbanco_v, captura_cuenta_banco_v, captura_istatus_v
				FROM 
					mirror.nm_fijos_tpercardep 
				WHERE 
					ccedula = captura_ccedula_v AND tnacionalidad = captura_tnacionalidad_v;

--			Validar sí existen mas de un registro de la cedula en tpercardep, dado el caso inicia el proceso para determinar cual de todos esos registro es el que nos permitira la data para trabajar
			ELSEIF (existencia_tpercardep_v > 1) THEN
			RAISE NOTICE 'existencia_tpercardep_v > 1';
--				+Seleccionando la mayor cantidad de horas de cada tipo de persona

--				Mayor cantidad de horas de: Docente.
				SELECT COUNT(1) INTO existencia_nhora_doc_v FROM mirror.nm_fijos_tpercardep WHERE ccedula = captura_ccedula_v AND tnacionalidad = captura_tnacionalidad_v AND tpersona = 'D';
				IF (existencia_nhora_doc_v > 0) THEN
				RAISE NOTICE 'existencia_nhora_doc_v > 0';
					SELECT MAX(nhora_doc) 
					INTO cantidad_nhora_doc_v
					FROM mirror.nm_fijos_tpercardep 
					WHERE ccedula = captura_ccedula_v AND tpersona = 'D'  limit 1;
				END IF;

--				Mayor cantidad de horas de: Administrativo.
				SELECT COUNT(1) INTO existencia_nhora_adm_v FROM mirror.nm_fijos_tpercardep WHERE ccedula = captura_ccedula_v AND tnacionalidad = captura_tnacionalidad_v AND tpersona = 'A';
				IF (existencia_nhora_adm_v > 0) THEN
				RAISE NOTICE 'existencia_nhora_adm_v > 0';
					SELECT MAX(nhora_adm) 
					INTO cantidad_nhora_adm_v
					FROM mirror.nm_fijos_tpercardep 
					WHERE ccedula = captura_ccedula_v AND tpersona = 'A'  limit 1;
				END IF;

--				Mayor cantidad de horas de: Obrero
				SELECT COUNT(1) INTO existencia_nhora_obr_v FROM mirror.nm_fijos_tpercardep WHERE ccedula = captura_ccedula_v AND tnacionalidad = captura_tnacionalidad_v AND tpersona = 'O';
				IF (existencia_nhora_obr_v > 0) THEN
				RAISE NOTICE 'existencia_nhora_obr_v > 0';
					SELECT MAX(nhora_adm) 
					INTO cantidad_nhora_obr_v
					FROM mirror.nm_fijos_tpercardep 
					WHERE ccedula = captura_ccedula_v AND tpersona = 'O' limit 1;
				END IF;

--				Selecciona entre Docente, Administrativo y Obrero cual es el que posee la mayor carga de horas.
				SELECT UNNEST(ARRAY['D', 'A', 'O']), UNNEST(ARRAY[cantidad_nhora_doc_v	, cantidad_nhora_adm_v, cantidad_nhora_obr_v]) as horas INTO captura_tpersona_v, captura_horas_v ORDER BY horas DESC LIMIT 1;
--				+Capturando los datos segun el tipo de persona seleccionado

--				Docente:
				IF(captura_tpersona_v = 'D') THEN
				RAISE NOTICE 'captura_tpersona_v = "D"';
					SELECT 
						centidad, cmunicipio, ccargo, cdependencia, cbanco, ccta_banco, istatus
					INTO
						captura_centidad_v, captura_cmunicipio_v, captura_ccargo_v, captura_cdependencia_v, captura_cbanco_v, captura_cuenta_banco_v, captura_istatus_v
					FROM mirror.nm_fijos_tpercardep 
					WHERE ccedula = captura_ccedula_v AND tpersona = captura_tpersona_v AND nhora_doc = captura_horas_v
					ORDER BY fdesde DESC limit 1;
--				Administrativo:
				ELSEIF (captura_tpersona_v = 'A') THEN
				RAISE NOTICE 'captura_tpersona_v = "A"';
					SELECT 
						centidad, cmunicipio, ccargo, cdependencia, cbanco, ccta_banco, istatus
					INTO
						captura_centidad_v, captura_cmunicipio_v, captura_ccargo_v, captura_cdependencia_v, captura_cbanco_v, captura_cuenta_banco_v, captura_istatus_v
					FROM mirror.nm_fijos_tpercardep 
					WHERE ccedula = captura_ccedula_v AND tpersona = captura_tpersona_v AND nhora_adm = captura_horas_v
					ORDER BY fdesde DESC limit 1;
--				Obrero:
				ELSEIF(captura_tpersona_v = 'O') THEN
				RAISE NOTICE 'captura_tpersona_v = "O"';
					SELECT 
						centidad, cmunicipio, ccargo, cdependencia, cbanco, ccta_banco, istatus
					INTO
						captura_centidad_v, captura_cmunicipio_v, captura_ccargo_v, captura_cdependencia_v, captura_cbanco_v, captura_cuenta_banco_v, captura_istatus_v
					FROM mirror.nm_fijos_tpercardep 
					WHERE ccedula = captura_ccedula_v AND tpersona = captura_tpersona_v AND nhora_adm = captura_horas_v
					ORDER BY fdesde DESC limit 1;
				END IF;

			END IF;
--)---------------------------------------------------------------------------------------------------------------------------------------------------

SELECT  (clock_timestamp() - tiempo_inicio ) INTO tiempo;
RAISE NOTICE 'tiempo transcurrido: %',tiempo;

--			Almacena el formato correcto de la fecha:
			SELECT  NEW.nano_ingreso||'-'||NEW.nmes_ingreso||'-'||NEW.ndia_ingreso INTO fecha_ingreso_v;

--(----------------------------------------------------------------------------------------------------------------------------------------------------
--		  	Determinar el id del estado segun el codigo_ini
	    	SELECT COUNT(1) INTO existencia_codigo_nomina_v FROM catastro.estado WHERE codigo_nomina = captura_centidad_V;
	    	IF (existencia_codigo_nomina_v = 1) THEN 
	    		RAISE NOTICE 'existencia_codigo_nomina_v = 1';
	  			SELECT id INTO captura_id_estado_v FROM catastro.estado WHERE codigo_nomina = captura_centidad_v;
			END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------


--(----------------------------------------------------------------------------------------------------------------------------------------------------

--		  	Determinar el id del municipio segun el codigo_ini
	    	SELECT COUNT(1) INTO existencia_co_munc_asap_v FROM catastro.municipio WHERE co_munc_asap = captura_cmunicipio_V;
	    	IF (existencia_co_munc_asap_v = 1) THEN 
	    		RAISE NOTICE 'existencia_co_munc_asap_v = 1';
	  			SELECT id INTO captura_id_municipio_v FROM catastro.municipio WHERE co_munc_asap = captura_cmunicipio_v;
			END IF;

--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------

--			Determinar el ID del Cargo Nominal por medio del codigo de cargo 			
			SELECT COUNT(1) INTO existencia_cargo_nominal_v FROM gestion_humana.cargo_nominal WHERE codigo = captura_ccargo_v;
			IF (existencia_cargo_nominal_v = 1) THEN
				RAISE NOTICE 'existencia_cargo_nominal_v = 1';
				SELECT id, codigo||' - '||nombre, nombre INTO captura_cargo_nominal_id_v, captura_cargo_v, captura_nombre_cargo_v FROM gestion_humana.cargo_nominal WHERE codigo = captura_ccargo_v;
			END IF;

--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------


--			Captura el nombre de la Zona Eduvativa.
			SELECT COUNT(1) INTO existencia_cdependencia_v FROM mirror.nm_fijos_tdependencia WHERE cdependencia = captura_cdependencia_v;
			IF ( existencia_cdependencia_v = 1 ) THEN
				RAISE NOTICE 'existencia_cdependencia_v = 1';
				SELECT ddependencia INTO captura_zona_educativa_v FROM mirror.nm_fijos_tdependencia WHERE cdependencia = captura_cdependencia_v AND izona = 'S';
			END IF;

--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
--			captura el nombre de id y el nombre de la dependencia
			SELECT COUNT(1) INTO existencia_dependencia_v FROM gestion_humana.dependencia WHERE codigo = captura_cdependencia_v;
			IF (existencia_dependencia_v = 1) THEN
				RAISE NOTICE 'existencia_dependencia_v = 1';
				SELECT id, codigo||' - '||nombre, estado_id INTO captura_dependencia_id_v, captura_dependencia_v, captura_estado_dependencia_id_v FROM gestion_humana.dependencia WHERE codigo = captura_cdependencia_v;
			END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
--		  	Determinar el id del estado de la dependencia
	    	SELECT COUNT(1) INTO existencia_estado_id_v FROM catastro.estado WHERE id = captura_estado_dependencia_id_v;
	    	IF (existencia_estado_id_v = 1) THEN 
	    		RAISE NOTICE 'existencia_codigo_nomina_v = 1';
	  			SELECT nombre INTO captura_nombre_dependencia_estado_v FROM catastro.estado WHERE id = captura_estado_dependencia_id_v;
			END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------


--(----------------------------------------------------------------------------------------------------------------------------------------------------
--			captura el captura_categoria_cargo_id_v, captura_categoria_cargo_actual_id_v y captura_tipo_personal_v de categoria_cargo_nominal
		  	SELECT COUNT(1) INTO existencia_tpersona_v FROM gestion_humana.categoria_cargo_nominal WHERE siglas = captura_tpersona_v;
		  	IF (existencia_tpersona_v = 1) THEN
		  		RAISE NOTICE 'existencia_tpersona_v = 1';
--  			Captura el nombre del tipo de persona en categoria cargo nominal
				SELECT id, nombre INTO captura_categoria_cargo_actual_id_v, captura_tipo_personal_v FROM gestion_humana.categoria_cargo_nominal WHERE siglas = captura_tpersona_v;
  			END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
			SELECT COUNT(1) INTO existencia_tipo_cuenta_id_v FROM administracion.tipo_cuenta WHERE upper(substring(nombre, 1, 1)) = captura_cbanco_v; 
			IF (existencia_tipo_cuenta_id_v = 1) THEN 
				RAISE NOTICE 'existencia_tipo_cuenta_id_v = 1';
				SELECT id INTO captura_tipo_cuenta_id_v FROM administracion.tipo_cuenta WHERE upper(substring(nombre, 1, 1)) = captura_cbanco_v; 
			END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------


			SELECT ARRAY_AGG(tpersona), ARRAY_LENGTH(ARRAY_AGG(tpersona), 1) INTO array_tpersona_v, indice_final_tpersona_v FROM mirror.nm_fijos_tpercardep WHERE ccedula = captura_ccedula_v; 
			FOR indice_tpersona_v IN 1.. indice_final_tpersona_v LOOP 
				RAISE NOTICE 'LOOP: %', indice_tpersona_v;
				SELECT count(1) INTO existencia_tpersona_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_tpersona_v[indice_tpersona_v];	
				IF (existencia_tpersona_v = 1) THEN
				RAISE NOTICE 'existencia_tpersona_v = 1';
--				Siempre Le da prioridad a Docentes por lo que si pasa por alguna condicion de nivel superior. ignora las inferiores 
--					Nivel: 3
					IF(array_tpersona_v[indice_tpersona_v] = 'D' ) THEN
						RAISE NOTICE 'array_tpersona_v[indice_tpersona_v] = "D"';
						SELECT id INTO captura_cobertura_id_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_tpersona_v[indice_tpersona_v];
						SELECT array_tpersona_v[indice_tpersona_v] INTO estado_cobertura_v;

	--				Nivel: 2
					ELSEIF (array_tpersona_v[indice_tpersona_v] =  'O' AND estado_cobertura_v != 'D') THEN
						RAISE NOTICE 'array_tpersona_v[indice_tpersona_v] = "O" AND estado_cobertura_v != "D"';
						SELECT id INTO captura_cobertura_id_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_tpersona_v[indice_tpersona_v];
						SELECT array_tpersona_v[indice_tpersona_v] INTO estado_cobertura_v;

	--				Nivel: 1
					ELSEIF (array_tpersona_v[indice_tpersona_v] =  'A' AND estado_cobertura_v != 'D' AND estado_cobertura_v != 'O' ) THEN
						RAISE NOTICE 'array_tpersona_v[indice_tpersona_v] = "A" AND estado_cobertura_v != "D" AND estado_cobertura_v != "O"';
						SELECT id INTO captura_cobertura_id_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = array_tpersona_v[indice_tpersona_v];
						SELECT array_tpersona_v[indice_tpersona_v] INTO estado_cobertura_v;
					END IF;

				END IF;		
				RAISE NOTICE 'END LOOP: %', indice_tpersona_v;
			END LOOP;
			RAISE NOTICE 'END LOOP'; 

SELECT  (clock_timestamp() - tiempo_inicio ) INTO tiempo;
RAISE NOTICE 'tiempo transcurrido: %',tiempo;
--(----------------------------------------------------------------------------------------------------------------------------------------------------
--				Recibe: 
/*
			captura_ccedula_v INT := null;

			captura_primer_nombre_v VARCHAR;
			captura_segundo_nombre_v VARCHAR;
			captura_primer_apellido_v VARCHAR;
			captura_segundo_apellido_v VARCHAR;
*/

			


			SELECT COUNT(1) INTO existencia_cedula_saime_v FROM mirror.saime WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v;
			IF (existencia_cedula_saime_v = 1) THEN 
				RAISE NOTICE 'existencia_cedula_saime_v = 1';

				SELECT 'Si' INTO verificado_saime_v;

				SELECT 
					primer_nombre, 
					segundo_nombre, 
					primer_apellido, 
					segundo_apellido,
					fecha_nacimiento,
					sexo
				INTO 
					captura_primer_nombre_v, 
					captura_segundo_nombre_v, 
					captura_primer_apellido_v, 
					captura_segundo_apellido_v,
					captura_fecha_nacimiento_saime_v,
					captura_sexo_saime_v

				FROM mirror.saime WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v;

				SELECT 
					captura_primer_nombre_v||' '||captura_segundo_nombre_v, captura_primer_apellido_v||' '||captura_segundo_apellido_v 
				INTO 
					nombre_v, apellido_v;

				IF(NEW.fnacimiento is null) THEN
					RAISE NOTICE 'NEW.fnacimiento is null';
					IF (captura_fecha_nacimiento_saime_v = '0001-01-01') THEN
						RAISE NOTICE 'captura_fecha_nacimiento_saime_v = "0001-01-01"';
						SELECT '1800-01-01' INTO captura_fecha_nacimiento_v;
						SELECT 1 INTO fecha_nacimiento_errada_v;
					ELSE
						captura_fecha_nacimiento_v = captura_fecha_nacimiento_saime_v;
					END IF;
				ELSE
					captura_fecha_nacimiento_v = NEW.fnacimiento;
				END IF;

			ELSE
				RAISE NOTICE 'existencia_cedula_saime_v = 0';
				SELECT 'No' INTO verificado_saime_v;
				SELECT NEW.dnombre, NEW.dnombre INTO nombre_v, apellido_v;

				IF(NEW.fnacimiento is null) THEN
					RAISE NOTICE 'NEW.fnacimiento is null';
					SELECT '1800-01-01' INTO captura_fecha_nacimiento_v;
					SELECT 1 INTO fecha_nacimiento_errada_v;
				ELSE
					captura_fecha_nacimiento_v = NEW.fnacimiento;
				END IF;
			END IF;

			IF (NEW.tsexo != 'F' AND NEW.tsexo != 'M') THEN
				RAISE NOTICE 'NEW.tsexo != "F" OR NEW.tsexo != "M"';
				SELECT captura_sexo_saime_v INTO captura_sexo_v;
			ELSE
				SELECT NEW.tsexo INTO captura_sexo_v;
			END IF;

--			Remplazar ? por las ñ en el nombre
			IF nombre_v ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]' THEN
				SELECT 
					regexp_replace(nombre_v, '\?', 'ñ')
				INTO 
					nombre_v
				WHERE
					nombre_v ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]';
			END IF;

--			Remplazar ? por las ñ en el apellido
			IF apellido_v ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]' THEN
				SELECT 
					regexp_replace(apellido_v, '\?', 'ñ')  
				INTO 
					apellido_v
				WHERE
					apellido_v  ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]';
			END IF;

--				Retorna: 
/*
			verificado_saime_v VARCHAR;
			nombre_v VARCHAR := '';
			apellido_v VARCHAR := '';
*/
--)----------------------------------------------------------------------------------------------------------------------------------------------------

SELECT  (clock_timestamp() - tiempo_inicio ) INTO tiempo;
RAISE NOTICE 'tiempo transcurrido: %',tiempo;

--(----------------------------------------------------------------------------------------------------------------------------------------------------
			SELECT COUNT(1) INTO existencia_talento_humano_v FROM gestion_humana.talento_humano WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v;
			IF (existencia_talento_humano_v = 0) THEN
				RAISE NOTICE 'existencia_talento_humano_v = 0';
				INSERT INTO gestion_humana.talento_humano 
				(
					origen,
					cedula,
					nombre,
					apellido,
					sexo,
					fecha_nacimiento,
					estado_id,
					municipio_id,
					fecha_ingreso,
					condicion_actual_id, 
					categoria_cargo_actual_id,
					cargo_actual_id,
					tipo_nomina_id,
					dependencia_id,
					tipo_personal,
					condicion,
					cargo,
					zona_educativa,
					dependencia,
					verificado_saime,
					tipo_cuenta_id,
					numero_cuenta,
					usuario_ini_id,
					usuario_act_id,
					fecha_ini,
					fecha_act,
					estatus,
					cobertura_id,
					--fecha_nacimiento_errada,
					estado_civil,
					estado_dependencia_id,
					estado_dependencia

				) VALUES (
					NEW.tnacionalidad, --origen
					NEW.ccedula, --cedula
					nombre_v, --nombre
					apellido_v, --apellido
					captura_sexo_v, --sexo
					captura_fecha_nacimiento_v, --fecha_nacimiento,
					captura_id_estado_v, --estado_id,
					captura_id_municipio_v, --municipio_id,
					fecha_ingreso_v, --fecha_ingreso,
					condicion_actual_id_v, --condicion_actual_id, 
					captura_categoria_cargo_actual_id_v, --categoria_cargo_actual_id,
					captura_cargo_nominal_id_v, --cargo_actual_id,
					tipo_nomina_id_v, --tipo_nomina_id,
					captura_dependencia_id_v, --dependencia_id,
					captura_tipo_personal_v, --tipo_personal,
					condicion_v, --condicion,
					captura_cargo_v, --cargo,
					captura_zona_educativa_v, --zona_educativa,
					captura_dependencia_v, --dependencia,
					verificado_saime_v, --verificado_saime,
					captura_tipo_cuenta_id_v, --tipo_cuenta_id,
					captura_cuenta_banco_v, --numero_cuenta,
					usuario_id_v, --usuario_ini_id,
					usuario_id_v, --usuario_act_id,
					fecha_actual_v, --fecha_ini,
					fecha_actual_v, --fecha_act,
					captura_istatus_v, --estatus,
					captura_cobertura_id_v, --cobertura_id
					--fecha_nacimiento_errada_v,
					NEW.testado_civil,
					captura_estado_dependencia_id_v,
					captura_nombre_dependencia_estado_v
				) RETURNING id INTO captura_id_talento_humano_v;
			ELSE
				RAISE NOTICE 'existencia_talento_humano_v = 1';
				UPDATE gestion_humana.talento_humano  
				SET 
					origen = NEW.tnacionalidad, 
					cedula = NEW.ccedula, 
					nombre = nombre_v, 
					apellido = apellido_v,
					sexo = captura_sexo_v,
					fecha_nacimiento = captura_fecha_nacimiento_v,
					municipio_id = captura_id_municipio_v,
					fecha_ingreso = fecha_ingreso_v,
					condicion_actual_id = condicion_actual_id_v, 
					categoria_cargo_actual_id = captura_categoria_cargo_actual_id_v,
					cargo_actual_id = captura_cargo_nominal_id_v,
					tipo_nomina_id = tipo_nomina_id_v,
					dependencia_id = captura_dependencia_id_v,
					tipo_personal = captura_tipo_personal_v,
					condicion = condicion_v,
					cargo = captura_cargo_v,
					zona_educativa = captura_zona_educativa_v,
					dependencia = captura_dependencia_v,
					verificado_saime = verificado_saime_v,
					tipo_cuenta_id = captura_tipo_cuenta_id_v,
					numero_cuenta = captura_cuenta_banco_v,
					fecha_ini = fecha_actual_v,
					fecha_act_nomina_fija = fecha_actual_v,
					estatus = captura_istatus_v,
					cobertura_id = captura_cobertura_id_v,
					--fecha_nacimiento_errada = fecha_nacimiento_errada_v,
					estado_civil = NEW.testado_civil,
					estado_dependencia_id = captura_estado_dependencia_id_v,
					estado_dependencia = captura_nombre_dependencia_estado_v
				 WHERE cedula = captura_ccedula_v AND origen = captura_tnacionalidad_v RETURNING id INTO captura_id_talento_humano_v;
			END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------
		ELSE
			RAISE EXCEPTION 'No se pudo insertar la cedula: %-% en %.%, no existe en mirror.nm_fijos_tpercardep.',captura_tnacionalidad_v ,captura_ccedula_v, TG_TABLE_SCHEMA, TG_TABLE_NAME;
		END IF;
	END IF; 
	IF captura_id_talento_humano_v is not null THEN 
    	SELECT 1 INTO NEW.estado_carga;
	END IF;

	SELECT COUNT(1) INTO existencia_experiencia_laboral_mppe_v FROM gestion_humana.experiencia_laboral WHERE institucion = 'MPPE' AND talento_humano_id = captura_id_talento_humano_v ;
	IF existencia_experiencia_laboral_mppe_v = 0 THEN
		IF captura_istatus_v = 'A' THEN
			INSERT INTO
				gestion_humana.experiencia_laboral (
					talento_humano_id,
					institucion,
					publica,
					cargo_desempenhado,
					actividades_realizadas,
					activo_actualmente,
					fecha_ingreso,
					condicion_nominal_id,
					fecha_ini,
					fecha_act,
					usuario_ini_id,
					usuario_act_id
				) VALUES (
					captura_id_talento_humano_v,
					const_institucion_mppe_v,
					const_es_institucion_publica_v,
					captura_nombre_cargo_v,
					captura_nombre_cargo_v,
					const_actualmente_activo_v,
					fecha_ingreso_v,
					conts_condicion_nominal_id_v,
					const_fecha_actual_v,
					const_fecha_actual_v,
					const_usuario_ini_id_v,
					const_usuario_ini_id_v
				) RETURNING id INTO captura_id_experiencia_laboral_v;
			IF captura_id_experiencia_laboral_v is not null THEN
				 NEW.estado_carga_cargo_mppe := const_experiencia_laboral_mppe_registrada_v;
			END IF;
		END IF;
	ELSE
		NEW.estado_carga_cargo_mppe := const_experiencia_laboral_mppe_registrada_v;
	END IF;

	SELECT  (clock_timestamp() - tiempo_inicio ) INTO tiempo;
	RAISE NOTICE 'tiempo transcurrido: %',tiempo;
	PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, estatus_seccion_v);
	RETURN NEW;

EXCEPTION
    WHEN OTHERS THEN

		estatus_seccion_v := 'EXCEPTION';
		observaciones_v := 'Cedula: '||NEW.ccedula||' ('||SQLERRM||')';
		PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, estatus_seccion_v);
		RAISE NOTICE '%', SQLERRM;
    RETURN NEW;


END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


/*
CREATE TRIGGER mirror_bus_servicios_tpersona_tpercardep
BEFORE INSERT OR UPDATE
ON mirror.nm_fijos_tpersona
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_tpersona_tpercardep();
*/