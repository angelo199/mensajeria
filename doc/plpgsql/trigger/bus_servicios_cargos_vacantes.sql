CREATE OR REPLACE FUNCTION mirror.bus_servicios_cargos_vacantes()
  RETURNS trigger AS
$BODY$
DECLARE

--	Variables de EXCEPTION
	descripcion_v TEXT := 'Bus de servicio con CSV de Nómina que permite la transferencia de los datos entre el archivo CSV generado desde la base de datos de nomina y mirror.rac_cargos_vacantes de app_concurso_merito';
	nombre_ktr_v VARCHAR := 'TRANS_CARGOS_VACANTES';
	nombre_kjb_v VARCHAR := 'JOB_MIRROR_APP_CONCURSO_MERITO';
	observaciones_v TEXT := null;
	seccion_v TEXT := 'EXITO';
	tipo_dependencia_v INT := 0;
	const_fuente_v VARCHAR := 'RAC';

-- Variables 
	disponibilidad_v VARCHAR := NEW.apelli01; 
	descripcion_cargo_v VARCHAR := NEW.dcargo08;
	fecha_actual_v TIMESTAMP WITH TIME ZONE := NOW();
	usuario_id_v INT := 2;
	codnomin_v INT := NEW.codnomin;
	coddenpen_v INT := NEW.coddenpen;
	nomdenpen_v VARCHAR := NEW.nomdenpen;
	codestad_v SMALLINT := NEW.codestad;
	nomestad_v VARCHAR := NEW.nomestad;
	codclase_v INT := NEW.codclase;
	gradoesc_v VARCHAR := NEW.gradoesc;
	dencargo_v VARCHAR := NEW.dencargo;
	fecha_act_nomina_v TIMESTAMP WITH TIME ZONE := NOW();

-- Variables de captura
	captura_dependencia_id_v INT := null;
	captura_estado_id_v INT := null;
	captura_clase_cargo_id_v INT := null;
	captura_coddenpen_v VARCHAR := null;
	captura_codclase_v VARCHAR := null;
	captura_disponibilidad_v VARCHAR := null;
	captura_nombre_estado_v VARCHAR := null;
	captura_observacion_v VARCHAR := null;
	captura_tipo_dependencia_id_v INT := null; 


--	Variables de existencia
	existencia_dependencia_v INT := 0;
	existencia_clase_cargo_v INT := 0;
	existencia_cargo_vacante_v INT := 0;
	existencia_rac_cargo_vacante_v INT := 0;
	existencia_estado_v INT := 0;
	existencia_codigo_nomina_estado_v INT := 0;

BEGIN
	RAISE NOTICE '1';
	IF (disponibilidad_v = 'VACANTE') THEN
		captura_disponibilidad_v := 'S';
	ELSE 
		captura_disponibilidad_v := 'N';
	END IF;

	SELECT TRIM(to_char(coddenpen_v::INT, '000000000')) INTO captura_coddenpen_v;
	SELECT TRIM(to_char(codclase_v::INT, '00000')) INTO captura_codclase_v;


	SELECT COUNT(1) INTO existencia_dependencia_v FROM gestion_humana.dependencia WHERE codigo = captura_coddenpen_v;
	IF (existencia_dependencia_v = 1) THEN
			RAISE NOTICE '2';
		SELECT estado_id, id, tipo_dependencia_id INTO captura_estado_id_v ,captura_dependencia_id_v, captura_tipo_dependencia_id_v FROM gestion_humana.dependencia WHERE codigo = captura_coddenpen_v;
	ELSE
		 SELECT 'Campo dependencia_id no encontrado, el codigo "'||COALESCE(coddenpen_v::VARCHAR, '')||'" del campo coddenpen_v no existe en la tabla gestion_humana.dependencia.' INTO captura_observacion_v;
	END IF;
	SELECT COUNT(1) INTO existencia_clase_cargo_v FROM gestion_humana.clase_cargo WHERE codigo = captura_codclase_v;
	IF (existencia_clase_cargo_v = 1) THEN 
			RAISE NOTICE '3';
		SELECT id INTO captura_clase_cargo_id_v FROM gestion_humana.clase_cargo WHERE codigo = captura_codclase_v;
	ELSE
		RAISE NOTICE '4';
		IF (captura_observacion_v IS NOT NULL) THEN
				RAISE NOTICE '5';
			captura_observacion_v := captura_observacion_v||'||';
		END IF;
		 SELECT COALESCE(captura_observacion_v, '')||'Campo clase_cargo_id no encontrado, el codigo "'||COALESCE(codclase_v::VARCHAR, '')||'" del campo codclase_v no existe en la tabla gestion_humana.clase_cargo.' INTO captura_observacion_v;
	END IF;

	SELECT COUNT (1) INTO existencia_estado_v FROM catastro.estado WHERE id = captura_estado_id_v;
	IF (existencia_estado_v = 1) THEN 
			RAISE NOTICE '6';
		SELECT nombre INTO captura_nombre_estado_v FROM catastro.estado WHERE id = captura_estado_id_v;
	ELSE
		SELECT COUNT (1) INTO existencia_estado_v FROM catastro.estado WHERE nombre = UPPER(nomestad_v);
		IF (existencia_estado_v = 1) THEN
			SELECT id, nombre INTO captura_estado_id_v, captura_nombre_estado_v FROM catastro.estado WHERE nombre = UPPER(nomestad_v);
		ELSE
			IF (captura_observacion_v IS NOT NULL) THEN
			RAISE NOTICE '8';
				captura_observacion_v := captura_observacion_v||'||';
			END IF;
			SELECT COALESCE(captura_observacion_v, '')||'Campo estado_id no encontrado, el estado "'||COALESCE(nomestad_v::VARCHAR, '')||'" no existe en la tabla catastro.estado.' INTO captura_observacion_v;
		END IF;
		RAISE NOTICE '7';
	END IF;
	IF existencia_dependencia_v = 0 THEN
		SELECT COUNT(1) INTO existencia_codigo_nomina_estado_v FROM catastro.estado WHERE codigo_nomina::SMALLINT = NEW.codestad;
		IF existencia_codigo_nomina_estado_v > 0 THEN
			SELECT id INTO captura_estado_id_v FROM catastro.estado WHERE codigo_nomina::SMALLINT = NEW.codestad;
			INSERT INTO gestion_humana.dependencia (
		 	codigo,
		 	nombre,
		 	estado_id,
		 	tipo_dependencia_id,
		 	usuario_ini_id,
		 	usuario_act_id,
		 	fuente_ini,
		 	fuente_act
	 	) VALUES (
			captura_coddenpen_v,
			captura_codclase_v||' Dependencia No Encontrada',
			captura_estado_id_v,
			tipo_dependencia_v,
			usuario_id_v,
			usuario_id_v,
			const_fuente_v,
			const_fuente_v
	 	);
		END IF;
	END IF;

		RAISE NOTICE '9';
	SELECT COUNT(1) INTO existencia_cargo_vacante_v FROM gestion_humana.cargo_vacante WHERE codnomin = codnomin_v;
	IF (existencia_cargo_vacante_v = 0) THEN
			RAISE NOTICE '10';
		INSERT INTO gestion_humana.cargo_vacante (
			estado_id,
			dependencia_id,
			clase_cargo_id,
			disponibilidad,
			descripcion,
			fecha_ini,
			fecha_act,
			usuario_ini_id,
			usuario_act_id,
			codnomin,
			coddenpen,
			nomdenpen,
			codestad,
			nomestad,
			codclase,
			gradoesc,
			dencargo,
			fecha_act_nomina,
			observacion,
			tipo_dependencia_id
		) VALUES (
			captura_estado_id_v,
			captura_dependencia_id_v,
			captura_clase_cargo_id_v,
			captura_disponibilidad_v,
			descripcion_cargo_v,
			fecha_actual_v,
			fecha_actual_v,
			usuario_id_v,
			usuario_id_v,
			codnomin_v,
			coddenpen_v,
			nomdenpen_v,
			codestad_v,
			captura_nombre_estado_v,
			codclase_v,
			gradoesc_v,
			dencargo_v,
			fecha_act_nomina_v,
			captura_observacion_v,
			captura_tipo_dependencia_id_v
		);

	ELSE
			RAISE NOTICE '11';
		UPDATE gestion_humana.cargo_vacante SET 
			estado_id = captura_estado_id_v,
			dependencia_id = captura_dependencia_id_v,
			clase_cargo_id = captura_clase_cargo_id_v,
			disponibilidad = captura_disponibilidad_v,
			descripcion = descripcion_cargo_v,
			codnomin = codnomin_v,
			coddenpen = coddenpen_v,
			nomdenpen = nomdenpen_v,
			codestad = codestad_v,
			codclase = codclase_v,
			gradoesc = gradoesc_v,
			dencargo = dencargo_v,
			nomestad = captura_nombre_estado_v,
			fecha_act_nomina = fecha_act_nomina_v,
			observacion = captura_observacion_v,
			tipo_dependencia_id = captura_tipo_dependencia_id_v
		WHERE codnomin = codnomin_v;
	END IF;

	IF (captura_observacion_v is NULL) THEN
			RAISE NOTICE '12';
			NEW.estado_carga := 1;
	END IF;

	UPDATE gestion_humana.cargo_vacante SET estatus = 'A' WHERE codnomin = codnomin_v;

	PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);

	RETURN NEW;

EXCEPTION
    WHEN OTHERS THEN
    	RAISE NOTICE '13';

		seccion_v := 'EXCEPTION';
		observaciones_v := 'Codigo de Nomina: '||NEW.codnomin||' ('||SQLERRM||')';
		PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);


    RETURN NEW;

END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


/*
CREATE TRIGGER mirror_bus_servicios_cargos_vacantes
BEFORE INSERT
ON mirror.rac_cargos_vacantes
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_cargos_vacantes();
*/