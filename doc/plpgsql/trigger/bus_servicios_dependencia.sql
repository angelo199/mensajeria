CREATE OR REPLACE FUNCTION mirror.bus_servicios_dependencia()
  RETURNS trigger AS
$BODY$
DECLARE
--Variables 
cdependencia_v varchar;
fecha_actual_v TIMESTAMP WITH TIME ZONE := NOW();
cusu_modi_v varchar;

--Varibales de existencia
  existencia_dependencia_v INT := 0;
  existencia_usuario_nomina_v INT := 0;
  existencia_codigo_nomina INT := 0;
  existencia_co_munc_asap INT := 0;

--Variables de EXCEPTION
  descripcion_v TEXT := 'Bus de servicio con Nómina que permite la transferencia de los datos entre la tabla dbcontrato.tdependencia y dbfija.tdependencia de la base de datos de nomina con las tablas mirror.nm_contratados_tdependencia y mirror.nm_fijos_tdependencia de app_seguros';
  nombre_ktr_v VARCHAR := null;
  nombre_kjb_v VARCHAR := 'JOB_MIRROR_APP_CONCURSO_MERITO';
  observaciones_v TEXT := null;
  seccion_v TEXT := 'EXITO';  

 
--Variables de captura
  captura_id_usuario_v INT := 1;
  captura_id_tipo_dependencia_v INT;
  captura_id_estado_v INT;
  captura_id_municipio_v INT := null;
  captura_fuente_v VARCHAR := null;
  captura_id_dependencia_v INT := null;

BEGIN

--Ejecutar cuando se realiza un INSERT o un UPDATE.
  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  Seteando variables que se usaran para buscar los datos cuando sea un INSERT
    IF (TG_OP = 'INSERT') THEN
      cdependencia_v := NEW.cdependencia;
      cusu_modi_v := NEW.cusu_modi;
--  Seteando variables que se usaran para buscar los datos cuando sea un UPDATE
    ELSEIF (TG_OP = 'UPDATE') THEN
      cdependencia_v := OLD.cdependencia;
      cusu_modi_v := OLD.cusu_modi;
    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------


    IF (TG_TABLE_NAME = 'nm_contratados_tdependencia') THEN
      nombre_ktr_v := 'TRANS_CONTRATADOS_TDEPENDENCIA';
      captura_fuente_v := 'nomina_contratados';

    ELSEIF(TG_TABLE_NAME = 'nm_fijos_tdependencia') THEN
      nombre_ktr_v := 'TRANS_FIJOS_TDEPENDENCIA';
      captura_fuente_v := 'nomina_fijos';
    END IF;

--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  Valida que exista un usuario con el nombre de la variable cusu_modi_v
    SELECT COUNT(1) INTO existencia_usuario_nomina_v FROM seguridad.usergroups_user WHERE nombre = cusu_modi_v;
    IF existencia_usuario_nomina_v = 1 THEN
--    Captura el id del usuario
      SELECT id INTO captura_id_usuario_v FROM seguridad.usergroups_user WHERE nombre = cusu_modi_v;
    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
    /*  Regla para determinar el tipo_dependencia: 
    *    -Cuando iplanta = N y izona = N 
    *    ---Entonces tipo_dependencia = P
    *
    *    -Cuando iplanta = S y izona = N
    *    ---Entonces tipo_dependencia = C
    *
    *    -Cuando iplanta = N y izona = S
    *    ---Entonces tipo_dependencia = Z  
    */

    IF (NEW.iplanta = 'N' AND NEW.izona = 'N') THEN
      SELECT id INTO captura_id_tipo_dependencia_v FROM gestion_humana.tipo_dependencia WHERE siglas = 'P';
    ELSEIF (NEW.iplanta = 'S' AND NEW.izona = 'N') THEN
      SELECT id INTO captura_id_tipo_dependencia_v FROM gestion_humana.tipo_dependencia WHERE siglas = 'C';
    ELSEIF (NEW.iplanta = 'N' AND NEW.izona = 'S') THEN
      SELECT id INTO captura_id_tipo_dependencia_v FROM gestion_humana.tipo_dependencia WHERE siglas = 'Z';
    END IF;

--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  Determinar el id del estado y el municipio segun el codigo_ini
--  obtener el id del estado
    SELECT count(1) INTO existencia_codigo_nomina FROM catastro.estado WHERE codigo_nomina = NEW.centidad;
    IF (existencia_codigo_nomina = 1) THEN 
      SELECT id INTO captura_id_estado_v FROM catastro.estado WHERE codigo_nomina = NEW.centidad;
    END IF;


/*
--  obtener el id del municipio
     SELECT count(1) INTO existencia_co_munc_asap FROM catastro.municipio WHERE co_munc_asap = NEW.cmunicipio;
    IF (existencia_co_munc_asap = 1) THEN 
      SELECT id INTO captura_id_municipio_v FROM catastro.municipio WHERE co_munc_asap = NEW.cmunicipio;
    END IF;
*/
--)----------------------------------------------------------------------------------------------------------------------------------------------------



--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  Valida si el registro de la dependencia del mirror ya se encuentra en la tabla de gestion_humana.dependencia
    SELECT COUNT(1) INTO existencia_dependencia_v FROM gestion_humana.dependencia WHERE codigo = cdependencia_v;
    IF existencia_dependencia_v = 0 THEN
--    Registra la dependencia en gestion_humana.dependencia
      INSERT INTO gestion_humana.dependencia (codigo, nombre, estado_id, municipio_id, tipo_dependencia_id, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, fuente_ini, fuente_act) VALUES (NEW.cdependencia, NEW.cdependencia||'-'||NEW.ddependencia, captura_id_estado_v, captura_id_municipio_v, captura_id_tipo_dependencia_v, captura_id_usuario_v, captura_id_usuario_v, fecha_actual_v, fecha_actual_v, captura_fuente_v, captura_fuente_v) RETURNING id INTO captura_id_dependencia_v;
    ELSE 
--    Actualiza la dependencia en gestion_humana.dependencia
      UPDATE gestion_humana.dependencia SET codigo = NEW.cdependencia, nombre = NEW.cdependencia||'-'||NEW.ddependencia, estado_id = captura_id_estado_v, municipio_id = captura_id_municipio_v, tipo_dependencia_id = captura_id_tipo_dependencia_v, usuario_act_id = captura_id_usuario_v, usuario_ini_id = captura_id_usuario_v, fuente_act = captura_fuente_v WHERE codigo = cdependencia_v RETURNING id INTO captura_id_dependencia_v;
    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------
  END IF;
  IF captura_id_dependencia_v is not null THEN 
      SELECT 1 INTO NEW.estado_carga;
  END IF;
  PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);
  RETURN NEW;
EXCEPTION
    WHEN OTHERS THEN

    IF (TG_TABLE_NAME = 'nm_contratados_tdependencia') THEN
      nombre_ktr_v := 'TRANS_CONTRATADOS_TDEPENDENCIA';
    ELSEIF(TG_TABLE_NAME = 'nm_fijos_tdependencia') THEN
      nombre_ktr_v := 'TRANS_FIJOS_TDEPENDENCIA';
    END IF;

    seccion_v := 'EXCEPTION';
    observaciones_v := 'Codigo: '||NEW.cdependencia||' ('||SQLERRM||')';
    PERFORM etls.captura_log_estado(descripcion_v, nombre_ktr_v, nombre_kjb_v, observaciones_v, seccion_v);

    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;




/*

CREATE TRIGGER mirror_bus_servicios_contratados_dependencia
BEFORE INSERT OR UPDATE
ON mirror.nm_contratados_tdependencia
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_dependencia();

CREATE TRIGGER mirror_bus_servicios_fijos_dependencia
BEFORE INSERT OR UPDATE
ON mirror.nm_fijos_tdependencia
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_dependencia();

*/