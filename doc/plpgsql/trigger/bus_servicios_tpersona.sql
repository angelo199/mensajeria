--	Function: mirror.bus_servicios_tpersona()

--	DROP FUNCTION mirror.bus_servicios_tpersona();

CREATE OR REPLACE FUNCTION mirror.bus_servicios_contratados_tpersona()
  RETURNS trigger AS
  $BODY$
DECLARE
--	Variables
categoria_ingreso_v INT;
nombre_v VARCHAR;
apellido_v VARCHAR;
verificado_saime_v VARCHAR;
fecha_nacimiento_errada_v SMALLINT := 0;

--	Variables Array
nombre_array_v TEXT[];

--	Constantes
	const_institucion_mppe_v VARCHAR := 'MPPE';
	const_es_institucion_publica_v  VARCHAR := 'S';
	const_actualmente_activo_v VARCHAR := 'S';
	conts_condicion_nominal_id_v INT := 2;
	conts_condicion_v VARCHAR := 'Contratado';
	const_usuario_ini_id_v INT := 2;
	const_tipo_nomina_contratado_id_v INT := 2;
	const_fecha_actual_v TIMESTAMP WITH TIME ZONE := NOW();
	const_experiencia_laboral_mppe_registrada_v INT := 1;


--	Variables de EXCEPTION
descripcion_v TEXT := 'Bus de servicio con Nómina que permite la transferencia de los datos entre la tabla dbcontrato.tpersona de la base de datos de nomina y mirror.nm_contratados_tpersona de app_seguros';
nombre_ktr_v VARCHAR := 'TRANS_CONTRATADOS_TPERSONA';
nombre_kjb_v VARCHAR := 'JOB_MIRROR_APP_CONCURSO_MERITO';
observaciones_v TEXT := null;
seccion_v TEXT := 'EXITO';


--Variables indice para loops
cant_nombre_array_v INT;


--Variables de captura
captura_ccedula_v INT := null;
captura_origen_v VARCHAR := '';
captura_primer_nombre_v VARCHAR := ''; 
captura_segundo_nombre_v VARCHAR := ''; 
captura_primer_apellido_v VARCHAR := ''; 
captura_segundo_apellido_v VARCHAR := '';
captura_id_estado_v INT := null;
captura_categoria_cargo_actual_v VARCHAR := '';
captura_categoria_cargo_actual_id_v INT := null;
captura_cargo_nominal_id_v INT := null;
captura_dependencia_id_v INT := null;
captura_tipo_cuenta_id_v INT := null;
captura_tipo_personal_v VARCHAR := '';
captura_cargo_v VARCHAR := '';
captura_zona_educativa_v VARCHAR := '';
captura_dependencia_v VARCHAR := '';
captura_cobertura_id_v INT := null;
captura_fecha_nacimiento_v DATE;
captura_fecha_nacimiento_saime_v DATE;
captura_sexo_saime_v VARCHAR;
captura_sexo_v VARCHAR;
captura_estado_dependencia_id_v INT := null;
captura_nombre_estado_dependencia_v VARCHAR := null;
captura_id_talento_humano_v INT := null;
captura_mppe_fecha_ingreso_v DATE := null;
captura_nombre_cargo_v VARCHAR := null;
captura_id_experiencia_laboral_v BIGINT := null;
captura_istatus_v VARCHAR := null;


--Variables de existencia
existencia_cedula_v INT := 0;
existencia_cedula_saime_v INT := 0;
existencia_codigo_nomina_v INT := 0;
existencia_categoria_ingreso_v INT := 0;
existencia_cargo_nominal_v INT := 0;
existencia_dependencia_v INT :=0;
existencia_tipo_cuenta_id_v INT := 0;
existencia_tpersona_v INT := 0;
existencia_cdependencia_v INT := 0;
existencia_estado_id_v INT := 0;
existencia_experiencia_laboral_mppe_v INT := 0;
existencia_inactivo_nomina_v SMALLINT := 0;

BEGIN

SELECT COUNT(1) INTO existencia_inactivo_nomina_v FROM gestion_humana.talento_humano WHERE cedula = captura_ccedula_v AND origen = captura_origen_v  AND estatus_nomina = 'A' limit 1;
IF existencia_inactivo_nomina_v = 0 THEN
	conts_condicion_nominal_id_v = 0;
	conts_condicion_v := 'Aspirante';
END IF;

captura_istatus_v := NEW.istatus;
captura_mppe_fecha_ingreso_v := NEW.ffecha_efe;

--	Ejecutar cuando se realiza un INSERT o un UPDATE.
	IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  	Seteando variables que se usaran para buscar los datos cuando sea un INSERT
	    IF (TG_OP = 'INSERT') THEN
	    	captura_ccedula_v := NEW.ccedula;
	    	captura_origen_v := NEW.cnacionalidad;
--  	Seteando variables que se usaran para buscar los datos cuando sea un UPDATE
	    ELSEIF (TG_OP = 'UPDATE') THEN
	    	captura_ccedula_v := OLD.ccedula;
	    	captura_origen_v := OLD.cnacionalidad;
	    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------
	END IF;

--(----------------------------------------------------------------------------------------------------------------------------------------------------

	SELECT COUNT(1) INTO existencia_cdependencia_v FROM mirror.nm_contratados_tdependencia WHERE cdependencia = NEW.cdependencia limit 1;
	IF ( existencia_cdependencia_v = 1 ) THEN
		SELECT ddependencia INTO captura_zona_educativa_v FROM mirror.nm_contratados_tdependencia WHERE cdependencia = NEW.cdependencia AND izona = 'S' limit 1;
	END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT COUNT(1) INTO existencia_cedula_saime_v FROM mirror.saime WHERE cedula = captura_ccedula_v AND origen = captura_origen_v limit 1;
	IF (existencia_cedula_saime_v = 1) THEN 

		SELECT 'Si' INTO verificado_saime_v;

		SELECT 
			primer_nombre, 
			segundo_nombre, 
			primer_apellido, 
			segundo_apellido
		INTO 
			captura_primer_nombre_v, 
			captura_segundo_nombre_v, 
			captura_primer_apellido_v, 
			captura_segundo_apellido_v
		FROM mirror.saime WHERE cedula = captura_ccedula_v AND origen = captura_origen_v;

		SELECT 
			captura_primer_nombre_v||' '||captura_segundo_nombre_v, captura_primer_apellido_v||' '||captura_segundo_apellido_v 
		INTO 
			nombre_v, apellido_v;


		IF(NEW.ffecha_nac is null) THEN
			RAISE NOTICE 'NEW.fnacimiento is null';
			IF (captura_fecha_nacimiento_saime_v = '0001-01-01') THEN
				RAISE NOTICE 'captura_fecha_nacimiento_saime_v = "0001-01-01"';
				SELECT '1800-01-01' INTO captura_fecha_nacimiento_v;
				SELECT 1 INTO fecha_nacimiento_errada_v;
			ELSE
				captura_fecha_nacimiento_v = captura_fecha_nacimiento_saime_v;
			END IF;
		ELSE
			captura_fecha_nacimiento_v = NEW.ffecha_nac;
		END IF;

	ELSE

		SELECT 'No' INTO verificado_saime_v;
		IF(char_length(NEW.dnombre) > 50 ) THEN
			SELECT (STRING_TO_ARRAY(NEW.dnombre, ' ')) INTO nombre_array_v;
			SELECT array_length(nombre_array_v,1) INTO cant_nombre_array_v;
			SELECT nombre_array_v[1], nombre_array_v[cant_nombre_array_v] INTO nombre_v, apellido_v;
		ELSE
			SELECT NEW.dnombre, NEW.dnombre INTO nombre_v, apellido_v;
		END IF;

		IF(NEW.ffecha_nac is null) THEN
			RAISE NOTICE 'NEW.fnacimiento is null';
			SELECT '1800-01-01' INTO captura_fecha_nacimiento_v;
			SELECT 1 INTO fecha_nacimiento_errada_v;
		ELSE
			captura_fecha_nacimiento_v = NEW.ffecha_nac;
		END IF;

	END IF;
	
	IF (NEW.isexo != 'F' OR NEW.isexo != 'M') THEN
		SELECT captura_sexo_saime_v INTO captura_sexo_v;
	ELSE
		SELECT NEW.isexo INTO captura_sexo_v;
	END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------


	IF nombre_v ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]' THEN
		SELECT 
			regexp_replace(nombre_v, '\?', 'ñ')
		INTO 
			nombre_v
		WHERE
			nombre_v ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]';
	END IF;

--			Remplazar ? por las ñ en el apellido
	IF apellido_v ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]' THEN
		SELECT 
			regexp_replace(apellido_v, '\?', 'ñ')  
		INTO 
			apellido_v
		WHERE
			apellido_v  ~* '[^a-zA-Z0-9áÁéÉíÍóÓúÚñÑ ]';
	END IF;

--(----------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT COUNT(1) INTO existencia_dependencia_v FROM gestion_humana.dependencia WHERE codigo::INT = NEW.cdependencia::INT limit 1;
	IF (existencia_dependencia_v = 1) THEN
		SELECT id, codigo||' - '||nombre, estado_id INTO captura_dependencia_id_v, captura_dependencia_v, captura_estado_dependencia_id_v FROM gestion_humana.dependencia WHERE codigo::INT = NEW.cdependencia::INT limit 1;
	END IF;

--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  Determinar el id del estado segun el codigo_ini
--  obtener el id del estado
    

    SELECT COUNT(1) INTO existencia_estado_id_v FROM catastro.estado WHERE id = captura_estado_dependencia_id_v limit 1;
    IF (existencia_estado_id_v = 1) THEN 
  		SELECT  nombre INTO captura_nombre_estado_dependencia_v FROM catastro.estado WHERE id = captura_estado_dependencia_id_v limit 1;
    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
--  Determinar el id del estado segun el codigo_ini
--  obtener el id del estado
    SELECT COUNT(1) INTO existencia_codigo_nomina_v FROM catastro.estado WHERE codigo_nomina = NEW.centidad limit 1;
    IF (existencia_codigo_nomina_v = 1) THEN 
  		SELECT id INTO captura_id_estado_v FROM catastro.estado WHERE codigo_nomina = NEW.centidad limit 1;
    END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------


--(----------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT COUNT(1) INTO existencia_cargo_nominal_v FROM gestion_humana.cargo_nominal WHERE codigo = NEW.ccargo limit 1;
	IF (existencia_cargo_nominal_v = 1) THEN
		SELECT id, codigo||' - '||nombre, nombre INTO captura_cargo_nominal_id_v, captura_cargo_v, captura_nombre_cargo_v FROM gestion_humana.cargo_nominal WHERE codigo = NEW.ccargo limit 1;
	END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
  	SELECT count(1) INTO existencia_tpersona_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = NEW.tpersona limit 1;
	IF (existencia_tpersona_v = 1) THEN
--  	Captura el nombre del tipo de persona en categoria cargo nominal
    	SELECT id, id, nombre INTO captura_cobertura_id_v, captura_categoria_cargo_actual_id_v, captura_tipo_personal_v FROM gestion_humana.categoria_cargo_nominal WHERE upper(substring(nombre, 1, 1)) = NEW.tpersona limit 1;
  	END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT COUNT(1) INTO existencia_tipo_cuenta_id_v FROM administracion.tipo_cuenta WHERE upper(substring(nombre, 1, 1)) = NEW.ttipo_cta limit 1; 
	IF (existencia_tipo_cuenta_id_v = 1) THEN 
		SELECT id INTO captura_tipo_cuenta_id_v FROM administracion.tipo_cuenta WHERE upper(substring(nombre, 1, 1)) = NEW.ttipo_cta limit 1; 
	END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

--(----------------------------------------------------------------------------------------------------------------------------------------------------
	SELECT COUNT(1) INTO existencia_cedula_v FROM gestion_humana.talento_humano WHERE cedula = captura_ccedula_v AND origen = captura_origen_v limit 1;
	IF (existencia_cedula_v = 0) THEN

		INSERT INTO gestion_humana.talento_humano (
			origen,
			cedula,
			nombre,
			apellido,
			sexo,
			fecha_nacimiento,
			estado_id,
			fecha_ingreso,
			condicion_actual_id, 
			categoria_cargo_actual_id,
			cargo_actual_id,
			tipo_nomina_id,
			dependencia_id,
			tipo_personal,
			condicion,
			cargo,
			zona_educativa,
			dependencia,
			verificado_saime,
			tipo_cuenta_id,
			numero_cuenta,
			fecha_registro_egreso,
			usuario_ini_id,
			usuario_act_id,
			fecha_ini,
			fecha_act,
			estatus_nomina,
			cobertura_id,
			estado_dependencia_id,
			estado_dependencia
		) VALUES (
			NEW.cnacionalidad, --origen
			NEW.ccedula, -- cedula
			nombre_v, --nombre
			apellido_v, --apellido
			captura_sexo_v, --sexo
			captura_fecha_nacimiento_v, --fecha_nacimiento
			captura_id_estado_v, --estado_id
			NEW.ffecha_efe, --fecha_ingreso
			conts_condicion_nominal_id_v, --condicion_actual_id
			captura_categoria_cargo_actual_id_v, --categoria_cargo_actual_id
			captura_cargo_nominal_id_v, --cargo_actual_id
			const_tipo_nomina_contratado_id_v, --tipo_nomina_id
			captura_dependencia_id_v, --dependencia_id 
			captura_tipo_personal_v, --tipo_personal
			conts_condicion_v, --condicion
			captura_cargo_v, --cargo
			captura_zona_educativa_v, --zona_educativa
			captura_dependencia_v, --dependencia
			verificado_saime_v, --verificado_saime
			captura_tipo_cuenta_id_v, --tipo_cuenta_id
			NEW.ccta_banco, --numero_cuenta
			NEW.ffecha_egre, --fecha_registro_egreso
			const_usuario_ini_id_v, --usuario_ini_id
			const_usuario_ini_id_v, --usuario_act_id
			const_fecha_actual_v, --fecha_ini
			const_fecha_actual_v,  --fecha_act
			NEW.istatus, --estatus_nomina
			captura_cobertura_id_v, --cobertura_id,
			captura_estado_dependencia_id_v,
			captura_nombre_estado_dependencia_v
		) RETURNING id INTO captura_id_talento_humano_v;
	ELSE
		UPDATE gestion_humana.talento_humano SET 
			origen = NEW.cnacionalidad, 
			cedula = NEW.ccedula, 
			nombre = nombre_v, 
			apellido = apellido_v, 
			sexo = NEW.isexo, 
			fecha_nacimiento = captura_fecha_nacimiento_v, 
			fecha_ingreso = NEW.ffecha_efe, 
			condicion_actual_id = conts_condicion_nominal_id_v, 
			categoria_cargo_actual_id = captura_categoria_cargo_actual_id_v, 
			cargo_actual_id = captura_cargo_nominal_id_v, 
			tipo_nomina_id = const_tipo_nomina_contratado_id_v, 
			dependencia_id = captura_dependencia_id_v, 
			tipo_personal = captura_tipo_personal_v, 
			condicion = conts_condicion_v, 
			cargo = captura_cargo_v, 
			zona_educativa = captura_zona_educativa_v, 
			dependencia = captura_dependencia_v, 
			verificado_saime = verificado_saime_v, 
			tipo_cuenta_id = captura_tipo_cuenta_id_v, 
			numero_cuenta = NEW.ccta_banco, 
			fecha_registro_egreso = NEW.ffecha_egre, 
			estatus_nomina = NEW.istatus,
			fecha_act_nomina_contratado = const_fecha_actual_v,
			cobertura_id = captura_cobertura_id_v,
			estado_dependencia_id = captura_estado_dependencia_id_v,
			estado_dependencia = captura_nombre_estado_dependencia_v
		WHERE cedula = captura_ccedula_v AND origen = captura_origen_v RETURNING id INTO captura_id_talento_humano_v;
	END IF;	
	IF captura_id_talento_humano_v is not null THEN 
    	SELECT 1 INTO NEW.estado_carga;
	END IF;
--)----------------------------------------------------------------------------------------------------------------------------------------------------

	SELECT COUNT(1) INTO existencia_experiencia_laboral_mppe_v FROM gestion_humana.experiencia_laboral WHERE institucion = 'MPPE' AND talento_humano_id = captura_id_talento_humano_v ;
	IF existencia_experiencia_laboral_mppe_v = 0 THEN
		IF captura_istatus_v = 'A' THEN
			INSERT INTO
				gestion_humana.experiencia_laboral (
					talento_humano_id,
					institucion,
					publica,
					cargo_desempenhado,
					actividades_realizadas,
					activo_actualmente,
					fecha_ingreso,
					condicion_nominal_id,
					fecha_ini,
					fecha_act,
					usuario_ini_id,
					usuario_act_id
				) VALUES (
					captura_id_talento_humano_v,
					const_institucion_mppe_v,
					const_es_institucion_publica_v,
					captura_nombre_cargo_v,
					captura_nombre_cargo_v,
					const_actualmente_activo_v,
					captura_mppe_fecha_ingreso_v,
					conts_condicion_nominal_id_v,
					const_fecha_actual_v,
					const_fecha_actual_v,
					const_usuario_ini_id_v,
					const_usuario_ini_id_v
				) RETURNING id INTO captura_id_experiencia_laboral_v;
			IF captura_id_experiencia_laboral_v is not null THEN
				 NEW.estado_carga_cargo_mppe := const_experiencia_laboral_mppe_registrada_v;
			END IF;
		END IF;
	ELSE
		NEW.estado_carga_cargo_mppe := const_experiencia_laboral_mppe_registrada_v;
	END IF;

	PERFORM etls.captura_log_estado(descripcion_v::TEXT, nombre_ktr_v::VARCHAR, nombre_kjb_v::VARCHAR, observaciones_v::TEXT, seccion_v::TEXT);
	RETURN NEW;
EXCEPTION
	WHEN OTHERS THEN
	
		seccion_v := 'EXCEPTION';
		observaciones_v := 'Cedula: '||NEW.ccedula||' ('||SQLERRM||')';
		PERFORM etls.captura_log_estado(descripcion_v::TEXT, nombre_ktr_v::VARCHAR, nombre_kjb_v::VARCHAR, observaciones_v::TEXT, seccion_v::TEXT);
		

		RETURN NEW;
	END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

/*
CREATE TRIGGER mirror_bus_servicios_contratados_tpersona
BEFORE INSERT OR UPDATE
ON mirror.nm_contratados_tpersona
FOR EACH ROW
EXECUTE PROCEDURE mirror.bus_servicios_contratados_tpersona();
*/