--	Function: mirror.bus_servicios_tpersona()

--	DROP FUNCTION mirror.bus_servicios_tpersona();

CREATE OR REPLACE FUNCTION gestion_humana.actualizar_condicion_nominal()
  RETURNS trigger AS
  $BODY$
BEGIN

		UPDATE gestion_humana.experiencia_laboral SET condicion_nominal_id = NEW.condicion_actual_id WHERE talento_humano_id = NEW.id;
		UPDATE gestion_humana.curso_realizado SET condicion_nominal_id = NEW.condicion_actual_id WHERE talento_humano_id = NEW.id;
		UPDATE gestion_humana.estudio_realizado SET condicion_nominal_id = NEW.condicion_actual_id WHERE talento_humano_id = NEW.id;
		UPDATE gestion_humana.referencia_personal SET condicion_nominal_id = NEW.condicion_actual_id WHERE talento_humano_id = NEW.id;

	RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER gestion_humana_actualizar_condicion_nominal
BEFORE UPDATE
ON gestion_humana.talento_humano
FOR EACH ROW
EXECUTE PROCEDURE gestion_humana.actualizar_condicion_nominal();
