CREATE VIEW etls.lista_errores_log_etl AS
SELECT estado.id, estado.fecha_ejecucion, estado.nombre_kjb, estado.nombre_ktr, error.descripcion, estado.estatus FROM etls.captura_log_errores as error 
LEFT JOIN etls.proceso_etl_trigger as estado ON error.captura_log_estado_id = estado.id