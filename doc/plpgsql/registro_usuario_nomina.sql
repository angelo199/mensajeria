
--DROP FUNCTION seguridad.registro_usuario_externo(character varying,integer,character varying,character varying,character varying,character varying,character varying,integer,integer,character varying,character va----rying,integer,character varying,timestamp without time zone,integer,integer,timestamp without time zone,timestamp without time zone)

CREATE OR REPLACE FUNCTION seguridad.registro_externo_usuario(
    origen_vi varchar,
    cedula_vi integer,
    nombre_vi varchar,
    apellido_vi varchar,
    telefono_vi varchar,
    password_vi varchar,
    email_vi varchar,
    group_id_vi integer,
    estado_id_vi integer,
    ultimos_numeros_cuenta_vi varchar,
--  variables fuera del formulario
    username_vi varchar,
    status_vi int,
    activation_code_vi varchar,
    activation_time_vi TIMESTAMP,
    user_ini_id_vi int,
    user_act_id_vi int,
    date_ini_vi TIMESTAMP,
    date_act_vi TIMESTAMP,
    data_vi text
)

  RETURNS record AS
$BODY$
DECLARE

--  Variables:
--      Informacion del sistema:
        modulo_v varchar := 'modulo.UserGroupsRegistro.RegistroUsuario'; --log auditoria.traza
        ipaddress_v varchar := inet_client_addr(); --IP que captura postgresql

--      Variables constantes:
        site_v varchar := '/site';

--      Debug:
        seccion_v INT := 1; --Seccion del error

--      Variables de Existencia: 
        existencia_usuario SMALLINT := 0;
        existencia_usuario_cuenta SMALLINT := 0;
        existencia_usuario_titular SMALLINT := 0;
        existencia_usuario_nomina SMALLINT := 0;

--      Variables de Captura:
        captura_id_usuario_v int := 0;

--      Variables de Mensaje: 
        mensaje_v varchar := '';
        codigo_v varchar := '';
        estado_resultado_v varchar := '';

--      Variables de Resultado:
        resultado record;

BEGIN


    seccion_v := 2;

--  Validar si ya esta registrado el usuario
    SELECT count(1) INTO existencia_usuario FROM seguridad.usergroups_user WHERE cedula = cedula_vi AND origen = origen_vi;

    IF existencia_usuario = 1 THEN

        mensaje_v := 'El usuario con la cédula de identidad '||origen_vi||'-'||cedula_vi||' ya se encuentra registrado en el Sistema. Si ha olvidado su clave puede hacer click en el link <a onclick="show_box(\"signup-box\"); >¿Olvidaste tu clave?</a>.';
        codigo_v := 'ADB001';
        estado_resultado_v :='alert';

    ELSE
        seccion_v := 3;

--      Validar si el usuario se encuentra en la tabla titular y si se encuentra activo
        SELECT count(1) INTO existencia_usuario_titular FROM gestion_humana.titular WHERE cedula = cedula_vi AND origen = origen_vi AND estatus = 'A';
           
        IF existencia_usuario_titular = 0 THEN

--          El usuario no esta en la tabla gestion_humana.titular, se busco con el estatus A , origen y cedula del titular
            mensaje_v := 'Usted no esta Registrado en nuestra Base de Datos.';
            codigo_v := 'ADB002';
            estado_resultado_v :='alert';

        ELSE

            seccion_v := 4;

--          Validar si el usuario ingreso correctamente los ultimos cuatro digitos de la cuenta de nomina y a la vez valida en caso de que el usuario no es funcionario que los datos del campo de la cuenta nomina no contenga valores.          
            SELECT count(1) INTO existencia_usuario_cuenta FROM gestion_humana.titular WHERE (cedula = cedula_vi AND origen = origen_vi) AND( (substring(numero_cuenta from '....$') = ultimos_numeros_cuenta_vi) OR (group_id_vi = 9 AND ultimos_numeros_cuenta_vi is null) );

            IF existencia_usuario_cuenta = 0 THEN

--              los ultimos cuatro numeros de la cuenta nomina no coinciden con los registrados en la base de datos
                mensaje_v := 'Los ultimos cuatro numeros de la cuenta nomina no coinciden con los registrados en la base de datos, Verifique sus datos.';
                codigo_v := 'ADB003';
                estado_resultado_v :='alert';

            ELSE

                seccion_v := 6;

--              Insertar al usuario.
                INSERT INTO seguridad.usergroups_user (
--                  Variables del formulario
                    origen,
                    cedula,
                    nombre,
                    apellido,
                    telefono,
                    password,
                    email,
                    group_id,
                    estado_id,
--                  Variables fuera del formulario
                    username,
                    status,
                    activation_code,
                    activation_time,
                    user_ini_id,
                    user_act_id,
                    date_ini,
                    date_act,
                    creation_date,
--                  Variables estatiscas del psql
                    home
                ) VALUES (
--                  Variables del formulario
                    origen_vi::varchar,
                    cedula_vi::integer,
                    nombre_vi::varchar,
                    apellido_vi::varchar,
                    telefono_vi::varchar,
                    password_vi::varchar,
                    email_vi::varchar,
                    group_id_vi::integer,
                    estado_id_vi::integer,
--                  Variables fuera del formulario
                    username_vi::varchar,
                    status_vi::int,
                    activation_code_vi::varchar,
                    activation_time_vi::TIMESTAMP,
                    user_ini_id_vi::int,
                    user_act_id_vi::int,
                    date_ini_vi::TIMESTAMP,
                    date_act_vi::TIMESTAMP,
                    date_ini_vi::TIMESTAMP,                    
--                  Variables estatiscas del psql
                    site_v::varchar
                ) RETURNING id INTO captura_id_usuario_v;

                seccion_v := 7;
--                  Luego de registrar al usuario, actualiza el ser_ini_id al id del usuario
                UPDATE seguridad.usergroups_user SET user_ini_id = captura_id_usuario_v, user_act_id = captura_id_usuario_v WHERE id = captura_id_usuario_v;

                mensaje_v := 'Usuario Registrado Exitósamente. En unos instantes recibira un correo para continuar con la activacón a su cuenta, hasta tanto no se efectúe esta operación no podra acceder.';
                codigo_v := 'E00000';
                estado_resultado_v :='exito';


            END IF;
        END IF;
    END IF;

    SELECT codigo_v, estado_resultado_v, mensaje_v, captura_id_usuario_v INTO resultado;
--  Ingresa los resultados a la traza de la auditoria.
    INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (date_ini_vi, ipaddress_v, 'ESCRITURA', modulo_v, estado_resultado_v, 'SOLICITUD DE REGISTRO DE USUARIO EN SEGUROS: '||mensaje_v, captura_id_usuario_v, username_vi, data_vi);

    RETURN resultado;

EXCEPTION

    WHEN OTHERS THEN
        codigo_v := SQLSTATE||'';
        estado_resultado_v := 'error';
        mensaje_v := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||estado_resultado_v;
        RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (Seccion: %)', SQLERRM, SQLSTATE, seccion_v;
        

        SELECT codigo_v, estado_resultado_v, mensaje_v, seccion_v INTO resultado;
--  Ingresa los resultados a la traza de la auditoria.
        INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (date_ini_vi, ipaddress_v, 'ESCRITURA', modulo_v, estado_resultado_v, 'SOLICITUD DE REGISTRO DE USUARIO EN SEGUROS: '||mensaje_v, captura_id_usuario_v, username_vi, data_vi);

    RETURN resultado;

END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION seguridad.registro_externo_usuario(varchar, integer, varchar, varchar, varchar, varchar, varchar, integer, integer, varchar, varchar, int, varchar, TIMESTAMP, int, int, TIMESTAMP, TIMESTAMP, text)
  OWNER TO postgres;