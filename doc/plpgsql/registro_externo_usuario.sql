-- Function: seguridad.registro_externo_usuario(character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, integer, character varying, timestamp without time zone, bigint, bigint, timestamp without time zone, timestamp without time zone, text)

-- DROP FUNCTION seguridad.registro_externo_usuario(character varying, integer, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, integer, character varying, timestamp without time zone, bigint, bigint, timestamp without time zone, timestamp without time zone, text);

CREATE OR REPLACE FUNCTION seguridad.registro_externo_usuario(origen_vi character varying, cedula_vi integer, nombre_vi character varying, apellido_vi character varying, telefono_vi character varying, password_vi character varying, email_vi character varying, group_id_vi integer, estado_id_vi integer, ultimos_numeros_cuenta_vi character varying, username_vi character varying, status_vi integer, activation_code_vi character varying, activation_time_vi timestamp without time zone, user_ini_id_vi bigint, user_act_id_vi bigint, date_ini_vi timestamp without time zone, date_act_vi timestamp without time zone, data_vi text)
  RETURNS record AS
  $BODY$
  DECLARE

    --  Variables:
    --      Informacion del sistema:
    modulo_v VARCHAR := 'modulo.UserGroupsRegistro.RegistroUsuario'; --log auditoria.traza
    ipaddress_v VARCHAR := inet_client_addr(); --IP que captura postgresql

--      Variables constantes:
    site_v VARCHAR := '/site';

    --      Debug:
    seccion_v SMALLINT := 0; --Seccion del error

--      Variables de Existencia:
    existencia_usuario SMALLINT := 0;
    existencia_usuario_cuenta SMALLINT := 0;
    existencia_usuario_talento_humano SMALLINT := 0;
    existencia_usuario_nomina SMALLINT := 0;
    existencia_cedula_saime_v INT := 0;
    --              Validar si existe un un tipo de persona y si es externo validar que no este en talento humano
    existencia_tipo_persona_v INT := 0;
    existencia_estado_dependencia_id_v INT := 0;
    existencia_usuario_inactivo_v INT := 0;
    existencia_usuario_activo_v INT := 0;

    --      Variables de Captura:
    captura_id_usuario_v BIGINT := 0;
    captura_telefono_celular_v VARCHAR := '';
    captura_telefono_fijo_v VARCHAR := '';
    captura_fecha_nacimiento_v DATE := null;
    captura_verificado_saime_v VARCHAR := '';
    captura_talento_humano_id_v BIGINT := null;
    captura_correo_corporativo_v VARCHAR := null;
    captura_correo_personal_v VARCHAR := null;
    captura_estado_dependencia_v VARCHAR := null;
    captura_estado_dependencia_id_v INT := null;

    --      Variables de Mensaje:
    mensaje_v VARCHAR := '';
    codigo_v VARCHAR := '';
    estado_resultado_v VARCHAR := '';

    --      Variables de Resultado:
    resultado record;

  BEGIN

    seccion_v := 1;

    SELECT count(1) INTO existencia_estado_dependencia_id_v FROM catastro.estado WHERE id = estado_id_vi;
    IF existencia_estado_dependencia_id_v = 1 THEN
      SELECT nombre INTO captura_estado_dependencia_v FROM catastro.estado WHERE id = estado_id_vi;
    END IF;

    IF(substring(telefono_vi from '...') = '(02') THEN
      SELECT telefono_vi INTO captura_telefono_fijo_v;
    ELSEIF (substring(telefono_vi from '...') = '(04') THEN
      SELECT telefono_vi INTO captura_telefono_celular_v;
    END IF;

    IF email_vi ILIKE '%@me.gob.ve' THEN
      SELECT email_vi INTO captura_correo_corporativo_v;
    ELSE
      SELECT email_vi INTO captura_correo_personal_v;
    END IF;

    SELECT COUNT(1) INTO existencia_cedula_saime_v FROM mirror.saime WHERE cedula = cedula_vi AND origen = origen_vi;
    IF existencia_cedula_saime_v = 0 THEN
      mensaje_v := 'La cédula de identidad '||origen_vi||'-'||cedula_vi||' no se encuentra registrada en el SAIME, verifique que ingreso su cédula correctamente.';
      codigo_v := 'ADB000';
      estado_resultado_v :='alert';
    ELSEIF email_vi ILIKE '%@hotmail%' OR email_vi ILIKE '%@outlook%' THEN
      mensaje_v := 'No esta Permitido Realizar el Registro con Correo Hotmail, es Preferible que Utilice su Correo Institucional o en su Defecto un Correo Gmail.';
      codigo_v := 'ACE000';
      estado_resultado_v :='alert';
    ELSE

      SELECT 'Si' INTO captura_verificado_saime_v;

      seccion_v := 2;

      --      Validar si ya esta registrado el usuario
      SELECT count(1) INTO existencia_usuario FROM seguridad.usergroups_user WHERE cedula = cedula_vi AND origen = origen_vi;

      IF existencia_usuario = 1 THEN

        mensaje_v := 'El usuario con la cédula de identidad '||origen_vi||'-'||cedula_vi||' ya se encuentra registrado en el Sistema. Si ha olvidado su clave puede hacer click en el link <a onclick="show_box(\"signup-box\"); >¿Olvidaste tu clave?</a>.';
        codigo_v := 'ADB001';
        estado_resultado_v :='alert';

      ELSE
        seccion_v := 3;

        SELECT COUNT(1) INTO existencia_tipo_persona_v FROM gestion_humana.condicion_nominal cn
          LEFT JOIN seguridad.usergroups_group up ON up.groupname = ANY (STRING_TO_ARRAY(UPPER(cn.nombre), ' ')) --la condicion nominal es igual al grupo de usuario
          LEFT JOIN gestion_humana.talento_humano th ON th.condicion_actual_id = cn.id -- El Talento Humano posee la condicion nominal.
        WHERE up.id = group_id_vi AND th.origen = origen_vi AND th.cedula = cedula_vi; -- El Talento Humano pertenece al grupo seleccionado

        /*
        existencia_tipo_persona_v
        |tipo personal|grupo_id|Talento Humano|valor|
        ---------------------------------------------
        |   Externo   |   9    |       0      |  0  |
        |   Externo   |  !9    |       0      |  0  |
        ---------------------------------------------
        |   Inactivo  |   9    |       1      |  1  |
        |   Inactivo  |  !9    |       1      |  0  |
        ---------------------------------------------
        |   Fijo      |   9    |       1      |  0  |
        |   Fijo      |  !9    |       1      |  1  |
        ---------------------------------------------
        |   Contratado|   9    |       1      |  0  |
        |   Contratado|  !9    |       1      |  1  |
        ---------------------------------------------
         */

        SELECT count(1) INTO existencia_usuario_activo_v FROM gestion_humana.talento_humano WHERE cedula = cedula_vi AND origen = origen_vi AND estatus = 'A';
        SELECT count(1) INTO existencia_usuario_inactivo_v FROM gestion_humana.talento_humano WHERE cedula = cedula_vi AND origen = origen_vi AND estatus = 'I';

        --  Validar si el personal al ser externo se encuentra en talento humano
        IF group_id_vi = 9 AND (existencia_tipo_persona_v = 1 OR existencia_tipo_persona_v = 0) AND existencia_usuario_activo_v = 1 THEN
          mensaje_v := 'Usted se encuentra registrado en la base de datos, no es un personal externo del MPPE. ';
          codigo_v := 'ADB004';
          estado_resultado_v :='alert';
        ELSEIF existencia_tipo_persona_v = 0 AND group_id_vi != 9 THEN
          mensaje_v := 'El grupo de empleado seleccionado no coincide con el grupo de empleado al que pertenece en la base de datos, Verifique sus datos.';
          codigo_v := 'ADB005';
          estado_resultado_v :='alert';
        ELSE
          --              Validar si el usuario se encuentra en la tabla talento_humano.
          SELECT count(1) INTO existencia_usuario_talento_humano FROM gestion_humana.talento_humano WHERE cedula = cedula_vi AND origen = origen_vi;
          IF existencia_usuario_talento_humano = 0 THEN

            captura_estado_dependencia_id_v := estado_id_vi;

            SELECT fecha_nacimiento INTO captura_fecha_nacimiento_v FROM mirror.saime WHERE cedula = cedula_vi AND origen = origen_vi;

            --              Registra al Talento Humano como Aspirante en caso de que no se encuentre registrado en la tabla talento_humano.
            INSERT INTO
              gestion_humana.talento_humano(
                origen,
                cedula,
                nombre,
                apellido,
                fecha_nacimiento,
                telefono_fijo,
                telefono_celular,
                estado_id,
                verificado_saime,
                condicion_actual_id,
                condicion,
                usuario_ini_id,
                usuario_act_id,
                fecha_ini,
                fecha_act,
                email_corporativo,
                email_personal,
                estado_dependencia_id,
                estado_dependencia
              ) VALUES (
              origen_vi,
              cedula_vi,
              nombre_vi,
              apellido_vi,
              captura_fecha_nacimiento_v,
              captura_telefono_fijo_v,
              captura_telefono_celular_v,
              estado_id_vi,
              captura_verificado_saime_v,
              0,
              'Aspirante',
              1,
              1,
              date_ini_vi,
              date_act_vi,
              captura_correo_corporativo_v,
              captura_correo_personal_v,
              captura_estado_dependencia_id_v,
              captura_estado_dependencia_v
            ) RETURNING id INTO captura_talento_humano_id_v;

          ELSE

            SELECT id, estado_dependencia_id INTO captura_talento_humano_id_v, captura_estado_dependencia_id_v FROM gestion_humana.talento_humano WHERE cedula = cedula_vi AND origen = origen_vi;
            SELECT nombre INTO captura_estado_dependencia_v FROM catastro.estado WHERE id = captura_estado_dependencia_id_v;
            IF captura_estado_dependencia_id_v is null THEN
              UPDATE gestion_humana.talento_humano SET estado_dependencia_id = estado_id_vi, estado_dependencia = captura_estado_dependencia_v, estatus = 'A' WHERE cedula = cedula_vi AND origen = origen_vi;
            END IF;

          END IF;

          seccion_v := 4;

          --              Validar si el usuario ingreso correctamente los ultimos cuatro digitos de la cuenta de nomina y a la vez valida en caso de que el usuario no es funcionario que los datos del campo de la cuenta nomina no contenga valores.
          SELECT count(1) INTO existencia_usuario_cuenta FROM gestion_humana.talento_humano WHERE (cedula = cedula_vi AND origen = origen_vi) AND( (substring(numero_cuenta from '....$') = ultimos_numeros_cuenta_vi) OR (group_id_vi = 9 AND ultimos_numeros_cuenta_vi is null) );

          IF existencia_usuario_cuenta = 0 THEN

            --              los ultimos cuatro numeros de la cuenta nomina no coinciden con los registrados en la base de datos
            mensaje_v := 'Los ultimos cuatro numeros de la cuenta nomina no coinciden con los registrados en la base de datos, Verifique sus datos.';
            codigo_v := 'ADB003';
            estado_resultado_v :='alert';

          ELSE

            seccion_v := 6;

            --              Insertar al usuario.
            INSERT INTO seguridad.usergroups_user (
              --                  Variables del formulario
              origen,
              cedula,
              nombre,
              apellido,
              telefono,
              password,
              email,
              group_id,
              estado_id,
              answer,
              --                  Variables fuera del formulario
              username,
              status,
              activation_code,
              activation_time,
              user_ini_id,
              user_act_id,
              date_ini,
              date_act,
              creation_date,
              --                  Variables estatiscas del psql
              home
            ) VALUES (
              --                  Variables del formulario
              origen_vi::varchar,
              cedula_vi::integer,
              nombre_vi::varchar,
              apellido_vi::varchar,
              telefono_vi::varchar,
              password_vi::varchar,
              email_vi::varchar,
              group_id_vi::integer,
              estado_id_vi::integer,
              ultimos_numeros_cuenta_vi::varchar,
              --                  Variables fuera del formulario
              username_vi::varchar,
              status_vi::int,
              activation_code_vi::varchar,
              activation_time_vi::TIMESTAMP,
              user_ini_id_vi::bigint,
              user_act_id_vi::bigint,
              date_ini_vi::TIMESTAMP,
              date_act_vi::TIMESTAMP,
              date_ini_vi::TIMESTAMP,
              --                  Variables estatiscas del psql
              site_v::varchar
            ) RETURNING id INTO captura_id_usuario_v;

            seccion_v := 7;
            --                  Luego de registrar al usuario, actualiza el ser_ini_id al id del usuario
            UPDATE seguridad.usergroups_user SET user_ini_id = captura_id_usuario_v, user_act_id = captura_id_usuario_v WHERE id = captura_id_usuario_v;
            UPDATE gestion_humana.talento_humano SET user_id = captura_id_usuario_v, usuario_ini_id = captura_id_usuario_v, usuario_act_id = captura_id_usuario_v WHERE cedula = cedula_vi AND origen = origen_vi;

            --Registrar el telefono fijo en caso de que no posea alguno ya registrado
            UPDATE gestion_humana.talento_humano SET telefono_fijo = captura_telefono_fijo_v WHERE cedula = cedula_vi AND origen = origen_vi AND captura_telefono_fijo_v is not null AND (TRIM(telefono_fijo) is null OR char_length(TRIM(telefono_fijo)) < 11 );
            --Registrar el telefono celular en caso de que no posea alguno ya registrado
            UPDATE gestion_humana.talento_humano SET telefono_celular = captura_telefono_celular_v WHERE cedula = cedula_vi AND origen = origen_vi AND captura_telefono_celular_v is not null AND (TRIM(telefono_celular) is null OR char_length(TRIM(telefono_celular)) < 11 );
            --Registrar el correo corporativo en caso de que no posea alguno ya registrado
            UPDATE gestion_humana.talento_humano SET email_corporativo = captura_correo_corporativo_v WHERE cedula = cedula_vi AND origen = origen_vi AND captura_telefono_celular_v is not null AND (TRIM(email_corporativo) is null OR char_length(TRIM(email_corporativo)) > 3 );
            --Registrar el correo personal en caso de que no posea alguno ya registrado
            UPDATE gestion_humana.talento_humano SET email_personal = captura_correo_personal_v WHERE cedula = cedula_vi AND origen = origen_vi AND captura_correo_personal_v is not null AND (TRIM(email_personal) is null OR char_length(TRIM(email_personal)) > 3 );

            mensaje_v := 'Usuario Registrado Exitósamente. En unos instantes recibira un correo para continuar con la activacón de su cuenta, hasta tanto no se efectúe esta operación no podra acceder al Sistema con su usuario y Clave.';
            codigo_v := 'S00000';
            estado_resultado_v :='exito';
          END IF;
        END IF;
      END IF;
    END IF;

    SELECT codigo_v, estado_resultado_v, mensaje_v, captura_id_usuario_v, captura_talento_humano_id_v, seccion_v INTO resultado;
    --  Ingresa los resultados a la traza de la auditoria.
    INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (date_ini_vi, ipaddress_v, 'ESCRITURA', modulo_v, estado_resultado_v, 'SOLICITUD DE REGISTRO DE USUARIO EN SEGUROS: '||mensaje_v, captura_id_usuario_v, username_vi, data_vi);

    RETURN resultado;

    EXCEPTION

    WHEN OTHERS THEN
      codigo_v := SQLSTATE;
      estado_resultado_v := 'error';
      mensaje_v := 'Ha ocurrido un error '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||estado_resultado_v||')';
      RAISE NOTICE 'Ha ocurrido un error % (ERROR NRO: %) (Seccion: %)', SQLERRM, SQLSTATE, seccion_v;

      SELECT codigo_v, estado_resultado_v, mensaje_v, null::BIGINT, null::BIGINT, seccion_v INTO resultado;
      --  Ingresa los resultados a la traza de la auditoria.
      INSERT INTO auditoria.traza(fecha_hora, ip_maquina, tipo_transaccion, modulo, resultado_transaccion, descripcion, user_id, username, data) VALUES (date_ini_vi, ipaddress_v, 'ESCRITURA', modulo_v, estado_resultado_v, 'SOLICITUD DE REGISTRO DE USUARIO EN SEGUROS: '||mensaje_v, captura_id_usuario_v, username_vi, data_vi);

      RETURN resultado;

  END;$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;