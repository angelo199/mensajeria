-- Function: baremo.generar_ponderacion_entrevista(character varying, integer, character varying, character varying, date, integer, integer[], integer[], bigint, bigint, text, numeric)

-- DROP FUNCTION baremo.generar_ponderacion_entrevista(character varying, integer, character varying, character varying, date, integer, integer[], integer[], bigint, bigint, text, numeric);

CREATE OR REPLACE FUNCTION baremo.generar_ponderacion_entrevista(origen_evaluador_vi character varying, cedula_evaluador_vi integer, nombre_apellido_evaluador_vi character varying, cargo_evaluador_vi character varying, fecha_proceso_vi date, puntuacion_total_vi integer, puntuacion_obtenida_vi integer[], item_id_vi integer[], entrevista_id_vi bigint, usuario_ini_id_vi bigint, observacion_vi text, valor_minimo_vi numeric)
  RETURNS record AS
$BODY$
  --Declarando las variables  
  DECLARE

    --  Variables Constantes
    const_requerimientos_min_aprobados_v SMALLINT := 1;
    const_entrevista_finalizada_v SMALLINT := 2;

    --      Grupo de Datos
    const_grupo_bachiller_uno_id_v INT := 1;
    const_grupo_bachiller_dos_id_v INT := 2;
    const_grupo_bachiller_tres_id_v INT := 3;
    const_grupo_tsu_uno_id_v INT :=  4;
    const_grupo_tsu_dos_id_v INT :=  5;
    const_grupo_profesional_uno_id_v INT := 6;
    const_grupo_profesional_dos_id_v INT := 7;
    const_grupo_profesional_tres_id_v INT := 8;

    const_postulacion_aprobada_v SMALLINT := 1;
    const_requerimientos_minimos_aprobados_v SMALLINT := 3;
    const_beneficinado_v SMALLINT := 4;
    const_no_beneficinado_v SMALLINT := 5;

    --      Tipo Periodo
    const_concurso_publico_v INT := 1;
    const_sistema_merito_v INT := 2;

    --  Variables de existencia
    existencia_entrevista_postulacion_id_v INT := 0;
    existencia_entrevista_realizada_id_v INT := 0;
    existencia_item_id_v INT := 0;
    existencia_entrevista_ponderacion_id_v INT := null;
    existencia_postulacion_id_v BIGINT := 0;
    existencia_tipo_apertura_periodo_id_v INT := 0;
    existencia_nivel_cargo_postulado_id_v INT := 0;
    existencia_tipo_cargo_postulado_id_v INT := 0;


    --  Variables de captura
    captura_talento_humano_id_v INT := null;
    captura_postulacion_id_v INT := null;
    captura_entrevista_ponderacion_id_v BIGINT := null;
    captura_minimo_puntaje_v INT := null;
    captura_maximo_puntaje_v INT := null;
    captura_nombre_v VARCHAR := null;
    captura_fecha_ini_v timestamp without time zone := null;
    captura_estatus_aprobado_id_v SMALLINT := null;
    captura_estatus_aprobado_nuevo_id_v SMALLINT := null;
    captura_estatus_entrevista_v SMALLINT := 0;
    captura_tipo_proceso_v VARCHAR := '';
    captura_puntuacion_requerimientos_minimos_v INT := null;
    captura_puntuacion_entrevista_v INT := null;
    captura_puntuacion_total_v INT := 0;
    captura_tipo_cargo_postulado_id_v INT := null;
    captura_condicion_entrevista_puntaje_min_v VARCHAR := null;
    captura_condicion_credenciales_puntaje_min_v VARCHAR := null;
    captura_entrevista_puntaje_min_v INT := 0;
    captura_credenciales_puntaje_min_v INT := 0;
    captura_origen_entrevistador_v VARCHAR := null;
    captura_cedula_entrevistador_v INT := null;
    captura_nombre_apellido_entrevistador_v VARCHAR := null;
    captura_cargo_entrevistador_v VARCHAR := null;
    captura_fecha_entrevista_v DATE := null;
    captura_origen_analista_v VARCHAR := null;
    captura_cedula_analista_v INT := null;
    captura_nombre_apellido_analista_v VARCHAR := null;
    captura_cargo_analista_v VARCHAR := null;
    captura_fecha_evaluacion_req_min_v DATE := null;
    captura_observacion_evaluador_v TEXT := null;
    captura_observacion_analista_v TEXT := null;
    captura_user_id_v BIGINT := null;

    --  Variables de resultado:
    resultado record;

    --  Variables de Bucle
    indice_v INT := 1;
    length_v INt := 0;

    --  Variables de exception
    --      Seccion que permite localizar hasta que punto corrio el codigo hasta que se genero el error.
    seccion_v SMALLINT := 0;
    mensaje_v VARCHAR := 'Aun no ha generado el mensaje para esta respuesta.'; --SQLERRM
    codigo_v VARCHAR := 'ES000'; --SXXXX, ESXXX, EDBXX, ESVXX.
    /* Legenda de codigos de 'codigo_v':
        SXXXX: Proceso completado correctamente.
        EDBXX: Error en el resultado de la Base de Datos.
        ESXXX: Error general del procedimiento almacenado.
        ESVXX: Error de Variable, generalmente son condiciones que no se cumplen con las variables. 
    */
    estado_resultado_v VARCHAR := 'error';

    --  Variables de auditoria
    fecha_hora_v timestamp without time zone := NOW();
    ipaddress_v VARCHAR := ''; -- Si es necesario capturar la ip del servidor donde se aloja la base de datos se usa: 'inet_client_addr();'
    tipo_transaccion_v VARCHAR := 'ESCRITURA'; --Si es ESCRITURA, ELIMINACION, EDICION o LECTURA
    modulo_v VARCHAR := 'gestionHumana.Postulacion.entrevista'; --Modulo.Controlador.Accion 
    transaccion_v VARCHAR := null;
    mensaje_auditoria_v VARCHAR := null;
    username_v VARCHAR := '';
    data_v TEXT := '';

  BEGIN

    seccion_v := 1;

    --  Validar si existe el id de la entrevista ingresada.
    SELECT COUNT(1) INTO existencia_entrevista_postulacion_id_v FROM baremo.entrevista_postulacion WHERE id = entrevista_id_vi LIMIT 1;
    IF existencia_entrevista_postulacion_id_v = 0 THEN
      codigo_v := 'EDB00';
      RAISE EXCEPTION 'El usuario no posee una entrevista disponible en este periodo actual.';
    END IF;

    --  Validar si el usuario ya realizo la entrevista en el periodo actual.
    SELECT COUNT(1) INTO existencia_entrevista_realizada_id_v FROM baremo.entrevista_postulacion WHERE id = entrevista_id_vi AND finalizada = const_entrevista_finalizada_v LIMIT 1;
    IF existencia_entrevista_realizada_id_v = 1 THEN
      codigo_v := 'EDB01';
      RAISE EXCEPTION 'Ya esta entrevista se ha registrado anteriormente.';
    END IF;

    SELECT array_length(item_id_vi, 1) INTO length_v;

    IF length_v is null THEN
      length_v := 0;
    END IF;

    seccion_v := 2;

    --  Validar que la cantidad de items es la misma que la cantidad de puntuaciones
    IF length_v != array_length(puntuacion_obtenida_vi, 1) THEN
      codigo_v := 'ESV00';
      RAISE EXCEPTION 'La cantidad de items es diferente a la cantidad de puntuaciones obtenidas.';
    END IF;

    seccion_v := 3;

    SELECT talento_humano_id, postulacion_id, fecha_ini, puntuacion_req_minimos, puntuacion_entrevista INTO captura_talento_humano_id_v, captura_postulacion_id_v, captura_fecha_ini_v, captura_puntuacion_requerimientos_minimos_v, captura_puntuacion_entrevista_v FROM baremo.entrevista_postulacion WHERE id = entrevista_id_vi LIMIT 1;

    SELECT user_id INTO captura_user_id_v FROM gestion_humana.talento_humano WHERE id = captura_talento_humano_id_v;

    IF captura_user_id_v = usuario_ini_id_vi THEN
      codigo_v := 'EPU00';
      RAISE EXCEPTION 'Usted no Posee Permisos para Autoevaluarce.';
    END IF;

    SELECT COUNT(1) INTO existencia_postulacion_id_v FROM gestion_humana.postulacion WHERE id = captura_postulacion_id_v;
    IF existencia_postulacion_id_v = 0 THEN
      codigo_v := 'EDB02';
      RAISE EXCEPTION 'El id: % de la Postulacion no se Encuentra en la Tabla gestion_humana.postulacion. Comuniquese con el Administrador.', captura_postulacion_id_v;
    END IF;

    SELECT estatus_aprobado INTO captura_estatus_aprobado_id_v FROM gestion_humana.postulacion WHERE id = captura_postulacion_id_v;

    IF captura_estatus_aprobado_id_v = const_postulacion_aprobada_v THEN
      --RAISE EXCEPTION 'VALOR: %, %', captura_estatus_aprobado_id_v, const_postulacion_aprobada_v;
      captura_origen_analista_v := origen_evaluador_vi;
      captura_cedula_analista_v := cedula_evaluador_vi;
      captura_nombre_apellido_analista_v := nombre_apellido_evaluador_vi;
      captura_cargo_analista_v := cargo_evaluador_vi;
      captura_fecha_evaluacion_req_min_v := fecha_proceso_vi;
      captura_observacion_analista_v := observacion_vi;

      captura_puntuacion_requerimientos_minimos_v := puntuacion_total_vi;
      captura_estatus_entrevista_v := const_requerimientos_min_aprobados_v;
      captura_tipo_proceso_v := 'Evaluación de Requerimientos Mínimos';
      IF captura_puntuacion_requerimientos_minimos_v >= valor_minimo_vi THEN
        captura_estatus_aprobado_nuevo_id_v := const_requerimientos_minimos_aprobados_v;
      ELSE
        captura_estatus_aprobado_nuevo_id_v := const_no_beneficinado_v;
      END IF;

    ELSEIF captura_estatus_aprobado_id_v = const_requerimientos_minimos_aprobados_v THEN
      captura_origen_entrevistador_v := origen_evaluador_vi;
      captura_cedula_entrevistador_v := cedula_evaluador_vi;
      captura_nombre_apellido_entrevistador_v := nombre_apellido_evaluador_vi;
      captura_cargo_entrevistador_v := cargo_evaluador_vi;
      captura_fecha_entrevista_v := fecha_proceso_vi;
      captura_observacion_evaluador_v := observacion_vi;

      captura_puntuacion_entrevista_v := puntuacion_total_vi;
      captura_estatus_entrevista_v := const_entrevista_finalizada_v;
      captura_tipo_proceso_v := 'Entrevista';

      IF captura_puntuacion_entrevista_v >= valor_minimo_vi THEN
        captura_estatus_aprobado_nuevo_id_v := const_beneficinado_v;
      ELSE
        captura_estatus_aprobado_nuevo_id_v := const_no_beneficinado_v;
      END IF;
    END IF;

    IF captura_puntuacion_requerimientos_minimos_v is null AND captura_puntuacion_entrevista_v is null THEN
      captura_puntuacion_total_v := 0;
    ELSEIF captura_puntuacion_requerimientos_minimos_v is null OR captura_puntuacion_entrevista_v is null THEN
      IF captura_puntuacion_requerimientos_minimos_v is null THEN
        captura_puntuacion_total_v := captura_puntuacion_entrevista_v + 0;
      ELSEIF captura_puntuacion_entrevista_v is null THEN
        captura_puntuacion_total_v := captura_puntuacion_requerimientos_minimos_v + 0;
      END IF;
    ELSE
      captura_puntuacion_total_v := captura_puntuacion_requerimientos_minimos_v + captura_puntuacion_entrevista_v;
    END IF;

    seccion_v := 4;

    --IF THEN
    FOR indice_v IN 1..length_v LOOP

      captura_entrevista_ponderacion_id_v := null;

      RAISE NOTICE 'Valor de id: % Valor obtenido: %',item_id_vi[indice_v], puntuacion_obtenida_vi[indice_v];

      SELECT COUNT(1) INTO existencia_item_id_v FROM baremo.item_ponderacion WHERE id = item_id_vi[indice_v];
      IF existencia_item_id_v = 0 THEN
        codigo_v := 'EDB04';
        RAISE EXCEPTION 'El id: % no se encuentra en la tabla baremo.item_ponderacion. Comuniquese con el Administrador.', item_id_vi[indice_v];
      END IF;

      SELECT minimo_puntaje, maximo_puntaje, nombre INTO captura_minimo_puntaje_v, captura_maximo_puntaje_v,captura_nombre_v FROM baremo.item_ponderacion WHERE id = item_id_vi[indice_v];

      IF puntuacion_obtenida_vi[indice_v] < captura_minimo_puntaje_v THEN
        codigo_v := 'EDB05';
        RAISE EXCEPTION 'El valor (%) de "%" no puede ser menor de: %',puntuacion_obtenida_vi[indice_v], captura_nombre_v, captura_minimo_puntaje_v;
      END IF;

      IF puntuacion_obtenida_vi[indice_v] > captura_maximo_puntaje_v THEN
        codigo_v := 'EDB06';
        RAISE EXCEPTION 'El valor (%) de "%" no puede ser mayor de: %',puntuacion_obtenida_vi[indice_v], captura_nombre_v, captura_maximo_puntaje_v;
      END IF;

      INSERT INTO baremo.entrevista_ponderacion (
        puntuacion_obtenida,
        item_id,
        talento_humano_id,
        postulacion_id,
        entrevista_id,
        usuario_ini_id,
        usuario_act_id,
        fecha_ini,
        fecha_act
      ) VALUES (
        puntuacion_obtenida_vi[indice_v],
        item_id_vi[indice_v],
        captura_talento_humano_id_v,
        captura_postulacion_id_v,
        entrevista_id_vi,
        usuario_ini_id_vi,
        usuario_ini_id_vi,
        captura_fecha_ini_v,
        fecha_hora_v
      ) RETURNING id INTO captura_entrevista_ponderacion_id_v;

      IF captura_entrevista_ponderacion_id_v is null THEN
        codigo_v := 'EDB07';
        RAISE EXCEPTION 'Ha ocurrido un error al guardar el item: "%:%", Contacte al administrador del sistema para notificar el error.', item_id_vi[indice_v] , captura_nombre_v;
      END IF;
    END LOOP;

    seccion_v := 5;

    UPDATE
      baremo.entrevista_postulacion
    SET
      origen_evaluador = ( CASE WHEN captura_tipo_proceso_v = 'Entrevista' THEN captura_origen_entrevistador_v WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN NULL ELSE origen_evaluador END),
      cedula_evaluador = ( CASE WHEN captura_tipo_proceso_v = 'Entrevista' THEN captura_cedula_entrevistador_v WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN NULL ELSE cedula_evaluador END),
      nombre_apellido_evaluador = ( CASE WHEN captura_tipo_proceso_v = 'Entrevista' THEN captura_nombre_apellido_entrevistador_v WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN NULL ELSE nombre_apellido_evaluador END),
      cargo_evaluador = ( CASE WHEN captura_tipo_proceso_v = 'Entrevista' THEN captura_cargo_entrevistador_v WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN NULL ELSE cargo_evaluador END),
      fecha_entrevista = ( CASE WHEN captura_tipo_proceso_v = 'Entrevista' THEN captura_fecha_entrevista_v WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN NULL ELSE fecha_entrevista END),
      observacion_evaluador = ( CASE WHEN captura_tipo_proceso_v = 'Entrevista' THEN captura_observacion_evaluador_v WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN NULL ELSE observacion_evaluador END),
      puntuacion_total_obtenida = captura_puntuacion_total_v,
      fecha_act = fecha_hora_v,
      finalizada = captura_estatus_entrevista_v,
      puntuacion_req_minimos = captura_puntuacion_requerimientos_minimos_v,
      puntuacion_entrevista = captura_puntuacion_entrevista_v,
      origen_analista = ( CASE WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN captura_origen_analista_v WHEN captura_tipo_proceso_v = 'Entrevista' THEN origen_analista ELSE null END),
      cedula_analista = ( CASE WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN captura_cedula_analista_v WHEN captura_tipo_proceso_v = 'Entrevista' THEN cedula_analista ELSE null END),
      nombre_apellido_analista = ( CASE WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN captura_nombre_apellido_analista_v WHEN captura_tipo_proceso_v = 'Entrevista' THEN nombre_apellido_analista ELSE null END),
      cargo_analista = ( CASE WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN captura_cargo_analista_v WHEN captura_tipo_proceso_v = 'Entrevista' THEN cargo_analista ELSE null END),
      fecha_evaluacion_req_min = ( CASE WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN captura_fecha_evaluacion_req_min_v WHEN captura_tipo_proceso_v = 'Entrevista' THEN fecha_evaluacion_req_min ELSE null END),
      observacion_analista = ( CASE WHEN captura_tipo_proceso_v = 'Evaluación de Requerimientos Mínimos' THEN captura_observacion_analista_v WHEN captura_tipo_proceso_v = 'Entrevista' THEN observacion_analista ELSE null END)

    WHERE
      id = entrevista_id_vi
    RETURNING id INTO existencia_entrevista_ponderacion_id_v;

    IF existencia_entrevista_ponderacion_id_v is not null THEN
      UPDATE gestion_humana.postulacion SET estatus_aprobado = captura_estatus_aprobado_nuevo_id_v, estatus_anterior_aprobado = captura_estatus_aprobado_nuevo_id_v, puntuacion_obtenida = captura_puntuacion_total_v WHERE id = captura_postulacion_id_v;
    END IF;

    mensaje_v := 'Se han Registrado los Datos de la '||captura_tipo_proceso_v||' Correctamente.';
    codigo_v := 'S0000';
    estado_resultado_v := 'success';

    SELECT
      mensaje_v,
      estado_resultado_v,
      codigo_v,
      null::SMALLINT
    INTO
      resultado;

    --  AUDITORIA
    INSERT INTO
      auditoria.traza(
        fecha_hora,
        ip_maquina,
        tipo_transaccion,
        modulo,
        resultado_transaccion,
        descripcion,
        user_id,
        username,
        data
      ) VALUES (
      fecha_hora_v,
      ipaddress_v,
      tipo_transaccion_v,
      modulo_v,
      transaccion_v,
      mensaje_auditoria_v,
      usuario_ini_id_vi,
      username_v,
      data_v
    );
    RETURN resultado;

    EXCEPTION

    WHEN OTHERS THEN
      transaccion_v := SQLSTATE;
      mensaje_auditoria_v := 'ACTIVACION DE POSTULACION PARA ENTREVISTA: '||SQLERRM||' (ERROR NRO: '||SQLSTATE||') (Seccion: '||seccion_v||') (Resultado: '||transaccion_v;
      mensaje_v := SQLERRM;

      SELECT
        mensaje_v,
        estado_resultado_v,
        codigo_v,
        seccion_v
      INTO
        resultado;

      --  AUDITORIA
      INSERT INTO
        auditoria.traza(
          fecha_hora,
          ip_maquina,
          tipo_transaccion,
          modulo,
          resultado_transaccion,
          descripcion,
          user_id,
          username,
          data
        ) VALUES (
        fecha_hora_v,
        ipaddress_v,
        tipo_transaccion_v,
        modulo_v,
        transaccion_v,
        mensaje_auditoria_v,
        usuario_ini_id_vi,
        username_v,
        data_v
      );

      RETURN resultado;

  END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;