<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head profile="http://selenium-ide.openqa.org/profiles/test-case">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="selenium.base" href="http://concursoymerito.dev/" />
<title>Registro de Usuario Externo</title>
</head>
<body>
<table cellpadding="1" cellspacing="1" border="1">
<thead>
<tr><td rowspan="1" colspan="3">Registro de Usuario Externo</td></tr>
</thead><tbody>
<tr>
	<td>type</td>
	<td>id=UserGroupsRegistro_cedula</td>
	<td>17693788</td>
</tr>
<tr>
	<td>type</td>
	<td>id=UserGroupsRegistro_nombre</td>
	<td>José Gabriel</td>
</tr>
<tr>
	<td>type</td>
	<td>id=UserGroupsRegistro_apellido</td>
	<td>González Pérez</td>
</tr>
<tr>
	<td>focus</td>
	<td>id=UserGroupsRegistro_telefono</td>
	<td></td>
</tr>
<tr>
	<td>sendKeys</td>
	<td>id=UserGroupsRegistro_telefono</td>
	<td>4265124336</td>
</tr>
<tr>
	<td>type</td>
	<td>id=UserGroupsRegistro_email</td>
	<td>jgabrielsinner10@gmail.com</td>
</tr>
<tr>
	<td>type</td>
	<td>id=UserGroupsRegistro_password</td>
	<td>123456</td>
</tr>
<tr>
	<td>type</td>
	<td>id=UserGroupsRegistro_password_confirm</td>
	<td>123456</td>
</tr>
<tr>
	<td>select</td>
	<td>id=UserGroupsRegistro_group_id</td>
	<td>label=Fijo</td>
</tr>
<tr>
	<td>select</td>
	<td>id=UserGroupsRegistro_estado_id</td>
	<td>label=ARAGUA</td>
</tr>
<tr>
	<td>focus</td>
	<td>id=UserGroupsRegistro_digitos_cuenta_nomina</td>
	<td></td>
</tr>
<tr>
	<td>sendKeys</td>
	<td>id=UserGroupsRegistro_digitos_cuenta_nomina</td>
	<td>3210</td>
</tr>
<tr>
	<td>click</td>
	<td>id=buttonSubmitId</td>
	<td></td>
</tr>
</tbody></table>
</body>
</html>
