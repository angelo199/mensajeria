TRIGGERS para el bus de datos.

Lista de Triggers:
	-Contratados:
		--tcargos.
		--tdependencia
		--tpersona
	-Fijos
		--tcargos
		--tdependencia
		--tpersona
	-jubilados
		--tpersona

Orden para ejecutar los Triggers:

1: Primero tcargos.
2: Segundo tdependencias.
3: Finalmente tpersona.

Condiciones para los Triggers:
para nm_fijos_tpersona es necesario que antes se ejecuten los triggers de tcargo y tdependencia y se cargen los registros de nm_fijos_tpercardep ya que el Trigger por medio de tpersona busca y procesa la data de todas estas tablas para luego almacenarla en talento_humano.
