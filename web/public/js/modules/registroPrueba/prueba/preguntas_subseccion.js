$(document).ready(function() {
    // Acción para recargar las preguntas
    $('#dialogPreguntasSubSeccion #reloadWidgetPreguntas').click(function() {
        var subSeccionId = $('#dialogPreguntasSubSeccion').attr('data-subseccion-id');
        var uri = '/registroPrueba/prueba/consultaPreguntasSubSeccion/idSubSeccion/' + base64_encode(subSeccionId);
        executeAjax('dialogPreguntasSubSeccion', uri, null, false, true);
    });

    // Acción para recargar las respuestas de una pregunta
    $('.reloadRespuestas').each(function() {
        $(this).click(function() {
            var preguntaId = $(this).attr('data-pregunta-id');
            var divSelector = '#dialogPreguntasSeccion .dialogRespuestas.pregunta' + preguntaId;
            verRespuestasPregunta(preguntaId, divSelector);
        });
    });

    // Acción para ver respuestas de una pregunta
    $('.verRespuestas').each(function() {
        $(this).click(function() {
            var preguntaId = $(this).attr('data-pregunta-id');
            var divSelector = '#dialogPreguntasSubSeccion .dialogRespuestas.pregunta' + preguntaId;
            verRespuestasPregunta(preguntaId, divSelector);
            $(this).off('click');
        });
    });
});