
$('a.verSubSecciones').each(function() {
    
    $(this).click(function() {
        $('#dialogPreguntasSeccion').html('');
        var seccionId = $(this).attr('data-seccion-id');
        var seccionActualTitulo = $(this).attr('data-seccion-nombre');

        var uri = '/registroPrueba/prueba/consultaSubSecciones/idSeccion/' + base64_encode(seccionId);

        MostrarDialogo('dialogSubSecciones', uri, 'Subsecciones de ' + seccionActualTitulo);

    });
});
$('a.verPreguntas').each(function() {
    $(this).click(function() {
        var seccionId = $(this).attr('data-seccion-id');
        var seccionActualTitulo = $(this).attr('data-seccion-nombre');

        var uri = '/registroPrueba/prueba/consultaPreguntasSeccion/idSeccion/' + base64_encode(seccionId);
        $('#dialogPreguntasSeccion').attr('data-seccion-id', seccionId);

        executeAjax('dialogPreguntasSeccion', uri, null, true, true);

        $('#dialogPreguntasSeccion').removeClass('hide');
    });
});
