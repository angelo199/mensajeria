function consultarPregunta(id) {
    direccion = '/registroPrueba/pregunta/informacion/';
    title = 'Detalles de la Pregunta';
    Loading.show();
    var data = { id: id };
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantallaConsultar").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    }

                ]
            });
            $("#dialogPantallaConsultar").html(result);
        }
    });
    Loading.hide();
}


function modificarPregunta(id) {

    //alert(id);
    direccion = '/registroPrueba/pregunta/modificar/';
    title = 'Modificar Nivel';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

function procesarCambio()
{
    direccion = 'procesarCambio';
    var id = $('#id').val();
    var nombre = $('#Nivel_nombre_form').val();
    var nivel = $('#Nivel_tipo_periodo_id').val();
    var cantidad = $('#Nivel_cantidad_form').val();
    var cant_lapsos = $('#Nivel_cant_lapsos_form').val();
    var permite_materia_pendiente = $('#permite_materia_pendiente').val();
    var data = {Nivel: {id: id, nombre: nombre, nivel: nivel, cantidad: cantidad, cant_lapsos: cant_lapsos, permite_materia_pendiente: permite_materia_pendiente}};
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);

}

function inactivarPregunta(id) {
    var dialog = $("#dialogPantallaEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Pregunta</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = { id: id };
                    $.ajax({
                        url: '/registroPrueba/pregunta/inactivarPregunta',
                        data: data,
                        type: 'GET',
                        success:function(data){
                            // alert(data);
                            if(data == 1){
                                refrescarGrid();
                            }
                            if(data == 2){
                                
                            }
                        }

                    });
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    // executeAjax('_form', 'inactivarPregunta', data, true, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function activarPregunta(id,seccion_id) {
    
    //console.log(seccion_id);
    var data = { seccion_id : seccion_id };
    $.ajax({  
        url : '/registroPrueba/Pregunta/verificarEstatusSeccion',
        type: 'post',
        data: data,
        success: function(json) {
                    var json = jQuery.parseJSON(json);
                    //console.log(json.estatusSeccion);
                        if(json.estatusSeccion!='I') 
                        {
                            var dialogAct = $("#dialogPantallaActivacion").removeClass('hide').dialog({
                            modal: true,
                            width: '450px',
                            draggable: false,
                            resizable: false,
                            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Pregunta</h4></div>",
                            title_html: true,
                            buttons: [
                                {
                                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                                    "class": "btn btn-xs",
                                    click: function() {
                                        $(this).dialog("close");
                                    }
                                },
                                {
                                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Pregunta",
                                    "class": "btn btn-success btn-xs",
                                    click: function() {
                                        var data = {
                                            id: id
                                        };
                                        executeAjax('_form', '/registroPrueba/pregunta/activarPregunta', data, true, true, 'GET', refrescarGrid);
                                        $(this).dialog("close");
                                    }
                                }
                            ]
                        }); 
                        }
                        else {
                                var title = "Error de Activación";
                                var mensaje = "Estimado usuario, Para Habilitar Esta SubSección Habilite la Sección a la Que esta Asociada e Intente Nuevamente."
                                verDialogo(mensaje, title);
                             }
                                }
    });
    
  /*  var dialogAct = $("#dialogPantallaActivacion").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Pregunta</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Pregunta",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id
                    };
                    executeAjax('_form', '/registroPrueba/pregunta/activarPregunta', data, true, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                }
            }
        ]
    }); */
}

function notificacion(id) {
    var dialog = $("#dialogPantallaNotificacion").removeClass('hide').dialog({
        modal: true,
        width: '300px',
        dragable: false,
        resizable: false,
        position: 'top',
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Notificación</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                }

            }

        ]
    });
    $("#dialogPantallaNotificacion").html("<div class='alertDialogBox'><p>No puedes modificar esta prueba, ya fue aplicada.</p></div>");
    Loading.hide();
}

function refrescarGrid() {
    $('#pregunta-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}

function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }