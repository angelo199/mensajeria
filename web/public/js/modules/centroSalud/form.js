$(document).ready(function(){

// Masks

$('#CentroSalud_telefono_principal').mask('(0999)999-9999');
$('#CentroSalud_telefono_opcional').mask('(0999)999-9999');


$("#CentroSalud_estado_id").on('change', function(){
        if($(this).val()!=''){
            var municipios = getDataCatastro('Municipio', 'estado_id', $(this).val());
            var municipiosSelect = generateOptionsToSelect(municipios, 'nombre', 'id', '');
            $("#CentroSalud_municipio_id").html(municipiosSelect);
            $("#CentroSalud_parroquia_id").html("<option>- - -</option>");
        }
        else{
            $("#CentroSalud_municipio_id").html('<option>- - -</option>');
            $("#CentroSalud_parroquia_id").html("<option>- - -</option>");
        }
    });
    
    $("#CentroSalud_municipio_id").on('change', function(){
        if($(this).val()!=''){
            var parroquias = getDataCatastro('Parroquia', 'municipio_id', $(this).val());
            var parroquiasSelect = generateOptionsToSelect(parroquias, 'nombre', 'id', '');
            $("#CentroSalud_parroquia_id").html(parroquiasSelect);
        }
        else{
            $("#CentroSalud_parroquia_id").html("<option>- - -</option>");
        }
    });
    
    
    // Limpiar campos
    
    $("#CentroSalud_rif").on('keyup blur', function(){
        keyAlphaNum(this, false, false);
        makeUpper(this);
    });
    $("#CentroSalud_nombre").on('keyup blur', function(){
        keyAlpha(this, true, true);
        makeUpper(this);
    });
    $("#CentroSalud_nombre_contacto").on('keyup blur', function(){
        keyAlpha(this, true, true);
        makeUpper(this);
    });
    $("#CentroSalud_monto_promedio_consulta").on('keyup blur', function(){
        keyNum(this, true, true);
    });
    $("#CentroSalud_direccion").on('keyup blur', function(){
        keyText(this, true);
    });
    $("#CentroSalud_email").on('keyup blur', function(){
        keyEmail(this, false);
    });
    
});