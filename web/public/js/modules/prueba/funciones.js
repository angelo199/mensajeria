/**
 * Funciones del módulo de pruebas
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */

/**
 * Mostrar diálogo modal
 *
 * @param {String} idDivDialog Id del contenedor para el diálogo
 * @param {String} uri         URI de la petición
 * @param {String} title       Título del diálogo
 * @param {Array} buttons      Array de los botones que llevará el diálogo
 * @returns {undefined}
 */
function MostrarDialogo(idDivDialog, uri, title, buttons) {

    if (buttons === undefined) {
        buttons = [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-danger",
                click: function() {
                    $("#" + idDivDialog).dialog("close");
                }
            }
        ];
    }

    $.ajax({
        url: uri,
        method: 'GET',
        dataType: 'html',
        success: function(data) {
            $("#" + idDivDialog).removeClass('hide').dialog({
                modal: true,
                width: '1000px',
                draggable: false,
                resizable: false,
                position: ['center', 20],
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'> " + title + "</h4></div>",
                title_html: true,
                buttons: buttons
            });
            $("#" + idDivDialog).html(data);
        },
        statusCode: {
            404: function() {
                displayDialogBox(idDivDialog, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                $("#" + idDivDialog).removeClass('hide');
            },
            400: function() {
                displayDialogBox(idDivDialog, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                $("#" + idDivDialog).removeClass('hide');
            },
            401: function() {
                displayDialogBox(idDivDialog, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                $("#" + idDivDialog).removeClass('hide');
            },
            403: function() {
                displayDialogBox(idDivDialog, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                $("#" + idDivDialog).removeClass('hide');
            },
            500: function() {
                displayDialogBox(idDivDialog, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                $("#" + idDivDialog).removeClass('hide');
            },
            503: function() {
                displayDialogBox(idDivDialog, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                $("#" + idDivDialog).removeClass('hide');
            },
            999: function(resp) {
                displayDialogBox(idDivDialog, "error", resp.status + ': ' + resp.responseText);
                $("#" + idDivDialog).removeClass('hide');
            }
        }
    });
}

/**
 * Mostrar html
 * @param {String} divSelector Selector jQuery para el elemento donde se mostrará el html
 * @param {String} dataHtml    Response o html que se mostrará
 * @param {String} efecto      Si se desea un efecto como fade o slide
 * @returns {undefined}
 */
function displayHtmlInDivSelector(divSelector, dataHtml, efecto, parentHtmlContainer) {    
    if (efecto == 'slide') {
        if (parentHtmlContainer) {
            $(divSelector, parentHtmlContainer).html('').html(dataHtml).slideDown();
        } else {
            $(divSelector).html('').html(dataHtml).slideDown();
        }
    } else if (efecto == 'fade') {
        if (parentHtmlContainer) {
            $(divSelector, parentHtmlContainer).html('').html(dataHtml).fadeIn();
        } else {
            $(divSelector).html('').html(dataHtml).fadeIn();
        }
    } else {
        if (parentHtmlContainer) {
            $(divSelector, parentHtmlContainer).html('').html(dataHtml);
        } else {
            $(divSelector).html('').html(dataHtml);
        }
    }
}

/**
 * Mostrar una caja de diálogo
 * @param {String} divSelector Selector jQuery para el elemento donde se mostrará el html
 * @param {String} style error, info, alert
 * @param {String} mensaje un mensaje
 * @param {String} parentHtmlContainer Html que contiene el elemento <divSelector>
 * @returns {undefined}
 */
function displayDialogBoxSelector(divSelector, style, message, parentHtmlContainer) {
    var classStyle = dialog[style];
    var dataHtml = "<div class='" + classStyle + "'><p>" + message + "</p></div>";
    if (parentHtmlContainer) {
        displayHtmlInDivSelector(divSelector, dataHtml, false, parentHtmlContainer);
    } else {
        displayHtmlInDivSelector(divSelector, dataHtml);
    }
}

/**
 * Esta funcion efectúa una petición ajax mediante la funcion $.ajax de jquery con manejo de errores.
 *
 * @param {String} selectorDivResult Indica el string selector jquery, ya sea por clases o por id, donde se mostrará el resultado.
 * @param {String} urlDir Dirección a donde se efectuará la petición Ajax.
 * @param {Object|String} datos los datos a enviar junto a la petición ajax, puede estar en formato string serializado o en formato json...
 * @param {Boolean|String} loadingEfect Indica si muestra el efecto de cargando o no.
 * @param {Boolean} showResult Indica si muestra el Resultado de la petición Ajax o no.
 * @param {String} method POST, GET, entre otros...
 * @param {String} responseFormat json, html, xml...
 * @param {function} beforeSendCallback function que se ejecutará antes de enviar la petición.
 * @param {function} successCallback function que se ejecutará luego de enviar la petición, se recibe el dataResponse.
 * @param {function} errorCallback function que se ejecutará si se produce un error
 * @returns {undefined}
 */
function ejecutarAjax(divSelector, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback) {

    if (!method) {
        method = "POST";
    }

    if (!responseFormat) {
        responseFormat = "html";
    }

    $.ajax({
        type: method,
        url: urlDir,
        dataType: responseFormat,
        data: datos,
        beforeSend: function() {
            if (loadingEfect) {
                var url_image_load = "<div class='padding-5 center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                displayHtmlInDivSelector(divSelector, url_image_load);
            }
            if (typeof beforeSendCallback == "function" && beforeSendCallback) {
                beforeSendCallback();
            }
        },
        success: function(response, estatusCode, dom) {
            ajaxDataResponse = response;
            ajaxestatusCode = estatusCode;
            ajaxDoom = dom;
            ajaxDataResponse.error = null;
            if (showResult) {
                displayHtmlInDivSelector(divSelector, response, loadingEfect);
            }
            if (typeof beforeSendCallback == "function" && beforeSendCallback && typeof successCallback != "function") {
                beforeSendCallback();
            }
            if (typeof successCallback == "function" && successCallback) {
                successCallback(response, estatusCode, dom);
            }
        },
        statusCode: {
            404: function() {
                displayDialogBoxSelector(divSelector, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
            },
            400: function() {
                displayDialogBoxSelector(divSelector, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
            },
            401: function() {
                displayDialogBoxSelector(divSelector, "error", "401: Datos insuficientes para efectuar esta acci&oacute;n.");
            },
            403: function() {
                displayDialogBoxSelector(divSelector, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            },
            500: function() {
                displayDialogBoxSelector(divSelector, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            },
            503: function() {
                displayDialogBoxSelector(divSelector, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            },
            999: function(resp) {
                displayDialogBoxSelector(divSelector, "error", resp.status + ': ' + resp.responseText);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            //alert(xhr);
            //alert(thrownError);
            ajaxDataResponse = xhr;
            ajaxDataResponse['error'] = thrownError;
            ajaxDataResponse['mensaje'] = '';
            if (xhr.status == '403') {
                ajaxDataResponse['mensaje'] = "Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.";
                displayDialogBoxSelector(divSelector, "error", "Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
            } else if (xhr.status == '401') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBoxSelector(divSelector, "error", "Datos insuficientes para efectuar esta acci&oacute;n.");
            } else if (xhr.status == '400') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBoxSelector(divSelector, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
            } else if (xhr.status == '500') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBoxSelector(divSelector, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
            } else if (xhr.status == '503') {
                ajaxDataResponse['mensaje'] = "Datos insuficientes para efectuar esta acci&oacute;n.";
                displayDialogBoxSelector(divSelector, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
            }
            if (typeof errorCallback == "function" && errorCallback) {
                errorCallback(xhr, ajaxOptions, thrownError, ajaxDataResponse['mensaje']);
            }
        }
    });
}

/**
 * Función para ver un modal dialog
 * @param {String} message Mensaje del diálogo
 * @param {String} title Título del diálogo
 * @param {String} style Estilo del diálogo {error, info, ...}
 * @param {function} clickCallback Callback de las acciones que se hacen al hacer click
 * @param {Boolean} reload Si es true recarga la  página
 * @param {Object} buttonsExtra Si es un array se agrega a buttons
 * @returns {undefined}
 */
function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
    if (!style)
        style = 'info';

    var buttonsDialog = [
        {
            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
            "class": "btn btn-xs",
            click: function() {
                $(this).dialog("close");
                if (clickCallback) {
                    clickCallback();
                }
                if (reload) {
                    window.location.reload();
                }
            }
        }
    ];

    if (buttonsExtra)
        buttonsDialog.push(buttonsExtra);

    displayDialogBox('dialog_error', style, message);

    var dialog = $("#dialog_error").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
        title_html: true,
        buttons: buttonsDialog
    });
}

/**
 * Ver las respuestas de una pregunta
 * @param {Number} preguntaId
 * @param {String} divSelector
 * @returns {undefined}
 */
function verRespuestasPregunta(preguntaId, divSelector) {
    var uri = '/registroPrueba/prueba/consultaRespuestas/idPregunta/' + base64_encode(preguntaId);
    ejecutarAjax(divSelector, uri, null, true, true);
    $(divSelector).removeClass('hide');
}

/**
 * Validar los elementos duplicados
 * @param {String} elemento Elemento que hay que validar
 * @param {Array} elementoArray Arreglo de elementos en el que hay que validar
 * @returns {Boolean} Si el elemento se repite o no
 */
function validarElementoDuplicado(elemento, elementosArray) {
    if (elementosArray != '') {
        var repetidos = 0;
        
        for (x = 0; x <= elementosArray.length; x++) {
            if (elementosArray[x] == elemento) {
                repetidos++;
            }
        }

        if (repetidos > 1) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * Validar las respuestas duplicadas
 * @param {String} respuesta Respuesta que hay que validar
 * @param {Array} respuestaArray Arreglo de respuestas en el que hay que validar
 * @returns {Boolean} Si la respuesta se repite o no
 */
function validarRespuestaDuplicada(respuesta, respuestaArray) {
    return validarElementoDuplicado(respuesta, respuestaArray);
}

/**
 * Validar las preguntas duplicadas
 * @param {String} pregunta Subsección que hay que validar
 * @param {Array} preguntaArray 
 * @returns {Boolean}
 */
function validarPreguntaDuplicada (pregunta, preguntaArray) {
    return validarElementoDuplicado(pregunta, preguntaArray);
}

/**
 * Validar las subseccioes duplicadas
 * @param {String} subSeccion Subsección que hay que validar
 * @param {Array} subSeccionArray 
 * @returns {Boolean}
 */
function validarSubSeccionDuplicada(subSeccion, subSeccionArray) {
    return validarElementoDuplicado(subSeccion, subSeccionArray);
}

/**
 * Validar las secciones duplicadas
 * @param {String} seccion
 * @param {Array} seccionArray
 * @returns {boolean}
 */
function validarSeccionDuplicada(seccion, seccionArray) {
    return validarElementoDuplicado(seccion, seccionArray);
}

/**
 * Validar valores del formulario
 * @param {Array} arrayValues Arreglo de objetos de elementos del formulario
 * @returns {undefined}
 */
function validarFormRegistro(arrayValues) {
    //console.log(arrayValues);
    for (var i = 0; i < arrayValues.length; i++) {
        if (arrayValues[i].value == "") {
            console.log('Campo ' + arrayValues[i].name + ' vacío.');
            return arrayValues[i].name;
        }
    }
}

/**
 * Objeto para las funciones de Hora
 * @type Object
 */
function HaydeeTime() {
    
    this.time = new Date();
    
    /**
     * Agrega los ceros faltantes
     * @param {String} nStr
     * @param {Number} nLen
     * @returns {HaydeeTime.padNbm.sRes|String}
     */
    this.padNmb = function padNmb(nStr, nLen) {
        var sRes = String(nStr); 
        var sCeros = "0000000000"; 
        return sCeros.substr(0, nLen - sRes.length) + sRes;
    };
    
    /**
     * Convierte la hora en segundos
     * @param {String} time
     * @returns {Number}
     */
    this.stringToSeconds = function stringToSeconds(time) {
        var sep1 = time.indexOf(":"); 
        var sep2 = time.lastIndexOf(":"); 
        var hor = time.substr(0, sep1); 
        var min = time.substr(sep1 + 1, sep2 - sep1 - 1); 
        var sec = time.substr(sep2 + 1); 
        return (Number(sec) + (Number(min) * 60) + (Number(hor) * 3600));
    };
    
    /**
     * Convierte a hora los segundos
     * @param {Number} secs
     * @returns {String}
     */
    this.secondsToTime = function secondsToTime(secs) {
        var hor = Math.floor(secs / 3600); 
        var min = Math.floor((secs - (hor * 3600)) / 60); 
        var sec = secs - (hor * 3600) - (min * 60); 
        return this.padNmb(hor, 2) + ":" + this.padNmb(min, 2) + ":" + this.padNmb(sec, 2);
    };
    
    /**
     * Returna la resta de 2 horas
     * @param {String} a
     * @param {String} b
     * @returns {String}
     */
    this.substractTimes = function substractTimes(a, b) {
        var secs1 = this.stringToSeconds(a);
        var secs2 = this.stringToSeconds(b);
        var secsDif = secs1 - secs2;
        return this.secondsToTime(secsDif);
    };
    
    /**
     * Returna la suma de las horas
     * @param {Array} times Arreglo de las horas a sumar
     * @returns {String}
     */
    this.sumTimes = function sumTimes(times) {
        if ((typeof(times) === 'object' || typeof(times) === 'array') && times.length > 0) {
            var secsSum = 0;
            for (var i = 0; i < times.length; i++) {
                secsSum = secsSum + this.stringToSeconds(times[i]);
            }
            return this.secondsToTime(secsSum);
        } else {
            console.error(times, 'Parámetro inválido. Tipo esperado {Array|Object}. Mayor a 0.');
        }
    };
};

/**
 * Objeto de HaydeeTime
 * @type HaydeeTime
 */
var HTO = new HaydeeTime();