/**
 * Acciones para registro
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
$('form[data-form-type="registro"]').on('keypress', function(evt) {
    if (evt.keyCode == 13) {
        evt.preventDefault();
        return false;
    }
}).submit(function(evt) {
	evt.preventDefault();
});
// Agregar la seccion dependiendo de la prueba
$('form[data-form-type="registro"] select#Prueba_id').change(function() {
	$('.response').html('');

	if ($(this).val() != '') {
    	executeAjax('secciones', '/registroPrueba/SeccionPrueba/agregarSeccion', {'Prueba[id]': $(this).val()}, true, true);
	} else {
		displayDialogBoxSelector('#secciones', 'alert', 'Seleccione una prueba.');
	}
});