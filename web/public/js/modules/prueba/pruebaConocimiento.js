$(document).ready(function () {
    pruebaConocimiento.validarReloj();
    $(".btn-siguiente-pregunta").on("click", function () {
        var indice = $(this).attr("data-indice");
        indice++;
        $("#tab-pregunta-" + indice).click();
    });

    $(".btn-anterior-pregunta").on("click", function () {
        var indice = $(this).attr("data-indice");
        indice--;
        //console.log(typeof indice);   
        $("#tab-pregunta-" + indice).click();
    });

    $('input:radio').on("change", function () {
        var indice = ($(this).attr("data-indice")) * 1;
        // console.log("#i-pregunta-"+indice);
        $("#i-pregunta-" + indice).removeClass("fa-square-o").addClass("fa-check-square-o blue");
        $("#s-pregunta-" + indice).addClass("blue");
    });

    $(".btnPruebaConocimientoa").on("click", function () {
        var cont = pruebaConocimiento.getCantAnsweredQuestions();
        pruebaConocimiento.showDialog('USUARIO', cont);
    });

    $("#prueba-conocimiento-form").attr("data-form-type", "create");


    $("#prueba-conocimiento-form").on('submit', function (evt) {
        evt.preventDefault();
        pruebaConocimiento.submitForm($(this));
    });
    
    pruebaConocimiento.enableBeforeUnload();
    // Prevenir el us del F5 - Evita la actualización de la página
    (function() {
        $(document).keydown(function(e){
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 116) {
                e.preventDefault();
            }
        });
    })();

});


var clock = null;

var pruebaConocimiento = {

    submitForm: function (form) {

        // configuracion de la peticion ajax
        var divResult = "#div-result-prueba-conocimiento";
        var urlDir = form.attr('action');

        var type = form.attr('data-form-type');
        var method = "POST";
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;
        var responseFormat = "json";

        var successCallback = function (response) {

            if(response.resultado == 'success') {
                
                pruebaConocimiento.disableBeforeUnload();
                
                displayDialogBox(divResult, 'exito', response.mensaje_usuario);
                $("#div-mensaje-confirmar-envio-prueba").html("<div class='successDialogBox'><p>"+response.mensaje_usuario+"</p></div>");

                $("#Prueba_mensaje_sistema").val(response.mensaje_sistema);
                $("#Prueba_mensaje_usuario").val(response.mensaje_usuario);
                $("#Prueba_estatus").val(response.estatus);
                $("#Prueba_seccion").val(response.seccion);
                $("#Prueba_estatus_postulacion").val(response.estatus_postulacion);

                setTimeout(function () {
                    $("#submit-mostrar-resultado-prueba").click();
                }, 1000);

            }
            else {

                $("#botonCancelarEnvioPrueba").removeAttr("disabled");
                $("#botonConfirmarEnvioPrueba").removeAttr("disabled");

                if (containsHtml(response.mensaje_usuario)) {
                    displayHtmlInDivId(divResult, response.mensaje_usuario);
                    $("#div-mensaje-confirmar-envio-prueba").html(response.mensaje_usuario);
                }
                else {
                    displayDialogBox(divResult, 'error', response.mensaje_usuario);
                    $("#div-mensaje-confirmar-envio-prueba").html("<div class='errorDialogBox'><p>"+response.mensaje_usuario+"</p></div>");
                }
            }
        };
        var errorCallback = function (xhr, ajaxOptions, thrownError) {
            $("#botonConfirmarEnvioPrueba").removeAttr("disabled");
            $("#botonCancelarEnvioPrueba").removeAttr("disabled");
        };

        var beforeSendCallback = function () {
            clock.stop();
            $("#botonCancelarEnvioPrueba").attr("disabled", "disabled");
            $("#botonConfirmarEnvioPrueba").attr("disabled", "disabled");
        };

        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback, beforeSendCallback);

    },

    showDialog: function (tipo, cont) {
        //cont cantidad de opciones sin responder  
        //tipo = 1 si proviene del usuario tipo =2 si proviene del clock
        console.log(cont);
        if (tipo == 'USUARIO') {
            $("#finalizada_por").val(tipo);// seteando el hidden para enviar si fue enviado por el usuario o por clock
            var mitad = cantidadPreguntas / 2;
            var dialog = $("#div-mensaje-confirmar-envio-prueba").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                draggale: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Finalización de la Prueba</h4></div>",
                title_html: true,
                buttons: [{
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                        "class": "btn btn-xs btn-danger",
                        "id": "botonCancelarEnvioPrueba",
                        "click": function () {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Confirmar &nbsp;<i class='fa fa-exclamation-triangle bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonConfirmarEnvioPrueba",
                        "click": function () {
                            $("#btnSubmitPrueba").click();
                        }
                    }]
            });

            if (cont == 0) { //cont cantidad de opciones sin responder
                displayDialogBox("#div-mensaje-confirmar-envio-prueba", 'alert', '¿Confirma que desea realizar el envió de la prueba?');
            }
            else
            {
                if (cont > mitad) {
                    displayDialogBox("#div-mensaje-confirmar-envio-prueba", 'alert', 'Usted Posee más de la mitad de la prueba sin responder. Esto puede afectar su calificacion final. ¿Confirma que desea realizar el envió de la prueba?');
                }
                else {
                    displayDialogBox("#div-mensaje-confirmar-envio-prueba", 'alert', 'Posee preguntas sin responder. ¿Confirma que desea realizar el envió de la prueba?');
                }
            }
        }
        else { // si se expiro  el tiempo.
            var dialog = $("#div-mensaje-confirmar-envio-prueba").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                draggale: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Finalización de la Prueba</h4></div>",
                title_html: true
            });

            displayDialogBox("#div-mensaje-confirmar-envio-prueba", 'alert', 'El  tiempo de la prueba ha expirado. Estamos realizando el envio de la prueba con los datos que ha suministrado hasta este momento.');
            $("#finalizada_por").val(base64_encode(tipo));// seteando el hidden para enviar si fue enviado por el usuario o por clock
            $("#btnSubmitPrueba").click();
        }

    },

    validarReloj: function () {
        //Cronometro.

        var reloj = '';
        var clock;
        var mensaje = '';
        var titulo = '';
        var tiempoEnSegundos = tiempoSegundos;
        console.log(tiempoEnSegundos);
        var resultado = false;
        clock = $('.clock').FlipClock(tiempoEnSegundos, {
            autoStart: false,
            countdown: true,
            language: 'es',
            callbacks: {
                interval: function () {
                    var time = clock.getTime().time;
                    if (time == 0) {
                        //Change element
                        clock.stop();
                        var cont = pruebaConocimiento.getCantAnsweredQuestions();
                        pruebaConocimiento.showDialog('TIEMPO', cont);
                    }
                }
            }
        });

        clock.start();
        //Fin.
    },
    getCantAnsweredQuestions: function () {
        var i = 0;
        var cont = 0;//contador de preguntas sin responder.
        var selector = '';
        selector = 'respuestas["' + preguntasId[i] + '"]';
        for (i = 0; i < preguntasId.length; i++) {
            selector = 'respuestas[' + preguntasId[i] + ']';
            if ($('input[name="' + selector + '"]:checked').length == '0') {
                cont++;
            }
        }
        return cont;

    },
    
    enableBeforeUnload : function () {
        window.onbeforeunload = function (e) {
            return "Desea usted dejar la página actual? Si lo hace no podrá presentar la prueba de nuevo y sus datos se perderán.";
        };
    },
    
    disableBeforeUnload: function() {
        window.onbeforeunload = null;
    }

};
