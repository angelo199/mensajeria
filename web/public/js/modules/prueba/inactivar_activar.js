

$(".inactivacionPrueba").unbind('click');
$(".inactivacionPrueba").on('click', function(e) {
    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");
    console.log(registro_id);
    inactivar(registro_id);
});

function inactivar(registro_id) {
    $("#pregunta_inactivar").removeClass('hide');
    $("#msj_error_inactivar").addClass('hide');
    $("#msj_error_inactivar p").html('');

    var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Prueba</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: registro_id
                    };
                    $.ajax({
                        url: "/registroPrueba/prueba/inactivacion/",
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == "success") {

                                    refrescarGrid();
                                    dialogInactivar.dialog('close');
                                    $("#msj_error_inactivar").addClass('hide');
                                    $("#msj_error_inactivar p").html('');
                                    $("#msj_exitoso").removeClass('hide');
                                    $("#msj_exitoso p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }

                                if (json.statusCode == "error") {

                                    $("#pregunta_inactivar").addClass('hide');
                                    $("#msj_exitoso").addClass('hide');
                                    $("#msj_exitoso p").html('');
                                    $("#msj_error_inactivar").removeClass('hide');
                                    $("#msj_error_inactivar p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_inactivacion").show();
}




$(".activacionPrueba").unbind('click');
$(".activacionPrueba").on('click', function(e) {

    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    activar(registro_id);

});


function activar(registro_id) {

    $("#pregunta_activar").removeClass('hide');
    $("#msj_error_activar").addClass('hide');
    $("#msj_error_activar p").html('');

    var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de Prueba</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: registro_id
                    }

                    $.ajax({
                        url: "/registroPrueba/prueba/activacion/",
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == "success") {

                                    refrescarGrid();
                                    dialogActivar.dialog('close');
                                    $("#msj_error_activar").addClass('hide');
                                    $("#msj_error_activar p").html('');
                                    $("#msj_exitoso").removeClass('hide');
                                    $("#msj_exitoso p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }

                                if (json.statusCode == "error") {

                                    $("#pregunta_activar").addClass('hide');
                                    $("#msj_exitoso").addClass('hide');
                                    $("#msj_exitoso p").html('');
                                    $("#msj_error_activar").removeClass('hide');
                                    $("#msj_error_activar p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_activacion").show();
}





function refrescarGrid() {

    $('#pruebaGrid').yiiGridView('update', {
        data: $(this).serialize()
    });
}

