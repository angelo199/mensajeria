$(document).ready(function() {
    // Acción para recargar las preguntas
    $('#dialogPreguntasSeccion #reloadWidgetPreguntas').click(function() {
        var seccionId = $('#dialogPreguntasSeccion').attr('data-seccion-id');
        var uri = '/registroPrueba/prueba/consultaPreguntasSeccion/idSeccion/' + base64_encode(seccionId);
        executeAjax('dialogPreguntasSeccion', uri, null, false, true);
    });

    // Acción para recargar las respuestas de una pregunta
    $('.reloadRespuestas').each(function() {
        $(this).click(function() {
            var preguntaId = $(this).attr('data-pregunta-id');
            var divSelector = '#dialogPreguntasSeccion .dialogRespuestas.pregunta' + preguntaId;
            verRespuestasPregunta(preguntaId, divSelector);
        });
    });

    // Acción para ver respuestas de una pregunta
    $('.verRespuestas').each(function() {
        $(this).click(function() {
            var preguntaId = $(this).attr('data-pregunta-id');
            var divSelector = '#dialogPreguntasSeccion .dialogRespuestas.pregunta' + preguntaId;
            verRespuestasPregunta(preguntaId, divSelector);
            $(this).off('click');
        });
    });
});