/**
 * Funciones para la busqueda general de pruebas
 * @author Ramon Serrano
 */

$(document).ready(function () {
    $('#Prueba_nombre').bind('keyup blur', function () {
        keyLettersAndSpaces(this, true);// Acepta solo letras y espacios
    });
});

//ELEMENTOS DE LA BUSQUEDA AVANZADA DE ASPIRANTES
$(document).ready(function () {
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        'showOn': 'focus',
        'showOtherMonths': false,
        'selectOtherMonths': true,
        'changeMonth': true,
        'readOnly': true,
        'changeYear': true,
        minDate: new Date(1979, 1, 1),
        maxDate: 'today',
        yearRange: '1979:2014'
    });
    $('#Prueba_fecha_nacimiento').datepicker();

    $('#Aspirante_fecha_nacimiento').attr('readOnly', true);


    $('#nombreAspirante').bind('keyup blur', function () {
        keyLettersAndSpaces(this, true);// Acepta solo letras y espacios
        makeUpper(this);
    });

});