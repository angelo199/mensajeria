$(document).ready(function() {

    $("#Pregunta_id").bind('change', function(){
        if($("#Pregunta_id").val() == ''){
        $("#Pregunta_seccion_prueba_id").html('<option>Seleccione</option>');
        }
        else{
            $("#Pregunta_seccion_prueba_id").html('<select id="Pregunta_seccion_prueba_id" name="Pregunta[seccion_prueba_id]" required="required" class="span-7"><option value="">Seleccione</option><option value="1">Sección</option></select>');
        }
        $("#detallesSeccion").html('<option>Seleccione</option>');
        $("#detalles").addClass('hide');
    });
    $("#botonGuardarPregunta").bind('click', function(){
        var tipoPrueba = $("#Pregunta_id").val();
        var pregunta_seccion_prueba_id = $("#Pregunta_seccion_prueba_id").val();
        var detallesSeccion = $("#detallesSeccion").val();
        var pregunta_nombre = $("#Pregunta_nombre").val();
        var pregunta_ponderacion = $("#Pregunta_ponderacion").val();
        var cantidadRespuestas = $("#cantidadRespuestas").val();
        var respuestaVacia = 0;
        var respuestaCheckVacia = 0;
        var i = 0;

        if(tipoPrueba == '' || pregunta_seccion_prueba_id == '' || detallesSeccion == ''){
            $("#mensajeAlerta").html('<p>Todos los campos con <span class="required">*</span>son requeridos.</p>');
            $("#mensajeAlerta").addClass('errorDialogBox');
            $("#mensajeAlerta").removeClass('infoDialogBox');
        }

    });
    $("#Pregunta_seccion_prueba_id").bind('change', function(){
        $("#detalles").addClass('hide');
        $("#step2").addClass('hide');
    });
    $("#detallesSeccion").bind('change', function(){
        $("#detalles").removeClass('hide');
        $("#step2").removeClass('hide');
    });

    var MaxInputs       = 8; //Número Maximo de Campos
    var contenedor       = $("#contenedor"); //ID del contenedor
    var AddButton       = $("#agregarCampo"); //ID del Botón Agregar

    var cabecera = '<div class="widget-box"><div class="widget-header"><h5>Datos Generales</h5><div class="widget-toolbar"><a data-action="collapse" href="#"><i class="icon-chevron-up"></i></a></div></div><div class="widget-body"><div class="widget-body-inner"><div class="widget-main"><div class="widget-main form"><div class="row"><div class="row" id="step2"><div id="contenedor"><div class=""><div class="col-md-8">';
    var pie = '</div></div></div></div></div></div></div></div></div></div>';

    var formulario_pregunta = '<div class="col-md-8"><label for="Pregunta_Indica_la_Pregunta_&lt;span_class=&quot;required&quot;&gt;*&lt;/span&gt;">Indica La  Pregunta <span class="required">*</span></label>                                                                <input type="text" maxlength="150" id="Pregunta_nombre" name="Pregunta[nombre]" required="required" class="span-12" prompt="Seleccione"></div><div class="col-md-4"><label for="Pregunta_Ponderación_&lt;span_class=&quot;required&quot;&gt;*&lt;/span&gt;">Ponderación <span class="required">*</span></label>                                                                <input type="text" id="Pregunta_ponderacion" name="Pregunta[ponderacion]" required="required" class="span-12" prompt="Seleccione"></div>';

    //var x = número de campos existentes en el contenedor
    var x = $("#contenedor div").length + 1;
    var FieldCount = x-1; //para el seguimiento de los campos

    $(AddButton).click(function (e) {
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            //agregar campo
            $(contenedor).append(cabecera + formulario_pregunta  + pie);

//\n\
//\n\
//<div><div id="contenedor"><div class="col-md-8"><label for="Pregunta_Indica_la_Pregunta_&lt;span_class=&quot;required&quot;&gt;*&lt;/span&gt;">Indica La  Pregunta <span class="required">*</span></label>                                                                <input type="text" maxlength="150" id="Pregunta_nombre" name="Pregunta[nombre]" required="required" class="span-12" prompt="Seleccione"></div><div class="col-md-4"><label for="Pregunta_Ponderación_&lt;span_class=&quot;required&quot;&gt;*&lt;/span&gt;">Ponderación <span class="required">*</span></label>                                                                <input type="text" id="Pregunta_ponderacion" name="Pregunta[ponderacion]" required="required" class="span-12" prompt="Seleccione"></div></div>');
//<input type="text" name="mitexto[]" id="campo_'+ FieldCount +'" placeholder="Texto '+ FieldCount +'"/><a href="#" class="eliminar">&times;</a></div>');
            x++; //text box increment
        }
        return false;
    });

    $("body").on("click",".eliminar", function(e){ //click en eliminar campo
        if( x > 1 ) {
            $(this).parent('div').remove(); //eliminar el campo
            x--;
        }
        return false;
    });
    
    cargarFormPreguntas();
});

function cargarFormPreguntas(){
    var periodoId = $("#Pregunta_id").val();
    var detallesSeccion = $("#detallesSeccion").val();
    if(!(isNaN(periodoId) || isNaN(detallesSeccion))){
        var datos = {"Pregunta[id]": periodoId, "Pregunta[seccion_prueba_id]": detallesSeccion};
        var divResult = "detalles";
        var urlDir = "/prueba/pregunta/seleccionarNombreSeccion";
        var loadingEfect = true;
        var showResult = true;
        var method = "GET";
        var responseFormat = "html";
        var successCallback = function(){
            $("#detalles").removeClass('hide');
            $("#step2").removeClass('hide');
        };
        var errorCallback = null;
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
    }
}

function consultarPregunta(id) {
    direccion = '/prueba/pregunta/informacion/';
    title = 'Detalles de la Pregunta';
    Loading.show();
    var data = { id: id };
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantallaConsultar").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>" + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    }

                ]
            });
            $("#dialogPantallaConsultar").html(result);
        }
    });
    Loading.hide();
}


function modificarPregunta(id) {

    //alert(id);
    direccion = '/prueba/pregunta/modificar/';
    title = 'Modificar Nivel';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '900px',
                dragable: false,
                resizable: false,
                position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

function procesarCambio()
{
    direccion = 'procesarCambio';
    var id = $('#id').val();
    var nombre = $('#Nivel_nombre_form').val();
    var nivel = $('#Nivel_tipo_periodo_id').val();
    var cantidad = $('#Nivel_cantidad_form').val();
    var cant_lapsos = $('#Nivel_cant_lapsos_form').val();
    var permite_materia_pendiente = $('#permite_materia_pendiente').val();
    var data = {Nivel: {id: id, nombre: nombre, nivel: nivel, cantidad: cantidad, cant_lapsos: cant_lapsos, permite_materia_pendiente: permite_materia_pendiente}};
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);
}

function inactivarPregunta(id) {
    var dialog = $("#dialogPantallaEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Pregunta</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = { id: id };
                    $.ajax({
                        url: '/prueba/pregunta/inactivarPregunta',
                        data: data,
                        type: 'GET',
                        success:function(data){
                            // alert(data);
                            if(data == 1){
                                refrescarGrid();
                            }
                            if(data == 2){
                                
                            }
                        }

                    });
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    // executeAjax('_form', 'inactivarPregunta', data, true, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function activarPregunta(id,seccion_id) {
    
    //console.log(seccion_id);
    var data = { seccion_id : seccion_id };
    $.ajax({  
        url : '/prueba/Pregunta/verificarEstatusSeccion',
        type: 'post',
        data: data,
        success: function(json) {
                    var json = jQuery.parseJSON(json);
                    //console.log(json.estatusSeccion);
                        if(json.estatusSeccion!='I') 
                        {
                            var dialogAct = $("#dialogPantallaActivacion").removeClass('hide').dialog({
                            modal: true,
                            width: '450px',
                            draggable: false,
                            resizable: false,
                            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Pregunta</h4></div>",
                            title_html: true,
                            buttons: [
                                {
                                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                                    "class": "btn btn-xs",
                                    click: function() {
                                        $(this).dialog("close");
                                    }
                                },
                                {
                                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Pregunta",
                                    "class": "btn btn-success btn-xs",
                                    click: function() {
                                        var data = {
                                            id: id
                                        };
                                        executeAjax('_form', '/prueba/pregunta/activarPregunta', data, true, true, 'GET', refrescarGrid);
                                        $(this).dialog("close");
                                    }
                                }
                            ]
                        }); 
                        }
                        else {
                                var title = "Error de Activación";
                                var mensaje = "Estimado usuario, Para Habilitar Esta SubSección Habilite la Sección a la Que esta Asociada e Intente Nuevamente."
                                verDialogo(mensaje, title);
                             }
                                }
    });
    
  /*  var dialogAct = $("#dialogPantallaActivacion").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Pregunta</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Pregunta",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id
                    };
                    executeAjax('_form', '/prueba/pregunta/activarPregunta', data, true, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                }
            }
        ]
    }); */
}

function notificacion(id) {
    var dialog = $("#dialogPantallaNotificacion").removeClass('hide').dialog({
        modal: true,
        width: '300px',
        dragable: false,
        resizable: false,
        position: 'top',
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Notificación</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                }

            }

        ]
    });
    $("#dialogPantallaNotificacion").html("<div class='alertDialogBox'><p>No puedes modificar esta prueba, ya fue aplicada.</p></div>");
    Loading.hide();
}

function refrescarGrid() {
    $('#pregunta-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}

function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }
