/**
 * Acciones para edición
 * @author Ramón Serrano <ramon.calle.88@gmail.com>
 */
$('form#prueba-form').on('keypress', function(evt) {
    if (evt.keyCode == 13) {
        evt.preventDefault();
        return false;
    }
}).submit(function(evt) {
    evt.preventDefault();

    var nameInputEmpty = validarFormRegistro($(this).serializeArray());

    if (nameInputEmpty) {
        var clickCallback = function() {
            console.log(nameInputEmpty);
            $('input[name="' + nameInputEmpty + '"]').addClass('error').focus();
        };
        verDialogo('Estimado usuario por favor ingrese los datos correspondientes en los campos marcados como requeridos con el * ya que son obligatorios.', 'Campo vacío', 'error', clickCallback);
        return;
    } else {
        $('#subSeccionPregunta, #subSeccionPonderacion, #pregunta, #ponderacion').attr('disabled', 'disabled');
        $('input[name="' + nameInputEmpty + '"]').removeClass('error');
    }

    var respuestasArray = new Array();
    $('.respuestaCorrecta:checked').each(function() {
        if ($(this).val() != "") {
            respuestasArray.push($(this).val());
        }
    });

    if (respuestasArray.length > 0 && !jQuery.isEmptyObject(respuestasArray)) {
        var success = function(response) {
            response = JSON.parse(response);

            if (response.mensaje && response.tipo) {
                if (response.mensaje == 'Nombre de sección ya existe.') {
                    $('input#Seccion_nombre').removeAttr('readonly');
                }
                $('.response').html('<div class="' + response.tipo + '"><p>' + response.mensaje + '</p></div>');
            } else if (response.filasNoInsertadas == 0 && response.filasAfectadas > 1) {
                $('.response').html('<div class="successDialogBox"><p>¡Actualización exitosa!</p></div>');

                $('input#SeccionPrueba_nombre, input#SeccionPrueba_cantidad_respuesta, input#SeccionPrueba_duracion').removeAttr('disabled');
                $('button#agregar-seccion').removeClass('disabled');
            } else {
                $('.response').html('<div class="errorDialogBox"><p>¡Ocurrió un error al tratar de guardar.!</p><p>Intente de nuevo más tarde.</p></div>');
                console.log(response);
            }
            scrollUp('fast');
        };

        ejecutarAjax('.response', '', $(this).serialize(), true, true, 'POST', false, false, success);
    } else {
        mensaje = mensaje + 'Por favor seleccione la respuesta correcta porque es obligatorio seleccionar al menos una. <br>';
        verDialogo(mensaje, 'Seleccionar respuesta correcta');
    }
});
// Inactivar la seccion
$('a.inactivarSeccion').each(function() {
    $(this).click(function(evt) {
        evt.preventDefault();

        var seccionId = $(this).attr('data-seccion-id');
        var seccionNombre = $(this).attr('data-seccion-nombre');
        var url = '/registroPrueba/seccionPrueba/inactivacion/id/' + base64_encode(seccionId);
        var container = $(this);

        var buttonsExtra = {
            "html": "<i class='icon-check bigger-110'></i>&nbsp; Aceptar",
            "class": 'btn btn-primary btn-xs',
            "click": function() {
                $('#dialog_error').dialog("close");
                var success = function(response) {
                    response = JSON.parse(response);

                    if (response.filasNoActualizadas == 0 && response.filasAfectadas >= 1) {
                        $('.response').html('<div class="successDialogBox"><p>¡Inactivación exitosa de la sección ' + seccionNombre + '.!</p></div>');
                        container.fadeOut();
                        container.parent().find('a.activarSeccion').fadeIn();
                        container.parent().find('a[data-action="collapse"]').fadeOut();
                        container.parent().parent().parent().addClass('collapsed');
                        $('.subSeccionNombre').attr('readonly', 'readonly');
                        $('.subSeccionDuracion').attr('readonly', 'readonly');
                        $('.preguntaNombre').attr('readonly', 'readonly');
                        $('.preguntaPonderacion').attr('readonly', 'readonly');
                        $('.respuesta').attr('readonly', 'readonly');
                        $('.respuestaCorrecta').attr('readonly', 'readonly');
                    } else {
                        $('.response').html('<div class="errorDialogBox"><p>¡Ocurrió un error al tratar de inactivar la sección ' + seccionNombre + '.!</p><p>Intente de nuevo más tarde.</p></div>');
                        console.log(response);
                    }
                };
                ejecutarAjax('.response', url, true, true, false, 'POST', false, false, success);
                scrollUp('fast');
            }
        };
        verDialogo('¿Realmente desea inactivar la sección ' + seccionNombre + '?', 'Inactivar sección', 'alert', false, false, buttonsExtra);
    });
});
// Activar la seccion
$('a.activarSeccion').each(function() {
    $(this).click(function(evt) {
        evt.preventDefault();

        var seccionId = $(this).attr('data-seccion-id');
        var seccionNombre = $(this).attr('data-seccion-nombre');
        var url = '/registroPrueba/seccionPrueba/activacion/id/' + base64_encode(seccionId);
        var container = $(this);

        var buttonsExtra = {
            "html": "<i class='icon-check bigger-110'></i>&nbsp; Aceptar",
            "class": 'btn btn-primary btn-xs',
            "click": function() {
                $('#dialog_error').dialog("close");
                var success = function(response) {
                    response = JSON.parse(response);

                    if (response.filasNoActualizadas == 0 && response.filasAfectadas >= 1) {
                        $('.response').html('<div class="successDialogBox"><p>¡Activación exitosa de la sección ' + seccionNombre + '.!</p></div>');
                        container.fadeOut();
                        container.parent().find('a.inactivarSeccion').fadeIn();
                        container.parent().find('a[data-action="collapse"]').fadeIn();
                        container.parent().parent().parent().removeClass('collapsed');
                        $('.subSeccionNombre').removeAttr('readonly');
                        $('.subSeccionDuracion').removeAttr('readonly');
                        $('.preguntaNombre').removeAttr('readonly');
                        $('.preguntaPonderacion').removeAttr('readonly');
                        $('.respuesta').removeAttr('readonly');
                        $('.respuestaCorrecta').removeAttr('readonly');
                    } else {
                        $('.response').html('<div class="errorDialogBox"><p>¡Ocurrió un error al tratar de activar la sección ' + seccionNombre + '.!</p><p>Intente de nuevo más tarde.</p></div>');
                        console.log(response);
                    }
                };
                ejecutarAjax('.response', url, true, true, false, 'POST', false, false, success);
                scrollUp('fast');

            }
        };
        verDialogo('¿Realmente desea activar la sección ' + seccionNombre + '?', 'Activar sección', 'alert', false, false, buttonsExtra);
    });
});
// Inactivar subseccion
$('a.inactivarSubSeccion').each(function() {
    $(this).click(function(evt) {
        evt.preventDefault();

        var subSeccionId = $(this).attr('data-sub-seccion-id');
        var subSeccionNombre = $(this).attr('data-sub-seccion-nombre');
        var url = '/registroPrueba/subSeccionPrueba/inactivacion/id/' + base64_encode(subSeccionId);
        var container = $(this);

        var buttonsExtra = {
            "html": "<i class='icon-check bigger-110'></i>&nbsp; Aceptar",
            "class": 'btn btn-primary btn-xs',
            "click": function() {
                $('#dialog_error').dialog("close");
                var success = function(response) {
                    response = JSON.parse(response);

                    if (response.filasNoActualizadas == 0 && response.filasAfectadas >= 1) {
                        $('.response').html('<div class="successDialogBox"><p>¡Inactivación exitosa de la sub-sección ' + subSeccionNombre + '.!</p></div>');
                        container.fadeOut();
                        container.parent().find('a.activarSubSeccion').fadeIn();
                        container.parent().find('a[data-action="collapse"]').fadeOut();
                        container.parent().parent().parent().addClass('collapsed');
                        $('.subSeccionNombre').attr('readonly', 'readonly');
                        $('.subSeccionDuracion').attr('readonly', 'readonly');
                        $('.preguntaNombre').attr('readonly', 'readonly');
                        $('.preguntaPonderacion').attr('readonly', 'readonly');
                        $('.respuesta').attr('readonly', 'readonly');
                        $('.respuestaCorrecta').attr('readonly', 'readonly');
                    } else {
                        $('.response').html('<div class="errorDialogBox"><p>¡Ocurrió un error al tratar de inactivar la sub-sección ' + subSeccionNombre + '.!</p><p>Intente de nuevo más tarde.</p></div>');
                        console.log(response);
                    }
                };
                ejecutarAjax('.response', url, true, true, false, 'POST', false, false, success);
                scrollUp('fast');
            }
        };
        verDialogo('¿Realmente desea inactivar la subsección ' + subSeccionNombre + '?', 'Inactivar subsección', 'alert', false, false, buttonsExtra);
    });
});
// Activar subseccion
$('a.activarSubSeccion').each(function() {
    $(this).click(function(evt) {
        evt.preventDefault();

        var subSeccionId = $(this).attr('data-sub-seccion-id');
        var subSeccionNombre = $(this).attr('data-sub-seccion-nombre');
        var url = '/registroPrueba/subSeccionPrueba/activacion/id/' + base64_encode(subSeccionId);
        var container = $(this);

        var buttonsExtra = {
            "html": "<i class='icon-check bigger-110'></i>&nbsp; Aceptar",
            "class": 'btn btn-primary btn-xs',
            "click": function() {
                $('#dialog_error').dialog("close");
                var success = function(response) {
                    response = JSON.parse(response);

                    if (response.filasNoActualizadas == 0 && response.filasAfectadas >= 1) {
                        $('.response').html('<div class="successDialogBox"><p>¡Activación exitosa de la sub-sección ' + subSeccionNombre + '.!</p></div>');
                        container.fadeOut();
                        container.parent().find('a.inactivarSubSeccion').fadeIn();
                        container.parent().find('a[data-action="collapse"]').fadeIn();
                        container.parent().parent().parent().removeClass('collapsed');
                        $('.subSeccionNombre').removeAttr('readonly');
                        $('.subSeccionDuracion').removeAttr('readonly');
                        $('.preguntaNombre').removeAttr('readonly');
                        $('.preguntaPonderacion').removeAttr('readonly');
                        $('.respuesta').removeAttr('readonly');
                        $('.respuestaCorrecta').removeAttr('readonly');
                    } else {
                        $('.response').html('<div class="errorDialogBox"><p>¡Ocurrió un error al tratar de activar la sub-sección ' + subSeccionNombre + '.!</p><p>Intente de nuevo más tarde.</p></div>');
                        console.log(response);
                    }
                };
                ejecutarAjax('.response', url, true, true, false, 'POST', false, false, success);
                scrollUp('fast');
            }
        };
        verDialogo('¿Realmente desea activar la subsección ' + subSeccionNombre + '?', 'Activar subsección', 'alert', false, false, buttonsExtra);
    });
});
// Inactivar pregunta
$('a.inactivarPregunta').each(function() {
    $(this).click(function(evt) {
        evt.preventDefault();

        var preguntaId = $(this).attr('data-pregunta-id');
        var preguntaNombre = $(this).attr('data-pregunta-nombre');
        var url = '/registroPrueba/pregunta/inactivacion/id/' + base64_encode(preguntaId);
        var container = $(this);

        var buttonsExtra = {
            "html": "<i class='icon-check bigger-110'></i>&nbsp; Aceptar",
            "class": 'btn btn-primary btn-xs',
            "click": function() {
                $('#dialog_error').dialog("close");
                var success = function(response) {
                    response = JSON.parse(response);

                    if (response.filasNoActualizadas == 0 && response.filasAfectadas >= 1) {
                        $('.response').html('<div class="successDialogBox"><p>¡Inactivación exitosa de la pregunta ' + preguntaNombre + '.!</p></div>');
                        container.fadeOut();
                        container.parent().find('a.activarPregunta').fadeIn();
                        container.parent().find('a[data-action="collapse"]').fadeOut();
                        container.parent().parent().parent().addClass('collapsed');
                        $('.preguntaNombre').attr('readonly', 'readonly');
                        $('.preguntaPonderacion').attr('readonly', 'readonly');
                        $('.respuesta').attr('readonly', 'readonly');
                        $('.respuestaCorrecta').attr('readonly', 'readonly');
                    } else {
                        $('.response').html('<div class="errorDialogBox"><p>¡Ocurrió un error al tratar de inactivar la pregunta ' + preguntaNombre + '.!</p><p>Intente de nuevo más tarde.</p></div>');
                        console.log(response);
                    }
                };
                ejecutarAjax('.response', url, true, true, false, 'POST', false, false, success);
                scrollUp('fast');
            }
        };
        verDialogo('¿Realmente desea inactivar la pregunta ' + preguntaNombre + '?', 'Activar subsección', 'alert', false, false, buttonsExtra);
    });
});
// Activar pregunta
$('a.activarPregunta').each(function() {
    $(this).click(function(evt) {
        evt.preventDefault();

        var preguntaId = $(this).attr('data-pregunta-id');
        var preguntaNombre = $(this).attr('data-pregunta-nombre');
        var url = '/registroPrueba/pregunta/activacion/id/' + base64_encode(preguntaId);
        var container = $(this);

        var buttonsExtra = {
            "html": "<i class='icon-check bigger-110'></i>&nbsp; Aceptar",
            "class": 'btn btn-primary btn-xs',
            "click": function() {
                $('#dialog_error').dialog("close");
                var success = function(response) {
                    response = JSON.parse(response);

                    if (response.filasNoActualizadas == 0 && response.filasAfectadas >= 1) {
                        $('.response').html('<div class="successDialogBox"><p>¡Activación exitosa de la pregunta ' + preguntaNombre + '.!</p></div>');
                        container.fadeOut();
                        container.parent().find('a.inactivarPregunta').fadeIn();
                        container.parent().find('a[data-action="collapse"]').fadeIn();
                        container.parent().parent().parent().removeClass('collapsed');
                        $('.preguntaNombre').removeAttr('readonly');
                        $('.preguntaPonderacion').removeAttr('readonly');
                        $('.respuesta').removeAttr('readonly');
                        $('.respuestaCorrecta').removeAttr('readonly');
                    } else {
                        $('.response').html('<div class="errorDialogBox"><p>¡Ocurrió un error al tratar de activar la pregunta ' + preguntaNombre + '.!</p><p>Intente de nuevo más tarde.</p></div>');
                        console.log(response);
                    }
                };
                ejecutarAjax('.response', url, true, true, false, 'POST', false, false, success);
                scrollUp('fast');
            }
        };
        verDialogo('¿Realmente desea activar la pregunta ' + preguntaNombre + '?', 'Activar subsección', 'alert', false, false, buttonsExtra);
    });
});

/** Validaciones **/
$(document).ready(function() {
    $('.seccionNombre, .subSeccionNombre, .preguntaNombre, .respuesta').unbind();
    $('.seccionNombre, .subSeccionNombre, .preguntaNombre, .respuesta').bind('keyup blur', function() {
        keyAlphaNum(this, true, false);
        makeUpper(this);
    });

    $('.preguntaPonderacion').unbind();
    $('.preguntaPonderacion').bind('keyup blur', function() {
        keyNum(this, false, false);
    });

    $.mask.definitions['H'] = '[0-1-2]';
    $.mask.definitions['Y'] = '[0|1|2|3]';
    $.mask.definitions['M'] = '[0|1|2|3|4|5]';
    $.mask.definitions['~'] = '[+-]';

    $('.seccionDuracion, .subSeccionDuracion').mask("H9:M9:M9");

    $('.seccionDuracion').unbind('blur');
    $('.seccionDuracion').bind(' blur', function() {
        var mensaje = '<b>Estimado usuario recuerde que el formato del tiempo de duración de la subsección debe ser HH:MM:SS ejemplo 00:00:00, por favor intente nuevamente.</b>';
        var title = 'Formato de hora';
        var duracion = $(this).val();
        var valor = new RegExp(/^\d{2}:\d{2}:\d{2}$/);
        var valid = valor.test(duracion);
        var tam;
        var tiempo = new Array();
        tiempo = duracion.split(":");
        var hora = tiempo[0];
        var minuto = tiempo[1];
        var segundo = tiempo[2];

        if (duracion != '')
            tam = duracion.length;
        if (!valid) {
            if (duracion != '' && duracion != null && duracion != '__:__:__') {
                $(this).val('');
                verDialogo(mensaje, title);
            }
        } else {
            if (duracion != '' && duracion != null) {
                if (tam == 8) {
                    if (hora <= 23) {
                        if (minuto <= 59) {
                            if (segundo <= 59) {
                                if (duracion == '00:00:00') {
                                    var mensaje = '<b>Estimado usuario el tiempo de duración ingresado es inválido, por favor intente nuevamente.</b>';
                                    $(this).val('');
                                    verDialogo(mensaje, title);
                                } else {
                                    var duracion_seccion = $('#duracion_seccion').val();
                                    if (duracion_seccion != duracion) {
                                        $('.subSeccionDuracion').val('');
                                        $('.widgetSubSeccion').removeClass('collapsed');
                                        verDialogo('Por favor ingrese la duracción de las demas sub-secciones', 'Duracción Sub-Secciones');
                                    }
                                }
                            } else {
                                var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 59 segundos, por favor intente nuevamente.</b>';
                                $(this).val('');
                                verDialogo(mensaje, title);
                            }
                        } else {
                            var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 59 minutos, por favor intente nuevamente.</b>';
                            $(this).val('');
                            verDialogo(mensaje, title);
                        }
                    } else {
                        var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 23 horas, por favor intente nuevamente.</b>';
                        $(this).val('');
                        verDialogo(mensaje, title);
                    }
                } else {
                    var mensaje = '<b>Estimado usuario el tamaño de la duración ingresado es maximo de 8 digitos, por favor intente nuevamente.</b>';
                    $(this).val('');
                    verDialogo(mensaje, title);
                }
            }
        }
    });

    $('.subSeccionDuracion').unbind('blur');
    $('.subSeccionDuracion').bind(' blur', function() {
        var mensaje = '<b>Estimado usuario recuerde que el formato del tiempo de duración de la subsección debe ser HH:MM:SS ejemplo 00:00:00, por favor intente nuevamente.</b>';
        var title = 'Formato de hora';
        var duracion = $(this).val();
        var valor = new RegExp(/^\d{2}:\d{2}:\d{2}$/);
        var valid = valor.test(duracion);
        var tam;
        var tiempo = new Array();
        tiempo = duracion.split(":");
        var hora = tiempo[0];
        var minuto = tiempo[1];
        var segundo = tiempo[2];

        if (duracion != '')
            tam = duracion.length;
        if (!valid) {
            if (duracion != '' && duracion != null && duracion != '__:__:__') {
                $(this).val('');
                verDialogo(mensaje, title);
            }
        } else {
            if (duracion != '' && duracion != null) {
                if (tam == 8) {
                    if (hora <= 23) {
                        if (minuto <= 59) {
                            if (segundo <= 59) {
                                if (duracion == '00:00:00') {
                                    var mensaje = '<b>Estimado usuario el tiempo de duración ingresado es inválido, por favor intente nuevamente.</b>';
                                    $(this).val('');
                                    verDialogo(mensaje, title);
                                }
                            } else {
                                var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 59 segundos, por favor intente nuevamente.</b>';
                                $(this).val('');
                                verDialogo(mensaje, title);
                            }
                        } else {
                            var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 59 minutos, por favor intente nuevamente.</b>';
                            $(this).val('');
                            verDialogo(mensaje, title);
                        }
                    } else {
                        var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 23 horas, por favor intente nuevamente.</b>';
                        $(this).val('');
                        verDialogo(mensaje, title);
                    }
                } else {
                    var mensaje = '<b>Estimado usuario el tamaño de la duración ingresado es maximo de 8 digitos, por favor intente nuevamente.</b>';
                    $(this).val('');
                    verDialogo(mensaje, title);
                }
            }
        }

        var horasSubSecciones = new Array();
        var cantidadSubSecciones = Number($('#Seccion_cantidad_sub_seccion').val());
        var duracionSeccion = $('.seccionDuracion').val();
        var duracion_seccion = $('#duracion_seccion').val();

        $('.subSeccionDuracion').each(function() {
            var duracionActual = $(this).val();
            if (duracionActual != '' && duracionActual != '__:__:__') {

                if (duracionActual != '00:00:00') {
                    if (HTO.stringToSeconds(duracionActual) >= HTO.stringToSeconds(duracionSeccion) && cantidadSubSecciones > 1) {
                        verDialogo('La duración de la subsección no puede ser mayor o igual que la duración de sección.', 'Duración de subsección');
                        displayDialogBoxSelector('.errorValidacionSubSeccion', 'info', 'Recuerde que la suma de cada duración de subsección es la duración de sección que es: ' + duracionSeccion, $(this).parent().parent().parent().parent().parent());
                        $(this).addClass('error').val('').focus();
                    } else if ($('.subSeccionDuracion:last') != $(this) && (HTO.stringToSeconds(duracionActual) == HTO.stringToSeconds(duracionSeccion) && cantidadSubSecciones > 1)) {
                        verDialogo('La duración de la subsección debe ser menor que la duración restante de la sección.', 'Duración de subsección');
                        displayDialogBoxSelector('.errorValidacionSubSeccion', 'info', 'Recuerde que la suma de cada duración de subsección es la duración de sección que es: ' + duracionSeccion, $(this).parent().parent().parent().parent().parent() + '<br>' + 'Duración faltante: ' + (HTO.sumTimes(horasSubSecciones) - HTO.stringToSeconds(duracionActual)));
                        $(this).addClass('error').val('').focus();
                    } else if (HTO.stringToSeconds(duracionActual) != HTO.stringToSeconds(duracionSeccion) && cantidadSubSecciones == 1) {
                        verDialogo('La duración de la subsección no puede ser diferente que la duración de la sección.', 'Duración de subsección', 'error');
                        $(this).val(duracionSeccion).focus();
                    } else {
                        horasSubSecciones.push($(this).val());
                        $(this).removeClass('error');
                    }
                } else {
                    var mensaje = '<b>Estimado usuario el tiempo de duración ingresado es inválido, por favor intente nuevamente.</b>';
                    $(this).val('').focus();
                    verDialogo(mensaje, title);
                }
            }
        });

        // Validar que la suma de la duración de las subsecciones sea igual que la duración de la sección
        // Sólo si la cantidad de subsecciones es mayor a 1 cantidadSubSecciones.length
        if (horasSubSecciones.length == 2) {
            var suma = HTO.sumTimes(horasSubSecciones);
            if (HTO.stringToSeconds(suma) < HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length == cantidadSubSecciones) {
                verDialogo('La suma de las duraciones de subsecciones no puede ser menor a la duración de la sección.', 'Duración de subsección');
                var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                // Agregar el valor de la duración actual

                if (horasSubSecciones.length == cantidadSubSecciones) {
                    $(this).val(HTO.secondsToTime(duracionValida));
                } else {
                    $(this).val('');
                }
                $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
            }
            if (HTO.stringToSeconds(suma) > HTO.stringToSeconds(duracionSeccion)) {
                verDialogo('La suma de las duraciones de subsecciones no puede ser mayor a la duración de la sección.', 'Duración de subsección');
                var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                // Agregar el valor de la duración actual
                if (horasSubSecciones.length == cantidadSubSecciones) {
                    $(this).val(HTO.secondsToTime(duracionValida));
                } else {
                    $(this).val('');
                }
                $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
            }
            if (HTO.stringToSeconds(suma) != HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length == cantidadSubSecciones) {
                verDialogo('La suma de las duraciones de subsecciones no es igual a la duración de la sección.', 'Duración de subsección');
                // Calcular la duración válida para la subsección actual

                var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                // Agregar el valor de la duración actual
                // if (HTO.secondsToTime(duracionValida) > 0)
                $(this).val(HTO.secondsToTime(duracionValida));
                // Permitir edición de la duración de las subsecciones
                $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
            } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length == cantidadSubSecciones) {
                $(this).removeClass('error');
            } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length < cantidadSubSecciones) {
                verDialogo('La suma de las duraciones de subsecciones debe coincidir con la cantidad de subsecciones creadas.', 'Duración de subsección');
                $(this).val('');
                $(this).addClass('error');
            }
        } else if (horasSubSecciones.length > 2) {
            var suma = HTO.sumTimes(horasSubSecciones);
            if (HTO.stringToSeconds(suma) < HTO.stringToSeconds(duracionSeccion)) {
                verDialogo('La suma de las duraciones de subsecciones no puede ser menor a la duración de la sección.', 'Duración de subsección');
                var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                // Agregar el valor de la duración actual

                $(this).val(HTO.secondsToTime(duracionValida));
                $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
            }
            if (HTO.stringToSeconds(suma) > HTO.stringToSeconds(duracionSeccion)) {
                verDialogo('La suma de las duraciones de subsecciones no es igual a la duración de la sección.', 'Duración de subsección');
                var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                // Agregar el valor de la duración actual
                // if (HTO.secondsToTime(duracionValida) > 0)
                $(this).val(HTO.secondsToTime(duracionValida));
                $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
            } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && $(this) === $('.subSeccionDuracion:last')) {
                $(this).removeClass('error');
            } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length < cantidadSubSecciones) {
                verDialogo('La suma de las duraciones de subsecciones debe coincidir con la cantidad de subsecciones creadas.', 'Duración de subsección');
                $(this).val('');
                $(this).addClass('error');
            }
        }


    });

    // Validar que no se repitan elementos
    $('.seccionNombre').bind('blur', function() {
        var seccion = $(this).val();
        var seccionArray = new Array();
        var tam;

        $('.seccionNombre').each(function() {
            if ($(this).val() != '') {
                seccionArray.push($(this).val());
            }
        });

        if (seccionArray != '')
            tam = seccionArray.length;

        if (seccion != '' && tam > 1) {
            var resultado = validarSeccionDuplicada(seccion, seccionArray);
            if (resultado == true) {
                var mensaje = 'Por favor ingrese otra sección para esta prueba porque ya fue ingresada.';
                var title = 'Subsección ya existe';
                $(this).val('');
                verDialogo(mensaje, title, 'error');
            }
        }
    });

    $('.widgetSeccion').each(function() {
        $('.subSeccionNombre', this).bind('blur', function() {
            var subSeccion = $(this).val();
            var subSeccionArray = new Array();
            var tam;

            $('.subSeccionNombre').each(function() {
                if ($(this).val() != '') {
                    subSeccionArray.push($(this).val());
                }
            });
            if (subSeccionArray != '')
                tam = subSeccionArray.length;

            if (subSeccion != '' && tam > 1) {
                var resultado = validarSubSeccionDuplicada(subSeccion, subSeccionArray);

                if (resultado == true) {
                    var mensaje = 'Por favor ingrese otra subsección para esta sección porque ya fue ingresada';
                    var title = 'Subsección ya existe';
                    $(this).val('');
                    verDialogo(mensaje, title, 'error');
                }
            }
        });

        var widgetSeccion = this;

        $('.widgetSeccionPregunta', widgetSeccion).each(function() {
            $('.preguntaNombre', widgetSeccion).blur(function() {
                var pregunta = $(this).val();
                var preguntaArray = new Array();
                var tam;
                $('.preguntaNombre', widgetSeccion).each(function() {
                    if ($(this).val() != '') {
                        preguntaArray.push($(this).val());
                    }
                });

                if (preguntaArray != '')
                    tam = preguntaArray.length;

                if (pregunta != '' && tam > 1) {
                    var resultado = validarPreguntaDuplicada(pregunta, preguntaArray);
                    if (resultado == true) {
                        var mensaje = 'Por favor ingrese otra pregunta para esta sección porque ya fue ingresada';
                        var title = 'Pregunta ya existe';
                        $(this).val('');
                        verDialogo(mensaje, title, 'error');
                    }
                }
            });
            $('.preguntaPonderacion', widgetSeccion).blur(function() {
                if ($(this).val() > 3) {
                    var mensaje = 'La ponderación de la pregunta debe ser entre 1-3';
                    var title = 'Ponderación inválida';
                    $(this).val('').focus();
                    verDialogo(mensaje, title, 'error');
                }
            });
        });
    });

    $('.widgetSubSeccion').each(function() {
        var widgetSubSeccion = this;
        $('.preguntaNombre', widgetSubSeccion).blur(function() {
            var pregunta = $(this).val();
            var preguntaArray = new Array();
            var tam;
            $('.preguntaNombre', widgetSubSeccion).each(function() {
                if ($(this).val() != '') {
                    preguntaArray.push($(this).val());
                }
            });

            if (preguntaArray != '')
                tam = preguntaArray.length;

            if (pregunta != '' && tam > 1) {
                var resultado = validarPreguntaDuplicada(pregunta, preguntaArray);

                if (resultado == true) {
                    var mensaje = 'Por favor ingrese otra pregunta para esta subsección porque ya fue ingresada';
                    var title = 'Pregunta ya existe';
                    $(this).val('');
                    verDialogo(mensaje, title, 'error');
                }
            }
        });
        $('.preguntaPonderacion', widgetSubSeccion).blur(function() {
            if ($(this).val() > 3) {
                var mensaje = 'La ponderación de la pregunta debe ser entre 1-3';
                var title = 'Ponderación inválida';
                $(this).val('').focus();
                verDialogo(mensaje, title);
            }
        });
    });

    $('.widgetSubSeccionPregunta, .widgetSeccionPregunta').each(function() {
        var widgetPregunta = this;
        $('.respuesta').blur(function() {
            var respuesta = $(this).val();
            var respuestaArray = new Array();
            var tam;
            $('.respuesta', widgetPregunta).each(function() {
                if ($(this).val() != '') {
                    respuestaArray.push($(this).val());
                }
            });
            if (respuestaArray != '')
                tam = respuestaArray.length;

            if (respuesta != '' && tam > 1) {
                var resultado = validarRespuestaDuplicada(respuesta, respuestaArray);
                if (resultado == true) {
                    var mensaje = 'Por favor ingrese otra respuesta para esta pregunta porque ya fue ingresada';
                    var title = 'Respuesta ya existe';
                    $(this).val('');
                    verDialogo(mensaje, title, 'error');
                }
            }
        });
    });

});