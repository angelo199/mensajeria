$(document).ready(function() {
    $('#prueba_nombre_form').bind('keyup blur', function() {
        keyAlphaNum(this, true, true);// Valida que contenga números, letras y espacios
        makeUpper(this);//Convierte las letras en mayusculas
    });

    $('#instruccion_prueba_form').bind('keyup blur', function() {
        keyText(this, true);
        makeUpper(this);//Convierte las letras en mayusculas
    });

    $('#nombre_prueba_grid').bind('keyup blur', function() {
        keyAlphaNum(this, true, true);// Valida que contenga números, letras y espacios
        makeUpper(this);//Convierte las letras en mayusculas
    });
});




$("#btnTipoPrueba").unbind('click');
$("#btnTipoPrueba").click(function(e) {
    e.preventDefault();
    var prueba_id = $('#Prueba_id').val();
    var url = '';
    if (prueba_id != '') {
        url = "/prueba/tipoPrueba/modificarPrueba/id/" + prueba_id;
    } else if (prueba_id == '') {
        url = "/prueba/tipoPrueba/registro";
    }
    $.ajax({
        url: url,
        data: $("#prueba_form").serialize(),
        dataType: 'html',
        type: 'post',
        success: function(resp, resp2, resp3) {

            try {
                var json = jQuery.parseJSON(resp3.responseText);

                if (json.statusCode == "success") {

                    $("#msj_error").addClass('hide');
                    $("#msj_error p").html('');
                    $("#msj_exitoso").removeClass('hide');
                    $("#msj_exitoso p").html(json.mensaje);
                    $("html, body").animate({scrollTop: 0}, "fast");
                }

                if (json.statusCode == "error") {

                    $("#msj_exitoso").addClass('hide');
                    $("#msj_exitoso p").html('');
                    $("#msj_error").removeClass('hide');
                    $("#msj_error p").html(json.mensaje);
                    $("html, body").animate({scrollTop: 0}, "fast");
                }
            }
            catch (e) {
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        }
    });

});




$(".add-consultar").unbind('click');
$(".add-consultar").on('click', function(e) {
    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    consultar(registro_id);
});


function consultar(registro_id) {

    var data = {
        id: registro_id
    };

    $.ajax({
        url: "/prueba/tipoPrueba/consultarPrueba",
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(resp, resp2, resp3)
        {
            var dialog_consultar = $("#dialog_consultar").removeClass('hide').dialog({
                modal: true,
                width: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='icon-list bigger-110'></i>&nbsp; Detalles del Registro de Historial Médico</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            dialog_consultar.dialog("close");
                        }
                    }
                ]
            });

            try {
                var json = jQuery.parseJSON(resp3.responseText);

                if (json.statusCode == "error") {

                    $("#msj_error_consulta").removeClass('hide');
                    $("#msj_error_consulta p").html(json.mensaje);
                    $("html, body").animate({scrollTop: 0}, "fast");
                }
            }
            catch (e) {

                $("#msj_error_consulta").addClass('hide');
                $("#msj_error_consulta p").html('');
                $("#dialog_consultar").html(resp);
                $("html, body").animate({scrollTop: 0}, "fast");
            }
        }
    });
}






$(".add-inactivar").unbind('click');
$(".add-inactivar").on('click', function(e) {
    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    inactivar(registro_id);
});

function inactivar(registro_id) {

    $("#pregunta_inactivar").removeClass('hide');
    $("#msj_error_inactivar").addClass('hide');
    $("#msj_error_inactivar p").html('');

    var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Prueba</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: registro_id
                    };
                    $.ajax({
                        url: "/prueba/tipoPrueba/inactivarPrueba",
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == "success") {

                                    refrescarGrid();
                                    dialogInactivar.dialog('close');
                                    $("#msj_error_inactivar").addClass('hide');
                                    $("#msj_error_inactivar p").html('');
                                    $("#msj_exitoso").removeClass('hide');
                                    $("#msj_exitoso p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }

                                if (json.statusCode == "error") {

                                    $("#pregunta_inactivar").addClass('hide');
                                    $("#msj_exitoso").addClass('hide');
                                    $("#msj_exitoso p").html('');
                                    $("#msj_error_inactivar").removeClass('hide');
                                    $("#msj_error_inactivar p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_inactivacion").show();
}




$(".add-activar").unbind('click');
$(".add-activar").on('click', function(e) {

    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr("data");

    activar(registro_id);

});


function activar(registro_id) {

    $("#pregunta_activar").removeClass('hide');
    $("#msj_error_activar").addClass('hide');
    $("#msj_error_activar p").html('');

    var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de Prueba</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: registro_id
                    }

                    $.ajax({
                        url: "/prueba/tipoPrueba/activarPrueba",
                        data: data,
                        dataType: 'html',
                        type: 'get',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == "success") {

                                    refrescarGrid();
                                    dialogActivar.dialog('close');
                                    $("#msj_error_activar").addClass('hide');
                                    $("#msj_error_activar p").html('');
                                    $("#msj_exitoso").removeClass('hide');
                                    $("#msj_exitoso p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }

                                if (json.statusCode == "error") {

                                    $("#pregunta_activar").addClass('hide');
                                    $("#msj_exitoso").addClass('hide');
                                    $("#msj_exitoso p").html('');
                                    $("#msj_error_activar").removeClass('hide');
                                    $("#msj_error_activar p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_activacion").show();
}





function refrescarGrid() {

    $('#prueba-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}