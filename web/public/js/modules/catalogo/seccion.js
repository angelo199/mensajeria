$(document).ready(function()
{


    $('#SeccionPrueba_tipo_pregunta').change(function()
    {
        var variable = $('#SeccionPrueba_tipo_pregunta').val();
        if (variable == 'D') {
            $('#SeccionPrueba_cantidad_respuesta').prop("readOnly", true);
            $('#SeccionPrueba_cantidad_respuesta').val(2);
        }
        else {
            $('#SeccionPrueba_cantidad_respuesta').prop("readOnly", false);
            $('#SeccionPrueba_cantidad_respuesta').val('');
        }
    });
    function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }

    // VALIDACIONES DEL CGRIDVIEW
    $('#nombre').bind('keyup blur', function() {
        keyAlphaNum(this, true, true);
        makeUpper(this); //Convierte las palabras en mayusculas.
    });
    $.mask.definitions['H'] = '[0-1-2]';
    $.mask.definitions['Y'] = '[0|1|2|3]';
    $.mask.definitions['M'] = '[0|1|2|3|4|5]';
    $.mask.definitions['~'] = '[+-]';
    $.mask.definitions['n'] = '[0123]'
    $("#duracion_seccion").mask("Hn:M9:M9"); // Filtrar el campo "duracion_seccion" del CGGridView

    // VALIDACIONES DEL FORMULARIO SeccionPrueba.
    $('#SeccionPrueba_nombre').bind('keyup blur', function() {
        keyAlphaNum(this, true, true); // Validar que solo sea Letras, Numeros y Espacios.
        makeUpper(this); //Convierte las palabras en mayusculas.
    });
    $("#SeccionPrueba_duracion_seccion").mask("H9:M9:M9"); // Valida el Campo "duracion_seccion" del formulario.

    $('#SeccionPrueba_cantidad_respuesta').bind('keyup blur', function() {
        keyNum(this, true, true); // validar Solo Numero
    });

    $('#SeccionPrueba_cantidad_sub_seccion').bind('keyup blur', function() {
        keyNum(this, true, true); // validar Solo Numero
    });
    $('#SeccionPrueba_cantidad_sub_seccion').blur(function() {
        var cantidad_sub_secciones = $('#SeccionPrueba_cantidad_sub_seccion').val();
        //$('#SeccionPrueba_prueba_id').val();
        var seccion_prueba_id = {id: $('#SeccionPrueba_id').val()};
        $.ajax({
            url: "/prueba/Seccion/obtenerDatos",
            data: seccion_prueba_id,
            dataType: 'html',
            type: 'post',
            success: function(json)
            {
                var json = jQuery.parseJSON(json);
                if (json.statusCode == "success")
                {
                    var totalSubSecciones = json.totalSubSecciones;
                    if (cantidad_sub_secciones < totalSubSecciones)
                    {
                        var mensaje = '<b>Estimado usuario: ya existen ' + totalSubSecciones + ' Registros Asociados a la Sección Ingresada, Ingrese una Cantidad Superior e Intente Nuevamente.</b>';
                        var title = 'Cantidad de Sub-Secciones';
                        $('#SeccionPrueba_cantidad_sub_seccion').val('');
                        verDialogo(mensaje, title);
                    }
                    if (cantidad_sub_secciones == '')
                    {
                        var mensaje = '<b>Estimado Usuario Ingrese un Numero valido</b>';
                        var title = 'Cantidad de Sub-Secciones';
                        $('#SeccionPrueba_cantidad_sub_seccion').val('');
                        verDialogo(mensaje, title);
                    }
                    //alert(json.totalSubSecciones); 
                }
            }
        });
    });


    //$("#SeccionPrueba_instruccion_seccion").
    $('#SeccionPrueba_instruccion_seccion').bind('keyup blur', function() {
        keyText(this, true, true); // validar Solo texto
        makeUpper(this);
    });

    $("#seccion-prueba-form").submit(function() {
        var tieneSeccion = $('input[name="tieneSubSeccion"]:checked').val();
        //var tieneSeccionSi = $('#tieneSubSeccionSi').val();
        //var tieneSeccionNo = $('#tieneSubSeccionNo').val();
        var instrucciones = $("#instruccion_seccion1").html();
        var instrucciones2 = $("#instruccion_seccion2").html();
        var tipoPrueba = $("#SeccionPrueba_tipo_pregunta").val();
        var cantidadSeccion = $('#SeccionPrueba_cantidad_sub_seccion').val();
        var duracionSeccion = $('#SeccionPrueba_duracion_seccion').val();
        var cantidadRespuesta = $('#SeccionPrueba_cantidad_respuesta').val();
        //alert(tieneSeccion);
        //alert(tieneSeccion);
//        alert(instrucciones);
//        alert(instrucciones2);
        //alert(tipoPrueba);
        var mensaje = '<b>Estimado usuario Asegurese de Llenar los Campos Correspondientes, por favor intente nuevamente.</b>';
        var title = 'Campos Vacios';
        if (tieneSeccion == 'si')
        {
            // alert(cantidadSeccion);
            if (cantidadSeccion == '')
            {
                // alert('pude entrar aqui1');
                verDialogo(mensaje, title);
                return false;
            }

        }
        if (tieneSeccion == 'no')
        {
            //alert(instrucciones);

            if (tipoPrueba == '' || cantidadRespuesta == '')
            {
                //alert('pude entrar aqui2');
                document.getElementById('instruccion_seccion1').style.border = '1px solid ##ff3333';
                verDialogo(mensaje, title);
                return false;
            }

            if (instrucciones == '' && instrucciones2 == '')
            {
                // alert('pude entrar aqui3');
                document.getElementById('instruccion_seccion1').style.border = '1px solid ##ff3333';
                verDialogo(mensaje, title);
                return false;
            } else {
                document.getElementById('instruccion_seccion1').style.border = '1px solid #ccc';
            }
            if (duracionSeccion == '' || duracionSeccion == '00:00:00' || duracionSeccion == '__:__:__')
            {
                // alert('pude entrar aqui4');
                var mensaje = '<b>Estimado usuario Asegurese de Llenar la duración de la Sección en un Rango Válido.</b>';
                var title = 'Formato de Duración de Sección';
                verDialogo(mensaje, title);
                return false;
            }
        }

    });
    $('input[name="tieneSubSeccion"]').on('change', function() {
        if ($(this).val() == 'si') {
            $('#tipo_pregunta, #instruccion_seccion,#cantidad_respuesta,#duracion_seccion').hide();
            $('#cantidad_sub_seccion').show();
            $('#instruccion_seccion1').html('');
            $('#instruccion_seccion2').html('');
            $('#SeccionPrueba_cantidad_sub_seccion').val('');
            $('#SeccionPrueba_tipo_pregunta').val('');
            $('#SeccionPrueba_cantidad_respuesta').val('');
            $('#SeccionPrueba_duracion_seccion').val('');
        } else {
            $('#cantidad_sub_seccion').hide();
            $('#tipo_pregunta,#instruccion_seccion,#cantidad_respuesta,#duracion_seccion').show();
            $('#instruccion_seccion1').html('');
            $('#instruccion_seccion2').html('');
            $('#SeccionPrueba_cantidad_sub_seccion').val('');
            $('#SeccionPrueba_tipo_pregunta').val('');
            $('#SeccionPrueba_duracion_seccion').val('');
        }
    });

    $("#SeccionPrueba_duracion_seccion").blur(function() {
        var duracion_seccion = $('#SeccionPrueba_duracion_seccion').val();

        if (duracion_seccion == '00:00:00') {
            var mensaje = '<b>Estimado usuario el tiempo de duración ingresado es inválido, por favor intente nuevamente.</b>';
            var title = 'Formato de hora';
            $("#SeccionPrueba_duracion_seccion").val('');
            verDialogo(mensaje, title);
        }
    });


    $("#SeccionPrueba_cantidad_respuesta").blur(function() {
        var cantidad_respuesta = $('#SeccionPrueba_cantidad_respuesta').val();
        if (cantidad_respuesta < 2) {
            var mensaje = '<b>Estimado usuario la cantidad de Repuestas debe ser mayor a 1, por favor intente nuevamente.</b>';
            var title = 'Cantidad de Repuestas';
            $("#SeccionPrueba_cantidad_respuesta").val('');
            verDialogo(mensaje, title);
        }
    });


    function refrescarGrid() {

        $('#seccion-prueba-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
    }

    function inactivar(registro_id) {
        $("#pregunta_inactivar").removeClass('hide');
        $("#msj_error_inactivar").addClass('hide');
        $("#msj_error_inactivar p").html('');

        var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Sección de Prueba</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var data = {
                            id: registro_id
                        };
                        $.ajax({
                            url: "/prueba/seccion/inactivarSeccionPrueba",
                            data: data,
                            dataType: 'html',
                            type: 'post',
                            success: function(resp, resp2, resp3) {

                                try {
                                    var json = jQuery.parseJSON(resp3.responseText);

                                    if (json.statusCode == "success") {

                                        refrescarGrid();
                                        dialogInactivar.dialog('close');
                                        $("#msj_error_inactivar").addClass('hide');
                                        $("#msj_error_inactivar p").html('');
                                        $("#msj_exitoso").removeClass('hide');
                                        $("#msj_exitoso p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }

                                    if (json.statusCode == "error") {

                                        $("#pregunta_inactivar").addClass('hide');
                                        $("#msj_exitoso").addClass('hide');
                                        $("#msj_exitoso p").html('');
                                        $("#msj_error_inactivar").removeClass('hide');
                                        $("#msj_error_inactivar p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                                catch (e) {

                                }
                            }
                        })
                    }
                }
            ]
        });
        $("#dialog_inactivacion").show();
    }



    $(".editar-datos").unbind('click');
    $(".editar-datos").on('click', function(e) {
        e.preventDefault();

        var registro_id;
        registro_id = $(this).attr("data");
        var data = {id: registro_id};


        //return false;
        $.ajax({
            url: "/prueba/seccion/verificarEstatus",
            data: data,
            dataType: 'html',
            type: 'post',
            success: function(json) {
                try {
                    var json = jQuery.parseJSON(json);

                    if (json.statusCode == "success")
                    {
                        var mensaje = '<b>Estimado Usuario: La Seccion Seleccionada ya ha sido utilizada para las pruebas Correspondientes, No puede editar este Registro</b>';
                        var title = 'Edicion de Seccion';
                        //return false;
                        verDialogo(mensaje, title);
                        //alert('no puede editar');
                    }
                    if (json.statusCode == "error") {
                        //$(".editar-datos").unbind('click');
                        //$(".editar-datos").click();
                        var ruta = json.mensaje;
                        //alert('puede editar');
                        location.href = ruta + "/prueba/seccion/edicion/id/" + registro_id;

                    }
                }
                catch (e) {

                }
            }
        });

        //return false;    
    });

    $(".add-inactivar").unbind('click');
    $(".add-inactivar").on('click', function(e) {
        e.preventDefault();
        var registro_id;
        registro_id = $(this).attr("data");
        inactivar(registro_id);
    });

    function activar(registro_id) {

        $("#pregunta_activar").removeClass('hide');
        $("#msj_error_activar").addClass('hide');
        $("#msj_error_activar p").html('');

        var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de la Sección de Prueba</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                    "class": "btn btn-success btn-xs",
                    click: function() {
                        var data = {
                            id: registro_id
                        }
                        $.ajax({
                            url: "/prueba/seccion/activarSeccionPrueba",
                            data: data,
                            dataType: 'html',
                            type: 'get',
                            success: function(resp, resp2, resp3) {

                                try {
                                    var json = jQuery.parseJSON(resp3.responseText);

                                    if (json.statusCode == "success") {

                                        refrescarGrid();
                                        dialogActivar.dialog('close');
                                        $("#msj_error_activar").addClass('hide');
                                        $("#msj_error_activar p").html('');
                                        $("#msj_exitoso").removeClass('hide');
                                        $("#msj_exitoso p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }

                                    if (json.statusCode == "error") {

                                        $("#pregunta_activar").addClass('hide');
                                        $("#msj_exitoso").addClass('hide');
                                        $("#msj_exitoso p").html('');
                                        $("#msj_error_activar").removeClass('hide');
                                        $("#msj_error_activar p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                                catch (e) {

                                }
                            }
                        })
                    }
                }
            ]
        });
        $("#dialog_activacion").show();
    }


    /*
    $(".add-activar").unbind('click');
    $(".add-activar").on('click', function(e) {

        e.preventDefault();

        var registro_id;
        registro_id = $(this).attr("data");

        activar(registro_id);

    }); */
    
    $('.add-activar').unbind('click');
    $('.add-activar').on('click', function(e) {
    
        e.preventDefault();
        var registro_id = $(this).attr('data');
        var prueba_id = $(this).attr('prueba_id');
        var data = { prueba_id: prueba_id };
        $.ajax({
            url: "/prueba/Seccion/verificarEstatusPueba",
            data: data,
            type: 'post',
            success: function(json) {
                var json = jQuery.parseJSON(json);
                //console.log(json.estatusSeccion);
                if (json.estatusSeccion != 'I') {
                    activar(registro_id);
                }
                else {
                    var title = "Error de Activación";
                    var mensaje = "Estimado usuario, Para Habilitar Esta Sección Habilite la Prueba a la Que esta Asociada e Intente Nuevamente."
                    verDialogo(mensaje, title);
                }
            }
        }); 
        //activar(registro_id); 
    });



    $("#SeccionPrueba_cantidad_sub_seccion").blur(function() {
        var cantidad_respuesta = $('#SeccionPrueba_cantidad_sub_seccion').val();
        if (cantidad_respuesta < 1) {
            var mensaje = '<b>Estimado usuario la cantidad de Sub-Secciones debe ser mayor o igual a 1, por favor intente nuevamente.</b>';
            var title = 'Cantidad de Sub-secciones';
            $("#SeccionPrueba_cantidad_sub_seccion").val('');
            verDialogo(mensaje, title);
        }
    });



});
