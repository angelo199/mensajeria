$(document).ready(function()
{
    function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
        if (!style)
            style = 'info';

        var buttonsDialog = [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                    if (clickCallback) {
                        clickCallback();
                    }
                    if (reload) {
                        window.location.reload();
                    }
                }
            }
        ];

        if (buttonsExtra)
            buttonsDialog.push(buttonsExtra);

        displayDialogBox('dialog_error', style, message);

        var dialog = $("#dialog_error").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: buttonsDialog
        });
    }



// FUNCION QUE PERMITE SUBIR IMAGEN Y MOSTRARLA EN UN DIV.
    $('#imagenes').change(function(e) {
        addImage(e);
    });
    function addImage(e) {
        var file = e.target.files[0];
        imageType = /image.*/;
        if (!file.type.match(imageType))
            return;
        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    }
    function fileOnload(e) {
        var result = e.target.result;
        $('#tumbnailLogo').attr("src", result);
    }
// FINAL DE FUNCION QUE PERMITE SUBIR IMAGEN Y MONTARLA EN DIV.
    $('#imagenes').change(function() {

        var prueba = $('#imagenes').val();
        var extensionPermitida = new Array(".gif", ".jpg", ".jpeg", ".png");
        var extension = (prueba.substring(prueba.lastIndexOf("."))).toLowerCase();
        //alert (extension); 
        var permitida = 0;
        for (var i = 0; i < extensionPermitida.length; i++)
        {
            if (extensionPermitida[i] == extension) {
                permitida = 1;
                //alert('es una imagen aceptada');
                break;
            }
        }
        if (permitida == 1)
        {

        } else {
            var mensaje = '<b>Estimado usuario, Asegurese que el archivo subido sea una imagen en los formatos ".jpg",".jpeg",".png".</b>';
            var title = 'Formata de Archivo';
            $('#tumbnailLogo').attr("src", "/public/images/error/fella-wait.png");
            $('#imagenes').val('');
            verDialogo(mensaje, title);
        }
    });
    
    
    $('#Imagen_titulo_imagen').bind('keyup blur', function()
    {
        keyAlphaNum(this, true, true);
        makeUpper(this);//Convierte las letras en mayusculas
    });
    function refrescarGrid()
    {
        $('#imagen-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
    }
    $('#titulo_imagen').bind('keyup blur', function() {
        keyAlphaNum(this, true, true); // Validar que solo sea Letras, Numeros y Espacios.
        makeUpper(this); //Convierte las palabras en mayusculas.
    });
    function inactivar(registro_id) {
        $("#pregunta_inactivar").removeClass('hide');
        $("#msj_error_inactivar").addClass('hide');
        $("#msj_error_inactivar p").html('');

        var dialogInactivar = $("#dialog_inactivacion").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Inactivar Registro de Imagen</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var data = {
                            id: registro_id
                        };
                        $.ajax({
                            url: "/catalogo/imagen/inactivarImagen",
                            data: data,
                            dataType: 'html',
                            type: 'post',
                            success: function(resp, resp2, resp3) {

                                try {
                                    var json = jQuery.parseJSON(resp3.responseText);

                                    if (json.statusCode == "success") {

                                        refrescarGrid();
                                        dialogInactivar.dialog('close');
                                        $("#msj_error_inactivar").addClass('hide');
                                        $("#msj_error_inactivar p").html('');
                                        $("#msj_exitoso").removeClass('hide');
                                        $("#msj_exitoso p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                        location.reload();

                                    }

                                    if (json.statusCode == "error") {

                                        $("#pregunta_inactivar").addClass('hide');
                                        $("#msj_exitoso").addClass('hide');
                                        $("#msj_exitoso p").html('');
                                        $("#msj_error_inactivar").removeClass('hide');
                                        $("#msj_error_inactivar p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                                catch (e) {

                                }
                            }
                        })
                    }
                }
            ]
        });
        $("#dialog_inactivacion").show();
    }
    $(".add-inactivar").unbind('click');
    $(".add-inactivar").on('click', function(e) {
        e.preventDefault();

        var registro_id;
        registro_id = $(this).attr("data");

        inactivar(registro_id);
    });

    function activar(registro_id) {

        $("#pregunta_activar").removeClass('hide');
        $("#msj_error_activar").addClass('hide');
        $("#msj_error_activar p").html('');

        var dialogActivar = $("#dialog_activacion").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Registro de Imagen</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar",
                    "class": "btn btn-success btn-xs",
                    click: function() {
                        var data = {
                            id: registro_id
                        }
                        $.ajax({
                            url: "/catalogo/imagen/activarImagen",
                            data: data,
                            dataType: 'html',
                            type: 'get',
                            success: function(resp, resp2, resp3) {

                                try {
                                    var json = jQuery.parseJSON(resp3.responseText);

                                    if (json.statusCode == "success") {

                                        refrescarGrid();
                                        dialogActivar.dialog('close');
                                        $("#msj_error_activar").addClass('hide');
                                        $("#msj_error_activar p").html('');
                                        $("#msj_exitoso").removeClass('hide');
                                        $("#msj_exitoso p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                        location.reload();
                                    }

                                    if (json.statusCode == "error") {

                                        $("#pregunta_activar").addClass('hide');
                                        $("#msj_exitoso").addClass('hide');
                                        $("#msj_exitoso p").html('');
                                        $("#msj_error_activar").removeClass('hide');
                                        $("#msj_error_activar p").html(json.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                                catch (e) {

                                }
                            }
                        })
                    }
                }
            ]
        });
        $("#dialog_activacion").show();
    }

    $(".add-activar").unbind('click');
    $(".add-activar").on('click', function(e) {
        e.preventDefault();
        var registro_id;
        registro_id = $(this).attr("data");
        activar(registro_id);
    });
});