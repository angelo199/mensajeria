$(document).ready(function() {

    $('#Autoridad_cedula').bind('keyup blur', function() {
        keyNum(this);
    });

    $('#Autoridad_nombre').bind('keyup blur', function() {
        keyAlphaNum(this, true, false);
    });

});