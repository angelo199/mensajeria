$(document).ready(function() {
    formatoCamposEvent();
    seleccionaOrigenEvent();
    reloadCaptchaEvent();
    submitFormEvent();
    
    $.gritter.add({
        title: 'Recuerde!',
        text: 'Si usted es Personal Fijo o Contratado del MPPE, debe de tener a la mano la información de su Número de Cuenta Nómina y otros datos personales, esto con la finalidad de poder completar su registro de forma rápida.',
        class_name: 'gritter-warning',
        time: 10000
    });

    $.gritter.add({
        title: '¡Importante!',
        text: 'Debe ingresar un Correo Electrónico activo para poder registrarse en el Sistema, le recomendamos utilizar su correo institucional o un correo Gmail, de no poseer ninguno puede crearlo desde el siguiente enlace: <a style="color:white;" href="https://accounts.google.com/SignUp?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ltmpl=default" target="_blank"><b>https://accounts.google.com</b><a/> </br> <b>Por favor, haga el registro con mucha calma y verifique que todos sus datos sean correctos</b>.',
        class_name: 'gritter-warning gritter-center center',
        time: 20000
    });
    
});

//Funciones 

function formatoCamposEvent(){
    //Limpiar campos.
    //$("#UserGroupsRegistro_cedula").numeric();

    $("#UserGroupsRegistro_cedula").on('keyup blur', function(){
        keyNum(this, false, false);
    });
    
    $("#UserGroupsRegistro_nombre").on('keyup blur', function(){
        keyLettersAndSpaces(this, true);
        makeUpper(this);
    });
    $("#UserGroupsRegistro_nombre").on('keyup blur', function(){
        keyLettersAndSpaces(this, true);
        makeUpper(this);
    });
    $("#UserGroupsRegistro_apellido").on('keyup blur', function(){
        keyLettersAndSpaces(this, true);
        makeUpper(this);
    });
    $("#UserGroupsRegistro_password").on('keyup blur', function(){
        $(this).val($(this).val().trim());
    });
    $("#passwordValidate").on('keyup blur', function(){
        keyText(this);
    });
    
    // Si el Usuario es externo al Ministerio de Educación no se le solicitará que ingrese los últimos 4 Números de la Cuenta Nómina del MPPE
    $("#UserGroupsRegistro_group_id").on('change', function(){
       if($(this).val()=='9'){
           $("#UserGroupsRegistro_digitos_cuenta_nomina").val("").removeAttr("required").attr("disabled", "true");
       }
       else{
           $("#UserGroupsRegistro_digitos_cuenta_nomina").val("").attr("required", "required").removeAttr("disabled", "true");
       }
    });
    
    //Mascaras 
    $('#UserGroupsRegistro_telefono').mask('(0999)999-9999');
    $('#UserGroupsRegistro_digitos_cuenta_nomina').mask('9999');
    
}

//Funcion que controla el funcionamiento del boton desplegable del formulario _registro
function seleccionaOrigenEvent() {
    $(".origenAcciones").on('click', function(){
        var data = $(this).attr('data-value');
            $("#botonOrigen").html(data+"&nbsp; &nbsp;<span class='caret'></span>");
            $("#UserGroupsRegistro_origen").val(data);
    });
}

//Recarga el Captcha de la vista _registro
function reloadCaptchaEvent(){
    $("#linkRefreshCaptchaRegistro").on('click', function (){
        reloadCaptcha();
    });
}
function reloadCaptcha(){
    jQuery('#siimageRegistro').attr('src', '/login/captcha/sid/' + Math.random());
    $("#UserGroupsRegistro_verifyCode").val("");
}

//Luego de pasar por la validación HTML5
function submitFormEvent() {
    $("#register-form").on( 'submit', function(evt){
        //Si se ejecuta el submit del formulario registre-form, Previene el evento.
        evt.preventDefault();
        //Valida si el password coincide 
        if(validateFields()){
            ejecutarRegistroUsuario($("#register-form"));
        }
        scrollUp();
    });
}

//Funciones para validar campos
function validateFields() {
    var clave = $("#UserGroupsRegistro_password").val();
    var confirmacionDeClave = $("#UserGroupsRegistro_password_confirm").val();
    var correo = $("#UserGroupsRegistro_email").val();
    var confirmarCorreo = $("#confirmarEmail").val();
    var origen = $("#UserGroupsRegistro_origen").val();
    var mensaje = "";
    if(clave != confirmacionDeClave){
        mensaje += "<i class='fa fa-chevron-right fa-1'></i>&nbsp; La Confirmación de la Clave no coincide con la Clave Ingresada.";
        $("#UserGroupsRegistro_password").focus();
    }
    if(correo != confirmarCorreo){
        if(mensaje.length>0){mensaje += "<br/>";}
        mensaje += "El Correo que ha ingresado no coincide con la confirmación del Correo.";
        $("#UserGroupsRegistro_email").focus();
    }
    if(clave.length<6){
        if(mensaje.length>0){mensaje += "<br/>";}
        mensaje += "<i class='fa fa-chevron-right fa-1'></i>&nbsp; La Clave debe poseer al menos 6 caracteres.";
    }
    if(clave==$("#UserGroupsRegistro_cedula").val()){
        if(mensaje.length>0){mensaje += "<br/>";}
        mensaje += "<i class='fa fa-chevron-right fa-1'></i>&nbsp; La Clave no puede ser el mísmo número de Cédula.";
    }

    if(mensaje.length>0){
        $("#UserGroupsRegistro_password").focus();
        displayDialogBox("#result", "error", mensaje);
        return false;
    }
    else{
        $("#result").html("");
        return true;
    }
}

//Llamar a actionAdmin del Controlador RegistroUsuarioController
function ejecutarRegistroUsuario(form){
    var divResult = "#result";
    var urlDir = form.attr('action');
    var datos = form.serialize();
    var loadingEfect = true;
    var showResult = true;
    var method = "POST";
    var responseFormat = "json";
    var successCallback = function(response, estatusCode, xhr){
        try{
            $("#UserGroupsRegistro_verifyCode").val("").focus();
            $("#registroUsuarioToken").val(response.tokenCsrf);
            
            console.log(response.codigoResultado);
            
            if(response.codigoResultado!='EA0002'){ // Este error (EA0002) viene con el HTML de error. Validación de Datos del Formulario.
                displayDialogBox("#result", response.resultado, response.mensaje);
            }
            else{
                
                if(response.codigoResultado == "S00000"){
                    $("#register-form").reset();
                }
                $("#result").html(response.mensaje);
            }

            reloadCaptcha();
        }
        catch(err) {
            displayDialogBox("#result", 'error', 'Ha ocurrido un error en el sistema. Comuniquese con el administrador del mismo. Error: '+err.message);
        }
    };

    var errorCallback = function(){
        $("#UserGroupsRegistro_verifyCode").val("").focus();
        reloadCaptcha();
    };

    executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
}
