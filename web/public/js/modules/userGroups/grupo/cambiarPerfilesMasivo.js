/**
 * Created by developer on 20/10/15.
 */
$(document).ready(function() {

    $('#cedulas').on('keyup blur', function () {
        keyNumWithEnters(this, false, false);
    });
    $('#formulario-perfil').on('submit', function(event) {
        event.preventDefault();
        cambioPerfil.porCedulas($(this));
    });

});

var cambioPerfil = {

    porCedulas: function (form) {

        var datos = form.serialize();
        var urlDir = form.attr('action');
        var divResult = '#divResultado';
        var loadingEfect = true;
        var showResult = true;
        var method = form.attr('method');
        var responseFormat = "html";

        var beforeSendCallback = function(){ 
            $("#div-cedulas").addClass("hide");
        };
        
        var errorCallback = function(xhr, ajaxOptions, thrownError, mensaje){ 
            // $("#div-cedulas").removeClass("hide");
            scrollUp('slow');
        };
        
        var successCallback = function(response){ 
            scrollUp('slow');
            $("#div-cedulas").addClass("hide");
        };

        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback, beforeSendCallback);
    }
};
