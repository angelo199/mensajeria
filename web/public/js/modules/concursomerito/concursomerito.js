$(document).ready(function(){
    
    $( "#fecha_inicio" ).datepicker({
      defaultDate: "+1d",
      minDate: "+1d",
      dateFormat: "dd-mm-yy",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#fecha_final" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#fecha_final" ).datepicker({
      defaultDate: "+1d",
      changeMonth: true,      
      dateFormat: "dd-mm-yy",
      onClose: function( selectedDate ) {
        $( "#fecha_inicio" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    $( "#AperturaPeriodo_fecha_inicio" ).datepicker({
      defaultDate: "+1d",
      minDate: "+1d",
      dateFormat: "yy-mm-dd",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#AperturaPeriodo_fecha_inicio" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#AperturaPeriodo_fecha_final" ).datepicker({
      defaultDate: "+1d",
      changeMonth: true,      
      dateFormat: "yy-mm-dd",
      onClose: function( selectedDate ) {
        $( "#AperturaPeriodo_fecha_inicio" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    $("#periodo_nombre").on('keyup blur',function(e){
        keyAlphaNum(this,true,true);
    });
    
})