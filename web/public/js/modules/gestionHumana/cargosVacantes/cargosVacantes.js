$(document).ready(function(){

getListaCargosVacantes();
cargarDatosCargos();
capturaCodigoCargoLista();
validarCampos();
validarCamposCargosVacantes();
getPerfilCargoVacante();


    $.gritter.add({
                title: 'Requisitos para Postulación',
                text: 'Recuerde que antes de llevar a cabo la postulación debe registrar en su perfil su Información Académica, Experiencia Laboral y Referencias Personales',
                class_name: 'gritter-warning',
                time: 4000
            });
    


    $("#btnPostulacion").on("click", function(){
        var divResult = "#div-result";
        var mensaje='';
        var supervisorInmediato = $("#Postulacion_supervisor_inmediato").val();
        console.log(supervisorInmediato);
        var periodoEvaluacion= $("#Postulacion_periodo_evaluacion").val();
        var rangoActuacion= $("#Postulacion_rango_actuacion_id").val();
        var puntuacionObtenida= $("#Postulacion_puntuacion_obtenida").val();
        
        var codigoCargo= $("#Postulacion_cargo_postulado_id").val();
        var cargoPostulado= $("#cargoVacanteLabel").val();
        var estadoDependencia= $("#nombreEstadoLabel").val();
        var codigoDependencia= $("#nombreDependenciaLabel").val();
        var claseCargo= $("#nombreClaseCargoLabel").val();
        var estatusPostulacion= $("#nombreEstadoPostulacionLabel").val();
        var categoriaCargoActualId= $("#talento_humano_categoria_cargo_actual_id").val();
        
        //Variables de Aptitudes Registradas
        var cantidadEstudiosRealizados = $("#cantidadEstudiosRealizados").val();
        var cantidadExperienciaLaboral = $("#cantidadExperienciaLaboral").val();
        var cantidadReferenciaPersonal = $("#cantidadReferenciaPersonal").val();
        //console.log();
        if( $("#tipoPeriodo").val() === 'concurso' && supervisorInmediato.length==0) {
            mensaje = mensaje+"- Debe llenar el campo nombre del supervisor.<br/>";
        }
             
        if( $("#tipoPeriodo").val() === 'merito' ) {

            if (supervisorInmediato.length==0){
                mensaje = mensaje+"- Debe llenar el campo nombre del supervisor.<br/>";
             } 
            if (periodoEvaluacion.length==0){
                mensaje = mensaje+"- Debe llenar el campo Periodo de Evaluación.<br/>";
             }
             if (rangoActuacion.length==0){
                mensaje = mensaje+"- Debe llenar el campo Rango de Actuación de desempeño.<br/>";
             }
            if (puntuacionObtenida.length==0){
                mensaje = mensaje+"- Debe llenar el campo Puntuación obtenida.<br/>";
            }  
            
            if ($.isNumeric(puntuacionObtenida) && puntuacionObtenida.length > 0) {
                if (categoriaCargoActualId == 1 && (puntuacionObtenida < 20 || puntuacionObtenida > 100)) {
                    mensaje = mensaje + "- La Puntuación obtenida debe  estar en el rango del 10 al 100 verifique su respuesta.<br/>";
                }
                if (categoriaCargoActualId == 2 && (puntuacionObtenida < 100 || puntuacionObtenida > 500)) {
                    mensaje = mensaje + "- La Puntuación obtenida debe estar en el rango de 100 a 500 verifique su respuesta.<br/>";
                }
            } else {
            mensaje = mensaje + "- La Puntuación obtenida es inválida. El valor del campo debe ser numérico.<br/>";
        }
            
        }

        if (codigoCargo.length==0){
            mensaje = mensaje+"- Debe llenar el campo Código de Cargo Nominal .<br/>";
        }
        
        //Validar que el Talento Humano cumpla con los registros de Estudios Realizados, Experiencia Laboral y Referencias Personales.
        if(cantidadEstudiosRealizados==0) {
            mensaje = mensaje+"- Debe poseer al menos un(1) Estudio Realizado Registrado.<br/>";
        }
        if(cantidadExperienciaLaboral==0 && $("#tipoPeriodo").val() === 'merito') {
            mensaje = mensaje+"- Debe poseer al menos una(1) Experiencia Laboral Registrada.<br/>";
        }
        if(cantidadReferenciaPersonal<2 && $("#tipoPeriodo").val() === 'concurso')  {
            mensaje = mensaje+"- Debe poseer al menos dos(2) Referencias Personales Registradas.<br/>";
        }

  
        if(mensaje.length>0){
           displayDialogBox(divResult, 'error', mensaje);
           scrollUp('slow');
       }
        else{
  
    var dialog = $("#divConfirmarPostulacionDialogBox").removeClass('hide').dialog({
        modal: true,
        width: '900px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Confirmar Postulación</h4></div>",
        title_html: true,
        buttons: [{
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs btn-danger",
                "id": "botonCancelarPostulacion",
                "click": function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "Confirmar &nbsp;<i class='fa fa-exclamation-triangle bigger-110'></i>",
                "class": "btn btn-warning btn-xs",
                "id": "botonConfirmarPostulacion",
                "click": function() {
                    $("#btnSubmitPostulacion").click();
                }
            }]
        });
    }
    });
    
});

function getPerfilCargoVacante() {
    
    $(".checkPerfilCargo").on('click', function() {
        
        var idDivPerfil = 'divPerfilCargo-'+$(this).attr('data-codigo');
        var nombreCargo = $(this).attr('data-codigo')+' - '+$(this).attr('data-nombre');
        
        var mensaje = $('#'+idDivPerfil ).html();
        
        $.gritter.add({
            title: 'Perfil del Cargo: '+nombreCargo,
            text: mensaje,
            class_name: 'gritter-center gritter-info',
            time: 10000
        }); 
    });
}

function datosCargoVacante(codNomin){
        
    var urlDir = "/gestionHumana/Postulacion/getCargoVacante/codnomin/"+codNomin;
    var divResult = '';
    var datos = '';
    var loadingEfect = false;
    var showResult = false;
    var method = "GET";
    var responseFormat = "json";
    var successCallback = function(response){ 
        $("#cargoVacanteLabel").html(response.descripcion);
        $("#nombreEstadoLabel").html(response.estadoNombre);
        $("#nombreDependenciaLabel").html(response.dependenciaNombre);
        
        $("#nombreClaseCargoLabel").html(response.claseCargoNombre);
        
        $("#Postulacion_cargo_postulado_id").val(response.cargoId);
        $("#Postulacion_estado_id").val(response.estadoId);
        $("#Postulacion_dependencia_postulado_id").val(response.dependenciaId);
        $("#Postulacion_dependencia_general_postulado").val(response.dependenciaNombre);
        $("#Postulacion_clase_cargo_id").val(response.claseCargoId);
        
        
        if(response.cargoId == null ){
            $("#inputCodigoCargo").val('');
            $.gritter.add({
                title: 'Seleccione un Código de Cargo Nominal Valido',
                text: ' - Verifique que este ingresando correctamente el Código del Cargo. <br/> - Puede que este Código de Cargo no pertenezca a su Estado (Si es personal Fijo o Contratado el Código de Cargo que puede elegir está condicionado al Estado de su Dependencia Nominal).',
                class_name: 'gritter-warning',
                time: 8000
            });
        }
        
    };
    executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
}

function getListaCargosVacantes(){
    var dialog = $("#divListaCargosVacantes").removeClass('hide').dialog({
        modal: true,
        width: '1200px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Seleccionar Cargo Vacante</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs btn-danger",
                "id": "botonCancelarCargoVacante",
                "click": function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

function capturaCodigoCargoLista() {
    $(".checkCodigoCargo").on('click', function (){
        var cantidadPostulados = $(this).attr("data-postulados");
        if (cantidadPostulados>=cantidadMaximaCargos){
             $.gritter.add({
                title: 'Limite de Postulaciones Excedidas',
                text: 'El Cargo que Usted Selecciono ya Supero el Número de Postulaciones Disponible, Por favor Seleccione Otro Cargo.',
                class_name: 'gritter-warning',
                time: 6000
            });
        }
        else{
            $("#inputCodigoCargo").val($(this).attr('data-codigo'));
            $("#inputCodigoCargo").focus();
            $("#divListaCargosVacantes").dialog("close");
        }
    });
}

function validarCamposCargosVacantes(){
    $("#CargoVacante_codnomin").on('keyup blur', function(){
        keyNum(this, true, true);
    });
    
    $("#CargoVacante_descripcion").on('keyup blur', function(){
        keyText(this, true, true);
        makeUpper(this);
    });
    
    $("#CargoVacante_coddenpen").on('keyup blur', function(){
        keyNum(this, true, true);
    });
    
    $("#CargoVacante_dependenciaNombre").on('keyup blur', function(){
        keyText(this, true, true);
        makeUpper(this);
    });
}

function validarCampos() {
    
    $("#inputCodigoCargo").on('keyup blur', function(){
        keyNum(this, true, true);
    });
    
    $("#Postulacion_puntuacion_obtenida").on('keyup blur', function(){
        keyNum(this, true, true);
    });
    
    $("#Postulacion_supervisor_inmediato").on('keyup blur', function(){
        keyAlpha(this, true, true);
        makeUpper(this);
    });
}

function cargarDatosCargos(){
    $("#inputCodigoCargo").on('focusout', function(){
        var codigo = $(this).val();
        if(codigo.length>0 && codigo <= 2147483647 ){
            datosCargoVacante($(this).val());
        }else if(codigo.length>0 && codigo > 2147483647 ) {
            $.gritter.add({
                title: 'Código de Cargo Nominal Invalido',
                text: 'Ingrese un Código Valido.',
                class_name: 'gritter-warning',
                time: 4000
            });
            limpiarCamposDeCargoPostulado();
        }
        else {
            limpiarCamposDeCargoPostulado();
        }
    });
    

    $("#btnCodigoCargo").on('click', function(){
        getListaCargosVacantes();
        var codigoNomina = $("#inputCodigoCargo").val();
        if(codigoNomina.length > 0) {
            $.fn.yiiGridView.update("cargo-vacante-grid", {data: {"CargoVacante[codnomin]":codigoNomina}});
        }
        else{
            $.fn.yiiGridView.update("cargo-vacante-grid");
        }
    });
}

function limpiarCamposDeCargoPostulado(){
    $("#cargoVacanteLabel").html("");
    $("#nombreEstadoLabel").html("");
    $("#nombreDependenciaLabel").html("");
    $("#nombreClaseCargoLabel").html("");
    $("#Postulacion_cargo_postulado_id").val("");
    $("#Postulacion_estado_id").val("");
    $("#Postulacion_dependencia_postulado_id").val("");
    $("#Postulacion_clase_cargo_id").val("");
}
