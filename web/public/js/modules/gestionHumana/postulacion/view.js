/**
 * Created by developer on 02/11/15.
 */
$(document).ready(function(){

    $("#btnCancelarPostulacion").on("click", function(){
        limpiarCampos();
        confirmarCancelarPostulacion();
    });
    $('#btnSubmitCancelarPostulacion').on('click', function(event){
        if($("#motivo_eliminacion_id").val().length > 0 && $("#descripcion_motivo_eliminacion").val().length > 0){
            event.preventDefault();
            cancelarPostulacion($("#postulacionEncodeId").val(), $("#talentoHumanoId").val(), $("#tipoPeriodo").val());
        }
    });

});


function confirmarCancelarPostulacion() {
    var dialog = $("#divConfirmarCancelarPostulacionDialogBox").removeClass('hide').dialog({
        modal: true,
        width: '900px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Cancelar Postulación</h4></div>",
        title_html: true,
        buttons: [{
            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
            "class": "btn btn-xs btn-danger",
            "id": "botonCancelar",
            "click": function() {
                $(this).dialog("close");
            }
        },
            {
                html: "Confirmar &nbsp;<i class='fa fa-exclamation-triangle bigger-110'></i>",
                "class": "btn btn-warning btn-xs",
                "id": "botonConfirmarCancelarPostulacion",
                "click": function() {
                    loadingEfect(true);
                    $("#btnSubmitCancelarPostulacion").click();
                }
            }]
    });
}

function cancelarPostulacion(postulacionIdEncode, talentoHumanoIdEncode, tipoPeriodoEncode) {

    $("#botonConfirmarCancelarPostulacion").attr("disabled", "disabled");
    var siglasTipoPeriodo = base64_decode($("#siglasTipoPeriodo").val());

    var urlDir = "/gestionHumana/postulacion/cancelarPostulacion/id/"+postulacionIdEncode+"/th/"+talentoHumanoIdEncode+"/tipo/"+tipoPeriodoEncode;
    var divResult = '';
    var datos = {'descripcionMotivoEliminacion':$("#descripcion_motivo_eliminacion").val(), 'motivoEliminacion':$("#motivo_eliminacion_id").val()};
    var loadingEfect = true;
    var showResult = false;
    var method = "POST";
    var responseFormat = "json";
    var successCallback = function(response){
        if (response.estado_resultado === 'success') {
            $("#divConfirmarCancelarPostulacionDialogBox").dialog("close");
            $.gritter.add({
                title: 'Aprobación Realizada',
                text: response.mensaje,
                class_name: 'gritter-success',
                time: 3000
            });
            limpiarCampos();
            location.href = $("#postulacion-"+siglasTipoPeriodo).attr('href');

        }
        else if (response.estado_resultado === 'error') {
            $.gritter.add({
                title: 'Ha ocurrido un error',
                text: response.mensaje,
                class_name: 'gritter-error',
                time: 3000
            });
        }
        $("#botonConfirmarCancelarPostulacion").removeAttr("disabled");
    };
    executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
}

function limpiarCampos() {
    $("#motivo_eliminacion_id").val(null);
    $("#descripcion_motivo_eliminacion").val(null);
}


function loadingEfect(start) {
    if (start) {
        if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
            Loading.show();
        }else{
            var url_image_load = "<div class='padding-5 center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
            if(responseFormat.toLowerCase()=='html'){
                displayHtmlInDivId(divResult, url_image_load);
            }
        }
    } else {
        if(undefined !== Loading && Loading !== null && typeof Loading === 'object'){
            Loading.hide();
        }
    }

}