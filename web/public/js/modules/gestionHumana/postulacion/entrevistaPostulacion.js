$(document).ready(function() {
   
    asignarEventos();
    
});

function asignarEventos(){
    $(".getPostulacionBtn").on('click', function(){
        validarAprobacion($(this).attr('data-postulacion'));
    });
}



function getVentanaEntrevista(){
    var dialog = $("#ventanaIniciarEntrevista").removeClass('hide').dialog({
        modal: true,
        width: '800px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-check-square'></i>Aprobación Realizada</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs btn-danger",
                "id": "botonCancelarVentanaEntrevista",
                "click": function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}


function validarAprobacion(dataPostulacion) {
    
    $(".checkbox").prop('checked' ,false);
    $("#textareaObservacion").val('');
    var dialog = $("#divValidarAprobacion").removeClass('hide').dialog({
        modal: true,
        width: '1000px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Validar la Aprobación</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs btn-danger",
                "id": "botonCancelarPostular",
                "click": function() {
                    //console.log(this);
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='fa fa-times bigger-110'></i>&nbsp; No Aprobar",
                "class": "btn btn-xs btn-warning",
                "id": "botonRechazarPostular",
                "click": function() {
                    finalizarPostulacion(dataPostulacion ,base64_encode($("#inputResultado").val()), base64_encode('No Aprobada'), $("#textareaObservacion").val(), this);
                }
            },
            {
                html: "<i class='fa fa-check bigger-110'></i>&nbsp; Aprobar",
                "class": "btn btn-xs btn-success",
                "id": "botonAprobarPostular",
                "click": function() {
                    finalizarPostulacion(dataPostulacion ,base64_encode($("#inputResultado").val()), base64_encode('Aprobada'), $("#textareaObservacion").val(), this);
                }
            }
        ]
    });
    
}

function finalizarPostulacion(idPostulacion, resultados, estatus, observacion, dialogBoxId) {
    var estatusDecode = base64_decode(estatus); // El valor de estatusDecode determina si fue aprobado o la postulacion,
    if($(".checkbox:checked").size() < 3  && estatusDecode == 'Aprobada') { // el valor de Postulación Aprobada
        
        $.gritter.add({
            title: 'Faltan Documentos por Consignar',
            text: 'Debe cumplir con al menos 3 Documentos Consignados de las selecciones.',
            class_name: 'gritter-warning',
            time: 3000
        });
        
    } 
    else if ( estatusDecode == 'No Aprobada' && observacion.length == 0 ) { // el valor de Postulación No Aprobada
        $.gritter.add({
            title: 'Debe Indicar el Motivo del Rechazo de la Postulación',
            text: 'La Observación no puede estar vacía cuando selecciona "No Aprobar" la Postulación.',
            class_name: 'gritter-warning',
            time: 3000
        });
    
    } else {
    
        var urlDir = "/gestionHumana/postulacion/aprobar/id/"+idPostulacion+"/respuestas/"+resultados+"/estatus/"+estatus;
        var divResult = '';
        var datos = {observacion: observacion};
        var loadingEfect = true;
        var showResult = false;
        var method = "POST";
        var responseFormat = "json";
        var successCallback = function(response){ 
            if (response.estado_resultado === 'success') {
                //console.log(dialogBoxId);
                $(dialogBoxId).dialog("close");
                //$("#Postulacion_cedula").val(response.cedula);
                //$("#mensajeResponse").html(response.mensaje);
                $.fn.yiiGridView.update("postulacion-grid");
                $.gritter.add({
                    title: 'Aprobación Realizada',
                    text: response.mensaje,
                    class_name: 'gritter-success',
                    time: 3000
                });
                
                //getVentanaEntrevista();
            }
            else if (response.estado_resultado === 'error') {
                $.gritter.add({
                    title: 'Ha ocurrido un error',
                    text: response.mensaje,
                    class_name: 'gritter-error',
                    time: 3000
                });
            }

        };
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback); 
    
    }
}
