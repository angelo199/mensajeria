$(document).ready(function() {
    
    
   var resultado = [];
    
    
    //Generales

    $("#checkboxInscripcion").on('click', function() {   
        capturarValorCheck(this.id);
    });
    $("#checkboxCurriculo").on('click', function() {   
        capturarValorCheck(this.id);
    });
    $("#checkboxTitulo").on('click', function() {   
        capturarValorCheck(this.id);
    });
    $("#checkboxCursosTalleres").on('click', function() {   
        capturarValorCheck(this.id);
    });
    
    //Campos para la vista de documentos en concurso.
    
    $("#checkboxReferenciasLaborales").on('click', function() {   
        capturarValorCheck(this.id);
    });
    $("#checkboxReferenciasPersonales").on('click', function() {   
        capturarValorCheck(this.id);
    });
    
    //Campos para la vista de documentos en merito.
   
    $("#checkboxConstancia").on('click', function() {   
        capturarValorCheck(this.id);
    });
    $("#checkboxEvaluacion").on('click', function() {   
        capturarValorCheck(this.id);
    });
    $("#checkboxReferencias").on('click', function() {   
        capturarValorCheck(this.id);
    });
    $("#checkboxEvaluacionDesempeno").on('click', function() {   
        capturarValorCheck(this.id);
    });
    
    $(".checkbox").on('click', function() {
        resultado = {
            checkboxInscripcion:$("#checkboxInscripcionHiden").val(),
            checkboxConstancia:$("#checkboxConstanciaHiden").val(),
            checkboxEvaluacion:$("#checkboxEvaluacionHiden").val(),
            checkboxCurriculo:$("#checkboxCurriculoHiden").val(),
            checkboxTitulo:$("#checkboxTituloHiden").val(),
            checkboxCursosTalleres:$("#checkboxCursosTalleresHiden").val(),
            checkboxReferencias:$("#checkboxReferenciasHiden").val(),
            checkboxReferenciasLaborales:$("#checkboxReferenciasLaboralesHiden").val(),
            checkboxReferenciasPersonales:$("#checkboxReferenciasPersonalesHiden").val(),
            checkboxEvaluacionDesempeno:$("#checkboxEvaluacionDesempenoHiden").val()
        };
//        JSON.parse();
        resultado = JSON.stringify(resultado);
        $("#inputResultado").val(resultado);
    });
    
    
});


function capturarValorCheck(id) {
    /*  
     * Si el valor del campo check es diferente al valor del campo Hiden, 
     * captura el valor del check en el hiden. 
     * Si por lo contrario poseen el mismo valor, borra el valor del hiden 
     */  
    if($("#"+id+"Hiden").val() !== $("#"+id).val()){
        $("#"+id+"Hiden").val($("#"+id).val());
    } else {
        $("#"+id+"Hiden").val('');
    }    
}

