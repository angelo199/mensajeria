$(document).ready(function () {


// generacion de la constancia de registro.
    $("#linkSolicitudConstanciaDeInscripcion").on('click', function(evt){
        evt.preventDefault();
       
        dialogSolicitudComprobante();
       
        var divResult = "#divResultDescargaConstanciaDeInscripcion";
        var urlDir = $(this).attr('href');
        var datos = '';
        var loadingEfect = false;
        var showResult = false;
        var method = 'GET';
        var responseFormat = "json";
        var successCallback = function(response, estatusCode, dom){
            var result = response.codigo;
            var mensaje = response.mensaje;
            if(result=='EXITO'){
                displayDialogBox("#divResultDescargaConstanciaDeInscripcion", 'success', mensaje+" Haga click en el siguiente enlace para efectuar su descarga.");
                $("#linkDescargaComprobanteDeInscripcion").attr("href", response.url_archivo_pdf+"?v="+Math.floor((Math.random() * 1000) + 1)).removeClass("hide");
            }
            else{
                displayDialogBox("#divResultDescargaConstanciaDeInscripcion", 'error', mensaje);
            }
           
        };
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
    });

});

function dialogSolicitudComprobante(){
    
    var mensaje = '<div class="infoDialogBox">\
            <p>\
                Se está efectuando la generación del comprobante. En pocos segundos se podrá visualizar un link de descarga del mismo.\
            </p>\
        </div>\
        <div align="center">\
            <img src="/public/images/ajax-loader-red.gif" />\
        </div>\
         <div class="padding-6 center">\
        <a class="hide" target="_blank" id="linkDescargaComprobanteDeInscripcion" href="#">\
            <img src="/public/images/file_pdf.png" />\
        </a>\
    </div>';
     $("#linkDescargaComprobanteDeInscripcion").attr("href", "#").addClass("hide");
    $("#divResultDescargaConstanciaDeInscripcion").html(mensaje);
   
    
    var dialog = $("#dialogDescargaConstanciaInscripcion").removeClass('hide').dialog({
        modal: true,
        width: '680px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-file-pdf-o'></i>&nbsp; Solicitud de Constancia de Inscripción.</h4></div>",
        title_html: true,
        buttons: [
            {
                "html":  "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs btn-danger",
                "id":    "btnCancelarDialogSolicitudComprobante",
                "click": function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}
