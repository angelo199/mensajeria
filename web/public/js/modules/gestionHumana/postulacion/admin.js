$(document).ready(function() {
    
     $("#btnLeyenda").on("click", function(){
     var dialog = $("#mostrarLeyenda").removeClass('hide').dialog({
        modal: true,
        width: '500px',
        draggale: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-info'></i> &nbsp; Leyenda</h4></div>",
        title_html: true,
        buttons: [{
                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs btn-danger",
                "id": "botonCancelarLeyenda",
                "click": function() {
                    $(this).dialog("close");
                }
            }]
        });
    });

    $.mask.definitions['X'] = '[V|E|P|v|e|p]';
    $('#Postulacion_cedula').mask('X-999?9999999999');
    
    $('#Postulacion_cedula').bind('blur', function() {
        clearField(this);
    });

    $('#Postulacion_cargoPostuladoId').bind('keyup blur', function() {
        keyText(this, true);
        clearField(this);
    });

    $('#Postulacion_cargoVacanteDependencia').bind('keyup blur', function() {
        keyText(this, true);
        clearField(this);
    });

});
