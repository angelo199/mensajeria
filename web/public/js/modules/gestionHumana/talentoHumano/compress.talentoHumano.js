//--------------------------------------------------------------------------------------------------
//------------------------------------ form.js ---------------------------------------
//--------------------------------------------------------------------------------------------------
$(document).ready(function(){

    formInputFilters();
    //busquedaSaime();

    // Si la persona es Aspirante no debe de tener datos laborales ni datos bancarios.
    var condicionActualId = $("#condicion_actual_id").val();
    if(condicionActualId=='0'){
        $("#div-datos-laborales").attr("class", "hide");
        $("#div-datos-banco").attr("class", "hide");
    }

    $("#AdminTalentoHumano_estado_id").on('change', function(){
        var municipios = getDataCatastro('Municipio', 'estado_id', $(this).val(), false, 'nombre');
        $("#AdminTalentoHumano_municipio_id").html(generateOptionsToSelect(municipios, 'nombre', 'id', null));
    });

    $("#AdminTalentoHumano_municipio_id").on('change', function(){
        var parroquias = getDataCatastro('Parroquia', 'municipio_id', $(this).val(), false, 'nombre');
        $("#AdminTalentoHumano_parroquia_id").html(generateOptionsToSelect(parroquias, 'nombre', 'id', null));
    });

    var formType = $("#talento-humano-form").attr('data-form-type');

    if(formType==='create'){
        $("#AdminTalentoHumano_cedula").on('blur', function(){
            busquedaSaime();
        });
    }
    else{
        $("#AdminTalentoHumano_origen").attr('disabled', 'disabled');
        $("#AdminTalentoHumano_cedula").attr('readOnly','readOnly');
        $("#AdminTalentoHumano_nombre").attr('disabled','disabled');
        $("#AdminTalentoHumano_apellido").attr('disabled','disabled');
        $("#AdminTalentoHumano_fecha_nacimiento").attr('disabled','disabled');
        $("#AdminTalentoHumano_sexo").attr('disabled', 'disabled');
    }

    if($("#talento-humano-form").attr('data-form-type')=='edicion'){
        $("#talento-humano-form").on('submit',function(evt){
            evt.preventDefault();
            registroDatosTalentoHumano($(this));
        });
    }

});

function formInputFilters(){

    // Filters

    $("#AdminTalentoHumano_cedula").on('keyup blur', function(){
        keyNum(this, false, false);
    });

    $("#AdminTalentoHumano_rif, #AdminTalentoHumano_urb_sector, #AdminTalentoHumano_av_calle, #AdminTalentoHumano_casa_edificio, #AdminTalentoHumano_apartamento").on('keyup blur', function () {
        keyAlphaNum(this, true, true);
    });

    $("#AdminTalentoHumano_aptitudes").on('keyup blur', function () {
        keyText(this, true);
    });

    $("#AdminTalentoHumano_piso").on('keyup blur', function () {
        keyNum(this, false, false);
    });

    $("#AdminTalentoHumano_direccion, #AdminTalentoHumano_ruta_trabajo, #AdminTalentoHumano_ruta_alterna").on('keyup blur', function () {
        keyText(this, true);
    });

    $("#AdminTalentoHumano_email_personal, #AdminTalentoHumano_email_corporativo").on('keyup blur', function () {
        keyEmail(this, false);
    });


    $("#AdminTalentoHumano_twitter").on('keyup blur', function () {
        keyTwitter(this, false);
    });



    // Clear Fields

    $("#AdminTalentoHumano_email_corporativo, #AdminTalentoHumano_twitter, #AdminTalentoHumano_email_personal, #AdminTalentoHumano_ruta_alterna, #AdminTalentoHumano_ruta_trabajo, #AdminTalentoHumano_direccion, #AdminTalentoHumano_piso, #AdminTalentoHumano_apartamento, #AdminTalentoHumano_casa_edificio, #AdminTalentoHumano_av_calle, #AdminTalentoHumano_urba_sector, #AdminTalentoHumano_rif").on('blur', function () {
        clearField(this);
    });



    // Masks

    $.mask.definitions['L'] = '[1-2]';
    $.mask.definitions['X'] = '[2|4|6]';
    $.mask.definitions['R'] = '[V|E|J|G|v|e|j|g]';

    $('#AdminTalentoHumano_telefono_fijo').mask('(0299)999-9999');
    $('#AdminTalentoHumano_telefono_celular').mask('(04LX)999-9999');
    $('#AdminTalentoHumano_telefono_oficina').mask('(0299)999-9999');
    $('#AdminTalentoHumano_numero_cuenta').mask('99999999999999999999');
    $('#AdminTalentoHumano_rif').mask('R-999?999999999999999');

    if($("#talento-humano-form").attr('data-form-type')!='update'){

        var fechaNacimiento = $('#AdminTalentoHumano_fecha_nacimiento').val();

        if(in_array(fechaNacimiento, ['0001-01-01', '1800-01-01'])){

            $("#read_fecha_nacimiento_latino").removeAttr('disabled').attr('readOnly', 'readOnly');

            $.datepicker.setDefaults({
                closeText: 'Cerrar',
                prevText: 'Anterior',
                nextText: 'Siguiente',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd-mm-yy',
                showOn: 'focus',
                showOtherMonths: false,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                minDate: new Date(1700, 1, 1),
                maxDate: 'yesterday',
                onSelect: function(dateText, inst)
                {
                    $('#AdminTalentoHumano_fecha_nacimiento').val($.datepicker.formatDate("yy-mm-dd", $('#read_fecha_nacimiento_latino').datepicker('getDate')));
                }
            });

            $('#read_fecha_nacimiento_latino').datepicker();

        }

    }

    // $.datepicker.setDefaults();
}

function busquedaSaime(){

    var origen = $("#AdminTalentoHumano_origen").val();
    var cedula = $("#AdminTalentoHumano_cedula").val();

    if(origen.length>0 && cedula.length>0){

        if(isValidOrigen(origen)){

            var divResult = "";
            var urlDir = "/ayuda/saime/consultaSaime/origen/"+origen+"/cedula/"+cedula;
            var datos = "";
            var loadingEfect = false;
            var showResult = false;
            var method = "GET";
            var responseFormat = "json";
            var beforeSendCallback = null;
            var successCallback = function(response, estatusCode, dom){

                console.log(response.statusCode);
                if(response.statusCode==='success'){
                    var origen = response.origen;
                    var cedula = response.cedula;
                    var nombre = response.nombre;
                    var apellido = response.apellido;
                    var fecha_nacimiento = response.fecha_nacimiento;
                    var fecha_nacimiento_latino = response.fecha_nacimiento_latino;
                    var sexo = response.sexo;

                    var cedulaInicial = $("#AdminTalentoHumano_cedula").attr('data-inicial');

                    $("#read_existe_cedula").val('Si');

                    var nombreInicial = $("#AdminTalentoHumano_nombre").val();
                    if(nombreInicial.length==0 || !isValidNombre() || cedulaInicial!=cedula){
                        $("#AdminTalentoHumano_nombre").val(nombre);
                    }

                    var apellidoInicial = $("#AdminTalentoHumano_apellido").val();
                    if(apellidoInicial.length==0 || !isValidApellido() || cedulaInicial!=cedula){
                        $("#AdminTalentoHumano_apellido").val(apellido);
                    }

                    $("#AdminTalentoHumano_cedula").attr('data-inicial',cedula);
                    $("#AdminTalentoHumano_nombre").attr('data-inicial',nombre);
                    $("#AdminTalentoHumano_apellido").attr('data-inicial',apellido);

                    $("#AdminTalentoHumano_fecha_nacimiento").val(fecha_nacimiento);
                    $("#read_fecha_nacimiento_latino").val(fecha_nacimiento_latino);
                    $("#AdminTalentoHumano_sexo").val(sexo);

                    $("#AdminTalentoHumano_origen_titular").val(origen);
                    $("#AdminTalentoHumano_cedula_titular").val(cedula);
                    var nombreCompleto = nombre + " " + apellido;
                    nombreCompleto = nombreCompleto.toUpperCase();
                    nombreCompleto = nombreCompleto.substr(0, 40);
                    $("#AdminTalentoHumano_nombre_titular").val();

                    $("#AdminTalentoHumano_verificado_saime").val('Si');
                }
                else{
                    var cedula = $("#AdminTalentoHumano_cedula").attr('data-inicial');
                    if($("#talento-humano-form").attr('data-form-type')!='update'){
                        $("#AdminTalentoHumano_cedula").val(cedula);
                    }
                    $("#read_existe_cedula").val('No');
                    $("#AdminTalentoHumano_verificado_saime").val('No');
                    simpleDialogPopUp("#resultadoDialogo", "fa-credit-card", 'Búsqueda de Documento de Identidad', getDialogBox('error', response.mensaje));
                }

            };

            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);

        }else{
            displayDialogBox('#resultado', 'error', 'El valor del Origen no es válido. Le recomendamos recargar la página e intentarlo de nuevo.');
        }

    }

}


function validateForm(){
    var cedulaRead = $("#read_existe_cedula").val();
    var existeCedula = cedulaRead==='Si';
    if(!existeCedula){
        displayDialogBox('resultado', 'error', 'El número de documento de identidad no ha podido ser encontrado en nuestra base de datos.');
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    }
    else{

        if($("#AdminTalentoHumano_telefono_fijo").val()=="" && $("#AdminTalentoHumano_telefono_celular").val()==""){
            $("#AdminTalentoHumano_telefono_fijo, #AdminTalentoHumano_telefono_celular").addClass('error');
            displayDialogBox('resultado', 'error', 'Debe indicar al menos un número telefónico de contacto.');
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
        else{
            $("#AdminTalentoHumano_telefono_fijo, #AdminTalentoHumano_telefono_celular").removeClass('error');
        }

        if(!isValidNombre()){
            $("#AdminTalentoHumano_nombre").addClass('error');
            displayDialogBox('resultado', 'error', 'Solo debe hacer correcciones del nombre, no modificarlo por completo.');
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
        else{
            $("#AdminTalentoHumano_nombre").removeClass('error');
        }

        if(!isValidApellido()){
            $("#AdminTalentoHumano_apellido").addClass('error');
            displayDialogBox('resultado', 'error', 'Solo debe hacer correcciones del apellido, no modificarlo por completo.');
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
        else{
            $("#AdminTalentoHumano_apellido").removeClass('error');
        }
        var edad = getAge($('#AdminTalentoHumano_fecha_nacimiento').val());
        if(edad<18 || edad>90){
            $("#AdminTalentoHumano_fecha_nacimiento").addClass('error');
            if(edad<18){
                displayDialogBox('resultado', 'error', 'Esta persona parece no ser mayor de edad.');
            }
            else{
                displayDialogBox('resultado', 'error', 'Esta persona es mayor de 90 años.');
            }
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
    }
    return true;
}

function registroDatosTalentoHumano(form){
    var datos = form.serialize();
    var urlDir = form.attr('action');

    if(datos.length>0 && urlDir.length){
        var divResult = "#resultado";
        var loadingEfect = true;
        var showResult = true;
        var method = "PUT";
        var responseFormat = "html";
        var successCallback = function(response, dom, statusCode){
            scrollUp();
        };
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
    }
}

function registroDatosBancarios(form){

    var datos = form.serialize();
    var urlDir = form.attr('action');

    if(datos.length>0 && urlDir.length){
        var divResult = "#divResultDatosBancarios";
        var loadingEfect = true;
        var showResult = true;
        var method = "POST";
        var responseFormat = "html";
        var successCallback = function(response, dom, statusCode){
            scrollUp();
        };
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
    }
}

function isValidOrigen(origen){
    var origenes = ['V', 'E', 'P'];
    if($.inArray(origen, origenes)>=0){ // La función inArray de jQuery te devuelve el índice del array en donde se encuentra el valor buscado
        return true;
    }
    return false;
}

function isValidBooleanEs(input){
    var booleans = ['Si', 'No'];
    if($.inArray(input, booleans)>=0){
        return true;
    }
    return false;
}

function isValidNombre(){
    $nombreInicial = $("#AdminTalentoHumano_nombre").attr('data-inicial');
    $nombre = $("#AdminTalentoHumano_nombre").val();
    $indiceNombre = levenshtein($nombreInicial, $nombre);
    console.log($indiceNombre);
    return ($indiceNombre<=5);
}

function isValidApellido(){
    $apellidoInicial = $("#AdminTalentoHumano_apellido").attr('data-inicial');
    $apellido = $("#AdminTalentoHumano_apellido").val();
    $indiceApellido = levenshtein($apellidoInicial, $apellido);
    console.log($indiceApellido);
    return ($indiceApellido<=5);
}


//--------------------------------------------------------------------------------------------------
//------------------------------------ estudiosrealizados.js ---------------------------------------
//--------------------------------------------------------------------------------------------------
$(document).ready(function(){

    modelEstudioRealizado.filterValues();

    $("#btnNuevoEstudioRealizado").on("click", function(){

        if(cantEstudioRealizado>=cantMaxEstudiosRealizados){
            $.gritter.add({
                title: 'Estudios realizados Excedidos',
                text: 'Sólo puede registrar hasta un máximo de '+cantMaxEstudiosRealizados+' Estudios Realizados.',
                class_name: 'gritter-error'
            });
            return false;
        }

        if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
        }
        $("#estudio-realizado-form").attr("data-form-type", "create");
        modelEstudioRealizado.showFormRegister($("#estudio-realizado-form"), 'create');
    });

    $("#estudio-realizado-form").on('submit', function(evt){
       evt.preventDefault();
       modelEstudioRealizado.submitForm($(this));
    });

    modelEstudioRealizado.addEventsToLinks();

});

var modelEstudioRealizado = {

    showFormRegister: function(form, type, data){

        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-estudio-realizado', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        $("#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", "today");

        var dialog = $("#divEstudioRealizadoDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-graduation-cap'></i> Registrar Nuevo Estudio Realizado</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroEstudioRealizado",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroEstudioRealizado",
                "click": function() {
                    $("#btnSubmitEstudioRealizado").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-estudio-realizado";
        var divResultSuccess = "#div-result-success-registro-estudio-realizado";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;

        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroEstudioRealizado").click();
                modelEstudioRealizado.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroEstudioRealizado").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroEstudioRealizado").removeAttr("disabled");
        };

        if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#EstudioRealizadoTh_fecha_egreso").val("");
            $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
            if($("#EstudioRealizadoTh_fecha_egreso").val()==''){
                mensaje = "Debe indicar la fecha de Egreso si ya ha Culminado con la Carga Académica del Estudio Realizado (Estatus: Culminado, En Espera del Título o En Espera de Notas Certificadas)<br/>"+mensaje;
            }
        }

        if(containsHtml($("#EstudioRealizadoTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#EstudioRealizadoTh_titulo_obtenido").val())){
            mensaje = "El Campo \"Titulo Obtenido\" posee datos no válidos.<br/>"+mensaje;
        }

        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroEstudioRealizado").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');

        var data = form.serializeObject(true, "EstudioRealizadoTh");
        data.id = id;
        data.nivelEstudio = $("#EstudioRealizadoTh_nivel_estudio_id option:selected").text();
        data.especialidadEstudio = $("#EstudioRealizadoTh_especialidad_estudio_id option:selected").text();
        data.institucionEducativa = $("#EstudioRealizadoTh_institucion_educativa_id option:selected").text();
        var estatus = {'A': 'En Curso', 'P': 'En Pasantías', 'T': 'En Espera del Título', 'N': 'En Esperra de Notas Certificadas', 'C': 'Culminado'};
        data.estatusEstudioRealizado = estatus[$("#EstudioRealizadoTh_estatus").val()];
        if(cantEstudioRealizado==0){
            $("#bodyTableEstudiosRealizados").html("");
        }

        var idRow = '';
        if (type=="update"){
            idRow=$("#EstudioRealizadoTh_row_id").val();//data-row-ft-x
        }
        else{
            idRow=idRowEstudioRealizado+lastIndexEstudioRealizado;
        }

        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.nivelEstudio)+"\">"+htmlentities(data.nivelEstudio.substring(0, 21))+"</td>\
                        <td title=\""+htmlentities(data.especialidadEstudio)+"\">"+htmlentities(data.especialidadEstudio.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.titulo_obtenido)+"\">"+htmlentities(data.titulo_obtenido.substring(0, 25))+"</td>\
                        <td title=\""+htmlentities(data.institucionEducativa)+"\">"+htmlentities(data.institucionEducativa.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.fecha_ingreso)+"\">"+htmlentities(data.fecha_ingreso)+"</td>\
                        <td title=\""+htmlentities(data.fecha_egreso)+"\">"+htmlentities(data.fecha_egreso)+"</td>\
                        <td title=\""+htmlentities(data.estatusEstudioRealizado)+"\">"+htmlentities(data.estatusEstudioRealizado.substring(0, 15))+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtEstudioRealizado\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelEstudioRealizado\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            lastIndexEstudioRealizado = lastIndexEstudioRealizado + 1;
            cantEstudioRealizado = cantEstudioRealizado + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#EstudioRealizadoTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableEstudiosRealizados").append(fila).ready(function(){
            modelEstudioRealizado.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtEstudioRealizado").unbind("click");
        $(".linkDelEstudioRealizado").unbind("click");

        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtEstudioRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#EstudioRealizadoTh_row_id").val(rowId);

                $("#EstudioRealizadoTh_id").val(base64_encode(data.id));
                $("#EstudioRealizadoTh_nivel_estudio_id").val(data.nivel_estudio_id);
                $("#EstudioRealizadoTh_especialidad_estudio_id").val(data.especialidad_estudio_id);
                $("#EstudioRealizadoTh_institucion_educativa_id").val(data.institucion_educativa_id);
                $("#EstudioRealizadoTh_titulo_obtenido").val(data.titulo_obtenido);
                $("#EstudioRealizadoTh_estatus").val(data.estatus);
                $("#EstudioRealizadoTh_fecha_ingreso").val(data.fecha_ingreso);
                $("#EstudioRealizadoTh_fecha_egreso").val(data.fecha_egreso);
                $("#EstudioRealizadoTh_observacion").val(data.observacion);

                if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
                    $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                }
                else{
                    $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
                }

                $("#estudio-realizado-form").attr("data-form-type", "update");
                modelEstudioRealizado.showFormRegister($("#estudio-realizado-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0001',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelEstudioRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#EstudioRealizadoTh_row_id").val(rowId);
                $("#EstudioRealizadoTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarEstudioRealizadoDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Estudio Realizado</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarEstudioRealizado",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarEstudioRealizado",
                        "click": function() {
                            modelEstudioRealizado.deleteData(data.id, rowId);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0002',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

    },

    deleteData: function(id, rowId){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#estudio-realizado-form");
            var divResultError = "#div-result-error-registro-estudio-realizado";
            var divResultSuccess = "#div-result-success-registro-estudio-realizado";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelEstudioRealizado.deleteRowFromTable(rowId);
                    $("#botonCancelarEliminarEstudioRealizado").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0003',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantEstudioRealizado = cantEstudioRealizado - 1;
        if(cantEstudioRealizado<0){
            cantEstudioRealizado=0;
        }
        $("#"+rowId).remove();
        if(cantEstudioRealizado==0){
            $("#bodyTableEstudiosRealizados").append("<tr><td colspan='8'><div class='alertDialogBox'><p>No se han registrado Estudios Realizados</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#EstudioRealizadoTh_titulo_obtenido, #EstudioRealizadoTh_observacion").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });

        $("#EstudioRealizadoTh_titulo_obtenido, #EstudioRealizadoTh_observacion").on('blur', function(){
            clearField(this);
        });

        $("#EstudioRealizadoTh_nivel_estudio_id").on("change", function(){
            if($(this).val()=='1' || $(this).val()=='2'){ // Bachiller(1) o Tecnico Medio(2)
                $("#EstudioRealizadoTh_institucion_educativa_id").val('0'); // Otra Institución Educativa(0)
            }
        });

        $("#EstudioRealizadoTh_estatus").on("change", function(){
            if(!in_array($(this).val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
                $("#EstudioRealizadoTh_fecha_egreso").val("");
                $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
            }
            else{
                $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
            }
        });

        $.datepicker.setDefaults({
            closeText: 'Cerrar',
            prevText: 'Anterior',
            nextText: 'Siguiente',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd-mm-yy',
            showOn: 'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1700, 1, 1),
            maxDate: 'yesterday'
        });

        $("#EstudioRealizadoTh_fecha_ingreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#EstudioRealizadoTh_fecha_egreso" ).datepicker( "option", "minDate", selectedDate+"" );
            }
        });

        $("#EstudioRealizadoTh_fecha_egreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#EstudioRealizadoTh_fecha_ingreso" ).datepicker( "option", "maxDate", selectedDate );
            },
            onSelect: function(dateText, inst)
            {
                if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){
                    $("#EstudioRealizadoTh_fecha_egreso").val("");
                    $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                    $.gritter.add({
                        title: 'Dato no válido',
                        text: 'Para efectuar el registro de la Fecha de Egreso debe indicar en el Campo Estatus alguno de los valores "Culminado, En Espera de Título ó En Espera de Notas Certificadas" con respecto al Estudio Realizado.',
                        class_name: 'gritter-warning'
                    });
                }
                else{
                    $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
                }
            }
        });

    }

};


//--------------------------------------------------------------------------------------------------
//------------------------------------ cursosrealizados.js ---------------------------------------
//--------------------------------------------------------------------------------------------------
$(document).ready(function(){

    modelCursoRealizado.filterValues();

    $("#btnNuevoCursoRealizado").on("click", function(){

        if(cantCursoRealizado>=cantMaxCursos){
            $.gritter.add({
                title: 'Cursos realizados Excedidos',
                text: 'Sólo puede registrar hasta un máximo de '+cantMaxCursos+' Cursos Realizados.',
                class_name: 'gritter-error'
            });
            return false;
        }

        $("#estatusActual").val("");

        if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
        }
        $("#curso-realizado-form").attr("data-form-type", "create");
        modelCursoRealizado.showFormRegister($("#curso-realizado-form"), 'create');
    });

    $("#curso-realizado-form").on('submit', function(evt){
       evt.preventDefault();
       modelCursoRealizado.submitForm($(this));
    });

    modelCursoRealizado.addEventsToLinks();

});

var modelCursoRealizado = {

    showFormRegister: function(form, type, data){

        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-curso-realizado', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        $( "#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", "today");

        var dialog = $("#divCursoRealizadoDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-book'></i> Registrar Nuevo Curso Realizado</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroCursoRealizado",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroCursoRealizado",
                "click": function() {
                    $("#btnSubmitCursoRealizado").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-curso-realizado";
        var divResultSuccess = "#div-result-success-registro-curso-realizado";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;

        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroCursoRealizado").click();
                modelCursoRealizado.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroCursoRealizado").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroCursoRealizado").removeAttr("disabled");
        };

        if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#CursoRealizadoTh_fecha_egreso").val("");
            $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
            if($("#CursoRealizadoTh_fecha_egreso").val()==''){
                mensaje = "Debe indicar la fecha de Egreso si ya ha Culminado con la Carga Académica del Curso Realizado (Estatus: Culminado)<br/>"+mensaje;
            }
        }

        if(containsHtml($("#CursoRealizadoTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#CursoRealizadoTh_titulo_obtenido").val())){
            mensaje = "El Campo \"Nombre o Temática del Curso\" posee datos no válidos.<br/>"+mensaje;
        }

        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroCursoRealizado").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');
        var sumaHoras = $("#txtSumaDeHorasAcademicas").html()*1;
        var data = form.serializeObject(true, "CursoRealizadoTh");
        var estatusActual = $("#estatusActual").val();
        data.id = id;
        data.especialidadCurso = $("#CursoRealizadoTh_especialidad_estudio_id option:selected").text();
        data.estatusCursoRealizado = $("#CursoRealizadoTh_estatus option:selected").text();
        var estatus = {'A': 'En Curso', 'P': 'En Pasantías', 'T': 'En Espera del Título', 'N': 'En Esperra de Notas Certificadas', 'C': 'Culminado'};

        var idRow = '';
        if (type=="update"){ // Actualización de Datos
            var horasActuales = 0;
            if(dataActualCursoRealizado.horas){
                horasActuales = dataActualCursoRealizado.horas*1;
            }

            if(data.estatus==='C'){
                if(estatusActual!='C'){ // Antes no estaba culminado, por tanto se suman las horas sin restar hora alguna
                    sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) + (data.horas*1);
                }
                else{ // Ya estaba culminado por lo que se deben restar las horas que ya formaban parte de las horas totales además de sumar las horas nuevas
                    sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) + (data.horas*1) - (horasActuales);
                }
            }
            else{
                sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) - (horasActuales);
            }

            idRow=$("#CursoRealizadoTh_row_id").val();//data-row-ft-x
        }
        else{ // Nuevo Registro
            if(data.estatus=='C'){
                sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) + (data.horas*1);
            }
            else{
                sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1);
            }
            idRow=idRowCursoRealizado+lastIndexCursoRealizado;
        }

        $("#txtSumaDeHorasAcademicas").html(sumaHoras);

        if(cantCursoRealizado==0){
            $("#bodyTableCursosRealizados").html("");
        }
        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.especialidadCurso)+"\">"+htmlentities(data.especialidadCurso.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.titulo_obtenido)+"\">"+htmlentities(data.titulo_obtenido.substring(0, 25))+"</td>\
                        <td title=\""+htmlentities(data.institucion_educativa)+"\">"+htmlentities(data.institucion_educativa.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.horas)+"\">"+htmlentities(data.horas)+"</td>\
                        <td title=\""+htmlentities(data.fecha_egreso)+"\">"+htmlentities(data.fecha_egreso)+"</td>\
                        <td title=\""+htmlentities(data.estatusCursoRealizado)+"\">"+htmlentities(data.estatusCursoRealizado.substring(0, 15))+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtCursoRealizado\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelCursoRealizado\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            cantCursoRealizado = cantCursoRealizado + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#CursoRealizadoTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableCursosRealizados").append(fila).ready(function(){
            modelCursoRealizado.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtCursoRealizado").unbind("click");
        $(".linkDelCursoRealizado").unbind("click");

        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtCursoRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                dataActualCursoRealizado = data;

                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#CursoRealizadoTh_row_id").val(rowId);

                $("#CursoRealizadoTh_id").val(base64_encode(data.id));
                $("#CursoRealizadoTh_especialidad_estudio_id").val(data.especialidad_estudio_id);
                $("#CursoRealizadoTh_institucion_educativa").val(data.institucion_educativa);
                $("#CursoRealizadoTh_titulo_obtenido").val(data.titulo_obtenido);
                $("#CursoRealizadoTh_estatus").val(data.estatus);
                $("#CursoRealizadoTh_horas").val(data.horas);
                $("#CursoRealizadoTh_fecha_egreso").val(data.fecha_egreso);
                $("#CursoRealizadoTh_observacion").val(data.observacion);
                $("#estatusActual").val(data.estatus);

                if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){ // Si no está en Culminado
                    $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                }
                else{
                    $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
                }

                $("#curso-realizado-form").attr("data-form-type", "update");
                modelCursoRealizado.showFormRegister($("#curso-realizado-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0001',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelCursoRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#CursoRealizadoTh_row_id").val(rowId);
                $("#CursoRealizadoTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarCursoRealizadoDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Curso Realizado</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarCursoRealizado",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarCursoRealizado",
                        "click": function() {
                            modelCursoRealizado.deleteData(data.id, rowId, data);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0002',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

    },

    deleteData: function(id, rowId, data){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#curso-realizado-form");
            var divResultError = "#div-result-error-registro-curso-realizado";
            var divResultSuccess = "#div-result-success-registro-curso-realizado";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelCursoRealizado.deleteRowFromTable(rowId, data);
                    $("#botonCancelarEliminarCursoRealizado").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0003',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId, data){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantCursoRealizado = cantCursoRealizado - 1;
        if(cantCursoRealizado<0){
            cantCursoRealizado=0;
        }
        $("#"+rowId).remove();

        var sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1);

        console.log(data.estatus);
        console.log(data.estatus=='C');
        if(data.estatus=='C'){
            sumaHoras = sumaHoras - (data.horas*1);
        }

        if(sumaHoras<0){
            sumaHoras = 0;
        }
        $("#txtSumaDeHorasAcademicas").html(sumaHoras);
        if(cantCursoRealizado==0){
            $("#txtSumaDeHorasAcademicas").html(0);
            $("#bodyTableCursosRealizados").append("<tr><td colspan='7'><div class='alertDialogBox'><p>No se han registrado Cursos Realizados</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#CursoRealizadoTh_titulo_obtenido, #CursoRealizadoTh_observacion, #CursoRealizadoTh_institucion_educativa").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });

        $("#CursoRealizadoTh_horas").numeric();

        $("#CursoRealizadoTh_titulo_obtenido, #CursoRealizadoTh_observacion, #CursoRealizadoTh_horas").on('blur', function(){
            clearField(this);
        });

        $("#CursoRealizadoTh_estatus").on("change", function(){
            if(!in_array($(this).val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
                $("#CursoRealizadoTh_fecha_egreso").val("");
                $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
            }
            else{
                $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
            }
        });

        $.datepicker.setDefaults({
            closeText: 'Cerrar',
            prevText: 'Anterior',
            nextText: 'Siguiente',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd-mm-yy',
            showOn: 'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1700, 1, 1),
            maxDate: 'yesterday'
        });

        $("#CursoRealizadoTh_fecha_egreso").datepicker({
            onSelect: function(dateText, inst)
            {
                if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){
                    $("#CursoRealizadoTh_fecha_egreso").val("");
                    $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                    $.gritter.add({
                        title: 'Dato no válido',
                        text: 'Para efectuar el registro de la Fecha de Egreso debe indicar en el Campo Estatus el valor "Culminado" con respecto al Curso Realizado.',
                        class_name: 'gritter-warning'
                    });
                }
                else{
                    $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
                }
            }
        });

    }

};


//--------------------------------------------------------------------------------------------------
//------------------------------------ experienciaLaboral.js ---------------------------------------
//--------------------------------------------------------------------------------------------------
$(document).ready(function(){

    modelExperienciaLaboral.filterValues();

    $("#btnNuevaExperienciaLaboral").on("click", function(){

       
        if(cantExperienciaLaboral>=cantMaxExperienciaLaboral){
            $.gritter.add({
                title: 'Experiencias Laborales Excedidas',
                text: 'Sólo puede registrar hasta un máximo de '+ cantMaxExperienciaLaboral+' Experiencias Laborales.',
                class_name: 'gritter-error'
            });
            return false;
        }

        var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();
        if(activoActualmente=='S'){ // Si indica que está activo no debería indicar fecha de egreso
            $("#ExperienciaLaboralTh_fecha_egreso").val("");
            $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
        }
        else{
            $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
        }
        $("#experiencia-laboral-form").attr("data-form-type", "create");
        modelExperienciaLaboral.showFormRegister($("#experiencia-laboral-form"), 'create');
    });

    $("#experiencia-laboral-form").on('submit', function(evt){
       evt.preventDefault();
       modelExperienciaLaboral.submitForm($(this));
    });

    modelExperienciaLaboral.addEventsToLinks();

});

var modelExperienciaLaboral = {

    showFormRegister: function(form, type, data){

        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-experiencia-laboral', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        $( "#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", "+0m +0w");

        var dialog = $("#divExperienciaLaboralDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-building-o'></i> Registrar Nueva Experiencia Laboral</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroExperienciaLaboral",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroExperienciaLaboral",
                "click": function() {
                    $("#btnSubmitExperienciaLaboral").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-experiencia-laboral";
        var divResultSuccess = "#div-result-success-registro-experiencia-laboral";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;

        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroExperienciaLaboral").click();
                modelExperienciaLaboral.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroExperienciaLaboral").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroExperienciaLaboral").removeAttr("disabled");
        };


        var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();
        if(activoActualmente=='S'){ // Si indica que está activo no debería indicar fecha de egreso
            $("#ExperienciaLaboralTh_fecha_egreso").val("");
            $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
        }
        else{
            $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
            if($("#ExperienciaLaboralTh_fecha_egreso").val()==''){
                mensaje = "Debe indicar la Fecha de Egreso si ya no se encuentra activo en esta empresa o institución<br/>"+mensaje;
            }
        }

        if(containsHtml($("#ExperienciaLaboralTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#ExperienciaLaboralTh_actividades_realizadas").val())){
            mensaje = "El Campo \"Actividades Desempeñadas\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#ExperienciaLaboralTh_institucion").val())){
            mensaje = "El Campo \"Empresa/Institución\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#ExperienciaLaboralTh_cargo_desempenhado").val())){
            mensaje = "El Campo \"Cargo Desempeñado\" posee datos no válidos.<br/>"+mensaje;
        }

        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroExperienciaLaboral").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');
        var data = form.serializeObject(true, "ExperienciaLaboralTh");

        var booleano = {'S': 'Sí', 'N': 'No'};

        data.id = id;
        data.estaActivoActualmente = booleano[$("#ExperienciaLaboralTh_activo_actualmente").val()];
        data.esPublica = booleano[$("#ExperienciaLaboralTh_publica").val()];

        if(cantExperienciaLaboral==0){
            $("#bodyTableExperienciasLaborales").html("");
        }

        var idRow = '';
        if (type=="update"){
            idRow=$("#ExperienciaLaboralTh_row_id").val();//data-row-ft-x
        }
        else{
            idRow=idRowExperienciaLaboral+lastIndexExperienciaLaboral;
        }

        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.institucion)+"\">"+htmlentities(data.institucion.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.esPublica)+"\">"+htmlentities(data.esPublica)+"</td>\
                        <td title=\""+htmlentities(data.cargo_desempenhado)+"\">"+htmlentities(data.cargo_desempenhado.substring(0, 35))+"</td>\
                        <td title=\""+htmlentities(data.estaActivoActualmente)+"\">"+htmlentities(data.estaActivoActualmente)+"</td>\
                        <td title=\""+htmlentities(data.fecha_ingreso)+"\">"+htmlentities(data.fecha_ingreso)+"</td>\
                        <td title=\""+htmlentities(data.fecha_egreso)+"\">"+htmlentities(data.fecha_egreso)+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtExperienciaLaboral\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelExperienciaLaboral\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            lastIndexExperienciaLaboral = lastIndexExperienciaLaboral + 1;
            cantExperienciaLaboral = cantExperienciaLaboral + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#ExperienciaLaboralTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableExperienciasLaborales").append(fila).ready(function(){
            modelExperienciaLaboral.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtExperienciaLaboral").unbind("click");
        $(".linkDelExperienciaLaboral").unbind("click");

        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtExperienciaLaboral").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#ExperienciaLaboralTh_row_id").val(rowId);

                $("#ExperienciaLaboralTh_id").val(base64_encode(data.id));
                $("#ExperienciaLaboralTh_institucion").val(data.institucion);
                $("#ExperienciaLaboralTh_publica").val(data.publica);
                $("#ExperienciaLaboralTh_institucion_educativa_id").val(data.institucion_educativa_id);
                $("#ExperienciaLaboralTh_cargo_desempenhado").val(data.cargo_desempenhado);
                $("#ExperienciaLaboralTh_nombre_referencia").val(data.nombre_referencia);
                $("#ExperienciaLaboralTh_telefono_referencia").val(data.telefono_referencia);
                $("#ExperienciaLaboralTh_activo_actualmente").val(data.activo_actualmente);
                $("#ExperienciaLaboralTh_fecha_ingreso").val(data.fecha_ingreso);
                $("#ExperienciaLaboralTh_fecha_egreso").val(data.fecha_egreso);
                $("#ExperienciaLaboralTh_actividades_realizadas").val(data.actividades_realizadas);
                $("#ExperienciaLaboralTh_observacion").val(data.observacion);

                var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();

                if(activoActualmente=='S'){
                    $("#ExperienciaLaboralTh_fecha_egreso").val("");
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
                }
                else{
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
                }

                $("#experiencia-laboral-form").attr("data-form-type", "update");
                modelExperienciaLaboral.showFormRegister($("#experiencia-laboral-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0004',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelExperienciaLaboral").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#ExperienciaLaboralTh_row_id").val(rowId);
                $("#ExperienciaLaboralTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarExperienciaLaboralDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Experiencia Laboral</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarExperienciaLaboral",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarExperienciaLaboral",
                        "click": function() {
                            modelExperienciaLaboral.deleteData(data.id, rowId);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0005',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

    },

    deleteData: function(id, rowId){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#experiencia-laboral-form");
            var divResultError = "#div-result-error-registro-experiencia-laboral";
            var divResultSuccess = "#div-result-success-registro-experiencia-laboral";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelExperienciaLaboral.deleteRowFromTable(rowId);
                    $("#botonCancelarEliminarExperienciaLaboral").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0006',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantExperienciaLaboral = cantExperienciaLaboral - 1;
        if(cantExperienciaLaboral<0){
            cantExperienciaLaboral=0;
        }
        $("#"+rowId).remove();
        if(cantExperienciaLaboral==0){
            $("#bodyTableExperienciasLaborales").append("<tr><td colspan='7'><div class='alertDialogBox'><p>No se han registrado Experiencias Laborales</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#ExperienciaLaboralTh_institucion, #ExperienciaLaboralTh_cargo_desempenhado, #ExperienciaLaboralTh_nombre_referencia").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });

        $("#ExperienciaLaboralTh_observacion, #ExperienciaLaboralTh_actividades_realizadas").on('keyup blur', function(){
            keyText(this, true);
            makeUpper(this);
        });

        $("#ExperienciaLaboralTh_institucion, #ExperienciaLaboralTh_cargo_desempenhado, #ExperienciaLaboralTh_nombre_referencia, #ExperienciaLaboralTh_observacion, #ExperienciaLaboralTh_actividades_realizadas").on('blur', function(){
            clearField(this);
        });

        $("#ExperienciaLaboralTh_activo_actualmente").on("change", function(){
            if($(this).val()=='S'){ // Si indica que está activo no debería indicar fecha de egreso
                $("#ExperienciaLaboralTh_fecha_egreso").val("");
                $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
                $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
            }
            else{
                $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
                $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
            }
        });

        $.datepicker.setDefaults({
            closeText: 'Cerrar',
            prevText: 'Anterior',
            nextText: 'Siguiente',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd-mm-yy',
            showOn: 'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1700, 1, 1),
            maxDate: 'yesterday'
        });

        $("#ExperienciaLaboralTh_fecha_ingreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#ExperienciaLaboralTh_fecha_egreso" ).datepicker( "option", "minDate", selectedDate+"" );
            }
        });

        $("#ExperienciaLaboralTh_fecha_egreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", selectedDate );
            },
            onSelect: function(dateText, inst)
            {
                var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();

                if(activoActualmente=='S'){
                    $("#ExperienciaLaboralTh_fecha_egreso").val("");
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
                    $.gritter.add({
                        title: 'Dato no válido',
                        text: 'Para efectuar el registro de la Fecha de Egreso no debe indicar que se encuentra activo en esta empresa o institución.',
                        class_name: 'gritter-warning'
                    });
                }
                else{
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
                }

            }
        });

        $('#ExperienciaLaboralTh_telefono_referencia').mask('(0999)999-9999');

    }

};


//--------------------------------------------------------------------------------------------------
//------------------------------------ referenciasPersonales.js ---------------------------------------
//--------------------------------------------------------------------------------------------------
$(document).ready(function(){

    modelReferenciaPersonal.filterValues();

    $("#btnNuevaReferenciaPersonal").on("click", function(){
          if(cantReferenciaPersonal>=cantMaxReferenciaPersonal){
            $.gritter.add({
                title: 'Referencias Personales Excedidas',
                text: 'Sólo puede registrar hasta un máximo de '+cantMaxReferenciaPersonal+' Referencias Personales.',
                class_name: 'gritter-error'
            });
            return false;
        }
        $("#referencia-personal-form").attr("data-form-type", "create");
        modelReferenciaPersonal.showFormRegister($("#referencia-personal-form"), 'create');
    });

    $("#referencia-personal-form").on('submit', function(evt){
       evt.preventDefault();
       modelReferenciaPersonal.submitForm($(this));
    });

    modelReferenciaPersonal.addEventsToLinks();

});

var modelReferenciaPersonal = {

    showFormRegister: function(form, type, data){

        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-referencia-personal', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        var dialog = $("#divReferenciaPersonalDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-group'></i> Registrar Nueva Referencia Personal</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroReferenciaPersonal",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroReferenciaPersonal",
                "click": function() {
                    $("#btnSubmitReferenciaPersonal").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-referencia-personal";
        var divResultSuccess = "#div-result-success-registro-referencia-personal";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;

        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroReferenciaPersonal").click();
                modelReferenciaPersonal.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroReferenciaPersonal").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroReferenciaPersonal").removeAttr("disabled");
        };

        if(containsHtml($("#ReferenciaPersonalTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#ReferenciaPersonalTh_nombre_referencia").val())){
            mensaje = "El Campo \"Nombre y Apellido\" posee datos no válidos.<br/>"+mensaje;
        }

        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroReferenciaPersonal").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');

        var data = form.serializeObject(true, "ReferenciaPersonalTh");
        data.id = id;
        data.funcionarioMppe = $("#ReferenciaPersonalTh_funcionario_mppe option:selected").text();
        if(cantReferenciaPersonal==0){
            $("#bodyTableReferenciasPersonales").html("");
        }

        var idRow = '';
        if (type=="update"){
            idRow=$("#ReferenciaPersonalTh_row_id").val();//data-row-ft-x
        }
        else{
            idRow=idRowReferenciaPersonal+lastIndexReferenciaPersonal;
        }

        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.cedula_referencia)+"\">"+htmlentities(data.cedula_referencia.toUpperCase())+"</td>\
                        <td title=\""+htmlentities(data.nombre_referencia)+"\">"+htmlentities(data.nombre_referencia.substring(0,80))+"</td>\
                        <td title=\""+htmlentities(data.telefono_referencia)+"\">"+htmlentities(data.telefono_referencia)+"</td>\
                        <td title=\""+htmlentities(data.email_referencia)+"\">"+htmlentities(data.email_referencia)+"</td>\
                        <td title=\""+htmlentities(data.funcionarioMppe)+"\">"+htmlentities(data.funcionarioMppe)+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtReferenciaPersonal\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelReferenciaPersonal\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            lastIndexReferenciaPersonal = lastIndexReferenciaPersonal + 1;
            cantReferenciaPersonal = cantReferenciaPersonal + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#ReferenciaPersonalTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableReferenciasPersonales").append(fila).ready(function(){
            modelReferenciaPersonal.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtReferenciaPersonal").unbind("click");
        $(".linkDelReferenciaPersonal").unbind("click");

        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtReferenciaPersonal").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#ReferenciaPersonalTh_row_id").val(rowId);

                $("#ReferenciaPersonalTh_id").val(base64_encode(data.id));
                $("#ReferenciaPersonalTh_nombre_referencia").val(data.nombre_referencia);
                $("#ReferenciaPersonalTh_cedula_referencia").val(data.cedula_referencia.toUpperCase());
                $("#ReferenciaPersonalTh_email_referencia").val(data.email_referencia);
                $("#ReferenciaPersonalTh_telefono_referencia").val(data.telefono_referencia);
                $("#ReferenciaPersonalTh_observacion").val(data.observacion);

                $("#referencia-personal-form").attr("data-form-type", "update");
                modelReferenciaPersonal.showFormRegister($("#referencia-personal-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0007',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelReferenciaPersonal").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#ReferenciaPersonalTh_row_id").val(rowId);
                $("#ReferenciaPersonalTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarReferenciaPersonalDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Referencia Personal</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarReferenciaPersonal",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarReferenciaPersonal",
                        "click": function() {
                            modelReferenciaPersonal.deleteData(data.id, rowId);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0008',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

    },

    deleteData: function(id, rowId){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#referencia-personal-form");
            var divResultError = "#div-result-error-registro-referencia-personal";
            var divResultSuccess = "#div-result-success-registro-referencia-personal";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelReferenciaPersonal.deleteRowFromTable(rowId);
                    $("#botonCancelarEliminarReferenciaPersonal").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0009',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantReferenciaPersonal = cantReferenciaPersonal - 1;
        if(cantReferenciaPersonal<0){
            cantReferenciaPersonal=0;
        }
        $("#"+rowId).remove();
        if(cantReferenciaPersonal==0){
            $("#bodyTableReferenciasPersonales").append("<tr><td colspan='6'><div class='alertDialogBox'><p>No se han registrado Referencias Personales</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#ReferenciaPersonalTh_nombre_referencia").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });

        $("#ReferenciaPersonalTh_email_referencia").on('keyup blur', function(){
            keyEmail(this, true, true);
            makeUpper(this);
        });

        $("#ReferenciaPersonalTh_observacion").on('keyup blur', function(){
            keyText(this, true);
            makeUpper(this);
        });

        $("#ReferenciaPersonalTh_nombre_referencia, #ReferenciaPersonalTh_email_referencia, #ReferenciaPersonalTh_observacion").on('blur', function(){
            clearField(this);
        });

        $('#ReferenciaPersonalTh_telefono_referencia').mask('(0999)999-9999');

        $.mask.definitions['X'] = '[V|E|P|v|e|p]';
        $('#ReferenciaPersonalTh_cedula_referencia').mask('X-999?9999999999');

    }

};
