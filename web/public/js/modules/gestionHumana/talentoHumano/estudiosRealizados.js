$(document).ready(function(){

    modelEstudioRealizado.filterValues();
    
    $("#btnNuevoEstudioRealizado").on("click", function(){
        
        if(cantEstudioRealizado>=cantMaxEstudiosRealizados){
            $.gritter.add({
                title: 'Estudios realizados Excedidos',
                text: 'Sólo puede registrar hasta un máximo de '+cantMaxEstudiosRealizados+' Estudios Realizados.',
                class_name: 'gritter-error'
            });
            return false;
        }
        
        if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
        }
        $("#estudio-realizado-form").attr("data-form-type", "create");
        modelEstudioRealizado.showFormRegister($("#estudio-realizado-form"), 'create');
    });
    
    $("#estudio-realizado-form").on('submit', function(evt){
       evt.preventDefault();
       modelEstudioRealizado.submitForm($(this));
    });
    
    modelEstudioRealizado.addEventsToLinks();

});

var modelEstudioRealizado = {
    
    showFormRegister: function(form, type, data){
        
        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-estudio-realizado', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        $("#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", "today");
        
        var dialog = $("#divEstudioRealizadoDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-graduation-cap'></i> Registrar Nuevo Estudio Realizado</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroEstudioRealizado",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroEstudioRealizado",
                "click": function() {
                    $("#btnSubmitEstudioRealizado").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-estudio-realizado";
        var divResultSuccess = "#div-result-success-registro-estudio-realizado";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;
        
        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroEstudioRealizado").click();
                modelEstudioRealizado.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroEstudioRealizado").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroEstudioRealizado").removeAttr("disabled");
        };

        if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#EstudioRealizadoTh_fecha_egreso").val("");
            $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
            if($("#EstudioRealizadoTh_fecha_egreso").val()==''){
                mensaje = "Debe indicar la fecha de Egreso si ya ha Culminado con la Carga Académica del Estudio Realizado (Estatus: Culminado, En Espera del Título o En Espera de Notas Certificadas)<br/>"+mensaje;
            }
        }
        
        if(containsHtml($("#EstudioRealizadoTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }
        
        if(containsHtml($("#EstudioRealizadoTh_titulo_obtenido").val())){
            mensaje = "El Campo \"Titulo Obtenido\" posee datos no válidos.<br/>"+mensaje;
        }
        
        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroEstudioRealizado").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');

        var data = form.serializeObject(true, "EstudioRealizadoTh");
        data.id = id;
        data.nivelEstudio = $("#EstudioRealizadoTh_nivel_estudio_id option:selected").text();
        data.especialidadEstudio = $("#EstudioRealizadoTh_especialidad_estudio_id option:selected").text();
        data.institucionEducativa = $("#EstudioRealizadoTh_institucion_educativa_id option:selected").text();
        var estatus = {'A': 'En Curso', 'P': 'En Pasantías', 'T': 'En Espera del Título', 'N': 'En Esperra de Notas Certificadas', 'C': 'Culminado'};
        data.estatusEstudioRealizado = estatus[$("#EstudioRealizadoTh_estatus").val()];
        if(cantEstudioRealizado==0){
            $("#bodyTableEstudiosRealizados").html("");
        }
        
        var idRow = '';
        if (type=="update"){
            idRow=$("#EstudioRealizadoTh_row_id").val();//data-row-ft-x
        }
        else{
            idRow=idRowEstudioRealizado+lastIndexEstudioRealizado;
        }
        
        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.nivelEstudio)+"\">"+htmlentities(data.nivelEstudio.substring(0, 21))+"</td>\
                        <td title=\""+htmlentities(data.especialidadEstudio)+"\">"+htmlentities(data.especialidadEstudio.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.titulo_obtenido)+"\">"+htmlentities(data.titulo_obtenido.substring(0, 25))+"</td>\
                        <td title=\""+htmlentities(data.institucionEducativa)+"\">"+htmlentities(data.institucionEducativa.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.fecha_ingreso)+"\">"+htmlentities(data.fecha_ingreso)+"</td>\
                        <td title=\""+htmlentities(data.fecha_egreso)+"\">"+htmlentities(data.fecha_egreso)+"</td>\
                        <td title=\""+htmlentities(data.estatusEstudioRealizado)+"\">"+htmlentities(data.estatusEstudioRealizado.substring(0, 15))+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtEstudioRealizado\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelEstudioRealizado\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            lastIndexEstudioRealizado = lastIndexEstudioRealizado + 1;
            cantEstudioRealizado = cantEstudioRealizado + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#EstudioRealizadoTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableEstudiosRealizados").append(fila).ready(function(){
            modelEstudioRealizado.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtEstudioRealizado").unbind("click");
        $(".linkDelEstudioRealizado").unbind("click");
        
        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtEstudioRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#EstudioRealizadoTh_row_id").val(rowId);

                $("#EstudioRealizadoTh_id").val(base64_encode(data.id));
                $("#EstudioRealizadoTh_nivel_estudio_id").val(data.nivel_estudio_id);
                $("#EstudioRealizadoTh_especialidad_estudio_id").val(data.especialidad_estudio_id);
                $("#EstudioRealizadoTh_institucion_educativa_id").val(data.institucion_educativa_id);
                $("#EstudioRealizadoTh_titulo_obtenido").val(data.titulo_obtenido);
                $("#EstudioRealizadoTh_estatus").val(data.estatus);
                $("#EstudioRealizadoTh_fecha_ingreso").val(data.fecha_ingreso);
                $("#EstudioRealizadoTh_fecha_egreso").val(data.fecha_egreso);
                $("#EstudioRealizadoTh_observacion").val(data.observacion);
                
                if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
                    $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                }
                else{
                    $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
                }

                $("#estudio-realizado-form").attr("data-form-type", "update");
                modelEstudioRealizado.showFormRegister($("#estudio-realizado-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0001',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelEstudioRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#EstudioRealizadoTh_row_id").val(rowId);
                $("#EstudioRealizadoTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarEstudioRealizadoDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Estudio Realizado</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarEstudioRealizado",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarEstudioRealizado",
                        "click": function() {
                            modelEstudioRealizado.deleteData(data.id, rowId);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0002',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });
        
    },

    deleteData: function(id, rowId){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#estudio-realizado-form");
            var divResultError = "#div-result-error-registro-estudio-realizado";
            var divResultSuccess = "#div-result-success-registro-estudio-realizado";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelEstudioRealizado.deleteRowFromTable(rowId);
                    $("#botonCancelarEliminarEstudioRealizado").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0003',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantEstudioRealizado = cantEstudioRealizado - 1;
        if(cantEstudioRealizado<0){
            cantEstudioRealizado=0;
        }
        $("#"+rowId).remove();
        if(cantEstudioRealizado==0){
            $("#bodyTableEstudiosRealizados").append("<tr><td colspan='8'><div class='alertDialogBox'><p>No se han registrado Estudios Realizados</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#EstudioRealizadoTh_titulo_obtenido, #EstudioRealizadoTh_observacion").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });

        $("#EstudioRealizadoTh_titulo_obtenido, #EstudioRealizadoTh_observacion").on('blur', function(){
            clearField(this);
        });

        $("#EstudioRealizadoTh_nivel_estudio_id").on("change", function(){
            if($(this).val()=='1' || $(this).val()=='2'){ // Bachiller(1) o Tecnico Medio(2)
                $("#EstudioRealizadoTh_institucion_educativa_id").val('0'); // Otra Institución Educativa(0)
            }
        });

        $("#EstudioRealizadoTh_estatus").on("change", function(){
            if(!in_array($(this).val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
                $("#EstudioRealizadoTh_fecha_egreso").val("");
                $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
            }
            else{
                $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
            }
        });

        $.datepicker.setDefaults({
            closeText: 'Cerrar',
            prevText: 'Anterior',
            nextText: 'Siguiente',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd-mm-yy',
            showOn: 'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1700, 1, 1),
            maxDate: 'yesterday'
        });

        $("#EstudioRealizadoTh_fecha_ingreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#EstudioRealizadoTh_fecha_egreso" ).datepicker( "option", "minDate", selectedDate+"" );
            }
        });

        $("#EstudioRealizadoTh_fecha_egreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#EstudioRealizadoTh_fecha_ingreso" ).datepicker( "option", "maxDate", selectedDate );
            },
            onSelect: function(dateText, inst)
            {
                if(!in_array($("#EstudioRealizadoTh_estatus").val(), ['C', 'T', 'N'])){
                    $("#EstudioRealizadoTh_fecha_egreso").val("");
                    $("#EstudioRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                    $.gritter.add({
                        title: 'Dato no válido',
                        text: 'Para efectuar el registro de la Fecha de Egreso debe indicar en el Campo Estatus alguno de los valores "Culminado, En Espera de Título ó En Espera de Notas Certificadas" con respecto al Estudio Realizado.',
                        class_name: 'gritter-warning'
                    });
                }
                else{
                    $("#EstudioRealizadoTh_fecha_egreso").removeAttr("disabled");
                }
            }
        });

    }

};
