$(document).ready(function(){

    modelCursoRealizado.filterValues();
    
    $("#btnNuevoCursoRealizado").on("click", function(){
        if(cantCursoRealizado>=cantMaxCursos){
            $.gritter.add({
                title: 'Cursos realizados Excedidos',
                text: 'Sólo puede registrar hasta un máximo de '+cantMaxCursos+' Cursos Realizados.',
                class_name: 'gritter-error'
            });
            return false;
        }

        $("#estatusActual").val("");

        if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
        }
        $("#curso-realizado-form").attr("data-form-type", "create");
        modelCursoRealizado.showFormRegister($("#curso-realizado-form"), 'create');
    });
    
    $("#curso-realizado-form").on('submit', function(evt){
       evt.preventDefault();
       modelCursoRealizado.submitForm($(this));
    });
    
    modelCursoRealizado.addEventsToLinks();

});

var modelCursoRealizado = {
    
    showFormRegister: function(form, type, data){
        
        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-curso-realizado', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        $( "#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", "today");
        
        var dialog = $("#divCursoRealizadoDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-book'></i> Registrar Nuevo Curso Realizado</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroCursoRealizado",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroCursoRealizado",
                "click": function() {
                    $("#btnSubmitCursoRealizado").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-curso-realizado";
        var divResultSuccess = "#div-result-success-registro-curso-realizado";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;

        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroCursoRealizado").click();
                modelCursoRealizado.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroCursoRealizado").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroCursoRealizado").removeAttr("disabled");
        };

        if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
            $("#CursoRealizadoTh_fecha_egreso").val("");
            $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
        }
        else{
            $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
            if($("#CursoRealizadoTh_fecha_egreso").val()==''){
                mensaje = "Debe indicar la fecha de Egreso si ya ha Culminado con la Carga Académica del Curso Realizado (Estatus: Culminado)<br/>"+mensaje;
            }
        }
        
        if(containsHtml($("#CursoRealizadoTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }
        
        if(containsHtml($("#CursoRealizadoTh_titulo_obtenido").val())){
            mensaje = "El Campo \"Nombre o Temática del Curso\" posee datos no válidos.<br/>"+mensaje;
        }
        
        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroCursoRealizado").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');
        var sumaHoras = $("#txtSumaDeHorasAcademicas").html()*1;
        var data = form.serializeObject(true, "CursoRealizadoTh");
        var estatusActual = $("#estatusActual").val();
        data.id = id;
        data.especialidadCurso = $("#CursoRealizadoTh_especialidad_estudio_id option:selected").text();
        data.estatusCursoRealizado = $("#CursoRealizadoTh_estatus option:selected").text();
        var estatus = {'A': 'En Curso', 'P': 'En Pasantías', 'T': 'En Espera del Título', 'N': 'En Esperra de Notas Certificadas', 'C': 'Culminado'};

        var idRow = '';
        if (type=="update"){ // Actualización de Datos
            var horasActuales = 0;
            if(dataActualCursoRealizado.horas){
                horasActuales = dataActualCursoRealizado.horas*1;
            }

            if(data.estatus==='C'){
                if(estatusActual!='C'){ // Antes no estaba culminado, por tanto se suman las horas sin restar hora alguna
                    sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) + (data.horas*1);
                }
                else{ // Ya estaba culminado por lo que se deben restar las horas que ya formaban parte de las horas totales además de sumar las horas nuevas
                    sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) + (data.horas*1) - (horasActuales);
                }
            }
            else{
                sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) - (horasActuales);
            }

            idRow=$("#CursoRealizadoTh_row_id").val();//data-row-ft-x
        }
        else{ // Nuevo Registro
            if(data.estatus=='C'){
                sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1) + (data.horas*1);
            }
            else{
                sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1);
            }
            idRow=idRowCursoRealizado+lastIndexCursoRealizado;
        }

        $("#txtSumaDeHorasAcademicas").html(sumaHoras);
        
        if(cantCursoRealizado==0){
            $("#bodyTableCursosRealizados").html("");
        }
        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.especialidadCurso)+"\">"+htmlentities(data.especialidadCurso.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.titulo_obtenido)+"\">"+htmlentities(data.titulo_obtenido.substring(0, 25))+"</td>\
                        <td title=\""+htmlentities(data.institucion_educativa)+"\">"+htmlentities(data.institucion_educativa.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.horas)+"\">"+htmlentities(data.horas)+"</td>\
                        <td title=\""+htmlentities(data.fecha_egreso)+"\">"+htmlentities(data.fecha_egreso)+"</td>\
                        <td title=\""+htmlentities(data.estatusCursoRealizado)+"\">"+htmlentities(data.estatusCursoRealizado.substring(0, 15))+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtCursoRealizado\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelCursoRealizado\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            cantCursoRealizado = cantCursoRealizado + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#CursoRealizadoTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableCursosRealizados").append(fila).ready(function(){
            modelCursoRealizado.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtCursoRealizado").unbind("click");
        $(".linkDelCursoRealizado").unbind("click");
        
        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtCursoRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");
                
                dataActualCursoRealizado = data;
                
                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#CursoRealizadoTh_row_id").val(rowId);

                $("#CursoRealizadoTh_id").val(base64_encode(data.id));
                $("#CursoRealizadoTh_especialidad_estudio_id").val(data.especialidad_estudio_id);
                $("#CursoRealizadoTh_institucion_educativa").val(data.institucion_educativa);
                $("#CursoRealizadoTh_titulo_obtenido").val(data.titulo_obtenido);
                $("#CursoRealizadoTh_estatus").val(data.estatus);
                $("#CursoRealizadoTh_horas").val(data.horas);
                $("#CursoRealizadoTh_fecha_egreso").val(data.fecha_egreso);
                $("#CursoRealizadoTh_observacion").val(data.observacion);
                $("#estatusActual").val(data.estatus);
                
                if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){ // Si no está en Culminado
                    $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                }
                else{
                    $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
                }

                $("#curso-realizado-form").attr("data-form-type", "update");
                modelCursoRealizado.showFormRegister($("#curso-realizado-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0001',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelCursoRealizado").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#CursoRealizadoTh_row_id").val(rowId);
                $("#CursoRealizadoTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarCursoRealizadoDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Curso Realizado</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarCursoRealizado",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarCursoRealizado",
                        "click": function() {
                            modelCursoRealizado.deleteData(data.id, rowId, data);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0002',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });
        
    },

    deleteData: function(id, rowId, data){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#curso-realizado-form");
            var divResultError = "#div-result-error-registro-curso-realizado";
            var divResultSuccess = "#div-result-success-registro-curso-realizado";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelCursoRealizado.deleteRowFromTable(rowId, data);
                    $("#botonCancelarEliminarCursoRealizado").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0003',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId, data){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantCursoRealizado = cantCursoRealizado - 1;
        if(cantCursoRealizado<0){
            cantCursoRealizado=0;
        }
        $("#"+rowId).remove();

        var sumaHoras = ($("#txtSumaDeHorasAcademicas").html()*1);

        console.log(data.estatus);
        console.log(data.estatus=='C');
        if(data.estatus=='C'){
            sumaHoras = sumaHoras - (data.horas*1);
        }

        if(sumaHoras<0){
            sumaHoras = 0;
        }
        $("#txtSumaDeHorasAcademicas").html(sumaHoras);
        if(cantCursoRealizado==0){
            $("#txtSumaDeHorasAcademicas").html(0);
            $("#bodyTableCursosRealizados").append("<tr><td colspan='7'><div class='alertDialogBox'><p>No se han registrado Cursos Realizados</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#CursoRealizadoTh_titulo_obtenido, #CursoRealizadoTh_observacion, #CursoRealizadoTh_institucion_educativa").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });
        
        $("#CursoRealizadoTh_horas").numeric();

        $("#CursoRealizadoTh_titulo_obtenido, #CursoRealizadoTh_observacion, #CursoRealizadoTh_horas").on('blur', function(){
            clearField(this);
        });

        $("#CursoRealizadoTh_estatus").on("change", function(){
            if(!in_array($(this).val(), ['C', 'T', 'N'])){ // Si no está en Culminado, En Espera del Título o En Espera de Notas Certificadas
                $("#CursoRealizadoTh_fecha_egreso").val("");
                $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
            }
            else{
                $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
            }
        });

        $.datepicker.setDefaults({
            closeText: 'Cerrar',
            prevText: 'Anterior',
            nextText: 'Siguiente',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd-mm-yy',
            showOn: 'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1700, 1, 1),
            maxDate: 'yesterday'
        });

        $("#CursoRealizadoTh_fecha_egreso").datepicker({
            onSelect: function(dateText, inst)
            {
                if(!in_array($("#CursoRealizadoTh_estatus").val(), ['C'])){
                    $("#CursoRealizadoTh_fecha_egreso").val("");
                    $("#CursoRealizadoTh_fecha_egreso").attr("disabled", "disabled");
                    $.gritter.add({
                        title: 'Dato no válido',
                        text: 'Para efectuar el registro de la Fecha de Egreso debe indicar en el Campo Estatus el valor "Culminado" con respecto al Curso Realizado.',
                        class_name: 'gritter-warning'
                    });
                }
                else{
                    $("#CursoRealizadoTh_fecha_egreso").removeAttr("disabled");
                }
            }
        });

    }

};
