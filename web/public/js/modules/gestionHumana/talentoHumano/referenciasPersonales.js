$(document).ready(function(){

    modelReferenciaPersonal.filterValues();
    
    $("#btnNuevaReferenciaPersonal").on("click", function(){
        if(cantReferenciaPersonal>=cantMaxReferenciaPersonal){
            $.gritter.add({
                title: 'Referencias Personales Excedidas',
                text: 'Sólo puede registrar hasta un máximo de '+cantMaxReferenciaPersonal+' Referencias Personales.',
                class_name: 'gritter-error'
            });
            return false;
        }
        $("#referencia-personal-form").attr("data-form-type", "create");
        modelReferenciaPersonal.showFormRegister($("#referencia-personal-form"), 'create');
    });
    
    $("#referencia-personal-form").on('submit', function(evt){
       evt.preventDefault();
       modelReferenciaPersonal.submitForm($(this));
    });
    
    modelReferenciaPersonal.addEventsToLinks();

});

var modelReferenciaPersonal = {
    
    showFormRegister: function(form, type, data){
        
        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-referencia-personal', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        var dialog = $("#divReferenciaPersonalDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-group'></i> Registrar Nueva Referencia Personal</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroReferenciaPersonal",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroReferenciaPersonal",
                "click": function() {
                    $("#btnSubmitReferenciaPersonal").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-referencia-personal";
        var divResultSuccess = "#div-result-success-registro-referencia-personal";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;
        
        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroReferenciaPersonal").click();
                modelReferenciaPersonal.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroReferenciaPersonal").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroReferenciaPersonal").removeAttr("disabled");
        };

        if(containsHtml($("#ReferenciaPersonalTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }
        
        if(containsHtml($("#ReferenciaPersonalTh_nombre_referencia").val())){
            mensaje = "El Campo \"Nombre y Apellido\" posee datos no válidos.<br/>"+mensaje;
        }
        
        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroReferenciaPersonal").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');

        var data = form.serializeObject(true, "ReferenciaPersonalTh");
        data.id = id;
        data.funcionarioMppe = $("#ReferenciaPersonalTh_funcionario_mppe option:selected").text();
        if(cantReferenciaPersonal==0){
            $("#bodyTableReferenciasPersonales").html("");
        }
        
        var idRow = '';
        if (type=="update"){
            idRow=$("#ReferenciaPersonalTh_row_id").val();//data-row-ft-x
        }
        else{
            idRow=idRowReferenciaPersonal+lastIndexReferenciaPersonal;
        }
        
        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.cedula_referencia)+"\">"+htmlentities(data.cedula_referencia.toUpperCase())+"</td>\
                        <td title=\""+htmlentities(data.nombre_referencia)+"\">"+htmlentities(data.nombre_referencia.substring(0,80))+"</td>\
                        <td title=\""+htmlentities(data.telefono_referencia)+"\">"+htmlentities(data.telefono_referencia)+"</td>\
                        <td title=\""+htmlentities(data.email_referencia)+"\">"+htmlentities(data.email_referencia)+"</td>\
                        <td title=\""+htmlentities(data.funcionarioMppe)+"\">"+htmlentities(data.funcionarioMppe)+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtReferenciaPersonal\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelReferenciaPersonal\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            lastIndexReferenciaPersonal = lastIndexReferenciaPersonal + 1;
            cantReferenciaPersonal = cantReferenciaPersonal + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#ReferenciaPersonalTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableReferenciasPersonales").append(fila).ready(function(){
            modelReferenciaPersonal.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtReferenciaPersonal").unbind("click");
        $(".linkDelReferenciaPersonal").unbind("click");
        
        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtReferenciaPersonal").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#ReferenciaPersonalTh_row_id").val(rowId);

                $("#ReferenciaPersonalTh_id").val(base64_encode(data.id));
                $("#ReferenciaPersonalTh_nombre_referencia").val(data.nombre_referencia);
                $("#ReferenciaPersonalTh_cedula_referencia").val(data.cedula_referencia.toUpperCase());
                $("#ReferenciaPersonalTh_email_referencia").val(data.email_referencia);
                $("#ReferenciaPersonalTh_telefono_referencia").val(data.telefono_referencia);
                $("#ReferenciaPersonalTh_observacion").val(data.observacion);

                $("#referencia-personal-form").attr("data-form-type", "update");
                modelReferenciaPersonal.showFormRegister($("#referencia-personal-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0007',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelReferenciaPersonal").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#ReferenciaPersonalTh_row_id").val(rowId);
                $("#ReferenciaPersonalTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarReferenciaPersonalDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Referencia Personal</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarReferenciaPersonal",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarReferenciaPersonal",
                        "click": function() {
                            modelReferenciaPersonal.deleteData(data.id, rowId);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0008',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });
        
    },

    deleteData: function(id, rowId){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#referencia-personal-form");
            var divResultError = "#div-result-error-registro-referencia-personal";
            var divResultSuccess = "#div-result-success-registro-referencia-personal";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelReferenciaPersonal.deleteRowFromTable(rowId);
                    $("#botonCancelarEliminarReferenciaPersonal").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0009',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantReferenciaPersonal = cantReferenciaPersonal - 1;
        if(cantReferenciaPersonal<0){
            cantReferenciaPersonal=0;
        }
        $("#"+rowId).remove();
        if(cantReferenciaPersonal==0){
            $("#bodyTableReferenciasPersonales").append("<tr><td colspan='6'><div class='alertDialogBox'><p>No se han registrado Referencias Personales</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#ReferenciaPersonalTh_nombre_referencia").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });
        
        $("#ReferenciaPersonalTh_email_referencia").on('keyup blur', function(){
            keyEmail(this, true, true);
            makeUpper(this);
        });
        
        $("#ReferenciaPersonalTh_observacion").on('keyup blur', function(){
            keyText(this, true);
            makeUpper(this);
        });

        $("#ReferenciaPersonalTh_nombre_referencia, #ReferenciaPersonalTh_email_referencia, #ReferenciaPersonalTh_observacion").on('blur', function(){
            clearField(this);
        });
        
        $('#ReferenciaPersonalTh_telefono_referencia').mask('(0999)999-9999');
        
        $.mask.definitions['X'] = '[V|E|P|v|e|p]';
        $('#ReferenciaPersonalTh_cedula_referencia').mask('X-999?9999999999');

    }

};
