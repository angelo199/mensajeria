$(document).ready(function(){

    modelExperienciaLaboral.filterValues();

    $("#btnNuevaExperienciaLaboral").on("click", function(){
        
        if(cantExperienciaLaboral>=cantMaxExperienciaLaboral){
            $.gritter.add({
                title: 'Experiencias Laborales Excedidas',
                text: 'Sólo puede registrar hasta un máximo de '+ cantMaxExperienciaLaboral+' Experiencias Laborales.',
                class_name: 'gritter-error'
            });
            return false;
        }
        
        var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();
        if(activoActualmente=='S'){ // Si indica que está activo no debería indicar fecha de egreso
            $("#ExperienciaLaboralTh_fecha_egreso").val("");
            $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
        }
        else{
            $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
        }
        $("#experiencia-laboral-form").attr("data-form-type", "create");
        modelExperienciaLaboral.showFormRegister($("#experiencia-laboral-form"), 'create');
    });

    $("#experiencia-laboral-form").on('submit', function(evt){
       evt.preventDefault();
       modelExperienciaLaboral.submitForm($(this));
    });

    modelExperienciaLaboral.addEventsToLinks();

});

var modelExperienciaLaboral = {

    showFormRegister: function(form, type, data){

        if(type=='create'){
            form.reset();
        }

        displayDialogBox('#div-result-error-registro-experiencia-laboral', 'info', 'Todos los campos con <span class="required">*</span> son requeridos.');

        $( "#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", "+0m +0w");
        
        var dialog = $("#divExperienciaLaboralDialogBox").removeClass('hide').dialog({
            modal: true,
            width: '900px',
            draggale: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-building-o'></i> Registrar Nueva Experiencia Laboral</h4></div>",
            title_html: true,
            buttons: [{
                    html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                    "class": "btn btn-xs btn-danger",
                    "id": "botonCancelarRegistroExperienciaLaboral",
                    "click": function() {
                        $(this).dialog("close");
                    }
                },
                {
                html: "Guardar &nbsp;<i class='fa fa-save bigger-110'></i>",
                "class": "btn btn-info btn-xs",
                "id": "botonRegistroExperienciaLaboral",
                "click": function() {
                    $("#btnSubmitExperienciaLaboral").click();
                }
            }]
        });

    },


    submitForm: function(form){
        var mensaje = "";
        var divResultError = "#div-result-error-registro-experiencia-laboral";
        var divResultSuccess = "#div-result-success-registro-experiencia-laboral";
        var divResult = divResultError;
        var urlDir = form.attr('action');
        var type = form.attr('data-form-type');
        if(type=='create'){
            urlDir = urlDir.replace('{operacion}', 'registro');
            var method = "POST";
        }
        else if(type=='update'){
            urlDir = urlDir.replace('{operacion}', 'edicion');
            var method = "PUT";
        }
        var datos = form.serialize();
        var loadingEfect = true;
        var showResult = false;

        var responseFormat = "json";
        var successCallback = function(response){
            if(response.resultado=='EXITO'){
                displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                $("#botonCancelarRegistroExperienciaLaboral").click();
                modelExperienciaLaboral.addRowToTable(form, response.id);
            }
            else{
                displayHtmlInDivId(divResultError, response.mensaje);
            }
            $("#botonRegistroExperienciaLaboral").removeAttr("disabled");
        };
        var errorCallback = function(xhr, ajaxOptions, thrownError){
            $("#botonRegistroExperienciaLaboral").removeAttr("disabled");
        };


        var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();
        if(activoActualmente=='S'){ // Si indica que está activo no debería indicar fecha de egreso
            $("#ExperienciaLaboralTh_fecha_egreso").val("");
            $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
        }
        else{
            $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
            $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
            if($("#ExperienciaLaboralTh_fecha_egreso").val()==''){
                mensaje = "Debe indicar la Fecha de Egreso si ya no se encuentra activo en esta empresa o institución<br/>"+mensaje;
            }
        }

        if(containsHtml($("#ExperienciaLaboralTh_observcacion").val())){
            mensaje = "El Campo \"Observcación\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#ExperienciaLaboralTh_actividades_realizadas").val())){
            mensaje = "El Campo \"Actividades Desempeñadas\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#ExperienciaLaboralTh_institucion").val())){
            mensaje = "El Campo \"Empresa/Institución\" posee datos no válidos.<br/>"+mensaje;
        }

        if(containsHtml($("#ExperienciaLaboralTh_cargo_desempenhado").val())){
            mensaje = "El Campo \"Cargo Desempeñado\" posee datos no válidos.<br/>"+mensaje;
        }

        if(mensaje.length>0){
            displayDialogBox(divResultError, 'error', mensaje);
        }
        else{
            $("#botonRegistroExperienciaLaboral").attr("disabled", "disabled");
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);
        }

    },


    addRowToTable:function(form, id){

        var type = form.attr('data-form-type');
        var data = form.serializeObject(true, "ExperienciaLaboralTh");
        
        var booleano = {'S': 'Sí', 'N': 'No'};

        data.id = id;
        data.estaActivoActualmente = booleano[$("#ExperienciaLaboralTh_activo_actualmente").val()];
        data.esPublica = booleano[$("#ExperienciaLaboralTh_publica").val()];

        if(cantExperienciaLaboral==0){
            $("#bodyTableExperienciasLaborales").html("");
        }
        
        var idRow = '';
        if (type=="update"){
            idRow=$("#ExperienciaLaboralTh_row_id").val();//data-row-ft-x
        }
        else{
            idRow=idRowExperienciaLaboral+lastIndexExperienciaLaboral;
        }

        var fila = "<tr id=\""+idRow+"\">\
                        <td title=\""+htmlentities(data.institucion)+"\">"+htmlentities(data.institucion.substring(0, 27))+"</td>\
                        <td title=\""+htmlentities(data.esPublica)+"\">"+htmlentities(data.esPublica)+"</td>\
                        <td title=\""+htmlentities(data.cargo_desempenhado)+"\">"+htmlentities(data.cargo_desempenhado.substring(0, 35))+"</td>\
                        <td title=\""+htmlentities(data.estaActivoActualmente)+"\">"+htmlentities(data.estaActivoActualmente)+"</td>\
                        <td title=\""+htmlentities(data.fecha_ingreso)+"\">"+htmlentities(data.fecha_ingreso)+"</td>\
                        <td title=\""+htmlentities(data.fecha_egreso)+"\">"+htmlentities(data.fecha_egreso)+"</td>\
                        <td nowrap=\"nowrap\">\
                            <div class=\"action-buttons\">\
                                <a class=\"fa icon-pencil green linkEdtExperienciaLaboral\" title=\"Editar datos\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                                <a class=\"fa fa-trash red linkDelExperienciaLaboral\" title=\"Eliminar\" data-row-id=\""+idRow+"\" data-json='"+(JSON.stringify(data))+"'></a>&nbsp;&nbsp;\
                            </div>\
                        </td>\
                    </tr>";

        if(type=='create'){
            lastIndexExperienciaLaboral = lastIndexExperienciaLaboral + 1;
            cantExperienciaLaboral = cantExperienciaLaboral + 1;
        }
        else if(type=='update'){
            var rowIdToRemove = $("#ExperienciaLaboralTh_row_id").val();
            $("#"+rowIdToRemove).remove();
        }

        $("#bodyTableExperienciasLaborales").append(fila).ready(function(){
            modelExperienciaLaboral.addEventsToLinks();
        });
    },


    addEventsToLinks: function(){

        $(".linkEdtExperienciaLaboral").unbind("click");
        $(".linkDelExperienciaLaboral").unbind("click");

        /**
         * Link que permite Editar los Datos de un registro seleccionado
         */
        $(".linkEdtExperienciaLaboral").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                // Seteo este valor en el formulario para saber el id de la fila(row) de la tabla HTML que eliminaré luego de Editar los datos.
                $("#ExperienciaLaboralTh_row_id").val(rowId);

                $("#ExperienciaLaboralTh_id").val(base64_encode(data.id));
                $("#ExperienciaLaboralTh_institucion").val(data.institucion);
                $("#ExperienciaLaboralTh_publica").val(data.publica);
                $("#ExperienciaLaboralTh_institucion_educativa_id").val(data.institucion_educativa_id);
                $("#ExperienciaLaboralTh_cargo_desempenhado").val(data.cargo_desempenhado);
                $("#ExperienciaLaboralTh_nombre_referencia").val(data.nombre_referencia);
                $("#ExperienciaLaboralTh_telefono_referencia").val(data.telefono_referencia);
                $("#ExperienciaLaboralTh_activo_actualmente").val(data.activo_actualmente);
                $("#ExperienciaLaboralTh_fecha_ingreso").val(data.fecha_ingreso);
                $("#ExperienciaLaboralTh_fecha_egreso").val(data.fecha_egreso);
                $("#ExperienciaLaboralTh_actividades_realizadas").val(data.actividades_realizadas);
                $("#ExperienciaLaboralTh_observacion").val(data.observacion);

                var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();

                if(activoActualmente=='S'){
                    $("#ExperienciaLaboralTh_fecha_egreso").val("");
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
                }
                else{
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
                }

                $("#experiencia-laboral-form").attr("data-form-type", "update");
                modelExperienciaLaboral.showFormRegister($("#experiencia-laboral-form"), "update");
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0004',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

        /**
         * Link que permite Eliminar un registro seleccionado
         */
        $(".linkDelExperienciaLaboral").on("click", function(evt){
            evt.preventDefault();
            var strData = $(this).attr("data-json").trim();
            try{
                var data = eval("(" + strData + ")");
                var rowId =  $(this).attr("data-row-id");

                $("#ExperienciaLaboralTh_row_id").val(rowId);
                $("#ExperienciaLaboralTh_id").val(base64_encode(data.id));

                var dialog = $("#divEliminarExperienciaLaboralDialogBox").removeClass('hide').dialog({
                    modal: true,
                    width: '500px',
                    draggale: false,
                    resizable: false,
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation'></i> Eliminar Experiencia Laboral</h4></div>",
                    title_html: true,
                    buttons: [{
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cancelar",
                            "class": "btn btn-xs btn-danger",
                            "id": "botonCancelarEliminarExperienciaLaboral",
                            "click": function() {
                                $(this).dialog("close");
                            }
                        },
                        {
                        html: "Aceptar &nbsp;<i class='fa fa-trash bigger-110'></i>",
                        "class": "btn btn-warning btn-xs",
                        "id": "botonEliminarExperienciaLaboral",
                        "click": function() {
                            modelExperienciaLaboral.deleteData(data.id, rowId);
                        }
                    }]
                });
            }
            catch (e){
                $.gritter.add({
                    title: 'Error CL0005',
                    text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                    class_name: 'gritter-error'
                });
            }
        });

    },

    deleteData: function(id, rowId){
        try{
            // console.log("Eliminando el Registro con el Id: "+id);
            var form = $("#experiencia-laboral-form");
            var divResultError = "#div-result-error-registro-experiencia-laboral";
            var divResultSuccess = "#div-result-success-registro-experiencia-laboral";
            var divResult = divResultSuccess;
            var urlDir = form.attr('action');
            var type = form.attr('data-form-type');
            var urlDir = urlDir.replace('{operacion}', 'eliminacion');
            var datos = form.serialize();
            var loadingEfect = true;
            var showResult = false;
            var method = "DELETE";
            var responseFormat = "json";
            var successCallback = function(response){
                if(response.resultado=='EXITO'){
                    displayDialogBox(divResultSuccess, 'exito', response.mensaje);
                    modelExperienciaLaboral.deleteRowFromTable(rowId);
                    $("#botonCancelarEliminarExperienciaLaboral").click();
                }
                else{
                    displayHtmlInDivId(divResultError, response.mensaje);
                }
            };
            var errorCallback = function(xhr, ajaxOptions, thrownError){

            };
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback, errorCallback);

        }
        catch (e){
            $.gritter.add({
                title: 'Error CL0006',
                text: 'Se ha Producido un error. Recargue la página e intentelo de nuevo.',
                class_name: 'gritter-error'
            });
        }
    },

    deleteRowFromTable: function(rowId){
        // console.log("Eliminando la Fila de la Tabla con el Id: "+rowId);
        cantExperienciaLaboral = cantExperienciaLaboral - 1;
        if(cantExperienciaLaboral<0){
            cantExperienciaLaboral=0;
        }
        $("#"+rowId).remove();
        if(cantExperienciaLaboral==0){
            $("#bodyTableExperienciasLaborales").append("<tr><td colspan='7'><div class='alertDialogBox'><p>No se han registrado Experiencias Laborales</p></div></td><tr>");
        }
    },

    filterValues: function(){

        $("#ExperienciaLaboralTh_institucion, #ExperienciaLaboralTh_cargo_desempenhado, #ExperienciaLaboralTh_nombre_referencia").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });

        $("#ExperienciaLaboralTh_observacion, #ExperienciaLaboralTh_actividades_realizadas").on('keyup blur', function(){
            keyText(this, true);
            makeUpper(this);
        });

        $("#ExperienciaLaboralTh_institucion, #ExperienciaLaboralTh_cargo_desempenhado, #ExperienciaLaboralTh_nombre_referencia, #ExperienciaLaboralTh_observacion, #ExperienciaLaboralTh_actividades_realizadas").on('blur', function(){
            clearField(this);
        });

        $("#ExperienciaLaboralTh_activo_actualmente").on("change", function(){
            if($(this).val()=='S'){ // Si indica que está activo no debería indicar fecha de egreso
                $("#ExperienciaLaboralTh_fecha_egreso").val("");
                $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
                $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
            }
            else{
                $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
                $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
            }
        });

        $.datepicker.setDefaults({
            closeText: 'Cerrar',
            prevText: 'Anterior',
            nextText: 'Siguiente',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd-mm-yy',
            showOn: 'focus',
            showOtherMonths: false,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            minDate: new Date(1700, 1, 1),
            maxDate: 'yesterday'
        });

        $("#ExperienciaLaboralTh_fecha_ingreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#ExperienciaLaboralTh_fecha_egreso" ).datepicker( "option", "minDate", selectedDate+"" );
            }
        });

        $("#ExperienciaLaboralTh_fecha_egreso").datepicker({
            onClose: function( selectedDate ) {
                $( "#ExperienciaLaboralTh_fecha_ingreso" ).datepicker( "option", "maxDate", selectedDate );
            },
            onSelect: function(dateText, inst)
            {
                var activoActualmente = $("#ExperienciaLaboralTh_activo_actualmente").val();

                if(activoActualmente=='S'){
                    $("#ExperienciaLaboralTh_fecha_egreso").val("");
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("disabled", "disabled");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("required");
                    $.gritter.add({
                        title: 'Dato no válido',
                        text: 'Para efectuar el registro de la Fecha de Egreso no debe indicar que se encuentra activo en esta empresa o institución.',
                        class_name: 'gritter-warning'
                    });
                }
                else{
                    $("#ExperienciaLaboralTh_fecha_egreso").attr("required","required");
                    $("#ExperienciaLaboralTh_fecha_egreso").removeAttr("disabled");
                }

            }
        });

        $('#ExperienciaLaboralTh_telefono_referencia').mask('(0999)999-9999');

    }

};
