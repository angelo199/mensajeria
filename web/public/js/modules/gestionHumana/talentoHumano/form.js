$(document).ready(function(){
    
    formInputFilters();
    //busquedaSaime();
    
    // Si la persona es Aspirante no debe de tener datos laborales ni datos bancarios.
    var condicionActualId = $("#condicion_actual_id").val();
    if(condicionActualId=='0'){
        $("#div-datos-laborales").attr("class", "hide");
        $("#div-datos-banco").attr("class", "hide");
    }

    $("#AdminTalentoHumano_estado_id").on('change', function(){
        var municipios = getDataCatastro('Municipio', 'estado_id', $(this).val(), false, 'nombre');
        $("#AdminTalentoHumano_municipio_id").html(generateOptionsToSelect(municipios, 'nombre', 'id', null));
    });
    
    $("#AdminTalentoHumano_municipio_id").on('change', function(){
        var parroquias = getDataCatastro('Parroquia', 'municipio_id', $(this).val(), false, 'nombre');
        $("#AdminTalentoHumano_parroquia_id").html(generateOptionsToSelect(parroquias, 'nombre', 'id', null));
    });
    
    var formType = $("#talento-humano-form").attr('data-form-type');
    
    if(formType==='create'){
        $("#AdminTalentoHumano_cedula").on('blur', function(){
            busquedaSaime();
        });
    }
    else{
        $("#AdminTalentoHumano_origen").attr('disabled', 'disabled');
        $("#AdminTalentoHumano_cedula").attr('readOnly','readOnly');
        $("#AdminTalentoHumano_nombre").attr('disabled','disabled');
        $("#AdminTalentoHumano_apellido").attr('disabled','disabled');
        $("#AdminTalentoHumano_fecha_nacimiento").attr('disabled','disabled');
        $("#AdminTalentoHumano_sexo").attr('disabled', 'disabled');
    }

    if($("#talento-humano-form").attr('data-form-type')=='edicion'){
        $("#talento-humano-form").on('submit',function(evt){
            evt.preventDefault();
            registroDatosTalentoHumano($(this));
        });
    }

});

function formInputFilters(){
    
    // Filters
    $("#AdminTalentoHumano_cedula").on('keyup blur', function(){
        keyNum(this, false, false);
    });
    
    $("#AdminTalentoHumano_rif, #AdminTalentoHumano_urb_sector, #AdminTalentoHumano_av_calle, #AdminTalentoHumano_casa_edificio, #AdminTalentoHumano_apartamento").on('keyup blur', function () {
        keyAlphaNum(this, true, true);
    });

    $("#AdminTalentoHumano_aptitudes").on('keyup blur', function () {
        keyText(this, true);
    });


    $("#AdminTalentoHumano_piso").on('keyup blur', function () {
        keyNum(this, false, false);
    });

    $("#AdminTalentoHumano_direccion, #AdminTalentoHumano_ruta_trabajo, #AdminTalentoHumano_ruta_alterna").on('keyup blur', function () {
        keyText(this, true);
    });

    $("#AdminTalentoHumano_email_personal, #AdminTalentoHumano_email_corporativo").on('keyup blur', function () {
        keyEmail(this, false);
    });


    $("#AdminTalentoHumano_twitter").on('keyup blur', function () {
        keyTwitter(this, false);
    });

   

    // Clear Fields

    $("#AdminTalentoHumano_email_corporativo, #AdminTalentoHumano_twitter, #AdminTalentoHumano_email_personal, #AdminTalentoHumano_ruta_alterna, #AdminTalentoHumano_ruta_trabajo, #AdminTalentoHumano_direccion, #AdminTalentoHumano_piso, #AdminTalentoHumano_apartamento, #AdminTalentoHumano_casa_edificio, #AdminTalentoHumano_av_calle, #AdminTalentoHumano_urba_sector, #AdminTalentoHumano_rif").on('blur', function () {
        clearField(this);
    });

   

    // Masks

    $.mask.definitions['L'] = '[1-2]';
    $.mask.definitions['X'] = '[2|4|6]';
    $.mask.definitions['R'] = '[V|E|J|G|v|e|j|g]';

    $('#AdminTalentoHumano_telefono_fijo').mask('(0299)999-9999');
    $('#AdminTalentoHumano_telefono_celular').mask('(04LX)999-9999');
    $('#AdminTalentoHumano_telefono_oficina').mask('(0299)999-9999');
    $('#AdminTalentoHumano_numero_cuenta').mask('99999999999999999999');
    $('#AdminTalentoHumano_rif').mask('R-999?999999999999999');
    
    if($("#talento-humano-form").attr('data-form-type')!='update'){
        
        var fechaNacimiento = $('#AdminTalentoHumano_fecha_nacimiento').val();

        if(in_array(fechaNacimiento, ['0001-01-01', '1800-01-01'])){

            $("#read_fecha_nacimiento_latino").removeAttr('disabled').attr('readOnly', 'readOnly');

            $.datepicker.setDefaults({
                closeText: 'Cerrar',
                prevText: 'Anterior',
                nextText: 'Siguiente',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd-mm-yy',
                showOn: 'focus',
                showOtherMonths: false,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                minDate: new Date(1700, 1, 1),
                maxDate: 'yesterday',
                onSelect: function(dateText, inst)
                {
                    $('#AdminTalentoHumano_fecha_nacimiento').val($.datepicker.formatDate("yy-mm-dd", $('#read_fecha_nacimiento_latino').datepicker('getDate')));
                }
            });

            $('#read_fecha_nacimiento_latino').datepicker();

        }

    }

    // $.datepicker.setDefaults();
}

function busquedaSaime(){
            
    var origen = $("#AdminTalentoHumano_origen").val();
    var cedula = $("#AdminTalentoHumano_cedula").val();
    
    if(origen.length>0 && cedula.length>0){
    
        if(isValidOrigen(origen)){
            
            var divResult = "";
            var urlDir = "/ayuda/saime/consultaSaime/origen/"+origen+"/cedula/"+cedula;
            var datos = "";
            var loadingEfect = false;
            var showResult = false;
            var method = "GET";
            var responseFormat = "json";
            var beforeSendCallback = null;
            var successCallback = function(response, estatusCode, dom){
                
                console.log(response.statusCode);
                if(response.statusCode==='success'){
                    var origen = response.origen;
                    var cedula = response.cedula;
                    var nombre = response.nombre;
                    var apellido = response.apellido;
                    var fecha_nacimiento = response.fecha_nacimiento;
                    var fecha_nacimiento_latino = response.fecha_nacimiento_latino;
                    var sexo = response.sexo;
                    
                    var cedulaInicial = $("#AdminTalentoHumano_cedula").attr('data-inicial');
                    
                    $("#read_existe_cedula").val('Si');

                    var nombreInicial = $("#AdminTalentoHumano_nombre").val();
                    if(nombreInicial.length==0 || !isValidNombre() || cedulaInicial!=cedula){
                        $("#AdminTalentoHumano_nombre").val(nombre);
                    }

                    var apellidoInicial = $("#AdminTalentoHumano_apellido").val();
                    if(apellidoInicial.length==0 || !isValidApellido() || cedulaInicial!=cedula){
                        $("#AdminTalentoHumano_apellido").val(apellido);
                    }

                    $("#AdminTalentoHumano_cedula").attr('data-inicial',cedula);
                    $("#AdminTalentoHumano_nombre").attr('data-inicial',nombre);
                    $("#AdminTalentoHumano_apellido").attr('data-inicial',apellido);

                    $("#AdminTalentoHumano_fecha_nacimiento").val(fecha_nacimiento);
                    $("#read_fecha_nacimiento_latino").val(fecha_nacimiento_latino);
                    $("#AdminTalentoHumano_sexo").val(sexo);

                    $("#AdminTalentoHumano_origen_titular").val(origen);
                    $("#AdminTalentoHumano_cedula_titular").val(cedula);
                    var nombreCompleto = nombre + " " + apellido;
                    nombreCompleto = nombreCompleto.toUpperCase();
                    nombreCompleto = nombreCompleto.substr(0, 40);
                    $("#AdminTalentoHumano_nombre_titular").val();
                    
                    $("#AdminTalentoHumano_verificado_saime").val('Si');
                }
                else{
                    var cedula = $("#AdminTalentoHumano_cedula").attr('data-inicial');
                    if($("#talento-humano-form").attr('data-form-type')!='update'){
                        $("#AdminTalentoHumano_cedula").val(cedula);
                    }
                    $("#read_existe_cedula").val('No');
                    $("#AdminTalentoHumano_verificado_saime").val('No');
                    simpleDialogPopUp("#resultadoDialogo", "fa-credit-card", 'Búsqueda de Documento de Identidad', getDialogBox('error', response.mensaje));
                }

            };
            
            executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);

        }else{
            displayDialogBox('#resultado', 'error', 'El valor del Origen no es válido. Le recomendamos recargar la página e intentarlo de nuevo.');
        }

    }
    
}


function validateForm(){
    var cedulaRead = $("#read_existe_cedula").val();
    var existeCedula = cedulaRead==='Si';
    if(!existeCedula){
        displayDialogBox('resultado', 'error', 'El número de documento de identidad no ha podido ser encontrado en nuestra base de datos.');
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    }
    else{
        
        if($("#AdminTalentoHumano_telefono_fijo").val()=="" && $("#AdminTalentoHumano_telefono_celular").val()==""){
            $("#AdminTalentoHumano_telefono_fijo, #AdminTalentoHumano_telefono_celular").addClass('error');
            displayDialogBox('resultado', 'error', 'Debe indicar al menos un número telefónico de contacto.');
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
        else{
            $("#AdminTalentoHumano_telefono_fijo, #AdminTalentoHumano_telefono_celular").removeClass('error');
        }
        
        if(!isValidNombre()){
            $("#AdminTalentoHumano_nombre").addClass('error');
            displayDialogBox('resultado', 'error', 'Solo debe hacer correcciones del nombre, no modificarlo por completo.');
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
        else{
            $("#AdminTalentoHumano_nombre").removeClass('error');
        }

        if(!isValidApellido()){
            $("#AdminTalentoHumano_apellido").addClass('error');
            displayDialogBox('resultado', 'error', 'Solo debe hacer correcciones del apellido, no modificarlo por completo.');
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
        else{
            $("#AdminTalentoHumano_apellido").removeClass('error');
        }
        var edad = getAge($('#AdminTalentoHumano_fecha_nacimiento').val());
        if(edad<18 || edad>90){
            $("#AdminTalentoHumano_fecha_nacimiento").addClass('error');
            if(edad<18){
                displayDialogBox('resultado', 'error', 'Esta persona parece no ser mayor de edad.');
            }
            else{
                displayDialogBox('resultado', 'error', 'Esta persona es mayor de 90 años.');
            }
            $("html, body").animate({ scrollTop: 0 }, "fast");
            return false;
        }
    }
    return true;
}

function registroDatosTalentoHumano(form){
    var datos = form.serialize();
    var urlDir = form.attr('action');
    
    if(datos.length>0 && urlDir.length){
        var divResult = "#resultado";
        var loadingEfect = true;
        var showResult = true;
        var method = "PUT";
        var responseFormat = "html";
        var successCallback = function(response, dom, statusCode){
            scrollUp();
        };
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
    }
}

function registroDatosBancarios(form){
    
    var datos = form.serialize();
    var urlDir = form.attr('action');
    
    if(datos.length>0 && urlDir.length){
        var divResult = "#divResultDatosBancarios";
        var loadingEfect = true;
        var showResult = true;
        var method = "POST";
        var responseFormat = "html";
        var successCallback = function(response, dom, statusCode){
            scrollUp();
        };
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
    }
}

function isValidOrigen(origen){
    var origenes = ['V', 'E', 'P'];
    if($.inArray(origen, origenes)>=0){ // La función inArray de jQuery te devuelve el índice del array en donde se encuentra el valor buscado
        return true;
    }
    return false;
}

function isValidBooleanEs(input){
    var booleans = ['Si', 'No'];
    if($.inArray(input, booleans)>=0){
        return true;
    }
    return false;
}

function isValidNombre(){
    $nombreInicial = $("#AdminTalentoHumano_nombre").attr('data-inicial');
    $nombre = $("#AdminTalentoHumano_nombre").val();
    $indiceNombre = levenshtein($nombreInicial, $nombre);
    console.log($indiceNombre);
    return ($indiceNombre<=5);
}

function isValidApellido(){
    $apellidoInicial = $("#AdminTalentoHumano_apellido").attr('data-inicial');
    $apellido = $("#AdminTalentoHumano_apellido").val();
    $indiceApellido = levenshtein($apellidoInicial, $apellido);
    console.log($indiceApellido);
    return ($indiceApellido<=5);
}
