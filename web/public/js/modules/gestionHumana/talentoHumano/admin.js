$(document).ready(function() {

    $('#date-picker').datepicker();
    $.datepicker.setDefaults({
        dateFormat: 'dd-mm-yy',
        showOn: 'focus',
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: new Date(1700, 1, 1),
        maxDate: 'today'
    });

    $('#AdminTalentoHumano_cedula').bind('keyup blur', function() {
        keyNum(this, false);
        clearField(this);
    });

    $('#AdminTalentoHumano_origen').bind('keyup blur', function() {
        keyText(this, true);
        clearField(this);
    });

    $('#AdminTalentoHumano_nombre').bind('keyup blur', function() {
        keyAlphaNum(this, true);
    });

    $('#AdminTalentoHumano_nombre').bind('blur', function() {
        clearField(this);
    });

    $('#AdminTalentoHumano_apellido').bind('keyup blur', function() {
        keyAlphaNum(this, true);
    });

    $('#AdminTalentoHumano_apellido').bind('blur', function() {
        clearField(this);
    });

});
