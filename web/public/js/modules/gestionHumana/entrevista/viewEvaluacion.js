/**
 * @author José Gabriel González
 */
$(document).ready(function(){
    
    $("#btnImprimirEntrevista").on('click', function() {
        imprimirArea();
    });

    /**
     * @type Number valorTotal Es la Variable Global que me permite almacenar el valor total de la ponderación o baremo, producto de la entrevista
     */
    var valorTotal = 0;

    /**
     * 
     * @type Number valorTotalPadreSelects Es la Variable Global que me permite almacenar el valor total de los factores acumulativos (selects)
     */
    var valorTotalPadreSelects = 0;

    /**
     * Para entender esto, hay que ver el método "getFactorEvaluacionInput" del controlador "gestionHumana/entrevista".
     * Al recorrer allí todos los factores de evaluación se generan a partir de estos un array que contiene todos códigos de
     * los factores padres de items seleccionables, con estos a su vez genero (unas líneas más arriba 50-52) unos inputsArray
     * que contienen esos código a esos input se les coloca como clase o atributo class el valor "factoresCuantificables".
     * 
     * En esta área recorro todos los llamados factores cuantificables que no son más que los códigos de
     * los factores padres de items seleccionables, por tanto todo item seleccionable posee un input (checkbox o select) donde
     * su atributo name es el código de su factor padre.
     * 
     * Al recorrerlos puedo asignar eventos de cambio en cada item seleccionable (o factor de evaluacion seleccionable) y así
     * poder obtener los valores de dichos items para cuantificar el valor total de la evaluación así como los ids de los
     * elementos seleccionados.
     */
    $('input.factoresCuantificables').each(function() {

        var factor = $(this).val();
        var acumulativo = ($(this).attr("data-acumulativo")=='true');

        //console.log($(this).val());

        /**
         * Elementos no acumulativos: Regularmente son checkbox de los que sólo puedo seleccionar uno (1) sólo
         * es decir, se comportan como radiobutton, en esta sección manejo los eventos de ellos para obtener los valores seleccionados
         * y calcular el valor total
         */
        if(!acumulativo){
            $('input[type="checkbox"].'+factor).each(function(){
                //console.log($(this).val());                    
                //console.log($(this).attr("data-padre-codigo"));
                if($(this).is(":checked")){
                    $("span#txt-"+$(this).attr("data-padre-codigo")).html($(this).attr("data-valor-maximo"));
                    $("input#input-"+$(this).attr("data-padre-codigo")).val($(this).attr("data-valor-maximo"));
                }
                else{
                    if(!$('input[name="' + this.name + '"]').is(":checked")){
                        $("span#txt-"+$(this).attr("data-padre-codigo")).html("0");
                        $("input#input-"+$(this).attr("data-padre-codigo")).val("0");
                    }
                }
            });
        } else {

            /**
             * Elementos acumulativos: Regularmente son campos selects de los que el usuario debe seleccionar
             * un valor en el rango establecido.
             */
            $('.select#input-'+factor).each(function(){

                valorTotal = 0;
                $('input.factoresCuantificables').each(function() {
                    var factor = $(this).val();
                    valorTotal += $("#input-"+factor).val()*1;
                });

                valorTotalPadreSelects = 0;
                var codigoPadreSelects = $(this).attr("data-padre-codigo");
                $('.selects-'+codigoPadreSelects).each(function() {
                    valorTotalPadreSelects += $(this).val()*1;
                });

                $("input#input-"+codigoPadreSelects).val(valorTotalPadreSelects);
                $("span#txt-"+codigoPadreSelects).html(valorTotalPadreSelects);
            });

        }
    });
});

function imprimirArea() {
    var tituloPrintArea = $("#aTituloEntrevista").html();
        $(".widget-box").removeClass("collapsed");
        $(".widget-box").printArea(
            {
               popTitle:tituloPrintArea
            }
        );
}
