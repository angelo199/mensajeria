$(document).ready(function () {         
   $("#btnGenerarPdfEntrevista").on('click', function(evt){
        evt.preventDefault();
        dialogGenerarPdfEntrevista();
        
        var divResult = "#divResultDescargaGenerarPdfEntrevista";
        var urlDir = $(this).attr('href');
        var datos = '';
        var loadingEfect = false;
        var showResult = false;
        var method = 'GET';
        var responseFormat = "json";
        var successCallback = function(response, estatusCode, dom){
            var result = response.codigo;
            var mensaje = response.mensaje;
            if(result=='EXITO'){
                displayDialogBox("#divResultDescargaGenerarPdfEntrevista", 'success', mensaje+" Haga click en el siguiente enlace para efectuar su descarga.");
                $("#linkDescargaGenerarPdfEntrevista").attr("href", response.url_archivo_pdf+"?v="+Math.floor((Math.random() * 1000) + 1)).removeClass("hide");
            }
            else{
                displayDialogBox("#divResultDescargaGenerarPdfEntrevista", 'error', mensaje);
            }
           
        };
        executeAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, successCallback);
        
    }); 
});
function dialogGenerarPdfEntrevista(){
    
    var mensaje = '<div class="infoDialogBox">\
            <p>\
                Se está efectuando la generación del PDF de la Entrevista. En pocos segundos se podrá visualizar un link de descarga del mismo.\
            </p>\
        </div>\
        <div align="center">\
            <img src="/public/images/ajax-loader-red.gif" />\
        </div>\
         <div class="padding-6 center">\
        <a class="hide" target="_blank" id="linkDescargaGenerarPdfEntrevista" href="#">\
            <img src="/public/images/file_pdf.png" />\
        </a>\
    </div>';
     $("#linkDescargaGenerarPdfEntrevista").attr("href", "#").addClass("hide");
    $("#divResultDescargaGenerarPdfEntrevista").html(mensaje);
   
    
    var dialog = $("#dialogDescargaGenerarPdfEntrevista").removeClass('hide').dialog({
        modal: true,
        width: '680px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-file-pdf-o'></i>&nbsp; Generar PDF de la Entrevista.</h4></div>",
        title_html: true,
        buttons: [
            {
                "html":  "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                "class": "btn btn-xs btn-danger",
                "id":    "btnCancelarDialogGenerarPdfEntrevista",
                "click": function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}

