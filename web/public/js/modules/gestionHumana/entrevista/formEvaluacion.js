/**
 * @author José Gabriel González
 */
$(document).ready(function(){

        evaluacionObj.filterInputs();

        /**
         * Este script permite que un grupo de checkbox con el mismo nombre (con la clase "radio")
         * se comporten como radiobuttons, es decir, sólo puede ser seleccionado uno del grupo
         * que posea el mismo nombre
         */
        $('input[type="checkbox"].radio').on('change', function() {
            $('input[name="' + this.name + '"].radio').not(this).prop('checked', false);
        });

        /**
         * @type Number valorTotal Es la Variable Global que me permite almacenar el valor total de la ponderación o baremo, producto de la entrevista
         */
        var valorTotal = 0;
        
        /**
         * 
         * @type Number valorTotalPadreSelects Es la Variable Global que me permite almacenar el valor total de los factores acumulativos (selects)
         */
        var valorTotalPadreSelects = 0;

        /**
         * Para entender esto, hay que ver el método "getFactorEvaluacionInput" del controlador "gestionHumana/entrevista".
         * Al recorrer allí todos los factores de evaluación se generan a partir de estos un array que contiene todos códigos de
         * los factores padres de items seleccionables, con estos a su vez genero (unas líneas más arriba 50-52) unos inputsArray
         * que contienen esos código a esos input se les coloca como clase o atributo class el valor "factoresCuantificables".
         * 
         * En esta área recorro todos los llamados factores cuantificables que no son más que los códigos de
         * los factores padres de items seleccionables, por tanto todo item seleccionable posee un input (checkbox o select) donde
         * su atributo name es el código de su factor padre.
         * 
         * Al recorrerlos puedo asignar eventos de cambio en cada item seleccionable (o factor de evaluacion seleccionable) y así
         * poder obtener los valores de dichos items para cuantificar el valor total de la evaluación así como los ids de los
         * elementos seleccionados.
         */
        $('input.factoresCuantificables').each(function() {
            
            var factor = $(this).val();
            var acumulativo = ($(this).attr("data-acumulativo")=='true');
            
            /**
             * Elementos no acumulativos: Regularmente son checkbox de los que sólo puedo seleccionar uno (1) sólo
             * es decir, se comportan como radiobutton, en esta sección manejo los eventos de ellos para obtener los valores seleccionados
             * y calcular el valor total
             */
            if(!acumulativo){
                $('input[type="checkbox"].'+factor).on('change', function(){
                    //console.log($(this).attr("data-padre-codigo"));
                    if($(this).is(":checked")){
                        $("span#txt-"+$(this).attr("data-padre-codigo")).html($(this).attr("data-valor-maximo"));
                        $("input#input-"+$(this).attr("data-padre-codigo")).val($(this).attr("data-valor-maximo"));
                    }
                    else{
                        if(!$('input[name="' + this.name + '"]').is(":checked")){
                            $("span#txt-"+$(this).attr("data-padre-codigo")).html("0");
                            $("input#input-"+$(this).attr("data-padre-codigo")).val("0");
                        }
                    }
                    valorTotal = 0;
                    $('input.factoresCuantificables').each(function() {
                        var factor = $(this).val();
                        valorTotal += $("#input-"+factor).val()*1;
                    });
                    $("input#input-total").val(valorTotal);
                    $("span#txt-total").html(valorTotal);
                    // console.log(valorTotal);
                    evaluacionObj.isRequiredObservacion(valorTotal);

                });
            }
            else{

                /**
                 * Elementos acumulativos: Regularmente son campos selects de los que el usuario debe seleccionar
                 * un valor en el rango establecido.
                 */
                $('select#input-'+factor).on('change', function(){

                    valorTotal = 0;
                    $('input.factoresCuantificables').each(function() {
                        var factor = $(this).val();
                        valorTotal += $("#input-"+factor).val()*1;
                    });

                    valorTotalPadreSelects = 0;
                    var codigoPadreSelects = $(this).attr("data-padre-codigo");
                    $('select.selects-'+codigoPadreSelects).each(function() {
                        valorTotalPadreSelects += $(this).val()*1;
                    });

                    $("input#input-"+codigoPadreSelects).val(valorTotalPadreSelects);
                    $("span#txt-"+codigoPadreSelects).html(valorTotalPadreSelects);
                    
                    $("input#input-total").val(valorTotal);
                    $("span#txt-total").html(valorTotal);

                    evaluacionObj.isRequiredObservacion(valorTotal);

                });

            }
        });
        
        $('input.factoresCuantificables').each(function() {
            
            var factor = $(this).val();
            var acumulativo = ($(this).attr("data-acumulativo")=='true');
            $('input[type="checkbox"]').each(function() {
                
                if($(this).is(":checked")){
                    $("span#txt-"+$(this).attr("data-padre-codigo")).html($(this).attr("data-valor-maximo"));
                    $("input#input-"+$(this).attr("data-padre-codigo")).val($(this).attr("data-valor-maximo"));
                }
                else{
                    if(!$('input[name="' + this.name + '"]').is(":checked")){
                        $("span#txt-"+$(this).attr("data-padre-codigo")).html("0");
                        $("input#input-"+$(this).attr("data-padre-codigo")).val("0");
                    }
                }
                valorTotal = 0;
                $('input.factoresCuantificables').each(function() {
                    var factor = $(this).val();
                    valorTotal += $("#input-"+factor).val()*1;
                });
                $("input#input-total").val(valorTotal);
                $("span#txt-total").html(valorTotal);
                // console.log(valorTotal);
                evaluacionObj.isRequiredObservacion(valorTotal);
                
            });

        });
        
    });

var evaluacionObj = {
    filterInputs: function(){
        /**
         * Filtrando datos ingresados por el usuario en el campo de cargo del evaluador
         */
        $("#cargo-evaluador").on('keyup blur', function(){
            keyText(this, true);makeUpper(this);
        });

        $("#cargo-analista").on('keyup blur', function(){
            keyText(this, true);makeUpper(this);
        });

        /**
         * Filtrando la introducción de la Observación
         */
        $("#observacion").on('keyup blur', function(){
            keyAlphaNum(this, true, true);
            makeUpper(this);
        });
    },
    isRequiredObservacion: function(valorTotal){
        if(isNaN(configPuntuacionMinimaAprobacion)){
            $("#observacion").attr("required", "required");
        }
        else{
            if(valorTotal<configPuntuacionMinimaAprobacion){
                $("#observacion").attr("required", "required");
                $("#observacion").attr("placeholder", "Debe dejar registro de una Observación Indicando el Por qué esta Persona no ha aprobado esta fase. Puntuación Obtenida: "+valorTotal+".");
                $("#observacion").attr("x-moz-errormessage", "Debe dejar registro de una Observación Indicando el Por qué esta Persona no ha aprobado esta fase. Puntuación Obtenida: "+valorTotal+".");
                $("#observacion").attr("title", "Debe dejar registro de una Observación Indicando el Por qué esta Persona no ha aprobado esta fase. Puntuación Obtenida: "+valorTotal+".");
            }
            else{
                $("#observacion").removeAttr("required");
                $("#observacion").removeAttr("placeholder");
                $("#observacion").removeAttr("x-moz-errormessage");
                $("#observacion").attr("title", "Si lo ve necesario puede hacer registro una observación");
            }
        }
    }
};
