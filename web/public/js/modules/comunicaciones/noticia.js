/**
 * Created by nelson on 30/10/14.
 */
var formValidado = false;
/****************************************************/



/*$("#noticia-form").submit(function() {
 console.log('sdasd');
 cambio = $("#cuerpo").html();
 $("#cuerpo2").html(cambio);

 });*/


/***************************************************/
$.urlParam = function (name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results != null)
        return results[1] || 0;
    else
        return results;
    //return results != null ? results[1] || 0 : results;
}
/******************************************************/

$(document).ready(function() {
    /**************DESDE AQUI CARGAMOS LAS FUNCIONALIDADES WIN******/
    function showErrorAlert(reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
        }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    }
    //$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

    //but we want to change a few buttons colors for the third style
    $('#cuerpo').ace_wysiwyg({
        toolbar:
            [
                'font',
                null,
                {name: 'bold', title:'Negrita', className:'btn-info'},
                {name: 'italic', className: 'btn-info'},
                {name: 'strikethrough', className: 'btn-info'},
                {name: 'underline', className: 'btn-info'},
                null,
                {name: 'insertunorderedlist', className: 'btn-success'},
                {name: 'insertorderedlist', className: 'btn-success'},
                {name: 'outdent', className: 'btn-purple'},
                {name: 'indent', className: 'btn-purple'},
                null,
                {name: 'justifyleft', className: 'btn-primary'},
                {name: 'justifycenter', className: 'btn-primary'},
                {name: 'justifyright', className: 'btn-primary'},
                {name: 'justifyfull', className: 'btn-inverse'},
                null,
                null,
                {name:'createLink', className:'btn-pink'},
                {name: 'unlink', className: 'btn-primary'},
                null,
                {name:'insertImage', className:'btn-success'},
                null,
                {name: 'undo', className: 'btn-grey'},
                {name: 'redo', className: 'btn-grey'}
            ],
        hotKeys: {
            'ctrl+b meta+b': 'bold',
            'ctrl+i meta+i': 'italic',
            'ctrl+u meta+u': 'underline',
            'ctrl+z meta+z': 'undo',
            'ctrl+y ctrl+shift+z meta+y meta+shift+z': 'redo'
        },
        'wysiwyg': {
            fileUploadError: showErrorAlert
        }
    }).prev().addClass('wysiwyg-style1');

    $('#editor2').css({'height': '200px'}).ace_wysiwyg({
        toolbar_place: function(toolbar) {
            return $(this).closest('.widget-box').find('.widget-header').prepend(toolbar).children(0).addClass('inline');
        },
        toolbar:
            [
                'bold',
                {name: 'italic', title: 'Change Title!', icon: 'icon-leaf'},
                'strikethrough',
                null,
                'insertunorderedlist',
                'insertorderedlist',
                null,
                'justifyleft',
                'justifycenter',
                'justifyright'
            ],
        speech_button: false
    });
    $('[data-toggle="buttons"] .btn').on('click', function(e) {
        var target = $(this).find('input[type=radio]');
        var which = parseInt(target.val());
        var toolbar = $('#cuerpo').prev().get(0);
        if (which == 1 || which == 2 || which == 3) {
            toolbar.className = toolbar.className.replace(/wysiwyg\-style(1|2)/g, '');
            if (which == 1)
                $(toolbar).addClass('wysiwyg-style1');
            else if (which == 2)
                $(toolbar).addClass('wysiwyg-style2');
        }
    });
    //Add Image Resize Functionality to Chrome and Safari
    //webkit browsers don't have image resize functionality when content is editable
    //so let's add something using jQuery UI resizable
    //another option would be opening a dialog for user to enter dimensions.
    if (typeof jQuery.ui !== 'undefined' && /applewebkit/.test(navigator.userAgent.toLowerCase())) {

        var lastResizableImg = null;
        function destroyResizable() {
            if (lastResizableImg == null)
                return;
            lastResizableImg.resizable("destroy");
            lastResizableImg.removeData('resizable');
            lastResizableImg = null;
        }
        var enableImageResize = function() {
            $('.wysiwyg-editor')
                .on('mousedown', function(e) {
                    var target = $(e.target);
                    if (e.target instanceof HTMLImageElement) {
                        if (!target.data('resizable')) {
                            target.resizable({
                                aspectRatio: e.target.width / e.target.height
                            });
                            target.data('resizable', true);

                            if (lastResizableImg != null) {//disable previous resizable image
                                lastResizableImg.resizable("destroy");
                                lastResizableImg.removeData('resizable');
                            }
                            lastResizableImg = target;
                        }
                    }
                })
                .on('click', function(e) {
                    if (lastResizableImg != null && !(e.target instanceof HTMLImageElement)) {
                        destroyResizable();
                    }
                })
                .on('keydown', function() {
                    destroyResizable();
                });
        }

        enableImageResize();

        /**
         //or we can load the jQuery UI dynamically only if needed
         if (typeof jQuery.ui !== 'undefined') enableImageResize();
         else {//load jQuery UI if not loaded
         $.getScript($path_assets+"/js/jquery-ui-1.10.3.custom.min.js", function(data, textStatus, jqxhr) {
         if('ontouchend' in document) {//also load touch-punch for touch devices
         $.getScript($path_assets+"/js/jquery.ui.touch-punch.min.js", function(data, textStatus, jqxhr) {
         enableImageResize();
         });
         } else  enableImageResize();
         });
         }
         */
    }





    /****************************************************/


    /*******AQUI ESTA EL DATA-PICKER  ****/
    $('#cuerpo').keyup(function(){
        $('#cuerpo2').val($('#cuerpo').html());
    });
    $('#date-picker').datepicker({
            minDate: '+30D'
        }
    );

    /**********************************************************/
    $('#date-picker-CGridView').datepicker();
    var fecha = new Date();
    var anoActual = fecha.getFullYear();
    var fecha = new Date();
    var anoActual = fecha.getFullYear();
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: "yy-mm-dd",
        showOn:"focus",
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        closeText: "Cerrar",
        prevText: "<-Ant",
        nextText: "Sig->",
        currentText: "Hoy",
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene","Feb","Mar","Abr", "May","Jun","Jul","Ago","Sep", "Oct","Nov","Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesShort: ["Dom","Lun","Mar","Mié","Juv","Vie","Sáb"],
        dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
        weekHeader: "Sm"
    });
    /**************************************************************/

    $('#Noticia_titulo').bind('keyup blur', function () {
        keyText(this, true);
    });

    $('#Modalidad_nombre').bind('blur', function () {
        clearField(this);
    });
    //$('#Noticia_fecha_de_clausura').daterangepicker();
    $('#Noticia_titulo').bind('keyup blur', function () {
        keyText(this, true);
    });

    /*******DESD AQUI CARGAMOS EL TEXTAREA OCULTO ****/
    edit = $("#cuerpo2").val();
    $("#cuerpo").html(edit);


    $("#cuerpo").keyup(function() {
        cambio = $("#cuerpo").html();
        $("#cuerpo2").html(cambio);

    });

    $("#btn_guardar").unbind("click");
    $("#btn_guardar").on("click", function() {

        cambio = $("#cuerpo").html();
        $("#cuerpo2").html(cambio);
        $("#noticia-form").submit();
    });

});
/****************************************/

