
$(document).ready(function() {

//    $('#tipoDocumento').change(function(e) {
//        e.preventDefault();
//        var tipo_ficha;
//        tipo_ficha = $(this).val();
//        // tipo_ficha = $('#tipoDocumento').val()
//        console.log($(this).val());
//    })


    $("#fileupload").click(function(e) {
        e.preventDefault();
        var tipo_ficha;
        var documento;

        tipo_ficha = $('#tipoDocumento').val();
        documento = $('#documento').val();
        var style = 'alerta';
        var msgtitulo = "<p>Estimado usuario, por favor se requiere que ingrese ambos campos para poder realizar esta acción .</p>";
        //console.log(documento);

        if (tipo_ficha != '' && documento != '') {
            $('#fileupload').unbind('click');
            $('#fileupload').fileupload({
                url: '/expediente/insercion/CargarExpediente/',
                acceptFileTypes: /(\.|\/)(pdf|png|jpg|jpeg|jpe)$/i,
                maxFileSize: 50000000, // 50MB
                singleFileUploads: true,
                autoUpload: true,
                process: [
                    {
                        action: 'load',
                        fileTypes: /(\.|\/)(xls?x|ods)$/i,
                        maxFileSize: 50000000 // 50MB
                    },
                    {
                        action: 'resize',
                        maxWidth: 1440,
                        maxHeight: 900
                    },
                    {
                        action: 'save'
                    }
                ],
//                formData: [{
//                        tipo_ficha: tipo_ficha,
//                    }],
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    alert("Se ha producido un error en la carga del archivo.");
                }

            });
        } else {


            displayDialogBox('alertInsercion', style, msgtitulo);




        }
    });
    $('#fileupload').bind('fileuploaddone', function(e, data) {
        var archivos = data.jqXHR.responseJSON.files;

        $.each(archivos, function(index, file) {


            var tipo_ficha;
            var documento;
            var descripcion;
            descripcion = $('#Descripción').val();
            tipo_ficha = $('#tipoDocumento').val();
            documento = $('#documento').val();
            var archivo = file.name;
            //console.log(file);
            var divResult = "dialog_vizualizar";
            var urlDir = "/expediente/insercion/ValidarArchivo/";
            var datos = {
                archivo: archivo,
                documento: documento,
                tipo_ficha: tipo_ficha,
                descripcion: descripcion,
            };
            var loadingEfect = false;
            var showResult = false;
            var method = "POST";
            var responseFormat = "json";
            var beforeSend = null;
            var successCallback = null;

            successCallback = function(response) {
                if (response.statusCode == 'SUCCESS') {
                    dialogo(response.mensaje, 'Registro Exitoso', divResult, 'check')
                }
                if (response.statusCode == 'ERROR') {
                    dialogo(response.mensaje, 'Notificación de Error', divResult, 'exclamation-triangle')
                }
                if (response.statusCode == 'INFO') {
                    dialogo(response.mensaje, 'Notificación de Alerta', divResult, 'exclamation-triangle')
                }
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, successCallback);
        });
    });

});

function dialogo(mensaje, title, divResult, icon) {
    displayDialogBox(divResult, 'info', mensaje);
    var dialog = $("#" + divResult).removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-" + icon + "'></i> " + title + " </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}