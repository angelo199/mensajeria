$('#aplicar_prueba_form').submit(function(evt) {
    evt.preventDefault();
    var mensaje = '';
    var prueba_id = $('#Prueba_id').val();

    if (prueba_id != '') {
        $('#Prueba_id').removeClass('error');
        $("#msj_error").addClass('hide');
        $("#msj_error p").html('');

        $.ajax({
            url: "/aplicarPrueba/aplicarPrueba/iniciarPrueba",
            data: $('#aplicar_prueba_form').serialize(),
            dataType: 'html',
            type: 'post',
            success: function(resp, resp2, resp3) {
                try {
                    var json = jQuery.parseJSON(resp3.responseText);
//                    if (json.statusCode == "success") {
//                        console.log('entre success');
//                        $("#msj_error").addClass('hide');
//                        $("#msj_error p").html('');
////                        $("#msj_exitoso").removeClass('hide');
////                        $("#msj_exitoso p").html(json.mensaje);
//                        $('#index').html(json.vista);
//                        $("html, body").animate({scrollTop: 0}, "fast");
//                }
                    if (json.statusCode == "error") {
                        $("#msj_exitoso").addClass('hide');
                        $("#msj_exitoso p").html('');
                        $("#msj_error").removeClass('hide');
                        $("#msj_error p").html(json.mensaje);
                        $("html, body").animate({scrollTop: 0}, "fast");
                    }
                }
                catch (e) {
                    $("#msj_error").addClass('hide');
                    $("#msj_error p").html('');
                    $("#index").html(resp);
                    $("html, body").animate({scrollTop: 0}, "fast");
                }
            }
        });
    } else {
        $('#Prueba_id').addClass('error');
        mensaje = 'Por favor debe seleccionar al menos una prueba para ser aplicada.';
        $("#msj_error").removeClass('hide');
        $('#msj_error p').html(mensaje);
        $("html, body").animate({scrollTop: 0}, "fast");
    }
});


//Inicio de prueba de los aspirantes.
$('#inicio_prueba_asp_form').submit(function(evt) {
    evt.preventDefault();
    var prueba_id = '';

    $.ajax({
        url: "/aplicarPrueba/aplicarPrueba/validarExistenciaPruebaActiva",
        dataType: 'html',
        type: 'post',
        success: function(resp, resp2, resp3) {

            try {
                var json = jQuery.parseJSON(resp3.responseText);
                if (json.statusCode == "success") {
                    prueba_id = json.prueba_id;
                    $.ajax({
                        url: "/aplicarPrueba/aplicarPrueba/mostrarPruebaAsp",
                        data: {prueba_id: prueba_id},
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {
                            try {
                                var json = jQuery.parseJSON(resp3.responseText);
                                if (json.statusCode == "success") {
                                    console.log('entre success');
                                    $("#msj_error").addClass('hide');
                                    $("#msj_error p").html('');
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                                if (json.statusCode == "error") {
                                    console.log('entre error');
                                    $("#msj_error").removeClass('hide');
                                    $("#msj_error p").html(json.mensaje);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            }
                            catch (e) {
                                $("#msj_error").addClass('hide');
                                $("#msj_error p").html('');
                                $('.cerrar').addClass('hide');
                                $("#indexAsp_form").html(resp);
                                $("html, body").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });

                }
                if (json.statusCode == "error") {
                    console.log('entre error');
                    $("#msj_error").removeClass('hide');
                    $("#msj_error p").html(json.mensaje);
                    $("html, body").animate({scrollTop: 0}, "fast");
                }
            }
            catch (e) {
                console.log('No existen pruebas activas');
            }
        }
    });

});
