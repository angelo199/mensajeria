<div class="errorDialogBox">
    <button onclick="$(this).parent().fadeOut('slow');" class="close" type="button"><span aria-hidden="true">&times;</span></button>
    <?php
        echo CHtml::errorSummary($model);
    ?>
</div>
