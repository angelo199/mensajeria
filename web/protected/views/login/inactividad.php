<div class="col-sm-10 col-sm-offset-1" style="min-height: 400px;">
    <div class="login-container">
        <div class="space-6"></div>
        <div class="position-relative">
            <div>
                <div class="signup-box widget-box no-border visible" id="pass-request-box">
                    <div class="widget-body">
                        <div class="widget-main">
                            <h4 class="header green lighter bigger">
                                <i class="icon-group blue"></i>
                                Inactividad
                            </h4>
                            <div class="space-6"></div>
                            <div class="infoDialogBox">
                                <p>
                                    Se ha detectado inactividad. Parece que no ha efectuado ninguna acción sobre el sistema durante un buen tiempo. Si desea ingresar al sistema haga click en "<a href="/login">Volver al Login</a>".
                                </p>
                            </div>
                        </div>

                        <div class="toolbar center">
                            <a class="back-to-login-link" href="/login">
                                <i class="icon-arrow-left"></i>
                                Volver al Login
                            </a>
                        </div>
                    </div><!-- /widget-body -->
                </div>
            </div>

        </div>
    </div>

</div>
