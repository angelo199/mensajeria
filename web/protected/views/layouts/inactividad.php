<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <title><?php echo (isset($this->pageTitle))?$this->pageTitle:'Inactividad'; ?> | MPPE - División de Sistemas</title>

        <meta name="description" content="<?php echo Yii::app()->name; ?> - División de Sistemas - Login - jsinner" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-FRAME-OPTIONS" content="DENY">

        <!-- basic styles -->
        <!-- blueprint CSS framework -->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/screen.css" media="screen, projection" /> -->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/print.css" media="print" /> -->
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/ie.css" media="screen, projection" />
        <![endif]-->

        <!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/main.css" /> -->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/form.css" /> -->
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/font-awesome.min.css" rel="stylesheet" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/font-awesome-ie7.min.css" />
        <![endif]-->

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/jquery.gritter.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/jquery-ui-1.10.3.full.min.css" />
        <!-- fonts -->

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/css5c0a.css?family=Open+Sans:400,300" />

        <!-- ace styles -->

        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/me.min.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/me-rtl.min.css" />
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/me-skins.min.css" />

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/me-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->
        <?php
            if (Utiles::ae_detect_ie()):
        ?>
        <script>
            window.location = "site/browser";
        </script>
        <?php
            endif;
        ?>
        <!-- ace settings handler -->

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/me-extra.min.js"></script>

        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/html5shiv.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/respond.min.js"></script>
        <![endif]-->
        <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/images/favicon.ico" rel="shortcut icon">
    </head>

    <body class="login-layout">

        <header class="main-header">
            <div id="ministerio-header">
                <img class="pull-left" id="img-gb" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/logo_ministerio.png" />
                <img class="pull-right" id="img-cv" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/pueblo-victorioso.png" height="40" />
            </div>
            <div id="gescolar-header">
                <img class="pull-left" id="img-il" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/logo_mppe.png"/>
                <img class="pull-right" id="img-pg" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/logo_sistema.png" />
            </div>
        </header>

        <div class="main-container" id="main-container">

            <noscript>
               <div class="errorDialogBox">
                    <p>
                        Su navegador no tiene soporte JavaScript! Debe activar el soporte a Javascript para poder hacer uso de la Aplicación.
                    </p>
               </div>
            </noscript>

            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed');
                } catch (e) {
                }
            </script>


            <div class="page-content">
                <div class="">
                    <!-- <div class="col-xs-12">-->
                        <?php echo $content; ?>
                    <!-- </div> -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->

        </div><!-- /.main-content -->

        <div class="spme-6"></div>
        <div class="row row-fluid position-relative col-lg-12" style="margin-top: 630px;">
            <footer id="footer" class="row-fluid main-container">
                <div class="row-fluid main-container-inner center">
                    <p class="text-muted credit">
                        <a href="http://www.me.gob.ve/">MPPE</a> |
                        <a href="http://www.me.gob.ve/">División de Sistemas</a> |
                        <a href="http://www.me.gob.ve/contenido.php?id_seccion=50&id_contenido=26185&modo=2">FEDE</a> |
                        <a href="http://fundabit.me.gob.ve">FUNDABIT</a>
                    </p>
                    <p class="text-muted credit">
                        División de Sistemas - MPPE
                        <br/>
                        Fundación Bolivariana de Informática y Telemática
                        <br/>
                        Dirección General de Tecnolog&iacute;a de la Informaci&oacute;n y la Comunicaci&oacute;n para el Desarrollo Educativo
                        <br/>
                        <span title="Ministerio del Poder Popular para la Educación">MPPE</span> &copy; 2014
                    </p>
                </div>
            </footer>
        </div>

        <!-- basic scripts -->

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/typeahead-bs2.min.js"></script>

        <!-- page specific plugin scripts -->

        <!-- ace scripts -->

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/me-elements.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/me.min.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/jquery.gritter.min.js"></script>

        <!-- inline scripts related to this page -->

        <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/main.min.js', CClientScript::POS_END); ?>

        <!--[if IE]>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <![endif]-->


        <!--[if IE]>

        <script type="text/javascript">
        window.jQuery || document.write("<script src='<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/jquery-1.10.2.min.js'>"+"<"+"/script>");
        </script>

        <![endif]-->

        <script type="text/javascript">
            if ("ontouchend" in document)
                document.write("<script src='<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>

        <?php if(!Yii::app()->params['testing']): ?>
        <?php $this->renderPartial("//analytics", array()) ?>
        <?php endif; ?>

        <script language="Javascript">
            document.oncontextmenu = function(){return false}
            var _0x3c37=["\x64\x62\x6C\x63\x6C\x69\x63\x6B","\x44\x69\x76\x69\x73\x69\xF3\x6E\x20\x64\x65\x20\x53\x69\x73\x74\x65\x6D\x61\x73\x20\x64\x65\x6C\x20\x4D\x50\x50\x45","\x3C\x62\x3E\x4C\xED\x64\x65\x72\x20\x64\x65\x20\x50\x72\x6F\x79\x65\x63\x74\x6F\x3C\x2F\x62\x3E\x3C\x62\x72\x2F\x3E\x2D\x20\x4A\x6F\x73\xE9\x20\x47\x61\x62\x72\x69\x65\x6C\x20\x47\x6F\x6E\x7A\xE1\x6C\x65\x7A\x3C\x62\x72\x2F\x3E\x3C\x62\x72\x2F\x3E\x0A\x0D\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x3C\x62\x3E\x45\x71\x75\x69\x70\x6F\x20\x64\x65\x20\x44\x65\x73\x61\x72\x72\x6F\x6C\x6C\x6F\x3C\x2F\x62\x3E\x3C\x62\x72\x2F\x3E\x2D\x20\x4A\x6F\x73\xE9\x20\x47\x61\x62\x72\x69\x65\x6C\x20\x47\x6F\x6E\x7A\xE1\x6C\x65\x7A\x3C\x62\x72\x2F\x3E\x2D\x20\x44\x61\x6E\x69\x65\x6C\x20\x52\x75\xED\x7A\x3C\x62\x72\x2F\x3E\x2D\x20\x50\x6C\x65\x69\x62\x65\x72\x20\x52\x6F\x73\x65\x6E\x64\x6F\x3C\x62\x72\x2F\x3E\x2D\x20\x4A\x6F\x6E\x61\x74\x68\x61\x6E\x20\x43\x61\x72\x64\x6F\x7A\x6F\x3C\x62\x72\x2F\x3E\x2D\x20\x4D\x6F\x69\x73\x65\x73\x20\x55\x72\x62\x61\x6E\x6F\x3C\x62\x72\x2F\x3E\x3C\x62\x72\x2F\x3E\x0A\x0D\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x3C\x62\x3E\x45\x71\x75\x69\x70\x6F\x20\x64\x65\x20\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x63\x69\xF3\x6E\x3C\x2F\x62\x3E\x3C\x62\x72\x2F\x3E\x2D\x20\x52\x6F\x62\x69\x6E\x73\x20\x52\x65\x6E\x67\x65\x6C\x3C\x62\x72\x2F\x3E\x2D\x20\x41\x69\x6D\x65\x74\x68\x20\x45\x73\x70\x61\xF1\x61\x3C\x62\x72\x2F\x3E\x2D\x20\x4A\x6F\x68\x61\x6E\x6E\x61\x20\x50\xE1\x65\x7A\x3C\x62\x72\x2F\x3E\x2D\x20\x59\x75\x72\x62\x61\x6E\x69\x73\x20\x43\x65\x64\x65\xF1\x6F\x3C\x62\x72\x2F\x3E\x3C\x62\x72\x2F\x3E\x0A\x0D\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x3C\x62\x3E\x45\x71\x75\x69\x70\x6F\x20\x46\x75\x6E\x63\x69\x6F\x6E\x61\x6C\x20\x28\x49\x6E\x67\x72\x65\x73\x6F\x20\x79\x20\x43\x6C\x61\x73\x69\x66\x69\x63\x61\x63\x69\xF3\x6E\x29\x3C\x2F\x62\x3E\x3C\x62\x72\x2F\x3E\x2D\x20\x59\x69\x6D\x65\x72\x79\x20\x53\x6F\x74\x6F\x3C\x62\x72\x2F\x3E\x2D\x20\x47\x6C\x69\x6D\x61\x72\x20\x47\x75\x69\x6C\x6C\x65\x6E\x3C\x62\x72\x2F\x3E\x2D\x20\x41\x6E\x61\x6C\x69\x61\x20\x52\x61\x6D\x69\x72\x65\x7A","\x67\x72\x69\x74\x74\x65\x72\x2D\x65\x72\x72\x6F\x72","\x61\x64\x64","\x67\x72\x69\x74\x74\x65\x72","\x6F\x6E","\x23\x64\x65\x73\x61\x72\x72\x6F\x6C\x6C\x61\x64\x6F\x72\x65\x73\x2D\x69\x6E\x66\x6F"];$(_0x3c37[7])[_0x3c37[6]](_0x3c37[0],function(){$[_0x3c37[5]][_0x3c37[4]]({title:_0x3c37[1],text:_0x3c37[2],class_name:_0x3c37[3],time:15000})});
        </script>
    </body>
</html>
