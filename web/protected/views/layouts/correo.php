<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
    </head>
    <body>
        <img width="670" alt="Ministerio del Poder Popular para La Educación" src="http://me.gob.ve/images/bar_gob.png">
        <br/>
        <div align="left">
            <p>Un saludo cordial del Ministerio del Poder Popular para la Educación (MPPE).</p>
            <p><?php echo $content; ?></p>
        </div>
        <br/>
        <div align="left" style="color: #990000;">
            --------
            <br/>
            <?php echo Yii::app()->params['divisionCliente']; ?>
            <br/>
            <?php echo Yii::app()->params['appName']; ?>
            <br/>
            División de Sistemas del MPPE
        </div>
        <br/></br><br/>
        <div align="center" style="color: #222A2D; font-size: 10px;">
            "Todos los niños tienen derecho desde el momento de nacer, por nacer y por ser seres humanos, 
            el derecho a la educación; es un derecho natural. No podemos privatizar ni eliminar ese derecho" 
            <br/>
            Hugo Chávez Frías
        </div>
        <div align="center" style="color: green; font-size: 10px;">
            Si no es necesaria la impresión de este correo, evite imprimirlo. Todos somos responsables del cuidado del medio ambiente.
            <br/>
            Un mensaje de MPPE
        </div>
        <br/>
        <div align="center" style="color: #222A2D; font-size: 10px;">
            Si usted ha recibido este correo de forma equivocada notifíquelo al correo <?php echo Yii::app()->params['adminEmailSend']; ?> y luego elimine su contenido.
            <br/>
            El contenido absoluto de este correo y sus adjuntos pertenecen al Ministerio del Poder Popular para La Educación y Fundabit cualquier difusión de esta información a entes externos sin la debida autorización del Ministerio 
            recurrirá a la apertura de procesos administrativos y legales.
            <br/>
            Evite ser sancionado.
        </div>
    </body>
</html>
       
