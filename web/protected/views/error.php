<div class="row">
    <!-- <div class="col-xs-12">-->
    <div class="errorDialogBox">
        <p>
            <b><?php echo $code; ?></b>
        </p>
        <p>
            <?php echo CHtml::encode($message); ?>
        </p>
    </div>

    <?php if(isset(Yii::app()->params['testing']) && Yii::app()->params['testing']): ?>
        <?php var_dump(Yii::app()->errorHandler->error); ?>
    <?php endif; ?>
    <!-- </div> -->
    <!-- /.col -->
</div><!-- /.row -->
<div class="row center">
    <a class="btn btn-primary" href="<?php echo (isset($url)) ? $url : Yii::app()->request->urlReferrer ; ?>" id="btnRegresar">
        <i class="icon-arrow-left"></i>
        Volver a la Página Anterior
    </a>
</div>
