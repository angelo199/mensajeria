<?php
/* @var $this DocumentController */
/* @var $model Document */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'acceso'); ?>
		<?php echo $form->textField($model,'acceso',array('size'=>13, 'maxlength'=>13, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ext_acceso'); ?>
		<?php echo $form->textField($model,'ext_acceso',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jerarquia'); ?>
		<?php echo $form->textField($model,'jerarquia',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ubicacion'); ?>
		<?php echo $form->textField($model,'ubicacion',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_liter'); ?>
		<?php echo $form->textField($model,'tipo_liter',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nivel_bibl'); ?>
		<?php echo $form->textField($model,'nivel_bibl',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nivel_reg'); ?>
		<?php echo $form->textField($model,'nivel_reg',array('size'=>3, 'maxlength'=>3, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_ref_analitica'); ?>
		<?php echo $form->textField($model,'tipo_ref_analitica',array('size'=>2, 'maxlength'=>2, 'class' => 'span-12', "required"=>"required",)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'paginas'); ?>
		<?php echo $form->textField($model,'paginas',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'volumen'); ?>
		<?php echo $form->textField($model,'volumen',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'volumen_final'); ?>
		<?php echo $form->textField($model,'volumen_final',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numero'); ?>
		<?php echo $form->textField($model,'numero',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numero_final'); ?>
		<?php echo $form->textField($model,'numero_final',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numeracion'); ?>
		<?php echo $form->textField($model,'numeracion',array('size'=>60, 'maxlength'=>64, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'enum_nivel_ini_3'); ?>
		<?php echo $form->textField($model,'enum_nivel_ini_3',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'enum_nivel_ini_4'); ?>
		<?php echo $form->textField($model,'enum_nivel_ini_4',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'enum_nivel_fin_3'); ?>
		<?php echo $form->textField($model,'enum_nivel_fin_3',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'enum_nivel_fin_4'); ?>
		<?php echo $form->textField($model,'enum_nivel_fin_4',array('size'=>8, 'maxlength'=>8, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_editor'); ?>
		<?php echo $form->textField($model,'cod_editor',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_editor_inst'); ?>
		<?php echo $form->textField($model,'cod_editor_inst',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'edicion'); ?>
		<?php echo $form->textField($model,'edicion',array('size'=>60, 'maxlength'=>100, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_iso'); ?>
		<?php echo $form->textField($model,'fecha_iso',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_iso_final'); ?>
		<?php echo $form->textField($model,'fecha_iso_final',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_pub'); ?>
		<?php echo $form->textField($model,'fecha_pub',array('size'=>60, 'maxlength'=>80, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'isbn'); ?>
		<?php echo $form->textField($model,'isbn',array('size'=>20, 'maxlength'=>20, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'indice_suplemento'); ?>
		<?php echo $form->textField($model,'indice_suplemento',array('size'=>32, 'maxlength'=>32, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_conf'); ?>
		<?php echo $form->textField($model,'cod_conf',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cod_proy'); ?>
		<?php echo $form->textField($model,'cod_proy',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disemina'); ?>
		<?php echo $form->textField($model,'disemina',array('size'=>50, 'maxlength'=>50, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60, 'maxlength'=>255, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'n_disk_cin'); ?>
		<?php echo $form->textField($model,'n_disk_cin',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sala'); ?>
		<?php echo $form->textField($model,'sala',array('size'=>35, 'maxlength'=>35, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'impresion'); ?>
		<?php echo $form->textField($model,'impresion',array('size'=>25, 'maxlength'=>25, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'orden'); ?>
		<?php echo $form->textField($model,'orden',array('size'=>4, 'maxlength'=>4, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cerrado'); ?>
		<?php echo $form->textField($model,'cerrado',array('size'=>1, 'maxlength'=>1, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_iso_inicio'); ?>
		<?php echo $form->textField($model,'hora_iso_inicio',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hora_iso_final'); ?>
		<?php echo $form->textField($model,'hora_iso_final',array('size'=>15, 'maxlength'=>15, 'class' => 'span-12', )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id', array('class' => 'span-12',"required"=>"required",)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->