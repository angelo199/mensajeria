<?php
/**
 * Este controlador efectuará los procesos de registro y confirmación de usuarios externos.
 *
 * @author developer
 */
class RegistroController extends Controller {
    //public $layout='//layouts/cuerpo';   // DEFINICIÒN DEL LAYOUT

    public $defaultAction = 'nuevoUsuario';
    
    public $layout = '//layouts/login';
    
    // PARTE I
    static $_permissionControl = array(
        'read' => 'Ver el formulario de Usuarios',
        'write' => 'Registro de Usuarios',
        'admin' => 'Jefe de Administración de Usuarios',
        'label' => 'Registro de Usuarios del Sistema'
    );

    /**
     * 
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    public function actionNuevoUsuario() {
        Controller::forward('userGroups/registroExternoUsuario/nuevo');
    }
    
    public function actionActivarCuenta($cod) {
        Controller::forward('userGroups/registroExternoUsuario/activarCuenta/cod/'.$cod);
    }
    
}
