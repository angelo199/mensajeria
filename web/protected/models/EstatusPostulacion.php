<?php

/**
 * This is the model class for table "gestion_humana.estatus_postulacion".
 *
 * The followings are the available columns in table 'gestion_humana.estatus_postulacion':
 * @property integer $id
 * @property string $nombre
 * @property string $descripcion
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $estatus
 * @property integer $orden
 * @property integer $tipo_proceso_id
 *
 * The followings are the available model relations:
 * @property Postulacion[] $postulacions
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property TipoPeriodoProceso $tipoProceso
 */
class EstatusPostulacion extends CActiveRecord {

    /**
     * @var int ESP_CONSIG_DOCUMENTS En Espera de Consignación de Documenos
     *
     * Es el único estatus de la postulación cuya movilización depende de la persona postulada, ya que si no consigna los
     * documentos la postulación no podrá pasar a otro estatus.
     *
     */
    const ESP_CONSIGN_DOCUMENTOS = 0;

    /**
     * @var int POST_APROBADA Postulación Aprobada
     *
     * En Sistema de Mérito: En Espera de Evaluación de Requisitos Mínimos.
     *
     * En Concurso Público: En Espera de la Presentación de la Prueba de Conocimiento.
     *
     */
    const POST_APROBADA = 1;

    /**
     * @var int PRUEBA_CONOC_APROBADA Prueba de Concocimiento Aprobada (En Espera de Evaluación de Requisitos Mínimos, Sólo para Concurso Público)
     */
    const PRUEBA_CONOC_APROBADA = 2;

    /**
     * @var int REQ_MIN_APROBADOS Requerimientos Mínimos Aprobados (En Espera de la Entrevista)
     */
    const REQ_MIN_APROBADOS = 3;

    /**
     * @var int BENEFICIADO Beneficiado, ha Aprobado el Proceso de Entrevista (Debe Esperar al Proceso Automáico de Elección el cual pasará a Ganador o Elegible).
     *
     */
    const BENEFICIADO = 4;

    /**
     * @var int NO_BENEFICIADO No ha aprobado el Proceso Anterior (Los Procesos que pueden caer en este estatus: Aprobación, Eval. de Requerimientos Mínimos, Presentación de Prueba de Conocimiento, Entrevista, Proceso Automático de Elección)
     */
    const NO_BENEFICIADO = 5;

    /**
     * @var int ELEGIBLE Ha sido seleccionado como Elegible.
     */
    const ELEGIBLE = 6;

    /**
     * @var int GANADOR Ha sido seleccionado como Ganador del Proceso.
     */
    const GANADOR = 7;


    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gestion_humana.estatus_postulacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, usuario_ini_id, fecha_ini, fecha_act', 'required'),
            array('usuario_ini_id, usuario_act_id, orden, tipo_proceso_id', 'numerical', 'integerOnly' => true),
            array('nombre', 'length', 'max' => 20),
            array('estatus', 'length', 'max' => 1),
            array('estatus', 'in', 'range' => array('A', 'I', 'E'), 'allowEmpty' => false, 'strict' => true,),
            array('usuario_ini_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'insert'),
            array('usuario_act_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'update'),
            array('descripcion', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, descripcion, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, estatus, orden, tipo_proceso_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'postulacions' => array(self::HAS_MANY, 'Postulacion', 'estatus_aprobado'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'tipoProceso' => array(self::BELONGS_TO, 'TipoPeriodoProceso', 'tipo_proceso_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'estatus' => 'Estatus',
            'orden' => 'Orden',
            'tipo_proceso_id' => 'Tipo Proceso',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        if (is_numeric($this->id))
            $criteria->compare('id', $this->id);
        if (strlen($this->nombre) > 0)
            $criteria->compare('nombre', $this->nombre, true);
        if (strlen($this->descripcion) > 0)
            $criteria->compare('descripcion', $this->descripcion, true);
        if (is_numeric($this->usuario_ini_id))
            $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d'))
            $criteria->compare('fecha_ini', $this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (is_numeric($this->usuario_act_id))
            $criteria->compare('usuario_act_id', $this->usuario_act_id);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d'))
            $criteria->compare('fecha_act', $this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (in_array($this->estatus, array('A', 'I', 'E')))
            $criteria->compare('estatus', $this->estatus, true);
        if (is_numeric($this->orden))
            $criteria->compare('orden', $this->orden);
        if (is_numeric($this->tipo_proceso_id))
            $criteria->compare('tipo_proceso_id', $this->tipo_proceso_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeInsert() {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    public function __toString() {
        try {
            return (string) $this->id;
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EstatusPostulacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
