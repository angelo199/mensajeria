<?php

/**
 * This is the model class for table "proveedor.centro_salud".
 *
 * The followings are the available columns in table 'proveedor.centro_salud':
 * @property integer $id
 * @property string $rif
 * @property string $nombre
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $direccion
 * @property string $nombre_contacto
 * @property string $telefono_principal
 * @property string $telefono_opcional
 * @property string $email
 * @property string $monto_promedio_consulta
 * @property string $servicio_id
 * @property string $activa
 * @property string $de_confianza
 * @property integer $calificacion_actual_usuarios
 * @property integer $calificacion_actual_ministerio
 * @property string $checksum
 * @property string $direccion_ip_ini
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property string $direccion_ip_act
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $direccion_ip_elim
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Parroquia $parroquia
 * @property Servicio $servicio
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 * @property Estado $estado
 * @property Municipio $municipio
 */
class CentroSalud extends CActiveRecord
{
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
            return 'proveedor.centro_salud';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rif, nombre, estado_id, direccion, servicio_id, activa, de_confianza, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act', 'required'),
			array('estado_id, municipio_id, parroquia_id, calificacion_actual_usuarios, calificacion_actual_ministerio, usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly'=>true),
			array('rif', 'length', 'max'=>15),
			array('nombre, nombre_contacto', 'length', 'max'=>150),
			array('telefono_principal, telefono_opcional', 'length', 'max'=>14),
			array('email', 'length', 'max'=>100),
			array('activa, de_confianza, estatus', 'length', 'max'=>1),
			array('checksum', 'length', 'max'=>255),
			array('direccion_ip_ini, direccion_ip_act, direccion_ip_elim', 'length', 'max'=>40),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			array('email', 'email'),
			array('monto_promedio_consulta', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, rif, nombre, estado_id, municipio_id, parroquia_id, direccion, nombre_contacto, telefono_principal, telefono_opcional, email, monto_promedio_consulta, servicio_id, activa, de_confianza, calificacion_actual_usuarios, calificacion_actual_ministerio, checksum, direccion_ip_ini, usuario_ini_id, fecha_ini, direccion_ip_act, usuario_act_id, fecha_act, direccion_ip_elim, usuario_elim_id, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parroquia' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_id'),
			'servicio' => array(self::BELONGS_TO, 'ProveedorServicio', 'servicio_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
			'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rif' => 'RIF',
			'nombre' => 'Nombre',
			'estado_id' => 'Estado',
			'municipio_id' => 'Municipio',
			'parroquia_id' => 'Parroquia',
			'direccion' => 'Direccion',
			'nombre_contacto' => 'Nombre del Contacto',
			'telefono_principal' => 'Telefono Principal',
			'telefono_opcional' => 'Telefono Opcional',
			'email' => 'Correo Electronico',
			'monto_promedio_consulta' => 'Monto Promedio de Consulta',
			'servicio_id' => 'Servicio',
			'activa' => 'Activa',
			'de_confianza' => 'Estado de Confianza',
			'calificacion_actual_usuarios' => 'Calificacion Actual Usuarios',
			'calificacion_actual_ministerio' => 'Calificacion Actual Ministerio',
			'checksum' => 'Checksum',
			'direccion_ip_ini' => 'Direccion Ip Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'direccion_ip_act' => 'Direccion Ip Act',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'direccion_ip_elim' => 'Direccion Ip Elim',
			'usuario_elim_id' => 'Usuario Elim',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(strlen($this->rif)>0) $criteria->compare('rif',strtoupper($this->rif),true);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',strtoupper($this->nombre),true);
		if(is_numeric($this->estado_id)) $criteria->compare('estado_id',$this->estado_id);
		if(is_numeric($this->municipio_id)) $criteria->compare('municipio_id',$this->municipio_id);
		if(is_numeric($this->parroquia_id)) $criteria->compare('parroquia_id',$this->parroquia_id);
		if(strlen($this->direccion)>0) $criteria->compare('direccion',$this->direccion,true);
		if(strlen($this->nombre_contacto)>0) $criteria->compare('nombre_contacto',strtoupper($this->nombre_contacto),true);
		if(strlen($this->telefono_principal)>0) $criteria->compare('telefono_principal',$this->telefono_principal,true);
		if(strlen($this->telefono_opcional)>0) $criteria->compare('telefono_opcional',$this->telefono_opcional,true);
		if(strlen($this->email)>0) $criteria->compare('email',strtolower($this->email),true);
		if(strlen($this->monto_promedio_consulta)>0) $criteria->compare('monto_promedio_consulta',$this->monto_promedio_consulta,true);
		if(is_numeric($this->servicio_id)) $criteria->compare('servicio_id',$this->servicio_id);
		if(strlen($this->activa)>0) $criteria->compare('activa',$this->activa,true);
		if(strlen($this->de_confianza)>0) $criteria->compare('de_confianza',$this->de_confianza,true);
		if(is_numeric($this->calificacion_actual_usuarios)) $criteria->compare('calificacion_actual_usuarios',$this->calificacion_actual_usuarios);
		if(is_numeric($this->calificacion_actual_ministerio)) $criteria->compare('calificacion_actual_ministerio',$this->calificacion_actual_ministerio);
		if(strlen($this->checksum)>0) $criteria->compare('checksum',$this->checksum,true);
		if(strlen($this->direccion_ip_ini)>0) $criteria->compare('direccion_ip_ini',$this->direccion_ip_ini,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(strlen($this->direccion_ip_act)>0) $criteria->compare('direccion_ip_act',$this->direccion_ip_act,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(strlen($this->direccion_ip_elim)>0) $criteria->compare('direccion_ip_elim',$this->direccion_ip_elim,true);
		if(is_numeric($this->usuario_elim_id)) $criteria->compare('usuario_elim_id',$this->usuario_elim_id);
		if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CentroSalud the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
