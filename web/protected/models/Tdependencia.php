<?php

/**
 * This is the model class for table "tdependencia".
 *
 * The followings are the available columns in table 'tdependencia':
 * @property string $cdependencia
 * @property string $ddependencia
 * @property string $centidad
 * @property string $cmunicipio
 * @property string $iplanta
 * @property string $izona
 * @property string $inocturno
 * @property string $inivel
 * @property string $igeografica
 * @property string $irural
 * @property string $imarginal
 * @property string $ifrontera
 * @property string $iinsular
 * @property string $iindigena
 * @property string $ibolivariana
 * @property string $irobinsoniana
 * @property integer $tmarca
 * @property string $cempresa
 * @property string $accion_espec
 * @property string $unidad_ejec
 * @property string $cprograma
 * @property string $csub_programa
 * @property string $cactividad
 * @property string $ccentro_pago
 * @property string $cusuario
 * @property string $fsistema
 * @property string $cusu_modi
 * @property string $fmodifi
 */
class Tdependencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tdependencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tmarca', 'numerical', 'integerOnly'=>true),
			array('cdependencia, accion_espec', 'length', 'max'=>9),
			array('ddependencia', 'length', 'max'=>50),
			array('centidad, cmunicipio, inivel, cempresa, cprograma, csub_programa, cactividad', 'length', 'max'=>2),
			array('iplanta, izona, ccentro_pago', 'length', 'max'=>3),
			array('inocturno, igeografica, irural, imarginal, ifrontera, iinsular, iindigena, ibolivariana, irobinsoniana', 'length', 'max'=>1),
			array('unidad_ejec', 'length', 'max'=>5),
			array('cusuario, cusu_modi', 'length', 'max'=>8),
			array('fsistema, fmodifi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cdependencia, ddependencia, centidad, cmunicipio, iplanta, izona, inocturno, inivel, igeografica, irural, imarginal, ifrontera, iinsular, iindigena, ibolivariana, irobinsoniana, tmarca, cempresa, accion_espec, unidad_ejec, cprograma, csub_programa, cactividad, ccentro_pago, cusuario, fsistema, cusu_modi, fmodifi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cdependencia' => 'Cdependencia',
			'ddependencia' => 'Ddependencia',
			'centidad' => 'Centidad',
			'cmunicipio' => 'Cmunicipio',
			'iplanta' => 'Iplanta',
			'izona' => 'Izona',
			'inocturno' => 'Inocturno',
			'inivel' => 'Inivel',
			'igeografica' => 'Igeografica',
			'irural' => 'Irural',
			'imarginal' => 'Imarginal',
			'ifrontera' => 'Ifrontera',
			'iinsular' => 'Iinsular',
			'iindigena' => 'Iindigena',
			'ibolivariana' => 'Ibolivariana',
			'irobinsoniana' => 'Irobinsoniana',
			'tmarca' => 'Tmarca',
			'cempresa' => 'Cempresa',
			'accion_espec' => 'Accion Espec',
			'unidad_ejec' => 'Unidad Ejec',
			'cprograma' => 'Cprograma',
			'csub_programa' => 'Csub Programa',
			'cactividad' => 'Cactividad',
			'ccentro_pago' => 'Ccentro Pago',
			'cusuario' => 'Cusuario',
			'fsistema' => 'Fsistema',
			'cusu_modi' => 'Cusu Modi',
			'fmodifi' => 'Fmodifi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cdependencia',$this->cdependencia,true);
		$criteria->compare('ddependencia',$this->ddependencia,true);
		$criteria->compare('centidad',$this->centidad,true);
		$criteria->compare('cmunicipio',$this->cmunicipio,true);
		$criteria->compare('iplanta',$this->iplanta,true);
		$criteria->compare('izona',$this->izona,true);
		$criteria->compare('inocturno',$this->inocturno,true);
		$criteria->compare('inivel',$this->inivel,true);
		$criteria->compare('igeografica',$this->igeografica,true);
		$criteria->compare('irural',$this->irural,true);
		$criteria->compare('imarginal',$this->imarginal,true);
		$criteria->compare('ifrontera',$this->ifrontera,true);
		$criteria->compare('iinsular',$this->iinsular,true);
		$criteria->compare('iindigena',$this->iindigena,true);
		$criteria->compare('ibolivariana',$this->ibolivariana,true);
		$criteria->compare('irobinsoniana',$this->irobinsoniana,true);
		$criteria->compare('tmarca',$this->tmarca);
		$criteria->compare('cempresa',$this->cempresa,true);
		$criteria->compare('accion_espec',$this->accion_espec,true);
		$criteria->compare('unidad_ejec',$this->unidad_ejec,true);
		$criteria->compare('cprograma',$this->cprograma,true);
		$criteria->compare('csub_programa',$this->csub_programa,true);
		$criteria->compare('cactividad',$this->cactividad,true);
		$criteria->compare('ccentro_pago',$this->ccentro_pago,true);
		$criteria->compare('cusuario',$this->cusuario,true);
		$criteria->compare('fsistema',$this->fsistema,true);
		$criteria->compare('cusu_modi',$this->cusu_modi,true);
		$criteria->compare('fmodifi',$this->fmodifi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->drh;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tdependencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
