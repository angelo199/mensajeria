<?php

/**
 * This is the model class for table "baremo.entrevista_postulacion".
 *
 * The followings are the available columns in table 'baremo.entrevista_postulacion':
 * @property integer $id
 * @property integer $tipo_periodo_id
 * @property string $postulacion_id
 * @property string $talento_humano_id
 * @property integer $categoria_cargo_id
 * @property integer $estado_id
 * @property string $origen_evaluador
 * @property integer $cedula_evaluador
 * @property string $nombre_apellido_evaluador
 * @property string $cargo_evaluador
 * @property string $fecha_entrevista
 * @property string $origen_talento_humano
 * @property integer $cedula_talento_humano
 * @property string $nombre_apellido_talento_humano
 * @property string $fecha_nacimiento
 * @property string $cargo_postulado
 * @property integer $codigo_cargo_postulado
 * @property string $dependencia_talento_humano
 * @property integer $codigo_dependencia_talento_humano
 * @property integer $puntuacion_total_obtenida
 * @property string $estatus
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property integer $finalizada
 * @property string $codigo_entrevista
 * @property integer $puntuacion_req_minimos
 * @property integer $puntuacion_entrevista
 * @property string $origen_analista
 * @property integer $cedula_analista
 * @property string $nombre_apellido_analista
 * @property string $cargo_analista
 * @property string $fecha_evaluacion_req_min
 * @property string codigo_constancia_req_minimos
 *
 * The followings are the available model relations:
 * @property Postulacion $postulacion
 * @property TalentoHumano $talentoHumano
 * @property TipoPeriodoProceso $tipoPeriodo
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property EntrevistaPonderacion[] $entrevistaPonderacions
 */
class EntrevistaPostulacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'baremo.entrevista_postulacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_periodo_id, postulacion_id, talento_humano_id, estado_id, origen_talento_humano, cedula_talento_humano, nombre_apellido_talento_humano, fecha_nacimiento, cargo_postulado, codigo_cargo_postulado, fecha_ini, fecha_act', 'required'),
			array('tipo_periodo_id, categoria_cargo_id, estado_id, cedula_evaluador, cedula_talento_humano, codigo_cargo_postulado, codigo_dependencia_talento_humano, puntuacion_total_obtenida, usuario_ini_id, usuario_act_id, finalizada, puntuacion_req_minimos, puntuacion_entrevista', 'numerical', 'integerOnly'=>true),
			array('origen_evaluador, origen_talento_humano, estatus, origen_analista', 'length', 'max'=>1),
			array('nombre_apellido_evaluador, nombre_apellido_talento_humano, nombre_apellido_analista', 'length', 'max'=>150),
			array('cargo_evaluador, cargo_postulado, codigo_entrevista, cargo_analista', 'length', 'max'=>50),
			array('dependencia_talento_humano', 'length', 'max'=>100),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo_periodo_id, postulacion_id, talento_humano_id, categoria_cargo_id, estado_id, origen_evaluador, cedula_evaluador, nombre_apellido_evaluador, cargo_evaluador, fecha_entrevista, origen_talento_humano, cedula_talento_humano, nombre_apellido_talento_humano, fecha_nacimiento, cargo_postulado, codigo_cargo_postulado, dependencia_talento_humano, codigo_dependencia_talento_humano, puntuacion_total_obtenida, estatus, fecha_ini, fecha_act, usuario_ini_id, usuario_act_id, finalizada, codigo_entrevista, puntuacion_req_minimos, puntuacion_entrevista', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'postulacion' => array(self::BELONGS_TO, 'Postulacion', 'postulacion_id'),
			'talentoHumano' => array(self::BELONGS_TO, 'TalentoHumano', 'talento_humano_id'),
			'tipoPeriodo' => array(self::BELONGS_TO, 'TipoPeriodoProceso', 'tipo_periodo_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'entrevistaPonderacions' => array(self::HAS_MANY, 'EntrevistaPonderacion', 'entrevista_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo_periodo_id' => 'Tipo Periodo',
			'postulacion_id' => 'Postulacion',
			'talento_humano_id' => 'Talento Humano',
			'categoria_cargo_id' => 'Categoria Cargo',
			'estado_id' => 'Estado',
			'origen_evaluador' => 'Origen Evaluador',
			'cedula_evaluador' => 'Cedula Evaluador',
			'nombre_apellido_evaluador' => 'Nombre Apellido Evaluador',
			'cargo_evaluador' => 'Cargo Evaluador',
			'fecha_entrevista' => 'Fecha Entrevista',
			'origen_talento_humano' => 'Origen Talento Humano',
			'cedula_talento_humano' => 'Cedula Talento Humano',
			'nombre_apellido_talento_humano' => 'Nombre Apellido Talento Humano',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'cargo_postulado' => 'Cargo Postulado',
			'codigo_cargo_postulado' => 'Codigo Cargo Postulado',
			'dependencia_talento_humano' => 'Dependencia Talento Humano',
			'codigo_dependencia_talento_humano' => 'Codigo Dependencia Talento Humano',
			'puntuacion_total_obtenida' => 'puntuacion_total_obtenida = puntuacion_req_minimos + puntuacion_entrevista',
			'estatus' => 'Estatus',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'finalizada' => 'Este campo permite determinar si la entrevista ya fue finalizada.
0.- No ha iniciado. 1.- Evaluación de Requerimientos Mínimos Realizada. 2.- Entrevista Realizada.',
			'codigo_entrevista' => 'Codigo Entrevista',
			'puntuacion_req_minimos' => 'Puntuacion de Requerimientos Mínimos',
			'puntuacion_entrevista' => 'Puntuacion de la Entrevista',
			'origen_analista' => 'Origen del Analista',
			'cedula_analista' => 'Cedula del Analista',
			'nombre_apellido_analista' => 'Nombre y Apellido del Analista',
			'cargo_analista' => 'Cargo del Analista',
			'fecha_evaluacion_req_min' => 'Fecha de Evaluacion de Requerimientos Mínimos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(is_numeric($this->tipo_periodo_id)) $criteria->compare('tipo_periodo_id',$this->tipo_periodo_id);
		if(strlen($this->postulacion_id)>0) $criteria->compare('postulacion_id',$this->postulacion_id,true);
		if(strlen($this->talento_humano_id)>0) $criteria->compare('talento_humano_id',$this->talento_humano_id,true);
		if(is_numeric($this->categoria_cargo_id)) $criteria->compare('categoria_cargo_id',$this->categoria_cargo_id);
		if(is_numeric($this->estado_id)) $criteria->compare('estado_id',$this->estado_id);
		if(strlen($this->origen_evaluador)>0) $criteria->compare('origen_evaluador',$this->origen_evaluador,true);
		if(is_numeric($this->cedula_evaluador)) $criteria->compare('cedula_evaluador',$this->cedula_evaluador);
		if(strlen($this->nombre_apellido_evaluador)>0) $criteria->compare('nombre_apellido_evaluador',$this->nombre_apellido_evaluador,true);
		if(strlen($this->cargo_evaluador)>0) $criteria->compare('cargo_evaluador',$this->cargo_evaluador,true);
		if(Utiles::isValidDate($this->fecha_entrevista, 'y-m-d')) $criteria->compare('fecha_entrevista',$this->fecha_entrevista);
		// if(strlen($this->fecha_entrevista)>0) $criteria->compare('fecha_entrevista',$this->fecha_entrevista,true);
		if(strlen($this->origen_talento_humano)>0) $criteria->compare('origen_talento_humano',$this->origen_talento_humano,true);
		if(is_numeric($this->cedula_talento_humano)) $criteria->compare('cedula_talento_humano',$this->cedula_talento_humano);
		if(strlen($this->nombre_apellido_talento_humano)>0) $criteria->compare('nombre_apellido_talento_humano',$this->nombre_apellido_talento_humano,true);
		if(Utiles::isValidDate($this->fecha_nacimiento, 'y-m-d')) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento);
		// if(strlen($this->fecha_nacimiento)>0) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		if(strlen($this->cargo_postulado)>0) $criteria->compare('cargo_postulado',$this->cargo_postulado,true);
		if(is_numeric($this->codigo_cargo_postulado)) $criteria->compare('codigo_cargo_postulado',$this->codigo_cargo_postulado);
		if(strlen($this->dependencia_talento_humano)>0) $criteria->compare('dependencia_talento_humano',$this->dependencia_talento_humano,true);
		if(is_numeric($this->codigo_dependencia_talento_humano)) $criteria->compare('codigo_dependencia_talento_humano',$this->codigo_dependencia_talento_humano);
		if(is_numeric($this->puntuacion_total_obtenida)) $criteria->compare('puntuacion_total_obtenida',$this->puntuacion_total_obtenida);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(is_numeric($this->finalizada)) $criteria->compare('finalizada',$this->finalizada);
		if(strlen($this->codigo_entrevista)>0) $criteria->compare('codigo_entrevista',$this->codigo_entrevista,true);
		if(is_numeric($this->puntuacion_req_minimos)) $criteria->compare('puntuacion_req_minimos',$this->puntuacion_req_minimos);
		if(is_numeric($this->puntuacion_entrevista)) $criteria->compare('puntuacion_entrevista',$this->puntuacion_entrevista);
		if(strlen($this->origen_analista)>0) $criteria->compare('origen_analista',$this->origen_analista,true);
		if(is_numeric($this->cedula_analista)) $criteria->compare('cedula_analista',$this->cedula_analista);
		if(strlen($this->nombre_apellido_analista)>0) $criteria->compare('nombre_apellido_analista',$this->nombre_apellido_analista,true);
		if(strlen($this->cargo_analista)>0) $criteria->compare('cargo_analista',$this->cargo_analista,true);
		if(Utiles::isValidDate($this->fecha_evaluacion_req_min, 'y-m-d')) $criteria->compare('fecha_evaluacion_req_min',$this->fecha_evaluacion_req_min);
		// if(strlen($this->fecha_evaluacion_req_min)>0) $criteria->compare('fecha_evaluacion_req_min',$this->fecha_evaluacion_req_min,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EntrevistaPostulacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
