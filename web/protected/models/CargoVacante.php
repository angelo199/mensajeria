<?php

/**
 * This is the model class for table "gestion_humana.cargo_vacante".
 *
 * The followings are the available columns in table 'gestion_humana.cargo_vacante':
 * @property integer $id
 * @property integer $estado_id
 * @property integer $dependencia_id
 * @property integer $clase_cargo_id
 * @property string $disponibilidad
 * @property string $descripcion
 * @property string $estatus
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property integer $codnomin
 * @property integer $coddenpen
 * @property string $nomdenpen
 * @property integer $codestad
 * @property string $nomestad
 * @property string $codclase
 * @property string $gradoesc
 * @property string $dencargo
 * @property string $fecha_act_nomina
 * @property string $observacion
 *
 * The followings are the available model relations:
 * @property Estado $estado
 * @property Dependencia $dependencia
 * @property ClaseCargo $claseCargo
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property Postulacion[] $postulacions
 */
class CargoVacante extends CActiveRecord {

    public $cacheCargoVacanteCodIndex = 'CV:USERID:{idUsuario}:{codnom}';
    public $cacheCargoVacanteIdIndex = 'CV:ID:{id}';
    
    public $dependenciaNombre = '';
    public $cantidad_postulados = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'gestion_humana.cargo_vacante';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fecha_ini, fecha_act, usuario_ini_id, usuario_act_id, fecha_act_nomina', 'required'),
            array('estado_id, dependencia_id, clase_cargo_id, usuario_ini_id, usuario_act_id, codnomin, coddenpen, codestad', 'numerical', 'integerOnly'=>true),
            array('disponibilidad, estatus', 'length', 'max'=>1),
            array('descripcion', 'length', 'max'=>200),
            array('nomdenpen, nomestad, dencargo', 'length', 'max'=>80),
            array('codclase', 'length', 'max'=>5),
            array('gradoesc', 'length', 'max'=>2),
            array('observacion', 'length', 'max'=>255),
            array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
            array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
            array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, estado_id, dependencia_id, clase_cargo_id, disponibilidad, descripcion, estatus, fecha_ini, fecha_act, usuario_ini_id, usuario_act_id, codnomin, coddenpen, nomdenpen, codestad, nomestad, codclase, gradoesc, dencargo, fecha_act_nomina, observacion, dependenciaNombre', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
            'dependencia' => array(self::BELONGS_TO, 'Dependencia', 'dependencia_id'),
            'claseCargo' => array(self::BELONGS_TO, 'ClaseCargo', 'clase_cargo_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'postulacions' => array(self::HAS_MANY, 'Postulacion', 'cargo_postulado_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'estado_id' => 'Estado',
            'dependencia_id' => 'Dependencia',
            'clase_cargo_id' => 'Clase Cargo',
            'disponibilidad' => 'Disponibilidad',
            'descripcion' => 'Descripcion',
            'estatus' => 'Estatus',
            'fecha_ini' => 'Fecha Ini',
            'fecha_act' => 'Fecha Act',
            'usuario_ini_id' => 'Usuario Ini',
            'usuario_act_id' => 'Usuario Act',
            'codnomin' => 'Codnomin',
            'coddenpen' => 'Coddenpen',
            'nomdenpen' => 'Nomdenpen',
            'codestad' => 'Codestad',
            'nomestad' => 'Nomestad',
            'codclase' => 'Codclase',
            'gradoesc' => 'Gradoesc',
            'dencargo' => 'Dencargo',
            'fecha_act_nomina' => 'Fecha del momento que se actualizo la data desde nomina.',
            'observacion' => 'Campo que captura el estado de la carga en caso de que salga algun error. ',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */

    public function search($estadoId, $notAdmin, $aperturaPeriodoId, $tipoPeriodoId = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        //$criteria->select ='t.descripcion AS descripcion';
        
        $criteria->with = array(
            'estado' => array('alias'=>'es', 'select'=>'es.nombre'),
            'dependencia' => array('alias'=>'dp', 'select'=>'dp.nombre'),
            'claseCargo' => array('alias'=>'cc', 'select'=>'cc.nombre'),
        );

        if($tipoPeriodoId AND is_numeric($tipoPeriodoId) AND $aperturaPeriodoId AND is_numeric($aperturaPeriodoId)) {
            $criteria->compare('t.tipo_periodo_proceso_id', $tipoPeriodoId);
            $criteria->compare('t.apertura_periodo_id', $aperturaPeriodoId);
        }

        $criteria->select = "t.*, (SELECT COUNT(1) FROM gestion_humana.postulacion p WHERE p.cargo_postulado_id = t.id AND p.apertura_periodo_id = $aperturaPeriodoId AND p.estatus = 'A') AS cantidad_postulados";

        if($notAdmin) {
            if(is_numeric($estadoId)){
                $criteria->compare('t.estado_id', $estadoId);
            }
        } else {
            if(is_numeric($this->estado_id)) $criteria->compare('t.estado_id',$this->estado_id);
        }

        if(strlen($this->dependenciaNombre)>0) $criteria->compare('dp.nombre',$this->dependenciaNombre,true);
        if(is_numeric($this->id)) $criteria->compare('id',$this->id);
        if(is_numeric($this->dependencia_id)) $criteria->compare('t.dependencia_id',$this->dependencia_id);
        if(is_numeric($this->clase_cargo_id)) $criteria->compare('t.clase_cargo_id',$this->clase_cargo_id);
        if(strlen($this->disponibilidad)>0) $criteria->compare('disponibilidad',$this->disponibilidad,true);
        if(strlen($this->descripcion)>0) $criteria->compare('t.descripcion',$this->descripcion,true);
        if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('t.estatus',$this->estatus,true);
        if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
        if(is_numeric($this->codnomin)) $criteria->compare('codnomin::VARCHAR',$this->codnomin, true);
        if(is_numeric($this->coddenpen)) $criteria->compare('coddenpen',$this->coddenpen);
        if(strlen($this->nomdenpen)>0) $criteria->compare('nomdenpen',$this->nomdenpen,true);
        if(is_numeric($this->codestad)) $criteria->compare('codestad',$this->codestad);
        if(strlen($this->nomestad)>0) $criteria->compare('nomestad',$this->nomestad,true);
        if(strlen($this->codclase)>0) $criteria->compare('codclase',$this->codclase,true);
        if(strlen($this->gradoesc)>0) $criteria->compare('gradoesc',$this->gradoesc,true);
        if(strlen($this->dencargo)>0) $criteria->compare('dencargo',$this->dencargo,true);
        if(Utiles::isValidDate($this->fecha_act_nomina, 'y-m-d')) $criteria->compare('fecha_act_nomina',$this->fecha_act_nomina);
        // if(strlen($this->fecha_act_nomina)>0) $criteria->compare('fecha_act_nomina',$this->fecha_act_nomina,true);
        if(strlen($this->observacion)>0) $criteria->compare('observacion',$this->observacion,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

        
    public function datosCargoVacante($codnomin, $estadoId, $esAdmin, $usuarioId) {
        $cacheIndex = str_replace('{idUsuario}:{codnom}', $usuarioId.':'.$codnomin, $this->cacheCargoVacanteCodIndex);
        $result = Yii::app()->cache->get($cacheIndex);
            
        if(!$result){
            $condicionEstado = (!$esAdmin)?" AND estado_id = :estadoId ":'';
            $sql = "SELECT id, descripcion, nomestad, estado_id, dependencia_id, nomdenpen, dencargo, clase_cargo_id, tipo_dependencia_id, perfil FROM gestion_humana.cargo_vacante WHERE codnomin = :codnomin $condicionEstado limit 1";        
            $cargoVacante = $this->getDbConnection()->createCommand($sql);
            $cargoVacante->bindParam(':codnomin', $codnomin, PDO::PARAM_INT);
            (!$esAdmin)?$cargoVacante->bindParam(':estadoId', $estadoId, PDO::PARAM_INT):'';
            $result = $cargoVacante->queryRow();
            if($result){
                Yii::app()->cache->set($cacheIndex, $result, Helper::$SEGUNDOS_UN_MES);
            }
        }
        return $result;
    }

    public function beforeInsert()
    {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate()
    {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete(){
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate(){
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    public function __toString() {
        try {
            return (string) $this->id;
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }
        
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CargoVacante the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
