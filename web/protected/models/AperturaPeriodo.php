<?php

/**
 * This is the model class for table "gestion_humana.apertura_periodo".
 *
 * The followings are the available columns in table 'gestion_humana.apertura_periodo':
 * @property integer $id
 * @property integer $tipo_apertura
 * @property string $fecha_inicio
 * @property string $fecha_final
 * @property string $nombre_clave
 * @property string $estatus
 */
class AperturaPeriodo extends CActiveRecord
{
        const CONCURSOPUBLICO = 1;
        const SISTEMADEMERITO = 2;
        PUBLIC $cacheIndexPeriodo = 'AP:{tipoApertura}';
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gestion_humana.apertura_periodo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_apertura_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>2),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('nombre_clave', 'length','max'=>80),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fecha_inicio, fecha_final, nombre_clave, estatus', 'safe', 'on'=>'search'),
                        array('tipo_apertura_id, fecha_inicio, fecha_final, periodo_nombre','required'),
                        array('tipo_apertura_id','checkIfExists','on'=>'insert')
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'tipoPeriodoProceso' => array(self::BELONGS_TO, 'TipoPeriodoProceso', 'tipo_apertura_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo_apertura_id' => 'Tipo de Proceso',
			'fecha_inicio' => 'Fecha de Apertura de Proceso',
			'fecha_final' => 'Fecha de Cierre de Apertura de Proceso',
			'nombre_clave' => 'Nombre Clave',
			'estatus' => 'Estado de Apertura de Proceso',
                        'periodo_nombre' => 'Descripción del Proceso',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(is_numeric($this->tipo_apertura_id)) $criteria->compare('tipo_apertura_id',$this->tipo_apertura_id);
		if(Utiles::isValidDate($this->fecha_inicio, 'y-m-d')) $criteria->compare('fecha_inicio',$this->fecha_inicio);
		// if(strlen($this->fecha_inicio)>0) $criteria->compare('fecha_inicio',$this->fecha_inicio,true);
		if(Utiles::isValidDate($this->fecha_final, 'y-m-d')) $criteria->compare('fecha_final',$this->fecha_final);
		// if(strlen($this->fecha_final)>0) $criteria->compare('fecha_final',$this->fecha_final,true);
                if(strlen($this->periodo_nombre)>0) $criteria->addSearchCondition('periodo_nombre', '%' . addslashes($this->periodo_nombre). '%', false, 'AND', 'ILIKE');
		if(strlen($this->nombre_clave)>0) $criteria->addSearchCondition('nombre_clave', '%' . addslashes($this->nombre_clave). '%', false, 'AND', 'ILIKE');
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
                $criteria->order = 't.estatus ASC';

		return new CActiveDataProvider($this, array('criteria'=>$criteria,));                
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';                                    
            $this->fecha_inicio = Helper::toServerDate($this->fecha_inicio);
            $this->fecha_final = Helper::toServerDate($this->fecha_final);
            $mesano = date("m-Y",strtotime($this->fecha_inicio));
            $this->nombre_clave = ($this->tipo_apertura_id==1)?'Concurso Publico '.$mesano:'Sistema de Mérito '.$mesano;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            
            $this->fecha_inicio = Helper::toServerDate($this->fecha_inicio);
            $this->fecha_final = Helper::toServerDate($this->fecha_final);            
            $mesano = date("m-Y",strtotime($this->fecha_inicio));
            $this->nombre_clave = $this->tipo_apertura_id==0?'Concurso Publico '.$mesano:'Sistema de Mérito '.$mesano;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function getPeriodoActual($tipoApertura) {
            
            $cacheIndexPeriodo = str_replace('{tipoApertura}',$tipoApertura,$this->cacheIndexPeriodo);
            $result = Yii::app()->cache->get($cacheIndexPeriodo);
            if(!$result){
                $fechaActual = date("Y-m-d");
                if($tipoApertura == 'merito') {
                    $tipoAperturaId = self::SISTEMADEMERITO;
                } else if($tipoApertura == 'concurso') {
                    $tipoAperturaId = self::CONCURSOPUBLICO;
                }

                $sql= "SELECT id FROM gestion_humana.apertura_periodo WHERE estatus = 'A' AND tipo_apertura_id = :tipoAperturaId AND fecha_inicio <= :fechaActual AND fecha_final >= :fechaActual;";

                $postulacion = $this->getDbConnection()->createCommand($sql);
                $postulacion->bindParam(':fechaActual', $fechaActual, PDO::PARAM_STR);
                $postulacion->bindParam(':tipoAperturaId', $tipoAperturaId, PDO::PARAM_INT);
                $result = $postulacion->queryScalar();
                if($result){
                    Yii::app()->cache->set($cacheIndexPeriodo,$result, Helper::$SEGUNDOS_UN_MES);
                }
            }
            
            return $result;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AperturaPeriodo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function verificarPrueba($estatus) {
        $sql = "SELECT DISTINCT p.id, p.nombre_clave as nombre
                   FROM gestion_humana.apertura_periodo p
                   INNER JOIN gprueba.seccion_prueba sp ON (sp.prueba_id = p.id)
                   INNER JOIN gprueba.pregunta p_ ON (p_.seccion_prueba_id = sp.id)
                   INNER JOIN gprueba.respuesta r ON (r.pregunta_id = p_.id)
                   WHERE p.estatus = :estatus
                   AND sp.estatus = :estatus
                   AND p_.estatus = :estatus
                   AND r.estatus = :estatus
                   GROUP BY p.id, p.nombre_clave
                   ORDER BY p.nombre_clave";
        $guardar = $this->getDbConnection()->createCommand($sql);
        $guardar->bindParam(':estatus', $estatus, PDO::PARAM_STR);
        $result = $guardar->queryAll();

        return $result;
        }

        public function pruebaActivas() {
            $fecha = date('Y') . '-01-01 00:00:00';
            $sql = "SELECT DISTINCT p.nombre_clave AS nombre_prueba, p.id AS prueba_id
                                FROM gprueba.aplicacion_prueba ap, gestion_humana.apertura_periodo p
                                INNER JOIN gprueba.estatus_iniciar_prueba ep ON (ep.prueba_id = p.id)
                WHERE p.id NOT IN (SELECT DISTINCT prueba_id from gprueba.aplicacion_prueba
                                    WHERE substring(fecha_ini::character varying,0,5)=substring(now()::character varying,0,5))
                                AND ep.estatus='A'
                                AND substring(ap.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":fecha", $fecha, PDO::PARAM_STR);
            $resultado = $consulta->queryAll();
            return $resultado;
            // $this->verificarAplicacionPrueba($resultado);
        }

        public function checkIfExists($attribute,$param = null){
            
            $exist = $this->findAll(array('select'=>'id','limit'=>'1','params'=>array(':tipo_apertura_id'=>$this->tipo_apertura_id),'condition' => "estatus = 'A' AND tipo_apertura_id = :tipo_apertura_id"));
            if(count($exist)>0){
                $this->addError($attribute,'Ya existe un periodo abierto con este tipo de apertura');
            }
        }

        public function checkIfExistsActive($tipoAperturaId){
            
            $fechaActual = date("Y-m-d");
            $exist = $this->findAll(array('select'=>'id','limit'=>'1','params'=>array(':tipo_apertura_id'=>$tipoAperturaId, ':fechaActual'=>$fechaActual),'condition' => "estatus = 'A' AND tipo_apertura_id = :tipo_apertura_id AND fecha_inicio < :fechaActual AND fecha_final >= :fechaActual"));            
            return $exist;
        }
        
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return AperturaPeriodo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        if (is_numeric($id)) {
            $cacheIndex = 'PERIODO:APERT:'.$id;
            $model = Yii::app()->cache->get($cacheIndex);
            if(!$model){
                $model = $this->findByPk($id);
                if($model){
                    Yii::app()->cache->set($cacheIndex, $model, Helper::$SEGUNDOS_DOS_SEMANAS);
                }
            }
            return $model;
        } else {
            return null;
        }
    }
}
