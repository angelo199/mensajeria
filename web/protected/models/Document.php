<?php

/**
 * This is the model class for table "public.document2".
 *
 * The followings are the available columns in table 'public.document2':
 * @property string $acceso
 * @property string $ext_acceso
 * @property string $jerarquia
 * @property string $ubicacion
 * @property string $tipo_liter
 * @property string $nivel_bibl
 * @property string $nivel_reg
 * @property string $tipo_ref_analitica
 * @property string $paginas
 * @property string $volumen
 * @property string $volumen_final
 * @property string $numero
 * @property string $numero_final
 * @property string $numeracion
 * @property string $enum_nivel_ini_3
 * @property string $enum_nivel_ini_4
 * @property string $enum_nivel_fin_3
 * @property string $enum_nivel_fin_4
 * @property string $cod_editor
 * @property string $cod_editor_inst
 * @property string $edicion
 * @property string $fecha_iso
 * @property string $fecha_iso_final
 * @property string $fecha_pub
 * @property string $isbn
 * @property string $indice_suplemento
 * @property string $cod_conf
 * @property string $cod_proy
 * @property string $disemina
 * @property string $url
 * @property string $n_disk_cin
 * @property string $sala
 * @property string $impresion
 * @property string $orden
 * @property string $cerrado
 * @property string $hora_iso_inicio
 * @property string $hora_iso_final
 *
 * The followings are the available model relations:
 * @property Central $acceso0
 * @property Central $extAcceso
 * @property Conferen $codConf
 * @property Editor $codEditor
 * @property EditorInst $codEditorInst
 * @property Proyecto $codProy
 * @property Tipodoc $tipoLiter
 */
class Document extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'public.document2';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('acceso, ext_acceso, jerarquia', 'required'),
            array('acceso', 'length', 'max' => 13),
            array('ext_acceso, n_disk_cin, orden', 'length', 'max' => 4),
            array('jerarquia, volumen, volumen_final, numero, numero_final, enum_nivel_ini_3, enum_nivel_ini_4, enum_nivel_fin_3, enum_nivel_fin_4, fecha_iso, fecha_iso_final, hora_iso_inicio, hora_iso_final', 'length', 'max' => 8),
            array('ubicacion, paginas, disemina', 'length', 'max' => 50),
            array('tipo_liter, nivel_reg', 'length', 'max' => 3),
            array('nivel_bibl, cerrado', 'length', 'max' => 1),
            array('tipo_ref_analitica', 'length', 'max' => 2),
            array('numeracion', 'length', 'max' => 64),
            array('cod_editor, cod_editor_inst, cod_conf, cod_proy', 'length', 'max' => 6),
            array('edicion', 'length', 'max' => 100),
            array('fecha_pub', 'length', 'max' => 80),
            array('isbn', 'length', 'max' => 20),
            array('indice_suplemento', 'length', 'max' => 32),
            array('url', 'length', 'max' => 255),
            array('sala', 'length', 'max' => 35),
            array('impresion', 'length', 'max' => 25),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('acceso, ext_acceso, jerarquia, ubicacion, tipo_liter, nivel_bibl, nivel_reg, tipo_ref_analitica, paginas, volumen, volumen_final, numero, numero_final, numeracion, enum_nivel_ini_3, enum_nivel_ini_4, enum_nivel_fin_3, enum_nivel_fin_4, cod_editor, cod_editor_inst, edicion, fecha_iso, fecha_iso_final, fecha_pub, isbn, indice_suplemento, cod_conf, cod_proy, disemina, url, n_disk_cin, sala, impresion, orden, cerrado, hora_iso_inicio, hora_iso_final', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'acceso0' => array(self::BELONGS_TO, 'Central', 'acceso'),
            'extAcceso' => array(self::BELONGS_TO, 'Central', 'ext_acceso'),
            'codConf' => array(self::BELONGS_TO, 'Conferen', 'cod_conf'),
            'codEditor' => array(self::BELONGS_TO, 'Editor', 'cod_editor'),
            'codEditorInst' => array(self::BELONGS_TO, 'EditorInst', 'cod_editor_inst'),
            'codProy' => array(self::BELONGS_TO, 'Proyecto', 'cod_proy'),
            'tipoLiter' => array(self::BELONGS_TO, 'Tipodoc', 'tipo_liter'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'acceso' => 'Acceso',
            'ext_acceso' => 'Ext Acceso',
            'jerarquia' => 'Jerarquia',
            'ubicacion' => 'Ubicacion',
            'tipo_liter' => 'Tipo Liter',
            'nivel_bibl' => 'Nivel Bibl',
            'nivel_reg' => 'Nivel Reg',
            'tipo_ref_analitica' => 'Tipo Ref Analitica',
            'paginas' => 'Paginas',
            'volumen' => 'Volumen',
            'volumen_final' => 'Volumen Final',
            'numero' => 'Numero',
            'numero_final' => 'Numero Final',
            'numeracion' => 'Numeracion',
            'enum_nivel_ini_3' => 'Enum Nivel Ini 3',
            'enum_nivel_ini_4' => 'Enum Nivel Ini 4',
            'enum_nivel_fin_3' => 'Enum Nivel Fin 3',
            'enum_nivel_fin_4' => 'Enum Nivel Fin 4',
            'cod_editor' => 'Cod Editor',
            'cod_editor_inst' => 'Cod Editor Inst',
            'edicion' => 'Edicion',
            'fecha_iso' => 'Fecha Iso',
            'fecha_iso_final' => 'Fecha Iso Final',
            'fecha_pub' => 'Fecha Pub',
            'isbn' => 'Isbn',
            'indice_suplemento' => 'Indice Suplemento',
            'cod_conf' => 'Cod Conf',
            'cod_proy' => 'Cod Proy',
            'disemina' => 'Disemina',
            'url' => 'Url',
            'n_disk_cin' => 'N Disk Cin',
            'sala' => 'Sala',
            'impresion' => 'Impresion',
            'orden' => 'Orden',
            'cerrado' => 'Cerrado',
            'hora_iso_inicio' => 'Hora Iso Inicio',
            'hora_iso_final' => 'Hora Iso Final',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;

        $criteria->compare('acceso', $this->acceso, true);
        $criteria->compare('ext_acceso', $this->ext_acceso, true);
        $criteria->compare('jerarquia', $this->jerarquia, true);
        $criteria->compare('ubicacion', $this->ubicacion, true);
        $criteria->compare('tipo_liter', $this->tipo_liter, true);
        $criteria->compare('nivel_bibl', $this->nivel_bibl, true);
        $criteria->compare('nivel_reg', $this->nivel_reg, true);
        $criteria->compare('tipo_ref_analitica', $this->tipo_ref_analitica, true);
        $criteria->compare('paginas', $this->paginas, true);
        $criteria->compare('volumen', $this->volumen, true);
        $criteria->compare('volumen_final', $this->volumen_final, true);
        $criteria->compare('numero', $this->numero, true);
        $criteria->compare('numero_final', $this->numero_final, true);
        $criteria->compare('numeracion', $this->numeracion, true);
        $criteria->compare('enum_nivel_ini_3', $this->enum_nivel_ini_3, true);
        $criteria->compare('enum_nivel_ini_4', $this->enum_nivel_ini_4, true);
        $criteria->compare('enum_nivel_fin_3', $this->enum_nivel_fin_3, true);
        $criteria->compare('enum_nivel_fin_4', $this->enum_nivel_fin_4, true);
        $criteria->compare('cod_editor', $this->cod_editor, true);
        $criteria->compare('cod_editor_inst', $this->cod_editor_inst, true);
        $criteria->compare('edicion', $this->edicion, true);
        $criteria->compare('fecha_iso', $this->fecha_iso, true);
        $criteria->compare('fecha_iso_final', $this->fecha_iso_final, true);
        $criteria->compare('fecha_pub', $this->fecha_pub, true);
        $criteria->compare('isbn', $this->isbn, true);
        $criteria->compare('indice_suplemento', $this->indice_suplemento, true);
        $criteria->compare('cod_conf', $this->cod_conf, true);
        $criteria->compare('cod_proy', $this->cod_proy, true);
        $criteria->compare('disemina', $this->disemina, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('n_disk_cin', $this->n_disk_cin, true);
        $criteria->compare('sala', $this->sala, true);
        $criteria->compare('impresion', $this->impresion, true);
        $criteria->compare('orden', $this->orden, true);
        $criteria->compare('cerrado', $this->cerrado, true);
        $criteria->compare('hora_iso_inicio', $this->hora_iso_inicio, true);
        $criteria->compare('hora_iso_final', $this->hora_iso_final, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Document the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function busquedaExpediente($cedula) {
        $resultado = '';
        $sql = "SELECT * FROM (select * from (select row_number() OVER(order by c.numero) as Nro, descrip_desc as Etiqueta, descriptor_salida as Valor from coddesc c,descript d, tipodesc t, document2 a
                            where
                            a.ubicacion=:ubicacion and a.ext_acceso = 0 and a.acceso = c.acceso and
                            c.cod_desc = d.codesc and
                            t.tipo_desc = d.tipo
                             ) uno

                            union
                            select * from (select 98 as Nro, 'Fecha Nacimiento' as Etiqueta, a.fecha_iso_final as Valor
                            from document2 a
                            where
                            a.ubicacion=:ubicacion and a.ext_acceso = 0 ) dos

                            union
                            select * from (select 99 as Nro, 'Fecha de Ingreso' as Etiqueta, a.fecha_iso as Valor
                            from document2 a
                            where
                            a.ubicacion=:ubicacion and a.ext_acceso = 0 ) tres

                            union
                            select * from (select 0 as Nro, 'Cédula de identidad' as Etiqueta, a.ubicacion as Valor
                            from document2 a
                            where
                            a.ubicacion=:ubicacion and a.ext_acceso = 0 ) cuatro

                            union
                             select * from (SELECT 0 as nro, 'Nombres y Apellidos' as Etiqueta, t.titulo_salida as Valor
                             FROM codtit c
                              inner join titulos t on t.cod_titulo=c.cod_titulo and c.ext_acceso = 0
                              inner join document2 d on c.acceso=d.acceso where d.ubicacion=:ubicacion and d.ext_acceso = 0) cinco

                            ) total
                            order by Nro"
        ;
        $query = Yii::app()->db->createCommand($sql);
        $query->bindParam(':ubicacion', $cedula, PDO::PARAM_STR);
        //echo "<pre> $sql </pre>";
        $resultado = $query->queryAll();
        //var_dump($resultado); die();
        return $resultado;
    }

    public function busquedaAvanzadaCatia($cedula) {
        $sql = "select *from expediente.empleado where cedula=:cedula  limit 1";
        $consultar = Yii::app()->db->createCommand($sql);
        $consultar->bindParam(':cedula', $cedula, PDO::PARAM_STR);
        $resultado = $consultar->queryRow();
        return $resultado;
    }

    public function busquedaAvanzada($cedula, $nombre, $estado) {
        $sql = "select cedula,nombres_y_apellidos,expediente from expediente.empleado where nombres_y_apellidos=:nombre or cedula=:cedula ";
        $consultar = Yii::app()->db->createCommand($sql);
        $consultar->bindParam(':cedula', $cedula, PDO::PARAM_STR);
        $consultar->bindParam(':nombre', $nombre, PDO::PARAM_STR);
        $consultar->bindParam(':estado', $estado, PDO::PARAM_STR);
        $resultado = $consultar->queryRow();
        return $resultado;
    }

    public function foto($cedula) {
        $sql = "SELECT archivo  as url, archivo,p.estatus_foto,p.id
                        FROM hipertextos h inner join document2 p on h.acceso = p.acceso  where p.ubicacion=:ubicacion and h.ext_acceso = 0";
        //var_dump($sql);die();
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(':ubicacion', $cedula, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();

        return $resultado;
    }

    public function busquedaHiperTexto($cedula) {
        $sql = "SELECT '<a target=_doc href=\"http://172.16.3.114' || archivo || '\" > ' || descripcion || '</a>' as url, h.ext_acceso,archivo,h.id,estatus,h.fecha_documento
  FROM hipertextos h, document2 d where h.acceso = d.acceso and d.ext_acceso = 0 and d.ubicacion = '" . $cedula . "' and h.ext_acceso <>0 and h.estatus='A' order by h.ext_acceso asc";

        $query = Yii::app()->db->createCommand($sql);
        $query->bindParam(':ubicacion', $cedula, PDO::PARAM_STR);
        $resultado = $query->queryAll();
        //var_dump($resultado); die();
        return $resultado;
    }

    public function BusquedaCatia($cedula,$nacionalidad) {
        $resultado = '';
        $resultado = $this->tpersona($cedula);

        if (!empty($resultado)) {
            return $resultado;
        } else if (empty($resultado) || $resultado == '' || $resultado == NULL) {
            $resultado = $this->busquedaTegresado($cedula);
        }
        if (!empty($resultado)) {
            return $resultado;
        } else if (empty($resultado) || $resultado == '' || $resultado == NULL) {
            $sql = "select t.estado_de_nacimiento as centidad, split_part(t.nombres_y_apellidos,' ',1) as nombre, split_part(t.nombres_y_apellidos,' ',2) as apellido, t.tipo_de_personal as tipo_de_persona,
CASE WHEN fecha_de_nacimiento is not null and length(fecha_de_nacimiento)=10
   then fecha_de_nacimiento::date
   END AS fecha_nacimiento,
   CASE WHEN t.fecha_de_ingreso is not null and length(fecha_de_ingreso)=10
   then t.fecha_de_ingreso::date
   END AS fdesde,
   t.estado_de_nacimiento
from expediente.empleado_catia t where t.cedula=:cedula limit 1";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
            $resultado = $consulta->queryRow();
        }
        if (!empty($resultado)) {
            return $resultado;
        } elseif (empty($resultado) || $resultado == '' || $resultado == NULL) {
            $resultado = $this->tpersonaContratado($cedula);
             }
             if (!empty($resultado)) {
            return $resultado;

        }elseif (empty($resultado) || $resultado == '' || $resultado == NULL) {
            $resultado = $this->datosBasicos($cedula,$nacionalidad);
            return $resultado;
        }
        return $resultado;
    }

    public function busquedaTegresado($cedula) {
        $resultado = '';
        $sql = "select t.ccedula from tegresados t where t.ccedula=:cedula";
        $consulta = Yii::app()->drh->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        //$consulta->bindParam(":nac", $nac, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function datosBasicos($cedula,$nacionalidad) {
        $resultado = '';
        $sql = "select p.telefono_celular, p.nombre,p.apellido, p.estado, p.correo, p.sexo, p.fecha_ingreso, p.fecha_nacimiento, p.telefono_local, p.cargo,p.dependencia, p.direccion from ficha.personal p where p.cedula=:cedula and nacionalidad=:nacionalidad";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        $consulta->bindParam(":nacionalidad", $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function datosEstado($codigo) {
        $resultado = '';
        $sql = "select t.centidad, t.dentidad from tentidad t where t.centidad=:codigo";
        $consulta = Yii::app()->dbcontratado->createCommand($sql);
        $consulta->bindParam(":codigo", $codigo, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function tpersona($cedula) {
        $resultado = '';
        $sql = "select d.ddependencia,split_part(tt.dnombre, ' ', 1) AS apellido, split_part(tt.dnombre, ' ', 2) AS nombre, t.centidad,t.fdesde, t.ccedula,c.dcargo as tipo_de_persona from tpercardep t
                   left join tcargo c on t.ccargo=c.ccargo
                   left join tdependencia d on t.cdependencia=d.cdependencia 
                   left join tpersona tt on tt.ccedula=t.ccedula where t.ccedula=:cedula";
        $consulta = Yii::app()->drh->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        //$consulta->bindParam(":nac", $nac, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function tpersonaContratado($cedula) {
        $resultado = '';
        $sql = "select d.ddependencia, t.centidad,t.ffecha_efe as fdesde, t.ffecha_nac as fecha_nacimiento,  t.ccedula,c.dcargo as tipo_de_persona from tpersona t
               left join tcargo c on t.ccargo=c.ccargo
               left join tdependencia d on t.cdependencia=d.cdependencia where t.ccedula=:cedula";
        // var_dump($sql); die();
        $consulta = Yii::app()->dbcontratado->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        //$consulta->bindParam(":nac", $nac, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function saimeBuscar($cedula,$nacionalidad) {
        $resultado = '';
        $sql = "select s.primer_nombre, s.segundo_nombre, s.cedula, s.primer_apellido, s.segundo_apellido, s.fecha_nacimiento, s.origen, s.sexo from auditoria.saime s where s.cedula=:cedula and origen=:origen group by s.primer_nombre, s.segundo_nombre, s.cedula, s.primer_apellido, s.segundo_apellido, s.fecha_nacimiento,s.origen, s.sexo limit 1";
        $consulta = Yii::app()->db->createCommand($sql);
         $consulta->bindParam(":cedula", $cedula, PDO::PARAM_INT);
         $consulta->bindParam(":origen", $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function documento($cedula){
        $resultado='';
        $sql="SELECT  ubicacion
            FROM document2 where ubicacion=:cedula";
        $consulta = Yii::app()->db->createCommand($sql);
         $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
         $resultado=$consulta->queryRow();
         return $resultado;
    }

    public function saimeBuquedaAvanzada($cedula, $nombres, $apellidos) {
        $resultado = '';
        $sql = "select s.primer_nombre, s.segundo_nombre, s.cedula, s.primer_apellido, s.segundo_apellido, s.fecha_nacimiento, s.sexo from auditoria.saime s where s.cedula=:cedula or primer_nombre || segundo_nombre=:nombre or primer_apellido || segundo_apellido=:apellidos  group by s.primer_nombre, s.segundo_nombre, s.cedula, s.primer_apellido, s.segundo_apellido, s.fecha_nacimiento, s.sexo";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        $consulta->bindParam(":nombres", $nombres, PDO::PARAM_STR);
        $consulta->bindParam(":apellidos", $apellidos, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function fichaNueva($cedula,$nacionalidad) {
        $resultado = '';
        $sql = "select *from ficha.personal where cedula=:cedula and nacionalidad=:nacionalidad";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        $consulta->bindParam(":nacionalidad", $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function where_cedula($cedula, $nombres) {

    }

    public function fichaNuevaPersonal($cedula, $nombres, $estado) {
        $resultado = '';
        $where = '';
        if ($cedula != '') {
            $where = "where cedula='" . $cedula . "'";
        } elseif ($estado != '' and $nombres != '') {
            $where = "where estado_de_nacimiento ILIKE '$estado' and nombres_y_apellidos like '%$nombres%'";
        } elseif ($nombres != '') {
            $where = "where nombres_y_apellidos like '%$nombres%'";
        }

        $sql = "select * from expediente.empleado $where";
        var_dump($sql);
        die();
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function fichaNuevaPersonalAvan($cedula, $nombres, $estado) {
        $resultado = '';
        $where = '';
        if ($cedula != '') {
            $where = "where cedula='" . $cedula . "'";
        }
        if ($nombres != '') {
            $where = "where nombre || apellido like '%$nombres%'";
        }
        if ($estado != '' and $nombres != '') {
            $where = "where estado_id::text ILIKE '%$estado'";
        }
        $sql = "select *from ficha.personal $where";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function fichaEstado($centidad) {
        $resultado = '';
        $sql = "select f.dentidad,f.centidad from ficha.tentidad f where centidad=:centidad";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":centidad", $centidad, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function busquedaSaime($origen, $cedula) {
        $cedulaInt = $cedula;
        /* $sql = "select u_u.id, s.origen ,s.cedula, u_u.nombre, u_u.apellido "
          . " from seguridad.usergroups_user u_u "
          . " inner join auditoria.saime s on (s.origen = u_u.origen AND s.cedula = u_u.cedula)"
          . " where "
          . " u_u.cedula= :cedula AND "
          . " u_u.origen= :origen ";
         *
         */
        $sql = "SELECT cedula, (primer_nombre || ' ' || segundo_nombre) AS nombre, (primer_apellido || ' ' || segundo_apellido) AS apellido, fecha_nacimiento  "
                . " FROM auditoria.saime s"
                . " WHERE "
                . " s.cedula= :cedula AND "
                . " s.origen= :origen ";

        $buqueda = Yii::app()->db->createCommand($sql);
        $buqueda->bindParam(":cedula", $cedulaInt, PDO::PARAM_INT);
        $buqueda->bindParam(":origen", $origen, PDO::PARAM_INT);
        $resultadoCedula = $buqueda->queryRow();

        if ($resultadoCedula !== array()) {
            return $resultadoCedula;
        } else {
            return false;
        }
    }

    public function modificarFichaPersonal($fecha_personal, $id) {
        $resultado = '';
        $sql = "update ficha.documento_personal set fecha_documento=:fecha_personal where id=:id";
        //var_dump($sql); die();
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(':id', $id, PDO::PARAM_INT);
        $consulta->bindParam(':fecha_personal', date("Y-m-d", strtotime($fecha_personal)), PDO::PARAM_STR);
        $resultado = $consulta->execute();
        return $resultado;
    }

    public function modificarFichaHipertexto($fecha_personal, $id) {
        $resultado = '';
        $sql = "update hipertextos set fecha_documento=:fecha_personal where id=:id";
        //var_dump($sql); die();
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(':id', $id, PDO::PARAM_INT);
        $consulta->bindParam(':fecha_personal', date("Y-m-d", strtotime($fecha_personal)), PDO::PARAM_STR);
        $resultado = $consulta->execute();
        return $resultado;
    }

}
