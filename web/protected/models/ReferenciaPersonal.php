<?php

/**
 * This is the model class for table "gestion_humana.referencia_personal".
 *
 * The followings are the available columns in table 'gestion_humana.referencia_personal':
 * @property string $id
 * @property string $talento_humano_id
 * @property string $nombre_referencia
 * @property string $telefono_referencia
 * @property string $observacion
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property string $funcionario_mppe
 * @property string $email_referencia
 * @property string $cedula_referencia
 *
 * The followings are the available model relations:
 * @property TalentoHumano $talentoHumano
 * @property CondicionNominal $condicionNominal
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 */
class ReferenciaPersonal extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gestion_humana.referencia_personal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('talento_humano_id, cedula_referencia, nombre_referencia, condicion_nominal_id, telefono_referencia, usuario_ini_id, funcionario_mppe, estatus', 'required'),
            array('talento_humano_id, usuario_ini_id, usuario_act_id, condicion_nominal_id', 'numerical', 'integerOnly' => true),
            array('nombre_referencia', 'length', 'max' => 150),
            array('email_referencia', 'length', 'max' => 180),
            array('email_referencia', 'email'),
            array('cedula_referencia, telefono_referencia', 'length', 'max' => 15),
            array('estatus, funcionario_mppe', 'length', 'max' => 1),
            array('estatus', 'in', 'range' => array('A', 'I'), 'allowEmpty' => false, 'strict' => true,),
            array('funcionario_mppe', 'in', 'range' => array('S', 'N'), 'allowEmpty' => false, 'strict' => true,),
            array('usuario_ini_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'insert'),
            array('usuario_act_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'update'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, talento_humano_id, condicion_nominal_id, cedula_referencia, nombre_referencia, telefono_referencia, observacion, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, funcionario_mppe', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'talentoHumano' => array(self::BELONGS_TO, 'TalentoHumano', 'talento_humano_id'),
            'condicionNominal' => array(self::BELONGS_TO, 'CondicionNominal', 'condicion_nominal_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'talento_humano_id' => 'Talento Humano',
            'cedula_referencia' => 'Cédula de Identidad',
            'nombre_referencia' => 'Nombre y Apellido',
            'telefono_referencia' => 'Teléfono Referencia',
            'email_referencia' => 'Correo Electrónico',
            'funcionario_mppe' => '¿Es Funcionario MPPE?',
            'observacion' => 'Observación',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        if (strlen($this->id) > 0)
            $criteria->compare('id', $this->id, true);
        if (strlen($this->talento_humano_id) > 0)
            $criteria->compare('talento_humano_id', $this->talento_humano_id, true);
        if (strlen($this->nombre_referencia) > 0)
            $criteria->compare('nombre_referencia', $this->nombre_referencia, true);
        if (strlen($this->telefono_referencia) > 0)
            $criteria->compare('telefono_referencia', $this->telefono_referencia, true);
        if (strlen($this->observacion) > 0)
            $criteria->compare('observacion', $this->observacion, true);
        if (is_numeric($this->usuario_ini_id))
            $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d'))
            $criteria->compare('fecha_ini', $this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (is_numeric($this->usuario_act_id))
            $criteria->compare('usuario_act_id', $this->usuario_act_id);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d'))
            $criteria->compare('fecha_act', $this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (Utiles::isValidDate($this->fecha_elim, 'y-m-d'))
            $criteria->compare('fecha_elim', $this->fecha_elim);
        // if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
        if (in_array($this->estatus, array('A', 'I', 'E')))
            $criteria->compare('estatus', $this->estatus, true);
        if (strlen($this->funcionario_mppe) > 0)
            $criteria->compare('funcionario_mppe', $this->funcionario_mppe, true);
        if (strlen($this->funcionario_mppe) > 0)
            $criteria->compare('email_referencia', $this->email_referencia, true);
        if (strlen($this->cedula_referencia) > 0)
            $criteria->compare('cedula_referencia', $this->cedula_referencia, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeInsert($talentoHumanoId, $condicionNominalId) {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate($talentoHumanoId, $condicionNominalId) {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    public function __toString() {
        try {
            return (string) $this->id;
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ReferenciaPersonal the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
