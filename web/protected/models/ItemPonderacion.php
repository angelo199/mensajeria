<?php

/**
 * This is the model class for table "baremo.item_ponderacion".
 *
 * The followings are the available columns in table 'baremo.item_ponderacion':
 * @property integer $id
 * @property integer $tipo_periodo_proceso_id
 * @property integer $nivel_cargo_id
 * @property string $codigo
 * @property string $nombre
 * @property string $descripcion
 * @property integer $orden_interno
 * @property integer $orden_general
 * @property integer $es_padre
 * @property integer $item_padre_id
 * @property integer $es_solo_texto
 * @property integer $es_seleccionable
 * @property integer $tipo_item_ponderacion_id
 * @property integer $es_rango
 * @property integer $es_respuesta_correcta
 * @property string $minimo_puntaje
 * @property string $maximo_puntaje
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $estatus
 * @property integer $estatus_postulacion_id
 * @property string $dependiente_de
 * @property integer $valor_minimo_dependendiente
 * @property integer $valor_maximo_dependiente
 *
 * The followings are the available model relations:
 * @property EstatusPostulacion $estatusPostulacion
 * @property ItemPonderacion $itemPadre
 * @property ItemPonderacion[] $itemPonderacions
 * @property NivelCargo $nivelCargo
 * @property TipoItemPonderacion $tipoItemPonderacion
 * @property TipoPeriodoProceso $tipoPeriodoProceso
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property EntrevistaPonderacion[] $entrevistaPonderacions
 */
class ItemPonderacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'baremo.item_ponderacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_periodo_proceso_id, nivel_cargo_id, nombre, orden_interno, orden_general, fecha_ini', 'required'),
			array('tipo_periodo_proceso_id, nivel_cargo_id, orden_interno, orden_general, es_padre, item_padre_id, es_solo_texto, es_seleccionable, tipo_item_ponderacion_id, es_rango, es_respuesta_correcta, usuario_ini_id, usuario_act_id, estatus_postulacion_id, valor_minimo_dependendiente, valor_maximo_dependiente', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>10),
			array('nombre', 'length', 'max'=>150),
			array('descripcion', 'length', 'max'=>300),
			array('estatus', 'length', 'max'=>1),
			array('dependiente_de', 'length', 'max'=>100),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			array('minimo_puntaje, maximo_puntaje', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo_periodo_proceso_id, nivel_cargo_id, codigo, nombre, descripcion, orden_interno, orden_general, es_padre, item_padre_id, es_solo_texto, es_seleccionable, tipo_item_ponderacion_id, es_rango, es_respuesta_correcta, minimo_puntaje, maximo_puntaje, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, estatus, estatus_postulacion_id, dependiente_de, valor_minimo_dependendiente, valor_maximo_dependiente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estatusPostulacion' => array(self::BELONGS_TO, 'EstatusPostulacion', 'estatus_postulacion_id'),
			'itemPadre' => array(self::BELONGS_TO, 'ItemPonderacion', 'item_padre_id'),
			'itemPonderacions' => array(self::HAS_MANY, 'ItemPonderacion', 'item_padre_id'),
			'nivelCargo' => array(self::BELONGS_TO, 'NivelCargo', 'nivel_cargo_id'),
			'tipoItemPonderacion' => array(self::BELONGS_TO, 'TipoItemPonderacion', 'tipo_item_ponderacion_id'),
			'tipoPeriodoProceso' => array(self::BELONGS_TO, 'TipoPeriodoProceso', 'tipo_periodo_proceso_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'entrevistaPonderacions' => array(self::HAS_MANY, 'EntrevistaPonderacion', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo_periodo_proceso_id' => 'Tipo Periodo Proceso',
			'nivel_cargo_id' => 'Nivel Cargo',
			'codigo' => 'Codigo',
			'nombre' => 'Nombre',
			'descripcion' => 'Descripcion',
			'orden_interno' => 'Orden Interno',
			'orden_general' => 'Orden General',
			'es_padre' => 'Es Padre',
			'item_padre_id' => 'Item Padre',
			'es_solo_texto' => 'Es Solo Texto',
			'es_seleccionable' => 'Es Seleccionable',
			'tipo_item_ponderacion_id' => 'Tipo Item Ponderacion',
			'es_rango' => 'Es Rango',
			'es_respuesta_correcta' => 'Es Respuesta Correcta',
			'minimo_puntaje' => 'Minimo Puntaje',
			'maximo_puntaje' => 'Maximo Puntaje',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'estatus' => 'Estatus',
			'estatus_postulacion_id' => 'Estatus Postulacion',
			'dependiente_de' => 'Dependiente De',
			'valor_minimo_dependendiente' => 'Valor Minimo Dependendiente',
			'valor_maximo_dependiente' => 'Valor Maximo Dependiente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(is_numeric($this->tipo_periodo_proceso_id)) $criteria->compare('tipo_periodo_proceso_id',$this->tipo_periodo_proceso_id);
		if(is_numeric($this->nivel_cargo_id)) $criteria->compare('nivel_cargo_id',$this->nivel_cargo_id);
		if(strlen($this->codigo)>0) $criteria->compare('codigo',$this->codigo,true);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);
		if(strlen($this->descripcion)>0) $criteria->compare('descripcion',$this->descripcion,true);
		if(is_numeric($this->orden_interno)) $criteria->compare('orden_interno',$this->orden_interno);
		if(is_numeric($this->orden_general)) $criteria->compare('orden_general',$this->orden_general);
		if(is_numeric($this->es_padre)) $criteria->compare('es_padre',$this->es_padre);
		if(is_numeric($this->item_padre_id)) $criteria->compare('item_padre_id',$this->item_padre_id);
		if(is_numeric($this->es_solo_texto)) $criteria->compare('es_solo_texto',$this->es_solo_texto);
		if(is_numeric($this->es_seleccionable)) $criteria->compare('es_seleccionable',$this->es_seleccionable);
		if(is_numeric($this->tipo_item_ponderacion_id)) $criteria->compare('tipo_item_ponderacion_id',$this->tipo_item_ponderacion_id);
		if(is_numeric($this->es_rango)) $criteria->compare('es_rango',$this->es_rango);
		if(is_numeric($this->es_respuesta_correcta)) $criteria->compare('es_respuesta_correcta',$this->es_respuesta_correcta);
		if(strlen($this->minimo_puntaje)>0) $criteria->compare('minimo_puntaje',$this->minimo_puntaje,true);
		if(strlen($this->maximo_puntaje)>0) $criteria->compare('maximo_puntaje',$this->maximo_puntaje,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(is_numeric($this->estatus_postulacion_id)) $criteria->compare('estatus_postulacion_id',$this->estatus_postulacion_id);
		if(strlen($this->dependiente_de)>0) $criteria->compare('dependiente_de',$this->dependiente_de,true);
		if(is_numeric($this->valor_minimo_dependendiente)) $criteria->compare('valor_minimo_dependendiente',$this->valor_minimo_dependendiente);
		if(is_numeric($this->valor_maximo_dependiente)) $criteria->compare('valor_maximo_dependiente',$this->valor_maximo_dependiente);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItemPonderacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
