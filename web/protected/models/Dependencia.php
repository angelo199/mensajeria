<?php

/**
 * This is the model class for table "gestion_humana.dependencia".
 *
 * The followings are the available columns in table 'gestion_humana.dependencia':
 * @property integer $id
 * @property string $codigo
 * @property string $nombre
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $tipo_dependencia_id
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $estatus
 * @property string $fecha_act_nomina
 * @property string $categoria_dependencia
 *
 * The followings are the available model relations:
 * @property Postulacion[] $postulacions
 * @property Postulacion[] $postulacions1
 * @property TipoDependencia $tipoDependencia
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Estado $estado
 * @property Municipio $municipio
 * @property CargoVacante[] $cargoVacantes
 */
class Dependencia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gestion_humana.dependencia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, nombre, estado_id, tipo_dependencia_id, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act', 'required'),
			array('estado_id, municipio_id, tipo_dependencia_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('codigo', 'length', 'max'=>9),
			array('nombre', 'length', 'max'=>50),
			array('estatus, categoria_dependencia', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo, nombre, estado_id, municipio_id, tipo_dependencia_id, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, estatus, fecha_act_nomina, categoria_dependencia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'postulacions' => array(self::HAS_MANY, 'Postulacion', 'dependencia_postulado_id'),
			'postulacions1' => array(self::HAS_MANY, 'Postulacion', 'dependencia_evaluacion_id'),
			'tipoDependencia' => array(self::BELONGS_TO, 'TipoDependencia', 'tipo_dependencia_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
			'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
			'cargoVacantes' => array(self::HAS_MANY, 'CargoVacante', 'dependencia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'nombre' => 'Nombre',
			'estado_id' => 'Estado',
			'municipio_id' => 'Municipio',
			'tipo_dependencia_id' => 'Tipo Dependencia',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'estatus' => 'Estatus',
			'fecha_act_nomina' => 'Fecha Act Nomina',
			'categoria_dependencia' => '-- Valores
M=Despacho del Ministerio,
V=Vice-Ministerio,
G=Dirección General,
L=Dirección de Línea,
D=División,
C=Coordinación,
I=Institucion Educativa
O=Otro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(strlen($this->codigo)>0) $criteria->compare('codigo',$this->codigo,true);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);
		if(is_numeric($this->estado_id)) $criteria->compare('estado_id',$this->estado_id);
		if(is_numeric($this->municipio_id)) $criteria->compare('municipio_id',$this->municipio_id);
		if(is_numeric($this->tipo_dependencia_id)) $criteria->compare('tipo_dependencia_id',$this->tipo_dependencia_id);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(Utiles::isValidDate($this->fecha_act_nomina, 'y-m-d')) $criteria->compare('fecha_act_nomina',$this->fecha_act_nomina);
		// if(strlen($this->fecha_act_nomina)>0) $criteria->compare('fecha_act_nomina',$this->fecha_act_nomina,true);
		if(strlen($this->categoria_dependencia)>0) $criteria->compare('categoria_dependencia',$this->categoria_dependencia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dependencia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
