<?php
/**
 * This is the model class for table "ficha.personal".
 *
 * The followings are the available columns in table 'ficha.personal':
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property integer $cedula
 * @property string $dependencia
 * @property integer $cargo_id
 * @property string $url_foto
 * @property string $telefono_celular
 * @property string $telefono_local
 * @property string $direccion
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $url_copia
 * @property string $estatus
 * @property string $correo
 * @property string $fecha_ingreso
 * @property integer $estado_id
 *
 * The followings are the available model relations:
 * @property Cargo $cargo
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Tentidad $estado
 * @property DocumentoPersonal[] $documentoPersonals
 */
class Personal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ficha.personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido', 'length', 'max'=>40),
                        array('cargo', 'length', 'max'=>500),
			array('dependencia', 'length', 'max'=>50),
                        array('sexo', 'length', 'max'=>10),
                        array('cedula','unique'),
                        //  array('cedula','required'),
			array('url_foto, url_copia', 'length', 'max'=>100),
			array('telefono_celular, telefono_local', 'length', 'max'=>20),
			array('estatus', 'length', 'max'=>2),
			array('correo', 'length', 'max'=>30),
                        array('nacionalidad', 'length', 'max'=>5),
			array('direccion, fecha_ini, fecha_act, fecha_ingreso, fecha_nacimiento,fecha_movimiento,estado,cargo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, apellido, cedula, dependencia, cargo_id, url_foto, telefono_celular, telefono_local, direccion, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, url_copia, estatus,estado, correo, fecha_ingreso, estado_id, fecha_movimiento,sexo,estado,cargo,nacionalidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cargo' => array(self::BELONGS_TO, 'Cargo', 'cargo_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'estado' => array(self::BELONGS_TO, 'Tentidad', 'estado_id'),
			'documentoPersonals' => array(self::HAS_MANY, 'DocumentoPersonal', 'id_personal'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'cedula' => 'Cedula',
			'dependencia' => 'Dependencia',
			'cargo_id' => 'Cargo',
			'url_foto' => 'Url Foto',
			'telefono_celular' => 'Telefono Celular',
			'telefono_local' => 'Telefono Local',
			'direccion' => 'Direccion',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'url_copia' => 'Url Copia',
			'estatus' => 'Estatus',
			'correo' => 'Correo',
			'fecha_ingreso' => 'Fecha Ingreso',
			'estado_id' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('apellido',$this->apellido,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('dependencia',$this->dependencia,true);
		$criteria->compare('cargo_id',$this->cargo_id);
		$criteria->compare('url_foto',$this->url_foto,true);
		$criteria->compare('telefono_celular',$this->telefono_celular,true);
		$criteria->compare('telefono_local',$this->telefono_local,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('url_copia',$this->url_copia,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('fecha_ingreso',$this->fecha_ingreso,true);
		$criteria->compare('estado_id',$this->estado_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Personal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

            public function buscarIdPersonal($cedula) {
        $sql = "select id from ficha.personal where cedula=:cedula";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        return $busqueda->queryScalar();
    }

          public function buscarDatos($cedula) {
          $resultado='';
        $sql = "select id, cedula, telefono_local,telefono_celular from ficha.personal where cedula=:cedula";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        $resultado=$busqueda->queryRow();
        return $resultado;
    }
   public function saimeBuscar($cedula,$origen) {
        $resultado = '';
        $sql = "select s.primer_nombre, s.segundo_nombre, s.cedula, s.primer_apellido, s.segundo_apellido, s.fecha_nacimiento, s.sexo from auditoria.saime s where s.cedula=:cedula and s.origen=:origen group by s.primer_nombre, s.segundo_nombre, s.cedula, s.primer_apellido, s.segundo_apellido, s.fecha_nacimiento, s.sexo limit 1";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        $consulta->bindParam(":origen", $origen, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

          public function buscarDatos_nuevo($cedula) {
          $resultado='';
        $sql = "select id, cedula, telefono_local,telefono_celular from ficha.personal where cedula=$cedula";
        //var_dump($sql); die();
        $busqueda = Yii::app()->db->createCommand($sql);
        //$busqueda->bindParam(":cedula", $cedula, PDO::PARAM_STR);
        $resultado=$busqueda->queryRow();
        return $resultado;
    }


        public function buscarFiniquitos($cedula) {
        $resultado='';
        $sql = "SELECT raiz || ruta AS ruta FROM finiquito_ruta WHERE cedula =:cedula UNION SELECT raiz || ruta as ruta FROM rutas_modulo WHERE cedula =:cedula UNION SELECT ruta FROM rutas_masiva WHERE cedula =:cedula UNION SELECT ruta FROM public.rutas_masiva_lote_680_720 WHERE cedula=:cedula";
        //<var_dump($sql); die();
        $busqueda = Yii::app()->finiquitos->createCommand($sql);
        $busqueda->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        $resultado=$busqueda->queryAll();
        return $resultado;
    }


    public function buscarExpedienteCatiaAlejandria(){

    }



}
