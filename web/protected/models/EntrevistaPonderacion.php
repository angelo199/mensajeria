<?php

/**
 * This is the model class for table "baremo.entrevista_ponderacion".
 *
 * The followings are the available columns in table 'baremo.entrevista_ponderacion':
 * @property integer $id

 * @property integer $puntuacion_obtenida
 * @property integer $item_id
 * @property string $talento_humano_id
 * @property string $postulacion_id
 * @property string $entrevista_id
 * @property string $estatus
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 *
 * The followings are the available model relations:
 * @property EntrevistaPostulacion $entrevista
 * @property Postulacion $postulacion
 * @property TalentoHumano $talentoHumano
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property ItemPonderacion $item
 */
class EntrevistaPonderacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'baremo.entrevista_ponderacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('puntuacion_maxima, puntuacion_obtenida, item_id, talento_humano_id, postulacion_id, entrevista_id, fecha_ini, fecha_act, usuario_ini_id, usuario_act_id', 'required'),
			array('puntuacion_maxima, puntuacion_obtenida, item_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, puntuacion_maxima, puntuacion_obtenida, item_id, talento_humano_id, postulacion_id, entrevista_id, estatus, fecha_ini, fecha_act, usuario_ini_id, usuario_act_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'entrevista' => array(self::BELONGS_TO, 'EntrevistaPostulacion', 'entrevista_id'),
			'postulacion' => array(self::BELONGS_TO, 'Postulacion', 'postulacion_id'),
			'talentoHumano' => array(self::BELONGS_TO, 'TalentoHumano', 'talento_humano_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'item' => array(self::BELONGS_TO, 'ItemPonderacion', 'item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'puntuacion_maxima' => 'Puntuacion Maxima',
			'puntuacion_obtenida' => 'Puntuacion Obtenida',
			'item_id' => 'Item',
			'talento_humano_id' => 'Talento Humano',
			'postulacion_id' => 'Postulacion',
			'entrevista_id' => 'Entrevista',
			'estatus' => 'Estatus',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(is_numeric($this->puntuacion_maxima)) $criteria->compare('puntuacion_maxima',$this->puntuacion_maxima);
		if(is_numeric($this->puntuacion_obtenida)) $criteria->compare('puntuacion_obtenida',$this->puntuacion_obtenida);
		if(is_numeric($this->item_id)) $criteria->compare('item_id',$this->item_id);
		if(strlen($this->talento_humano_id)>0) $criteria->compare('talento_humano_id',$this->talento_humano_id,true);
		if(strlen($this->postulacion_id)>0) $criteria->compare('postulacion_id',$this->postulacion_id,true);
		if(strlen($this->entrevista_id)>0) $criteria->compare('entrevista_id',$this->entrevista_id,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EntrevistaPonderacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
