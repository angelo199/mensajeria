<?php

/**
 * This is the model class for table "hipertextos".
 *
 * The followings are the available columns in table 'hipertextos':
 * @property string $cod_hipertexto
 * @property string $codigo
 * @property string $acceso
 * @property string $ext_acceso
 * @property string $dir_virtual
 * @property string $dir_fisico
 * @property string $archivo
 * @property string $descripcion
 * @property string $grado
 * @property string $valor
 * @property string $uso
 * @property string $orden
 * @property string $icono_reg
 * @property string $sonido_reg
 * @property string $documento_reg
 * @property string $comentario
 * @property string $nro_pagina
 * @property string $extension
 * @property string $codigo_crea
 * @property string $fecha_crea
 * @property string $codigo_superv
 * @property string $fecha_superv
 * @property string $imagen_reg
 * @property string $video_reg
 * @property string $tamano
 * @property string $usr_creador
 * @property string $usr_supervisor
 *
 * The followings are the available model relations:
 * @property Central $acceso0
 * @property Central $extAcceso
 * @property Usuarios $codigo0
 * @property Usuarios $usrCreador
 * @property Usuarios $usrSupervisor
 */
class Hipertextos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hipertextos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cod_hipertexto, archivo', 'required'),
			array('cod_hipertexto, uso, orden', 'length', 'max'=>8),
			array('codigo, codigo_crea, codigo_superv, usr_creador, usr_supervisor', 'length', 'max'=>12),
			array('acceso', 'length', 'max'=>13),
			array('ext_acceso, valor, nro_pagina', 'length', 'max'=>4),
			array('dir_virtual, dir_fisico', 'length', 'max'=>180),
			array('archivo', 'length', 'max'=>255),
			array('descripcion', 'length', 'max'=>192),
			array('grado, icono_reg, sonido_reg, documento_reg, imagen_reg, video_reg', 'length', 'max'=>1),
			array('comentario', 'length', 'max'=>2),
			array('extension', 'length', 'max'=>3),
			array('fecha_crea, fecha_superv, tamano', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cod_hipertexto, codigo, acceso, ext_acceso, dir_virtual, dir_fisico, archivo, descripcion, grado, valor, uso, orden, icono_reg, sonido_reg, documento_reg, comentario, nro_pagina, extension, codigo_crea, fecha_crea, codigo_superv, fecha_superv, imagen_reg, video_reg, tamano, usr_creador, usr_supervisor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'acceso0' => array(self::BELONGS_TO, 'Central', 'acceso'),
			'extAcceso' => array(self::BELONGS_TO, 'Central', 'ext_acceso'),
			'codigo0' => array(self::BELONGS_TO, 'Usuarios', 'codigo'),
			'usrCreador' => array(self::BELONGS_TO, 'Usuarios', 'usr_creador'),
			'usrSupervisor' => array(self::BELONGS_TO, 'Usuarios', 'usr_supervisor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cod_hipertexto' => 'Cod Hipertexto',
			'codigo' => 'Codigo',
			'acceso' => 'Acceso',
			'ext_acceso' => 'Ext Acceso',
			'dir_virtual' => 'Dir Virtual',
			'dir_fisico' => 'Dir Fisico',
			'archivo' => 'Archivo',
			'descripcion' => 'Descripcion',
			'grado' => 'Grado',
			'valor' => 'Valor',
			'uso' => 'Uso',
			'orden' => 'Orden',
			'icono_reg' => 'Icono Reg',
			'sonido_reg' => 'Sonido Reg',
			'documento_reg' => 'Documento Reg',
			'comentario' => 'Comentario',
			'nro_pagina' => 'Nro Pagina',
			'extension' => 'Extension',
			'codigo_crea' => 'Codigo Crea',
			'fecha_crea' => 'Fecha Crea',
			'codigo_superv' => 'Codigo Superv',
			'fecha_superv' => 'Fecha Superv',
			'imagen_reg' => 'Imagen Reg',
			'video_reg' => 'Video Reg',
			'tamano' => 'Tamano',
			'usr_creador' => 'Usr Creador',
			'usr_supervisor' => 'Usr Supervisor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cod_hipertexto',$this->cod_hipertexto,true);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('acceso',$this->acceso,true);
		$criteria->compare('ext_acceso',$this->ext_acceso,true);
		$criteria->compare('dir_virtual',$this->dir_virtual,true);
		$criteria->compare('dir_fisico',$this->dir_fisico,true);
		$criteria->compare('archivo',$this->archivo,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('grado',$this->grado,true);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('uso',$this->uso,true);
		$criteria->compare('orden',$this->orden,true);
		$criteria->compare('icono_reg',$this->icono_reg,true);
		$criteria->compare('sonido_reg',$this->sonido_reg,true);
		$criteria->compare('documento_reg',$this->documento_reg,true);
		$criteria->compare('comentario',$this->comentario,true);
		$criteria->compare('nro_pagina',$this->nro_pagina,true);
		$criteria->compare('extension',$this->extension,true);
		$criteria->compare('codigo_crea',$this->codigo_crea,true);
		$criteria->compare('fecha_crea',$this->fecha_crea,true);
		$criteria->compare('codigo_superv',$this->codigo_superv,true);
		$criteria->compare('fecha_superv',$this->fecha_superv,true);
		$criteria->compare('imagen_reg',$this->imagen_reg,true);
		$criteria->compare('video_reg',$this->video_reg,true);
		$criteria->compare('tamano',$this->tamano,true);
		$criteria->compare('usr_creador',$this->usr_creador,true);
		$criteria->compare('usr_supervisor',$this->usr_supervisor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hipertextos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
