<?php

/**
 * This is the model class for table "gestion_humana.experiencia_laboral".
 *
 * The followings are the available columns in table 'gestion_humana.experiencia_laboral':
 * @property string $id
 * @property string $talento_humano_id
 * @property string $institucion
 * @property string $publica
 * @property string $cargo_desempenhado
 * @property string $actividades_realizadas
 * @property string $fecha_ingreso
 * @property string $activo_actualmente
 * @property string $fecha_egreso
 * @property string $nombre_referencia
 * @property string $telefono_referencia
 * @property string $observacion
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property string $condicion_nominal_id
 *
 * The followings are the available model relations:
 * @property TalentoHumano $talentoHumano
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property CondicionNominal $condicionNominal
 */
class ExperienciaLaboral extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gestion_humana.experiencia_laboral';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('talento_humano_id, institucion, publica, cargo_desempenhado, actividades_realizadas, fecha_ingreso, activo_actualmente, usuario_ini_id, condicion_nominal_id', 'required'),
			array('usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('institucion, cargo_desempenhado, nombre_referencia', 'length', 'max'=>150),
                        array('observacion, actividades_realizadas', 'length', 'max'=>200),
			array('publica, activo_actualmente, estatus', 'length', 'max'=>1),
			array('telefono_referencia', 'length', 'max'=>15),
			array('estatus', 'in', 'range'=>array('A', 'I',), 'allowEmpty'=>false, 'strict'=>true,),
                        array('publica, activo_actualmente', 'in', 'range'=>array('S', 'N',), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			array('actividades_realizadas', 'safe'),
                        array('fecha_egreso', 'checkFechaEgreso'),
                        array('fecha_ingreso, fecha_egreso', 'checkFechaValida'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, talento_humano_id, institucion, publica, cargo_desempenhado, actividades_realizadas, fecha_ingreso, activo_actualmente, fecha_egreso, nombre_referencia, telefono_referencia, observacion, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, condicion_nominal_id', 'safe', 'on'=>'search'),
		);
	}

        public function checkFechaValida($attribute, $params=null){
            if(!Helper::isValidDate($this->$attribute, 'y-m-d') && strlen($this->$attribute)>0){
                $field = strtr($attribute, 'fecha_', '');
                $this->addError($attribute, 'La fecha de '.$field.' indicada no es válida.');
            }
        }

        public function checkFechaEgreso($attribute, $param=null){
            if(($this->activo_actualmente=='S') && strlen($this->$attribute)>0){
                $this->addError($attribute, 'Para efectuar el registro de la Fecha de Egreso no debe indicar que se encuentra activo actualmente en esta empresa o institución.');
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'talentoHumano' => array(self::BELONGS_TO, 'TalentoHumano', 'talento_humano_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'condicionNominal' => array(self::BELONGS_TO, 'CondicionNominal', 'condicion_nominal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'talento_humano_id' => 'Talento Humano',
			'institucion' => 'Empresa / Institución',
			'publica' => 'Es una Institución Pública',
			'cargo_desempenhado' => 'Cargo Desempeñado',
			'actividades_realizadas' => 'Actividades Desempeñadas',
			'fecha_ingreso' => 'Fecha Ingreso',
			'activo_actualmente' => 'Activo Actualmente',
			'fecha_egreso' => 'Fecha Egreso',
			'nombre_referencia' => 'Nombre de Contacto Laboral',
			'telefono_referencia' => 'Teléfono de Contacto Laboral',
			'observacion' => 'Observación',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
			'condicion_nominal_id' => 'Condición Nominal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->id)>0) $criteria->compare('id',$this->id,true);
		if(strlen($this->talento_humano_id)>0) $criteria->compare('talento_humano_id',$this->talento_humano_id,true);
		if(strlen($this->institucion)>0) $criteria->compare('institucion',$this->institucion,true);
		if(strlen($this->publica)>0) $criteria->compare('publica',$this->publica,true);
		if(strlen($this->cargo_desempenhado)>0) $criteria->compare('cargo_desempenhado',$this->cargo_desempenhado,true);
		if(strlen($this->actividades_realizadas)>0) $criteria->compare('actividades_realizadas',$this->actividades_realizadas,true);
		if(Utiles::isValidDate($this->fecha_ingreso, 'y-m-d')) $criteria->compare('fecha_ingreso',$this->fecha_ingreso);
		// if(strlen($this->fecha_ingreso)>0) $criteria->compare('fecha_ingreso',$this->fecha_ingreso,true);
		if(strlen($this->activo_actualmente)>0) $criteria->compare('activo_actualmente',$this->activo_actualmente,true);
		if(Utiles::isValidDate($this->fecha_egreso, 'y-m-d')) $criteria->compare('fecha_egreso',$this->fecha_egreso);
		// if(strlen($this->fecha_egreso)>0) $criteria->compare('fecha_egreso',$this->fecha_egreso,true);
		if(strlen($this->nombre_referencia)>0) $criteria->compare('nombre_referencia',$this->nombre_referencia,true);
		if(strlen($this->telefono_referencia)>0) $criteria->compare('telefono_referencia',$this->telefono_referencia,true);
		if(strlen($this->observacion)>0) $criteria->compare('observacion',$this->observacion,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(strlen($this->condicion_nominal_id)>0) $criteria->compare('condicion_nominal_id',$this->condicion_nominal_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert($talentoHumanoId, $condicionNominalId)
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate($talentoHumanoId, $condicionNominalId)
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExperienciaLaboral the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
