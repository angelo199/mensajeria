<?php

/**
 * This is the model class for table "movimientos.personal".
 *
 * The followings are the available columns in table 'movimientos.personal':
 * @property integer $id
 * @property string $nombre
 * @property string $apellido
 * @property integer $cedula
 * @property string $direccion
 * @property string $estatus
 * @property string $nacionalidad
 * @property string $sexo
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_ini
 * @property string $fecha_nacimiento
 * @property integer $departamento_id
 *
 * The followings are the available model relations:
 * @property Departamento $departamento
 * @property MovimientoFicha[] $movimientoFichas
 */
class Personal_movimientos extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'movimientos.personal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cedula, usuario_ini_id, usuario_act_id, departamento_id', 'numerical', 'integerOnly' => true),
            array('nombre, apellido', 'length', 'max' => 40),
            array('estatus, nacionalidad', 'length', 'max' => 2),
            array('sexo', 'length', 'max' => 10),
            array('direccion, fecha_act, fecha_ini, fecha_nacimiento', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, apellido, cedula, direccion, estatus, nacionalidad, sexo, usuario_ini_id, usuario_act_id, fecha_act, fecha_ini, fecha_nacimiento, departamento_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'departamento' => array(self::BELONGS_TO, 'Departamento', 'departamento_id'),
            'movimientoFichas' => array(self::HAS_MANY, 'MovimientoFicha', 'personal_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'cedula' => 'Cedula',
            'direccion' => 'Direccion',
            'estatus' => 'Estatus',
            'nacionalidad' => 'Nacionalidad',
            'sexo' => 'Sexo',
            'usuario_ini_id' => 'Usuario Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_ini' => 'Fecha Ini',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'departamento_id' => 'Departamento',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('apellido', $this->apellido, true);
        $criteria->compare('cedula', $this->cedula);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('estatus', $this->estatus, true);
        $criteria->compare('nacionalidad', $this->nacionalidad, true);
        $criteria->compare('sexo', $this->sexo, true);
        $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('usuario_act_id', $this->usuario_act_id);
        $criteria->compare('fecha_act', $this->fecha_act, true);
        $criteria->compare('fecha_ini', $this->fecha_ini, true);
        $criteria->compare('fecha_nacimiento', $this->fecha_nacimiento, true);
        $criteria->compare('departamento_id', $this->departamento_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function personal_nuevo($cedula, $nacionalidad) {
        $sql = "select *from movimientos.personal where cedula=:cedula and nacionalidad=:nacionalidad";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('cedula', $cedula, PDO::PARAM_INT);
        $consulta->bindParam('nacionalidad', $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function buscar_movimientos($cedula, $nacionalidad) {
        $sql = "SELECT  m.fecha_solicitud, m.estatus, d.nombre as departamento, de.nombre as departamento_entrega,  m.id as movimiento_id, p.id, m.fecha_entrega, p.nombre,p.apellido,p.cedula,p.nacionalidad
        FROM movimientos.movimiento_ficha m
        inner join movimientos.personal p on m.personal_id=p.id
        left join movimientos.departamento d on m.departamento_id=d.id
        left join movimientos.departamento de on m.departamento_id_entrega=de.id
        where p.cedula=:cedula and p.nacionalidad=:nacionalidad order by m.fecha_entrega desc";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('cedula', $cedula, PDO::PARAM_INT);
        $consulta->bindParam('nacionalidad', $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function buscar_movimientos_fore($cedula, $nacionalidad) {
        $sql = "SELECT  m.fecha_solicitud, m.estatus, d.nombre as departamento, de.nombre as departamento_entrega,  m.id as movimiento_id, p.id, m.fecha_entrega, p.nombre,p.apellido,p.cedula,p.nacionalidad
        FROM movimientos.movimiento_ficha m
        inner join movimientos.personal p on m.personal_id=p.id
        left join movimientos.departamento d on m.departamento_id=d.id
        left join movimientos.departamento de on m.departamento_id_entrega=de.id
        where p.cedula=:cedula and p.nacionalidad=:nacionalidad order by m.fecha_entrega desc";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('cedula', $cedula, PDO::PARAM_INT);
        $consulta->bindParam('nacionalidad', $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function viata_general($cedula, $nacionalidad) {
        $sql = "SELECT  m.fecha_solicitud, m.estatus, d.nombre as departamento,
de.nombre as departamento_entrega,  m.id as movimiento_id, p.id,
m.fecha_entrega, p.nombre,p.apellido,p.cedula,p.nacionalidad,
fun.nombre as nombre_funcionario,fun.apellido as apellido_funcionario,fun.cedula as cedula_funcionario,fun.correo as correo_funcionario,
fun.telefono as telefono_funcionario
        FROM movimientos.movimiento_ficha m
        inner join movimientos.personal p on m.personal_id=p.id
        left join movimientos.departamento d on m.departamento_id=d.id
        left join movimientos.departamento de on m.departamento_id_entrega=de.id
        left  join movimientos.funcionario fun on fun.id=m.funcionario_id
        where p.cedula=:cedula and p.nacionalidad=:nacionalidad order by m.fecha_entrega desc";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('cedula', $cedula, PDO::PARAM_INT);
        $consulta->bindParam('nacionalidad', $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryRow();
        return $resultado;
    }

    public function movimientoBotonMostrar($cedula, $nacionalidad) {
        $sql = "SELECT  m.fecha_entrega
        FROM movimientos.movimiento_ficha  m
        INNER JOIN movimientos.personal p ON p.id=m.personal_id
        where p.cedula=:cedula and p.nacionalidad=:nacionalidad order by m.id desc limit 1";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('cedula', $cedula, PDO::PARAM_INT);
        $consulta->bindParam('nacionalidad', $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryScalar();
        return $resultado;
    }

    public function penultimoMovimiento($cedula, $nacionalidad) {
        $sql = "select m.fecha_solicitud  from movimientos.movimiento_ficha m
inner join movimientos.personal p  on p.id=m.personal_id
left join movimientos.departamento d on d.id=m.departamento_id
left join movimientos.departamento dd on dd.id=m.departamento_id
where p.cedula=:cedula and p.nacionalidad=:nacionalidad and fecha_entrega is null";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam('cedula', $cedula, PDO::PARAM_INT);
        $consulta->bindParam('nacionalidad', $nacionalidad, PDO::PARAM_STR);
        $resultado = $consulta->queryScalar();
        return $resultado;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Personal_movimientos the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
