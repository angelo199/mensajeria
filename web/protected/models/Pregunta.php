<?php

/**
 * This is the model class for table "gprueba.pregunta".
 *
 * The followings are the available columns in table 'gprueba.pregunta':
 * @property string $id
 * @property string $nombre
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $seccion_prueba_id
 * @property integer $sub_seccion_prueba_id
 * @property integer $ponderacion
 *
 * The followings are the available model relations:
 * @property SeccionPrueba $seccionPrueba
 * @property SubSeccionPrueba $subSeccionPrueba
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 * @property Respuesta[] $respuestas
 * @property RespuestaPruebaAsp[] $respuestaPruebaAsps
 */
class Pregunta extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gprueba.pregunta';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, usuario_ini_id, fecha_ini, estatus, seccion_prueba_id', 'required'),
            array('usuario_ini_id, usuario_act_id, usuario_elim_id, seccion_prueba_id, ponderacion', 'numerical', 'integerOnly' => true),
            array('nombre', 'length', 'max' => 150),
            array('fecha_ini, fecha_act, fecha_elim', 'length', 'max' => 6),
            array('estatus', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus, seccion_prueba_id, ponderacion', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'seccionPrueba' => array(self::BELONGS_TO, 'SeccionPrueba', 'seccion_prueba_id'),            
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'respuestas' => array(self::HAS_MANY, 'Respuesta', 'pregunta_id'),
            'respuestaPruebaAsps' => array(self::HAS_MANY, 'RespuestaPruebaAsp', 'pregunta_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'usuario_elim_id' => 'Usuario Elim',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'seccion_prueba_id' => 'Seccion Prueba',
            'sub_seccion_prueba_id' => 'Sub Seccion Prueba',
            'ponderacion' => 'Ponderacion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('fecha_ini', $this->fecha_ini, true);
        $criteria->compare('usuario_act_id', $this->usuario_act_id);
        $criteria->compare('fecha_act', $this->fecha_act, true);
        $criteria->compare('usuario_elim_id', $this->usuario_elim_id);
        $criteria->compare('fecha_elim', $this->fecha_elim, true);
        $criteria->compare('estatus', $this->estatus, true);
        if (is_numeric($this->seccion_prueba_id)) {
            if (strlen($this->seccion_prueba_id) <= 10) {
                $criteria->compare('seccion_prueba_id', $this->seccion_prueba_id);
            }
        }
        if (is_numeric($this->sub_seccion_prueba_id)) {
            if (strlen($this->sub_seccion_prueba_id) <= 10) {
                $criteria->compare('sub_seccion_prueba_id', $this->sub_seccion_prueba_id);
            }
        }
        if (is_numeric($this->ponderacion)) {
            if (strlen($this->ponderacion) <= 10) {
                $criteria->compare('ponderacion', $this->ponderacion);
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function guardarCambiosSimple($nombre, $usuario_ini_id, $fecha_ini, $seccion_prueba_id, $sub_seccion_prueba_id, $ponderacion, $tipo_pregunta) {

        $sql = "SELECT * FROM gprueba.pregunta p WHERE p.nombre ILIKE '%$nombre%'";
        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->queryAll();
        if (!$resultado) {
            $sql = "INSERT INTO gprueba.pregunta(
                nombre, usuario_ini_id, fecha_ini, estatus, seccion_prueba_id, sub_seccion_prueba_id, ponderacion, tipo_pregunta)
                VALUES (:nombre, :usuario_ini, :fecha_ini, 'A', :seccion_prueba_id, :sub_seccion_prueba_id, :ponderacion, :tipo_pregunta) RETURNING id;";

            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":nombre", $nombre, PDO::PARAM_STR);
            $consulta->bindParam(":usuario_ini", $usuario_ini_id, PDO::PARAM_INT);
            $consulta->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
            $consulta->bindParam(":seccion_prueba_id", $seccion_prueba_id, PDO::PARAM_INT);
            $consulta->bindParam(":sub_seccion_prueba_id", $sub_seccion_prueba_id, PDO::PARAM_INT);
            $consulta->bindParam(":ponderacion", $ponderacion, PDO::PARAM_INT);
            $consulta->bindParam(":tipo_pregunta", $tipo_pregunta, PDO::PARAM_STR);
            $resultado = $consulta->queryAll();
        } else {
            $resultado = 2;
        }

        return $resultado;
    }

    public function modificarCambiosSimple($id, $nombre, $usuario_ini_id, $fecha_ini, $seccion_prueba_id, $sub_seccion_prueba_id, $ponderacion, $tipo_pregunta) {
        if ($sub_seccion_prueba_id == '') {
            $sub_seccion_prueba_id = 'NULL';
        }
//        $sql = "SELECT * FROM gprueba.pregunta p WHERE p.nombre ILIKE '%$nombre%'";
//        $consulta = Yii::app()->db->createCommand($sql);
//        $resultado = $consulta->queryAll();
//        if(!$resultado){
//            $sql = "UPDATE gprueba.pregunta SET
//                nombre=:nombre,
//                seccion_prueba_id=:seccion_prueba_id,
//                sub_seccion_prueba_id=:sub_seccion_prueba_id,
//                ponderacion=:ponderacion,
//                tipo_pregunta=:tipo_pregunta
//                WHERE id = :id;";
        $sql = "UPDATE gprueba.pregunta SET
                nombre='$nombre',
                seccion_prueba_id=$seccion_prueba_id,
                sub_seccion_prueba_id=$sub_seccion_prueba_id,
                ponderacion=$ponderacion,
                tipo_pregunta='$tipo_pregunta'
                WHERE id = $id;";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":id", $id, PDO::PARAM_INT);
        $consulta->bindParam(":nombre", $nombre, PDO::PARAM_STR);
        $consulta->bindParam(":usuario_ini", $usuario_ini_id, PDO::PARAM_INT);
        $consulta->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
        $consulta->bindParam(":seccion_prueba_id", $seccion_prueba_id, PDO::PARAM_INT);
        $consulta->bindParam(":sub_seccion_prueba_id", $sub_seccion_prueba_id, PDO::PARAM_INT);
        $consulta->bindParam(":ponderacion", $ponderacion, PDO::PARAM_INT);
        $consulta->bindParam(":tipo_pregunta", $tipo_pregunta, PDO::PARAM_STR);
        $resultado = $consulta->execute();
//        }
//        else{
//            $resultado = 2;
//        }

        return $resultado;
    }

    public function guardarCambiosSimpleImagen($pregunta, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo) {
        $sql = "INSERT INTO gprueba.pregunta(
                nombre, usuario_ini_id, fecha_ini, estatus, seccion_prueba_id, sub_seccion_prueba_id, ponderacion, tipo_pregunta)
                VALUES (:nombre, :usuario_ini, :fecha_ini, 'A', :seccion_prueba_id, :sub_seccion_prueba_id, :ponderacion, :tipo_pregunta) RETURNING id;";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":nombre", $pregunta, PDO::PARAM_STR);
        $consulta->bindParam(":usuario_ini", $usuario_id, PDO::PARAM_INT);
        $consulta->bindParam(":fecha_ini", $fecha, PDO::PARAM_STR);
        $consulta->bindParam(":seccion_prueba_id", $seccion_id, PDO::PARAM_INT);
        $consulta->bindParam(":sub_seccion_prueba_id", $sub_seccion_prueba_id, PDO::PARAM_INT);
        $consulta->bindParam(":ponderacion", $pregunta_ponderacion, PDO::PARAM_INT);
        $consulta->bindParam(":tipo_pregunta", $tipo_pregunta_acronimo, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();

        return $resultado;
    }

    public function modificarCambiosSimpleImagen($id, $pregunta, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo) {
        if ($sub_seccion_prueba_id == '') {
            $sub_seccion_prueba_id = 'NULL';
        }
        $sql = "
            UPDATE gprueba.pregunta SET
                nombre = '$pregunta',
                seccion_prueba_id = $seccion_id,
                sub_seccion_prueba_id = $sub_seccion_prueba_id,
                ponderacion = $pregunta_ponderacion,
                tipo_pregunta = '$tipo_pregunta_acronimo'
                WHERE id = $id;";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":nombre", $pregunta, PDO::PARAM_STR);
        $consulta->bindParam(":usuario_ini", $usuario_id, PDO::PARAM_INT);
        $consulta->bindParam(":fecha_ini", $fecha, PDO::PARAM_STR);
        $consulta->bindParam(":seccion_prueba_id", $seccion_id, PDO::PARAM_INT);
        $consulta->bindParam(":sub_seccion_prueba_id", $sub_seccion_prueba_id, PDO::PARAM_INT);
        $consulta->bindParam(":ponderacion", $pregunta_ponderacion, PDO::PARAM_INT);
        $consulta->bindParam(":tipo_pregunta", $tipo_pregunta_acronimo, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();

        return $resultado;
    }

    public function modelPregunta($pregunta_id) {
        $pregunta_id = base64_decode($pregunta_id);
        $sql = "SELECT * FROM gprueba.pregunta p
                INNER JOIN gprueba.respuesta r ON r.pregunta_id = p.id
                LEFT JOIN gprueba.seccion_prueba sp ON sp.id = p.seccion_prueba_id
                LEFT JOIN gprueba.sub_seccion_prueba ssp ON ssp.id = p.sub_seccion_prueba_id
                WHERE p.id = $pregunta_id
            ";
        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function inactivarPregunta($id) {
        $sql = "UPDATE gprueba.pregunta SET estatus = 'I' WHERE id = $id";
        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->execute();
        return $resultado;
    }

    public function activarPregunta($id) {
        $sql = "UPDATE gprueba.pregunta SET estatus = 'A' WHERE id = $id";
        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->execute();
        return $resultado;
    }

    public function aplicoPrueba($id) {
        $id = base64_decode($id);
        $sql = "SELECT * FROM gprueba.pregunta p
                INNER JOIN gprueba.seccion_prueba sp ON sp.id = p.seccion_prueba_id
                INNER JOIN gprueba.prueba pr ON pr.id = sp.prueba_id
                INNER JOIN gprueba.aplicacion_prueba ap ON ap.prueba_id = pr.id
                WHERE p.id = $id";
        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function guardarCambiosPreguntasSimple() {
//            $sql = "INSERT INTO gprueba.pregunta(
//                nombre, usuario_ini_id, fecha_ini, estatus, seccion_prueba_id, sub_seccion_prueba_id, ponderacion)
//                VALUES (:nombre, :usuario_ini, :fecha_ini, 'A', :seccion_prueba_id, :sub_seccion_prueba_id, :ponderacion) RETURNING id;";
//
//            $consulta = Yii::app()->db->createCommand($sql);
//            $consulta->bindParam(":nombre", $nombre, PDO::PARAM_STR);
//            $consulta->bindParam(":usuario_ini", $usuario_ini_id, PDO::PARAM_INT);
//            $consulta->bindParam(":fecha_ini", $fecha_ini, PDO::PARAM_STR);
//            $consulta->bindParam(":seccion_prueba_id", $seccion_prueba_id, PDO::PARAM_INT);
//            $consulta->bindParam(":sub_seccion_prueba_id", $sub_seccion_prueba_id, PDO::PARAM_INT);
//            $consulta->bindParam(":ponderacion", $ponderacion, PDO::PARAM_INT);
//            $resultado = $consulta->queryAll();
//
//            return $resultado;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pregunta the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function existePregunta($seccionId, $nombre) {
        $criteria = new CDbCriteria();

        $criteria->addInCondition('estatus', array('A'));
        $criteria->addInCondition('nombre', array($nombre));
        $criteria->addInCondition('seccion_prueba_id', array($seccionId));

        return $this->findAll($criteria);
    }

    /**
     * @param int $subSeccionId Id de la subseccion de prueba
     * @param string $nombre Nombre de la pregunta
     *
     * @return array|CActiveRecord|mixed|null
     */
    public function existePreguntaSubSeccion($subSeccionId, $nombre) {
        $criteria = new CDbCriteria();

        $criteria->addInCondition('estatus', array('A'));
        $criteria->addInCondition('nombre', array($nombre));
        $criteria->addInCondition('sub_seccion_prueba_id', array($subSeccionId));

        return $this->findAll($criteria);
    }

    /**
     * Obtener las preguntas de una sección de prueba
     * @param int $idSeccion
     * @param string $estatus_prueba
     * @return CActiveRecord
     */
    public function getPreguntasSeccion($idSeccion, $estatus_prueba = '', $variable = '') {
        // Arreglar validacion
        if ($estatus_prueba == 'A' && $variable == '') {
            return $this->findAll(" seccion_prueba_id = $idSeccion AND sub_seccion_prueba_id IS NULL AND estatus='A' ORDER BY estatus ASC, nombre ASC");
        } elseif ($estatus_prueba == 'I' || $estatus_prueba == '' || $variable == 'U') {
            return $this->findAll("seccion_prueba_id = $idSeccion AND sub_seccion_prueba_id IS NULL ORDER BY estatus ASC, nombre ASC");
        }
    }

    /**
     * Obtener las preguntas de una sub-sección de prueba
     * @param int $idSubSeccion
     * @return CActiveRecord
     */
    public function getPreguntasSubSeccion($idSubSeccion, $estatus_prueba = '', $variable = '') {
        $criteria = new CDbCriteria();
        if ($estatus_prueba == 'A' && $variable = '') {
            $criteria->addInCondition('estatus', array($estatus_prueba));
            $criteria->addInCondition('sub_seccion_prueba_id', array($idSubSeccion));
        } elseif ($estatus_prueba == 'I' || $estatus_prueba == '' || $variable == 'U') {
            $criteria->addInCondition('sub_seccion_prueba_id', array($idSubSeccion));
        }
        $criteria->order = 'estatus, nombre ASC';
        return $this->findAll($criteria);
    }

    public function datosPreguntas($seccion_id) {

        $estatus = 'A';
        $sql = "SELECT p.id AS pregunta_id, sp.id AS seccion_id
                    FROM gprueba.seccion_prueba sp
                    INNER JOIN gprueba.pregunta p ON(p.seccion_prueba_id = sp.id)
                    WHERE p.estatus=:estatus
                    AND sp.id=:seccion_id";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":seccion_id", $seccion_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }
    

    public function datosPreguntaSubSeccion($sub_seccion_id) {
        $estatus = 'A';
        $sub_seccion_id = (int) $sub_seccion_id;
        $sql = "SELECT p.id AS pregunta_id, spp.id AS seccion_id
                    FROM gprueba.seccion_prueba sp
                    INNER JOIN gprueba.sub_seccion_prueba spp ON(spp.seccion_prueba_id = sp.id)
                    INNER JOIN gprueba.pregunta p ON(p.seccion_prueba_id = sp.id AND p.sub_seccion_prueba_id = spp.id)
                    WHERE p.estatus=:estatus
                    AND spp.id=:sub_seccion_id";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":sub_seccion_id", $sub_seccion_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }
    
    public function getCantidadPreguntas($seccionPruebaId) {
        $sql = "SELECT COUNT(1) FROM gprueba.pregunta WHERE seccion_prueba_id = :seccionPruebaId";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":seccionPruebaId", $seccionPruebaId, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();
        return $resultado;
    }

}
