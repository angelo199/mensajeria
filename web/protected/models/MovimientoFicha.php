<?php

/**
 * This is the model class for table "movimientos.movimiento_ficha".
 *
 * The followings are the available columns in table 'movimientos.movimiento_ficha':
 * @property integer $id
 * @property integer $personal_id
 * @property string $fecha_solicitud
 * @property string $fecha_entrega
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_ini
 * @property integer $departamento_id
 * @property integer $departamento_id_entrega
 * @property string $estatus
 * @property integer $funcionario_id
 *
 * The followings are the available model relations:
 * @property Departamento $departamentoIdEntrega
 * @property Departamento $departamento
 * @property Personal $personal
 * @property Funcionario $funcionario
 */
class MovimientoFicha extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movimientos.movimiento_ficha';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('personal_id, usuario_ini_id, usuario_act_id, departamento_id, departamento_id_entrega, funcionario_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('fecha_solicitud, fecha_entrega, fecha_act, fecha_ini', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, personal_id, fecha_solicitud, fecha_entrega, usuario_ini_id, usuario_act_id, fecha_act, fecha_ini, departamento_id, departamento_id_entrega, estatus, funcionario_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'departamentoIdEntrega' => array(self::BELONGS_TO, 'Departamento', 'departamento_id_entrega'),
			'departamento' => array(self::BELONGS_TO, 'Departamento', 'departamento_id'),
			'personal' => array(self::BELONGS_TO, 'Personal', 'personal_id'),
			'funcionario' => array(self::BELONGS_TO, 'Funcionario', 'funcionario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'personal_id' => 'Personal',
			'fecha_solicitud' => 'Fecha Solicitud',
			'fecha_entrega' => 'Fecha Entrega',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_ini' => 'Fecha Ini',
			'departamento_id' => 'Departamento',
			'departamento_id_entrega' => 'Departamento Id Entrega',
			'estatus' => 'Estatus',
			'funcionario_id' => 'Funcionario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('personal_id',$this->personal_id);
		$criteria->compare('fecha_solicitud',$this->fecha_solicitud,true);
		$criteria->compare('fecha_entrega',$this->fecha_entrega,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('departamento_id',$this->departamento_id);
		$criteria->compare('departamento_id_entrega',$this->departamento_id_entrega);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('funcionario_id',$this->funcionario_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MovimientoFicha the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
