<?php

/**
 * This is the model class for table "gprueba.prueba".
 *
 * The followings are the available columns in table 'gprueba.prueba':
 * @property string $id
 * @property string $nombre
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 * @property text $instruccion_prueba
 * @property integer $tipo_prueba
 *
 * The followings are the available model relations:
 * @property SeccionPrueba[] $seccionPruebas
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 */
class Prueba extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gprueba.prueba';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, tipo_prueba', 'required'),
            array('usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly' => true),
            array('nombre', 'length', 'max' => 150),
            //array('fecha_ini, fecha_act, fecha_elim', 'length', 'max'=>6),
            array('estatus', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus, instruccion_prueba', 'safe', 'on' => 'search'),
            array('id, nombre, instruccion_prueba, tipo_prueba, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'seccionPruebas' => array(self::HAS_MANY, 'SeccionPrueba', 'prueba_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre de Prueba',
            'instruccion_prueba' => 'Instruciones',
            'tipo_prueba' => 'Prueba Cualitativa/Cuantitativa',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'usuario_elim_id' => 'Usuario Elim',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'instruccion_prueba' => 'Instrucciones'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->addSearchCondition('nombre', '%' . $this->nombre . '%', false, 'AND', 'ILIKE'); //Busqueda en el catalogo de tipo de prueba
        if ($this->estatus === 'A' || $this->estatus === 'I') {
            $criteria->compare('estatus', $this->estatus);
        }
        $sort = new CSort();
        $sort->defaultOrder = 'estatus ASC, nombre ASC';
        //Va a ordenar la tabla utilizando el campo id_representacion en forma descendente "DESC",

        return new CActiveDataProvider(get_class($this), array(
            'pagination' => array(
                'pageSize' => 10
            ),
            'criteria' => $criteria,
            'sort' => $sort,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Prueba the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function estatusPrueba($idSeccion, $variable) {
        if ($variable == 'S') {
            $sql = "SELECT p.estatus
                            FROM gprueba.prueba p
                            INNER JOIN gprueba.seccion_prueba sp ON (sp.prueba_id = p.id)
                            WHERE sp.id = :idSeccion ";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":idSeccion", $idSeccion, PDO::PARAM_INT);
            $resultado = $consulta->queryScalar();

            return $resultado;
        } elseif ($variable == 'SS') {
            $sql = "SELECT p.estatus
                            FROM gprueba.prueba p
                            INNER JOIN gprueba.seccion_prueba sp ON (sp.prueba_id = p.id)
                            INNER JOIN gprueba.sub_seccion_prueba ssp ON (ssp.seccion_prueba_id = sp.id)
                            WHERE ssp.id = :idSubSeccion ";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":idSubSeccion", $idSeccion, PDO::PARAM_INT);
            $resultado = $consulta->queryScalar();

            return $resultado;
        }
    }

    public function datosCompletosPrueba($prueba_id) {

        $estatus = 'A';
        $sql = "SELECT s.id AS seccion_id, s.prueba_id, s.nombre AS nombre_seccion, s.duracion_seccion, s.cantidad_respuesta, s.tipo_pregunta, s.cantidad_sub_seccion, s.instruccion_seccion,
                    COALESCE(ss.id, null) AS sub_seccion_id, COALESCE(ss.tipo_pregunta,'') AS tipo_pregunta_sub_seccion, COALESCE(ss.nombre,'') AS nombre_sub_seccion, COALESCE(ss.duracion_seccion,null) AS duracion_seccion, COALESCE(ss.cantidad_respuesta,null) AS sub_cantidad_respuesta, COALESCE(ss.seccion_prueba_id,null) AS s_seccion_prueba_id, COALESCE(ss.instruccion_sub_seccion,'') AS instruccion_sub_seccion,
                    COALESCE(p.id, null) AS pregunta_id, COALESCE(p.nombre, '') AS nombre_pregunta, COALESCE(p.seccion_prueba_id, null) AS preg_seccion_prueba_id, COALESCE(p.sub_seccion_prueba_id, null) AS preg_sub_seccion_prueba_id,
                    COALESCE(r.id, null) AS respuesta_id, COALESCE(r.nombre, '') AS nombre_respuesta, COALESCE(r.resp_correcta, null) AS resp_correcta, COALESCE(r.pregunta_id, null) AS resp_pregunta_id,
                    pr.nombre AS nombre_prueba
                   FROM gprueba.prueba pr
                   INNER JOIN gprueba.seccion_prueba s ON (s.prueba_id = pr.id)
                   INNER JOIN gprueba.sub_seccion_prueba ss ON (ss.seccion_prueba_id = s.id)
                   INNER JOIN gprueba.pregunta p ON (p.seccion_prueba_id = s.id AND p.sub_seccion_prueba_id = ss.id)
                   INNER JOIN gprueba.respuesta r ON (r.pregunta_id = p.id)
                   WHERE pr.id = :prueba_id
                   AND pr.estatus=:estatus
                   AND s.estatus=:estatus
                   AND ss.estatus=:estatus
                   AND p.estatus=:estatus
                   AND r.estatus=:estatus
              UNION
                   (SELECT s.id AS seccion_id, s.prueba_id, s.nombre AS nombre_seccion, s.duracion_seccion, s.cantidad_respuesta, s.tipo_pregunta, s.cantidad_sub_seccion, s.instruccion_seccion,
                    COALESCE(ss.id, null) AS sub_seccion_id, COALESCE(ss.tipo_pregunta,'') AS tipo_pregunta_sub_seccion, COALESCE(ss.nombre,'') AS nombre_sub_seccion, COALESCE(ss.duracion_seccion,null) AS sub_duracion_seccion, COALESCE(ss.cantidad_respuesta,null) AS sub_cantidad_respuesta, COALESCE(ss.seccion_prueba_id,null) AS s_seccion_prueba_id, COALESCE(ss.instruccion_sub_seccion,'') AS instruccion_sub_seccion,
                    COALESCE(p.id, null) AS pregunta_id, COALESCE(p.nombre, '') AS nombre_pregunta, COALESCE(p.seccion_prueba_id, null), COALESCE(p.sub_seccion_prueba_id, null),
                    COALESCE(r.id, null) AS respuesta_id, COALESCE(r.nombre, '') AS nombre_respuesta, COALESCE(r.resp_correcta, null), COALESCE(r.pregunta_id, null) AS pregunta_id,
                   pr.nombre AS nombre_prueba
                    FROM gprueba.prueba pr
                   INNER JOIN gprueba.seccion_prueba s ON (s.prueba_id = pr.id)
                   LEFT JOIN gprueba.sub_seccion_prueba ss ON (ss.seccion_prueba_id = s.id)
                   LEFT JOIN gprueba.pregunta p ON (p.sub_seccion_prueba_id = ss.id)
                   LEFT JOIN gprueba.respuesta r ON (r.pregunta_id = p.id)
                   WHERE pr.id = :prueba_id
                   AND p.sub_seccion_prueba_id IS NOT NULL
                   AND pr.estatus=:estatus
                   AND s.estatus=:estatus
                   AND ss.estatus=:estatus
                   AND p.estatus=:estatus
                   AND r.estatus=:estatus)
                UNION
                   (SELECT s.id AS seccion_id, s.prueba_id, s.nombre AS nombre_seccion, s.duracion_seccion, s.cantidad_respuesta, s.tipo_pregunta, s.cantidad_sub_seccion, s.instruccion_seccion,
                     null AS sub_seccion_id, '' AS tipo_pregunta_sub_seccion, '' AS nombre_sub_seccion, '00:00:00' AS s_duracion_seccion, null AS sub_cantidad_respuesta, null AS s_seccion_prueba_id, '' AS instruccion_sub_seccion,
                    COALESCE(p.id, null) AS pregunta_id, COALESCE(p.nombre, '') AS nombre_pregunta, COALESCE(p.seccion_prueba_id, null) AS preg_seccion_prueba_id, COALESCE(p.sub_seccion_prueba_id, null) AS preg_sub_seccion_prueba_id,
                    COALESCE(r.id, null) AS respuesta_id, COALESCE(r.nombre, '') AS nombre_respuesta, COALESCE(r.resp_correcta, null) AS resp_correcta, COALESCE(r.pregunta_id, null) AS resp_pregunta_id,
                    pr.nombre AS nombre_prueba
                   FROM gprueba.prueba pr
                   INNER JOIN gprueba.seccion_prueba s ON (s.prueba_id = pr.id)
                   INNER JOIN gprueba.pregunta p ON (p.seccion_prueba_id = s.id)
                   LEFT JOIN gprueba.respuesta r ON (r.pregunta_id = p.id)
                   WHERE pr.id = :prueba_id
                   AND p.sub_seccion_prueba_id IS null
                   AND pr.estatus=:estatus
                   AND s.estatus=:estatus
                   AND p.estatus=:estatus
                   AND r.estatus=:estatus)
                   ORDER BY nombre_seccion ASC, nombre_sub_seccion ASC, nombre_pregunta ASC, respuesta_id ASC
                   ";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
        $result = $consulta->queryAll();
        return $result;
    }

    public function pruebaActivas() {
        $fecha = date('Y') . '-01-01 00:00:00';
        $sql = "SELECT DISTINCT p.nombre AS nombre_prueba, p.id AS prueba_id
                            FROM gprueba.aplicacion_prueba ap, gprueba.prueba p
                            INNER JOIN gprueba.estatus_iniciar_prueba ep ON (ep.prueba_id = p.id)
	    WHERE p.id NOT IN (SELECT DISTINCT prueba_id from gprueba.aplicacion_prueba
                                WHERE substring(fecha_ini::character varying,0,5)=substring(now()::character varying,0,5))
                            AND ep.estatus='A'
                            AND substring(ap.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":fecha", $fecha, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
        // $this->verificarAplicacionPrueba($resultado);
    }

    public function datosAplicadorPrueba($prueba_id, $aspirante_id, $aplicacion_id) {
        $sql = "SELECT u.nombre AS nombre, u.apellido AS apellido, a.fecha_ini As fecha
                    FROM gprueba.aplicacion_prueba a
                    INNER JOIN seguridad.usergroups_user u ON (u.id = a.responsable_aplicar_prueba_id)
                    WHERE a.prueba_id = :prueba_id
                    AND a.aspirante_id = :aspirante_id
                    AND a.id = :aplicacion_id";
        $guardar = $this->getDbConnection()->createCommand($sql);
        $guardar->bindParam(':prueba_id', $prueba_id, PDO::PARAM_INT);
        $guardar->bindParam(':aspirante_id', $aspirante_id, PDO::PARAM_INT);
        $guardar->bindParam(':aplicacion_id', $aplicacion_id, PDO::PARAM_INT);
        $result = $guardar->queryAll();

        return $result;
    }

    public function verificarPrueba($estatus, $tipo_prueba) {
        $sql = "SELECT DISTINCT p.id, p.nombre
                   FROM gprueba.prueba p
                   INNER JOIN gprueba.seccion_prueba sp ON (sp.prueba_id = p.id)
                   INNER JOIN gprueba.pregunta p_ ON (p_.seccion_prueba_id = sp.id)
                   INNER JOIN gprueba.respuesta r ON (r.pregunta_id = p_.id)
                   WHERE p.tipo_prueba = :tipo_prueba
                   AND p.estatus = :estatus
                   AND sp.estatus = :estatus
                   AND p_.estatus = :estatus
                   AND r.estatus = :estatus
                   GROUP BY p.id, p.nombre
                   ORDER BY p.nombre";
        $guardar = $this->getDbConnection()->createCommand($sql);
        $guardar->bindParam(':estatus', $estatus, PDO::PARAM_STR);
        $guardar->bindParam(':tipo_prueba', $tipo_prueba, PDO::PARAM_INT);
        $result = $guardar->queryAll();

        return $result;
    }

}
