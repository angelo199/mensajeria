<?php

/**
 * This is the model class for table "gprueba.sub_seccion_prueba".
 *
 * The followings are the available columns in table 'gprueba.sub_seccion_prueba':
 * @property string $id
 * @property string $nombre
 * @property string $duracion_seccion
 * @property integer $cantidad_respuesta
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $seccion_prueba_id
 * @property string $instruccion_sub_seccion
 * @property string $tipo_pregunta
 *
 * The followings are the available model relations:
 * @property Pregunta[] $preguntas
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioAct
 * @property SeccionPrueba $seccionPrueba
 */
class SubSeccionPrueba extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gprueba.sub_seccion_prueba';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, duracion_seccion, cantidad_respuesta, usuario_ini_id, fecha_ini, estatus, seccion_prueba_id, instruccion_sub_seccion, tipo_pregunta', 'required'),
			array('cantidad_respuesta, usuario_ini_id, usuario_act_id, usuario_elim_id, seccion_prueba_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>150),
			array('estatus, tipo_pregunta', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, duracion_seccion, cantidad_respuesta, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus, seccion_prueba_id, instruccion_sub_seccion, tipo_pregunta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'preguntas' => array(self::HAS_MANY, 'Pregunta', 'sub_seccion_prueba_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'seccionPrueba' => array(self::BELONGS_TO, 'SeccionPrueba', 'seccion_prueba_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'duracion_seccion' => 'Duracion Seccion',
			'cantidad_respuesta' => 'Cantidad Respuesta',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'usuario_elim_id' => 'Usuario Elim',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
			'seccion_prueba_id' => 'Seccion Prueba',
			'instruccion_sub_seccion' => 'Instruccion Sub Seccion',
			'tipo_pregunta' => 'Tipo Pregunta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->id)>0) $criteria->compare('id',$this->id,true);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);
		if(strlen($this->duracion_seccion)>0) $criteria->compare('duracion_seccion',$this->duracion_seccion,true);
		if(is_numeric($this->cantidad_respuesta)) $criteria->compare('cantidad_respuesta',$this->cantidad_respuesta);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(is_numeric($this->usuario_elim_id)) $criteria->compare('usuario_elim_id',$this->usuario_elim_id);
		if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(is_numeric($this->seccion_prueba_id)) $criteria->compare('seccion_prueba_id',$this->seccion_prueba_id);
		if(strlen($this->instruccion_sub_seccion)>0) $criteria->compare('instruccion_sub_seccion',$this->instruccion_sub_seccion,true);
		if(strlen($this->tipo_pregunta)>0) $criteria->compare('tipo_pregunta',$this->tipo_pregunta,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SubSeccionPrueba the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
