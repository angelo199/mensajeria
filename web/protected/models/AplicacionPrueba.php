<?php

/**
 * This is the model class for table "gprueba.aplicacion_prueba".
 *
 * The followings are the available columns in table 'gprueba.aplicacion_prueba':
 * @property string $id
 * @property integer $responsable_aplicar_prueba_id
 * @property integer $aspirante_id
 * @property integer $prueba_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $estatus_prueba_aplicada
 * @property string $hora_inicio_prueba
 * @property string $hora_final_prueba
 * @property integer $seccion_prueba_id
 * @property string $puntuacion_prueba
 * @property string $hora_final_real
 * @property integer $cantidad_respuestas_correctas
 * @property integer $cantidad_respuestas_incorrectas
 * @property string $finalizada_por
 * @property string $data_pregunta_respuesta
 *
 * The followings are the available model relations:
 * @property AperturaPeriodo $prueba
 * @property UsergroupsUser $responsableAplicarPrueba
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 * @property SeccionPrueba $seccionPrueba
 * @property TalentoHumano $aspirante
 * @property RespuestaPruebaAsp[] $respuestaPruebaAsps
 */
class AplicacionPrueba extends CActiveRecord {

    public static $cacheIndexById = 'APLICACION-PRUEBA:{id}';
    
    const INICIALIZADA = 1;
    const FINALIZADA = 2;
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gprueba.aplicacion_prueba';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('responsable_aplicar_prueba_id, aspirante_id, prueba_id, usuario_ini_id, fecha_ini, estatus_prueba_aplicada, hora_inicio_prueba, hora_final_prueba, seccion_prueba_id', 'required'),
            array('responsable_aplicar_prueba_id, aspirante_id, prueba_id, usuario_ini_id, usuario_act_id, usuario_elim_id, estatus_prueba_aplicada, seccion_prueba_id, cantidad_respuestas_correctas, cantidad_respuestas_incorrectas', 'numerical', 'integerOnly'=>true),
            array('estatus', 'length', 'max'=>1),
            array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
            array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
            array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
            array('puntuacion_prueba, hora_final_real, finalizada_por, data_pregunta_respuesta', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, responsable_aplicar_prueba_id, aspirante_id, prueba_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus, estatus_prueba_aplicada, hora_inicio_prueba, hora_final_prueba, seccion_prueba_id, puntuacion_prueba, hora_final_real, cantidad_respuestas_correctas, cantidad_respuestas_incorrectas, finalizada_por, data_pregunta_respuesta', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'prueba' => array(self::BELONGS_TO, 'AperturaPeriodo', 'prueba_id'),
            'responsableAplicarPrueba' => array(self::BELONGS_TO, 'UsergroupsUser', 'responsable_aplicar_prueba_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'seccionPrueba' => array(self::BELONGS_TO, 'SeccionPrueba', 'seccion_prueba_id'),
            'aspirante' => array(self::BELONGS_TO, 'TalentoHumano', 'aspirante_id'),
            'respuestaPruebaAsps' => array(self::HAS_MANY, 'RespuestaPruebaAsp', 'aplicacion_prueba_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'responsable_aplicar_prueba_id' => 'Responsable Aplicar Prueba',
            'aspirante_id' => 'Aspirante',
            'prueba_id' => 'Prueba',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'usuario_elim_id' => 'Usuario Elim',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'estatus_prueba_aplicada' => 'Estatus Prueba Aplicada',
            'hora_inicio_prueba' => 'Hora Inicio Prueba',
            'hora_final_prueba' => 'Hora Final Prueba',
            'seccion_prueba_id' => 'Seccion Prueba',
            'puntuacion_prueba' => 'Puntuacion Prueba',
            'hora_final_real' => 'Hora Final Real',
            'cantidad_respuestas_correctas' => 'Cantidad Respuestas Correctas',
            'cantidad_respuestas_incorrectas' => 'Cantidad Respuestas Incorrectas',
            'finalizada_por' => 'Finalizada Por',
            'data_pregunta_respuesta' => 'Data Pregunta Respuesta',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        if(strlen($this->id)>0) $criteria->compare('id',$this->id,true);
        if(is_numeric($this->responsable_aplicar_prueba_id)) $criteria->compare('responsable_aplicar_prueba_id',$this->responsable_aplicar_prueba_id);
        if(is_numeric($this->aspirante_id)) $criteria->compare('aspirante_id',$this->aspirante_id);
        if(is_numeric($this->prueba_id)) $criteria->compare('prueba_id',$this->prueba_id);
        if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
        if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if(is_numeric($this->usuario_elim_id)) $criteria->compare('usuario_elim_id',$this->usuario_elim_id);
        if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
        // if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
        if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
        if(is_numeric($this->estatus_prueba_aplicada)) $criteria->compare('estatus_prueba_aplicada',$this->estatus_prueba_aplicada);
        if(strlen($this->hora_inicio_prueba)>0) $criteria->compare('hora_inicio_prueba',$this->hora_inicio_prueba,true);
        if(strlen($this->hora_final_prueba)>0) $criteria->compare('hora_final_prueba',$this->hora_final_prueba,true);
        if(is_numeric($this->seccion_prueba_id)) $criteria->compare('seccion_prueba_id',$this->seccion_prueba_id);
        if(strlen($this->puntuacion_prueba)>0) $criteria->compare('puntuacion_prueba',$this->puntuacion_prueba,true);
        if(strlen($this->hora_final_real)>0) $criteria->compare('hora_final_real',$this->hora_final_real,true);
        if(is_numeric($this->cantidad_respuestas_correctas)) $criteria->compare('cantidad_respuestas_correctas',$this->cantidad_respuestas_correctas);
        if(is_numeric($this->cantidad_respuestas_incorrectas)) $criteria->compare('cantidad_respuestas_incorrectas',$this->cantidad_respuestas_incorrectas);
        if(strlen($this->finalizada_por)>0) $criteria->compare('finalizada_por',$this->finalizada_por,true);
        if(strlen($this->data_pregunta_respuesta)>0) $criteria->compare('data_pregunta_respuesta',$this->data_pregunta_respuesta,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AplicacionPrueba the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function finalizarPrueba($prueba_id, $aspirante_id) {

        (int) $usuario_id = Yii::app()->user->id;
        (string) $fecha = date('Y-m-d H:i:s');
        (string) $estatus = 'I';
        (int) $estatus_prueba_aplicada = 2;
        $prueba_id = (int) $prueba_id;
        $aspirante_id = (int) $aspirante_id;
        $sql = "UPDATE gprueba.aplicacion_prueba
                                  SET   estatus_prueba_aplicada=:estatus_aplicada,
                                  usuario_act_id=:usuario_id,
                                  fecha_act=:fecha,
                                  estatus=:estatus
                                  WHERE prueba_id=:prueba
                                  AND aspirante_id=:aspirante
                                  AND estatus='A'
                                  AND substring(fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)";
        $commandBusqueda = Yii::app()->db->createCommand($sql);
        $commandBusqueda->bindParam(':aspirante', $aspirante_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':prueba', $prueba_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':usuario_id', $usuario_id, PDO::PARAM_INT);
        $commandBusqueda->bindParam(':fecha', $fecha, PDO::PARAM_STR);
        $commandBusqueda->bindParam(':estatus', $estatus, PDO::PARAM_STR);
        $commandBusqueda->bindParam(':estatus_aplicada', $estatus_prueba_aplicada, PDO::PARAM_INT);

        $resultado = $commandBusqueda->execute();
        return $resultado;
    }

    function verificarAspirantePresentando($prueba_id) {
        if ($prueba_id != '') {
            $sql = "SELECT ap.hora_final_prueba, ap.fecha_ini
                        FROM gprueba.aplicacion_prueba ap
                        INNER JOIN gprueba.prueba p ON(p.id = ap.prueba_id)
                                  WHERE ap.prueba_id=:prueba
                                  AND ap.estatus='A'
                                  AND substring(ap.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)
                                 ORDER BY ap.hora_final_prueba DESC
                                 LIMIT 1";
            $commandBusqueda = Yii::app()->db->createCommand($sql);
            $commandBusqueda->bindParam(':prueba', $prueba_id, PDO::PARAM_INT);

            $resultado = $commandBusqueda->queryRow();
            return $resultado;
        }

    }
    
    /**
     * 
     * @param type $aplicacionPruebaId
     * @param type $dataPreguntaRespuesta
     * @param type $preguntas
     * @param type $respuestas
     * @param type $aspiranteId
     * @param type $aperturaPeriodoId
     * @param type $pruebaId
     * @param type $calificacion
     * @param type $preguntasCorrectas
     * @param type $preguntasIncorrectas
     * @param type $finalizadaPor
     * @param type $horaFinalPrueba
     * @param type $postulacionId
     * @param type $usuarioIni
     * @param type $fechaAct
     * @return type
     */
    public function spRegistroResultadoPrueba($aplicacionPruebaId, $dataPreguntaRespuesta,  $preguntas, $respuestas, $aspiranteId, $aperturaPeriodoId, $pruebaId, $calificacion, $preguntasCorrectas, $preguntasIncorrectas, $finalizadaPor, $horaFinalPrueba, $postulacionId, $usuarioIni, $fechaAct){
      
        $sql= "SELECT * FROM gprueba.capturar_respuestas_prueba_asp(
                   :aplicacion_prueba_id_vi::INTEGER,
                   ".Helper::toPgArray($preguntas)."::BIGINT[],
                   ".Helper::toPgArray($respuestas)."::BIGINT[],
                   :data_pregunta_respuesta_vi::TEXT,
                   :aspirante_id_vi::BIGINT,
                   :apertura_periodo_id_vi::INTEGER,
                   :seccion_prueba_id_vi::INTEGER,
                   :calificacion_final_vi::NUMERIC,
                   :cantidad_respuestas_correctas_vi::SMALLINT, 
                   :cantidad_respuestas_incorrectas_vi::SMALLINT, 
                   :finalizada_por_vi::CHARACTER VARYING, 
                   :hora_finalizada_real_vi::TIME,
                   :postulacion_id_vi::BIGINT,
                   :usuario_ini_id_vi::BIGINT,
                   :fecha_act_vi::TIMESTAMP
        ) AS f ( 
             mensaje_sistema VARCHAR, 
             mensaje_usuario VARCHAR, 
             estatus VARCHAR,
             seccion SMALLINT,
             estatus_postulacion SMALLINT
        )"; 

        $conection = Yii::app()->db;
        $query = $conection->createCommand($sql);
        
        $query->bindParam(':aplicacion_prueba_id_vi', $aplicacionPruebaId, PDO::PARAM_INT);       
        $query->bindParam(':aspirante_id_vi', $aspiranteId, PDO::PARAM_INT);
        $query->bindParam(':data_pregunta_respuesta_vi', $dataPreguntaRespuesta, PDO::PARAM_STR);
        $query->bindParam(':apertura_periodo_id_vi', $aperturaPeriodoId, PDO::PARAM_INT);
        $query->bindParam(':seccion_prueba_id_vi', $pruebaId, PDO::PARAM_INT);
        $query->bindParam(':calificacion_final_vi', $calificacion, PDO::PARAM_INT);
        $query->bindParam(':cantidad_respuestas_correctas_vi', $preguntasCorrectas, PDO::PARAM_INT);
        $query->bindParam(':cantidad_respuestas_incorrectas_vi', $preguntasIncorrectas, PDO::PARAM_INT);
        $query->bindParam(':finalizada_por_vi', $finalizadaPor, PDO::PARAM_STR);
        $query->bindParam(':hora_finalizada_real_vi', $horaFinalPrueba, PDO::PARAM_STR);
        $query->bindParam(':postulacion_id_vi', $postulacionId, PDO::PARAM_INT);
        $query->bindParam(':usuario_ini_id_vi', $usuarioIni, PDO::PARAM_INT);
        $query->bindParam(':fecha_act_vi', $fechaAct, PDO::PARAM_STR);
        $queryResponse = $query->queryRow();
        
        return $queryResponse;
        
    }
    
    /**
     * 
     * @param int $id
     * @return object
     */
    public function loadAplicacionPrueba($id){
        $cacheIndex = strtr(self::$cacheIndexById, array('{id}'=>$id));
        $result = Yii::app()->cache->get($cacheIndex);
        if(!$result){
            $result = $this->findByPk($id);
            if($result && $result->estatus_prueba_aplicada == self::FINALIZADA){
                Yii::app()->cache->set($cacheIndex, $result, Helper::$SEGUNDOS_UN_MES);
            }
        }
        return $result;
    }
    
    public function printSpRegistroResultadoPrueba($aplicacionPruebaId, $dataPreguntaRespuesta,  $preguntas, $respuestas, $aspiranteId, $aperturaPeriodoId, $pruebaId, $calificacion, $preguntasCorrectas, $preguntasIncorrectas, $finalizadaPor, $horaFinalPrueba, $postulacionId, $usuarioIni, $fechaAct){
        
        $sql= "SELECT * FROM gprueba.capturar_respuestas_prueba_asp(
                   $aplicacionPruebaId::INTEGER,
                   ".Helper::toPgArray($preguntas)."::BIGINT[],
                   ".Helper::toPgArray($respuestas)."::BIGINT[],
                   '$dataPreguntaRespuesta'::TEXT,
                   $aspiranteId::BIGINT,
                   $aperturaPeriodoId::INTEGER,
                   $pruebaId::INTEGER,
                   $calificacion::NUMERIC,
                   $preguntasCorrectas::SMALLINT, 
                   $preguntasIncorrectas::SMALLINT, 
                   '$finalizadaPor'::CHARACTER VARYING, 
                   '$horaFinalPrueba'::TIME,
                   $postulacionId::BIGINT,
                   $usuarioIni::BIGINT,
                   '$fechaAct'::TIMESTAMP
        ) AS f ( 
             mensaje_sistema varchar, 
             mensaje_usuario varchar, 
             estatus varchar,
             seccion INT
        )"; 
        print_r($sql);
    }
    
    public function beforeInsert() {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

}
