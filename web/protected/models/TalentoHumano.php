<?php

/**
 * This is the model class for table "gestion_humana.talento_humano".
 *
 * The followings are the available columns in table 'gestion_humana.talento_humano':
 * @property string $id
 * @property string $user_id
 * @property string $origen
 * @property integer $cedula
 * @property string $rif
 * @property string $nombre
 * @property string $apellido
 * @property string $sexo
 * @property string $fecha_nacimiento
 * @property string $codigo_empleado
 * @property string $foto
 * @property string $telefono_fijo
 * @property string $telefono_celular
 * @property string $telefono_oficina
 * @property string $email_personal
 * @property string $email_corporativo
 * @property string $twitter
 * @property string $linkedin
 * @property integer $grado_instruccion_actual_id
 * @property integer $estado_id
 * @property integer $estado_dependencia_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $direccion
 * @property string $enfermedades
 * @property string $aptitudes
 * @property integer $mision_id
 * @property string $fecha_ingreso
 * @property integer $cobertura_id
 * @property integer $condicion_actual_id
 * @property integer $categoria_cargo_actual_id
 * @property integer $cargo_actual_id
 * @property integer $tipo_nomina_id
 * @property integer $dependencia_id
 * @property integer $plantel_id
 * @property string $verificado_saime
 * @property integer $diversidad_funcional_id
 * @property integer $etnia_id
 * @property string $lateralidad
 * @property integer $traslado
 * @property string $tipo_personal
 * @property string $condicion
 * @property string $profesion
 * @property string $cargo
 * @property string $zona_educativa
 * @property string $dependencia
 * @property string $numero_ivss
 * @property integer $tipo_serial_cuenta_id
 * @property integer $tipo_cuenta_id
 * @property integer $banco_id
 * @property string $numero_cuenta
 * @property string $origen_talento_humano
 * @property integer $cedula_talento_humano
 * @property string $nombre_talento_humano
 * @property string $fecha_ultimo_concurso
 * @property string $fecha_ultimo_merito
 * @property string $fecha_registro_egreso
 * @property string $fecha_egreso
 * @property string $entrevista_realizada
 * @property string $observacion_entrevista
 * @property string $busqueda
 * @property string $codigo_integridad
 * @property string $observacion
 * @property string $usuario_ini_id
 * @property string $fecha_ini
 * @property string $ip_ini
 * @property string $usuario_act_id
 * @property string $fecha_act
 * @property string $ip_act
 * @property string $estatus
 * @property string $estado_dependencia
 *
 * The followings are the available model relations:
 * @property ExperienciaLaboral[] $experienciaLaborals
 * @property EstudioRealizado[] $estudioRealizados
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $user
 * @property TipoNomina $tipoNomina
 * @property Parroquia $parroquia
 * @property Municipio $municipio
 * @property Mision $mision
 * @property GradoInstruccion $gradoInstruccionActual
 * @property Etnia $etnia
 * @property Estado $estado
 * @property DiversidadFuncional $diversidadFuncional
 * @property CondicionNominal $condicionActual
 * @property CategoriaIngreso $categoriaIngreso
 * @property CategoriaCargoNominal $categoriaCargoActual
 * @property CargoNominal $cargoActual
 * @property ReferenciaPersonal[] $referenciaPersonals
 */
class TalentoHumano extends CActiveRecord{
    
    
    public $cacheDatosPostulacion = 'TH:POST:{id}';
    public $cacheIndexSimple = 'TH:SIMPLE:{id}';
    public $cacheIndexSimpleByIdTh = 'TH:SIMPLE:TH:{id}';
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gestion_humana.talento_humano';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cedula, nombre, apellido, estado_civil,  fecha_nacimiento, estado_id, municipio_id, usuario_ini_id, fecha_ini, fecha_act, grado_instruccion_actual_id, direccion, telefono_fijo, telefono_celular, parroquia_id', 'required'),
            array('cedula, grado_instruccion_actual_id, estado_id, municipio_id, parroquia_id, mision_id, cobertura_id, condicion_actual_id, categoria_cargo_actual_id, cargo_actual_id, tipo_nomina_id, dependencia_id, plantel_id, diversidad_funcional_id, etnia_id, traslado, tipo_serial_cuenta_id, tipo_cuenta_id, banco_id, cedula_talento_humano, categoria_cargo_actual_id, estado_dependencia_id', 'numerical', 'integerOnly' => true),
            array('origen, sexo, lateralidad, origen_talento_humano, entrevista_realizada, estatus', 'length', 'max' => 1),
            array('rif, numero_ivss', 'length', 'max' => 20),
            array('nombre, apellido', 'length', 'max' => 50),
            array('codigo_empleado', 'length', 'max' => 15),
            array('estado_civil', 'length', 'max' => 1),
            array('estado_civil', 'in', 'range' => array('C', 'V', 'D', 'S'), 'allowEmpty' => false, 'strict' => true,),
            array('foto, linkedin, codigo_integridad', 'length', 'max' => 255),
            array('telefono_fijo, telefono_celular, telefono_oficina', 'length', 'max' => 14),
            array('email_personal, email_corporativo', 'length', 'max' => 120),
            array('twitter, nombre_talento_humano', 'length', 'max' => 40),
            array('urb_sector, urba_sector, av_calle', 'length', 'max' => 50),
            array('apartamento', 'length', 'max' => 15),
            array('piso', 'length', 'max' => 3),
            array('verificado_saime', 'length', 'max' => 2),
            array('numero_cuenta, casa_edificio', 'length', 'max' => 30),
            array('direccion', 'length', 'max' => 500),
            array('ip_ini, ip_act', 'length', 'max' => 180),
            array('origen', 'in', 'range' => array('V', 'E', 'P'), 'allowEmpty' => false, 'strict' => true,),
            array('estatus', 'in', 'range' => array('A', 'I', 'E'), 'allowEmpty' => false, 'strict' => true,),
            array('lateralidad', 'in', 'range' => array('D', 'Z'), 'allowEmpty' => true, 'strict' => true,),
            array('usuario_ini_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'insert'),
            array('usuario_act_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'update'),
            array('email_personal, email_corporativo', 'email'),
            array('user_id, direccion, enfermedades, aptitudes, tipo_personal, condicion, profesion, urb_sector, cargo, zona_educativa, dependencia, observacion_entrevista, busqueda, observacion, usuario_act_id, av_calle, casa_edificio, piso, apartamento, estado_id, categoria_cargo_actual_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, origen, cedula, rif, nombre, apellido, av_calle, casa_edificio, piso, apartamento, sexo, fecha_nacimiento, codigo_empleado, foto, telefono_fijo, telefono_celular, telefono_oficina, email_personal, email_corporativo, twitter, linkedin, grado_instruccion_actual_id, estado_id, municipio_id, parroquia_id, direccion, enfermedades, aptitudes, mision_id, fecha_ingreso, cobertura_id, condicion_actual_id, categoria_cargo_actual_id, cargo_actual_id, tipo_nomina_id, dependencia_id, plantel_id, verificado_saime, diversidad_funcional_id, etnia_id, lateralidad, traslado, tipo_personal, condicion, profesion, cargo, zona_educativa, dependencia, numero_ivss, tipo_serial_cuenta_id, tipo_cuenta_id, banco_id, numero_cuenta, origen_talento_humano, cedula_talento_humano, nombre_talento_humano, fecha_ultimo_concurso, fecha_ultimo_merito, fecha_registro_egreso, fecha_egreso, entrevista_realizada, observacion_entrevista, busqueda, codigo_integridad, observacion, usuario_ini_id, fecha_ini, ip_ini, usuario_act_id, fecha_act, ip_act, categoria_cargo_actual_id, estatus, estado_dependencia_id, estado_dependencia, urba_sector', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'experienciaLaborals' => array(self::HAS_MANY, 'ExperienciaLaboral', 'talento_humano_id'),
            'estudioRealizados' => array(self::HAS_MANY, 'EstudioRealizado', 'talento_humano_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'user' => array(self::BELONGS_TO, 'UsergroupsUser', 'user_id'),
            'tipoNomina' => array(self::BELONGS_TO, 'TipoNomina', 'tipo_nomina_id'),
            'parroquia' => array(self::BELONGS_TO, 'Parroquia', 'parroquia_id'),
            'municipio' => array(self::BELONGS_TO, 'Municipio', 'municipio_id'),
            'mision' => array(self::BELONGS_TO, 'Mision', 'mision_id'),
            'gradoInstruccionActual' => array(self::BELONGS_TO, 'GradoInstruccion', 'grado_instruccion_actual_id'),
            'etnia' => array(self::BELONGS_TO, 'Etnia', 'etnia_id'),
            'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
            'estadoDependencia' => array(self::BELONGS_TO, 'Estado', 'estado_dependencia_id'),
            'diversidadFuncional' => array(self::BELONGS_TO, 'DiversidadFuncional', 'diversidad_funcional_id'),
            'condicionActual' => array(self::BELONGS_TO, 'CondicionNominal', 'condicion_actual_id'),
            'conbertura' => array(self::BELONGS_TO, 'CategoriaCargoNominal', 'categoria_cargo_actual_id'),
            'categoriaCargoActual' => array(self::BELONGS_TO, 'CategoriaCargoNominal', 'categoria_cargo_actual_id'),
            'cargoActual' => array(self::BELONGS_TO, 'CargoNominal', 'cargo_actual_id'),
            'referenciaPersonals' => array(self::HAS_MANY, 'ReferenciaPersonal', 'talento_humano_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'Usuario',
            'origen' => 'origen',
            'cedula' => 'Cédula',
            'rif' => 'RIF',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'sexo' => 'Sexo',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'codigo_empleado' => 'Código Empleado',
            'foto' => 'Foto',
            'telefono_fijo' => 'Teléfono Fijo',
            'telefono_celular' => 'Teléfono Celular',
            'telefono_oficina' => 'Teléfono de Oficina',
            'email_personal' => 'Email Personal',
            'email_corporativo' => 'Email Institucional',
            'twitter' => 'Twitter',
            'linkedin' => 'Linkedin',
            'grado_instruccion_actual_id' => 'Grado de Instrucción',
            'estado_id' => 'Estado',
            'estado_dependencia_id' => 'Estado de Dependencia',
            'estado_dependencia' => 'Estado de Dependencia',
            'municipio_id' => 'Municipio',
            'parroquia_id' => 'Parroquia',
            'direccion' => 'Dirección',
            'enfermedades' => 'Enfermedades',
            'aptitudes' => 'Aptitudes',
            'mision_id' => 'Mision',
            'fecha_ingreso' => 'Fecha de Ingreso',
            'cobertura_id' => 'Categoría Ingreso',
            'condicion_actual_id' => 'Condición Nominal Actual',
            'categoria_cargo_actual_id' => 'Categoría de Cargo Nominal Actual',
            'cargo_actual_id' => 'Cargo Nominal Actual',
            'tipo_nomina_id' => 'Tipo Nomina',
            'dependencia_id' => 'Dependencia',
            'urb_sector' => 'Urbanizaci&oacute;n o Sector',
            'av_calle' => 'Avenida / Calle',
            'casa_edificio' => 'Nro de Casa / Edificio',
            'plantel_id' => 'Plantel',
            'verificado_saime' => 'Verificado con el SAIME',
            'diversidad_funcional_id' => 'Diversidad Funcional',
            'etnia_id' => 'Etnia',
            'lateralidad' => 'Lateralidad',
            'traslado' => 'Traslado',
            'tipo_personal' => 'Tipo Personal',
            'condicion' => 'Condicion',
            'profesion' => 'Profesion',
            'cargo' => 'Cargo',
            'zona_educativa' => 'Zona Educativa',
            'dependencia' => 'Dependencia',
            'numero_ivss' => 'Numero Ivss',
            'tipo_serial_cuenta_id' => 'Tipo Serial de Cuenta',
            'tipo_cuenta_id' => 'Tipo de Cuenta',
            'banco_id' => 'Banco',
            'numero_cuenta' => 'Número de Cuenta',
            'origen_talento_humano' => 'Origen Talento Humano',
            'cedula_talento_humano' => 'Cedula Talento Humano',
            'nombre_talento_humano' => 'Nombre Talento Humano',
            'fecha_ultimo_concurso' => 'Fecha del Última Postulación a Concurso Público',
            'fecha_ultimo_merito' => 'Fecha del Última Postulación al Sistema de Mérito',
            'fecha_registro_egreso' => 'Fecha Registro Egreso',
            'fecha_egreso' => 'Fecha de Egreso',
            'entrevista_realizada' => 'Entrevista Realizada',
            'observacion_entrevista' => 'Observacion Entrevista',
            'busqueda' => 'Busqueda',
            'codigo_integridad' => 'Código Integridad',
            'observacion' => 'Observación',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'ip_ini' => 'Ip Ini',
            'usuario_act_id' => 'Último Usuario que efectuó una actualización de los datos del usuario',
            'fecha_act' => 'Fecha de la última actualización de datos',
            'ip_act' => 'Ip Act',
            'estatus' => 'Estatus',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        if (strlen($this->id) > 0)
            $criteria->compare('id', $this->id, true);
        if (strlen($this->user_id) > 0)
            $criteria->compare('user_id', $this->user_id, true);
        if (strlen($this->origen) > 0)
            $criteria->compare('origen', $this->origen, true);
        if (is_numeric($this->cedula))
            $criteria->addSearchCondition('cedula::varchar', $this->cedula . '%', false, 'AND', 'LIKE');
            //$criteria->compare('cedula::varchar', $this->cedula, true);
        if (strlen($this->rif) > 0)
            $criteria->compare('rif', $this->rif, true);
        if (strlen($this->nombre) > 0)
            $criteria->addSearchCondition('nombre', $this->nombre . '%', false, 'AND', 'ILIKE');
        if (strlen($this->apellido) > 0)
            $criteria->addSearchCondition('apellido', $this->apellido . '%', false, 'AND', 'ILIKE');
        if (strlen($this->sexo) > 0)
            $criteria->compare('sexo', $this->sexo, true);
        if (Utiles::isValidDate($this->fecha_nacimiento, 'y-m-d'))
            $criteria->compare('fecha_nacimiento', $this->fecha_nacimiento);
        // if(strlen($this->fecha_nacimiento)>0) $criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
        if (strlen($this->codigo_empleado) > 0)
            $criteria->compare('codigo_empleado', $this->codigo_empleado, true);
        if (strlen($this->foto) > 0)
            $criteria->compare('foto', $this->foto, true);
        if (strlen($this->telefono_fijo) > 0)
            $criteria->compare('telefono_fijo', $this->telefono_fijo, true);
        if (strlen($this->telefono_celular) > 0)
            $criteria->compare('telefono_celular', $this->telefono_celular, true);
        if (strlen($this->telefono_oficina) > 0)
            $criteria->compare('telefono_oficina', $this->telefono_oficina, true);
        if (strlen($this->email_personal) > 0)
            $criteria->compare('email_personal', $this->email_personal, true);
        if (strlen($this->email_corporativo) > 0)
            $criteria->compare('email_corporativo', $this->email_corporativo, true);
        if (strlen($this->twitter) > 0)
            $criteria->compare('twitter', $this->twitter, true);
        if (strlen($this->linkedin) > 0)
            $criteria->compare('linkedin', $this->linkedin, true);
        if (is_numeric($this->grado_instruccion_actual_id))
            $criteria->compare('grado_instruccion_actual_id', $this->grado_instruccion_actual_id);
        if (is_numeric($this->estado_id))
            $criteria->compare('estado_id', $this->estado_id);
        if (is_numeric($this->estado_dependencia_id))
            $criteria->compare('estado_dependencia_id', $this->estado_dependencia_id);
        if (is_numeric($this->municipio_id))
            $criteria->compare('municipio_id', $this->municipio_id);
        if (is_numeric($this->parroquia_id))
            $criteria->compare('parroquia_id', $this->parroquia_id);
        if (strlen($this->direccion) > 0)
            $criteria->compare('direccion', $this->direccion, true);
        if (strlen($this->enfermedades) > 0)
            $criteria->compare('enfermedades', $this->enfermedades, true);
        if (strlen($this->aptitudes) > 0)
            $criteria->compare('aptitudes', $this->aptitudes, true);
        if (is_numeric($this->mision_id))
            $criteria->compare('mision_id', $this->mision_id);
        if (Utiles::isValidDate($this->fecha_ingreso, 'y-m-d'))
            $criteria->compare('fecha_ingreso', $this->fecha_ingreso);
        // if(strlen($this->fecha_ingreso)>0) $criteria->compare('fecha_ingreso',$this->fecha_ingreso,true);
        if (is_numeric($this->cobertura_id))
            $criteria->compare('cobertura_id', $this->cobertura_id);
        if (is_numeric($this->condicion_actual_id))
            $criteria->compare('condicion_actual_id', $this->condicion_actual_id);
        if (is_numeric($this->categoria_cargo_actual_id))
            $criteria->compare('categoria_cargo_actual_id', $this->categoria_cargo_actual_id);
        if (is_numeric($this->cargo_actual_id))
            $criteria->compare('cargo_actual_id', $this->cargo_actual_id);
        if (is_numeric($this->tipo_nomina_id))
            $criteria->compare('tipo_nomina_id', $this->tipo_nomina_id);
        if (is_numeric($this->dependencia_id))
            $criteria->compare('dependencia_id', $this->dependencia_id);
        if (is_numeric($this->plantel_id))
            $criteria->compare('plantel_id', $this->plantel_id);
        if (strlen($this->verificado_saime) > 0)
            $criteria->compare('verificado_saime', $this->verificado_saime, true);
        if (is_numeric($this->diversidad_funcional_id))
            $criteria->compare('diversidad_funcional_id', $this->diversidad_funcional_id);
        if (is_numeric($this->etnia_id))
            $criteria->compare('etnia_id', $this->etnia_id);
        if (strlen($this->lateralidad) > 0)
            $criteria->compare('lateralidad', $this->lateralidad, true);
        if (is_numeric($this->traslado))
            $criteria->compare('traslado', $this->traslado);
        if (strlen($this->tipo_personal) > 0)
            $criteria->compare('tipo_personal', $this->tipo_personal, true);
        if (strlen($this->condicion) > 0)
            $criteria->compare('condicion', $this->condicion, true);
        if (strlen($this->profesion) > 0)
            $criteria->compare('profesion', $this->profesion, true);
        if (strlen($this->cargo) > 0)
            $criteria->compare('cargo', $this->cargo, true);
        if (strlen($this->zona_educativa) > 0)
            $criteria->compare('zona_educativa', $this->zona_educativa, true);
        if (strlen($this->dependencia) > 0)
            $criteria->compare('dependencia', $this->dependencia, true);
        if (strlen($this->numero_ivss) > 0)
            $criteria->compare('numero_ivss', $this->numero_ivss, true);
        if (is_numeric($this->tipo_serial_cuenta_id))
            $criteria->compare('tipo_serial_cuenta_id', $this->tipo_serial_cuenta_id);
        if (is_numeric($this->tipo_cuenta_id))
            $criteria->compare('tipo_cuenta_id', $this->tipo_cuenta_id);
        if (is_numeric($this->banco_id))
            $criteria->compare('banco_id', $this->banco_id);
        if (strlen($this->numero_cuenta) > 0)
            $criteria->compare('numero_cuenta', $this->numero_cuenta, true);
        if (strlen($this->origen_talento_humano) > 0)
            $criteria->compare('origen_talento_humano', $this->origen_talento_humano, true);
        if (is_numeric($this->cedula_talento_humano))
            $criteria->compare('cedula_talento_humano', $this->cedula_talento_humano);
        if (strlen($this->nombre_talento_humano) > 0)
            $criteria->compare('nombre_talento_humano', $this->nombre_talento_humano, true);
        if (Utiles::isValidDate($this->fecha_ultimo_concurso, 'y-m-d'))
            $criteria->compare('fecha_ultimo_concurso', $this->fecha_ultimo_concurso);
        // if(strlen($this->fecha_ultimo_concurso)>0) $criteria->compare('fecha_ultimo_concurso',$this->fecha_ultimo_concurso,true);
        if (Utiles::isValidDate($this->fecha_ultimo_merito, 'y-m-d'))
            $criteria->compare('fecha_ultimo_merito', $this->fecha_ultimo_merito);
        // if(strlen($this->fecha_ultimo_merito)>0) $criteria->compare('fecha_ultimo_merito',$this->fecha_ultimo_merito,true);
        if (Utiles::isValidDate($this->fecha_registro_egreso, 'y-m-d'))
            $criteria->compare('fecha_registro_egreso', $this->fecha_registro_egreso);
        // if(strlen($this->fecha_registro_egreso)>0) $criteria->compare('fecha_registro_egreso',$this->fecha_registro_egreso,true);
        if (Utiles::isValidDate($this->fecha_egreso, 'y-m-d'))
            $criteria->compare('fecha_egreso', $this->fecha_egreso);
        // if(strlen($this->fecha_egreso)>0) $criteria->compare('fecha_egreso',$this->fecha_egreso,true);
        if (strlen($this->entrevista_realizada) > 0)
            $criteria->compare('entrevista_realizada', $this->entrevista_realizada, true);
        if (strlen($this->observacion_entrevista) > 0)
            $criteria->compare('observacion_entrevista', $this->observacion_entrevista, true);
        if (strlen($this->busqueda) > 0)
            $criteria->compare('busqueda', $this->busqueda, true);
        if (strlen($this->codigo_integridad) > 0)
            $criteria->compare('codigo_integridad', $this->codigo_integridad, true);
        if (strlen($this->observacion) > 0)
            $criteria->compare('observacion', $this->observacion, true);
        if (strlen($this->usuario_ini_id) > 0)
            $criteria->compare('usuario_ini_id', $this->usuario_ini_id, true);
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d'))
            $criteria->compare('fecha_ini', $this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (strlen($this->ip_ini) > 0)
            $criteria->compare('ip_ini', $this->ip_ini, true);
        if (strlen($this->usuario_act_id) > 0)
            $criteria->compare('usuario_act_id', $this->usuario_act_id, true);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d'))
            $criteria->compare('fecha_act', $this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (strlen($this->ip_act) > 0)
            $criteria->compare('ip_act', $this->ip_act, true);
        if (in_array($this->estatus, array('A', 'I', 'E')))
            $criteria->compare('estatus', $this->estatus, true);

        // var_dump($criteria);
        
        $criteria->order = 't.estatus, t.origen DESC, t.cedula';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeInsert() {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    public function __toString() {
        try {
            return (string) $this->id;
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getDatosPersona($id,$condicion){

        $cacheIndexSimple = str_replace('{id}',$id,$this->cacheIndexSimple);
        $res = Yii::app()->cache->get($cacheIndexSimple);
        if(!$res){
            $qtxt = "SELECT t.id, t.user_id, t.origen, t.cedula, t.nombre, t.apellido, t.estado_id, t.sexo, t.fecha_nacimiento, t.diversidad_funcional_id, t.grado_instruccion_actual_id, t.condicion, t.condicion_actual_id, t.cargo, t.cargo_actual_id, t.dependencia, t.zona_educativa, t.categoria_cargo_actual_id, t.dependencia_id, t.condicion_actual_id, t.estatus, t.tipo_personal, t.estado_dependencia_id, t.estado_dependencia
                       FROM gestion_humana.talento_humano t
                      WHERE t.id = :id AND
                            t.condicion_actual_id = :condicion";

            $command = Yii::app()->db->createCommand($qtxt);            
            $command->bindValue(":id", $id, PDO::PARAM_STR);
            $command->bindValue(":condicion", $condicion, PDO::PARAM_STR);
            $res = $command->queryRow();

            if($res){
                Yii::app()->cache->set($cacheIndexSimple,$res, Helper::$SEGUNDOS_UN_MES);
            }

        }            
        return $res;
    }

    public function getDatosPersonaByIdTh($id){

        $cacheIndexSimpleByIdTh = str_replace('{id}',$id,$this->cacheIndexSimpleByIdTh);
        $res = Yii::app()->cache->get($cacheIndexSimpleByIdTh);
        if(!$res){
            $qtxt = "SELECT t.id, t.user_id, t.origen, t.cedula, t.nombre, t.apellido, t.estado_id, t.sexo, t.fecha_nacimiento, t.diversidad_funcional_id, t.grado_instruccion_actual_id, t.condicion, t.condicion_actual_id, t.cargo, t.cargo_actual_id, t.dependencia, t.zona_educativa, t.categoria_cargo_actual_id, t.dependencia_id, t.condicion_actual_id, t.estatus, t.tipo_personal, t.estado_dependencia_id, t.estado_dependencia
                       FROM gestion_humana.talento_humano t
                      WHERE t.id = :id ";

            $command = Yii::app()->db->createCommand($qtxt);
            $command->bindValue(":id", $id, PDO::PARAM_STR);
            $res = $command->queryRow();

            if($res){
                Yii::app()->cache->set($cacheIndexSimpleByIdTh,$res, Helper::$SEGUNDOS_UN_MES);
            }

        }
        return $res;
    }


    /**
     * @param $idUser
     * @param $idPostulacion
     * @return mixed Retorna solo los campos de id de talento humano
     */
    public function datosPostulacion ($idUser) {

        $cacheIndex = str_replace('{id}', $idUser, $this->cacheDatosPostulacion);
        $respuesta = Yii::app()->cache->get($cacheIndex);
        if(!$respuesta) {
            $sql = "SELECT 
                    id, condicion_actual_id, estado_dependencia_id
                    FROM  gestion_humana.talento_humano
                    WHERE                     
                    user_id = :idUser
                    LIMIT 1";

            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(":idUser", $idUser, PDO::PARAM_INT);
            $respuesta = $command->queryRow();

            if($respuesta){
                Yii::app()->cache->set($cacheIndex,$respuesta, Helper::$SEGUNDOS_UN_MES);
            }
        }
        
        return $respuesta;
    }
    
    public function deleteCacheDatosPostulacion($PostulacionId){
       $cacheIndex = strtr($this->cacheDatosPostulacion, array('{id}' => $PostulacionId));
        Yii::app()->cache->delete($cacheIndex);
        return true;
    }
    
    public function deleteCacheDatosTalentoHumano($talentoHumanoId){
        $cacheIndex = strtr($this->cacheIndexSimple, array('{id}' => $talentoHumanoId));
        Yii::app()->cache->delete($cacheIndex);
        $cacheIndex = strtr($this->cacheIndexSimpleByIdTh, array('{id}' => $talentoHumanoId));
        Yii::app()->cache->delete($cacheIndex);
        return true;
    }
        
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TalentoHumano the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
