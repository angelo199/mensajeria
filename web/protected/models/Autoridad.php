<?php

/**
 * This is the model class for table "gestion_humana.autoridad".
 *
 * The followings are the available columns in table 'gestion_humana.autoridad':
 * @property integer $id
 * @property integer $dependencia_id
 * @property string $origen
 * @property integer $cedula
 * @property string $nombre
 * @property string $apellido
 * @property string $imagen_firma
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $estatus
 * @property string $observacion
 * @property integer $es_encargado
 * @property string $titulo
 * @property string $resolucion
 * @property string $gaceta_oficial
 *
 * The followings are the available model relations:
 * @property Dependencia $dependencia
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 */
class Autoridad extends CActiveRecord {

    public $dependenciaNombre = '';
    public $cacheAutoridad='AUTORIDAD:DATOS';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gestion_humana.autoridad';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, cedula, origen, apellido, fecha_ini, fecha_act, usuario_ini_id, titulo, resolucion, gaceta_oficial', 'required'),
            array('dependencia_id, cedula, usuario_ini_id, usuario_act_id, es_encargado', 'numerical', 'integerOnly' => true),
            array('origen, estatus', 'length', 'max' => 1),
            array('nombre, apellido, titulo, resolucion, gaceta_oficial', 'length', 'max' => 150),
            array('imagen_firma', 'length', 'max' => 250),
            array('origen', 'in', 'range' => array('V', 'E', 'P'), 'allowEmpty' => false, 'strict' => true,),
            array('es_encargado', 'in', 'range' => array(0, 1), 'allowEmpty' => false, 'strict' => true,),
            array('estatus', 'in', 'range' => array('A', 'I', 'E'), 'allowEmpty' => false, 'strict' => true,),
            array('usuario_ini_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'insert'),
            array('usuario_act_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'update'),
            array('observacion', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, dependencia_id, origen, cedula, nombre, apellido, imagen_firma, fecha_ini, fecha_act, usuario_ini_id, usuario_act_id, estatus, observacion, es_encargado, titulo, resolucion, gaceta_oficial', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'dependencia' => array(self::BELONGS_TO, 'Dependencia', 'dependencia_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'dependencia_id' => 'Dependencia',
            'origen' => 'Origen',
            'cedula' => 'Cedula',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'imagen_firma' => 'Imagen Firma',
            'fecha_ini' => 'Fecha Ini',
            'fecha_act' => 'Fecha Act',
            'usuario_ini_id' => 'Usuario Ini',
            'usuario_act_id' => 'Usuario Act',
            'estatus' => 'Estatus',
            'observacion' => 'Observación',
            'es_encargado' => 'Es Encargado',
            'titulo' => 'Titulo',
            'resolucion' => 'Resolucion',
            'gaceta_oficial' => 'Gaceta Oficial',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array(
            'dependencia' => array('select' => 'd.codigo, d.nombre', 'alias' => 'd'),
        );

        if (strlen($this->dependenciaNombre) > 0)
            $criteria->compare('dd.codigo||d.nombre', $this->dependenciaNombre, true);

        if (is_numeric($this->id))
            $criteria->compare('id', $this->id);
        if (is_numeric($this->dependencia_id))
            $criteria->compare('dependencia_id', $this->dependencia_id);
        if (strlen($this->origen) > 0)
            $criteria->compare('origen', $this->origen, true);
        if (is_numeric($this->cedula))
            $criteria->compare('cedula', $this->cedula);
        if (strlen($this->nombre) > 0)
            $criteria->compare('nombre', $this->nombre, true);
        if (strlen($this->apellido) > 0)
            $criteria->compare('apellido', $this->apellido, true);
        if (strlen($this->imagen_firma) > 0)
            $criteria->compare('imagen_firma', $this->imagen_firma, true);
        if (strlen($this->observacion) > 0)
            $criteria->compare('observacion', $this->observacion, true);
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d'))
            $criteria->compare('fecha_ini', $this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d'))
            $criteria->compare('fecha_act', $this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (is_numeric($this->usuario_ini_id))
            $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        if (is_numeric($this->usuario_act_id))
            $criteria->compare('usuario_act_id', $this->usuario_act_id);
        if (in_array($this->estatus, array('A', 'I', 'E')))
            $criteria->compare('estatus', $this->estatus, true);
        if (is_numeric($this->es_encargado))
            $criteria->compare('es_encargado', $this->es_encargado);
        if (strlen($this->titulo) > 0)
            $criteria->compare('titulo', $this->titulo, true);
        if (strlen($this->resolucion) > 0)
            $criteria->compare('resolucion', $this->resolucion, true);
        if (strlen($this->gaceta_oficial) > 0)
            $criteria->compare('gaceta_oficial', $this->gaceta_oficial, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeInsert() {
        parent::beforeSave();
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    public function __toString() {
        try {
            return (string) $this->id;
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }
    
    public function getAutoridad(){
        $result= Yii::app()->cache->get($this->cacheAutoridad);
        if(!$result){
            $sql="SELECT * FROM gestion_humana.autoridad WHERE dependencia_id=3210 OR dependencia_id=531";
            $query = Yii::app()->db->createCommand($sql);
            $result = $query->queryAll();
            
            if (count($result)>0){
                Yii::app()->cache->set($this->cacheAutoridad, $result, Helper::$SEGUNDOS_UN_MES);
            }
       }
        return $result;

    }

    public function deleteCacheAutoridad() {
        $result= Yii::app()->cache->delete($this->cacheAutoridad);
        return $result;
    }
    

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Autoridad the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
