<?php

/**
 * This is the model class for table "gprueba.respuesta_prueba_asp".
 *
 * The followings are the available columns in table 'gprueba.respuesta_prueba_asp':
 * @property string $id
 * @property integer $aplicacion_prueba_id
 * @property integer $pregunta_id
 * @property integer $respuesta
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property AplicacionPrueba $aplicacionPrueba
 * @property Pregunta $pregunta
 * @property Respuesta $respuesta
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 */
class RespuestaPruebaAsp extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gprueba.respuesta_prueba_asp';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('aplicacion_prueba_id, pregunta_id, respuesta, usuario_ini_id, fecha_ini, estatus', 'required'),
            array('aplicacion_prueba_id, pregunta_id, respuesta, usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly' => true),
            // array('fecha_ini, fecha_act, fecha_elim', 'length', 'max' => 6),
            array('estatus', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, aplicacion_prueba_id, pregunta_id, respuesta, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'aplicacionPrueba' => array(self::BELONGS_TO, 'AplicacionPrueba', 'aplicacion_prueba_id'),
            'pregunta' => array(self::BELONGS_TO, 'Pregunta', 'pregunta_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'aplicacion_prueba_id' => 'Indica el id de aplicación de la prueba',
            'pregunta_id' => 'Indica el id de la pregunta de la prueba',
            'respuesta' => 'Indica el id de la respuesta de la pregunta',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'usuario_elim_id' => 'Usuario Elim',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('aplicacion_prueba_id', $this->aplicacion_prueba_id);
        $criteria->compare('pregunta_id', $this->pregunta_id);
        $criteria->compare('respuesta', $this->respuesta);
        $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('fecha_ini', $this->fecha_ini, true);
        $criteria->compare('usuario_act_id', $this->usuario_act_id);
        $criteria->compare('fecha_act', $this->fecha_act, true);
        $criteria->compare('usuario_elim_id', $this->usuario_elim_id);
        $criteria->compare('fecha_elim', $this->fecha_elim, true);
        $criteria->compare('estatus', $this->estatus, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RespuestaPruebaAsp the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function respuestasAsp($aplicacion_id, $idPregunta) {

        $sql = "SELECT COALESCE(n.respuesta::character varying,'X') AS respuesta, COALESCE(p.tipo_pregunta,'X') AS tipo_pregunta
                FROM gprueba.respuesta_prueba_asp n
                INNER JOIN gprueba.aplicacion_prueba a on (a.id = n.aplicacion_prueba_id)
                INNER JOIN gprueba.pregunta p on (p.id = n.pregunta_id)
                --INNER JOIN gprueba.respuesta r on (r.id = n.respuesta)
                WHERE a.id=:aplicacion_prueba_id
                AND n.pregunta_id=:pregunta_id
                ORDER BY n.id
                ";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":pregunta_id", $idPregunta, PDO::PARAM_STR);
        $consulta->bindParam(":aplicacion_prueba_id", $aplicacion_id, PDO::PARAM_INT);
        $result = $consulta->queryAll();
        return $result;
    }

    public function obtenerRespuestasPruebaAsp($aspirante_id, $prueba_id, $pregunta_id, $tipo_pregunta = '') {
        if ($tipo_pregunta == 'T' || $tipo_pregunta == '') {
            $sql = 'SELECT n.id As respuesta_prueba_asp, r.id As respuesta
                FROM gprueba.respuesta_prueba_asp n
                INNER JOIN gprueba.aplicacion_prueba a ON (a.id = n.aplicacion_prueba_id)
                INNER JOIN gprueba.respuesta r ON (r.id = n.respuesta)
                WHERE a.prueba_id = :prueba_id
                AND n.pregunta_id = :pregunta_id
                AND a.aspirante_id = :aspirante_id
                AND substring(n.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)
                LIMIT 1
                ';
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_STR);
            $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
            $consulta->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
            $result = $consulta->queryRow();
            return $result;
        } elseif ($tipo_pregunta == 'D') {
            $sql = 'SELECT n.id As respuesta_prueba_asp, n.respuesta As respuesta
                FROM gprueba.respuesta_prueba_asp n
                INNER JOIN gprueba.aplicacion_prueba a ON (a.id = n.aplicacion_prueba_id)
                WHERE a.prueba_id = :prueba_id
                AND n.pregunta_id = :pregunta_id
                AND a.aspirante_id = :aspirante_id
                AND substring(n.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)
                ORDER BY n.id ASC
                LIMIT 2
                ';
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_STR);
            $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
            $consulta->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
            $result = $consulta->queryAll();
            return $result;
        } elseif ($tipo_pregunta == 'I') {
            $sql = 'SELECT n.id As respuesta_prueba_asp, n.respuesta As respuesta
                FROM gprueba.respuesta_prueba_asp n
                INNER JOIN gprueba.aplicacion_prueba a ON (a.id = n.aplicacion_prueba_id)
                WHERE a.prueba_id = :prueba_id
                AND n.pregunta_id = :pregunta_id
                AND a.aspirante_id = :aspirante_id
                AND substring(n.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)
                LIMIT 1
                ';
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_STR);
            $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
            $consulta->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
            $result = $consulta->queryRow();
            return $result;
        }
    }

    public function registrarRespuesta($respuesta_domino1, $respuesta_domino2, $pregunta_id, $aplicacion_id, $usuario_id) {
        $estatus = 'A';
        $aplicacion_id = (int) $aplicacion_id;
        $pregunta_id = (int) $pregunta_id;
        $respuesta_domino = array(
            0 => $respuesta_domino1,
            1 => $respuesta_domino2
        );

        foreach ($respuesta_domino as $key => $value) {
            $respuesta_id = (int) $value;

            $sql = "INSERT INTO gprueba.respuesta_prueba_asp(
                        aplicacion_prueba_id, pregunta_id,
                        respuesta, usuario_ini_id,estatus)
                VALUES (:aplicacion_id, :pregunta_id, :respuesta_id,
                       :usuario_id,:estatus);";

            $guardar = Yii::app()->db->createCommand($sql);
            $guardar->bindParam(":aplicacion_id", $aplicacion_id, PDO::PARAM_INT);
            $guardar->bindParam(":estatus", $estatus, PDO::PARAM_STR);
            $guardar->bindParam(":respuesta_id", $respuesta_id, PDO::PARAM_INT);
            $guardar->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
            $guardar->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $result = $guardar->execute();
        }
        return $result;
    }

    public function actualizarRespuesta($id_respuesta_domino1, $id_respuesta_domino2, $respuesta_domino1, $respuesta_domino2, $fecha, $usuario_id) {

        $respuesta_domino = array(
            0 => $respuesta_domino1,
            1 => $respuesta_domino2
        );

        $id_respuesta_prueba_asp = array(
            0 => $id_respuesta_domino1,
            1 => $id_respuesta_domino2
        );
        foreach ($respuesta_domino as $key => $value) {
            $id = (int) $id_respuesta_prueba_asp[$key];
            $respuesta_id = (int) $value;

            $sql = "UPDATE gprueba.respuesta_prueba_asp
                      SET   respuesta=:respuesta_id,
                            usuario_act_id=:usuario_id,
                            fecha_act=:fecha
                      WHERE id=:id_respuesta
                      AND substring(fecha_ini::character varying,0,5)=substring(now()::character varying,0,5);";

            $guardar = Yii::app()->db->createCommand($sql);
            $guardar->bindParam(":fecha", $fecha, PDO::PARAM_STR);
            $guardar->bindParam(":respuesta_id", $respuesta_id, PDO::PARAM_INT);
            $guardar->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $guardar->bindParam(":id_respuesta", $id, PDO::PARAM_INT);
            $result = $guardar->execute();
        }
        return $result;
    }

    public function obtenerRespuestasDomino($aspirante_id, $prueba_id, $pregunta_id, $tipo_pregunta) {
        if ($tipo_pregunta == 'D') {
            $sql = 'SELECT n.respuesta As respuesta
                FROM gprueba.respuesta_prueba_asp n
                INNER JOIN gprueba.aplicacion_prueba a ON (a.id = n.aplicacion_prueba_id)
                WHERE a.prueba_id = :prueba_id
                AND n.pregunta_id = :pregunta_id
                AND a.aspirante_id = :aspirante_id
                AND substring(n.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)
                ORDER BY n.id ASC
                LIMIT 2
                ';
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_STR);
            $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
            $consulta->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
            $result = $consulta->queryAll();
            return $result;
        }
        if ($tipo_pregunta == 'I') {
            $sql = 'SELECT n.respuesta As respuesta
                FROM gprueba.respuesta_prueba_asp n
                INNER JOIN gprueba.aplicacion_prueba a ON (a.id = n.aplicacion_prueba_id)
                WHERE a.prueba_id = :prueba_id
                AND n.pregunta_id = :pregunta_id
                AND a.aspirante_id = :aspirante_id
                AND substring(n.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)
                ORDER BY n.id ASC
                LIMIT 1
                ';
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_STR);
            $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
            $consulta->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
            $result = $consulta->queryAll();
            return $result;
        }
    }

    public function datosPreguntaRespondida($prueba_id, $aspirante_id, $datosPrueba, $fecha_actual) {
        //   $fecha_actual=$fecha_actual.''
        $pregunta_id = '';
        foreach ($datosPrueba as $key => $value) {
            if ($key == 0) {
                $pregunta_id = $value['pregunta_id'];
            } else {
                break;
            }
        }
        $sql = 'SELECT rps.respuesta
                        FROM gprueba.respuesta_prueba_asp rps
                        INNER JOIN gprueba.aplicacion_prueba ap ON(ap.aspirante_id =:aspirante_id AND ap.id = rps.aplicacion_prueba_id)
                        INNER JOIN gprueba.pregunta p ON(rps.pregunta_id = p.id AND p.id =:pregunta_id)
                        WHERE ap.prueba_id=:prueba_id AND substring(rps.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)
                        LIMIT 2';
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_STR);
        $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
        $consulta->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
        $result = $consulta->queryAll();
        return $result;
    }

    public function obtenerPuntuacionFinal($aspirante_id) {
        if ($aspirante_id != '') {
            $sql = "SELECT puntuacion_final, fecha_inicial
                        FROM gprueba.puntuacion_final_prueba
                        WHERE aspirante_id=$aspirante_id
                        ORDER BY fecha_inicial DESC
                        ";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_INT);
            $result = $consulta->queryAll();
            return $result;
        }
    }

    public function resultadosGuardados($pregunta_id, $aspirante_id) {
        $pregunta_id = (int) $pregunta_id;
        $aspirante_id = (int) $aspirante_id;
        $sql = "SELECT p.id
                        FROM gprueba.respuesta_prueba_asp rp
                        INNER JOIN gprueba.pregunta p ON(p.id = rp.pregunta_id)
                        INNER JOIN gprueba.aplicacion_prueba ap ON(ap.id = rp.aplicacion_prueba_id)
                        WHERE rp.pregunta_id=:pregunta_id
                        AND ap.aspirante_id=:aspirante_id
                        AND substring(rp.fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":pregunta_id", $pregunta_id, PDO::PARAM_INT);
        $consulta->bindParam(":aspirante_id", $aspirante_id, PDO::PARAM_INT);
        $result = $consulta->queryRow();
        return $result;
    }

}
