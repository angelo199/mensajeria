<?php

/**
 * This is the model class for table "gprueba.estatus_iniciar_prueba".
 *
 * The followings are the available columns in table 'gprueba.estatus_iniciar_prueba':
 * @property string $id
 * @property integer $responsable_aplicar_prueba_id
 * @property integer $prueba_id
 * @property integer $estatus_prueba_aplicada
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property string $fecha_elim
 * @property string $estatus
 * @property string $hora_inicio_prueba
 * @property string $hora_final_prueba
 *
 * The followings are the available model relations:
 * @property Prueba $prueba
 * @property UsergroupsUser $responsableAplicarPrueba
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 */
class EstatusIniciarPrueba extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gprueba.estatus_iniciar_prueba';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('responsable_aplicar_prueba_id, prueba_id, estatus_prueba_aplicada, usuario_ini_id, fecha_ini, estatus, hora_inicio_prueba, hora_final_prueba', 'required'),
            array('responsable_aplicar_prueba_id, prueba_id, estatus_prueba_aplicada, usuario_ini_id, usuario_act_id, usuario_elim_id', 'numerical', 'integerOnly' => true),
            //array('fecha_ini, fecha_act, fecha_elim', 'length', 'max'=>6),
            array('estatus', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, responsable_aplicar_prueba_id, prueba_id, estatus_prueba_aplicada, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus, hora_inicio_prueba, hora_final_prueba', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'prueba' => array(self::BELONGS_TO, 'Prueba', 'prueba_id'),
            'responsableAplicarPrueba' => array(self::BELONGS_TO, 'UsergroupsUser', 'responsable_aplicar_prueba_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioElim' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_elim_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'responsable_aplicar_prueba_id' => 'Indica el id del responsable que aplica la prueba',
            'prueba_id' => 'Indica el id de la prueba que le estan aplicando al aspirante',
            'estatus_prueba_aplicada' => 'Indica 1: Si se esta iniciando la prueba o 2: Si ya se culmino la prueba.',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'usuario_elim_id' => 'Usuario Elim',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'hora_inicio_prueba' => 'Indica el tiempo inicial de la prueba',
            'hora_final_prueba' => 'Indica el tiempo final de la prueba',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('responsable_aplicar_prueba_id', $this->responsable_aplicar_prueba_id);
        $criteria->compare('prueba_id', $this->prueba_id);
        $criteria->compare('estatus_prueba_aplicada', $this->estatus_prueba_aplicada);
        $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('fecha_ini', $this->fecha_ini, true);
        $criteria->compare('usuario_act_id', $this->usuario_act_id);
        $criteria->compare('fecha_act', $this->fecha_act, true);
        $criteria->compare('usuario_elim_id', $this->usuario_elim_id);
        $criteria->compare('fecha_elim', $this->fecha_elim, true);
        $criteria->compare('estatus', $this->estatus, true);
        $criteria->compare('hora_inicio_prueba', $this->hora_inicio_prueba, true);
        $criteria->compare('hora_final_prueba', $this->hora_final_prueba, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EstatusIniciarPrueba the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function obtenerHoraFinal($prueba_id) {

        $sql = "SELECT sp.duracion_seccion
                    FROM gprueba.seccion_prueba sp
                    WHERE sp.prueba_id = :prueba";
        $commandBusqueda = Yii::app()->db->createCommand($sql);
        $commandBusqueda->bindParam(':prueba', $prueba_id, PDO::PARAM_INT);
        $result = $commandBusqueda->queryAll();

        return $result;
    }

    public function finalizarPrueba($prueba_id) {
        if ($prueba_id != '') {
            (int) $usuario_id = Yii::app()->user->id;
            (string) $fecha = date('Y-m-d H:i:s');
            (string) $estatus = 'I';
            $sql = "UPDATE gprueba.estatus_iniciar_prueba
                      SET   estatus_prueba_aplicada=2,
                            usuario_act_id=:usuario_id,
                            fecha_act=:fecha,
                            estatus=:estatu
                      WHERE prueba_id=:prueba
                      AND estatus='A'";
            $commandBusqueda = Yii::app()->db->createCommand($sql);
            $commandBusqueda->bindParam(':prueba', $prueba_id, PDO::PARAM_INT);
            $commandBusqueda->bindParam(':usuario_id', $usuario_id, PDO::PARAM_INT);
            $commandBusqueda->bindParam(':fecha', $fecha, PDO::PARAM_STR);
            $commandBusqueda->bindParam(':estatu', $estatus, PDO::PARAM_STR);

            $result = $commandBusqueda->execute();

            if ($result == 1) {

                (int) $usuario_id = Yii::app()->user->id;
                (string) $fecha = date('Y-m-d H:i:s');
                (string) $estatus = 'I';
                (int) $estatus_prueba_aplicada = 2;
                $sql = "UPDATE gprueba.aplicacion_prueba
                                     SET   estatus_prueba_aplicada=:estatus_aplicada,
                                              usuario_act_id=:usuario_id,
                                              fecha_act=:fecha,
                                              estatus=:estatus
                                    WHERE prueba_id=:prueba
                                    AND estatus='A'
                                    AND substring(fecha_ini::character varying,0,5)=substring(now()::character varying,0,5)";
                $commandBusqueda = Yii::app()->db->createCommand($sql);
                $commandBusqueda->bindParam(':prueba', $prueba_id, PDO::PARAM_INT);
                $commandBusqueda->bindParam(':usuario_id', $usuario_id, PDO::PARAM_INT);
                $commandBusqueda->bindParam(':fecha', $fecha, PDO::PARAM_STR);
                $commandBusqueda->bindParam(':estatus', $estatus, PDO::PARAM_STR);
                $commandBusqueda->bindParam(':estatus_aplicada', $estatus_prueba_aplicada, PDO::PARAM_INT);
                $resultado = $commandBusqueda->execute();
                //  var_dump($resultado);
                if ($resultado == 0) {
                    return $resultado = 1;
                } else {
                    return $resultado;
                }
            } else {
                return $result;
            }
        }
    }

    public function inactivarAplicacionPrueba($prueba_id) {
        if ($prueba_id != '') {
            (int) $usuario_id = Yii::app()->user->id;
            (string) $fecha = date('Y-m-d H:i:s');
            (string) $estatus = 'I';
            $sql = "UPDATE gprueba.estatus_iniciar_prueba
                      SET   estatus_prueba_aplicada=2,
                            usuario_act_id=:usuario_id,
                            fecha_act=:fecha,
                            estatus=:estatu
                      WHERE prueba_id=:prueba
                      AND estatus='A'";
            $commandBusqueda = Yii::app()->db->createCommand($sql);
            $commandBusqueda->bindParam(':prueba', $prueba_id, PDO::PARAM_INT);
            $commandBusqueda->bindParam(':usuario_id', $usuario_id, PDO::PARAM_INT);
            $commandBusqueda->bindParam(':fecha', $fecha, PDO::PARAM_STR);
            $commandBusqueda->bindParam(':estatu', $estatus, PDO::PARAM_STR);
            $result = $commandBusqueda->execute();
            return $result;
        }
    }

}
