<?php

/**
 * This is the model class for table "seguridad.usergroups_user".
 *
 * The followings are the available columns in table 'seguridad.usergroups_user':
 * @property string $id
 * @property string $group_id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $home
 * @property integer $status
 * @property string $question
 * @property string $answer
 * @property string $creation_date
 * @property string $activation_code
 * @property string $activation_time
 * @property string $last_login
 * @property string $ban
 * @property string $ban_reason
 * @property string $telefono
 * @property string $nombre
 * @property string $apellido
 * @property integer $cedula
 * @property string $direccion
 * @property integer $estado_id
 * @property string $user_ini_id
 * @property string $date_ini
 * @property string $user_act_id
 * @property string $date_act
 * @property string $user_ban_id
 * @property string $last_ip_address
 * @property string $origen
 * @property string $clave_anterior
 * @property integer $cambio_clave
 * @property string $twitter
 * @property string $telefono_celular
 * @property string $presento_documento_identidad
 * @property string $foto
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $userAct
 * @property UsergroupsUser[] $usergroupsUsers
 * @property UsergroupsUser $userBan
 * @property UsergroupsUser[] $usergroupsUsers1
 * @property Estado $estado
 * @property UsergroupsGroup $group
 * @property UsergroupsUser $userIni
 * @property UsergroupsUser[] $usergroupsUsers2
 * @property UsergroupsGroup[] $usergroupsGroups
 */
class UsergroupsUser extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'seguridad.usergroups_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email, nombre, cedula, estado_id, date_ini', 'required'),
			array('status, cedula, estado_id, cambio_clave', 'numerical', 'integerOnly'=>true),
			array('username, email, home', 'length', 'max'=>120),
			array('password', 'length', 'max'=>300),
			array('activation_code', 'length', 'max'=>30),
			array('telefono, telefono_celular', 'length', 'max'=>14),
			array('nombre, apellido, last_ip_address, twitter', 'length', 'max'=>40),
			array('origen', 'length', 'max'=>1),
			array('clave_anterior', 'length', 'max'=>255),
			array('presento_documento_identidad', 'length', 'max'=>2),
			array('foto', 'length', 'max'=>350),
			array('origen', 'in', 'range'=>array('V', 'E', 'P'), 'allowEmpty'=>false, 'strict'=>true,),
			array('email', 'email'),
			array('group_id, question, answer, activation_time, last_login, ban, ban_reason, direccion, user_ini_id, user_act_id, user_ban_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, group_id, username, password, email, home, status, question, answer, creation_date, activation_code, activation_time, last_login, ban, ban_reason, telefono, nombre, apellido, cedula, direccion, estado_id, user_ini_id, date_ini, user_act_id, date_act, user_ban_id, last_ip_address, origen, clave_anterior, cambio_clave, twitter, telefono_celular, presento_documento_identidad, foto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'user_act_id'),
			'usergroupsUsers' => array(self::HAS_MANY, 'UsergroupsUser', 'user_act_id'),
			'userBan' => array(self::BELONGS_TO, 'UsergroupsUser', 'user_ban_id'),
			'usergroupsUsers1' => array(self::HAS_MANY, 'UsergroupsUser', 'user_ban_id'),
			'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
			'group' => array(self::BELONGS_TO, 'UsergroupsGroup', 'group_id'),
			'userIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'user_ini_id'),
			'usergroupsUsers2' => array(self::HAS_MANY, 'UsergroupsUser', 'user_ini_id'),
			'usergroupsGroups' => array(self::HAS_MANY, 'UsergroupsGroup', 'user_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'home' => 'Home',
			'status' => 'Status',
			'question' => 'Question',
			'answer' => 'Answer',
			'creation_date' => 'Creation Date',
			'activation_code' => 'Activation Code',
			'activation_time' => 'Activation Time',
			'last_login' => 'Last Login',
			'ban' => 'Ban',
			'ban_reason' => 'Ban Reason',
			'telefono' => 'Telefono',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'cedula' => 'Cedula',
			'direccion' => 'Direccion',
			'estado_id' => 'Estado',
			'user_ini_id' => 'User Ini',
			'date_ini' => 'Date Ini',
			'user_act_id' => 'User Act',
			'date_act' => 'Date Act',
			'user_ban_id' => 'User Ban',
			'last_ip_address' => 'Last Ip Address',
			'origen' => 'Origen',
			'clave_anterior' => 'Clave Anterior',
			'cambio_clave' => 'Cambio Clave',
			'twitter' => 'Twitter',
			'telefono_celular' => 'Telefono Celular',
			'presento_documento_identidad' => 'Presento Documento Identidad',
			'foto' => 'Foto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->id)>0) $criteria->compare('id',$this->id,true);
		if(strlen($this->group_id)>0) $criteria->compare('group_id',$this->group_id,true);
		if(strlen($this->username)>0) $criteria->compare('username',$this->username,true);
		if(strlen($this->password)>0) $criteria->compare('password',$this->password,true);
		if(strlen($this->email)>0) $criteria->compare('email',$this->email,true);
		if(strlen($this->home)>0) $criteria->compare('home',$this->home,true);
		if(is_numeric($this->status)) $criteria->compare('status',$this->status);
		if(strlen($this->question)>0) $criteria->compare('question',$this->question,true);
		if(strlen($this->answer)>0) $criteria->compare('answer',$this->answer,true);
		if(Utiles::isValidDate($this->creation_date, 'y-m-d')) $criteria->compare('creation_date',$this->creation_date);
		// if(strlen($this->creation_date)>0) $criteria->compare('creation_date',$this->creation_date,true);
		if(strlen($this->activation_code)>0) $criteria->compare('activation_code',$this->activation_code,true);
		if(strlen($this->activation_time)>0) $criteria->compare('activation_time',$this->activation_time,true);
		if(strlen($this->last_login)>0) $criteria->compare('last_login',$this->last_login,true);
		if(strlen($this->ban)>0) $criteria->compare('ban',$this->ban,true);
		if(strlen($this->ban_reason)>0) $criteria->compare('ban_reason',$this->ban_reason,true);
		if(strlen($this->telefono)>0) $criteria->compare('telefono',$this->telefono,true);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);
		if(strlen($this->apellido)>0) $criteria->compare('apellido',$this->apellido,true);
		if(is_numeric($this->cedula)) $criteria->compare('cedula',$this->cedula);
		if(strlen($this->direccion)>0) $criteria->compare('direccion',$this->direccion,true);
		if(is_numeric($this->estado_id)) $criteria->compare('estado_id',$this->estado_id);
		if(strlen($this->user_ini_id)>0) $criteria->compare('user_ini_id',$this->user_ini_id,true);
		if(Utiles::isValidDate($this->date_ini, 'y-m-d')) $criteria->compare('date_ini',$this->date_ini);
		// if(strlen($this->date_ini)>0) $criteria->compare('date_ini',$this->date_ini,true);
		if(strlen($this->user_act_id)>0) $criteria->compare('user_act_id',$this->user_act_id,true);
		if(Utiles::isValidDate($this->date_act, 'y-m-d')) $criteria->compare('date_act',$this->date_act);
		// if(strlen($this->date_act)>0) $criteria->compare('date_act',$this->date_act,true);
		if(strlen($this->user_ban_id)>0) $criteria->compare('user_ban_id',$this->user_ban_id,true);
		if(strlen($this->last_ip_address)>0) $criteria->compare('last_ip_address',$this->last_ip_address,true);
		if(strlen($this->origen)>0) $criteria->compare('origen',$this->origen,true);
		if(strlen($this->clave_anterior)>0) $criteria->compare('clave_anterior',$this->clave_anterior,true);
		if(is_numeric($this->cambio_clave)) $criteria->compare('cambio_clave',$this->cambio_clave);
		if(strlen($this->twitter)>0) $criteria->compare('twitter',$this->twitter,true);
		if(strlen($this->telefono_celular)>0) $criteria->compare('telefono_celular',$this->telefono_celular,true);
		if(strlen($this->presento_documento_identidad)>0) $criteria->compare('presento_documento_identidad',$this->presento_documento_identidad,true);
		if(strlen($this->foto)>0) $criteria->compare('foto',$this->foto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsergroupsUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
