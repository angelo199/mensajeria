<?php

/**
 * This is the model class for table "expediente.empleado".
 *
 * The followings are the available columns in table 'expediente.empleado':
 * @property integer $id
 * @property string $nombres_y_apellidos
 * @property string $tipo_de_personal
 * @property string $situacion
 * @property string $anio_de_jubilacion
 * @property string $pais_de_nacimiento
 * @property string $estado_de_nacimiento
 * @property string $ciudad_de_nacimiento
 * @property string $sexo
 * @property integer $id_expediente_administrativo
 * @property integer $id_expediente_laboral
 * @property string $expediente
 * @property string $cedula
 * @property string $fecha_de_nacimiento
 * @property string $fecha_de_ingreso
 * @property string $estado_de_resolucion
 * @property string $ubicacion_fisica
 */
class Empleado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'expediente.empleado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_expediente_administrativo, id_expediente_laboral', 'numerical', 'integerOnly'=>true),
			array('nombres_y_apellidos', 'length', 'max'=>800),
			array('tipo_de_personal, situacion, pais_de_nacimiento, estado_de_nacimiento, ciudad_de_nacimiento, estado_de_resolucion', 'length', 'max'=>50),
			array('anio_de_jubilacion, sexo', 'length', 'max'=>10),
			array('expediente', 'length', 'max'=>13),
			array('cedula', 'length', 'max'=>60),
			array('fecha_de_nacimiento', 'length', 'max'=>61),
			array('fecha_de_ingreso', 'length', 'max'=>62),
			array('ubicacion_fisica', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombres_y_apellidos, tipo_de_personal, situacion, anio_de_jubilacion, pais_de_nacimiento, estado_de_nacimiento, ciudad_de_nacimiento, sexo, id_expediente_administrativo, id_expediente_laboral, expediente, cedula, fecha_de_nacimiento, fecha_de_ingreso, estado_de_resolucion, ubicacion_fisica', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombres_y_apellidos' => 'Nombres Y Apellidos',
			'tipo_de_personal' => 'Tipo De Personal',
			'situacion' => 'Situacion',
			'anio_de_jubilacion' => 'Anio De Jubilacion',
			'pais_de_nacimiento' => 'Pais De Nacimiento',
			'estado_de_nacimiento' => 'Estado De Nacimiento',
			'ciudad_de_nacimiento' => 'Ciudad De Nacimiento',
			'sexo' => 'Sexo',
			'id_expediente_administrativo' => 'Id Expediente Administrativo',
			'id_expediente_laboral' => 'Id Expediente Laboral',
			'expediente' => 'Expediente',
			'cedula' => 'Cedula',
			'fecha_de_nacimiento' => 'Fecha De Nacimiento',
			'fecha_de_ingreso' => 'Fecha De Ingreso',
			'estado_de_resolucion' => 'Estado De Resolucion',
			'ubicacion_fisica' => 'Ubicacion Fisica',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombres_y_apellidos',$this->nombres_y_apellidos,true);
		$criteria->compare('tipo_de_personal',$this->tipo_de_personal,true);
		$criteria->compare('situacion',$this->situacion,true);
		$criteria->compare('anio_de_jubilacion',$this->anio_de_jubilacion,true);
		$criteria->compare('pais_de_nacimiento',$this->pais_de_nacimiento,true);
		$criteria->compare('estado_de_nacimiento',$this->estado_de_nacimiento,true);
		$criteria->compare('ciudad_de_nacimiento',$this->ciudad_de_nacimiento,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('id_expediente_administrativo',$this->id_expediente_administrativo);
		$criteria->compare('id_expediente_laboral',$this->id_expediente_laboral);
		$criteria->compare('expediente',$this->expediente,true);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('fecha_de_nacimiento',$this->fecha_de_nacimiento,true);
		$criteria->compare('fecha_de_ingreso',$this->fecha_de_ingreso,true);
		$criteria->compare('estado_de_resolucion',$this->estado_de_resolucion,true);
		$criteria->compare('ubicacion_fisica',$this->ubicacion_fisica,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Empleado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        //query busquedaExpediente que trae los datos personales de la persona seleccionada con el numero de cedula
        public  function busquedaExpediente($cedula){
            $resultado='';
            $sql="select * from expediente.empleado where cedula=:cedula";
            $query=Yii::app()->db->createCommand($sql);
            $query->bindParam(':cedula', $cedula,  PDO::PARAM_STR);
            $resultado=$query->queryRow();
            return $resultado;  
        }
        
        //query foto que trae foto y/o imagen de la persona seleccionada
        public function foto($cedula){
            $sql="SELECT '<img src=\"http://apmecd.me.gob.ve/db' || archivo || '\" /> ' as url
                      FROM hipertextos h inner join document p on h.acceso = p.acceso  where p.ubicacion=:ubicacion and h.ext_acceso = 0";
            
                $consulta=Yii::app()->db->createCommand($sql);
                $consulta->bindParam(':ubicacion', $cedula,  PDO::PARAM_STR);
                $resultado=$consulta->queryRow(); 
                return $resultado;
        }
        
        //query busquedaExpediente que trae los datos personales de la persona seleccionada con el numero de Cédula, nombre, apellido ó número de expediente
        public function busquedaAvanzada($param) {
            $resultado='';
            $sql="select * from expediente.empleado where cedula=:cedula or nombres_y_apellidos=:nombres_y_apellidos or expediente=:expediente"
                    ;
            $query=Yii::app()->db->createCommand($sql);
            $query->bindParam(':cedula', $cedula,  PDO::PARAM_STR);
            $resultado=$query->queryAll();
            return $resultado;  
        }




}