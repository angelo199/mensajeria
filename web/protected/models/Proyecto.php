<?php

/**
 * This is the model class for table "gproyecto.proyecto".
 *
 * The followings are the available columns in table 'gproyecto.proyecto':
 * @property integer $id
 * @property string $nombre
 * @property integer $organismo_responsable_venezuela_id
 * @property integer $organismo_responsable_convenio_id
 * @property integer $pais_convenio_id
 * @property string $descripcion
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property OrganismoResponsableVenezuela $organismoResponsableVenezuela
 * @property OrganismoResponsableConvenio $organismoResponsableConvenio
 * @property PaisConvenio $paisConvenio
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property AvancesProyecto[] $avancesProyectos
 * @property DatosFinancierosProyecto[] $datosFinancierosProyectos
 */
class Proyecto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gproyecto.proyecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, organismo_responsable_venezuela_id, organismo_responsable_convenio_id, pais_convenio_id', 'required'),
			array('organismo_responsable_venezuela_id, organismo_responsable_convenio_id, pais_convenio_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>50),
			array('estatus', 'length', 'max'=>2),
			array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
			array('descripcion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombre, organismo_responsable_venezuela_id, organismo_responsable_convenio_id, pais_convenio_id, descripcion, usuario_ini_id, usuario_act_id, fecha_ini, fecha_act, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'organismoResponsableVenezuela' => array(self::BELONGS_TO, 'OrganismoResponsableVenezuela', 'organismo_responsable_venezuela_id'),
			'organismoResponsableConvenio' => array(self::BELONGS_TO, 'OrganismoResponsableConvenio', 'organismo_responsable_convenio_id'),
			'paisConvenio' => array(self::BELONGS_TO, 'PaisConvenio', 'pais_convenio_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'avancesProyectos' => array(self::HAS_MANY, 'AvancesProyecto', 'proyecto_id'),
			'datosFinancierosProyectos' => array(self::HAS_MANY, 'DatosFinancierosProyecto', 'proyecto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombre' => 'Nombre',
			'organismo_responsable_venezuela_id' => 'Organismo Responsable Venezuela',
			'organismo_responsable_convenio_id' => 'Organismo Responsable Convenio',
			'pais_convenio_id' => 'Pais Convenio',
			'descripcion' => 'Descripcion',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_ini' => 'Fecha Ini',
			'fecha_act' => 'Fecha Act',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(is_numeric($this->id)) $criteria->compare('id',$this->id);
		if(strlen($this->nombre)>0) $criteria->compare('nombre',$this->nombre,true);
		if(is_numeric($this->organismo_responsable_venezuela_id)) $criteria->compare('organismo_responsable_venezuela_id',$this->organismo_responsable_venezuela_id);
		if(is_numeric($this->organismo_responsable_convenio_id)) $criteria->compare('organismo_responsable_convenio_id',$this->organismo_responsable_convenio_id);
		if(is_numeric($this->pais_convenio_id)) $criteria->compare('pais_convenio_id',$this->pais_convenio_id);
		if(strlen($this->descripcion)>0) $criteria->compare('descripcion',$this->descripcion,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert()
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate()
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate(){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
