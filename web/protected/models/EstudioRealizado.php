<?php

/**
 * This is the model class for table "gestion_humana.estudio_realizado".
 *
 * The followings are the available columns in table 'gestion_humana.estudio_realizado':
 * @property string $id
 * @property string $talento_humano_id
 * @property string $condicion_nominal_id
 * @property integer $nivel_estudio_id
 * @property integer $institucion_educativa_id
 * @property integer $especialidad_estudio_id
 * @property string $titulo_obtenido
 * @property string $fecha_ingreso
 * @property string $fecha_egreso
 * @property string $observacion
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $verificado
 *
 * The followings are the available model relations:
 * @property InstitucionEducativa $institucionEducativa
 * @property NivelEstudio $nivelEstudio
 * @property EspecialidadEstudio $especialidadEstudio
 * @property TalentoHumano $talentoHumano
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 */
class EstudioRealizado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gestion_humana.estudio_realizado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('talento_humano_id, condicion_nominal_id, nivel_estudio_id, institucion_educativa_id, especialidad_estudio_id, titulo_obtenido, fecha_ingreso, usuario_ini_id, estatus', 'required'),
			array('nivel_estudio_id, institucion_educativa_id, especialidad_estudio_id, usuario_ini_id, usuario_act_id, verificado', 'numerical', 'integerOnly'=>true),
			array('titulo_obtenido', 'length', 'max'=>100),
			array('estatus', 'length', 'max'=>1),
			array('estatus', 'in', 'range'=>array('A', 'P', 'T', 'N', 'C'), 'allowEmpty'=>false, 'strict'=>true,),
			array('usuario_ini_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'insert'),
			array('usuario_act_id', 'default', 'value'=>Yii::app()->user->id, 'on'=>'update'),
                        array('titulo_obtenido, observacion', 'checkTieneGroserias'),
                        array('nivel_estudio_id', 'checkNivelEstudio'),
                        array('institucion_educativa_id', 'checkInstitucionEducativa'),
                        array('especialidad_estudio_id', 'checkEspecialidadEstudio'),
                        array('fecha_egreso', 'checkFechaEgreso'),
                        array('fecha_ingreso, fecha_egreso', 'checkFechaValida'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, talento_humano_id, condicion_nominal_id, nivel_estudio_id, institucion_educativa_id, especialidad_estudio_id, titulo_obtenido, fecha_ingreso, fecha_egreso, observacion, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, verificado', 'safe', 'on'=>'search'),
		);
	}

        public function checkTieneGroserias($attribute, $params=null){
            $datosLimipios = strtr($this->$attribute, Helper::palabrasBaneadas());
            if($datosLimipios!=$this->$attribute){
                $this->addError($attribute, 'El campo posee palabras no permitidas, por favor corrija el error y evite ser expulsado del sistema.');
            }
        }

        public function checkNivelEstudio($attribute, $params=null){
            if(!is_numeric($this->$attribute) || !CNivelEstudio::getData('id', $this->$attribute)){
                $this->addError($attribute, 'El Nivel de Estudio seleccionado no es válido.');
            }
        }

        public function checkEspecialidadEstudio($attribute, $param=null) {
            if(!is_numeric($this->$attribute) || !CEspecialidadEstudio::getData('id', $this->$attribute)){
                $this->addError($attribute, 'La Especialidad de Estudio seleccionada no es válida.');
            }
        }

        public function checkInstitucionEducativa($attribute, $param=null) {
            if(!is_numeric($this->$attribute) || !CInstitucionEducativa::getData('id', $this->$attribute)){
                $this->addError($attribute, 'La Institución Educativa seleccionada no es válida.');
            }
        }
        
        public function checkFechaEgreso($attribute, $param=null){
            if(!in_array($this->estatus, array('T', 'N', 'C')) && strlen($this->$attribute)>0){
                $this->addError($attribute, 'Para efectuar el registro de la Fecha de Egreso debe indicar en el Campo Estatus alguno de los valores "Culminado, En Espera de Título ó En Espera de Notas Certificadas" con respecto al Estudio Realizado.');
            }
        }
        
        public function checkFechaValida($attribute, $params=null){
            if(!Helper::isValidDate($this->$attribute, 'y-m-d') && strlen($this->$attribute)>0){
                $field = strtr($attribute, 'fecha_', '');
                $this->addError($attribute, 'La fecha de '.$field.' indicada no es válida.');
            }
        }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'institucionEducativa' => array(self::BELONGS_TO, 'InstitucionEducativa', 'institucion_educativa_id'),
			'nivelEstudio' => array(self::BELONGS_TO, 'NivelEstudio', 'nivel_estudio_id'),
			'especialidadEstudio' => array(self::BELONGS_TO, 'EspecialidadEstudio', 'especialidad_estudio_id'),
			'talentoHumano' => array(self::BELONGS_TO, 'TalentoHumano', 'talento_humano_id'),
                        'condicionNominal' => array(self::BELONGS_TO, 'CondicionNominal', 'condicion_nominal_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'talento_humano_id' => 'Talento Humano',
			'condicion_nominal_id' => 'Condición Nominal',
			'nivel_estudio_id' => 'Nivel de Estudio',
			'institucion_educativa_id' => 'Institución Educativa',
			'especialidad_estudio_id' => 'Especialidad de Estudio',
			'titulo_obtenido' => 'Título Obtenido o a Obtener',
			'fecha_ingreso' => 'Fecha de Ingreso',
			'fecha_egreso' => 'Fecha de Egreso',
			'observacion' => 'Observación',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus', // A = En Curso; P = En Pasantías; T = En Espera del Título; N = En Espera de Notas Certificadas; C = Culminado;
			'verificado' => 'Verificado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		if(strlen($this->id)>0) $criteria->compare('id',$this->id,true);
		if(strlen($this->talento_humano_id)>0) $criteria->compare('talento_humano_id',$this->talento_humano_id,true);
		if(strlen($this->condicion_nominal_id)>0) $criteria->compare('condicion_nominal_id',$this->condicion_nominal_id,true);
		if(is_numeric($this->nivel_estudio_id)) $criteria->compare('nivel_estudio_id',$this->nivel_estudio_id);
		if(is_numeric($this->institucion_educativa_id)) $criteria->compare('institucion_educativa_id',$this->institucion_educativa_id);
		if(is_numeric($this->especialidad_estudio_id)) $criteria->compare('especialidad_estudio_id',$this->especialidad_estudio_id);
		if(strlen($this->titulo_obtenido)>0) $criteria->compare('titulo_obtenido',$this->titulo_obtenido,true);
		if(Utiles::isValidDate($this->fecha_ingreso, 'y-m-d')) $criteria->compare('fecha_ingreso',$this->fecha_ingreso);
		// if(strlen($this->fecha_ingreso)>0) $criteria->compare('fecha_ingreso',$this->fecha_ingreso,true);
		if(Utiles::isValidDate($this->fecha_egreso, 'y-m-d')) $criteria->compare('fecha_egreso',$this->fecha_egreso);
		// if(strlen($this->fecha_egreso)>0) $criteria->compare('fecha_egreso',$this->fecha_egreso,true);
		if(strlen($this->observacion)>0) $criteria->compare('observacion',$this->observacion,true);
		if(is_numeric($this->usuario_ini_id)) $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		if(Utiles::isValidDate($this->fecha_ini, 'y-m-d')) $criteria->compare('fecha_ini',$this->fecha_ini);
		// if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
		if(is_numeric($this->usuario_act_id)) $criteria->compare('usuario_act_id',$this->usuario_act_id);
		if(Utiles::isValidDate($this->fecha_act, 'y-m-d')) $criteria->compare('fecha_act',$this->fecha_act);
		// if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
		if(Utiles::isValidDate($this->fecha_elim, 'y-m-d')) $criteria->compare('fecha_elim',$this->fecha_elim);
		// if(strlen($this->fecha_elim)>0) $criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus, array('A', 'I', 'E'))) $criteria->compare('estatus',$this->estatus,true);
		if(is_numeric($this->verificado)) $criteria->compare('verificado',$this->verificado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        
        public function beforeInsert($talentoHumanoId, $condicionNominalId)
	{
            parent::beforeSave();
            $this->fecha_ini = date('Y-m-d H:i:s');
            $this->usuario_ini_id = Yii::app()->user->id;
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeUpdate($talentoHumanoId, $condicionNominalId, $verificado)
	{
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            return true;
	}
        
        public function beforeDelete($talentoHumanoId=null, $condicionNominalId=null){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            // $this->fecha_eli = $this->fecha_act;
            $this->estatus = 'I';
            return true;
        }
        
        public function beforeActivate($talentoHumanoId=null, $condicionNominalId=null){
            parent::beforeSave();
            $this->fecha_act = date('Y-m-d H:i:s');
            $this->usuario_act_id = Yii::app()->user->id;
            $this->estatus = 'A';
            return true;
        }
        
        public function __toString() {
            try {
                return (string) $this->id;
            } catch (Exception $exception) {
                return $exception->getMessage();
            }
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EstudioRealizado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
