<?php

/**
 * This is the model class for table "gprueba.seccion_prueba".
 *
 * The followings are the available columns in table 'gprueba.seccion_prueba':
 * @property string $id
 * @property string $nombre
 * @property string $duracion_seccion
 * @property integer $cantidad_respuesta
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property integer $nivel_cargo_id
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $prueba_id
 * @property string $tipo_pregunta
 * @property integer $cantidad_sub_seccion
 * @property textstring $instruccion_seccion
 * @property integer $nivel_cargo_id
 *
 * The followings are the available model relations:
 * @property PruebaAplicacionPrueba[] $aplicacionPruebas
 * @property SubSeccionPrueba[] $subSeccionPruebas
 * @property Pregunta[] $preguntas
 * @property NivelCargo $nivelCargo
 * @property AperturaPeriodo $prueba
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 * @property Pregunta[] $preguntas
 * @property SubSeccionPrueba[] $subSeccionPruebas
 * @property NivelCargo $nivelCargoRespuestaPruebaAsp[] $respuestaPruebaAsps
 */

class SeccionPrueba extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gprueba.seccion_prueba';
    }

    public function getTipoPrueba($prueba_id = '') {
          
        if(empty($prueba_id))
        $lista = CHtml::listData(
        AperturaPeriodo::model()->findAll(array('condition' => "estatus='A' AND tipo_apertura_id = 1")), 'id', 'nombre_clave');
        else
        $lista = CHtml::listData(AperturaPeriodo::model()->findAll(array('condition' => "estatus='A' AND id = :prueba_id", 'params' => array('prueba_id'=>$prueba_id))), 'id', 'nombre_clave');
        //,array(':tipoPrueba'=>$tipoPrueba,':estatus'=>$estatus)

        return $lista;
    
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, duracion_seccion, usuario_ini_id, fecha_ini, estatus, prueba_id, tipo_pregunta, nivel_cargo_id, cantidad_respuesta', 'required'),
            array('cantidad_respuesta, usuario_ini_id, usuario_act_id, usuario_elim_id, prueba_id, cantidad_sub_seccion, nivel_cargo_id', 'numerical', 'integerOnly'=>true),
            array('nombre', 'length', 'max' => 150),
            array('fecha_ini, fecha_act, fecha_elim', 'length', 'max' => 30),
            array('estatus, tipo_pregunta', 'length', 'max' => 1),
            array('estatus', 'in', 'range'=>array('A', 'I', 'E'), 'allowEmpty'=>false, 'strict'=>true,),
            array('instruccion_seccion', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, nivel_cargo_id, duracion_seccion, cantidad_respuesta, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, usuario_elim_id, fecha_elim, estatus, prueba_id, tipo_pregunta, cantidad_sub_seccion, instruccion_seccion', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'aplicacionPruebas' => array(self::HAS_MANY, 'AplicacionPrueba', 'seccion_prueba_id'),
            'subSeccionPruebas' => array(self::HAS_MANY, 'SubSeccionPrueba', 'seccion_prueba_id'),
            'periodoApertura' => array(self::BELONGS_TO, 'AperturaPeriodo', 'prueba_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_act_id'),
            'usuarioElim' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_elim_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_ini_id'),
            'preguntas' => array(self::HAS_MANY, 'Pregunta', 'seccion_prueba_id'),
            'prueba' => array(self::BELONGS_TO, 'AperturaPeriodo', 'prueba_id'),
            'nivelCargo' => array(self::BELONGS_TO, 'NivelCargo', 'nivel_cargo_id'),
            'respuestaPruebaAsps' => array(self::HAS_MANY, 'RespuestaPruebaAsp', 'seccion_prueba_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Indique el Nombre de la Prueba a Realizar',
            'duracion_seccion' => 'Duración de la Prueba',
            'cantidad_respuesta' => 'Cantidad de Opciones por Pregunta',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'usuario_elim_id' => 'Usuario Elim',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'nivel_cargo_id' => 'Seleccione el Nivel del Cargo al que se le aplicará la Prueba',
            'prueba_id' => 'Seleccione el Proceso Aperturado en el cual aplicará la Prueba',
            'tipo_pregunta' => 'Indique el Tipo de Preguntas',
            'cantidad_sub_seccion' => 'Cantidad de Sub-secciones',
            'instruccion_seccion' => 'Instrucciones'
        );
    }

    /**
     * @param $pruebaId Id de la prueba
     * @param $nombre   Nombre de la sección
     * @return array|CActiveRecord|mixed|null
     */
    public function existeSeccionPrueba($pruebaId, $nombre) {
        $criteria = new CDbCriteria();

        $criteria->addInCondition('estatus', array('A'));
        $criteria->addInCondition('nombre', array($nombre));
        $criteria->addInCondition('prueba_id', array($pruebaId));

        return $this->findAll($criteria);
    }

    /**
     * @param $pruebaId Id de la prueba
     * @return array|CActiveRecord|mixed|null
     */
    public function getSeccionesPrueba($pruebaId) {
        $criteria = new CDbCriteria();

        $criteria->addInCondition('prueba_id', array($pruebaId));
        $criteria->order = 'estatus, nombre ASC';

        return $this->findAll($criteria);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($apertura = '') {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array(
            'periodoApertura' => array('alias'=>'pa', 'select'=>'pa.id , pa.nombre_clave, pa.periodo_nombre, pa.tipo_apertura_id, pa.fecha_inicio, pa.fecha_final',),
        );

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.nombre', $this->nombre, true);

        $criteria->compare('t.cantidad_respuesta', $this->cantidad_respuesta);
        $criteria->compare('t.usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('t.fecha_ini', $this->fecha_ini, true);
        $criteria->compare('t.usuario_act_id', $this->usuario_act_id);
        $criteria->compare('t.fecha_act', $this->fecha_act, true);
        $criteria->compare('t.usuario_elim_id', $this->usuario_elim_id);
        $criteria->compare('t.fecha_elim', $this->fecha_elim, true);
        $criteria->compare('t.estatus', $this->estatus, true);
        $criteria->compare('t.prueba_id', $this->prueba_id);
        $criteria->compare('t.tipo_pregunta', $this->tipo_pregunta, true);
        $criteria->compare('t.cantidad_sub_seccion', $this->cantidad_sub_seccion);
        $criteria->compare('t.instruccion_seccion', $this->instruccion_seccion, true);
        $criteria->compare('t.prueba_id',$apertura);
        if ($this->duracion_seccion != '' and $this->duracion_seccion != '__:__:__')
            $criteria->compare('duracion_seccion', $this->duracion_seccion/* , true */);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getSeccionesByPrueba($pruebaId) {
        /*$sql = "SELECT sp.* FROM gprueba.seccion_prueba sp
            INNER JOIN gprueba.prueba p ON p.id = sp.prueba_id
            WHERE sp.id NOT IN(SELECT DISTINCT(sp.id) FROM gprueba.seccion_prueba sp
            INNER JOIN gprueba.sub_seccion_prueba ssp ON
            ssp.seccion_prueba_id = sp.id) AND p.tipo_prueba = 2 AND p.id = $tipo_prueba_id AND tipo_pregunta IN ('T', 'I', 'D');";
        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->queryAll();*/
        $resultado = array();
        if(is_numeric($pruebaId)){
            $resultado = SeccionPrueba::model()->findAll('prueba_id = :pruebaId', array(':pruebaId'=>$pruebaId));
        }
        return $resultado;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SeccionPrueba the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getTotalSubSecciones($id) {
        $sql = "
        select count(*) from gprueba.sub_seccion_prueba as ssp
                        INNER JOIN gprueba.seccion_prueba as sp on (ssp.seccion_prueba_id=sp.id) where sp.id='$id'; ";
        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function getVerificarEstatus($id) {
        $sql = "select count(ap.id)
                    from gprueba.seccion_prueba as sp
                    INNER JOIN gprueba.prueba as p
                            on ( sp.prueba_id=p.id)
                    INNER JOIN gprueba.aplicacion_prueba as ap
                            on ( ap.prueba_id=sp.prueba_id)
                         where  sp.id='$id' ";

        $consulta = Yii::app()->db->createCommand($sql);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function datosSeccionPruebaOld($prueba_id) {
        $estatus = 'A';
        $prueba_id = (int) $prueba_id;
        $sql = "SELECT sp.id AS seccion_id, sp.nombre AS nombre_seccion
                    FROM gprueba.seccion_prueba sp
                    INNER JOIN gprueba.prueba p ON(p.id = sp.prueba_id)
                    WHERE sp.estatus=:estatus
                    AND p.id=:prueba_id
                    ORDER BY nombre_seccion ASC";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }
    
 public function spValidacionPrueba($postulacionId, $userId, $username, $modulo, $ipaddress, $action, $horaActual, $horaFinalPrueba){
        $sql=  'SELECT * FROM  gprueba.puede_presentar_examen(                   
                    :postulacion_id_vi:: BIGINT,
                    :usuario_id_vi:: BIGINT,
                    :username_vi :: CHARACTER VARYING,
                    :modulo_vi :: CHARACTER VARYING,
                    :ipaddress_vi:: CHARACTER VARYING,
                    :action_vi::CHARACTER VARYING,
                    :horaActual_vi::TIME,
                    :horaFinalPrueba_vi::TIME
               
                ) AS f(
                    mensaje text,
                    resultado character varying,    
                    codigo_resultado character varying,
                    id_prueba BIGINT,
                    aplicacion_prueba_id BIGINT,
                    talento_humano_id  BIGINT 
                )';
        $conection = Yii::app()->db;
        $query = $conection->createCommand($sql);
        
        $query->bindParam(':postulacion_id_vi', $postulacionId, PDO::PARAM_INT);
        $query->bindParam(':usuario_id_vi', $userId, PDO::PARAM_INT);
        $query->bindParam(':username_vi', $username, PDO::PARAM_STR);
        $query->bindParam(':modulo_vi', $modulo, PDO::PARAM_STR);
        $query->bindParam(':ipaddress_vi', $ipaddress, PDO::PARAM_STR);
        $query->bindParam(':action_vi', $action, PDO::PARAM_STR);
        $query->bindParam(':horaActual_vi', $horaActual, PDO::PARAM_STR);
         $query->bindParam(':horaFinalPrueba_vi', $horaFinalPrueba, PDO::PARAM_STR);
        $queryResponse = $query->queryRow();
        return $queryResponse;
    }
    
 public function datosCompletosPrueba($prueba_id) {

        $estatus = 'A';
        $sql = "SELECT s.id AS seccion_id, s.prueba_id, s.nombre AS nombre_seccion, s.duracion_seccion, s.cantidad_respuesta, 
                s.tipo_pregunta, s.cantidad_sub_seccion, s.instruccion_seccion,
                COALESCE(p.id, null) AS pregunta_id, COALESCE(p.nombre, '') AS nombre_pregunta, 
                COALESCE(p.seccion_prueba_id, null) AS preg_seccion_prueba_id, COALESCE(p.sub_seccion_prueba_id, null) AS preg_sub_seccion_prueba_id,
                COALESCE(r.id, null) AS respuesta_id, COALESCE(r.nombre, '') AS nombre_opcion, 
                COALESCE(r.resp_correcta, null) AS respuesta_correcta, COALESCE(r.pregunta_id, null) AS opcion_pregunta,
                s.nombre AS nombre_prueba, p.ponderacion 
                FROM gprueba.seccion_prueba s
                INNER JOIN gprueba.pregunta p ON (p.seccion_prueba_id = s.id) 
                INNER JOIN gprueba.respuesta r ON (r.pregunta_id = p.id)
                WHERE s.id =
                AND s.estatus='A'
                AND p.estatus='A'
                AND r.estatus='A'
                order by(p.id, s.id);
                ";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
        $result = $consulta->queryAll();
        return $result;
    }
    
    public function datosSeccionPrueba($prueba_id) {
        $estatus = 'A';
        $prueba_id = (int) $prueba_id;
        $sql = "SELECT sp.id AS seccion_id, sp.nombre AS nombre_seccion
                    FROM gprueba.seccion_prueba sp
                    WHERE sp.estatus=:estatus
                    AND sp.id=:prueba_id
                    ORDER BY nombre_seccion ASC";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":prueba_id", $prueba_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        $resultado = $consulta->queryAll();
        return $resultado;
    }
}
