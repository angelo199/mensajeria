<?php

/**
 * This is the model class for table "gestion_humana.postulacion".
 *
 * The followings are the available columns in table 'gestion_humana.postulacion':
 * @property integer $id
 * @property integer $talento_humano_id
 * @property integer $condicion_nominal_id
 * @property integer $diversidad_funcional_id
 * @property string $periodo_evaluacion
 * @property integer $cargo_evaluacion_id
 * @property integer $dependencia_evaluacion_id
 * @property string $dependencia_evaluacion
 * @property string $supervisor_inmediato
 * @property integer $rango_actuacion_id
 * @property integer $puntuacion_obtenida
 * @property integer $apertura_periodo_id
 * @property integer $estado_id
 * @property integer $dependencia_postulado_id
 * @property string $dependencia_general_postulado
 * @property integer $clase_cargo_id
 * @property integer $cargo_postulado_id
 * @property string $estatus
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_eli
 * @property integer $estatus_aprobado
 * @property integer $puntuacion_desempeno
 * @property string $codigo_constancia
 * @property string $documentos_consignado
 * @property string $observacion
 * @property string $puntuacion_prueba
 * @property integer $horas_totales_cursos
 * @property integer $autoridad_vigente_deingreso_id
 * @property integer $autoridad_vigente_degestionhumana_id
 * @property integer $autoridad_vigente_derrhh_id
 * @property integer $estatus_anterior_aprobado
 * @property string $origen_validador
 * @property integer $cedula_validador
 * @property string $nombre_validador
 *
 * The followings are the available model relations:
 * @property Dependencia $dependenciaEvaluacion
 * @property Estado $estado
 * @property AperturaPeriodo $aperturaPeriodo
 * @property CargoNominal $cargoEvaluacion
 * @property CargoVacante $cargoPostulado
 * @property ClaseCargo $claseCargo
 * @property CondicionNominal $condicionNominal
 * @property Dependencia $dependenciaPostulado
 * @property EstatusPostulacion $estatusAprobado
 * @property RangoActuacion $rangoActuacion
 * @property TalentoHumano $talentoHumano
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property EstatusPostulacion $estatusAnteriorAprobado
 * @property Autoridad $autoridadVigenteDeIngreso
 * @property Autoridad $autoridadVigenteDeGestionHumana
 * @property Autoridad $autoridadVigenteDeRrhh
 */
class Postulacion extends CActiveRecord {

    const PROCESO = 'POSTULACION';
    const NOMBRE_PROCESO = 'Postulación';

    const PERSONA_EXTERNA = 0;

    public $cedula = '';
    public $dependencia = '';
    public $origen = '';
    public $cargoPostuladoId = '';
    public $estadoId = '';
    public $cargoVacanteDependencia = '';
    public $aperturaNombre = '';
    public $nombre_proceso_periodo = '';

    /**
     *
     * @var int entrevistafinalizada 0 or 1
     */
    public $finalizada = '';

    const FINALIZADA_EVAL_REQ_MINIMOS = 1;
    const FINALIZADA_ENTREVISTA = 2;
    const FINALIZADA_NO = 0;

    /**
     *
     * @var int evaluacionRealizada 0 or 1
     */
    public $evaluacionRealizada = '';
    public $puntuacion_total_obtenida = '- - -';
    public $cachePostulacionIndexModel = 'PX:MODEL:{id}';
    
    /**
     *
     * @var string POSTULACION:TH:PA:{talentoHumanoId}:{periodoAperturadoId} 
     */
    public $cachePostulacionId = 'POSTULACION:TH:PA:{talentoHumanoId}:{periodoAperturadoId}';

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gestion_humana.postulacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('talento_humano_id, condicion_nominal_id, apertura_periodo_id, estado_id, clase_cargo_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, cargo_postulado_id', 'required'),
            array('periodo_evaluacion, rango_actuacion_id, supervisor_inmediato, puntuacion_obtenida', 'required', 'on' => 'merito'),
            array('supervisor_inmediato', 'supervisorRequerido'),
            array('talento_humano_id, condicion_nominal_id, cargo_evaluacion_id, dependencia_evaluacion_id, rango_actuacion_id, puntuacion_obtenida, apertura_periodo_id, estado_id, dependencia_postulado_id, clase_cargo_id, cargo_postulado_id, usuario_ini_id, usuario_act_id, puntuacion_desempeno, estatus_aprobado, horas_totales_cursos, autoridad_vigente_deingreso_id, estatus_anterior_aprobado, autoridad_vigente_degestionhumana_id, autoridad_vigente_derrhh_id, cedula_validador, estadoId', 'numerical', 'integerOnly' => true),
            array('periodo_evaluacion', 'length', 'max' => 40),
            array('nombre_validador', 'length', 'max' => 200),
            array('dependencia_evaluacion, supervisor_inmediato, dependencia_general_postulado', 'length', 'max' => 120),
            array('estatus, origen_validador', 'length', 'max' => 1),
            array('codigo_constancia', 'length', 'max' => 50),
            array('cargoVacanteDependencia, cargoPostuladoId', 'length', 'max' => 500),
            array('estatus', 'in', 'range' => array('A', 'I', 'E'), 'allowEmpty' => false, 'strict' => true,),
            array('usuario_ini_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'insert'),
            array('usuario_act_id', 'default', 'value' => Yii::app()->user->id, 'on' => 'update'),
            //array('puntuacion_obtenida', 'numerical', 'min'=>100, 'max'=>500, 'on'=>'Administrativo'),
            //array('puntuacion_obtenida', 'numerical', 'min'=>50, 'max'=>100, 'on'=>'Obrero'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, talento_humano_id, condicion_nominal_id, periodo_evaluacion, cargo_evaluacion_id, dependencia_evaluacion_id, dependencia_evaluacion, supervisor_inmediato, rango_actuacion_id, puntuacion_obtenida, apertura_periodo_id, estado_id, dependencia_postulado_id, dependencia_general_postulado, clase_cargo_id, cargo_postulado_id, estatus, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_eli, puntuacion_desempeno, estatus_aprobado, codigo_constancia, documentos_consignado, observacion, puntuacion_prueba, horas_totales_cursos, autoridad_vigente_deingreso_id, estatus_anterior_aprobado, autoridad_vigente_degestionhumana_id, autoridad_vigente_derrhh_id, origen, cedula, estadoId, cargoVacanteDependencia, cargoPostuladoId, origen_validador, cedula_validador, nombre_validador', 'safe', 'on' => 'search'),
                //array('cargo_postulado_id, talento_humano_id, apertura_periodo_id', 'ECompositeUniqueValidator', 'attributesToAddError'=>'cargo_postulado_id', 'message'=>'Usted ya ha sido postulado para este cargo, puede postularse para otro cargo o esperar a que se realice la evaluación'),
        );
    }

    /**
     *
     * @param type $attribute
     * @param type $params
     * @param type $params
     */
    public function supervisorRequerido($attribute, $params) {
        if (strlen($this->$attribute) == 0 && $this->condicion_nominal_id != $this::PERSONA_EXTERNA) {
            $this->addError($attribute, 'Es requerido el nombre del Supervisor Inmediato');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'dependenciaEvaluacion' => array(self::BELONGS_TO, 'Dependencia', 'dependencia_evaluacion_id'),
            'estado' => array(self::BELONGS_TO, 'Estado', 'estado_id'),
            'aperturaPeriodo' => array(self::BELONGS_TO, 'AperturaPeriodo', 'apertura_periodo_id'),
            'cargoEvaluacion' => array(self::BELONGS_TO, 'CargoNominal', 'cargo_evaluacion_id'),
            'cargoPostulado' => array(self::BELONGS_TO, 'CargoVacante', 'cargo_postulado_id'),
            'claseCargo' => array(self::BELONGS_TO, 'ClaseCargo', 'clase_cargo_id'),
            'condicionNominal' => array(self::BELONGS_TO, 'CondicionNominal', 'condicion_nominal_id'),
            'dependenciaPostulado' => array(self::BELONGS_TO, 'Dependencia', 'dependencia_postulado_id'),
            'estatusAprobado' => array(self::BELONGS_TO, 'EstatusPostulacion', 'estatus_aprobado'),
            'rangoActuacion' => array(self::BELONGS_TO, 'RangoActuacion', 'rango_actuacion_id'),
            'talentoHumano' => array(self::BELONGS_TO, 'TalentoHumano', 'talento_humano_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'estatusAnteriorAprobado' => array(self::BELONGS_TO, 'EstatusPostulacion', 'estatus_anterior_aprobado'),
            'entrevistaPostulacion' => array(self::HAS_ONE, 'EntrevistaPostulacion', 'postulacion_id'),
            'autoridadVigenteDeIngreso' => array(self::BELONGS_TO, 'Autoridad', 'autoridad_vigente_deingreso_id'),
            'autoridadVigenteDeGestionHumana' => array(self::BELONGS_TO, 'Autoridad', 'autoridad_vigente_degestionhumana_id'),
            'autoridadVigenteDeRrhh' => array(self::BELONGS_TO, 'Autoridad', 'autoridad_vigente_derrhh_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'talento_humano_id' => 'Talento Humano',
            'condicion_nominal_id' => 'Condicion Nominal Actual',
            'periodo_evaluacion' => 'Periodo de Evaluación de Desempeño',
            'cargo_evaluacion_id' => 'Cargo de Evaluacion',
            'dependencia_evaluacion_id' => 'Dependencia de Evaluación',
            'dependencia_evaluacion' => 'Dependencia de Evaluación',
            'supervisor_inmediato' => 'Nombre del Supervisor Inmediato Actual',
            'rango_actuacion_id' => 'Rango de Actuación de Desempeño',
            'puntuacion_obtenida' => 'Puntuación Obtenida del Desempeño',
            'apertura_periodo_id' => 'Apertura de Periodo',
            'estado_id' => 'Estado',
            'dependencia_postulado_id' => 'Dependencia del Postulado',
            'dependencia_general_postulado' => 'Dependencia General del Postulado',
            'clase_cargo_id' => 'Clase del Cargo',
            'cargo_postulado_id' => 'Cargo del Postulado',
            'estatus' => 'Estatus',
            'puntuacion_desempeno' => 'Puntuación Desempeno',
            'estatus_aprobado' => 'Este campo contiene los estados de la postulacion',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_eli' => 'Fecha Eli',
            'codigo_constancia' => 'Código Constancia',
            'documentos_consignado' => 'Campo que contiene un JSON de las opciones marcadas en la aprobacion de la postulacion.',
            'observacion' => 'Campo que contendra la razon por la cual la postulacion no fue aprobada.',
            'puntuacion_prueba' => 'Puntuación de Prueba de Conocimiento',
            'horas_totales_cursos' => 'Permite capturar el numero de horas totales que realizo el postulado en la postulación.',
            'autoridad_vigente_deingreso_id' => 'Autoridad Vigente de Ingreso y Clasificación',
            'autoridad_vigente_degestionhumana_id' => 'Autoridad vigente de la Oficina de Gestión Humana',
            'autoridad_vigente_derrhh_id' => 'Autoridad vigente de RRHH',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @param integer $tipoAperturaProceso Id del Tipo de Periodo o Proceso Aperturado Actualmente.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($tipoAperturaProceso = null, $aperturaId = null) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $this->cedula = strtr($this->cedula, array('_'=>''));
        if(!is_numeric($this->cedula) && strlen($this->cedula)>0){
            $cedulaCompletaArr = explode('-', $this->cedula);
            if(count($cedulaCompletaArr)>1){
                $this->origen = (isset($cedulaCompletaArr[0]))?$cedulaCompletaArr[0]:null;
                $this->cedula = (isset($cedulaCompletaArr[1]))?$cedulaCompletaArr[1]:null;
            }
        }

        $criteria = new CDbCriteria;

        $criteria->select = "t.id, t.talento_humano_id, t.condicion_nominal_id, t.periodo_evaluacion, t.cargo_evaluacion_id, t.dependencia_evaluacion_id, t.dependencia_evaluacion, t.supervisor_inmediato, t.rango_actuacion_id, t.puntuacion_obtenida, t.apertura_periodo_id, t.estado_id, t.dependencia_postulado_id, t.dependencia_general_postulado, t.clase_cargo_id, t.cargo_postulado_id, t.estatus, t.estatus_aprobado, t.puntuacion_desempeno, '$tipoAperturaProceso' AS nombre_proceso_periodo ";

        $criteria->with = array(
            'talentoHumano' => array('select' => 'th.origen, th.cedula, th.nombre, th.apellido, th.dependencia', 'alias' => 'th'),
            'cargoPostulado' => array('select' => 'cv.codnomin, cv.descripcion, cv.estado_id, cv.coddenpen, cv.nomdenpen', 'alias' => 'cv'),
            'claseCargo' => array('select' => 'cc.nombre', 'alias' => 'cc'),
            'entrevistaPostulacion' => array('select' => 'ep.puntuacion_total_obtenida, ep.finalizada, ep.puntuacion_req_minimos, puntuacion_entrevista', 'alias' => 'ep'),
            'aperturaPeriodo' => array('select' => 'ap.tipo_apertura_id', 'alias' => 'ap'),
            'condicionNominal' => array('select' => 'cn.nombre', 'alias' => 'cn'),
        );

        if ($tipoAperturaProceso == 'merito') {
            $criteria->compare('ap.tipo_apertura_id', AperturaPeriodo::SISTEMADEMERITO);
            $criteria->order = 'cv.estado_id ASC, t.cargo_postulado_id ASC';
        } elseif ($tipoAperturaProceso == 'concurso') {
            $criteria->compare('ap.tipo_apertura_id', AperturaPeriodo::CONCURSOPUBLICO);
            if (in_array($this->estatus_aprobado, array(EstatusPostulacion::BENEFICIADO, EstatusPostulacion::NO_BENEFICIADO)) && is_numeric($this->estatus_aprobado)) {
                $criteria->order = 't.cargo_postulado_id ASC, t.puntuacion_desempeno DESC';
            }
        }

        if ($aperturaId != null) {
            $criteria->compare('t.apertura_periodo_id', $aperturaId);
        }
        //$criteria->order = $criteria->order.' t.fecha_act DESC';
        if ((is_numeric($this->finalizada) && in_array($this->finalizada, array(self::FINALIZADA_NO, self::FINALIZADA_ENTREVISTA, self::FINALIZADA_EVAL_REQ_MINIMOS))) ) {
            //$this->estatus_aprobado = 1;
            $criteria->compare('ep.finalizada', $this->finalizada);
            $criteria->order = 'ep.puntuacion_total_obtenida DESC';
        }

        if (is_numeric($this->id))
            $criteria->compare('id', $this->id);
        if (is_numeric($this->cedula))
            $criteria->compare('th.cedula', $this->cedula);
        if (is_numeric($this->estatus_aprobado))
            $criteria->compare('t.estatus_aprobado', $this->estatus_aprobado);
        if (in_array($this->origen, array('V', 'P', 'E')))
            $criteria->compare('th.origen', $this->origen);
        if (strlen($this->cargoPostuladoId) > 0)
            $criteria->compare('cv.codnomin||\' - \'||cv.descripcion', $this->cargoPostuladoId, true);
        if (is_numeric($this->estatus_aprobado))
            $criteria->compare('estatus_aprobado', $this->estatus_aprobado);
        if (strlen($this->cargoVacanteDependencia) > 0)
            $criteria->compare('cv.coddenpen||\' - \'||cv.nomdenpen', $this->cargoVacanteDependencia, true);
        // if($this->dependencia)$criteria->compare('cv.coddenpen||\' - \'||cv.nomdenpen',$this->dependencia, true);
        if (is_numeric($this->estadoId))
            $criteria->compare('cv.estado_id', $this->estadoId);

        if (is_numeric($this->condicion_nominal_id))
            $criteria->compare('t.condicion_nominal_id', $this->condicion_nominal_id);
        if (strlen($this->periodo_evaluacion) > 0)
            $criteria->compare('t.periodo_evaluacion', $this->periodo_evaluacion, true);
        if (is_numeric($this->cargo_evaluacion_id))
            $criteria->compare('t.cargo_evaluacion_id', $this->cargo_evaluacion_id);
        if (is_numeric($this->dependencia_evaluacion_id))
            $criteria->compare('t.dependencia_evaluacion_id', $this->dependencia_evaluacion_id);
        if (strlen($this->dependencia_evaluacion) > 0)
            $criteria->compare('t.dependencia_evaluacion', $this->dependencia_evaluacion, true);
        if (strlen($this->supervisor_inmediato) > 0)
            $criteria->compare('t.supervisor_inmediato', $this->supervisor_inmediato, true);
        if (is_numeric($this->rango_actuacion_id))
            $criteria->compare('t.rango_actuacion_id', $this->rango_actuacion_id);
        if (is_numeric($this->puntuacion_obtenida))
            $criteria->compare('t.puntuacion_obtenida', $this->puntuacion_obtenida);
        //if(is_numeric($this->apertura_periodo_id)) $criteria->compare('apertura_periodo_id',$this->apertura_periodo_id);
        if (is_numeric($this->estado_id))
            $criteria->compare('t.estado_id', $this->estado_id);
        if (is_numeric($this->dependencia_postulado_id))
            $criteria->compare('t.dependencia_postulado_id', $this->dependencia_postulado_id);
        if (strlen($this->dependencia_general_postulado) > 0)
            $criteria->compare('t.dependencia_general_postulado', $this->dependencia_general_postulado, true);
        if (is_numeric($this->clase_cargo_id))
            $criteria->compare('t.clase_cargo_id', $this->clase_cargo_id);
        if (is_numeric($this->cargo_postulado_id))
            $criteria->compare('t.cargo_postulado_id', $this->cargo_postulado_id);
        if (in_array($this->estatus, array('A', 'I', 'E')))
            $criteria->compare('t.estatus', $this->estatus, true);
        if (is_numeric($this->usuario_ini_id))
            $criteria->compare('t.usuario_ini_id', $this->usuario_ini_id);
        if (Utiles::isValidDate($this->fecha_ini, 'y-m-d'))
            $criteria->compare('t.fecha_ini', $this->fecha_ini);
        // if(strlen($this->fecha_ini)>0) $criteria->compare('fecha_ini',$this->fecha_ini,true);
        if (is_numeric($this->usuario_act_id))
            $criteria->compare('t.usuario_act_id', $this->usuario_act_id);
        if (Utiles::isValidDate($this->fecha_act, 'y-m-d'))
            $criteria->compare('t.fecha_act', $this->fecha_act);
        // if(strlen($this->fecha_act)>0) $criteria->compare('fecha_act',$this->fecha_act,true);
        if (Utiles::isValidDate($this->fecha_eli, 'y-m-d'))
            $criteria->compare('t.fecha_eli', $this->fecha_eli);
        // if(strlen($this->fecha_eli)>0) $criteria->compare('fecha_eli',$this->fecha_eli,true);
        if (is_numeric($this->puntuacion_desempeno))
            $criteria->compare('t.puntuacion_desempeno', $this->puntuacion_desempeno);
        if (strlen($this->codigo_constancia) > 0)
            $criteria->compare('t.codigo_constancia', $this->codigo_constancia, true);
        if (strlen($this->documentos_consignado) > 0)
            $criteria->compare('t.documentos_consignado', $this->documentos_consignado, true);
        if (strlen($this->observacion) > 0)
            $criteria->compare('t.observacion', $this->observacion, true);
        if (strlen($this->puntuacion_prueba) > 0)
            $criteria->compare('t.puntuacion_prueba', $this->puntuacion_prueba, true);
        if (is_numeric($this->horas_totales_cursos))
            $criteria->compare('t.horas_totales_cursos', $this->horas_totales_cursos);
        if (is_numeric($this->autoridad_vigente_deingreso_id))
            $criteria->compare('t.autoridad_vigente_deingreso_id', $this->autoridad_vigente_deingreso_id);
        if (is_numeric($this->estatus_anterior_aprobado))
            $criteria->compare('t.estatus_anterior_aprobado', $this->estatus_anterior_aprobado);
        if (is_numeric($this->autoridad_vigente_degestionhumana_id))
            $criteria->compare('t.autoridad_vigente_degestionhumana_id', $this->autoridad_vigente_degestionhumana_id);
        if (is_numeric($this->autoridad_vigente_derrhh_id))
            $criteria->compare('t.autoridad_vigente_derrhh_id', $this->autoridad_vigente_derrhh_id);
        if (is_numeric($this->estatus_anterior_aprobado))
            $criteria->compare('t.estatus_anterior_aprobado', $this->estatus_anterior_aprobado);

        //var_dump($this->estadoId."-L:".__LINE__);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function aprobarPostulacion($idPostulacion, $respuestas, $estatusPostulacion, $observacion) {

        $idUsuario = Yii::app()->user->id;

        $sql = "SELECT * FROM baremo.crear_entrevista_postulacion(
                :idPostulacion::BIGINT,
                :idUsuario::BIGINT,
                :idUsuario::BIGINT,
                :respuestas::TEXT,
                :estatusPostulacion::SMALLINT,
                :observacion::TEXT
            ) AS f (
                codigo_resultado VARCHAR,
                estado_resultado VARCHAR,
                mensaje VARCHAR,
                entrevista_id BIGINT,
                origen VARCHAR,
                cedula INTEGER,
                seccion SMALLINT
            )";

        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":idPostulacion", $idPostulacion, PDO::PARAM_INT);
        $command->bindValue(":idUsuario", $idUsuario, PDO::PARAM_INT);
        $command->bindValue(":respuestas", $respuestas, PDO::PARAM_STR);
        $command->bindValue(":estatusPostulacion", $estatusPostulacion, PDO::PARAM_INT);
        $command->bindValue(":observacion", $observacion, PDO::PARAM_STR);
        $respuesta = $command->queryRow();

        return $respuesta;
    }

    public function insertarPostulacionTH($model, $tipoPeriodo) {

        $talentoHumanoId = ($model->talento_humano_id != null) ? $model->talento_humano_id : null;
        $aperturaPeriodoId = ($model->apertura_periodo_id != null) ? $model->apertura_periodo_id : null;
        $dependenciaEvaluacionId = ($model->dependencia_evaluacion_id != null) ? $model->dependencia_evaluacion_id : null;
        $dependenciaEvaluacion = $model->dependencia_evaluacion;
        $cargoPostuladoId = ($model->cargo_postulado_id != null) ? $model->cargo_postulado_id : null;
        $supervisorInmediato = $model->supervisor_inmediato;
        $periodoEvaluacion = $model->periodo_evaluacion;
        $rangoActuacionId = ($model->rango_actuacion_id != null) ? $model->rango_actuacion_id : null;
        $puntuacionObtenida = $model->puntuacion_obtenida;
        $estadoId = ($model->estado_id != null) ? $model->estado_id : null;
        $dependenciaPostuladoId = ($model->dependencia_postulado_id != null) ? $model->dependencia_postulado_id : null;
        $claseCargoId = ($model->clase_cargo_id != null) ? $model->clase_cargo_id : null;
        $cargoEvaluacionId = ($model->cargo_evaluacion_id != null) ? $model->cargo_evaluacion_id : null;
        $dependenciaGeneralPostulado = $model->dependencia_general_postulado;
        $condicionNominalId = ($model->condicion_nominal_id != null) ? $model->condicion_nominal_id : null;
        $fechaIni = $model->fecha_ini;
        $usuarioIniId = $model->usuario_ini_id;
        $fechaAct = $model->fecha_act;
        $usuarioActId = $model->usuario_act_id;

        $attributes = json_encode($model->attributes);

        $sql = "SELECT * FROM gestion_humana.insertar_postulacion_talento_humano(
                    :talentoHumanoId::INTEGER,
                    :condicionNominalId::INTEGER,
                    :periodoEvaluacion::VARCHAR,
                    :cargoEvaluacionId::INTEGER,
                    :dependenciaEvaluacionId::INTEGER,
                    :dependenciaEvaluacion::VARCHAR,
                    :supervisorInmediato::VARCHAR,
                    :rangoActuacionId::INTEGER,
                    :puntuacionObtenida::INTEGER,
                    :aperturaPeriodoId::INTEGER,
                    :estadoId::INTEGER,
                    :dependenciaPostuladoId::INTEGER,
                    :dependenciaGeneralPostulado::VARCHAR,
                    :claseCargoId::INTEGER,
                    :cargoPostuladoId::INTEGER,
                    :usuarioIniId::INTEGER,
                    :fechaIni::TIMESTAMP,
                    :usuarioActId::INTEGER,
                    :fechaAct::TIMESTAMP,
                    :dataAttributes::TEXT,
                    :tipoPeriodo::TEXT
                    ) AS f(
                        codigo_resultado VARCHAR,
                        estado_resultado VARCHAR,
                        mensaje VARCHAR,
                        postulacion_id INTEGER,
                        seccion SMALLINT
                    );";

        $command = Yii::app()->db->createCommand($sql);
        $command->bindValue(":talentoHumanoId", $talentoHumanoId, PDO::PARAM_INT);
        $command->bindValue(":aperturaPeriodoId", $aperturaPeriodoId, PDO::PARAM_INT);
        $command->bindValue(":dependenciaEvaluacionId", $dependenciaEvaluacionId, PDO::PARAM_INT);
        $command->bindValue(":dependenciaEvaluacion", $dependenciaEvaluacion, PDO::PARAM_STR);
        $command->bindValue(":cargoPostuladoId", $cargoPostuladoId, PDO::PARAM_INT);
        $command->bindValue(":supervisorInmediato", $supervisorInmediato, PDO::PARAM_STR);
        $command->bindValue(":periodoEvaluacion", $periodoEvaluacion, PDO::PARAM_STR);
        $command->bindValue(":rangoActuacionId", $rangoActuacionId, PDO::PARAM_INT);
        $command->bindValue(":puntuacionObtenida", $puntuacionObtenida, PDO::PARAM_INT);
        $command->bindValue(":estadoId", $estadoId, PDO::PARAM_INT);
        $command->bindValue(":dependenciaPostuladoId", $dependenciaPostuladoId, PDO::PARAM_INT);
        $command->bindValue(":claseCargoId", $claseCargoId, PDO::PARAM_INT);
        $command->bindValue(":cargoEvaluacionId", $cargoEvaluacionId, PDO::PARAM_INT);
        $command->bindValue(":dependenciaGeneralPostulado", $dependenciaGeneralPostulado, PDO::PARAM_STR);
        $command->bindValue(":condicionNominalId", $condicionNominalId, PDO::PARAM_INT);
        $command->bindValue(":fechaIni", $fechaIni, PDO::PARAM_STR);
        $command->bindValue(":usuarioIniId", $usuarioIniId, PDO::PARAM_INT);
        $command->bindValue(":fechaAct", $fechaAct, PDO::PARAM_STR);
        $command->bindValue(":usuarioActId", $usuarioActId, PDO::PARAM_INT);
        $command->bindValue(":dataAttributes", $attributes, PDO::PARAM_STR);
        $command->bindValue(":tipoPeriodo", $tipoPeriodo, PDO::PARAM_STR);
        $respuesta = $command->queryRow();

        return $respuesta;
    }

    /**
     *
     * @param type $talentoHumanoId
     * @param type $aperturaPeriodoId
     * @return type
     */
    public function existenciaPostulacion($talentoHumanoId, $aperturaPeriodoId) {

        $respuesta = null;
        if (is_numeric($talentoHumanoId) && is_numeric($aperturaPeriodoId)) {
            //POSTULACION:TH:PA:{talentoHumanoId}:{periodoAperturadoId}
            $cacheIndex = strtr($this->cachePostulacionId, array('{talentoHumanoId}' => $talentoHumanoId, '{periodoAperturadoId}' => $aperturaPeriodoId));
            $respuesta = Yii::app()->cache->get($cacheIndex);
            if (!$respuesta) {
                $sql = "SELECT id FROM gestion_humana.postulacion WHERE talento_humano_id = :talentoHumanoId AND apertura_periodo_id = :aperturaPeriodoId LIMIT 1";
                $command = Yii::app()->db->createCommand($sql);
                $command->bindValue(":aperturaPeriodoId", $aperturaPeriodoId, PDO::PARAM_INT);
                $command->bindValue(":talentoHumanoId", $talentoHumanoId, PDO::PARAM_INT);
                $respuesta = $command->queryScalar();
                if (is_numeric($respuesta)) {
                    Yii::app()->cache->set($cacheIndex, $respuesta, Helper::$SEGUNDOS_UN_MES);
                }
            }
        }

        return $respuesta;
    }

    public function beforeInsert() {
        parent::beforeSave();
        $this->estatus_aprobado = 0;
        $this->fecha_ini = date('Y-m-d H:i:s');
        $this->usuario_ini_id = Yii::app()->user->id;
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeUpdate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        return true;
    }

    public function beforeDelete() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        // $this->fecha_eli = $this->fecha_act;
        $this->estatus = 'I';
        return true;
    }

    public function beforeActivate() {
        parent::beforeSave();
        $this->fecha_act = date('Y-m-d H:i:s');
        $this->usuario_act_id = Yii::app()->user->id;
        $this->estatus = 'A';
        return true;
    }

    public function __toString() {
        try {
            return (string) $this->id;
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getModelPostulacion($id) {
        $cacheIndexPeriodo = str_replace('{id}', $id, $this->cachePostulacionIndexModel);

        $model = Yii::app()->cache->get($cacheIndexPeriodo);
        if (!$model) {
            //svar_dump('mosca');die();
            $model = $this->with('cargoPostulado', 'claseCargo')->findByPk($id);
            if ($model) {
                Yii::app()->cache->set($cacheIndexPeriodo, $model, Helper::$SEGUNDOS_UN_MES);
            }
        }
        return $model;
    }

    public function reloadCachePostulacion($id) {
        $cacheIndexPeriodo = str_replace('{id}', $id, $this->cachePostulacionIndexModel);
        $model = $this->with('cargoPostulado', 'claseCargo')->findByPk($id);
        if ($model) {
            Yii::app()->cache->set($cacheIndexPeriodo, $model, Helper::$SEGUNDOS_UN_MES);
        }
        return $model;
    }

    public function deleteCachePostulacion($id) {
        $cacheIndexGen = str_replace('{id}', $id, $this->cachePostulacionIndexModel);
        Yii::app()->cache->delete($cacheIndexGen);
        return true;
    }

    /**
     *
     * @param string $code (Los Valores permitidos son "concurso" ó "merito")
     */
    public function getNombreTipoProceso($code) {
        $result = null;
        if (in_array($code, array('concurso', 'merito'))) {
            $procesos = array(
                'concurso' => 'Concurso Público',
                'merito' => 'Sistema de Mérito'
            );
            $result = $procesos[$code];
        }
        return $result;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Postulacion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function can_postulate($attribute, $param = null) {

        $this->addError($attribute, 'Usted ya ha sido postulado para este cargo, puede postularse para otro cargo o esperar a que se realice la evaluación');
    }

}
