<?php

/**
 * This is the model class for table "tcargo".
 *
 * The followings are the available columns in table 'tcargo':
 * @property string $ccargo
 * @property string $dcargo
 * @property string $tpersona
 * @property integer $cgrado
 * @property string $msueldo
 * @property string $mcompensa
 * @property string $iprofesion
 * @property string $ijerarquia
 * @property string $imodalidad
 * @property string $icategoria
 * @property string $iturno
 * @property string $idedicacion
 * @property string $carea
 * @property integer $tmarca
 */
class Tcargo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tcargo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cgrado, tmarca', 'numerical', 'integerOnly'=>true),
			array('ccargo', 'length', 'max'=>6),
			array('dcargo', 'length', 'max'=>30),
			array('tpersona, iprofesion, ijerarquia, imodalidad, icategoria, iturno, idedicacion', 'length', 'max'=>1),
			array('msueldo', 'length', 'max'=>12),
			array('mcompensa', 'length', 'max'=>8),
			array('carea', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ccargo, dcargo, tpersona, cgrado, msueldo, mcompensa, iprofesion, ijerarquia, imodalidad, icategoria, iturno, idedicacion, carea, tmarca', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ccargo' => 'Ccargo',
			'dcargo' => 'Dcargo',
			'tpersona' => 'Tpersona',
			'cgrado' => 'Cgrado',
			'msueldo' => 'Msueldo',
			'mcompensa' => 'Mcompensa',
			'iprofesion' => 'Iprofesion',
			'ijerarquia' => 'Ijerarquia',
			'imodalidad' => 'Imodalidad',
			'icategoria' => 'Icategoria',
			'iturno' => 'Iturno',
			'idedicacion' => 'Idedicacion',
			'carea' => 'Carea',
			'tmarca' => 'Tmarca',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ccargo',$this->ccargo,true);
		$criteria->compare('dcargo',$this->dcargo,true);
		$criteria->compare('tpersona',$this->tpersona,true);
		$criteria->compare('cgrado',$this->cgrado);
		$criteria->compare('msueldo',$this->msueldo,true);
		$criteria->compare('mcompensa',$this->mcompensa,true);
		$criteria->compare('iprofesion',$this->iprofesion,true);
		$criteria->compare('ijerarquia',$this->ijerarquia,true);
		$criteria->compare('imodalidad',$this->imodalidad,true);
		$criteria->compare('icategoria',$this->icategoria,true);
		$criteria->compare('iturno',$this->iturno,true);
		$criteria->compare('idedicacion',$this->idedicacion,true);
		$criteria->compare('carea',$this->carea,true);
		$criteria->compare('tmarca',$this->tmarca);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->drh;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tcargo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
