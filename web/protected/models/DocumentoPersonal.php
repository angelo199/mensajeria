<?php

/**
 * This is the model class for table "ficha.documento_personal".
 *
 * The followings are the available columns in table 'ficha.documento_personal':
 * @property integer $id
 * @property integer $id_personal
 * @property integer $id_documento
 * @property string $url
 * @property string $descripcion
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property Personal $idPersonal
 * @property Documento $idDocumento
 */
class DocumentoPersonal extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'ficha.documento_personal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_personal, id_documento, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly' => true),
            array('url, descripcion', 'length', 'max' => 255),
            array('fecha_ini, fecha_act, fecha_elim', 'length', 'max' => 20),
            array('estatus', 'length', 'max' => 1),
            array('estado_resolucion','max'=> 5),
            array('tipo_documento','max'=>40),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, id_personal, id_documento, url, descripcion, usuario_ini_id, fecha_ini, tipo_documento, usuario_act_id, fecha_act, fecha_elim, estatus,estado_resolucion', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
            'idDocumento' => array(self::BELONGS_TO, 'Documento', 'id_documento'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'id_personal' => 'Id Personal',
            'id_documento' => 'Id Documento',
            'url' => 'Url',
            'descripcion' => 'Descripcion',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id_personal', $this->id_personal);
        $criteria->compare('id_documento', $this->id_documento);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('descripcion', $this->descripcion, true);
        $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('fecha_ini', $this->fecha_ini, true);
        $criteria->compare('usuario_act_id', $this->usuario_act_id);
        $criteria->compare('fecha_act', $this->fecha_act, true);
        $criteria->compare('fecha_elim', $this->fecha_elim, true);
        $criteria->compare('estatus', $this->estatus, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DocumentoPersonal the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function insertarArchivo($documento, $descripcion, $ruta, $usuario_id, $estatus,$fecha_documento,$tipo_documento, $personal = null) {

        $sql = "INSERT INTO ficha.documento_personal(
                id_personal, id_documento, url, descripcion, usuario_ini_id, estatus, ".((!is_null($fecha_documento))?"fecha_documento, ":"")." tipo_documento)
                VALUES (:id_personal, :id_documento, :url, :descripcion, :usuario_ini_id, :estatus, ".((!is_null($fecha_documento))?":fecha_documento, ":"").":tipo_documento) RETURNING id";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":id_personal", $personal, PDO::PARAM_INT);
        $busqueda->bindParam(":id_documento", $documento, PDO::PARAM_INT);
        $busqueda->bindParam(":url", $ruta, PDO::PARAM_STR);
        $busqueda->bindParam(":descripcion", $descripcion, PDO::PARAM_STR);
        $busqueda->bindParam(":usuario_ini_id", $usuario_id, PDO::PARAM_STR);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        if(!is_null($fecha_documento)){
            $busqueda->bindParam(":fecha_documento", $fecha_documento, PDO::PARAM_STR);
        }
        $busqueda->bindParam(":tipo_documento", $tipo_documento, PDO::PARAM_STR);
        return $busqueda->queryScalar();
    }

    public function buscarExpedienteCatia($cedula) {
        $sql = "select  dp.url, p.estado_resolucion, dp.id, dp.fecha_ini::date, td.id as id_tipo_documento,td.nombre,d.nombre as nombre_documento,dp.fecha_documento  from ficha.personal p
                    inner join ficha.documento_personal dp on p.id=dp.id_personal
                    inner join ficha.documento d on dp.id_documento=d.id
                    inner join ficha.tipo_documento td on td.id=d.tipo_documento_id
                    where p.cedula =:cedula and dp.estatus='A'";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":cedula", $cedula, PDO::PARAM_INT);
        return $busqueda->queryAll();
    }


    public function eliminarArchivo($estatus,$id){
     $resultado='';
     $sql="update ficha.documento_personal set estatus=:estatus where id=:id";
     $consulta=Yii::app()->db->createCommand($sql);
     $consulta->bindParam(':estatus',$estatus, PDO::PARAM_STR);
     $consulta->bindParam(':id',$id, PDO::PARAM_INT);
     $resultado=$consulta->execute();
     return $resultado;
    }

        public function eliminarFotoCatia($estatus,$id){
     $resultado='';
     $sql="update ficha.personal set estatus_foto=:estatus, url_foto= ''  where id=:id";
     $consulta=Yii::app()->db->createCommand($sql);
     $consulta->bindParam(':estatus',$estatus, PDO::PARAM_STR);
     $consulta->bindParam(':id',$id, PDO::PARAM_INT);
     $resultado=$consulta->execute();
     return $resultado;
        }
        
     public function eliminarFotoCopiaCedula($estatus,$id){
     $resultado='';
     $sql="update ficha.personal set estatus_fotocopia=:estatus, url_copia= '' where id=:id";
     $consulta=Yii::app()->db->createCommand($sql);
     $consulta->bindParam(':estatus',$estatus, PDO::PARAM_STR);
     $consulta->bindParam(':id',$id, PDO::PARAM_INT);
     $resultado=$consulta->execute();
     return $resultado;
    }
    
     public function eliminarFotoAlejandria($estatus,$id){
     $resultado='';
     $sql="update document2 set estatus_foto=:estatus where id=:id";
     $consulta=Yii::app()->db->createCommand($sql);
     $consulta->bindParam(':estatus',$estatus, PDO::PARAM_STR);
     $consulta->bindParam(':id',$id, PDO::PARAM_INT);
     $resultado=$consulta->execute();
     return $resultado;
    }



        public function eliminarArchivoAlejandria($estatus,$id){
     $resultado='';
     $sql="update hipertextos set estatus=:estatus where id=:id";
     $consulta=Yii::app()->db->createCommand($sql);
     $consulta->bindParam(':estatus',$estatus, PDO::PARAM_STR);
     $consulta->bindParam(':id',$id, PDO::PARAM_INT);
     $resultado=$consulta->execute();
     return $resultado;
    }

}
