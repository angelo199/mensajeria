<?php
$this->pageTitle = 'Video Tutoriales';

/* @var $this AplicacionPruebaController */
/* @var $model SeccionPrueba */
/* @var $form CActiveForm */
?>

<div id="div-video-Tutoriales">

    <div class="widget-box">

        <div class="widget-header">
            <h5>Vídeotutoriales del Sistema Concurso Público y Sistema de Mérito </h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-body-inner">
                <div class="widget-main">
                    <div class="row">
                        <div class='video-item col-md-12'>
                            <div class="col-md-4">
                                <a class='video-link' data-video-id='video-0'>
                                    <img src="<?php echo Yii::app()->params['urlUploads'].'tutoriales/thumbnail/200a2682-6b12-11e5-be0d-e0699576c200_mp4_1.png';?>">
                                </a>
                            </div> 
                            <div class="col-md-8">
                                <h5>1er. Tutorial del Aplicativo Web para el Concurso Público y Sistema de Mérito</h5>
                                <p> Registro de Nuevo Usuario, Activación de Cuenta y Acceso al Sistema</p>
                            </div>
                            <div id="div-video-0" class="hide">  
                                <video id="video-0" src="<?php echo Yii::app()->params['urlUploads'].'tutoriales/Concurso_Merito_Video_1.ogg';?>" type="video/ogg"  width="500" controls ></video>
                            </div>
                        </div>

                        <hr>

                        <div class='video-item col-md-12'>
                            <div class="col-md-4">
                                <a class='video-link' data-video-id='video-1'>
                                    <img src="<?php echo Yii::app()->params['urlUploads'].'tutoriales/thumbnail/Kazam_screencast_00005_mp4_11.png';?>">
                                </a>
                            </div> 
                            <div class="col-md-8">
                                <h5>2do. Tutorial del Aplicativo Web para el Concurso Público y Sistema de Mérito</h5>
                                <p> Registro de datos de Perfil, Estudios y Cursos Realizados, Experiencias Laborales, Referencias Personales y Postulación al Sistema de Mérito.</p>
                            </div>  
                            <div id="div-video-1" class="hide">  
                                <video id="video-1" src="<?php echo Yii::app()->params['urlUploads'] . 'tutoriales/Concurso_Merito_Video_2.ogg'; ?>" type="video/ogg"  width="500" controls ></video>                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScriptFile(
            Yii::app()->request->baseUrl . '/public/js/modules/ayuda/videoTutoriales.js',CClientScript::POS_END
        );
?>
