<?php

class AplicarPruebaController extends Controller {

    /**

     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning

     * using two-column layout. See 'protected/views/layouts/column2.php'.

     */
    public $defaultAction = 'admin';

    /**

     * @return array action filters

     */
    public static $_permissionControl = array(
        'read' => 'Permite Ver la Prueba al Aspirante',
        'write' => 'Permite Guardar las Respuestas de los Aspirantes',
        'admin' => 'Administración Completa de la Aplicación de Prueba (Permite al responsable de la prueba iniciarla).',
        'label' => 'Módulo de Aplicar Prueba Cuantitativa <br> "Aplicar prueba" <i class="icon-hand-right"></i> "Permiso de Administrador" <i class="icon-hand-right"></i> <br> "Realizar prueba" <i class="icon-hand-right"></i> "Permiso de Lectura y Escritura" <i class="icon-hand-right"></i> '
    );

    /**

     * @return array action filters

     */
    public function filters() {

        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

//    public function actions() {
//        return array(
//            'upload' => array(
//                'class' => 'xupload.actions.XUploadAction',
//                'path' => Yii::app()->getBasePath() . "/../fotoAspirante",
//                'publicPath' => Yii::app()->getBaseUrl() . "/fotoAspirante",
//            )
//        );
//    }

    /**

     * Specifies the access control rules.

     * This method is used by the 'accessControl' filter.

     * @return array access control rules

     */
    public function accessRules() {

// en este array colocar solo los action de consulta

        return array(
//            array('allow',
//                'actions' => array('admin', 'Upload'),
//                'pbac' => array('admin'),
//            ),

            array('allow',
                'actions' => array('index', 'sumaHoras', 'mostrarPruebaIniciada', 'indexAsp', 'validarExistenciaPruebaActiva', 'obtenerRepuestaPregunta', 'verificarRespuestaPregunta', 'obtenerRepuestaDomino'),
                'pbac' => array('read', 'write'),
            ),
            array('allow',
                'actions' => array('mostrarPruebaAsp', 'registroRespuesta', 'actualizarRespuesta', 'guardarEstatusPruebaTerminada', 'actualizarAplicacionPrueba', 'inactivarAplicacionPrueba'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('admin', 'iniciarPrueba'),
                'pbac' => array('admin'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionObtenerRepuestaDomino() {

        $obtenerRespuestaPrueba = false;

        $pregunta_id = (isset($_REQUEST['pregunta'])) ? base64_decode($_REQUEST['pregunta']) : "";

        $prueba_id = (isset($_REQUEST['prueba_id'])) ? base64_decode($_REQUEST['prueba_id']) : "";

        $tipo_pregunta = (isset($_REQUEST['tipo_pregunta'])) ? $_REQUEST['tipo_pregunta'] : "";

        $usuario_id = yii::app()->user->id;

        $obtenerRespuesta = array();

        $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

        if ($prueba_id != '') {

            $aplicacionPrueba = AplicacionPrueba::model()->find(array('condition' => 'prueba_id=' . $prueba_id . ' AND estatus=' . "'A'", 'order' => 'fecha_ini DESC'));

            if ($aplicacionPrueba != null) {

                $fecha_ini = $aplicacionPrueba['fecha_ini'];

                $fecha_array = explode('-', $fecha_ini);

                $fecha_inicial = isset($fecha_array[0]) ? $fecha_array[0] : '';

                $fecha_actual = date('Y');



                if ($fecha_inicial == $fecha_actual) {

                    //    var_dump($tipo_pregunta);

                    if ($tipo_pregunta == 'D' || $tipo_pregunta == 'I') {

                        if ($aspirante_id != false)
                            $obtenerRespuesta = RespuestaPruebaAsp::model()->obtenerRespuestasDomino($aspirante_id, $prueba_id, $pregunta_id, $tipo_pregunta);

                        //   var_dump($obtenerRespuesta);
                        //       die();

                        if ($obtenerRespuesta != array()) {

                            (isset($obtenerRespuesta[0])) ? $respuesta1 = $obtenerRespuesta[0]['respuesta'] : $respuesta1 = '';

                            (isset($obtenerRespuesta[1])) ? $respuesta2 = $obtenerRespuesta[1]['respuesta'] : $respuesta2 = '';

                            echo json_encode(array('statusCode' => 'informacionDomino', 'respuesta1' => (string) $respuesta1, 'respuesta2' => (string) $respuesta2));

                            Yii::app()->end();
                        } else {

                            $respuesta1 = false;

                            $respuesta2 = false;

                            echo json_encode(array('statusCode' => 'informacionDomino', 'respuesta1' => $respuesta1, 'respuesta2' => $respuesta2));

                            Yii::app()->end();
                        }
                    } else {

                        $respuesta1 = false;

                        $respuesta2 = false;

                        echo json_encode(array('statusCode' => 'informacionDomino', 'respuesta1' => $respuesta1, 'respuesta2' => $respuesta2));

                        Yii::app()->end();
                    }
                } else {

                    $respuesta1 = false;

                    $respuesta2 = false;

                    echo json_encode(array('statusCode' => 'informacionDomino', 'respuesta1' => $respuesta1, 'respuesta2' => $respuesta2));

                    Yii::app()->end();
                }
            } else {

                $respuesta1 = false;

                $respuesta2 = false;

                echo json_encode(array('statusCode' => 'informacionDomino', 'respuesta1' => $respuesta1, 'respuesta2' => $respuesta2));

                Yii::app()->end();
            }
        } else {

            $respuesta1 = false;

            $respuesta2 = false;

            echo json_encode(array('statusCode' => 'informacionDomino', 'respuesta1' => $respuesta1, 'respuesta2' => $respuesta2));

            Yii::app()->end();
        }
    }

    public function actionActualizarRespuesta() {

        $mensaje = '';

        $fecha = date('Y-m-d H:i:s');

        $usuario_id = Yii::app()->user->id;

        $hora_final_prueba = '';

        $hora_actual = date('H:i:s');

        $estatus = 'A';

        $respuestaPruebaAsp = null;



        //Respuesta de texto

        $respuesta_seleccionada = (isset($_REQUEST['respuesta_seleccionada'])) ? base64_decode($_REQUEST['respuesta_seleccionada']) : "";

        $id_respuesta_texto = (isset($_REQUEST['id_respuesta_texto'])) ? $_REQUEST['id_respuesta_texto'] : "";

        //Fin
        //Respuesta de imagen

        $respuesta_imagen = (isset($_REQUEST['respuesta_imagen'])) ? $_REQUEST['respuesta_imagen'] : '';

        $id_respuesta_imagen = (isset($_REQUEST['id_respuesta_imagen'])) ? $_REQUEST['id_respuesta_imagen'] : '';

        //fin

        $tipo_pregunta = (isset($_REQUEST['tipo_pregunta'])) ? $_REQUEST['tipo_pregunta'] : "";

        //Respuesta de domino

        $respuesta_domino1 = (isset($_REQUEST['respuesta_domino1'])) ? $_REQUEST['respuesta_domino1'] : '';

        $id_respuesta_domino1 = (isset($_REQUEST['id_respuesta_domino1'])) ? $_REQUEST['id_respuesta_domino1'] : '';

        $respuesta_domino2 = (isset($_REQUEST['respuesta_domino2'])) ? $_REQUEST['respuesta_domino2'] : '';

        $id_respuesta_domino2 = (isset($_REQUEST['id_respuesta_domino2'])) ? $_REQUEST['id_respuesta_domino2'] : '';

        //Fin



        $finalizar_prueba = (isset($_REQUEST['finalizar_prueba'])) ? $_REQUEST['finalizar_prueba'] : "";



        if ($tipo_pregunta != "") {



            if ($tipo_pregunta == 'T' && $id_respuesta_texto != '') {

                $respuestaPruebaAsp = RespuestaPruebaAsp::model()->findByPk($id_respuesta_texto);
            }



            if ($tipo_pregunta == 'I' && $id_respuesta_imagen != '') {

                $respuestaPruebaAsp = RespuestaPruebaAsp::model()->findByPk($id_respuesta_imagen);
            }



            if ($tipo_pregunta == 'D' && ($id_respuesta_domino1 != '' || $id_respuesta_domino2 != '' )) {

                $respuestaPruebaAsp = array(0 => '');
            }



            if ($respuestaPruebaAsp != null) {

                $usuario_id = Yii::app()->user->id;

                $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

                $pruebaActiva = AplicacionPrueba::model()->find(array('condition' => 'estatus = ' . "'" . $estatus . "'" . ' AND aspirante_id=' . $aspirante_id . ' AND estatus_prueba_aplicada=1'));

                if ($pruebaActiva != null) {

                    $hora_final_prueba = $pruebaActiva['hora_final_prueba'];

                    $prueba_id = $pruebaActiva['prueba_id'];



                    $datos = UserGroupsUser::model()->findByPk($usuario_id);

                    $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . '' . $datos['cedula'] : "";

                    if ($hora_actual <= $hora_final_prueba) {



                        if ($tipo_pregunta == 'T' && $respuesta_seleccionada != '' || $tipo_pregunta == 'I' && $respuesta_imagen != '') {

                            $transaction = Yii::app()->db->beginTransaction();

                            try {

                                if ($tipo_pregunta == 'T') {

                                    $respuestaPruebaAsp->respuesta = $respuesta_seleccionada;
                                } elseif ($tipo_pregunta == 'I') {

                                    $respuestaPruebaAsp->respuesta = $respuesta_imagen;
                                }

                                $respuestaPruebaAsp->usuario_act_id = $usuario_id;

                                $respuestaPruebaAsp->fecha_act = $fecha;





                                if ($respuestaPruebaAsp->save(false)) {



                                    $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.ActualizarRespuesta', 'EXITOSO', 'Permite actualizar la respuesta de la pregunta que selecciono el aspirante: ' . $datosPersonales);

                                    if ($finalizar_prueba == true) {

                                        $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_id);

                                        $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.ActualizarRespuesta', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales);
                                    }

                                    $transaction->commit();

                                    Yii::app()->end();
                                } else {



                                    $mensaje_error = 'No se pudo actualizar la respuesta de la pregunta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                                    $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.ActualizarRespuesta', 'FALLIDO', "Ocurrió un error cuando intento actualizar la respuesta de la pregunta que selecciono el aspirante: $datosPersonales");

                                    Yii::app()->end();
                                }
                            } catch (Exception $ex) {

                                /* var_dump($ex);

                                  die(); */

                                $transaction->rollback();

                                $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento actualizar la respuesta de la pregunta que selecciono el aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                                $mensaje = 'Ocurrió un error cuando intento actualizar la respuesta de la pregunta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                                Yii::app()->end();
                            }
                        } elseif ($tipo_pregunta == 'D' && ($respuesta_domino1 != '' || $respuesta_domino2 != '')) {

                            $transaction = Yii::app()->db->beginTransaction();

                            try {

                                $actualizar = RespuestaPruebaAsp::model()->actualizarRespuesta($id_respuesta_domino1, $id_respuesta_domino2, $respuesta_domino1, $respuesta_domino2, $fecha, $usuario_id);



                                if ($actualizar == 1) {



                                    $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.ActualizarRespuesta', 'EXITOSO', 'Permite actualizar la respuesta de la pregunta tipo domino que selecciono el aspirante: ' . $datosPersonales);

                                    if ($finalizar_prueba == true) {

                                        $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_id);

                                        $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.ActualizarRespuesta', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales);
                                    }

                                    $transaction->commit();

                                    Yii::app()->end();
                                } else {



                                    $mensaje_error = 'No se pudo actualizar la respuesta de la pregunta tipo domino que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                                    $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.ActualizarRespuesta', 'FALLIDO', "Ocurrió un error cuando intento actualizar la respuesta de la pregunta tipo domino que selecciono el aspirante: $datosPersonales");

                                    Yii::app()->end();
                                }
                            } catch (Exception $ex) {

                                /*  var_dump($ex);

                                  die(); */

                                $transaction->rollback();

                                $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento actualizar la respuesta de la pregunta que selecciono el aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                                $mensaje = 'Ocurrió un error cuando intento actualizar la respuesta de la pregunta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                                Yii::app()->end();
                            }
                        }
                    } else {

                        $mensaje = 'La hora de la prueba ya culmino para el aspirante: ' . $datosPersonales . ', Por favor comunique al responsable que la prueba culmino.<br>';

                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                        Yii::app()->end();
                    }
                } else {

                    $mensaje = 'La hora de la prueba ya culmino para el aspirante: ' . $datosPersonales . ', Por favor comunique al responsable que la prueba culmino.<br>';

                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                    Yii::app()->end();
                }
            } else {

                $mensaje = 'No se pudo obtener los datos que desea guardar, Por favor comunique este error al responsable de la prueba.';

                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                Yii::app()->end();
            }
        } else {

            $mensaje = 'No se pudo obtener los datos del tipo de prueba que desea actualizar, Por favor comunique este error al responsable de la prueba.';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();
        }
    }

    public function actionVerificarRespuestaPregunta() {

        $pregunta_id = (isset($_REQUEST['pregunta'])) ? base64_decode($_REQUEST['pregunta']) : "";

        $prueba_id = (isset($_REQUEST['prueba_id'])) ? base64_decode($_REQUEST['prueba_id']) : "";

        $tipo_pregunta = (isset($_REQUEST['tipo_pregunta'])) ? $_REQUEST['tipo_pregunta'] : "";

        $usuario_id = Yii::app()->user->id;

        $resultado = '';

        $domino1 = '';

        $domino2 = '';



        if ($prueba_id != '') {

            $aplicacionPrueba = AplicacionPrueba::model()->find(array('condition' => 'prueba_id=' . $prueba_id . ' AND estatus=' . "'A'", 'order' => 'fecha_ini DESC'));

            if ($aplicacionPrueba != null) {

                $fecha_ini = $aplicacionPrueba['fecha_ini'];

                $fecha_array = explode('-', $fecha_ini);

                $fecha_inicial = isset($fecha_array[0]) ? $fecha_array[0] : '';

                $fecha_actual = date('Y');



                if ($fecha_inicial == $fecha_actual) {



                    $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

                    $verificarRespuestaPregunta = RespuestaPruebaAsp::model()->obtenerRespuestasPruebaAsp($aspirante_id, $prueba_id, $pregunta_id, $tipo_pregunta);

//                    var_dump($verificarRespuestaPregunta);
//                    var_dump($tipo_pregunta);

                    if ($verificarRespuestaPregunta != array()) {

                        if ($tipo_pregunta == 'D') {

                            $domino1 = (isset($verificarRespuestaPregunta[0]) ? $verificarRespuestaPregunta[0]['respuesta_prueba_asp'] : '');

                            $domino2 = (isset($verificarRespuestaPregunta[1]) ? $verificarRespuestaPregunta[1]['respuesta_prueba_asp'] : '');

                            $respuesta1 = (isset($verificarRespuestaPregunta[0]) ? $verificarRespuestaPregunta[0]['respuesta'] : '');

                            $respuesta2 = (isset($verificarRespuestaPregunta[1]) ? $verificarRespuestaPregunta[1]['respuesta'] : '');

                            echo json_encode(array('statusCode' => 'actualizo', 'domino1' => $domino1, 'domino2' => $domino2, 'respuesta1' => $respuesta1, 'respuesta2' => $respuesta2, 'respuesta_imagen' => '', 'imagen' => '', 'texto' => '', 'respuesta_texto' => ''));

                            Yii::app()->end();
                        }

                        if ($tipo_pregunta == 'T') {

                            echo json_encode(array('statusCode' => 'actualizo', 'texto' => $verificarRespuestaPregunta['respuesta_prueba_asp'], 'respuesta_texto' => $verificarRespuestaPregunta['respuesta'], 'domino1' => '', 'domino2' => '', 'respuesta1' => '', 'respuesta2' => '', 'imagen' => '', 'respuesta_imagen' => ''));

                            Yii::app()->end();
                        } elseif ($tipo_pregunta == 'I') {

                            echo json_encode(array('statusCode' => 'actualizo', 'imagen' => $verificarRespuestaPregunta['respuesta_prueba_asp'], 'respuesta_imagen' => $verificarRespuestaPregunta['respuesta'], 'texto' => '', 'respuesta_texto' => '', 'domino1' => '', 'domino2' => '', 'respuesta1' => '', 'respuesta2' => ''));

                            Yii::app()->end();
                        }
                    } else {

                        echo json_encode(array('statusCode' => 'inserto', 'mensaje' => $resultado));

                        Yii::app()->end();
                    }
                } else {

                    $resultado = 'Aplicación del año ' . $fecha_inicial;

                    echo json_encode(array('statusCode' => 'inserto', 'mensaje' => $resultado));

                    Yii::app()->end();
                }
            } else {

                $resultado = 'No existe una aplicación de prueba activa';

                echo json_encode(array('statusCode' => 'inserto', 'mensaje' => $resultado));

                Yii::app()->end();
            }
        } else {

            $resultado = 'No existe prueba';

            echo json_encode(array('statusCode' => 'inserto', 'mensaje' => $resultado));

            Yii::app()->end();
        }
    }

    public function actionObtenerRepuestaPregunta() {

        $obtenerRespuestaPrueba = false;

        $respuesta_id = null;

        $pregunta_id = (isset($_REQUEST['pregunta'])) ? base64_decode($_REQUEST['pregunta']) : "";

        $prueba_id = (isset($_REQUEST['prueba_id'])) ? base64_decode($_REQUEST['prueba_id']) : "";

        $tipo_pregunta = (isset($_REQUEST['tipo_pregunta'])) ? $_REQUEST['tipo_pregunta'] : "";

        if ($prueba_id != '') {

            $aplicacionPrueba = AplicacionPrueba::model()->find(array('condition' => 'prueba_id=' . $prueba_id . ' AND estatus=' . "'A'", 'order' => 'fecha_ini DESC'));

            if ($aplicacionPrueba != null) {

                $fecha_ini = $aplicacionPrueba['fecha_ini'];

                $fecha_array = explode('-', $fecha_ini);

                $fecha_inicial = isset($fecha_array[0]) ? $fecha_array[0] : '';

                $fecha_actual = date('Y');

                if ($fecha_inicial == $fecha_actual) {

                    $usuario_id = yii::app()->user->id;

                    $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

                    if ($tipo_pregunta == 'T') {

                        if ($aspirante_id != false)
                            $obtenerRespuestaPrueba = RespuestaPruebaAsp::model()->obtenerRespuestasPruebaAsp($aspirante_id, $prueba_id, $pregunta_id);

                        if ($obtenerRespuestaPrueba != array()) {

                            $respuesta_id = $obtenerRespuestaPrueba['respuesta'];

                            echo json_encode(array('statusCode' => 'informacion', 'mensaje' => $respuesta_id));

                            Yii::app()->end();
                        } else {

                            $respuesta_id = false;

                            echo json_encode(array('statusCode' => 'informacion', 'mensaje' => $respuesta_id));

                            Yii::app()->end();
                        }
                    } else {

                        $respuesta_id = false;

                        echo json_encode(array('statusCode' => 'informacion', 'mensaje' => $respuesta_id));

                        Yii::app()->end();
                    }
                } else {

                    $respuesta_id = false;

                    echo json_encode(array('statusCode' => 'informacion', 'mensaje' => $respuesta_id));

                    Yii::app()->end();
                }
            } else {

                $respuesta_id = 'No existe una aplicación de prueba activa';

                echo json_encode(array('statusCode' => 'informacion', 'mensaje' => $respuesta_id));

                Yii::app()->end();
            }
        } else {

            $respuesta_id = 'No existe prueba';

            echo json_encode(array('statusCode' => 'informacion', 'mensaje' => $respuesta_id));

            Yii::app()->end();
        }
    }

    /**

     * Manages all models.

     */
    public function actionIndexAsp() {



        $estatus = 'A';

        $verificar = true;

        $pruebaActiva = EstatusIniciarPrueba::model()->find(array('condition' => 'estatus = ' . "'" . $estatus . "'"));

        $prueba_id = ($pruebaActiva != null) ? $pruebaActiva['prueba_id'] : 0;

        $prueba = Prueba::model()->find(array('condition' => 'id = ' . $prueba_id));

        $instruciones = ($prueba != null) ? $prueba['instruccion_prueba'] : "";

        if ($prueba != null) {

            $this->layout = '//layouts/prueba';

            $this->render('indexAsp', array(
                'instruciones' => $instruciones,
                'verificar' => $verificar
            ));
        } else {

            $verificar = false;

// $this->layout = '//layouts/prueba';

            $this->render('indexAsp', array(
                'instruciones' => $instruciones,
                'verificar' => $verificar
            ));
        }
    }

    public function actionRegistroRespuesta() {

        $respuestaPrueba = new RespuestaPruebaAsp;

        $usuario_id = yii::app()->user->id;

        $aplicacion_id = '';

        $estatus_iniciar_prueba = null;

        $hora_final_prueba = '';

        $hora_actual = date('H:i:s');

        $datos = UserGroupsUser::model()->findByPk($usuario_id);

        $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ' ' . $datos['origen'] . '' . $datos['cedula'] : "";

//obtengo los datos enviados por POST.

        $respuesta_seleccionada = (isset($_REQUEST['respuesta_seleccionada'])) ? base64_decode($_REQUEST['respuesta_seleccionada']) : null;

        $pregunta_id = (isset($_REQUEST['pregunta'])) ? base64_decode($_REQUEST['pregunta']) : null;

        //Respuesta de domino

        $respuesta_domino1 = (isset($_REQUEST['respuesta_domino1'])) ? $_REQUEST['respuesta_domino1'] : '';

        $respuesta_domino2 = (isset($_REQUEST['respuesta_domino2'])) ? $_REQUEST['respuesta_domino2'] : '';

        //Fin
        //  $resp_domino = (isset($_REQUEST['respuesta_domino'])) ? $_REQUEST['respuesta_domino'] : array();

        $respuesta_imagen = (isset($_REQUEST['respuesta_imagen'])) ? $_REQUEST['respuesta_imagen'] : '';

        $tipo_pregunta = (isset($_REQUEST['tipo_pregunta'])) ? $_REQUEST['tipo_pregunta'] : "";



//Fin

        $finalizar_prueba = (isset($_REQUEST['finalizar_prueba'])) ? $_REQUEST['finalizar_prueba'] : "";

        if ($pregunta_id != null) {

            $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

            if ($aspirante_id != false)
                $aplicacionPrueba = AplicacionPrueba::model()->find(array('condition' => 'aspirante_id = ' . $aspirante_id, 'order' => 'fecha_ini DESC'));

            if ($aplicacionPrueba != null) {

                $aplicacion_id = $aplicacionPrueba['id'];

                $prueba_id = $aplicacionPrueba['prueba_id'];

                $estatus_iniciar_prueba = EstatusIniciarPrueba::model()->find(array('condition' => 'prueba_id = ' . $prueba_id, 'order' => 'fecha_ini DESC'));

                if ($estatus_iniciar_prueba != null) {

                    $hora_final_prueba = $aplicacionPrueba['hora_final_prueba'];
                }
            }



            $datos = UserGroupsUser::model()->findByPk($usuario_id);

            $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . '' . $datos['cedula'] : "";

            if ($hora_actual <= $hora_final_prueba) {



                if ($tipo_pregunta == 'D' && ($respuesta_domino1 != '' || $respuesta_domino2 != '')) {



                    $transaction = Yii::app()->db->beginTransaction();

                    try {





                        $registrar = RespuestaPruebaAsp::model()->registrarRespuesta($respuesta_domino1, $respuesta_domino2, $pregunta_id, $aplicacion_id, $usuario_id);

                        if ($registrar == 1) {



                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'EXITOSO', 'Permite guardar la pregunta y respuesta que selecciono en el tipo de prueba de domino el aspirante: ' . $datosPersonales);

                            if ($finalizar_prueba == true) {

                                $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_id);

                                $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.resgistroRespuesta', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales);
                            }

                            $transaction->commit();

                            Yii::app()->end();
                        } else {



                            $mensaje_error = 'No se pudo guardar la pregunta y respuesta que selecciono en el tipo de prueba de domino el aspirante: ' . $datosPersonales . ' <br>';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento guardar la pregunta y respuesta que selecciono el aspirante: $datosPersonales");

                            Yii::app()->end();
                        }
                    } catch (Exception $ex) {

                        /* var_dump($ex);

                          die(); */

                        $transaction->rollback();

                        $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento guardar la pregunta y respuesta que selecciono el aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                        $mensaje = 'No se pudo guardar la pregunta y respuesta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                        Yii::app()->end();
                    }
                }

                if ($respuesta_seleccionada != null && $tipo_pregunta == 'T') {

                    $transaction = Yii::app()->db->beginTransaction();

                    try {



                        $respuestaPrueba->respuesta = $respuesta_seleccionada;

                        $respuestaPrueba->pregunta_id = $pregunta_id;

                        $respuestaPrueba->aplicacion_prueba_id = $aplicacion_id;

                        $respuestaPrueba->usuario_ini_id = $usuario_id;

                        $respuestaPrueba->estatus = 'A';



                        if ($respuestaPrueba->save(false)) {



                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'EXITOSO', 'Permite guardar la pregunta y respuesta que selecciono el aspirante: ' . $datosPersonales);

                            if ($finalizar_prueba == true) {

                                $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_id);

                                $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.resgistroRespuesta', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales);
                            }

                            $transaction->commit();

                            Yii::app()->end();
                        } else {



                            $mensaje_error = 'No se pudo guardar la pregunta y respuesta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento guardar la pregunta y respuesta que selecciono el aspirante: $datosPersonales");

                            Yii::app()->end();
                        }
                    } catch (Exception $ex) {

                        /* var_dump($ex);

                          die(); */

                        $transaction->rollback();

                        $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento guardar la pregunta y respuesta que selecciono el aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                        $mensaje = 'No se pudo guardar la pregunta y respuesta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                        Yii::app()->end();
                    }
                }

                if ($respuesta_imagen != null && $tipo_pregunta == 'I') {

                    $transaction = Yii::app()->db->beginTransaction();

                    try {



                        $respuestaPrueba->respuesta = $respuesta_imagen;

                        $respuestaPrueba->pregunta_id = $pregunta_id;

                        $respuestaPrueba->aplicacion_prueba_id = $aplicacion_id;

                        $respuestaPrueba->usuario_ini_id = $usuario_id;

                        $respuestaPrueba->estatus = 'A';



                        if ($respuestaPrueba->save(false)) {



                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'EXITOSO', 'Permite guardar la pregunta y respuesta que selecciono el aspirante: ' . $datosPersonales);

                            if ($finalizar_prueba == true) {

                                $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_id);

                                $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.resgistroRespuesta', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales);
                            }

                            $transaction->commit();

                            Yii::app()->end();
                        } else {



                            $mensaje_error = 'No se pudo guardar la pregunta y respuesta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento guardar la pregunta y respuesta que selecciono el aspirante: $datosPersonales");

                            Yii::app()->end();
                        }
                    } catch (Exception $ex) {

                        /* var_dump($ex);

                          die(); */

                        $transaction->rollback();

                        $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.registroRespuesta', 'FALLIDO', "Ocurrió un error cuando intento guardar la pregunta y respuesta que selecciono el aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                        $mensaje = 'No se pudo guardar la pregunta y respuesta que selecciono el aspirante: ' . $datosPersonales . ' <br>';

                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                        Yii::app()->end();
                    }
                }
            } else {

                $mensaje = 'La hora de la prueba ya culmino para el aspirante: ' . $datosPersonales . ' <br>';

                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                Yii::app()->end();
            }
        } else {

            $mensaje = 'No se pudo guardar la respuesta seleccionada por el aspirante, Por favor recargue la pagina e intente nuevamente.';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();
        }
    }

    public function actionValidarExistenciaPruebaActiva() {



        $estatus = 'A';

        $estatus_final = 'I';

        $hora_actual = date('H:i:s');

        $fecha_actual = date('Y-m-d');

        $hora_inicio_prueba = '';

        $hora_final_prueba = '';

        $mensaje = '';

        $fecha_presento = '';

        $usuario_id = Yii::app()->user->id;

        $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

        $validarPruebaActiva = EstatusIniciarPrueba::model()->find(array('condition' => 'estatus_prueba_aplicada=1 AND estatus = ' . "'" . $estatus . "'"));

        if ($aspirante_id != false)
            $pruebaAplicadaActiva = AplicacionPrueba::model()->find(array('condition' => 'aspirante_id=' . $aspirante_id . ' AND estatus = ' . "'" . $estatus_final . "'" . ' AND estatus_prueba_aplicada =2 '));

        if ($pruebaAplicadaActiva != null) {

            $fecha_array = explode(' ', $pruebaAplicadaActiva['fecha_ini']);

            $fecha_presento = isset($fecha_array[0]) ? $fecha_array[0] : '';
        }

        if ($pruebaAplicadaActiva == null || $fecha_presento < $fecha_actual) {

            if ($validarPruebaActiva != null || $fecha_presento != '') {

                $pruebaAplicadaActiva = AplicacionPrueba::model()->find(array('condition' => 'aspirante_id=' . $aspirante_id . ' AND estatus = ' . "'" . $estatus . "'" . ' AND estatus_prueba_aplicada =1 '));

                if ($pruebaAplicadaActiva != null) {

                    $hora_inicio_prueba = $pruebaAplicadaActiva['hora_inicio_prueba'];

                    $hora_final_prueba = $pruebaAplicadaActiva['hora_final_prueba'];

                    $fecha_array = explode(' ', $validarPruebaActiva['fecha_ini']);

                    $fecha_inicial = isset($fecha_array[0]) ? $fecha_array[0] : '';

                    if ($fecha_actual == $fecha_inicial) {

                        if ($hora_actual >= $hora_inicio_prueba && $hora_actual <= $hora_final_prueba) {

                            $prueba_id = $validarPruebaActiva['prueba_id'];

                            $hora_inicio_prueba = $validarPruebaActiva['hora_inicio_prueba'];

                            echo json_encode(array('statusCode' => 'success', 'prueba_id' => $prueba_id, 'hora_inicio_prueba' => $hora_inicio_prueba));

                            Yii::app()->end();
                        } else {

                            $mensaje = 'La hora estimada de la prueba ya ha culminado, Por favor comuniquele este error al responsable que esta aplicando la prueba.<br>';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                            Yii::app()->end();
                        }
                    } else {

                        $mensaje = 'Existe una prueba activa con una fecha distinta a la actual, Por favor comuniquele este error al responsable que esta aplicando la prueba.<br>';

                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                        Yii::app()->end();
                    }
                } else {

                    $prueba_id = $validarPruebaActiva['prueba_id'];

                    $hora_inicio_prueba = $validarPruebaActiva['hora_inicio_prueba'];

                    echo json_encode(array('statusCode' => 'success', 'prueba_id' => $prueba_id, 'hora_inicio_prueba' => $hora_inicio_prueba));

                    Yii::app()->end();
                }
            } else {

                $mensaje = 'No existen pruebas activas, Por favor comuniquele este error al responsable que esta aplicando la prueba.<br>';

                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                Yii::app()->end();
            }
        } else {

            $mensaje = 'Disculpe usted ya culmino la prueba, eso indica que en este proceso de selección no puede volver a presentar la prueba.<br>';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();
        }
    }

    public function actionMostrarPruebaAsp() {



        $prueba_id = isset($_REQUEST['prueba_id']) ? $_REQUEST['prueba_id'] : "";

        $usuario_id = Yii::app()->user->id;

        $hora_actual = date('H:i:s');

        $hora_inicio_prueba = '';

        $hora_final_prueba = '';

        $tiempo_prueba = '';

        $tiempo_segundos = '';

        $nuevaPreguntas = array();
        $resultadosGuardados = array();

        $aplicacionPrueba = new AplicacionPrueba;

        if ($prueba_id != "" && is_numeric($prueba_id) && $usuario_id != '') {



            $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

            if ($aspirante_id != false) {

                $datosPrueba = Prueba::model()->datosCompletosPrueba($prueba_id);

                if ($datosPrueba != array()) {


                    $datosPreguntas = array();
                    $datosPreguntasAcumulada = array();
                    $datosSeccionPrueba = SeccionPrueba::model()->datosSeccionPrueba($prueba_id);
                    if ($datosSeccionPrueba != array()) {
                        foreach ($datosSeccionPrueba as $key1 => $value) {

                            $seccion_id = (int) $value['seccion_id'];
                            $datosPreguntaSubSeccion = SubSeccionPrueba::model()->datosSubSeccionPrueba($seccion_id);
                            if ($datosPreguntaSubSeccion != array()) {
                                foreach ($datosPreguntaSubSeccion as $sub => $subDatos) {
                                    $sub_seccion_id = (int) $subDatos['sub_seccion_id'];
                                    $datosPreguntasAcumulada[] = Pregunta::model()->datosPreguntaSubSeccion($sub_seccion_id);
                                }
                            } else {
                                $datosPreguntasAcumulada[] = Pregunta::model()->datosPreguntas($seccion_id);
                            }
                        }
                        foreach ($datosPreguntasAcumulada as $z => $datosAcumulados) {
                            $datosPreguntas = $datosAcumulados;

                            if ($datosPreguntas != array()) {
                                shuffle($datosPreguntas);
                                foreach ($datosPreguntas as $key2 => $datos) {
                                    $pregunta = $datos['pregunta_id'];
                                    $resultadosGuardados[] = RespuestaPruebaAsp::model()->resultadosGuardados($pregunta, $aspirante_id);
                                    // var_dump($resultadosGuardados);
                                    $nuevaPreguntasC = array();
                                    for ($key3 = 0; $key3 < count($datosPrueba); $key3++) {
                                        $pregunta_prueba = $datosPrueba[$key3]['pregunta_id'];

                                        if ($pregunta == $pregunta_prueba) {
                                            $nuevaPreguntasC[] = $datosPrueba[$key3];
                                        }
                                    }
                                    shuffle($nuevaPreguntasC);
                                    for ($x = 0; $x < count($nuevaPreguntasC); $x++) {
                                        $nuevaPreguntas[] = $nuevaPreguntasC[$x];
                                    }
                                }
                            }
                        }
                    }
                    $cant_pregunta = count($resultadosGuardados);
                    //   var_dump($cant_pregunta);


                    $estatus = 'A';

                    $aplicacionPruebas = AplicacionPrueba::model()->find(array('condition' => 'prueba_id=' . $prueba_id . ' AND estatus=' . "'A'" . ' AND aspirante_id=' . $aspirante_id, 'order' => 'fecha_ini DESC'));

                    if ($aplicacionPruebas != null) {

                        $fecha_ini = $aplicacionPruebas['fecha_ini'];

                        $fecha_array = explode('-', $fecha_ini);

                        $fecha_inicial = isset($fecha_array[0]) ? $fecha_array[0] : '';

                        $fecha_actual = date('Y');

                        if ($fecha_inicial == $fecha_actual) {

                            $datosPreguntaRespondida = RespuestaPruebaAsp::model()->datosPreguntaRespondida($prueba_id, $aspirante_id, $nuevaPreguntas, $fecha_actual);
                        } else {

                            $datosPreguntaRespondida = null;
                        }
                    } else {

                        $datosPreguntaRespondida = null;
                    }

                    $pruebaActiva = EstatusIniciarPrueba::model()->find(array('condition' => 'estatus = ' . "'" . $estatus . "'"));

                    $responsable_prueba_id = $pruebaActiva['responsable_aplicar_prueba_id'];

                    $prueba = Prueba::model()->findByPk($prueba_id);

                    $nombrePrueba = (isset($prueba) && isset($prueba['nombre']) && $prueba != null) ? $prueba['nombre'] : "";

                    $estatus_prueba_aplicada = 1;

                    $verificarAplicacionUnica = AplicacionPrueba::model()->find(array('condition' => 'prueba_id=' . $prueba_id . ' AND aspirante_id=' . $aspirante_id . ' AND estatus_prueba_aplicada = ' . "'" . $estatus_prueba_aplicada . "'"));



                    if ($pruebaActiva != null && $verificarAplicacionUnica != null) {



                        $hora_inicio_prueba = $verificarAplicacionUnica['hora_inicio_prueba'];

                        $hora_final_prueba = $verificarAplicacionUnica['hora_final_prueba'];

                        $tiempo_prueba = $this->actionRestarHoras($hora_inicio_prueba, $hora_final_prueba);

                        if ($tiempo_prueba != '') {

                            $hora = explode(":", $tiempo_prueba);

                            $hora_segundos = $hora[0] * 3600;

                            $minutos_segungos = $hora[1] * 60;

                            $segundos = $hora[2];

                            $tiempo_segundos = $hora_segundos + $minutos_segungos + $segundos;
                        } else {

                            $mensaje = 'Por favor verifique la duración agregada en cada sección o sub-sección que posee esta prueba ya que al consultar los datos esta respondiendo con un tiempo vacio y se acota que debe de existir un tiempo relacionado a la prueba para iniciar la misma, Por favor contacte al responsable de la prueba de este error.';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                            Yii::app()->end();
                        }
                    } else {

                        $hora_inicio_prueba = $pruebaActiva['hora_inicio_prueba'];

                        $hora_final_prueba = $pruebaActiva['hora_final_prueba'];

                        $tiempo_prueba = $this->actionRestarHoras($hora_inicio_prueba, $hora_final_prueba);
                    }





                    if ($verificarAplicacionUnica != null && $hora_actual >= $hora_inicio_prueba && $hora_actual <= $hora_final_prueba) {

                        $hora_inicio = $verificarAplicacionUnica['hora_inicio_prueba'];



                        if ($hora_actual > $hora_inicio) {

                            $hora_final = $verificarAplicacionUnica['hora_final_prueba'];



                            $hora_final_prueba = $this->actionRestarHoras($hora_actual, $hora_final);

                            $hora = explode(":", $hora_final_prueba);

                            $hora_segundos = $hora[0] * 3600;

                            $minutos_segungos = $hora[1] * 60;

                            $segundos = $hora[2];

                            $tiempo_segundos = $hora_segundos + $minutos_segungos + $segundos;
                        }



                        $this->renderPartial('mostrarAsp', array(
                            'nombrePrueba' => $nombrePrueba,
                            'datosPrueba' => $nuevaPreguntas,
                            'datosPreguntaRespondida' => $datosPreguntaRespondida,
                            'tiempo_segundos' => $tiempo_segundos,
                            'resultadosGuardados' => $resultadosGuardados,
                            'cant_pregunta' => $cant_pregunta
                        ));
                    } else {



                        if ($verificarAplicacionUnica == null) {

                            $hora_inicio_prueba = date('H:i:s');

                            $hora_final_prueba = $this->actionSumaHoras($hora_inicio_prueba, $tiempo_prueba);

                            $hora = explode(":", $tiempo_prueba);

                            $hora_segundos = $hora[0] * 3600;

                            $minutos_segungos = $hora[1] * 60;

                            $segundos = $hora[2];

                            $tiempo_segundos = $hora_segundos + $minutos_segungos + $segundos;



                            $transaction = Yii::app()->db->beginTransaction();

                            try {



                                $aplicacionPrueba->usuario_ini_id = $usuario_id;

                                $aplicacionPrueba->estatus = 'A';

                                $aplicacionPrueba->prueba_id = $prueba_id;

                                $aplicacionPrueba->aspirante_id = $aspirante_id;

                                $aplicacionPrueba->responsable_aplicar_prueba_id = $responsable_prueba_id;

                                $aplicacionPrueba->estatus_prueba_aplicada = $estatus_prueba_aplicada;

                                $aplicacionPrueba->hora_inicio_prueba = $hora_inicio_prueba;

                                $aplicacionPrueba->hora_final_prueba = $hora_final_prueba;



                                if ($aplicacionPrueba->save(false)) {

                                    $this->renderPartial('mostrarAsp', array(
                                        'nombrePrueba' => $nombrePrueba,
                                        'datosPrueba' => $nuevaPreguntas,
                                        'datosPreguntaRespondida' => $datosPreguntaRespondida,
                                        'tiempo_segundos' => $tiempo_segundos,
                                        'resultadosGuardados' => $resultadosGuardados,
                                        'cant_pregunta' => $cant_pregunta
                                    ));

                                    $prueba = Prueba::model()->findByPk($prueba_id);

                                    $nombrePrueba = (isset($prueba) && isset($prueba['nombre']) && $prueba != null) ? $prueba['nombre'] : "";

                                    $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.mostrarPruebaAsp', 'EXITOSO', 'Permite guardar en la tabla aplicacion_aspirante el aspirante y la prueba que esta presentando:  ' . $nombrePrueba);

                                    $transaction->commit();

                                    Yii::app()->end();
                                } else {



                                    $mensaje_error = 'No se pudo guardar el aspirante que esta realizando la prueba, Por favor contacte al responsable de la prueba. <br>';

                                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                                    $datos = UserGroupsUser::model()->findByPk($usuario_id);

                                    $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ' ' . $datos['origen'] . '' . $datos['cedula'] : "";

                                    $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.mostrarPruebaAsp', 'FALLIDO', "Ocurrió un error cuando intento guardar en la tabla aplicacion_aspirante del aspirante: $datosPersonales");

                                    Yii::app()->end();
                                }
                            } catch (Exception $ex) {

                                /* var_dump($ex);

                                  die(); */

                                $transaction->rollback();

                                $datos = UserGroupsUser::model()->findByPk($usuario_id);

                                $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . '' . $datos['cedula'] : "";

                                $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.mostrarPruebaAsp', 'FALLIDO', "Ocurrió un error cuando intento guardar en la tabla aplicacion_aspirante del aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                                $mensaje = 'No se pudo guardar el aspirante que esta realizando la prueba, Por favor contacte al responsable de la prueba.';

                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                                Yii::app()->end();
                            }
                        } else {

                            $mensaje = 'Por favor contacte al responsable de la prueba, ya que usted ya inicio la prueba y aun esta activa.';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                            Yii::app()->end();
                        }
                    }
                } else {

                    $mensaje = 'No se pudo guardar el aspirante que esta realizando la prueba ya que no existen datos asociados a la prueba que de desea iniciar, Por favor contacte al responsable de la prueba.';

                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                    Yii::app()->end();
                }
            } else {

                $mensaje = 'Se le recuerda que para presentar una prueba debe estar registrado como aspirante primero, Por favor contacte al responsable de la prueba.';

                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                Yii::app()->end();

                // throw new CHttpException(404, "Error, por favor recargue la página he intente nuevamente, no se consigio la prueba que se va aplicar.");
            }
        } else {

            $mensaje = 'Error, por favor recargue la página he intente nuevamente, no se consigio la prueba que se va aplicar.';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();
        }
    }

    public function actionAdmin() {



        $model = new AperturaPeriodo;

        $estatus = 'A';        
        $prueba = AperturaPeriodo::model()->verificarPrueba($estatus);        
        $pruebaActivas = AperturaPeriodo::model()->pruebaActivas();
        $this->render('index', array(
            'prueba' => $prueba,
            'model' => $model,
            'pruebaActivas' => $pruebaActivas
        ));
    }

    public function actionIniciarPrueba() {

        $model = new EstatusIniciarPrueba;

        $mensaje_error = '';

        $datosPersonales = '';

        $datos = null;

        $nombrePrueba = '';

        $prueba = null;

        $usuario_id = Yii::app()->user->id;

        $estatus = 'A';

        $fecha = date('Y-m-d H:i:s');

        $hora_inicio_prueba = date('H:i:s');

        $hora_actual = date('H:i:s');

        $fecha_actual = date('Y-m-d');

        $tiempo_leer_instrucion = '';

        $hora_inicio = '';

        $horaFinal = '';

        $tiempo_segundos = '';

        $estatus_prueba_aplicada = 1;

        $hora_final_aplicada = '';

        $validarExistePruebaActiva = null;
        
        if ($this->hasPost('AperturaPeriodo')) {
            $prueba_id = (isset($_REQUEST['AperturaPeriodo']['id'])) ? $_REQUEST['AperturaPeriodo']['id'] : "";
            if ($prueba_id != '' && $prueba_id != null && is_numeric($prueba_id)) {
                $estatus = 'A';
                $validarExistePruebaActiva = EstatusIniciarPrueba::model()->find(array('condition' => 'estatus = ' . "'" . $estatus . "'"));
                $verificarAplicacion = AplicacionPrueba::model()->find(array('condition' => 'prueba_id=' . $prueba_id . ' AND estatus_prueba_aplicada = ' . "'" . $estatus_prueba_aplicada . "'" . ' AND estatus = ' . "'" . $estatus . "'", 'order' => 'hora_final_prueba DESC'));
                if ($validarExistePruebaActiva != null) {



                    if ($verificarAplicacion != null) {

                        $hora_final_aplicada = $verificarAplicacion['hora_final_prueba'];

                        $fecha_array = explode(' ', $verificarAplicacion['fecha_ini']);

                        $fecha_inicial = isset($fecha_array[0]) ? $fecha_array[0] : '';
                    }



                    if ($verificarAplicacion == null || ($hora_final_aplicada < $hora_actual && $fecha_actual == $fecha_inicial) || ($hora_final_aplicada < $hora_actual && $fecha_actual > $fecha_inicial)) {

                        $prueba_aplicada_id = $validarExistePruebaActiva['prueba_id'];

                        $hora_final_prueba = $validarExistePruebaActiva['hora_final_prueba'];

                        $fecha_array = explode(' ', $validarExistePruebaActiva['fecha_ini']);

                        $fecha_inicial = isset($fecha_array[0]) ? $fecha_array[0] : '';

                        if ($hora_final_prueba < $hora_actual && $fecha_actual == $fecha_inicial || $fecha_actual > $fecha_inicial) {

                            $datos = UserGroupsUser::model()->findByPk($usuario_id);

                            $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . ' ' . $datos['cedula'] : "";

                            $transaction = Yii::app()->db->beginTransaction();

                            try {

                                $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_aplicada_id);

                                if ($finalizar_prueba_resultado >= 1) {

                                    $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino. ');

                                    $validarExistePruebaActiva = EstatusIniciarPrueba::model()->find(array('condition' => 'estatus = ' . "'" . $estatus . "'"));

                                    $transaction->commit();

                                    //   Yii::app()->end();
                                } else {

                                    $mensaje_error = 'No se pudo actualizar el estatus de la prueba que ya culmino<br>';

                                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                                    $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino");

                                    Yii::app()->end();
                                }
                            } catch (Exception $ex) {

                                /*    var_dump($ex);

                                  die(); */

                                $transaction->rollback();

                                $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino.  Error: {$ex->getMessage()}.");



                                $mensaje = 'Ocurrio un error, No se pudo actualizar el estatus de la prueba que ya culmino. <br>';

                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                                Yii::app()->end();
                            }
                        }
                    } else {

                        $verificarAplicacion = AplicacionPrueba::model()->verificarAspirantePresentando($prueba_id);

                        if ($verificarAplicacion != array()) {
                            $fecha_array = explode(' ', $verificarAplicacion['fecha_ini']);
                            $fecha_prueba = isset($fecha_array[0]) ? $fecha_array[0] : '';
                            if ($fecha_prueba < date('Y-m-d') || ($verificarAplicacion['hora_final_prueba'] <= date('H:i:s') && $fecha_prueba == date('Y-m-d'))) {

                                $transaction = Yii::app()->db->beginTransaction();

                                try {

                                    $prueba_aplicada_id = $validarExistePruebaActiva['prueba_id'];

                                    $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_aplicada_id);

                                    if ($finalizar_prueba_resultado >= 1) {

                                        $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino. ');

                                        $validarExistePruebaActiva = EstatusIniciarPrueba::model()->find(array('condition' => 'estatus = ' . "'" . $estatus . "'"));

                                        $transaction->commit();

                                        //   Yii::app()->end();
                                    } else {

                                        $mensaje_error = 'No se pudo actualizar el estatus de la prueba que ya culmino<br>';

                                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                                        $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino");

                                        Yii::app()->end();
                                    }
                                } catch (Exception $ex) {

                                    /*    var_dump($ex);

                                      die(); */

                                    $transaction->rollback();

                                    $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino.  Error: {$ex->getMessage()}.");



                                    $mensaje = 'Ocurrio un error, No se pudo actualizar el estatus de la prueba que ya culmino. <br>';

                                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                                    Yii::app()->end();
                                }
                            } else {

                                $mensaje = 'Aún no se puede aplicar una prueba porque aun existen aspirantes presentando la misma. <br>';

                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                                Yii::app()->end();
                            }
                        } else {

                            $mensaje = 'Aún no se puede aplicar una prueba porque aun existen aspirantes presentando la misma. <br>';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                            Yii::app()->end();
                        }
                    }
                }

//Valido que no exista una prueba activa en la tabla estatus_iniciar_prueba

                if ($validarExistePruebaActiva == null) {

//   echo 'entre';



                    $estatus_prueba = 1; //Indica que se aplico o inicio la prueba.

                    $prueba = Prueba::model()->findByPk($prueba_id);

                    $nombrePrueba = (isset($prueba) && isset($prueba['nombre']) && $prueba != null) ? $prueba['nombre'] : "";



//                    var_dump($horaFinal);
//                    die();

                    $transaction = Yii::app()->db->beginTransaction();

                    try {





                        $obtenerHoraFinal = EstatusIniciarPrueba::model()->obtenerHoraFinal($prueba_id);



//Agregando tiempo de reposo entre secciones.

                        $cantidad_seccion = count($obtenerHoraFinal);

                        $tiempo = 3 * $cantidad_seccion;

                        $tiempo_leer_instrucion = '00:0' . $tiempo . ':00';



//Fin



                        if ($obtenerHoraFinal != array()) {

                            foreach ($obtenerHoraFinal as $key => $value) {



                                if (isset($key) && $key == 0) {

                                    $hora_inicial = $value['duracion_seccion'];

//    echo $hora_inicial . ' hora ini<br>';

                                    if (isset($obtenerHoraFinal[$key + 1])) {

                                        $hora2 = $obtenerHoraFinal[$key + 1]['duracion_seccion'];

//    echo $hora2 . ' hora 2<br>';

                                        $suma = $this->actionSumaHoras($hora_inicial, $hora2);

//    echo $suma . ' suma ini<br>';
                                    } else {

                                        $suma = $value['duracion_seccion'];
                                    }
                                } else {

                                    if (isset($obtenerHoraFinal[$key + 1]['duracion_seccion'])) {

                                        $hora_inicial2 = $suma;

//     echo $hora_inicial2 . ' hora ini abaj<br>';

                                        $hora = $obtenerHoraFinal[$key + 1]['duracion_seccion'];

//    echo $hora . 'hora2 abajo<br>';



                                        $suma = $this->actionSumaHoras($hora_inicial2, $hora);

//     echo $suma . ' ult<br>';
                                    } else {

//      echo 'salio';

                                        break;
                                    }
                                }
                            }



                            $sumaHoraPrueba = $suma;

                            $sumaHoraPrueba = $this->actionSumaHoras($tiempo_leer_instrucion, $sumaHoraPrueba);
                        } else {

                            $sumaHoraPrueba = '00:00:00';

                            $mensaje = 'La prueba que desea aplicar no fue registrada correctamente ya que le falta la duración de las secciones, Por favor registre la prueba completa y luego intente nuevamente.';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                            Yii::app()->end();
                        }

                        if ($sumaHoraPrueba != '00:00:00') {



                            $horaFinal = $this->actionSumaHoras($sumaHoraPrueba, $hora_inicio_prueba);
                        }



                        $model->responsable_aplicar_prueba_id = $usuario_id;

                        $model->prueba_id = $prueba_id;

                        $model->estatus_prueba_aplicada = $estatus_prueba;

                        $model->hora_inicio_prueba = $hora_inicio_prueba;

                        $model->hora_final_prueba = $horaFinal;

                        $model->usuario_ini_id = $usuario_id;

                        $model->estatus = $estatus;

                        $model->fecha_ini = $fecha;



                        if ($model->save(false)) {

                            $hora_inicio = $model['hora_inicio_prueba'];

                            $horaFinal = $model['hora_final_prueba'];

                            $tiempo_prueba = $this->actionRestarHoras($hora_inicio, $horaFinal);


                            if ($tiempo_prueba != '') {

                                $hora = explode(":", $tiempo_prueba);

                                $hora_segundos = $hora[0] * 3600;

                                $minutos_segungos = $hora[1] * 60;

                                $segundos = $hora[2];

                                $tiempo_segundos = $hora_segundos + $minutos_segungos + $segundos;



                                $datos = UserGroupsUser::model()->findByPk($usuario_id);

                                $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . ' ' . $datos['cedula'] : "";



                                $this->renderPartial('mostrar', array(
                                    'nombrePrueba' => $nombrePrueba,
                                    'datosPersonales' => $datosPersonales,
                                    'hora_inicio' => $hora_inicio,
                                    'horaFinal' => $horaFinal,
                                    'tiempo_prueba' => $tiempo_prueba
                                ));



                                $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.iniciarPrueba', 'EXITOSO', 'Permite guardar el control si se ha iniciado la aplicación de la prueba: ' . $nombrePrueba);

//echo json_encode(array('statusCode' => 'success', 'mensaje' => 'exitoso', 'vista' => $vista));

                                $transaction->commit();

                                Yii::app()->end();
                            } else {

                                $mensaje = 'Por favor verifique la duración agregada en cada sección o sub-sección que posee esta prueba ya que al consultar los datos esta respondiendo con un tiempo vacio y se acota que debe de existir un tiempo relacionado a la prueba para iniciar la misma, Por favor contacte al responsable de la prueba de este error.';

                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                                Yii::app()->end();
                            }
                        } else {

                            $mensaje_error = 'No se pudo guardar el control del inicio de la aplicación de la prueba, Por favor recargue la pagina e intente nuevamente <br>';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                            $datos = UserGroupsUser::model()->findByPk($usuario_id);

                            $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ' ' . $datos['origen'] . '' . $datos['cedula'] : "";

                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.iniciarPrueba', 'FALLIDO', "Ocurrió un error cuando intento iniciar la prueba el responsable: $datosPersonales");

                            Yii::app()->end();
                        }
                    } catch (Exception $ex) {

                        /* var_dump($ex);

                          die(); */

                        $transaction->rollback();

                        $datos = UserGroupsUser::model()->findByPk($usuario_id);

                        $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . '' . $datos['cedula'] : "";

                        $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.iniciarPrueba', 'FALLIDO', "Ocurrió un error cuando intento iniciar la prueba el responsable: $datosPersonales.  Error: {$ex->getMessage()}.");



                        $mensaje = 'No se pudo guardar el control del inicio de la aplicación de la prueba, Por favor recargue la pagina e intente nuevamente.';

                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                        Yii::app()->end();
                    }
                } else {

                    $mensaje = 'En este momento ya se encuentra aplicada una prueba por favor espere la culminación de la misma, Luego recargue la pagina e intente nuevamente.';

                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                    Yii::app()->end();
                }
            } else {

                $mensaje = 'Error, no se encuentra disponible la prueba que desea aplicar, por favor recargue la pagina e intente nuevamente.';

                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                Yii::app()->end();

                //  throw new CHttpException(404, "Error, por favor recargue la página he intente nuevamente, no se consigio la prueba que se va aplicar.");
            }
        } else {

            $mensaje = 'Error, no existen datos asociados a la prueba, por favor recargue la pagina e intente nuevamente.';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();

            // throw new CHttpException(404, "Error, por favor recargue la página he intente nuevamente .");
        }
    }

    public function actionActualizarAplicacionPrueba() {

        $prueba_id = (isset($_REQUEST['prueba_id'])) ? base64_decode($_REQUEST['prueba_id']) : "";

        $usuario_id = Yii::app()->user->id;

        $mensaje = '';

        $mensaje_error = '';

        $datos = UserGroupsUser::model()->findByPk($usuario_id);

        $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . '' . $datos['cedula'] : "";

        $aspirante_id = Aspirante::model()->aspiranteId($usuario_id);

        if ($aspirante_id != false && $prueba_id != '') {

            $transaction = Yii::app()->db->beginTransaction();

            try {

                $finalizar_prueba_resultado = AplicacionPrueba::model()->finalizarPrueba($prueba_id, $aspirante_id);





                if ($finalizar_prueba_resultado == 1) {

                    $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.ActualizarAplicacionPrueba', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales);

                    $mensaje = 'Actualización existosa de la aplicación de prueba.<br>';

                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje));

                    $transaction->commit();

                    Yii::app()->end();
                } else {



                    $mensaje_error = 'No se pudo actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales . ' <br>';

                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                    $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.ActualizarAplicacionPrueba', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino, para el aspirante: $datosPersonales");

                    Yii::app()->end();
                }
            } catch (Exception $ex) {

                /* var_dump($ex);

                  die(); */

                $transaction->rollback();

                $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.ActualizarAplicacionPrueba', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino, para el aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                $mensaje = 'No se pudo actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales . ' <br>';

                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                Yii::app()->end();
            }
        } else {

            $mensaje = 'No se pudo actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales . ' <br>';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();
        }
    }

    public function actionGuardarEstatusPruebaTerminada() {

        $prueba_id = (isset($_REQUEST['prueba_id'])) ? base64_decode($_REQUEST['prueba_id']) : "";

        $usuario_id = Yii::app()->user->id;

        $datos = UserGroupsUser::model()->findByPk($usuario_id);

        $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . '' . $datos['cedula'] : "";

        if ($prueba_id != '') {



            $transaction = Yii::app()->db->beginTransaction();

            try {

                $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->finalizarPrueba($prueba_id);



                if ($finalizar_prueba_resultado >= 1) {

                    $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales);

                    $mensaje = 'Actualización existosa de la aplicación de prueba.<br>';

                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje));

                    $transaction->commit();

                    Yii::app()->end();
                } else {



                    $mensaje_error = 'No se pudo actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales . ' <br>';

                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                    $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino, para el aspirante: $datosPersonales");

                    Yii::app()->end();
                }
            } catch (Exception $ex) {

                /*  var_dump($ex);

                  die(); */

                $transaction->rollback();

                $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino, para el aspirante: $datosPersonales.  Error: {$ex->getMessage()}.");



                $mensaje = 'No se pudo actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales . ' <br>';

                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                Yii::app()->end();
            }
        } else {

            $mensaje = 'No se pudo guardar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales . ' <br>';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();

            // throw new CHttpException(404, "Error, por favor recargue la página he intente nuevamente .");
        }
    }

    public function actionInactivarAplicacionPrueba() {

        $prueba_id = (isset($_REQUEST['prueba_id'])) ? $_REQUEST['prueba_id'] : "";

        $usuario_id = Yii::app()->user->id;

        $estatus_prueba_aplicada = 1;

        $estatus = 'A';

        $hora_actual = date('H:i:s');

        $fecha_actual = date('Y');

        $datos = UserGroupsUser::model()->findByPk($usuario_id);

        $datosPersonales = (isset($datos) && $datos != null) ? $datos['nombre'] . ' ' . $datos['apellido'] . ', C.I. ' . $datos['origen'] . '' . $datos['cedula'] : "";

        if ($prueba_id != '') {

            $verificarAplicacion = EstatusIniciarPrueba::model()->find(array('condition' => 'prueba_id=' . $prueba_id . ' AND estatus_prueba_aplicada = ' . "'" . $estatus_prueba_aplicada . "'" . ' AND estatus = ' . "'" . $estatus . "'", 'order' => 'hora_final_prueba DESC'));

            if ($verificarAplicacion != null) {

                $hora_final = $verificarAplicacion['hora_final_prueba'];

                $fecha_array = explode('-', $verificarAplicacion['fecha_ini']);

                $fecha_inicio_prueba = isset($fecha_array[0]) ? $fecha_array[0] : '';

                if ($hora_final <= $hora_actual && $fecha_inicio_prueba == $fecha_actual || $fecha_inicio_prueba < $fecha_actual) {



                    $transaction = Yii::app()->db->beginTransaction();

                    try {

                        $finalizar_prueba_resultado = EstatusIniciarPrueba::model()->inactivarAplicacionPrueba($prueba_id);

//                var_dump($finalizar_prueba_resultado);
//                die();

                        if ($finalizar_prueba_resultado >= 1) {

                            $this->registerLog('ACTUALIZACION', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'EXITOSO', 'Permite actualizar el estatus de la prueba que ya culmino, que no ha sido iniciada por ningún aspirante');

                            $mensaje = 'Actualización existosa de la aplicación de prueba.<br>';

                            echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje));

                            $transaction->commit();

                            Yii::app()->end();
                        } else {



                            $mensaje_error = 'No se pudo actualizar el estatus de la prueba que ya culmino, para el aspirante: ' . $datosPersonales . ' <br>';

                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));

                            $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino, que no ha sido iniciada por ningún aspirante. ");

                            Yii::app()->end();
                        }
                    } catch (Exception $ex) {

                        /*  var_dump($ex);

                          die(); */

                        $transaction->rollback();

                        $this->registerLog('ESCRITURA', 'aplicarPrueba.aplicarPrueba.GuardarEstatusPruebaTerminada', 'FALLIDO', "Ocurrió un error cuando intento actualizar el estatus de la prueba que ya culmino, que no ha sido iniciada por ningún aspirante.  Error: {$ex->getMessage()}.");



                        $mensaje = 'Ocurrio un error, no se pudo actualizar el estatus de la prueba que ya culmino. <br>';

                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                        Yii::app()->end();
                    }
                } else {

                    $mensaje = 'Aún no se puede culminar esta prueba ya que no ha pasado el tiempo de ejecución de prueba. <br>';

                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

                    Yii::app()->end();
                }
            }
        } else {

            $mensaje = 'No se encontró la prueba que desea culminar, por favor recargue la página e intente nuevamente, si el error persiste comuniquese con el administrador del sistema. <br>';

            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));

            Yii::app()->end();

            // throw new CHttpException(404, "Error, por favor recargue la página he intente nuevamente .");
        }
    }

    public function actionSumaHoras($hora1, $hora2) {

        $hora1 = explode(":", $hora1);

        $hora2 = explode(":", $hora2);

        $horas = (int) $hora1[0] + (int) $hora2[0];

        $minutos = (int) $hora1[1] + (int) $hora2[1];

        $segundos = (int) $hora1[2] + (int) $hora2[2];

        $horas+=(int) ($minutos / 60);

        $minutos = (int) ($minutos % 60) + (int) ($segundos / 60);

        $segundos = (int) ($segundos % 60);

        return (intval($horas) < 10 ? '0' . intval($horas) : intval($horas)) . ':' . ($minutos < 10 ? '0' . $minutos : $minutos) . ':' . ($segundos < 10 ? '0' . $segundos : $segundos);
    }

    public function actionRestarHoras($horaInicio, $horaFinal) {



        $horaInicio = explode(":", $horaInicio);

        $horaFinal = explode(":", $horaFinal);

        $horas = (int) $horaFinal[0] - (int) $horaInicio[0];

        $minutos = (int) $horaFinal[1] - (int) $horaInicio[1];

        $segundos = (int) $horaFinal[2] - (int) $horaInicio[2];



        $ini = ((($horaInicio[0] * 60) * 60) + ($horaInicio[1] * 60) + $horaInicio[2]);

        $fin = ((($horaFinal[0] * 60) * 60) + ($horaFinal[1] * 60) + $horaFinal[2]);

        $dif = $fin - $ini;

        $difh = floor($dif / 3600);

        $difm = floor(($dif - ($difh * 3600)) / 60);

        $difs = $dif - ($difm * 60) - ($difh * 3600);

        return date("H:i:s", mktime($difh, $difm, $difs));
    }

}
