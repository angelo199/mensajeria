<?php
/* @var $this PruebaController */
/* @var $model Prueba */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Prueba Iniciada',
);
$this->pageTitle = 'Prueba Iniciada';
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#pruebaIniciada" data-toggle="tab">Prueba Iniciada</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="pruebaIniciada">
                    <div class="form">

                        <div id="div-prueba-iniciada">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Prueba Iniciada</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div id="div-result">
                                                            <div class="infoDialogBox">
                                                                <p class="note">
                                                                    La prueba <b><?php echo $nombrePrueba; ?></b> en este momento esta siendo aplicada a los aspirantes.<br>
                                                                    Prueba iniciada por el responsable <b><?php echo $datosPersonales; ?></b><br>
                                                                    Hora de inicio <b><?php echo $hora_inicio; ?></b> y el tiempo de duración de la prueba: <b><?php echo $tiempo_prueba; ?></b>
                                                                </p></div>
                                                        </div>

                                                        <!--                                                        <div id="reloj2" class="col-md-12">
                                                                                                                    <div class="col-md-6"></div>
                                                                                                                    <div class="col-md-6 align-left">
                                                                                                                        <div class="clock" style="padding-left: 20%; margin-bottom: 0em;"></div>
                                                                                                                        <div class="message"></div>
                                                                                                                    </div>
                                                                                                                </div>-->
                                                    </div>

                                                    <div class="space-6"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <!--     <div class="row">

                                         <div class="col-md-6">
                                             <a class="btn btn-danger" href="<?php //echo $this->createUrl("/../");                       ?>"
                                                id="btnRegresar">
                                                 <i class="icon-arrow-left"></i>
                                                 Volver
                                             </a>
                                         </div>

                                     </div>-->
                            </div>
                        </div>
                    </div>
                    <!-- form -->
                </div>

            </div>
        </div>

    </div>
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="css_js">
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/aplicarPrueba/aplicar_prueba.js', CClientScript::POS_END);
    ?>
</div>
<div class="panel panel-warning hide" id="dialog_termino">
    <div class="panel-heading">
        <h2 class="panel-title center grey" style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
            <b>
                <i>Prueba: <?php echo $nombrePrueba; ?></i>
            </b>
        </h2>
    </div>
    <div class="panel-body">
        <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
            <b>
                La prueba que estaban presentando los aspirantes a llegado a su fin. Para visualizar los resultados de la prueba dirigase a:
                <u class="color blue">
                    <b><i>
                            <a href="<?php echo Yii::app()->baseUrl . '/registroAspirante/aspirante/'; ?>" class="color blue">Lista de aspirantes</a> y presione ver expediente.
                        </i></b>
                </u>
            </b>
        </p>
    </div>
</div>
<!--<link rel="stylesheet" href="/public/js/FlipClock-master/compiled/flipclock.css">

<script src="/public/js/FlipClock-master/compiled/flipclock.js"></script>-->
<script>
    $(document).ready(function() {

        $("html, body").animate({scrollTop: 0}, "fast");
    });

</script>
