<div id="indexAsp_form">
    <?php
    /* @var $this PruebaController */
    /* @var $model Prueba */
    /* @var $form CActiveForm */

    $this->breadcrumbs = array(
        'Inicio de la Prueba',
    );
    $this->pageTitle = 'Inicio de la Prueba';
    ?>
    <div class="col-xs-12">
        <div class="row-fluid">

            <div class="tabbable">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#iniPrueba" data-toggle="tab">Instrucciones de la Prueba</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="iniPrueba">
                        <div class="form">

                            <form id="inicio_prueba_asp_form" method="post" action="/mostrarPruebaAsp">

                                <?php if ($verificar == true) { ?>

                                    <div id="div-result">
                                        <div class="infoDialogBox">
                                            <p class="note">
                                                <b>
                                                    Por favor lea detenidamente las instrucciones de la prueba para que sea culminada con exito.
                                                </b>
                                            </p>
                                        </div>
                                    </div>
                                    <!--Muestro mensajes. -->
                                    <div id="msj_error" class="errorDialogBox hide">
                                        <p>

                                        </p>
                                    </div>
                                    <!--Fin mensajes. -->
                                    <div class="col-md-12 response"></div>
                                    <div class="row error_validacion"></div>

                                    <div id="div-aplicar-prueba">

                                        <div class="widget-box">

                                            <div class="widget-header">
                                                <h5>Instrucciones para el inicio de la Prueba</h5>

                                                <div class="widget-toolbar">
                                                    <a data-action="collapse" href="#">
                                                        <i class="icon-chevron-up"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="widget-body">
                                                <div class="widget-body-inner">
                                                    <div class="widget-main">
                                                        <div class="widget-main form">
                                                            <div class="row">
                                                                <div class="col-md-12">

                                                                            <div class="col-md-12 panel panel-info" style="word-wrap:break-word;">
                                                                            <p style="text-align: justify;font-size:12px;font-family: 'Helvetica';">
                                                                                <b>
                                                                                <br>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <?php echo $instruciones; ?>
                                                                                <br>
                                                                                <br>
                                                                            </b>
                                                                        </p>
                                                                    </div>

                                                                    <div class="col-md-12"></div>
                                                                    <br>

                                                                    <div class="col-md-12 center">
                                                                        <button class="btn btn-primary btn-next " data-last="Finish" type="submit">
                                                                            Iniciar Prueba
                                                                            <i class="icon-play icon-on-right"></i>
                                                                        </button>
                                                                    </div>

                                                                </div>

                                                                <div class="space-6"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } elseif ($verificar == false) { ?>
                                    <div class="infoDialogBox">
                                        <p class="note">
                                            <b>
                                                En este momento no se encuentra aun una prueba aplicada por favor espere las indicaciones del responsable de la prueba y actualicé la página.
                                            </b>
                                        </p>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                        <!-- form -->
                    </div>

                </div>
            </div>

            <div id="resultDialog" class="hide"></div>

        </div>
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
    <div id="css_js">
        <?php
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/aplicarPrueba/aplicar_prueba.js', CClientScript::POS_END);
        ?>
    </div>
    <script>
        $(document).ready(function() {
            //desactivar tecla f5
            document.onkeydown = function(e) {
                tecla = (document.all) ? e.keyCode : e.which;
                // alert(tecla)
                if (tecla = 116)
                    return false
            }
            function NoBack() {
                history.go(1)
            }

            $("html, body").animate({scrollTop: 0}, "fast");
        });

    </script>
</div>
