<div id="index">
    <?php
    /* @var $this PruebaController */
    /* @var $model Prueba */
    /* @var $form CActiveForm */

    $this->breadcrumbs = array(
        'Aplicar Prueba',
    );
    $this->pageTitle = 'Aplicar Prueba a los Aspirantes';


//Yii::app()->clientScript->registerScriptFile(
//        Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END
//);
    ?>
    <div class="panel panel-warning hide" id="dialogo_alert_aplica">
        <div class="panel-heading">
            <h2 class="panel-title center grey" style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                <b>
                    <i>Culminar Aplicación de Prueba</i>
                </b>
            </h2>
        </div>
        <div class="panel-body">
            <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                <b>
                    ¿Esta seguro que desea culminar esta aplicación de prueba, ya que no ha sido aplicada a ningun aspirante?.
                </b>
            </p>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="row-fluid">

            <div class="tabbable">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#aplicarPrueba" data-toggle="tab">Aplicar Prueba</a></li>
                    <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="aplicarPrueba">
                        <div class="form">

                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'aplicar_prueba_form',
                                //    'htmlOptions' => array('data-form-type' => 'registro',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation' => false,
                            ));
                            ?>

                            <div id="div-result">
                                <div class="infoDialogBox"><p class="note">Por favor elija la prueba que va aplicar ya que es obligatorio elegir una para poder ser aplicada.</p></div>
                            </div>
                            <!--Muestro mensajes. -->
                            <div id="msj_error" class="errorDialogBox hide">
                                <p>

                                </p>
                            </div>
                            <div id="msj_exitoso" class="successDialogBox hide">
                                <p>

                                </p>
                            </div>
                            <!--Fin mensajes. -->
                            <div class="col-md-12 response"></div>
                            <div class="row error_validacion"></div>

                            <div id="div-aplicar-prueba">

                                <div class="widget-box">

                                    <div class="widget-header">
                                        <h5>Aplicar Prueba</h5>

                                        <div class="widget-toolbar">
                                            <a data-action="collapse" href="#">
                                                <i class="icon-chevron-up"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-body-inner">
                                            <div class="widget-main">
                                                <div class="widget-main form">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <?php if ($pruebaActivas != array()) { ?>
                                                            <div class="col-md-12">
                                                                    <label title="Culminar pruebas aplicadas que no han sido iniciada por ningun aspirante">Culminar Aplicación de Pruebas</label>
                                                                    <?php
                                                                    foreach ($pruebaActivas as $key => $value) {
                                                                        $nombre = $value['nombre_prueba'];
                                                                        $prueba_id = $value['prueba_id'];
                                                                        ?>
                                                                        <div class="input-group" id="prueba_id_<?php echo $prueba_id; ?>">
                                                                            <span class="input-group-addon">
                                                                                <input class="inactivarAplicacion" type="radio" aria-label="..." value="<?php echo $prueba_id; ?>" title="Inactivar pruebas aplicadas que no han sido iniciada por ningun aspirante">
                                                                            </span>
                                                                            <input type="text" class="form-control" aria-label="..." value="<?php echo $nombre; ?>" readonly="true" title="Inactivar pruebas aplicadas que no han sido iniciada por ningun aspirante">
                                                                        </div><!-- /input-group -->
                                                                        <br>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div class="col-md-12"></div>
                                                                <br>
                                                                <hr>
                                                                <?php
                                                            }
                                                            ?>

                                                            <div class="col-md-4">
                                                                <?php echo $form->labelEx($model, 'Elije la prueba <span class="required">*</span>'); ?>
                                                                <?php echo $form->dropDownList($model, 'id', CHtml::listData($prueba, 'id', 'nombre'), array('prompt' => 'Seleccione', 'class' => 'span-9')); ?>
                                                            </div>

                                                            <div class="col-md-12"></div>
                                                            <br>

                                                            <div class="col-md-6 pull-left">
                                                                <button class="btn btn-primary btn-next " data-last="Finish" type="submit">
                                                                    Iniciar Prueba
                                                                    <i class="icon-play icon-on-right"></i>
                                                                </button>
                                                            </div>

                                                        </div>

                                                        <div class="space-6"></div>

                                                        <div class="col-md-12">

                                                            <div class="col-md-4">

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <?php $this->endWidget(); ?>
                        </div>
                        <!-- form -->
                    </div>
                </div>
            </div>
            <br>
            <div class="row">

                <div class="col-md-6">
                    <a class="btn btn-danger" href="<?php echo $this->createUrl("/../"); ?>"
                       id="btnRegresar">
                        <i class="icon-arrow-left"></i>
                        Volver
                    </a>
                </div>

            </div>
            <div id="resultDialog" class="hide"></div>

        </div>
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
    <div id="css_js">
        <?php
        Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/aplicarPrueba/aplicar_prueba.js', CClientScript::POS_END);
        ?>
    </div>


    <script>
        $(document).ready(function() {
            $("html, body").animate({scrollTop: 0}, "fast");

            $('.inactivarAplicacion').unbind('click');
            $('.inactivarAplicacion').click(function() {
                var prueba_id = $(this).val();
                var mensaje = '';
                var titulo = 'Verificar Acción';
                dialogo_alert(mensaje, titulo, prueba_id);
                //alert($('#inactivarAplicacion').val());
            });

        });

        function  dialogo_alert(mensaje, titulo, prueba_id) {
            var dialogTermino = $("#dialogo_alert_aplica").removeClass('hide').dialog({
                modal: true,
                width: '600px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i>" + titulo + " </h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "<i class='icon-ok bigger-110'></i>&nbsp; Aceptar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            $.ajax({
                                url: "/aplicarPrueba/aplicarPrueba/inactivarAplicacionPrueba",
                                data: {prueba_id: prueba_id},
                                dataType: 'html',
                                type: 'post',
                                success: function(resp, resp2, resp3) {
                                    try {
                                        var json = jQuery.parseJSON(resp3.responseText);
                                        if (json.statusCode == "success") {
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $("#msj_exitoso").removeClass('hide');
                                            $("#msj_exitoso p").html(json.mensaje);
                                          //  $('#prueba_id_' + prueba_id).addClass('hide');
                                            $("#dialogo_alert_aplica").dialog("close");
                                            window.location.reload();
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                        if (json.statusCode == "error") {
                                            $("#msj_exitoso").addClass('hide');
                                            $("#msj_exitoso p").html('');
                                            $("#msj_error").removeClass('hide');
                                            $("#msj_error p").html(json.mensaje);
                                            $("#dialogo_alert_aplica").dialog("close");
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                        }
                                    }
                                    catch (e) {
                                        $("#dialogo_alert_aplica").dialog("close");
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                            });
                        }
                    },
                ]
            });
            $("#dialogo_alert_aplica").show();
        }
    </script>
</div>
