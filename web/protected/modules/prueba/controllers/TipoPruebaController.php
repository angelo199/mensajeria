<?php

class TipoPruebaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Permite la Consulta de Tipo de Prueba o Tipo de Proceso Aperturado ',
        'write' => 'Permite la Creación y Modificación de Tipo de Prueba o Tipo de Proceso Aperturado ',
        'admin' => 'Permite la Administración Completa de Tipo de Prueba o Tipo de Proceso Aperturado ',
        'label' => 'Módulo de Tipo de Prueba o Tipo de Proceso Aperturado (prueba.tipoPrueba)'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
// en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('consultarPrueba', 'mostrarFormDePrueba', 'index', 'estatusPrueba', 'mostrarDatosPrueba'),
                'pbac' => array('read', 'write'),
            ),
            array('allow',
                'actions' => array('Registro', 'modificarPrueba', 'inactivarPrueba', 'activarPrueba'),
                'pbac' => array('write'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function estatusPrueba($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'I') {
            return 'Inactivo';
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $id_decod = '';
        $variable = '';
        $model = new Prueba('search');
        $model->unsetAttributes();  // clear any default values
        if ($this->hasQuery('Prueba')) {
            $model->attributes = $this->getQuery('Prueba');
        }
        $dataProvider = $model->search();
        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'id_decod' => $id_decod,
            'variable' => $variable
        ));
    }

//    public function actionMostrarFormDePrueba() {
//
//        $model = new Prueba;
//        $id_decod = '';
//        $this->renderPartial('_form', array(
//            'model' => $model,
//            'id_decod' => $id_decod
//        ));
//    }
//    public function actionMostrarDatosPrueba() {
//
//        $id = $this->getRequest('id');
//        $id_decod = base64_decode($id);
//        $mensaje = "";
//        if (!is_numeric($id_decod)) {
//            $mensaje = 'No se ha encontrado el registro de prueba que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
//            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
//        } else {
//            $model = Prueba::model()->findByPk($id_decod);
//            //Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//            $this->renderPartial('_form', array(
//                'model' => $model,
//                'id_decod' => $id_decod
//                    )//, false, true
//            );
//        }
//    }

    public function actionConsultarPrueba($id) {

        //   $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de prueba que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $model = Prueba::model()->findByPk($id_decod);
            if ($model != null) {
//                var_dump($model);
//                die();
                //   Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                $this->renderPartial('consultar', array('model' => $model)//, false, true
                );
            } else {
                $mensaje = 'No se ha encontrado el registro de prueba que ha solicitado para consultar. Recargue la página e intentelo de nuevo. </br>';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro() {
        $model = new Prueba;
        $variable = 'R'; //Significa que viene de registro.
        $id_decod = '';
        if ($this->hasPost('Prueba') && $this->getRequest('Prueba')) {

            $nombreValido = '';
            $resultadoErroneo = true;
            $resultadoExitoso = false;
            $mensaje_error = '';
            $model->attributes = $this->getRequest('Prueba');
            $model->nombre = trim($_REQUEST['Prueba']['nombre']);
            $model->tipo_prueba = $_REQUEST['Prueba']['tipo_prueba'];
            $model->instruccion_prueba = trim($_REQUEST['Prueba']['instruccion_prueba']);
            $nombre = $model->nombre;

            if ($nombre == '') {
                $mensaje_error.= 'El campo nombre de prueba no puede estar vacio, Por favor ingrese datos. <br>';
            }
            if ($model->tipo_prueba == '') {
                $mensaje_error.= 'El campo Prueba Cualitativa/Cuantitativa no puede estar vacio, Por favor seleccione una opción.<br>';
            }

            if ($mensaje_error != '') {
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                Yii::app()->end();
            }

            if ($model->tipo_prueba == '1') {
                if ($model->nombre == '') {
                    $mensaje_error.= 'El campo Nombre de prueba no puede estar vacio, Por favor ingrese datos.<br>';
                }
                if ($model->tipo_prueba == '') {
                    $mensaje_error.= 'El campo Prueba Cualitativa/Cuantitativa no puede estar vacio, Por favor seleccione una opción.<br>';
                }
            } else if ($model->tipo_prueba == '2') {
                if ($model->nombre == '') {
                    $mensaje_error.= 'El campo Nombre de prueba no puede estar vacio, Por favor ingrese datos.<br>';
                }
                if ($model->tipo_prueba == '') {
                    $mensaje_error.= 'El campo Prueba Cualitativa/Cuantitativa no puede estar vacio, Por favor seleccione una opción.<br>';
                }
                if ($model->instruccion_prueba == '') {
                    $mensaje_error.= 'El campo Instrucciones no puede estar vacio, Por favor ingrese datos.<br>';
                }
            }

//            if ($nombre != '') {
//compruebo que los caracteres sean los permitidos
            if ($nombre != '') {
                $permitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ÁÉÍÓÚÑÄËÏÖÜ ";
                for ($i = 0; $i < strlen($nombre); $i++) {
                    if (strpos($permitidos, substr($nombre, $i, 1)) === false) {
// echo $nombre . " no es válido<br>";
                        $resultadoErroneo = false;
                        break;
                    } else {
// echo $nombre . " es válido<br>";
                        $nombreValido = $nombre;
                        $resultadoExitoso = true;
                    }
                }

                if ($resultadoErroneo == false) {
                    $mensaje_error.= 'El campo nombre de prueba solo puede contener letras y números, intente nuevamente <br>';
                    //  echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                }
            }

            $usuario_id = Yii::app()->user->id;
            $estatus = 'A';
            $model->nombre = $nombreValido;
            $model->estatus = $estatus;
            $model->usuario_ini_id = $usuario_id;

            if ($mensaje_error != '') {
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                Yii::app()->end();
            } else {

                if ($model->save()) {
                    if ($resultadoExitoso == true) {
                        $nombreGuardado = $model->nombre;
                        $this->registerLog('ESCRITURA', 'catalogo.prueba.Registro', 'EXITOSO', 'Permite guardar un registro nuevo de una prueba');
                        $mensaje_exitoso = 'Registro exitoso de la prueba:' . ' ' . $nombreGuardado;
                        echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                        Yii::app()->end();
                    }
                } else {
                    $mensaje_error.= 'No se pudo guardar los datos, Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                    Yii::app()->end();
                }
            }
//            } elseif ($nombre == '') {
//                $mensaje_error.= 'El campo nombre de prueba no puede estar vacio, Por favor ingrese datos <br>';
//                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
//            }
        } else {
            $this->render('_form', array(
                'model' => $model,
                'id_decod' => $id_decod,
                'variable' => $variable
            ));
        }
    }

    public function actionModificarPrueba($id) {

        $model = new Prueba;
        $variable = 'M';
        $id_decod = base64_decode($id);

        $mensaje = "";
        $resultadoErroneo = true;
        $resultadoExitoso = false;
        $nombreValido = '';
        $mensaje_exitoso = '';
        $mensaje_error = '';

        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de prueba que ha solicitado para modificar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {

            $model = Prueba::model()->findByPk($id_decod);
            //var_dump($this->hasRequest('Prueba'));
            if ($this->hasRequest('Prueba')) {

                if (isset($_REQUEST['Prueba']['id'])) {
                    $nombre = trim($_REQUEST['Prueba']['nombre']);
                    $model->tipo_prueba = $_REQUEST['Prueba']['tipo_prueba'];
                    $model->instruccion_prueba = trim($_REQUEST['Prueba']['instruccion_prueba']);

                    if ($nombre == '') {
                        $mensaje_error.= 'El campo nombre de prueba no puede estar vacio, Por favor ingrese datos. <br>';
                    }
                    if ($model->tipo_prueba == '') {
                        $mensaje_error.= 'El campo Prueba Cualitativa/Cuantitativa no puede estar vacio, Por favor seleccione una opción.<br>';
                    }

                    if ($mensaje_error != '') {
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                        Yii::app()->end();
                    }

                    if ($model->tipo_prueba == '1') {
                        if ($nombre == '') {
                            $mensaje.= 'El campo Nombre de prueba no puede estar vacio, Por favor ingrese datos.<br>';
                        }
                        if ($model->tipo_prueba == '') {
                            $mensaje.= 'El campo Prueba Cualitativa/Cuantitativa no puede estar vacio, Por favor seleccione una opción.<br>';
                        }
                    } else if ($model->tipo_prueba == '2') {
                        if ($nombre == '') {
                            $mensaje.= 'El campo Nombre de prueba no puede estar vacio, Por favor ingrese datos.<br>';
                        }
                        if ($model->tipo_prueba == '') {
                            $mensaje.= 'El campo Prueba Cualitativa/Cuantitativa no puede estar vacio, Por favor seleccione una opción.<br>';
                        }
                        if ($model->instruccion_prueba == '' || $model->instruccion_prueba == '<BR>') {
                            $mensaje.= 'El campo Instrucciones no puede estar vacio, Por favor ingrese datos.<br>';
                        }
                    }

                    //compruebo que los caracteres sean los permitidos
                    if ($nombre != '') {
                        $permitidos = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ÁÉÍÓÚÑÄËÏÖÜ ";
                        for ($i = 0; $i < strlen($nombre); $i++) {
                            if (strpos($permitidos, substr($nombre, $i, 1)) === false) {
                                // echo $nombre . " no es válido<br>";
                                $resultadoErroneo = false;
                                break;
                            } else {
                                // echo $nombre . " es válido<br>";
                                $nombreValido = $nombre;
                                $resultadoExitoso = true;
                            }
                        }

                        if ($resultadoErroneo == false) {
                            $mensaje.= 'El campo nombre de prueba solo puede contener letras y números, intente nuevamente <br>';
                            //  echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje_error));
                        }
                    }

                    if ($mensaje != '') {
                        echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                    } else {
                        if ($model != null) {

                            $usuario_id = Yii::app()->user->id;
                            $estatus = 'A';
                            $fecha = date('Y-m-d H:i:s');
                            $model->nombre = $nombreValido;
                            $model->estatus = $estatus;
                            $model->usuario_act_id = $usuario_id;
                            $model->fecha_act = $fecha;

                            //   var_dump($model);
                            if ($model->save()) {
                                $this->registerLog('ACTUALIZACION', 'catalogo.prueba.ModificarPrueba', 'EXITOSO', 'Permite modificar un registro de una prueba');
                                if ($resultadoExitoso == true) {
                                    $nombreGuardado = $model->nombre;
                                    $mensaje_exitoso = 'Modificación exitosa de la prueba:' . ' ' . $nombreGuardado;
                                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                                    Yii::app()->end();
                                }
                            } else { // error que no guardo
                                $mensaje.= 'No se pudo guardar los datos, Por favor intente nuevamente <br>';
                                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                                Yii::app()->end();
                            }
                        } else {
                            $mensaje = 'Por favor seleccione un registro de prueba para modificar';
                            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                            Yii::app()->end();
                        }
                    }
                } else {
                    $mensaje = '404, No se ha especificado el registro de prueba que desea modificar. Recargue la página e intentelo de nuevo.';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {

                $this->render('_form', array(
                    'model' => $model,
                    'id_decod' => $id_decod,
                    'variable' => $variable
                ));
            }
        }
    }

    public function actionInactivarPrueba() {


        $id = $this->getRequest('id');

        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de prueba que ha solicitado para inactivar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $model = Prueba::model()->findByPk($id_decod);
            if ($model != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'I'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $model->estatus = $estatus;
                $model->usuario_elim_id = $usuario_id;
                $model->fecha_elim = $fecha;
                $nombre = $model->nombre;
                
                SeccionPrueba::model()->updateAll(array('estatus' => 'I'), array('condition' => 'prueba_id=' . $id_decod));
                $Secciones= SeccionPrueba::model()->findAll(array('select'=>'id','condition'=>'prueba_id='. $id_decod));
                 foreach($Secciones as $actualizarSubSecciones)
                 {
                     SubSeccionPrueba::model()->updateAll(array('estatus' => 'I'), array('condition' => 'seccion_prueba_id=' . $actualizarSubSecciones['id']));
                     $subSecciones= SubSeccionPrueba::model()->findAll(array('select'=>'id','condition'=>'seccion_prueba_id='. $actualizarSubSecciones['id']));
                      foreach($subSecciones as $actualizarPreguntas)
                      {
                          Pregunta::model()->updateAll(array('estatus' => 'I'), array('condition' => 'sub_seccion_prueba_id=' . $actualizarPreguntas['id']));
                      }
                 }
                //   var_dump($model);
                if ($model->save()) {
                    $this->registerLog('INACTIVAR', 'catalogo.prueba.InactivarPrueba', 'EXITOSO', 'Permite inactivar un registro de una prueba');
                    $mensaje_exitoso = 'Inactivación exitosa de la prueba:' . ' ' . $nombre;
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo inactivar el registro de prueba: ' . $nombre . ', Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro de prueba para inactivar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

    public function actionActivarPrueba() {


        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de prueba que ha solicitado para activar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {

            $model = Prueba::model()->findByPk($id_decod);
            if ($model != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $model->estatus = $estatus;
                $model->usuario_ini_id = $usuario_id;
                $model->fecha_ini = $fecha;
                $model->usuario_elim_id = $usuario_id;
                $model->fecha_elim = null;
                $model->usuario_act_id = $usuario_id;
                $model->fecha_act = null;
                $nombre = $model->nombre;

                if ($model->save()) {
                    $this->registerLog('ACTIVAR', 'catalogo.prueba.ActivarPrueba', 'EXITOSO', 'Permite activar un registro de una prueba');
                    $mensaje_exitoso = 'Activación exitosa de la prueba:' . ' ' . $nombre;
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $mensaje.= 'No se pudo activar el registro de prueba: ' . $nombre . ', Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro de prueba para activar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $estatus = $data->estatus;
        $columna = '<div class="left" class="action-buttons"> ';

        if ($estatus == "I" && $estatus != "") {
            //  $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/prueba/tipoPrueba/consultarPrueba/id/' . $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in add-consultar", "title" => "Ver datos", "data" => $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-ok green add-activar", "title" => "Activar datos", "data" => $id)) . '&nbsp;&nbsp;';
        } else {
            //$columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/prueba/tipoPrueba/consultarPrueba/id/' . $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in add-consultar", "title" => "Ver datos", "data" => $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '/prueba/tipoPrueba/modificarPrueba/id/' . $id)) . '&nbsp;&nbsp;';
            //$columna .= CHtml::link("", "", array("class" => "fa icon-pencil green add-modificar", "title" => "Editar datos", "data" => $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red add-inactivar", "title" => "Inactivar datos", "data" => $id)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';

        return $columna;
    }

}
