<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AplicacionPruebaController
 *
 * @author pleiber
 */
class AplicacionPruebaController extends Controller {

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'write' => 'Permite la Registro o Presentación de Prueba de Conocimiento',
        'admin' => 'Permite la Administración de La Aplicación de la Prueba de Conocimiento',
        'label' => 'Módulo de Presentación de Prueba de Conocimiento',
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'admin', 'mostrarFormDeSeccion', 'inactivarSeccionPrueba', 'activarSeccionPrueba', 'obtenerDatos', 'verificarEstatus', 'verificarEstatusPueba', 'validarPrueba', 'presentacion', 'evaluarPrueba', 'mostrarResultado'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin', 'validarPrueba', 'presentacion', 'evaluarPrueba', 'mostrarResultado'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'validarPrueba', 'presentacion', 'evaluarPrueba'),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * 
     * @param int $id  Indica el ID de la postulacion
     * @param int $per  Indicala el perido actual (apertura_periodo_id)
     * @param string tipo indica el tipo de periodo si es concurso publico o sistema de merito 
     */
    public function actionValidarPrueba($id, $per, $tipo) {
        $model = null;
        $postulacionId = $this->getIdDecoded($id);
        $aperturaPeriodoId = $this->getIdDecoded($per);
        $nombreTipoPeriodo = base64_decode($tipo);
        $horaActual = date('H:i:s');

        //ld($idDecoded, $aperturaPeriodoDecoded, Yii::app()->user->id, Yii::app()->user->name); die();

        $result = SeccionPrueba::model()->spValidacionPrueba($postulacionId, Yii::app()->user->id, Yii::app()->user->name, 'prueba.aplicacionPrueba.validarPrueba', Helper::getRealIP(), 'VALIDAR', $horaActual, $horaActual);
        // var_dump($result);die();
        if (isset($result['id_prueba'])) {
            $model = $this->loadModel($result['id_prueba']);
        }
        $this->render('_instrucciones', array(
            'model' => $model,
            'nombreTipoPeriodo' => $nombreTipoPeriodo,
            'postulacionId' => $postulacionId,
            'result' => $result,
            'aperturaPeriodoId' => $aperturaPeriodoId,
        ));
    }

    /**
     * 
     * @param int $pos       indica el ID de la postulacion
     * @param int per        indica el ID del periodo actual (apertura_periodo_id)
     * @param string $tipo   indica el tipo de periodo si es concurso publico o sistema de merito 
     * @param int $prueba  indica el ID de la prueba
     */
    public function actionPresentacion($pos, $per, $tipo, $prueba) {

        $postulacionId = $this->getIdDecoded($pos);
        $aperturaPeriodoId = $this->getIdDecoded($per);
        $nombreTipoPeriodo = base64_decode($tipo);
        $pruebaId = $this->getIdDecoded($prueba);

        $test = DatosPrueba::model()->getDatosCompletosPrueba($pruebaId);
        $modelPost = Postulacion::model()->getModelPostulacion($postulacionId); //obteniendo datos de la postulacion
        $horaActual = date('H:i:s');
        $horaFinalPrueba = $this->getHoraFinalPrueba($horaActual, $test['duracion']);

        $result = SeccionPrueba::model()->spValidacionPrueba($postulacionId, Yii::app()->user->id, Yii::app()->user->name, 'prueba.aplicacionPrueba.validarPrueba', Helper::getRealIP(), 'PRESENTACION', $horaActual, $horaFinalPrueba);
        $modelAplicacion = $this->loadModelApliPrueba($result['aplicacion_prueba_id']); // cargando el modelo AplicacionPrueba
        $tiempoSegundos = $this->getTiempoEnSegundos($test['duracion']); //contiene la duracion en segundos de la prueba
        //var_dump($modelAplicacion);die();
        // $talentoHumanoId = AdminTalentoHumano::model()->getTalentoHumanoIdFromUserId($usuarioId);

        if ($result['resultado'] == 'success') {
            //ld($test['opciones']);die();
            $this->render('_pruebaConocimiento', array(
                'test' => $test,
                'modelAplicacion' => $modelAplicacion,
                'nombreTipoPeriodo' => $nombreTipoPeriodo,
                'postulacionId' => $postulacionId,
                'aperturaPeriodoId' => $aperturaPeriodoId,
                'modelPost' => $modelPost,
                'pruebaId' => $pruebaId,
                'tiempoSegundos' => $tiempoSegundos
            ));
        } else {
            throw new CHttpException(403, 'ERR01P: ' . $result['mensaje']);
        }
    }

    /**
     * 
     * @param int $pos       indica el ID de la postulacion
     * @param int per        indica el ID del periodo actual (apertura_periodo_id)
     * @param string $tipo   indica el tipo de periodo si es concurso publico o sistema de merito 
     * @param int $prueba  indica el ID de la prueba
     */
    public function actionEvaluarPrueba($pos, $per, $tipo, $prueba) {

        $request = Yii::app()->request;

        if ($request->isPostRequest && $request->isAjaxRequest) {
            $test = null;
            $postulacionId = $this->getIdDecoded($pos);
            $aperturaPeriodoId = $this->getIdDecoded($per);
            $nombreTipoPeriodo = base64_decode($tipo);
            $pruebaId = $this->getIdDecoded($prueba);

            $datosPrueba = DatosPrueba::model()->getDatosCompletosPrueba($pruebaId);
            $result = SeccionPrueba::model()->spValidacionPrueba($postulacionId, Yii::app()->user->id, Yii::app()->user->name, 'prueba.aplicacionPrueba.validarPrueba', Helper::getRealIP(), 'EVALUACION', date('H:i:s'), date('H:i:s'));
            //var_dump($result);die(); 
            $horaActual = DateTime::createFromFormat('H:i:s', date('H:i:s'));


            $talentoHumanoId = $result['talento_humano_id'];
            if ($result['resultado'] == 'success') {
                // Obteniendo datos de la Prueba
                $respuestasForm = $this->getRequest('respuestas');
                $preguntas = json_decode(base64_decode($this->getRequest('preguntas')));
                $aplicacionPruebaId = base64_decode($this->getRequest('aplicacion_prueba_id'));
                $modelAplicacion = $this->loadModelApliPrueba($aplicacionPruebaId); // cargando el modelo AplicacionPrueba


                $respuestas = array();
                $preguntasRespuestas = array();
                foreach ($preguntas as $preguntaId) {
                    if (!isset($respuestasForm[$preguntaId]) || !is_numeric($respuestasForm[$preguntaId])) {
                        $respuestasForm[$preguntaId] = null;
                    }
                    $respuestas[] = $respuestasForm[$preguntaId];
                    $preguntasRespuestas[$preguntaId] = $respuestasForm[$preguntaId];
                }


                $calificacion = 0;
                $preguntasCorrectas = 0;
                $preguntasIncorrectas = 0;
                foreach ($datosPrueba['preguntas'] as $indexPregunta => $pregunta) {
                    $preguntaId = (int) $pregunta['pregunta_id'];
                    $ponderacion = (int) $pregunta['ponderacion'];
                    $respuestaCorrectaId = (int) $pregunta['respuesta_correcta_id'];
                    if ($respuestasForm[$preguntaId] == $respuestaCorrectaId) {
                        $calificacion = $calificacion + $ponderacion;
                        $preguntasCorrectas++;
                    } else {
                        $preguntasIncorrectas++;
                    }
                }
                $finalizadoPor = $this->getPost('finalizada_por');
                // var_dump($finalizadoPor);die();

                $dataPreguntaRespuesta = json_encode(array('preguntas' => $preguntas, 'respuestas' => $respuestas, 'preguntasRespuestas' => $preguntasRespuestas));
                //llamada al procedimiento almacenado que almacena los datos ingresados por el usuario en la prueba
                $resultadoPrueba = AplicacionPrueba::model()->spRegistroResultadoPrueba(
                        $aplicacionPruebaId, $dataPreguntaRespuesta, $preguntas, $respuestas, $talentoHumanoId, $aperturaPeriodoId, $pruebaId, $calificacion, $preguntasCorrectas, $preguntasIncorrectas, $finalizadoPor, $modelAplicacion->hora_final_prueba, $postulacionId, Yii::app()->user->id, date('Y-m-d H:i:s')
                );

                Postulacion::model()->deleteCachePostulacion($postulacionId); // borrando la cahe de la postulacion
                // var_dump($resultadoPrueba);die();
                if ($resultadoPrueba['estatus'] == "success") {
                    $this->jsonResponse(array('resultado' => $resultadoPrueba['estatus'], 'mensaje_sistema' => $resultadoPrueba['mensaje_sistema'], 'mensaje_usuario' => $resultadoPrueba['mensaje_usuario'], 'seccion' => $resultadoPrueba['seccion'], 'estatus_postulacion' => $resultadoPrueba['estatus_postulacion']));
                } else {
                    $this->jsonResponse(array('resultado' => $resultadoPrueba['estatus'], 'mensaje_sistema' => $resultadoPrueba['mensaje_sistema'], 'mensaje_usuario' => $resultadoPrueba['mensaje_usuario'], 'seccion' => $resultadoPrueba['seccion'], 'estatus_postulacion' => $resultadoPrueba['estatus_postulacion']));
                }

                /* var_dump(Yii::app()->user->id, $talentoHumanoId,  $postulacionId,$aperturaPeriodoId, $pruebaId);
                  var_dump($preguntas);
                  echo json_encode($preguntas);
                  var_dump($respuestas);
                  echo json_encode($respuestas);
                  var_dump($calificacion);
                  var_dump($preguntasCorrectas);
                  var_dump($preguntasIncorrectas);die(); */
            } else {
                throw new CHttpException(403, 'ERR02EP: ' . $result['mensaje']);
            }
        } else {
            throw new CHttpException(403, 'ERR01EP: No está permitido efectuar esta acción por medio de esta vía.');
        }
    }

    /**
     * 
     * @param int $pos       indica el ID de la postulacion
     * @param int per        indica el ID del periodo actual (apertura_periodo_id)
     * @param string $tipo   indica el tipo de periodo si es concurso publico o sistema de merito 
     * @param int $prueba  indica el ID de la prueba
     */
    public function actionMostrarResultado($pos, $per, $tipo, $prueba) {

        $request = Yii::app()->request;

        if ($request->isPostRequest) {

            $postulacionId = $this->getIdDecoded($pos);
            $aperturaPeriodoId = $this->getIdDecoded($per);
            $nombreTipoPeriodo = base64_decode($tipo);
            $pruebaId = $this->getIdDecoded($prueba);

            $test = DatosPrueba::model()->getDatosCompletosPrueba($pruebaId);
            $modelPost = Postulacion::model()->getModelPostulacion($postulacionId); //obteniendo datos de la postulacion
            $usuarioId = Yii::app()->user->id;
            $tiempoSegundos = $this->getTiempoEnSegundos($test['duracion']); //contiene la duracion en segundos de la prueba
            $talentoHumanoId = AdminTalentoHumano::model()->getTalentoHumanoIdFromUserId($usuarioId);

            $dataPrueba = $this->getPost("Prueba");
            $mensajeSistema = $dataPrueba['mensaje_sistema'];
            $mensajeUsuario = $dataPrueba['mensaje_usuario'];
            $estatus = $dataPrueba['estatus'];
            $seccion = $dataPrueba['seccion'];
            $estatusPostulacion = $dataPrueba['estatus_postulacion'];
            $aplicacionPruebaId = base64_decode($dataPrueba['aplicacion_prueba_id']);

            if (is_numeric($aplicacionPruebaId)) {

                $modelAplicacionPrueba = AplicacionPrueba::model()->loadAplicacionPrueba($aplicacionPruebaId);

                if ($modelAplicacionPrueba) {

                    if ($modelAplicacionPrueba->aspirante_id == $talentoHumanoId) {

                        $dataPruebaStr = $modelAplicacionPrueba->data_pregunta_respuesta;
                        $dataPrueba = json_decode($dataPruebaStr, true);
                        $preguntasYRespuestas = $dataPrueba['preguntasRespuestas'];

                        $this->render('_resultadoPruebaConocimiento', array(
                            'test' => $test,
                            'nombreTipoPeriodo' => $nombreTipoPeriodo,
                            'postulacionId' => $postulacionId,
                            'aperturaPeriodoId' => $aperturaPeriodoId,
                            'modelPost' => $modelPost,
                            'pruebaId' => $pruebaId,
                            'tiempoSegundos' => $tiempoSegundos,
                            'mensajeUsuario' => $mensajeUsuario,
                            'mensajeSistema' => $mensajeSistema,
                            'estatus' => $estatus,
                            'estatusPostulacion' => $estatusPostulacion,
                            'modelAplicacionPrueba' => $modelAplicacionPrueba,
                            'dataPrueba' => $dataPrueba,
                            'preguntasYRespuestas' => $preguntasYRespuestas,
                        ));
                    } else {
                        throw new CHttpException(400, 'ERR064EP: La prueba de conocimiento indicada no corresponde al usuario actual.');
                    }
                } else {
                    throw new CHttpException(404, 'ERR063EP: La prueba de conocimiento indicada no ha podido ser encontrada en nuestra base de datos.');
                }
            } else {
                throw new CHttpException(400, 'ERR062EP: No se ha recibido correctamente el identificador de la prueba de conocimiento.');
            }
        } else {
            throw new CHttpException(403, 'ERR061EP: No está permitido efectuar esta acción por medio de esta vía.');
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SeccionPrueba the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = SeccionPrueba::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function loadModelApliPrueba($id) {
        $model = AplicacionPrueba::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Este metodo se encarga de que se cumpla lo siguiente:
     * Si eres ASPIRANTE, CONTRATADO, FIJO o JUBILADO entonces puedes editar o registrar tus datos, no los de otro.
     * Si NO eres ASPIRANTE, CONTRATADO, FIJO o JUBILADO entonces te rijes por la permisologia de tu grupo, por lo que este metodo no se encarga de filtrarte.
     * 
     * @param int $talentoHumanoId Id del Talento Humano
     * @return boolean
     */
    public function hasPermission($talentoHumanoId) {
        $groupId = Yii::app()->user->group;
        // Si eres ASPIRANTE, CONTRATADO, FIJO o JUBILADO entonces puedes editar o registrar tus datos, no los de otro.
        /*      Los jubilados podran entrar como contratados en alguna oportunidad, falta validar esta condición.                                   */
        if (in_array($groupId, array(UserGroups::ASPIRANTE, UserGroups::EMP_CONTRATADO, UserGroups::EMP_FIJO))) {
            $userId = Yii::app()->user->id;
            $talentoHumano = $this->loadModelTh($talentoHumanoId);
            if ($userId == $talentoHumano->user_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TalentoHumano the loaded model
     * @throws CHttpException
     */
    public function loadModelTh($id) {
        $model = null;
        if (is_numeric($id)) {
            $cacheIndex = strtr(TalentoHumanoController::$cacheIndex, array('{talentoHumanoId}' => $id));
            $model = Yii::app()->cache->get($cacheIndex);
            if (!$model) {
                $model = AdminTalentoHumano::model()->findByPk($id);
                if ($model) {
                    Yii::app()->cache->set($cacheIndex, $model, Helper::$SEGUNDOS_DOS_SEMANAS);
                }
            }
        } else { // El ID no es numérico
            throw new CHttpException(404, 'ERR01L2: El recurso requerido no ha sido encontrado, o puede que no esté autorizado para efectuar esta acción.');
        }
        if ($model === null) { // Se hizo la consulta por el ID y no se encontró nada
            throw new CHttpException(404, 'ERR01L1: El recurso requerido no ha sido encontrado, o puede que no esté autorizado para efectuar esta acción.');
        }
        return $model;
    }

    /**
     * 
     * @param string $horaActual formato (HH:MM:SS)
     * @param string $duracionPrueba formato (HH:MM:SS)
     * @return string formato (HH:MM:SS)    
     */
    public function getHoraFinalPrueba($horaActual, $duracionPrueba) {
        $duracion = explode(':', $duracionPrueba);
        $horas = $duracion[0];
        $minutos = $duracion[1];
        $segundos = $duracion[2];
        $dt = new DateTime($horaActual);
        $dt->add(new DateInterval("PT{$horas}H{$minutos}M{$segundos}S"));
        $hora = $dt->format('h:i:s');
        return $hora;
    }

    /**
     * 
     * @param string $tiempoPrueba formato (HH:MM:SS)
     * @return int    
     */
    public function getTiempoEnSegundos($tiempoPrueba) {
        $hora = explode(":", $tiempoPrueba);
        $horaSegundos = $hora[0] * 3600;
        $minutosSegungos = $hora[1] * 60;
        $segundos = $hora[2] * 1;
        return ($horaSegundos + $minutosSegungos + $segundos);
    }

}
