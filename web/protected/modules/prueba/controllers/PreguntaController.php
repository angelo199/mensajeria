<?php

class PreguntaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction = 'lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Permite la Consulta de Pregunta y Respuestas',
        'write' => 'Permite la Creación y Modificación de Pregunta y Respuestas',
        'admin' => 'Permite la Administración Completa de Preguntas y Respuestas',
        'label' => 'Módulo de Administración de Preguntas (prueba.pregunta)'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'admin', 'seleccionarSeccion', 'seleccionarNombreSeccion', 'registrarPreguntaSimple', 'registrarPreguntaSimpleImagen', 'registrarPreguntaDomino', 'cargarImagen', 'informacion', 'modificar', 'modificarPreguntaSimple', 'modificarPreguntaSimpleImagen', 'modificarPreguntaDomino', 'inactivarPregunta', 'activarPregunta','verificarEstatusSeccion'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     * @var int sec Id de la Sección
     */
    public function actionLista($sec) {
        $model = new Pregunta('search');
        $model->unsetAttributes();  // clear any default values
        if ($this->hasQuery('Pregunta')) {
            $model->attributes = $this->getQuery('Pregunta');
        }
        $dataProvider = $model->search();
        $this->render('admin', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionSeleccionarSeccion() {
        $tipo_prueba_id = $_GET['Pregunta']['id'];
            echo CHtml::tag('option', array('value' => ''), CHtml::encode('Seleccione'), true);            
            if ($tipo_prueba_id) {
                $lista = SeccionPrueba::model()->getSeccionesByPrueba($tipo_prueba_id);
                $lista = CHtml::listData($lista, 'id', 'nombre');

                foreach ($lista as $valor => $descripcion) {
                    echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
                }
            }
    }

    public function actionSeleccionarNombreSeccion() {
        /* NOMBRE DE LA SECCIÓN */
        $seccion = 1;
        $item = $_REQUEST['Pregunta']['seccion_prueba_id']; /* SECCION O SUBSECCION */
        $seccionPadre = '';
        $tipo = 'Sección';
        
        /* Seccion */
        if ($seccion == 1) {
            $modelSeccion = SeccionPrueba::model()->findAll(array('condition' => 'id = ' . $item));
            $instruccion = $modelSeccion[0]['instruccion_seccion'];
            $cantidadPreguntas = Pregunta::model()->findAll(array('condition' => 'seccion_prueba_id = ' . $item));
            $cantidadPreguntas = count($cantidadPreguntas);
        }
        
        //var_dump($modelSeccion);die();
        $nombre = $modelSeccion[0]['nombre'];
        $duracion_seccion = $modelSeccion[0]['duracion_seccion'];
        $cantidad_respuesta = $modelSeccion[0]['cantidad_respuesta'];
        $tipo_pregunta = $modelSeccion[0]['tipo_pregunta'];
        $tipo_pregunta_acronimo = $modelSeccion[0]['tipo_pregunta'];

        if ($tipo_pregunta == 'T') {
            $tipo_pregunta = 'Selección Simple';
            $renderPartial = '_formSimple';
        } else {
            $tipo_pregunta = 'No posee';
        }
        $info = "<div id='detalles' class='infoDialogBox'><p>
            Datos de la $tipo<br><br>
            <b>Nombre:</b> $nombre<br>
            $seccionPadre
            <b>Duración:</b> $duracion_seccion<br>
            <b>Cantidad de Respuestas:</b> $cantidad_respuesta<br>
                <label id='cantidad_respuesta' class='hide'>$cantidad_respuesta</label>
            <b>Tipo de pregunta:</b> $tipo_pregunta<br>
            <b>Instrucción de la $tipo:</b> $instruccion<br>
            <b>Cantidad de preguntas formuladas: </b><label id='cantidadPregunta'>$cantidadPreguntas</label>
        </p></div>";

        $ponderacion = Configuracion::model()->findAll(array('condition' => "nombre = 'PONDERACION'"));
        $ponderacionCantidad = isset($ponderacion[0]['valor_int']) ? $ponderacion[0]['valor_int'] : "";

        $this->renderPartial($renderPartial, array(
            'info' => $info,
            'cantidadRespuestas' => $cantidad_respuesta,
            'ponderacion' => $ponderacionCantidad,
            'tipo_pregunta' => $tipo_pregunta_acronimo));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Pregunta('search');
        $model->unsetAttributes();  // clear any default values
        if ($this->hasQuery('Pregunta')) {
            $model->attributes = $this->getQuery('Pregunta');
        }
        $dataProvider = $model->search();
        $this->render('admin', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id) {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro($per, $secc) {
        $model = new Pregunta;
        $model->id = $this->getIdDecoded($per);
        $model->seccion_prueba_id = $this->getIdDecoded($secc);
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if ($this->hasPost('Pregunta')) {
            $model->attributes = $this->getPost('Pregunta');
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $listaSecciones = SeccionPrueba::model()->getSeccionesByPrueba($model->id);
        
        // Sólo toma los períodos de aperturados de Concurso Público
        $periodosAperturados = AperturaPeriodo::model()->findAll(array('order' => 'nombre_clave ASC', 'condition' => "estatus = 'A' AND tipo_apertura_id = 1"));
        $seccion = CSeccion::getData();
        $modelPregunta = new Pregunta();
        //var_dump($seccion);die();
        $this->render('create', array(
            'model' => $model,
            'modelPregunta' => $modelPregunta,
            'periodosAperturados' => $periodosAperturados,
            'listaSecciones' => $listaSecciones,
            'seccion' => $seccion,
        ));
    }

    public function actionRegistrarPreguntaSimple() {
        $respuesta = $_POST['Respuesta'];

        $cantidadRespuesta = $_POST['cantidadRespuestas'];
        $detallesSeccion = $_POST['detallesSeccion'];
        $pregunta_nombre = $_POST['pregunta_nombre'];
        $pregunta_ponderacion = $_POST['pregunta_ponderacion'];
        $pregunta_seccion_prueba_id = $_POST['pregunta_seccion_prueba_id'];
        $respuestaCorrecta = $_POST['respuestaCorrecta'];
        $tipoPrueba = $_POST['tipoPrueba'];
        $tipo_pregunta_acronimo = $_POST['tipo_pregunta'];
        
        $usuario_id = Yii::app()->user->id;
        $fecha = date('Y-m-d H:i:s');

        $modelPregunta = new Pregunta();
        $modelPregunta->nombre = $pregunta_nombre;
        $modelPregunta->usuario_ini_id = $usuario_id;
        $modelPregunta->fecha_ini = $fecha;
        $modelPregunta->estatus = 'A';
        $sub_seccion_prueba_id = NULL;

        /* SECCION */
        //if ($pregunta_seccion_prueba_id == 1) {
            $seccion_id = $detallesSeccion;
        //}
            
        /* SUBSECCION  else if ($pregunta_seccion_prueba_id == 2) {
            $modelSubSeccion = SubSeccionPrueba::model()->findAll(array('condition' => 'id = ' . $detallesSeccion));
            $seccion_id = $modelSubSeccion[0]['seccion_prueba_id'];
            $modelPregunta->sub_seccion_prueba_id = $detallesSeccion;
            $sub_seccion_prueba_id = $detallesSeccion;
        } */

        $modelPregunta->seccion_prueba_id = $seccion_id;
        $modelPregunta->ponderacion = $pregunta_ponderacion;
//        var_dump($modelPregunta);
        $guardarPregunta = $modelPregunta->guardarCambiosSimple($pregunta_nombre, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo);
        if ($guardarPregunta == 2) {
            echo $guardarPregunta;
        } else {
            $pregunta_id = $guardarPregunta[0]['id'];
            /* Guardar Respuestas */
            $modelRespuestas = new Respuesta();
            if ($modelRespuestas->guardarCambiosRespuestasSimple($respuesta, $respuestaCorrecta, $usuario_id, $fecha, $sub_seccion_prueba_id, $pregunta_id)) {
                echo '1';
            }
        }

//        foreach ($respuesta AS $r1){
//            foreach ($respuesta AS $r2){
//                if($r1 == $r2){
//                    var_dump($r1);
//                }
//            }
//        }
    }

    public function actionModificarPreguntaSimple() {

        $respuesta = $_POST['Respuesta'];
        $respuestaHidden = $_POST['RespuestaHidden'];
        
        $id = $_POST['id'];
        $cantidadRespuesta = $_POST['cantidadRespuestas'];
        $detallesSeccion = $_POST['detallesSeccion'];
        $pregunta_nombre = $_POST['pregunta_nombre'];
        $pregunta_ponderacion = $_POST['pregunta_ponderacion'];
        $pregunta_seccion_prueba_id = $_POST['pregunta_seccion_prueba_id'];
        $respuestaCorrecta = $_POST['respuestaCorrecta'];
        $tipoPrueba = $_POST['tipoPrueba'];
        $tipo_pregunta_acronimo = $_POST['tipo_pregunta'];

        $usuario_id = Yii::app()->user->id;
        $fecha = date('Y-m-d H:i:s');

        $modelPregunta = new Pregunta();
        $modelPregunta->nombre = $pregunta_nombre;
        $modelPregunta->usuario_ini_id = $usuario_id;
        $modelPregunta->fecha_ini = $fecha;
        $modelPregunta->estatus = 'A';
        $sub_seccion_prueba_id = NULL;

        /* SECCION */
        //if ($pregunta_seccion_prueba_id == 1) {
            $seccion_id = $detallesSeccion;
        //}
        /* SUBSECCION  else if ($pregunta_seccion_prueba_id == 2) {
            $modelSubSeccion = SubSeccionPrueba::model()->findAll(array('condition' => 'id = ' . $detallesSeccion));
            $seccion_id = $modelSubSeccion[0]['seccion_prueba_id'];
            $modelPregunta->sub_seccion_prueba_id = $detallesSeccion;
            $sub_seccion_prueba_id = $detallesSeccion;
        } */

        $modelPregunta->seccion_prueba_id = $seccion_id;
        $modelPregunta->ponderacion = $pregunta_ponderacion;
//        var_dump($modelPregunta);
        $guardarPregunta = $modelPregunta->modificarCambiosSimple($id, $pregunta_nombre, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo);
        if ($guardarPregunta == 2) {
            echo $guardarPregunta;
        } else {
//            $pregunta_id = $guardarPregunta[0]['id'];
            /* Guardar Respuestas */
            $modelRespuestas = new Respuesta();
            $resultadoRespuesta = $modelRespuestas->modificarCambiosRespuestasSimple($id, $respuesta, $respuestaHidden, $respuestaCorrecta, $usuario_id, $fecha, $sub_seccion_prueba_id);
            var_dump($resultadoRespuesta);
            if ($resultadoRespuesta) {
                echo '1';
            }
        }

//        foreach ($respuesta AS $r1){
//            foreach ($respuesta AS $r2){
//                if($r1 == $r2){
//                    var_dump($r1);
//                }
//            }
//        }
    }

    public function actionRegistrarPreguntaSimpleImagen() {
//        $respuesta = $_POST['Respuesta'];
//        detallesSeccion	2
//        pregunta	Captura de pantalla de 2015-01-20 09:15:59.png
//        pregunta_ponderacion	99
//        pregunta_seccion_prueba_i...	1
//        respuesta	1
//        tipoPrueba	12
//        tipo_pregunta	I
//        $cantidadRespuesta = $_POST['cantidadRespuestas'];
        $detallesSeccion = $_POST['detallesSeccion'];
        $pregunta = $_POST['pregunta'];
        $pregunta_ponderacion = $_POST['pregunta_ponderacion'];
        $pregunta_seccion_prueba_id = $_POST['pregunta_seccion_prueba_id'];
        $respuesta = $_POST['respuesta'];
        $tipoPrueba = $_POST['tipoPrueba'];
        $tipo_pregunta_acronimo = $_POST['tipo_pregunta'];
        $usuario_id = Yii::app()->user->id;
        $fecha = date('Y-m-d H:i:s');
        $sub_seccion_prueba_id = NULL;

        $modelPregunta = new Pregunta();

        /* SECCION */
        if ($pregunta_seccion_prueba_id == 1) {
            $seccion_id = $detallesSeccion;
        }
        /* SUBSECCION */ else if ($pregunta_seccion_prueba_id == 2) {
            $modelSubSeccion = SubSeccionPrueba::model()->findAll(array('condition' => 'id = ' . $detallesSeccion));
            $seccion_id = $modelSubSeccion[0]['seccion_prueba_id'];
            $modelPregunta->sub_seccion_prueba_id = $detallesSeccion;
            $sub_seccion_prueba_id = $detallesSeccion;
        }

        $guardarPregunta = $modelPregunta->guardarCambiosSimpleImagen($pregunta, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo);
        if ($guardarPregunta) {
//            echo 11111;
            $pregunta_id = $guardarPregunta[0]['id'];
            /* Guardar Respuestas */
            $modelRespuestas = new Respuesta();
            if ($modelRespuestas->guardarCambiosRespuestasSimpleImagen($pregunta, $respuesta, $usuario_id, $fecha, $sub_seccion_prueba_id, $pregunta_id)) {
                echo '1';
            }
        }
    }

    public function actionModificarPreguntaSimpleImagen() {

//        $cantidadRespuesta = $_POST['cantidadRespuestas'];
        $id = $_POST['id'];
        $detallesSeccion = $_POST['detallesSeccion'];
        $pregunta = $_POST['pregunta'];
        $pregunta_ponderacion = $_POST['pregunta_ponderacion'];
        $pregunta_seccion_prueba_id = $_POST['pregunta_seccion_prueba_id'];
        $respuesta = $_POST['respuesta'];
        $tipoPrueba = $_POST['tipoPrueba'];
        $tipo_pregunta_acronimo = $_POST['tipo_pregunta'];
        $usuario_id = Yii::app()->user->id;
        $fecha = date('Y-m-d H:i:s');
        $sub_seccion_prueba_id = NULL;

        $modelPregunta = new Pregunta();

        /* SECCION */
        if ($pregunta_seccion_prueba_id == 1) {
            $seccion_id = $detallesSeccion;
        }
        /* SUBSECCION */ else if ($pregunta_seccion_prueba_id == 2) {
            $modelSubSeccion = SubSeccionPrueba::model()->findAll(array('condition' => 'id = ' . $detallesSeccion));
            $seccion_id = $modelSubSeccion[0]['seccion_prueba_id'];
            $modelPregunta->sub_seccion_prueba_id = $detallesSeccion;
            $sub_seccion_prueba_id = $detallesSeccion;
        }

        $guardarPregunta = $modelPregunta->modificarCambiosSimpleImagen($id, $pregunta, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo);
        if ($guardarPregunta) {
//            echo 11111;
//            $pregunta_id = $guardarPregunta[0]['id'];
            /* Guardar Respuestas */
            $modelRespuestas = new Respuesta();
            if ($modelRespuestas->modificarCambiosRespuestasSimpleImagen($id, $pregunta, $respuesta, $usuario_id, $fecha, $sub_seccion_prueba_id)) {
                echo '1';
            }
        }
    }

    public function actionRegistrarPreguntaDomino() {

        $detallesSeccion = $_POST['detallesSeccion'];
        $pregunta = $_POST['pregunta'];
        $pregunta_ponderacion = $_POST['pregunta_ponderacion'];
        $pregunta_seccion_prueba_id = $_POST['pregunta_seccion_prueba_id'];
        $respuestaA = $_POST['respuestaA'];
        $respuestaB = $_POST['respuestaB'];
        $tipoPrueba = $_POST['tipoPrueba'];
        $tipo_pregunta_acronimo = $_POST['tipo_pregunta'];
        $usuario_id = Yii::app()->user->id;
        $fecha = date('Y-m-d H:i:s');
        $sub_seccion_prueba_id = NULL;

        $modelPregunta = new Pregunta();

        /* SECCION */
        if ($pregunta_seccion_prueba_id == 1) {
            $seccion_id = $detallesSeccion;
        }
        /* SUBSECCION */ else if ($pregunta_seccion_prueba_id == 2) {
            $modelSubSeccion = SubSeccionPrueba::model()->findAll(array('condition' => 'id = ' . $detallesSeccion));
            $seccion_id = $modelSubSeccion[0]['seccion_prueba_id'];
            $modelPregunta->sub_seccion_prueba_id = $detallesSeccion;
            $sub_seccion_prueba_id = $detallesSeccion;
        }

        $guardarPregunta = $modelPregunta->guardarCambiosSimpleImagen($pregunta, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo);
        if ($guardarPregunta) {
            $pregunta_id = $guardarPregunta[0]['id'];
            /* Guardar Respuestas */
            $modelRespuestas = new Respuesta();
            if ($modelRespuestas->guardarCambiosRespuestasSimpleImagen($pregunta, $respuestaA, $usuario_id, $fecha, $sub_seccion_prueba_id, $pregunta_id) && $modelRespuestas->guardarCambiosRespuestasSimpleImagen($pregunta, $respuestaB, $usuario_id, $fecha, $sub_seccion_prueba_id, $pregunta_id)) {
                echo '1';
            }
        }
    }

    public function actionModificarPreguntaDomino() {

//        $cantidadRespuesta = $_POST['cantidadRespuestas'];
        $id = $_POST['id'];
        $detallesSeccion = $_POST['detallesSeccion'];
        $pregunta = $_POST['pregunta'];
        $pregunta_ponderacion = $_POST['pregunta_ponderacion'];
        $pregunta_seccion_prueba_id = $_POST['pregunta_seccion_prueba_id'];
        $respuestaA = $_POST['respuestaA'];
        $respuestaAID = $_POST['respuestaAID'];
        $respuestaB = $_POST['respuestaB'];
        $respuestaBID = $_POST['respuestaBID'];
        $tipoPrueba = $_POST['tipoPrueba'];
        $tipo_pregunta_acronimo = $_POST['tipo_pregunta'];
        $usuario_id = Yii::app()->user->id;
        $fecha = date('Y-m-d H:i:s');
        $sub_seccion_prueba_id = NULL;

        $modelPregunta = new Pregunta();

        /* SECCION */
        if ($pregunta_seccion_prueba_id == 1) {
            $seccion_id = $detallesSeccion;
        }
        /* SUBSECCION */ else if ($pregunta_seccion_prueba_id == 2) {
            $modelSubSeccion = SubSeccionPrueba::model()->findAll(array('condition' => 'id = ' . $detallesSeccion));
            $seccion_id = $modelSubSeccion[0]['seccion_prueba_id'];
            $modelPregunta->sub_seccion_prueba_id = $detallesSeccion;
            $sub_seccion_prueba_id = $detallesSeccion;
        }

        $guardarPregunta = $modelPregunta->modificarCambiosSimpleImagen($id, $pregunta, $usuario_id, $fecha, $seccion_id, $sub_seccion_prueba_id, $pregunta_ponderacion, $tipo_pregunta_acronimo);
        if ($guardarPregunta) {
//            $pregunta_id = $guardarPregunta[0]['id'];
            /* Guardar Respuestas */
            $modelRespuestas = new Respuesta();
            if ($modelRespuestas->modificarCambiosRespuestasDomino($id, $pregunta, $respuestaA, $respuestaAID, $usuario_id, $sub_seccion_prueba_id) && $modelRespuestas->modificarCambiosRespuestasDomino($id, $pregunta, $respuestaB, $respuestaBID, $usuario_id, $sub_seccion_prueba_id)) {
                echo '1';
            }
        }
    }

    public function actionCargarImagen() {
//        var_dump($_REQUEST);die();
        $tipo = $_GET['t'];
//        $switch = $_POST['switch'];
        if ($tipo == 'd') {
            $tipo = 'Domino/';
        }
        if ($tipo == 'i') {
            $tipo = 'Imagen/';
        }
//        var_dump($tipo);die();
//        $seccion = Yii::app()->request->getParam('Pregunta');
//        $pruebasUploads = dirname(Yii::app()->getBasePath()) . '/public/prueba/' . $tipo;
//        $seccionUploads = $pruebasUploads . "/" . $seccion['id'];
//        if (!file_exists($pruebasUploads)) {
//            if (mkdir($pruebasUploads)) {
//                mkdir($seccionUploads);
//            }
//        }
//        chmod('/public/prueba/Domino/', 777);
//        $optionsUpload = array(
//            'script_url' => '/prueba/pregunta/eliminarImagenPregunta/',
//            'upload_dir' => dirname(Yii::app()->getBasePath()) . "/public/prueba/Domino/"
//        );
//        if($switch == 1){
        $command = 'chmod 777 -R . ' . '/public/prueba/';
        shell_exec($command);

        new UploadHandler(null, true, null, date('Ymdhms') . 'Pregunta', "/public/prueba/$tipo");

        $command = 'chmod 777 -R . ' . '/public/prueba/';
        shell_exec($command);
//        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id) {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if ($this->hasPost('Pregunta')) {
            $model->attributes = $this->getPost('Pregunta');
            if ($model->save()) {
                if (Yii::app()->request->isAjaxRequest) {
                    $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => 'La actualización de los Datos se ha efectuado de forma exitosa.'));
                    Yii::app()->end();
                }
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id) {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        // Descomenta este código para habilitar la eliminación física de registros.
        // $model->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!$this->hasQuery('ajax')) {
            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
    }

    public function actionInformacion($id) {
//        $model = Pregunta::model()->modelPregunta($id);
        $model = $this->loadModel(base64_decode($id));
//        var_dump($model);die();
        $this->renderPartial('informacion', array(
            'model' => $model,
        ));
    }

    public function actionModificar($id) {

        $model = $this->loadModel(base64_decode($id));
        $cantidadPreguntas = Pregunta::model()->getCantidadPreguntas($model->seccion_prueba_id);
        $ponderacion = Configuracion::model()->findAll(array('condition' => "nombre = 'PONDERACION'"));
        $ponderacionCantidad = isset($ponderacion[0]['valor_int']) ? $ponderacion[0]['valor_int'] : "";

        $this->renderPartial('modificar', array(
            'model' => $model,
            'cantidadPreguntas'=>$cantidadPreguntas,
            'ponderacionCantidad'=>$ponderacionCantidad,
        ));
    }

    public function actionInactivarPregunta($id) {
        $id = base64_decode($_REQUEST['id']);
        if (Pregunta::model()->inactivarPregunta($id)) {
            echo 1;
        } else {
            echo 2;
        }
    }

    public function actionActivarPregunta($id) {
        $id = base64_decode($_REQUEST['id']);
        if (Pregunta::model()->activarPregunta($id)) {
            echo 1;
        } else {
            echo 2;
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Pregunta the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Pregunta::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Pregunta $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if ($this->hasPost('ajax') && $this->getPost('ajax') === 'pregunta-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $estatus = $data['estatus'];
        $id = base64_encode($id_encoded);
        $seccion_id = $data['seccion_prueba_id'];
        

        $aplicoPrueba = Pregunta::model()->aplicoPrueba($id);

        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '#', 'onclick' => 'consultarPregunta("' . $id . '");')) . '&nbsp;&nbsp;';
        if ($estatus == 'A') {
            if (!isset($aplicoPrueba[0]['id'])) {
                $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '#', 'onclick' => 'modificarPregunta("' . $id . '");')) . '&nbsp;&nbsp;';
            } else {
                $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '#', 'onclick' => 'notificacion("' . $id . '");')) . '&nbsp;&nbsp;';
            }
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Inactivar datos", 'href' => '#', 'onclick' => 'inactivarPregunta("' . $id . '");')) . '&nbsp;&nbsp;';
        }
        if ($estatus == 'I') {
            $columna .= CHtml::link("", "", array("class" => "fa icon-check green", "title" => "Activar datos", 'href' => '#', 'onclick' => 'activarPregunta("' . $id . '","'. $seccion_id .'");')) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    public function nombrePregunta($data) {
        $seccion = $data->seccionPrueba->tipo_pregunta;        
        if ($seccion == 'I') {
            return '<img width="100px" src="/public/prueba/Imagen/' . $data->nombre . '">';
        }
        if ($seccion == 'D') {
            return '<img width="100px" src="/public/prueba/Domino/' . $data->nombre . '">';
            // return '
            //     <div class="row-fluid">
            //     <ul class="ace-thumbnails">
            //         <li>
            //             <a href="assets/images/gallery/image-1.jpg" title="Photo Title" data-rel="colorbox">
            //                 <span class="label label-info">Ver Imagen</span>
            //             </a>
            //         </li>
            //     </ul>
            //     </div>';
        } else {
            return ($data->estatus == 'A') ? $data->nombre : "<strike>" . $data->nombre . "</strike>";
        }
    }

    public function estatus($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'I') {
            return 'Inactivo';
        }
    }
    
    public function actionVerificarEstatusSeccion()
    {
        if($_POST)
        {
            $seccion_id=$_POST['seccion_id'];
            $Seccion = SeccionPrueba::model()->find('id= :Id',array(':Id'=>$seccion_id));
            $Seccion = $Seccion['estatus'];
            echo json_encode(array('estatusSeccion'=>$Seccion));
        }
    }

}
