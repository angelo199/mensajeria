<?php

class SeccionController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction = 'lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Permite la Consulta de Secciones de Prueba de Conocimiento',
        'write' => 'Permite la Creación y Modificación de Secciones de Prueba de Conocimiento',
        'admin' => 'Permite la Administración Completa de Secciones de Prueba de Conocimiento',
        'label' => 'Módulo de Sección de Exámenes de Ingreso o Prueba de Conocimiento (prueba.seccion)'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'admin', 'mostrarFormDeSeccion', 'inactivarSeccionPrueba', 'activarSeccionPrueba','obtenerDatos','verificarEstatus','verificarEstatusPueba'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'admin'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionLista() {
        $id_decod = null;
        $model = new SeccionPrueba('search');
        $model->unsetAttributes();  // clear any default values
        if ($this->hasQuery('SeccionPrueba')) {
            $model->attributes = $this->getQuery('SeccionPrueba');
        }
        $dataProvider = $model->search();
        $periodosAperturados = AperturaPeriodo::model()->findAll("t.estatus = 'A' AND t.tipo_apertura_id = :tipoApertura", array(':tipoApertura'=>  TipoPeriodoProceso::CONCURSO_PUBLICO));
        $this->render('admin', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'id_decod' => $id_decod,
            'periodosAperturados' => $periodosAperturados,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($apertura = '') {        
        $model = new SeccionPrueba('search');
        $model->unsetAttributes();  // clear any default values
        if ($this->hasQuery('SeccionPrueba')) {
            $model->attributes = $this->getQuery('SeccionPrueba');
        }
        if(empty($apertura))
        $dataProvider = $model->search();
        else $dataProvider = $model->search (base64_decode ($apertura));        
        $this->render('admin', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
            'apertura'=>$apertura
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id) {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro() {
        $model = new SeccionPrueba;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        // var_dump($model);
        if ($this->hasPost('SeccionPrueba')) {            
            $usuario_id = Yii::app()->user->id; // Con esto digo el tipo que esta logueado.
            $estatus = 'A';     // Asigno de manera automatica el estatus.
            $fecha = date('Y-m-d H:i:s'); // fecha de manera automatica, agarra el tiempo actual.            
            
          // print_r ($_REQUEST['tieneSubSeccion']);
            $model->attributes = $this->cleanInput($this->getPost('SeccionPrueba'));
            $model->estatus = $estatus;
            $model->usuario_ini_id = $usuario_id;
            $model->fecha_ini = $fecha;
            $model->usuario_act_id = $usuario_id;
            $model->fecha_act = $fecha;
            $model->instruccion_seccion = trim($this->cleanInput($_REQUEST['SeccionPrueba']['instruccion_seccion2'])); 
            
            if ($model->save()) {
                $modulo = 'prueba.seccion.registro';
                $ip = Yii::app()->request->userHostAddress;
                $usuario_id = Yii::app()->user->id;
                $username = Yii::app()->user->name;
                $this->registerLog('ESCRITURA', $modulo, 'Exitoso', 'Proceso que Registra las Secciones de las Pruebas', $ip, $usuario_id, $username);
                Yii::app()->user->setFlash('exitoso', 'Registro Satisfactorio de la Sección: ' . $model->nombre);
                $this->redirect('/prueba/seccion/edicion/id/'.base64_encode($model->id));
            } else {
                    $modulo = 'prueba.Seccion.edicion';
                    $ip = Yii::app()->request->userHostAddress;
                    $usuario_id = Yii::app()->user->id;
                    $username = Yii::app()->user->name;
                    $this->registerLog('ESCRITURA', $modulo, 'Error', 'Ocurrio un error cuando intento registrar la Sección de Prueba: ' . $model->nombre, $ip, $usuario_id, $username);
                    Yii::app()->user->setFlash('error', 'Ocurrio un error cuando intento registrar la Sección de Prueba: ' . $model->nombre);
            } 
        }
        $listaPeriodosAperturados = SeccionPrueba::model()->getTipoPrueba();
        $this->render('create', array(
            'model' => $model,
            'listaPeriodosAperturados' => $listaPeriodosAperturados,
        ));
    }
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id) {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if ($this->hasPost('SeccionPrueba')) {

            $model->attributes = $this->cleanInput($this->getPost('SeccionPrueba'));         
            $estatus = 'A';     // Asigno de manera automatica el estatus.
            $fecha = date('Y-m-d H:i:s'); // fecha de manera automatica, agarra el tiempo actual.
            $model->estatus = $estatus;
            $model->fecha_act = $fecha;
            $model->usuario_act_id = Yii::app()->user->id;
            $model->instruccion_seccion = trim($this->cleanInput($_REQUEST['SeccionPrueba']['instruccion_seccion2']));            
            if ($model->save()) {
                $modulo = 'prueba.seccion.edicion';
                $ip = Yii::app()->request->userHostAddress;
                $usuario_id = Yii::app()->user->id;
                $username = Yii::app()->user->name;
                $this->registerLog('ACTUALIZACIÓN', $modulo, 'Exitoso', 'Proceso que Actualiza las Secciones de las Pruebas', $ip, $usuario_id, $username);
                Yii::app()->user->setFlash('exitoso', 'Actualización Exitosa de la Sección de Prueba: ' . $model->nombre);
            } else {
                $modulo = 'prueba.Seccion.edicion';
                $ip = Yii::app()->request->userHostAddress;
                $usuario_id = Yii::app()->user->id;
                $username = Yii::app()->user->name;
                $this->registerLog('ACTUALIZACIÓN', $modulo, 'Error', 'Ocurrio un error cuando intento actualizar la Sección de Prueba: ' . $model->nombre, $ip, $usuario_id, $username);
                Yii::app()->user->setFlash('error', 'Ocurrio un error cuando intento actualizar la Sección de Prueba: ' . $model->nombre);
            }
        }

        // Consulta de las preguntas de esta sección o exámen
        $modelPregunta = new Pregunta('search');
        $modelPregunta->unsetAttributes();  // clear any default values
        if ($this->hasQuery('Pregunta')) {
            $modelPregunta->attributes = $this->getQuery('Pregunta');
        }
        $modelPregunta->seccion_prueba_id = $model->id;

        $dataProviderPreguntas = $modelPregunta->search();
        $apertura = $model->prueba_id; // Periodo aperturado para adecuar al proceso del sistema Haydee le dejamos el nombre prueba_id

        $aux = 1;
        $listaPeriodosAperturados = SeccionPrueba::model()->getTipoPrueba();
        $cantidadPreguntas = Pregunta::model()->getCantidadPreguntas($model->prueba_id);
        $this->render('update', array(
            'model' => $model,
            'modelPregunta' => $modelPregunta,
            'dataProviderPreguntas' => $dataProviderPreguntas,
            'listaPeriodosAperturados' => $listaPeriodosAperturados,
            'apertura'=>$apertura,
            'apertura_nombre'=> SeccionPrueba::model()->getTipoPrueba(base64_decode($apertura)),
            'aux' => $aux,
            'dataProviderPreguntas' => $dataProviderPreguntas,
            'cantidadPreguntas'=>$cantidadPreguntas,
        ));
    }

    public function nombrePregunta($data) {
        $seccion = $data->seccionPrueba->tipo_pregunta;
        if ($seccion == 'I') {
            return '<img width="100px" src="/public/prueba/Imagen/' . $data->nombre . '">';
        }
        if ($seccion == 'D') {
            return '<img width="100px" src="/public/prueba/Domino/' . $data->nombre . '">';
            // return '
            //     <div class="row-fluid">
            //     <ul class="ace-thumbnails">
            //         <li>
            //             <a href="assets/images/gallery/image-1.jpg" title="Photo Title" data-rel="colorbox">
            //                 <span class="label label-info">Ver Imagen</span>
            //             </a>
            //         </li>
            //     </ul>
            //     </div>';
        } else {
            return ($data->estatus == 'A') ? $data->nombre : "<strike>" . $data->nombre . "</strike>";
        }
    }

    public function estatus($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'I') {
            return 'Inactivo';
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionEliminacion($id) {
        $idDecoded = $this->getIdDecoded($id);
        $model = $this->loadModel($idDecoded);
        // Descomenta este código para habilitar la eliminación física de registros.
        // $model->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!$this->hasQuery('ajax')) {
            $this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SeccionPrueba the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = SeccionPrueba::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SeccionPrueba $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if ($this->hasPost('ajax') && $this->getPost('ajax') === 'seccion-prueba-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $prueba_id =  $data["prueba_id"];
        $id = base64_encode($id_encoded);
        $estatus = $data->estatus;
        $columna = '<div class="action-buttons">';
        if ($estatus == "I" && $estatus != "") {
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/prueba/seccion/consulta/id/' . $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-ok green add-activar", "title" => "Activar datos", "data" => $id,"prueba_id"=>$prueba_id)) . '&nbsp;&nbsp;';
        } else {
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '/prueba/seccion/consulta/id/' . $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green editar-datos", "title" => "Editar datos",  "data" => $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red add-inactivar", "title" => "Inactivar datos", "data" => $id)) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtonsPreguntas($data) {
        $id_encoded = $data["id"];
        $estatus = $data['estatus'];
        $id = base64_encode($id_encoded);
        $seccion_id = $data['seccion_prueba_id'];


        $aplicoPrueba = Pregunta::model()->aplicoPrueba($id);

        $columna = '<div class="action-buttons">';
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in", "title" => "Ver datos", 'href' => '#', 'onclick' => 'consultarPregunta("' . $id . '");')) . '&nbsp;&nbsp;';
        if ($estatus == 'A') {
            if (!isset($aplicoPrueba[0]['id'])) {
                $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '#', 'onclick' => 'modificarPregunta("' . $id . '");')) . '&nbsp;&nbsp;';
            } else {
                $columna .= CHtml::link("", "", array("class" => "fa icon-pencil green", "title" => "Editar datos", 'href' => '#', 'onclick' => 'notificacion("' . $id . '");')) . '&nbsp;&nbsp;';
            }
            $columna .= CHtml::link("", "", array("class" => "fa icon-trash red", "title" => "Inactivar datos", 'href' => '#', 'onclick' => 'inactivarPregunta("' . $id . '");')) . '&nbsp;&nbsp;';
        }
        if ($estatus == 'I') {
            $columna .= CHtml::link("", "", array("class" => "fa icon-check green", "title" => "Activar datos", 'href' => '#', 'onclick' => 'activarPregunta("' . $id . '","'. $seccion_id .'");')) . '&nbsp;&nbsp;';
        }
        $columna .= '</div>';
        return $columna;
    }

    public function estatusPrueba($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'I') {
            return 'Inactivo';
        }
    }

    public function actionInactivarSeccionPrueba() {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de la Sección de Prueba que ha solicitado para inactivar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $model = SeccionPrueba::model()->findByPk($id_decod);
            if ($model != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'I'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $model->estatus = $estatus;
                $model->usuario_elim_id = $usuario_id;
                $model->fecha_elim = $fecha;
                $nombre = $model->nombre;
                
                SubSeccionPrueba::model()->updateAll(array('estatus' => 'I'), array('condition' => 'seccion_prueba_id=' . $id_decod));
                $subSecciones= SubSeccionPrueba::model()->findAll(array('select'=>'id','condition'=>'seccion_prueba_id='. $id_decod));
                 foreach($subSecciones as $actualizar)
                 {
                     Pregunta::model()->updateAll(array('estatus' => 'I'), array('condition' => 'sub_seccion_prueba_id=' . $actualizar['id']));
                 }
                
                //   var_dump($model);
                if ($model->save()) {
                    $this->registerLog('INACTIVAR', 'prueba.SeccionPrueba.InactivarSeccionPrueba', 'EXITOSO', 'Permite inactivar un registro de Sección de Prueba');
                    $mensaje_exitoso = 'Inactivación exitosa de la Sección:' . ' ' . $nombre;
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $this->registerLog('INACTIVAR', 'prueba.SeccionPrueba.InactivarSeccionPrueba', 'FALLIDO', 'Permite inactivar un registro de Sección de Prueba');
                    $mensaje.= 'No se pudo inactivar el registro de Sección: ' . $nombre . ', Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro de la Sección de Prueba para inactivar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }

    public function actionActivarSeccionPrueba() {

        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        $mensaje = "";
        $mensaje_exitoso = "";
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de la Sección de Prueba que ha solicitado para activar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
            $model = SeccionPrueba::model()->findByPk($id_decod);
            if ($model != null) {
                $usuario_id = Yii::app()->user->id;
                $estatus = 'A'; //Estatus inactivado
                $fecha = date('Y-m-d H:i:s');
                $model->estatus = $estatus;
                $model->usuario_ini_id = $usuario_id;
                $model->fecha_ini = $fecha;
                $model->usuario_elim_id = $usuario_id;
                $model->fecha_elim = null;
                $model->usuario_act_id = $usuario_id;
                $model->fecha_act = null;
                $model->usuario_act_id = null;
                $model->usuario_elim_id = null;
                $nombre = $model->nombre;
                
                 SubSeccionPrueba::model()->updateAll(array('estatus' => 'A'), array('condition' => 'seccion_prueba_id=' . $id_decod));
                 $subSecciones= SubSeccionPrueba::model()->findAll(array('select'=>'id','condition'=>'seccion_prueba_id='. $id_decod));
                 foreach($subSecciones as $actualizar)
                 {
                     Pregunta::model()->updateAll(array('estatus' => 'A'), array('condition' => 'sub_seccion_prueba_id=' . $actualizar['id']));
                 }
                 
                if ($model->save()) {
                    $this->registerLog('ACTIVAR', 'prueba.SeccionPrueba.ActivarSeccionPrueba', 'EXITOSO', 'Permite activar un registro de la Sección de Prueba');
                    $mensaje_exitoso = 'Activación exitosa de la Sección de Prueba:' . ' ' . $nombre;
                    echo json_encode(array('statusCode' => 'success', 'mensaje' => $mensaje_exitoso));
                } else { // error que no guardo
                    $this->registerLog('ACTIVAR', 'prueba.SeccionPrueba.ActivarSeccionPrueba', 'FALLIDO', 'Permite activar un registro de la Sección de Prueba');
                    $mensaje.= 'No se pudo activar el registro de la Sección de Prueba: ' . $nombre . ', Por favor intente nuevamente <br>';
                    echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
                }
            } else {
                $mensaje = 'Por favor seleccione un registro de la Sección de Prueba para activar';
                echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
            }
        }
    }
    
    
    
    public function actionObtenerDatos()
    {
      $model = new SeccionPrueba(); 
        if(isset($_REQUEST))
        {
           $id= $_REQUEST['id'];
           if(!empty($id))
           {
           // CON ESTO MUESTRO EN PANTALLA LA DURACION DE LA SECCION ESCOGIDA.
           $totalSubSecciones= SubSeccionPrueba::model()->getTotalSubSecciones($id);
           $totalSubSecciones= $totalSubSecciones[0]['count'];
           $mensaje="HOLA MUNDO";
           echo json_encode(array('statusCode' => 'success','mensaje'=>$mensaje,'totalSubSecciones'=>$totalSubSecciones));
           }else
           {
            echo json_encode(array('statusCode' => 'error'));
           }
        }   
    }
    
    public function actionVerificarEstatus()
    {
        $id = $this->getRequest('id');
        $id_decod = base64_decode($id);
        if (!is_numeric($id_decod)) {
            $mensaje = 'No se ha encontrado el registro de la Sección de Prueba que ha solicitado para activar. Recargue la página e intentelo de nuevo. </br>';
            echo json_encode(array('statusCode' => 'error', 'mensaje' => $mensaje));
        } else {
          $verificarEstatus=SeccionPrueba::model()->getVerificarEstatus($id_decod);
          $verificarEstatus=$verificarEstatus[0]['count'];
          
          if($verificarEstatus==1)
          {
          $mensaje = "Estimado Usuario: la Sección Seleccionada ya ha sido Utilizada para aplicar pruebas, no puede editar este Registro";
          echo json_encode(array('statusCode'=>'success','mensaje'=>$verificarEstatus));
          Yii::app()->end();
          
          }else
          {
              $ruta=Yii::app()->baseUrl;
             echo json_encode(array('statusCode' => 'error','mensaje'=>$ruta));
             Yii::app()->end();
          }
          
        }
        
    }

     /**
     * Encontrado en Internet por Nelson Gonzalez
     * Funcion de filtrado de entradas XSS en formularios
     *
     *   '@<script[^>]*?>.*?</script>@si',   // Elimina javascript
     *   '@<[\/\!]*?[^<>]*?>@si',            // Elimina las etiquetas HTML
     *   '@<style[^>]*?>.*?</style>@siU',    // Elimina las etiquetas de estilo
     *   '@<![\s\S]*?--[ \t\n\r]*>@'         // Elimina los comentarios multi-línea
     */
   function cleanInput($input) {

        $search = array(
            '@<script[^>]*?>.*?</script>@si',
            '@<style[^>]*?>.*?</style>@siU',
            '@<![\s\S]*?--[ \t\n\r]*>@'
        );
        $output = preg_replace($search, '', $input);
        return $output;
    }
    
    public function actionVerificarEstatusPueba()
    {
        if($_POST)
        {
            $prueba_id=$_POST['prueba_id'];
            $Prueba = Prueba::model()->find('id= :Id',array(':Id'=>$prueba_id));
            $Prueba = $Prueba['estatus'];
            echo json_encode(array('estatusSeccion'=>$Prueba));
        }
    }

}
