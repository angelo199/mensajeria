<?php

/**
 * This is the model class for table "gprueba.seccion_prueba".
 *
 * The followings are the available columns in table 'gprueba.seccion_prueba':
 * @property string $id
 * @property string $nombre
 * @property string $duracion_seccion
 * @property integer $cantidad_respuesta
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property integer $usuario_elim_id
 * @property integer $nivel_cargo_id
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $prueba_id
 * @property string $tipo_pregunta
 * @property integer $cantidad_sub_seccion
 * @property text $instruccion_seccion
 *
 * The followings are the available model relations:
 * @property Prueba $prueba
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioElim
 * @property UsergroupsUser $usuarioIni
 * @property Pregunta[] $preguntas
 * @property SubSeccionPrueba[] $subSeccionPruebas
 * @property NivelCargo $nivelCargo
 */
class DatosPrueba extends SeccionPrueba {
    
    public  $cacheIndexPrueba = 'SP:PRUEBA:{idPrueba}';// indice de la cache de la prueba que esta presentando el talento humano

    public function getDatosCompletosPrueba($idPrueba) {        
        $result=null;
        if (is_numeric($idPrueba)){
            $cacheIndex= $this->getCacheIndexFormatedPrueba($idPrueba);
            $prueba= Yii::app()->cache->get($cacheIndex);           
            if (!$prueba){ 
                $estatus = 'A';
                $sql = "SELECT s.id AS seccion_id,
                    s.prueba_id AS periodo_proceso_id,
                    s.nombre AS nombre_prueba,
                    s.duracion_seccion AS duracion,
                    s.cantidad_respuesta AS cantidad_opciones,
                    s.tipo_pregunta,
                    s.instruccion_seccion,
                    COALESCE(p.id, null) AS pregunta_id,
                    COALESCE(p.nombre, '') AS nombre_pregunta,
                    p.ponderacion,
                    COALESCE(r.id, null) AS opcion_id,
                    COALESCE(r.nombre, '') AS nombre_opcion,
                    COALESCE(r.resp_correcta, null) AS respuesta_correcta
                    FROM gprueba.seccion_prueba s
                    INNER JOIN gprueba.pregunta p ON (p.seccion_prueba_id = s.id)
                    INNER JOIN gprueba.respuesta r ON (r.pregunta_id = p.id)
                    WHERE s.id =:prueba_id
                    AND s.estatus=:estatus
                    AND p.estatus=:estatus
                    AND r.estatus=:estatus
                    ORDER BY(p.id, r.resp_correcta);
                    ";
                $consulta = Yii::app()->db->createCommand($sql);
                $consulta->bindParam(":estatus", $estatus, PDO::PARAM_STR);
                $consulta->bindParam(":prueba_id", $idPrueba, PDO::PARAM_INT);
                $result = $consulta->queryAll();
               
                $prueba = null;

                if (count($result) > 0) {
                    $data = $result[0];
                    // Asignamos los datos generales de la prueba
                    $prueba['seccion_id'] = $data['seccion_id'];
                    $prueba['periodo_proceso_id'] = $data['periodo_proceso_id'];
                    $prueba['nombre_prueba'] = $data['nombre_prueba'];
                    $prueba['duracion'] = $data['duracion'];
                    $prueba['cantidad_opciones'] = $data['cantidad_opciones'];
                    $prueba['tipo_pregunta'] = $data['tipo_pregunta'];
                    $prueba['instruccion_seccion'] = $data['instruccion_seccion'];
                    $prueba['preguntas'] = array();
                    $indexPreguntas = 0;
                    $indexOpciones = 0;
                    // Primera pregunta
                    $prueba['preguntas'][$indexPreguntas]['pregunta_id'] = $data['pregunta_id'];
                    $prueba['preguntas'][$indexPreguntas]['nombre_pregunta'] = $data['nombre_pregunta'];
                    $prueba['preguntas'][$indexPreguntas]['ponderacion'] = $data['ponderacion'];
                    // Recorremos todas las preguntas con sus opciones
                    foreach ($result as $data) {
                        // Registramos los datos de cada pregunta una sóla vez
                        if ($data['pregunta_id'] != $prueba['preguntas'][$indexPreguntas]['pregunta_id']) {
                            $indexPreguntas++;
                            $indexOpciones = 0; // Se reinicio al índice de las opciones
                            $prueba['preguntas'][$indexPreguntas]['pregunta_id'] = $data['pregunta_id'];
                            $prueba['preguntas'][$indexPreguntas]['nombre_pregunta'] = $data['nombre_pregunta'];
                            $prueba['preguntas'][$indexPreguntas]['ponderacion'] = $data['ponderacion'];
                            if ($data['respuesta_correcta'] == '1') {
                                $prueba['preguntas'][$indexPreguntas]['respuesta_correcta_id'] = $data['respuesta_id'];
                            }
                            // Dentro de cada pregunta colocamos cada una de sus opciones
                            $prueba['preguntas'][$indexPreguntas]['opciones'][$indexOpciones]['opcion_id'] = $data['opcion_id'];
                            $prueba['preguntas'][$indexPreguntas]['opciones'][$indexOpciones]['nombre_opcion'] = $data['nombre_opcion'];
                            $prueba['preguntas'][$indexPreguntas]['opciones'][$indexOpciones]['respuesta_correcta'] = $data['respuesta_correcta'];
                        } else { // Si la pregunta anterior es igual a la actual se registra sólo la opción
                            if ($data['respuesta_correcta'] == '1') {
                                $prueba['preguntas'][$indexPreguntas]['respuesta_correcta_id'] = $data['opcion_id'];
                            }
                            // Dentro de cada pregunta colocamos cada una de sus opciones
                            $prueba['preguntas'][$indexPreguntas]['opciones'][$indexOpciones]['opcion_id'] = $data['opcion_id'];
                            $prueba['preguntas'][$indexPreguntas]['opciones'][$indexOpciones]['nombre_opcion'] = $data['nombre_opcion'];
                            $prueba['preguntas'][$indexPreguntas]['opciones'][$indexOpciones]['respuesta_correcta'] = $data['respuesta_correcta'];
                        }
                        $indexOpciones++; // Como en realidad estamos recorriendo es opciones de preguntas entonces se debe incrementar siempre el índice de las opciones
                    }
                    Yii::app()->cache->set($cacheIndex,$prueba,Helper::$SEGUNDOS_UN_MES );         
                }
            }
             // Se hacen las preguntas aleatorias
            shuffle($prueba['preguntas']);  
            foreach ($prueba['preguntas'] as $preguntaIndex => $pregunta) {
                // Se hacen las opciones aleatorias
                shuffle($pregunta['opciones']);
                $prueba['preguntas'][$preguntaIndex]['opciones'] = $pregunta['opciones'];
            }
        }
        else {
            throw new CHttpException(404, 'ERR02AP: El recurso requerido no ha sido encontrado.');
        }
          if ($prueba === null) {
            throw new CHttpException(404, 'ERR03AP: El recurso requerido no ha sido encontrado.');
        }
       

        // Se retorna toda la información de la prueba.
        return $prueba;
    }
    
  /**
     * Este método genera un índice para ser utilizada como clave de un valor a almacenar en caché de la prueba que va ser mostrada a un talento humano.
     * 
     * @param int IdPrueba  id de la prueba que va ser mostrada al talento humano
     * @return string
     */
    public function getCacheIndexFormatedPrueba($IdPrueba){
        $output = strtr($this->cacheIndexPrueba, array('{IdPrueba}'=>$IdPrueba));        
        return $output;
    }
    
    /**
     * Este método retorana los datos comppletos de una prueba dado su id 
     * 
     * @param int $idPrueba   
     * @return array
     * @throws CHttpException
     */
    public function getPrueba($idPrueba){
        $result=null;
        if (is_numeric($idPrueba)){
            $cacheIndex= $this->getCacheIndexFormatedPrueba($idPrueba);
            $result= Yii::app()->cache->get($cacheIndex);           
            if (!$result){ 
                $result= $this->getDatosCompletosPrueba($idPrueba);               
                if(count($result)>0){
                    Yii::app()->cache->set($cacheIndex,$result,Helper::$SEGUNDOS_UN_MES );
                }
            }
        }
        else {
            throw new CHttpException(404, 'ERR02AP: El recurso requerido no ha sido encontrado.');
        }
        if ($result === null) {
            throw new CHttpException(404, 'ERR03AP: El recurso requerido no ha sido encontrado.');
        }
        ld($result);die();
        return $result;
        
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SeccionPrueba the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
