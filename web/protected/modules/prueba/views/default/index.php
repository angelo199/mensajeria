<?php
/* @var $this DefaultController */
$this->pageTitle = 'Gestión de Exámenes de Postulación';

$this->breadcrumbs = array(
    'Gestión de Exámenes de Postulación'
);
?>


<div  class="col-xs-12">
    <p>
    <div class="linkCatalogo" onclick="">
        <span class="titulo">Tipos de Procesos</span>
        <a href="/administracion/tipoPeriodoProceso/" title="Tipos de Proceso de Ingreso y/o Clasificación" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Tipo_de_Pruebas.png' ?>');">
        </a>
    </div>
    
    <div class="linkCatalogo" onclick="">
        <span class="titulo">Proceso Aperturados</span>
        <a href="/administracion/aperturaPeriodo" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Sub_seccion.png' ?>');">
        </a>
    </div>

    <div class="linkCatalogo" onclick="">
        <span class="titulo">Exámenes de Postulación</span>
        <a href="/prueba/seccion" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../public/images/iconoCatalogo/Seccion.png' ?>');">
        </a>
    </div>

</p>
<div class="col-md-12"><br><br> </div>

</div>




<?php
echo CHtml::cssFile('/public/css/iconosCatalogo.css');

