<div style="min-height: 500px;" id="main-container" class="row-fluid">
    <!-- <div class="col-xs-12">-->

    <div class="col-xs-12">
        <div class="row-fluid">

            <div class="tabbable">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#datosGenerales">Datos Generales</a></li>
                    <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
                </ul>

                <div class="tab-content">
                    <div id="datosGenerales" class="tab-pane active">
                        <div class="form">

                            <form method="post" action="/registroPrueba/pregunta/registro" id="pregunta-form" data-form-type="registro">
                                <div id="div-result">
                                    <div id="mensajeAlerta" class="infoDialogBox"><p>Consulta de Datos de la Sección <b><?php echo $model->seccionPrueba->nombre; ?></b></p></div>
                                </div>

                                <div id="div-datos-generales">

                                    <div class="widget-box">

                                        <div class="widget-header">
                                            <h5>Datos Generales</h5>

                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-body-inner">
                                                <div class="widget-main">
                                                    <div class="widget-main form">
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                <label>Tipo de Prueba</label>
                                                                <select class="span-12" disabled="disabled">
                                                                    <option>
                                                                        <?php
                                                                        echo $model->seccionPrueba->nombre;
                                                                        ?>
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-md-4">
                                                                <label>Tipo de Sección</label>
                                                                <select class="span-7" disabled="disabled">
                                                                    <option>
                                                                        <?php
                                                                        if(isset($model->sub_seccion_prueba_id)){
                                                                            $tipoSeccion =  'Sub Sección';
                                                                            $duracion = $model->subSeccionPrueba->duracion_seccion;
                                                                            $cantidad_respuesta = $model->subSeccionPrueba->cantidad_respuesta;
                                                                            $tipo_pregunta = $model->subSeccionPrueba->tipo_pregunta;
                                                                            $instruccion = $model->subSeccionPrueba->instruccion_sub_seccion;
                                                                            echo $tipoSeccion;
                                                                            $cantidadPreguntas = Pregunta::model()->findAll(array('condition' => 'sub_seccion_prueba_id = ' . $model->sub_seccion_prueba_id));
                                                                            $cantidadPreguntas = count($cantidadPreguntas);
                                                                        }
                                                                        else if(isset($model->seccion_prueba_id)){
                                                                            $tipoSeccion = 'Sección';
                                                                            $duracion = $model->seccionPrueba->duracion_seccion;
                                                                            $cantidad_respuesta = $model->seccionPrueba->cantidad_respuesta;
                                                                            $tipo_pregunta = $model->seccionPrueba->tipo_pregunta;
                                                                            $instruccion = $model->seccionPrueba->instruccion_seccion;
                                                                            echo $tipoSeccion;
                                                                            $cantidadPreguntas = Pregunta::model()->findAll(array('condition' => 'seccion_prueba_id = ' . $model->seccion_prueba_id));
                                                                            $cantidadPreguntas = count($cantidadPreguntas);
                                                                        }
                                                                        ?>
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-md-4">
                                                                <label>Nombre de la Sección</label>
                                                                <select class="span-7" disabled="disabled">
                                                                    <option>
                                                                        <?php
                                                                        if(isset($model->sub_seccion_prueba_id)){
                                                                            $nombreSeccion = $model->subSeccionPrueba->nombre;
                                                                            echo $nombreSeccion;
                                                                        }
                                                                        else if(isset($model->seccion_prueba_id)){
                                                                            $nombreSeccion = $model->seccionPrueba->nombre;
                                                                            echo $nombreSeccion;
                                                                        }
                                                                        ?>
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            
                                                            <?php
                                                                if($tipo_pregunta == 'T'){
                                                                    $tipo_preguntaText = 'Selección Simple';
                                                                }
                                                                else if($tipo_pregunta == 'I'){
                                                                    $tipo_preguntaText = 'Selección Simple (Imagen)';
                                                                }
                                                                else if($tipo_pregunta == 'D'){
                                                                    $tipo_preguntaText = 'Domino';
                                                                }
                                                                else{
                                                                    $tipo_preguntaText = 'No posee';
                                                                }
                                                            ?>

                                                            <div class="col-md-12">
                                                                <div class="" id="detalles">
                                                                    <div class="infoDialogBox" id="detalles">
                                                                        <p>
                                                                            Datos de la <?php echo $tipoSeccion; ?><br><br>
                                                                            <b>Nombre:</b> <?php echo $nombreSeccion; ?><br>

                                                                            <b>Duración:</b> <?php echo $duracion; ?><br>
                                                                            <b>Cantidad de Respuestas:</b> <?php echo $cantidad_respuesta; ?><br>
                                                                            <label class="hide" id="cantidad_respuesta">3</label>
                                                                            <b>Tipo de pregunta:</b> <?php echo $tipo_preguntaText; ?><br>
                                                                            <b>Instrucción de la Sección:</b> <?php echo $instruccion; ?><br>
                                                                            <b>Cantidad de preguntas formuladas: </b><label id='cantidadPregunta'><?php echo $cantidadPreguntas; ?></label>
                                                                        </p>
                                                                    </div>
                                                                    
                                                                    <div id="contenedor"> 
                                                                        <div class="">
                                                                            <div class="widget-box">
                                                                                <div class="widget-header">
                                                                                    <h5>Pregunta de <?php echo $tipo_preguntaText; ?></h5>
                                                                                    <div class="widget-toolbar">
                                                                                        <a href="#" data-action="collapse">
                                                                                            <i class="icon-chevron-up"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="widget-body">
                                                                                    <div class="widget-body-inner">
                                                                                        <div class="widget-main">
                                                                                            <div class="widget-main form">
                                                                                                <div id="mensajeForm" class="infoDialogBox"><p>Datos de la Pregunta <b><?php echo $tipo_preguntaText; ?></b></p></div>
                                                                                                <?php
                                                                                                if($model->estatus == 'I'){
                                                                                                    ?>
                                                                                                    <div id="mensajeForm" class="alertDialogBox"><p>Esta pregunta esta inactiva.</b></p></div>
                                                                                                <?php
                                                                                                }
                                                                                                ?>
                                                                                                <div class="row">
                                                                                                    <?php if($tipo_pregunta == 'T'){
                                                                                                        ?>
                                                                                                        <div id="step2" class="row">
                                                                                                            <div id="contenidoPregunta">
                                                                                                                <div class="col-md-8">
                                                                                                                    <label>Indica La Pregunta</label>
                                                                                                                    <input type="text" class="span-12" value="<?php echo $model->nombre; ?>" readonly="readonly">
                                                                                                                </div>

                                                                                                                <div class="col-md-4">
                                                                                                                    <label>Ponderación</label>
                                                                                                                    <select class="span-12" disabled="disabled">
                                                                                                                        <option><?php echo $model->ponderacion; ?></option>
                                                                                                                    </select>
                                                                                                                </div>

                                                                                                                <?php
                                                                                                                $respuesta = Respuesta::model()->findAll(array('condition' => 'pregunta_id=' . $model->id));
                                                                                                                foreach($respuesta AS $r){
                                                                                                                    $check = '';
                                                                                                                    if($r['resp_correcta'] == 1){
                                                                                                                        $check = 'checked="checked"';
                                                                                                                    }
                                                                                                                ?>
                                                                                                                    <div class="col-md-12">
                                                                                                                        <div align="center" class="col-md-1">
                                                                                                                            <label>
                                                                                                                                <input type="radio" <?php echo $check; ?> disabled="disabled">
                                                                                                                            </label>
                                                                                                                        </div>
                                                                                                                        <div class="col-md-11">
                                                                                                                            <input type="text" class="span-12" value="<?php echo $r['nombre']; ?>" readonly="readonly">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                <?php
                                                                                                                }
                                                                                                                ?>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                    }
                                                                                                    
                                                                                                    if($tipo_pregunta == 'I'){
                                                                                                       $respuesta = Respuesta::model()->findAll(array('condition' => 'pregunta_id=' . $model->id));
                                                                                                        ?>
                                                                                                        <div id="step2" class="row">
                                                                                                            <div id="contenidoPregunta">
                                                                                                                <div class="col-md-4">
                                                                                                                    <img width="250px" src="<?php echo Yii::app()->baseUrl . '/public/registroPrueba/Imagen/' . $model->nombre; ?>">
                                                                                                                </div>

                                                                                                                <div class="col-md-4 pull-right">
                                                                                                                    <label>Ponderación</label>
                                                                                                                    <select class="span-12" disabled="disabled">
                                                                                                                        <option><?php echo $model->ponderacion; ?></option>
                                                                                                                    </select><br><br><br>
                                                                                                                    <label>Respuesta</label>
                                                                                                                    <select class="span-12" disabled="disabled">
                                                                                                                        <option><?php echo $respuesta[0]['nombre']; ?></option>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                    }
                                                                                                    
                                                                                                    if($tipo_pregunta == 'D'){
                                                                                                       $respuesta = Respuesta::model()->findAll(array('condition' => 'pregunta_id=' . $model->id));
                                                                                                        ?>
                                                                                                        <div id="step2" class="row">
                                                                                                            <div id="contenidoPregunta">
                                                                                                                <div class="col-md-4">
                                                                                                                    <img width="250px" src="<?php echo Yii::app()->baseUrl . '/public/registroPrueba/Domino/' . $model->nombre; ?>">
                                                                                                                </div>

                                                                                                                <div class="col-md-4 pull-right">
                                                                                                                    <label>Ponderación</label>
                                                                                                                    <select class="span-12" disabled="disabled">
                                                                                                                        <option><?php echo $model->ponderacion; ?></option>
                                                                                                                    </select><br><br><br>
                                                                                                                    <label>Respuesta (Imagen de arriba)</label>
                                                                                                                    <select class="span-12" disabled="disabled">
                                                                                                                        <option><?php echo $respuesta[0]['nombre']; ?></option>
                                                                                                                    </select><br><br><br>
                                                                                                                    <label>Respuesta (Imagen de abajo)</label>
                                                                                                                    <select class="span-12" disabled="disabled">
                                                                                                                        <option><?php echo $respuesta[1]['nombre']; ?></option>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                    }
                                                                                                ?>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <br>
                                                                    <div class="row">

                                                                        <!--    <div class="col-md-12" align="rigth">
                                                                                <a id="agregarCampo" class="btn btn-info" href="#">Agregar Pregunta</a>
                                                                            </div>-->
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                    </div>
                                </div>
                            </form>
                        </div><!-- form -->
                    </div>

                    <div id="otrosDatos" class="tab-pane">
                        <div class="alertDialogBox">
                            <p>
                                Próximamente: Esta área se encuentra en Desarrollo.
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="hide" id="resultDialog"></div>

        </div>
    </div>
    <!-- </div> -->
    <!-- /.col -->
</div>
