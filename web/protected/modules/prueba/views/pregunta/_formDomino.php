<?php
echo $info;
?>
<script>
    $("#botonGuardarPregunta").addClass("hide");
    $("#botonGuardarPreguntaSimple").addClass("hide");
    $("#botonGuardarPreguntaImagen").addClass("hide");
    $("#botonGuardarPreguntaDomino").removeClass("hide");
    
    $("#mensajeAlerta").html('<p>Todos los campos con <span class="required">*</span>son requeridos.</p>');
    $("#mensajeAlerta").addClass('infoDialogBox');
    $("#mensajeAlerta").removeClass('errorDialogBox');
    $("#mensajeForm").removeClass('successDialogBox');
    
    $("#botonGuardarPreguntaDomino").unbind('click');
    $("#botonGuardarPreguntaDomino").bind('click', function(){
        var tipoPrueba = $("#Pregunta_id").val();
        var pregunta_seccion_prueba_id = $("#Pregunta_seccion_prueba_id").val();
        var detallesSeccion = $("#detallesSeccion").val();
        var pregunta = $("#pregunta").val();
        var pregunta_ponderacion = $("#Pregunta_ponderacion").val();
        var respuestaA = $("#respuestaA").val();
        var respuestaB = $("#respuestaB").val();
        var tipo_pregunta = $("#tipo_pregunta").val();
        
        if(tipoPrueba == '' || pregunta_seccion_prueba_id == '' || detallesSeccion == ''){
            $("#mensajeAlerta").html('<p>Todos los campos con <span class="required">*</span>son requeridos.</p>');
            $("#mensajeAlerta").addClass('errorDialogBox');
            $("#mensajeAlerta").removeClass('infoDialogBox');
            $("#mensajeForm").removeClass('successDialogBox');
        }else{

            if(pregunta == ''){
                $("#mensajeForm").html('<p>Seleccione una imagen.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else if(pregunta_ponderacion == ''){
                $("#mensajeForm").html('<p>Seleccione la ponderacion para esta prueba.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else if(respuestaA == ''){
                $("#mensajeForm").html('<p>Seleccione la respuesta (Imagen de arriba).</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else if(respuestaB == ''){
                $("#mensajeForm").html('<p>Seleccione la respuesta (Imagen de abajo).</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else{
                $("#mensajeForm").html('<p>Formule la pregunta, seguido de marcar la respuesta correcta.</p>');
                $("#mensajeForm").addClass('infoDialogBox');
                $("#mensajeForm").removeClass('errorDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
                
                $("#loading").removeClass('hide');
                $("#contenidoPregunta").addClass('hide');
                
                $.ajax({
                    url: "/prueba/pregunta/registrarPreguntaDomino",
                    data: 'tipoPrueba=' + tipoPrueba +
                          '&pregunta_seccion_prueba_id=' + pregunta_seccion_prueba_id +
                          '&detallesSeccion=' + detallesSeccion +
                          '&pregunta=' + pregunta +
                          '&pregunta_ponderacion=' + pregunta_ponderacion +
                          '&respuestaA=' + respuestaA +
                          '&respuestaB=' + respuestaB +
                          '&tipo_pregunta=' + tipo_pregunta,
                    type: 'POST',
                    success: function (data) {
                        $("#mensajeForm").html('<p>Pregunta agregada con éxito.</p>');
                        $("#mensajeForm").addClass('successDialogBox');
                        $("#mensajeForm").removeClass('infoDialogBox');
                        $("#loading").addClass('hide');
                        $("#contenidoPregunta").removeClass('hide');
                        $("#pregunta").val('');
                        $("#Pregunta_ponderacion").val('');
                        $("#respuestaA").val('');
                        $("#respuestaB").val('');
                        $("#dataImage").attr('data-title', 'Seleccione una imagen...');
                        $("#preguntaImagen").attr('title', 'No se ha seleccionado ningún archivo.');
                        $("#cantidadPregunta").text(parseInt($("#cantidadPregunta").text()) + 1);
                    }
                });
            }
        }
        
//        console.log('RESPUESTA TEXT: ' + respuestaVacia);
//        console.log('RESPUESTAS TEXT CHECK: ' + respuestaCheckVacia);
        
    });
    
    $("#pregunta").bind('change', function(){
        
    });
    
    
    $(document).ready(function() {

            $('#preguntaImagen').fileupload({
                url: '/prueba/pregunta/cargarImagen?t=d',
                acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
                dataType: 'json',
                singleFileUploads: true,
                autoUpload: true,
//                beforeSend : function (){
//                    formatoImagen();
//                },
                done: function (e, data) {
                    if (data.result.files[0].url) {
                        
                        var imagen = data.result.files[0].name;
                        var elemento = imagen.split('.');
                        console.log(elemento);
                        if(elemento[1] != 'undefined' || elemento[1] != 'jpg' || elemento[1] != 'jpeg' || elemento[1] != 'png'){
                            $("#mensajeForm").html('<p>Seleccione una imagen con el formato .jpg, .jpeg o .png.</p>');
                            $("#mensajeForm").removeClass('infoDialogBox');
                            $("#mensajeForm").addClass('errorDialogBox');
                            $("#mensajeForm").removeClass('successDialogBox');
                            $("#pregunta").val('');
                            $("#dataImage").attr('data-title', 'Seleccione una imagen...');
                            $("#preguntaImagen").attr('title', 'No se ha seleccionado ningún archivo.');
                        }
                        if(elemento[1] == 'jpg' || elemento[1] == 'jpeg' || elemento[1] == 'png'){
                            $("#mensajeForm").html('<p>Formule la pregunta, seguido de marcar la respuesta correcta.</p>');
                            $("#mensajeForm").addClass('infoDialogBox');
                            $("#mensajeForm").removeClass('errorDialogBox');
                            $("#mensajeForm").removeClass('successDialogBox');
    //                        $('#pregunta').val(data.result.files[0].url);
                            console.log(data.result.files[0].url);
                            console.log(data.result.files[0].name);
                            $("#dataImage").attr('data-title', data.result.files[0].name);
                            $("#preguntaImagen").attr('title', 'Imagen seleccionada: ' + data.result.files[0].name);
                            $("#pregunta").val(data.result.files[0].name);
    //                        $("#imagenView").html("<img src='" + data.result.files[0].url + "' width='200px'>");
    //                        $('#pregunta').text(data.result.files[0].name);
                        }
                    }
                    else if(data.result.files[0].error != ''){
                        $("#mensajeForm").html('<p>Ocurrio un error, verifique con mucho cuidado la imagen e intente nuevamente.</p>');
                        $("#mensajeForm").removeClass('infoDialogBox');
                        $("#mensajeForm").addClass('errorDialogBox');
                        $("#mensajeForm").removeClass('successDialogBox');
                        $("#pregunta").val('');
                        $("#dataImage").attr('data-title', 'Seleccione una imagen...');
                        $("#preguntaImagen").attr('title', 'No se ha seleccionado ningún archivo.');
                        console.log('Ocurrio un error, verifique con mucho cuidado la imagen e intente nuevamente.');
                    }
                },
                process: [
                {
                    action: 'load',
                    fileTypes: /(\.|\/)(jpe?g|png)$/i,
                    maxFileSize: 5000000 // 5MB
                },
                {
                    action: 'resize',
                    maxWidth: 1440,
                    maxHeight: 900
                },
                {
                    action: 'save'
                }
            ],
            })
        });




//    $(document).ready(function() {
////        var nombre = $("#pregunta").val();
//        $("#pregunta").fileupload({
//            url: '/prueba/pregunta/cargarImagen',
//            acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
//            maxFileSize: 50000000,
//            singleFileUploads: true,
//            autoUpload: true,
//            process: [
//                {
//                    action: 'load',
//                    fileTypes: /(\.|\/)(jpe?g|png)$/i,
//                    maxFileSize: 5000000 // 5MB
//                },
//                {
//                    action: 'resize',
//                    maxWidth: 1440,
//                    maxHeight: 900
//                },
//                {
//                    action: 'save'
//                }
//            ],
//            error: function(jqXHR, textStatus, errorThrown) {
//                console.log("Se ha producido un error en la carga del archivo.");//ALEXIS ERROR DE CARGA
//            }
//
//        });
//    });

</script>
<div id="contenedor"> 
    <div class="">
        <div class="widget-box">
            <div class="widget-header">
                <h5>Pregunta de Selección Simple</h5>
                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-body-inner">
                    <div class="widget-main">
                        <div class="widget-main form">
                            <div class="infoDialogBox" id="mensajeForm"><p>Formule la pregunta, seguido de marcar la respuesta correcta.</p></div>
                            <div class="row">
                                <div class="row" id="step2">
                                    
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <img src="<?php echo Yii::app()->baseUrl . '/public/images/ajax-loader.gif'; ?>" width="75%" class="hide" id="loading">
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="contenidoPregunta">
                                    <div class="col-md-8">
                                        <label>Seleccione la imagen <span class="required">*</span></label>
                                        <div class="ace-file-input">
                                            <input type="hidden" id="pregunta">
                                            <input type="file" id="preguntaImagen">
                                            <label data-title="Explorar" class="file-label">
                                                <span data-title="Seleccione una imagen..." class="file-name" id="dataImage">
                                                    <i class="icon-upload-alt"></i></span>
                                            </label>
                                            <a href="#" class="remove"><i class="icon-remove"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Ponderación <span class="required">*</span></label>
                                        <select id="Pregunta_ponderacion" required="required" class="span-12">
                                            <option value="">Seleccione</option>
                                            <?php
                                            $contPonderacion = 1;
                                            while($contPonderacion != $ponderacion + 1){
                                                ?>
                                                <option value="<?php echo $contPonderacion; ?>"><?php echo $contPonderacion; ?></option>
                                                <?php
                                                $contPonderacion++;
                                            }
                                            ?>
                                        </select>
                                        <!--<input type="text" id="Pregunta_ponderacion" name="Pregunta[ponderacion]" required="required" class="span-12" prompt="Seleccione">-->
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Seleccione la Respuesta (Imagen de arriba) <span class="required">*</span></label>
                                        <select class="col-sm-12" id="respuestaA">
                                            <option value="">Seleccione</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                        <input type="hidden" id="tipo_pregunta" value="<?php echo $tipo_pregunta; ?>" required="required">
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Seleccione la Respuesta (Imagen de abajo) <span class="required">*</span></label>
                                        <select class="col-sm-12" id="respuestaB">
                                            <option value="">Seleccione</option>
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                        <input type="hidden" id="tipo_pregunta" value="<?php echo $tipo_pregunta; ?>" required="required">
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<br>
<div class="row">

<!--    <div class="col-md-12" align="rigth">
        <a id="agregarCampo" class="btn btn-info" href="#">Agregar Pregunta</a>
    </div>-->
</div>

<!--    
    
<script src="/public/js/jquery.upload/js/vendor/jquery.ui.widget.js"></script>
 The Load Image plugin is included for the preview images and image resizing functionality 
<script src="/public/js/jquery.upload/js/load-image.min.js"></script>
 The Canvas to Blob plugin is included for image resizing functionality 
<script src="/public/js/jquery.upload/js/canvas-to-blob.min.js"></script>
 Bootstrap JS is not required, but included for the responsive demo navigation 
 blueimp Gallery script 
<script src="/public/js/jquery.upload/js/jquery.blueimp-gallery.min.js"></script>
 The Iframe Transport is required for browsers without support for XHR file uploads 
<script src="/public/js/jquery.upload/js/jquery.iframe-transport.js"></script>
 The basic File Upload plugin 
<script src="/public/js/jquery.upload/js/jquery.fileupload.js"></script>
 The File Upload processing plugin 
<script src="/public/js/jquery.upload/js/jquery.fileupload-process.js"></script>
 The File Upload image preview & resize plugin 
<script src="/public/js/jquery.upload/js/jquery.fileupload-image.js"></script>
 The File Upload validation plugin 
<script src="/public/js/jquery.upload/js/jquery.fileupload-validate.js"></script>
 The File Upload user interface plugin 
<script src="/public/js/jquery.upload/js/jquery.fileupload-ui.js"></script>-->

<script>
//    $('#pregunta').ace_file_input({
//            no_file:'No File ...',
//            btn_choose:'Choose',
//            btn_change:'Change',
//            droppable:false,
//            onchange:null,
//            thumbnail:false //| true | large
//            //whitelist:'gif|png|jpg|jpeg'
//            //blacklist:'exe|php'
//            //onchange:''
//            //
//    });
</script>
