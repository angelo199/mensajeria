<?php
/* @var $this PreguntaController */
/* @var $seccion array */
?>
<!-- Preguntas Widget -->
<div class="widget-box pregunta-widget">
    <div class="widget-header header-color-blue2">
        <h5 class="title white">Sección: <b><?php echo $seccion['nombre'] ?></b></h5>
        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-body-inner">
            <div class="widget-main">
                <div class="widget-main">
                    <div class="row">
                        <div class="col-md-12 form">
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="SeccionPrueba_nombre">Nombre de la sección <span class="required">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text"
                                           id="Seccion_nombre"
                                           name="Seccion[nombre]"
                                           value="<?php echo $seccion['nombre'] ?>"
                                           readonly>
                                </div>
                            </div>
                            <div class="col-xs-12">&nbsp;</div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="SeccionPrueba_duracion">Tiempo de duración de la sección <span class="required">*</span></label>
                                <div class="col-sm-8">
                                    <input type="text"
                                           class="col-xs-1 col-sm-2"
                                           id="Seccion_duracion"
                                           name="Seccion[duracion_seccion]"
                                           value="<?php echo $seccion['duracion'] ?>"
                                           readonly>
                                </div>


                                  <input type="hidden"
                                        id = "Seccion_cantidad_sub_seccion"  
                                        name="Seccion[cantidad_sub_seccion]"
                                        value="<?php echo $seccion['cantidad_sub_seccion']?>">       

                                   <input type="hidden"
                                        id = "Seccion_instruccion_seccion"  
                                        name="Seccion[instruccion_seccion]"
                                        value="<?php echo $seccion['instruccion_seccion']?>">        
                            </div>


                            <div class="form-group">
                                <?php if ($seccion['tipo_respuestas'] == 'domino') : ?>
                                    <input type="hidden"
                                           id="Seccion_cantidad_respuesta"
                                           name="Seccion[cantidad_respuesta]"
                                           value="7" >
                                <?php elseif ($seccion['tipo_respuestas'] == 'simple') : ?>
                                    <input type="hidden"
                                           id="Seccion_cantidad_respuesta"
                                           name="Seccion[cantidad_respuesta]"
                                           value="<?php echo $seccion['cantidad_respuesta'] ?>" >
                                <?php endif; ?>
                                <input type="hidden"
                                       id="Seccion_tipo_respuestas"
                                       name="Seccion[tipo_respuestas]"
                                       value="<?php echo $seccion['tipo_respuestas'] ?>" >
                                <input type="hidden"
                                       id="Seccion_cantidad_sub_seccion"
                                       name="Seccion[cantidad_sub_seccion]"
                                       value="<?php echo $seccion['cantidad_sub_seccion'] ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="space-12"></div>
                    <?php if ($seccion['cantidad_sub_seccion'] > 0): ?>
                        <div class="row">
                            <?php for ($i = 0; $i < $seccion['cantidad_sub_seccion']; $i++) : ?>
                                <div class="widget-box" data-sub-seccion-id="<?php echo $i; ?>">
                                    <div class="widget-header widget-header-small header-color-green">
                                        <h6>
                                            <i class="icon-sort"></i>
                                            Sub-sección
                                        </h6>
                                        <div class="widget-toolbar">
                                            <a href="#" data-action="collapse">
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="row errorValidacionSubSeccion"></div>
                                            <div class="row">
                                                <div class="col-md-12 form">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label no-padding-right" for="SeccionPrueba_nombre">Nombre de la subsección <span class="required">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="text"
                                                                   class="col-xs-10 col-sm-6 subSeccionNombre"
                                                                   name="SubSeccion[<?php echo $i ?>][nombre]"
                                                                   autocomplete="off"
                                                                   >
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">&nbsp;</div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label no-padding-right" for="SeccionPrueba_duracion">Tiempo de duración de la sección <span class="required">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="text"
                                                                   class="col-xs-1 col-sm-2 subSeccionDuracion "
                                                                   name="SubSeccion[<?php echo $i ?>][duracion_seccion]"
                                                                   placeholder="HH:MM:SS"
                                                                   pattern="(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9]){2}"
                                                                   title="Formato: HH:MM:SS"
                                                                   autocomplete="off"
                                                                   >
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                         <br>
                                                   
                                                            <label class="col-sm-4 control-label no-padding-right" for="SeccionPrueba_instruccion_sub_seccion">Instrucciones <span class="required">*</span></label>
                                                            <div class="col-sm-8">
                                                                <textarea type="text"
                                                                       class="col-xs-10 col-sm-6"
                                                                       id="SubSeccion_instruccion_sub_seccion"
                                                                       name="SubSeccion[<?php echo $i ?>][instruccion_sub_seccion]"
                                                                       pattern="[0-9]"
                                                                       autocomplete="off"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12">&nbsp;</div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label no-padding-right">Tipo de respuestas <span class="required">*</span></label>
                                                        <select class="subSeccionTipoRespuestas"
                                                                name="SubSeccion[<?php echo $i ?>][tipo_respuestas]">
                                                            <option value="simple">Seleccion simple</option>
                                                            <option value="domino">Dominos</option>
                                                            <option value="imagen-simple">Seleccion simple (Imagen)</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12">&nbsp;</div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label no-padding-right" for="SeccionPrueba_cantidad_respuesta">Indica la cantidad de respuestas que va a contener cada pregunta <span class="required">*</span></label>
                                                        <div class="col-sm-8">
                                                            <input type="text"
                                                                   class="col-xs-10 col-sm-6 subSeccionCantidadRespuesta"
                                                                   name="SubSeccion[<?php echo $i ?>][cantidad_respuesta]"
                                                                   pattern="[0-9]"
                                                                   autocomplete="off"
                                                                   >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">&nbsp;</div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label no-padding-right labelPregunta" for="subSeccionPregunta">Indica la pregunta <span class="required">*</span></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control subSeccionPregunta"
                                                                   name="subSeccionPregunta"
                                                                   autocomplete="off">
                                                            <input type="hidden"
                                                                   class="subSeccionPreguntaImagen"
                                                                   name="files"
                                                                   disabled="disabled">
                                                        </div>
                                                        <div class="col-xs-12">&nbsp;</div>
                                                        <label class="col-sm-4 control-label no-padding-right" for="subSeccionPonderacion">Indica la ponderación <span class="required">*</span></label>
                                                        <div class="col-sm-6">
                                                            <input type="text"
                                                                   class="form-control subSeccionPonderacion"
                                                                   name="subSeccionPonderacion"
                                                                   autocomplete="off"
                                                                   maxlength="3">
                                                        </div>
                                                        <div class="col-xs-12">&nbsp;</div>
                                                        <div class="col-sm-4">
                                                            <button type="button"
                                                                    class="btn btn-primary agregarSubSeccionPregunta">Agregar pregunta</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 preguntas_respuestas_sub_seccion"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                    <?php endif; ?>
                    <div class="space-12"></div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="pregunta"><?php if ($seccion['tipo_respuestas'] == 'simple') : ?>Indica la pregunta <?php else : ?> Seleccione imagen de la pregunta <?php endif; ?><span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <?php if ($seccion['tipo_respuestas'] == 'simple') : ?>
                                        <input type="text"
                                               class="form-control"
                                               id="pregunta"
                                               name="pregunta"
                                               autocomplete="off">
                                    <?php else : ?>
                                        <input type="hidden"
                                               id="pregunta"
                                               name="pregunta">
                                        <input type="file" 
                                               id="pregunta-image" 
                                               name="files" />
                                    <?php endif; ?>
                                </div>
                                <?php if ($seccion['tipo_respuestas'] == 'imagen-simple') : ?>
                                    <div class="col-xs-12">&nbsp;</div>
                                    <label class="col-sm-4 control-label no-padding-right" for="Seccion_cantidad_respuesta">Indica la cantidad de respuestas de la pregunta <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text"
                                               class="form-control"
                                               id="Seccion_cantidad_respuesta"
                                               name="Seccion[cantidad_respuesta]"
                                               autocomplete="off"
                                               maxlength="3">
                                    </div>
                                <?php endif; ?>
                                <div class="col-xs-12">&nbsp;</div>
                                <label class="col-sm-4 control-label no-padding-right" for="ponderacion">Indica la ponderación <span class="required">*</span></label>
                                <div class="col-sm-6">
                                    <input type="text"
                                           class="form-control"
                                           id="ponderacion"
                                           name="ponderacion"
                                           autocomplete="off"
                                           maxlength="3">
                                </div>
                                <div class="col-xs-12">&nbsp;</div>

                                <div class="col-sm-12">
                                    <button type="button"
                                            class="btn btn-primary"
                                            id="agregar-pregunta">Agregar pregunta</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="space-12"></div>

                        <div class="col-md-12 preguntas_respuestas">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!--/ Pregunta widget -->
<script>

    $(document).ready(function() {

        <?php if ($seccion['tipo_respuestas'] != 'simple') : ?>
            $('#pregunta-image').fileupload({
                url: '/prueba/pregunta/agregarImagenPregunta',
                dataType: 'json',
                done: function (e, data) {
                    if (data.result.files[0].url) {
                        $('input#pregunta').val(data.result.files[0].url);
                    }
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                }
            }).ace_file_input({
                no_file:'Selecciona imagen...',
                btn_choose:'Explorar',
                btn_change:'Cambiar',
                droppable:false,
                onchange:null,
                thumbnail:false
            });
        <?php endif; ?>

        $('#pregunta, .subSeccionPregunta, .subSeccionNombre').bind('keyup blur', function() {
            keyAlphaNum(this, true, false);
            makeUpper(this);
        });

        $('#ponderacion, #Seccion_cantidad_respuesta, .subSeccionPonderacion, .subSeccionCantidadRespuesta').bind('keyup blur', function() {
            keyNum(this, false, false);
        });

        $('.subSeccionNombre').bind('blur', function() {
            var subSeccion = $(this).val();
            var subSeccionArray = new Array();
            var tam;

            $('.subSeccionNombre').each(function() {
                if ($(this).val() != '') {
                    subSeccionArray.push($(this).val());
                }
            });

            if (subSeccionArray != '')
                tam = subSeccionArray.length;

            if (subSeccion != '' && tam > 1) {
                var resultado = validarSubSeccionDuplicada(subSeccion, subSeccionArray);

                if (resultado == true) {
                    var mensaje = 'Por favor ingrese otra subsección para esta sección porque ya fue ingresada';
                    var title = 'Subsección ya existe';
                    $(this).val('');
                    verDialogo(mensaje, title, 'error');
                }
            }
        });

        $(".subSeccionDuracion").mask("H9:M9:M9");

        $('.subSeccionDuracion').unbind('blur');
        $('.subSeccionDuracion').bind(' blur', function() {
            var mensaje = '<b>Estimado usuario recuerde que el formato del tiempo de duración de la subsección debe ser HH:MM:SS ejemplo 00:00:00, por favor intente nuevamente.</b>';
            var title = 'Formato de hora';
            var duracion = $(this).val();
            var valor = new RegExp(/^\d{2}:\d{2}:\d{2}$/);//Valida que contenga 00:00:00 ese formato.
            var valid = valor.test(duracion);
            var tam;
            var tiempo = new Array();
            tiempo = duracion.split(":");
            var hora = tiempo[0];
            var minuto = tiempo[1];
            var segundo = tiempo[2];

            if (duracion != '__:__:__')
                tam = duracion.length;
            if (!valid) {//Si no contiene el formato requerido muestra error.
                if (duracion != '' && duracion != null && duracion != '__:__:__') {//Valido el formato de la hora.
                    $(this).addClass('error').val('').focus();
                    verDialogo(mensaje, title);
                }
            } else {//Si contiene el formato requerido.
                if (duracion != '' && duracion != null && duracion != '__:__:__') {
                    if (tam == 8) {
                        if (hora <= 23) {
                            if (minuto <= 59) {
                                if (segundo <= 59) {
                                    if (duracion == '00:00:00') {
                                        var mensaje = '<b>Estimado usuario el tiempo de duración ingresado es inválido, por favor intente nuevamente.</b>';
                                        $(this).addClass('error').val('').focus();
                                        verDialogo(mensaje, title);
                                    }
                                } else {
                                    var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 59 segundos, por favor intente nuevamente.</b>';
                                    $(this).addClass('error').val('').focus();
                                    verDialogo(mensaje, title);
                                }
                            } else {
                                var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 59 minutos, por favor intente nuevamente.</b>';
                                $(this).addClass('error').val('').focus();
                                verDialogo(mensaje, title);
                            }
                        } else {
                            var mensaje = '<b>Estimado usuario el tiempo que va a durar la subsección de prueba pueden contener maximo 23 horas, por favor intente nuevamente.</b>';
                            $(this).addClass('error').val('').focus();
                            verDialogo(mensaje, title);
                        }
                    } else {
                        var mensaje = '<b>Estimado usuario el tamaño de la duración ingresado es maximo de 8 digitos, por favor intente nuevamente.</b>';
                        $(this).addClass('error').val('').focus();
                        verDialogo(mensaje, title);
                    }
                }
            }

            var horasSubSecciones = new Array();
            var cantidadSubSecciones = Number($('#Seccion_cantidad_sub_seccion').val());
            var duracionSeccion = $('#Seccion_duracion').val();
             //var instruccion_prueba = $('#Seccion_instruccion_prueba').val();

            $('.subSeccionDuracion').each(function() {
                var duracionActual = $(this).val();
                if (duracionActual != '' && duracionActual != '__:__:__') {
                    if (duracionActual != '00:00:00') {

                        if (HTO.stringToSeconds(duracionActual) >= HTO.stringToSeconds(duracionSeccion) && cantidadSubSecciones > 1) {
                            verDialogo('La duración de la subsección no puede ser mayor o igual que la duración de sección.', 'Duración de subsección');
                            displayDialogBoxSelector('.errorValidacionSubSeccion', 'info', 'Recuerde que la suma de cada duración de subsección es la duración de sección que es: ' + duracionSeccion, $(this).parent().parent().parent().parent().parent());
                            $(this).addClass('error').val('').focus();
                        } else if ($('.subSeccionDuracion:last') != $(this) && (HTO.stringToSeconds(duracionActual) == HTO.stringToSeconds(duracionSeccion) && cantidadSubSecciones > 1)) {
                            verDialogo('La duración de la subsección debe ser menor que la duración restante de la sección.', 'Duración de subsección');
                            displayDialogBoxSelector('.errorValidacionSubSeccion', 'info', 'Recuerde que la suma de cada duración de subsección es la duración de sección que es: ' + duracionSeccion, $(this).parent().parent().parent().parent().parent() + '<br>' + 'Duración faltante: ' + (HTO.sumTimes(horasSubSecciones) - HTO.stringToSeconds(duracionActual)));
                            $(this).addClass('error').val('').focus();
                        } else if (HTO.stringToSeconds(duracionActual) != HTO.stringToSeconds(duracionSeccion) && cantidadSubSecciones == 1) {
                            verDialogo('La duración de la subsección no puede ser diferente que la duración de la sección.', 'Duración de subsección', 'error');
                            $(this).val(duracionSeccion).focus();
                        } else {
                            horasSubSecciones.push($(this).val());
                            $(this).removeClass('error');
                        }
                    } else {
                        var mensaje = '<b>Estimado usuario el tiempo de duración ingresado es inválido, por favor intente nuevamente.</b>';
                        $(this).val('').focus();
                        verDialogo(mensaje, title);
                    }
                }
            });

            // Validar que la suma de la duración de las subsecciones sea igual que la duración de la sección
            // Sólo si la cantidad de subsecciones es mayor a 1 cantidadSubSecciones.length
            if (horasSubSecciones.length == 2) {
                console.log('igual a dos');
                var suma = HTO.sumTimes(horasSubSecciones);
                if (HTO.stringToSeconds(suma) != HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length == cantidadSubSecciones) {
                    verDialogo('La suma de las duraciones de subsecciones no es igual a la duración de la sección.', 'Duración de subsección');
                    // Calcular la duración válida para la subsección actual
                    var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                    // Agregar el valor de la duración actual

                    $(this).val(HTO.secondsToTime(duracionValida));
                    // Permitir edición de la duración de las subsecciones
                    $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
                } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length == cantidadSubSecciones) {
                    $(this).removeClass('error');
                } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length < cantidadSubSecciones) {
                    verDialogo('La suma de las duraciones de subsecciones debe coincidir con la cantidad de subsecciones creadas.', 'Duración de subsección');
                    $(this).addClass('error').val('').focus();
                }
            } else if (horasSubSecciones.length > 2) {
                var suma = HTO.sumTimes(horasSubSecciones);
                if (HTO.stringToSeconds(suma) < HTO.stringToSeconds(duracionSeccion)) {
                    verDialogo('La suma de las duraciones de subsecciones no puede ser menor a la duración de la sección.', 'Duración de subsección');
                    var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                    // Agregar el valor de la duración actual

                    $(this).val(HTO.secondsToTime(duracionValida));
                    $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
                }
                if (HTO.stringToSeconds(suma) > HTO.stringToSeconds(duracionSeccion)) {
                    verDialogo('La suma de las duraciones de subsecciones no es igual a la duración de la sección.', 'Duración de subsección');
                    var duracionValida = HTO.stringToSeconds(duracionSeccion) - (HTO.stringToSeconds(suma) - HTO.stringToSeconds($(this).val()));
                    // Agregar el valor de la duración actual

                    $(this).val(HTO.secondsToTime(duracionValida));
                    $('.subSeccionDuracion').removeAttr('readonly').removeAttr('disabled');
                } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && $(this) === $('.subSeccionDuracion:last')) {
                    $(this).removeClass('error');
                } else if (HTO.stringToSeconds(suma) == HTO.stringToSeconds(duracionSeccion) && horasSubSecciones.length < cantidadSubSecciones) {
                    verDialogo('La suma de las duraciones de subsecciones debe coincidir con la cantidad de subsecciones creadas.', 'Duración de subsección');
                    $(this).addClass('error').val('').focus();
                }
            }
        });

    });
    // Agregar preguntas de sección
    var numeroPreguntaSeccion = 0;
    $('button#agregar-pregunta').on('click', function() {

      //alert($('#Seccion_cantidad_respuesta').val());

        var pregunta = $('input#pregunta').val();
        var ponderacion = $('input#ponderacion').val();
        //var ponderacion = $('#ponderacion').val();
        var cantidadRespuestas = $('#Seccion_cantidad_respuesta').val();
        var preguntaExiste = false;
        var tipoRespuestas = $('input#Seccion_tipo_respuestas').val();
        var instruccion_sub_seccion = $('#SubSeccion_instruccion_sub_seccion').val();
        var nombre = $('#Seccion_nombre').val();
        var duracion = $('#Seccion_duracion').val();
        var cantidad_respuesta = $('#Seccion_cantidad_respuesta').val();
        var cantidad_sub_seccion = $('#Seccion_cantidad_sub_seccion').val();
        var instruccion_seccion = $('#Seccion_instruccion_seccion').val();

        if (pregunta == '' || ponderacion == '' || cantidadRespuestas == '') {
            $('input#pregunta, input#ponderacion, #Seccion_cantidad_respuesta').addClass('error');
            <?php if ($seccion['tipo_respuestas'] == 'imagen-simple') : ?>
                verDialogo('Los campos <b>Seleccione imagen de la pregunta</b>, <b>Indica la cantidad de respuestas de la pregunta</b> e <b>Indica la ponderación</b> no pueden estar vacíos por favor ingrese la pregunta correspondiente.', 'Imagen de pregunta, antidad de respuestas o ponderación vacía', 'error');
            <?php elseif ($seccion['tipo_respuestas'] == 'domino') : ?>
                verDialogo('Los campos <b>Seleccione imagen de la pregunta</b> e <b>Indica la ponderación</b> no pueden estar vacíos por favor ingrese la pregunta correspondiente.', 'Imagen de pregunta o ponderación vacía', 'error');
            <?php else : ?>
                verDialogo('Los campos <b>Indica la pregunta</b> e <b>Indica la ponderación</b> no pueden estar vacíos por favor ingrese la pregunta correspondiente.', 'Pregunta o ponderación vacía', 'error');
            <?php endif; ?>
            //displayDialogBoxSelector('.error_validacion', 'error', 'Los campos <b>Indica la pregunta</b> e <b>Indica la ponderación</b> no pueden estar vacíos por favor ingrese la pregunta correspondiente.');
            //scrollUp('fast');
            return;
        <?php if ($seccion['tipo_respuestas'] == 'imagen-simple') : ?>
        } else if (cantidadRespuestas < 2) {
            $('#Seccion_cantidad_respuesta').addClass('error');
            verDialogo('La cantidad de respuestas que va a contener cada pregunta debe ser mayor a 1.', 'Cantidad de respuestas mayor a 1');
            return;
        } else if (cantidadRespuestas > 10) {
            $('#Seccion_cantidad_respuesta').addClass('error');
            verDialogo('La cantidad de respuestas que va a contener cada pregunta debe ser menor o igual a 10.', 'Cantidad de respuestas menor o igual a 10');
            return;
        <?php endif; ?>
        } else if (ponderacion < 1) {
            $('input#ponderacion').val('').addClass('error');
            verDialogo('El campo <b>Indica la ponderación</b> no puede ser menor que 1, por favor ingrese la ponderación correspondiente.', 'Ponderación mayor que 0');
            //displayDialogBoxSelector('.error_validacion', 'error', 'El campo <b>Indica la ponderación</b> no puede ser menor que 1, por favor ingrese la ponderación correspondiente.');
            //scrollUp('fast');
            return;
        } else if (ponderacion > 3) {
            var mensaje = 'La ponderación de la pregunta debe ser entre 1-3';
            var title = 'Ponderación inválida';
            $('input#ponderacion').val('').focus();
            verDialogo(mensaje, title);
            return;
        }

        $('input[data-type="pregunta"]').each(function() {
            if (pregunta == $(this).val()) {
                preguntaExiste = true;
            }
        });

        if (preguntaExiste) {
            $('input#pregunta').addClass('error');
            verDialogo('La pregunta que ingresó ya fue agregada anteriormente, por favor ingrese otra pregunta.', 'Pregunta ya existe', 'error');
            //displayDialogBoxSelector('.error_validacion', 'error', 'La pregunta que ingresó ya fue agregada anteriormente, por favor ingrese otra pregunta.');
            //scrollUp('fast');
            return;
        }

        $('input#pregunta, #Seccion_cantidad_respuesta,input#ponderacion').removeClass('error');

        var cantidadRespuestas = $('input#Seccion_cantidad_respuesta').val();
        var uri = '/prueba/Respuesta/agregarRespuestas/cantidad/' + base64_encode(cantidadRespuestas);
        var data = {
            'pregunta': pregunta,
            'ponderacion': ponderacion,
            'numeroPregunta': numeroPreguntaSeccion,
            'tipoRespuestas': tipoRespuestas,
            'instruccion_seccion': instruccion_seccion,
            'cantidad_sub_seccion': cantidad_sub_seccion,
            'cantidad_respuesta': cantidad_respuesta,
            'duracion': duracion,
            'nombre': nombre

        };

        var success = function(response) {
            $('.preguntas_respuestas').append(response);
        };

        ejecutarAjax('.preguntas_respuestas', uri, data, null, null, null, null, null, success);

        $('input#pregunta, input#ponderacion').val('');
        $('.ace-file-input').find('a.remove').click();
        $('input#pregunta, input#pregunta-image, input#ponderacion').attr('disabled', 'disabled');

        <?php if ($seccion['tipo_respuestas']) : ?>
            $('#Seccion_cantidad_respuesta').val('').attr('disabled', 'disabled');
        <?php endif; ?>

        $(this).attr('disabled', 'disabled');

        numeroPreguntaSeccion++;
    });

    // Agregar preguntas de subsección
    var numeroSubSeccionPregunta = 0;
    $('button.agregarSubSeccionPregunta').each(function(numeroSubSeccion) {
        // Al cambiar el tipo de respuestas 
        // Se puede ocultar el campo de pregunta a imagen
        $('.subSeccionTipoRespuestas', 'div[data-sub-seccion-id="' + numeroSubSeccion + '"]').change(function() {
            var widget = $('div[data-sub-seccion-id="' + numeroSubSeccion + '"]');
            if ($(this).val() == 'imagen-simple' || $(this).val() == 'domino') {
                // Se esconde el campo de pregunta
                $('.subSeccionPregunta', widget).attr('type', 'hidden');
                // Se agrega el campo de seleccionar imagen y el label adecuado
                $('.labelPregunta', widget).html('Seleccione imagen de la pregunta <span class="required">*</span>');
                $('.subSeccionPreguntaImagen', widget).removeAttr('disabled').attr('type', 'file').fileupload({
                    url: '/prueba/pregunta/agregarImagenPregunta',
                    dataType: 'json',
                    done: function (e, data) {
                        if (data.result.files[0].url) {
                            $('.subSeccionPregunta', widget).val(data.result.files[0].url);
                        }
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                    }
                }).ace_file_input({
                    no_file:'Selecciona imagen...',
                    btn_choose:'Explorar',
                    btn_change:'Cambiar',
                    droppable:false,
                    onchange:null,
                    thumbnail:false
                });
            } else {
                $('.labelPregunta', widget).html('Ingrese la pregunta <span class="required">*</span>');
                var inputPregunta = $('.subSeccionPregunta').attr('type', 'text');
                var inputFile = $('.subSeccionPreguntaImagen', widget).attr('disabled', 'disabled').attr('type', 'hidden');
                $('.ace-file-input', widget).remove();
                // Agregar de nuevo el campo al contenedor
                inputPregunta.parent().append(inputFile);
            }
        });

        $(this).on('click', function() {
            var widget = $('div[data-sub-seccion-id="' + numeroSubSeccion + '"]');
            if ($('input.subSeccionNombre', widget).val() == '' || $('input.subSeccionCantidadRespuesta', widget).val() == '' || $('input.subSeccionDuracion', widget).val() == '' || $('select.subSeccionTipoRespuestas', widget).val() == '') {
                $('input.subSeccionNombre, input.subSeccionCantidadRespuesta, input.subSeccionDuracion, select.subSeccionTipoRespuestas', widget).addClass('error');
                verDialogo('Estimado usuario por favor ingrese los datos correspondientes en los campos marcados como requeridos con el <span class="required">*</span> ya que son obligatorios.', 'Campo vacío', 'error');
                //displayDialogBoxSelector('.errorValidacionSubSeccion', 'error', 'Estimado usuario por favor ingrese los datos correspondientes en los campos marcados como requeridos con el <span class="required">*</span> ya que son obligatorios.', widget);
                return false;
            } else
            if ($('input.subSeccionCantidadRespuesta', widget).val() < 2) {
                verDialogo('La cantidad de respuestas que va a contener cada pregunta debe ser mayor a 1.', 'Cantidad de respuestas mayor a 1');
                //displayDialogBoxSelector('.errorValidacionSubSeccion', 'error', 'Cantidad de respuestas debe ser mayor a 1.', widget);
                return false;
            } else {
                $('input.subSeccionNombre, input.subSeccionCantidadRespuesta, input.subSeccionDuracion, select.subSeccionTipoRespuestas', widget).removeClass('error');
                $('input.subSeccionNombre, input.subSeccionCantidadRespuesta, input.subSeccionDuracion', widget).attr('readonly', 'readonly');
            }

            var pregunta = $('input.subSeccionPregunta', widget).val();
            var ponderacion = $('input.subSeccionPonderacion', widget).val();
            var tipoRespuestas = $('select.subSeccionTipoRespuestas', widget).val();


            if (pregunta == '' || ponderacion == '') {
                console.log('entre validacion');
                $('input.subSeccionPregunta, input.subSeccionPonderacion', widget).addClass('error');
                if (tipoRespuestas == 'simple') {
                    verDialogo('Los campos <b>Indica la pregunta</b> e <b>Indica la ponderación</b> no pueden estar vacíos por favor ingrese la pregunta correspondiente.', 'Pregunta o ponderación vacía');
                } else {
                    verDialogo('Los campos <b>Seleccione imagen de la pregunta</b> e <b>Indica la ponderación</b> no pueden estar vacíos por favor ingrese la pregunta correspondiente.', 'Imagen de pregunta o ponderación vacía', 'error');
                }
                //displayDialogBoxSelector('.error_validacion', 'error', 'El campo <b>Indica la pregunta</b> no puede estar vacío por favor ingrese la pregunta correspondiente.');
                //scrollUp('fast');
                return;
            } else if (ponderacion < 1) {
                $('input.subSeccionPonderacion', widget).addClass('error');
                verDialogo('El campo <b>Indica la ponderación</b> no puede ser menor que 1, por favor ingrese la ponderación correspondiente.', 'Ponderación mayor que 0');
                return;
            } else if (ponderacion > 3) {
                var mensaje = 'La ponderación de la pregunta debe ser entre 1-3';
                var title = 'Ponderación inválida';
                $('input.subSeccionPonderacion', widget).val('').focus();
                verDialogo(mensaje, title);
                return;
            }

            var preguntaExiste = false;

            $('input[data-type="subSeccionPregunta"]', widget).each(function() {
                console.log(this);
                if (pregunta == $(this).val()) {
                    preguntaExiste = true;
                }
            });

            if (preguntaExiste) {
                $('input.subSeccionPregunta', widget).addClass('error');
                verDialogo('La pregunta que ingresó ya fue agregada anteriormente, por favor ingrese otra pregunta.', 'Pregunta ya existe');
                //displayDialogBoxSelector('.error_validacion', 'error', 'La pregunta que ingresó ya fue agregada anteriormente, por favor ingrese otra pregunta.');
                //scrollUp('fast');
                return;
            }

            $('input.subSeccionPregunta, input.subSeccionPonderacion', widget).removeClass('error');

            var cantidadRespuestas = $('input.subSeccionCantidadRespuesta', widget).val();
            var uri = '/prueba/Respuesta/agregarRespuestasSubSeccion/cantidad/' + base64_encode(cantidadRespuestas);
            var data = {
                'pregunta': pregunta,
                'ponderacion': ponderacion,
                'numeroPregunta': numeroSubSeccionPregunta,
                'numeroSubSeccion': numeroSubSeccion,
                'tipoRespuestas': tipoRespuestas,
                'instruccion_sub_seccion': instruccion_sub_seccion,
                'instruccion_prueba': instruccion_prueba,
                'cantidad_sub_seccion': cantidad_sub_seccion,
                'cantidad_respuesta': cantidad_respuesta,
                'duracion': duracion,
                'nombre': nombre
            };
            var success = function(response) {
                $('.preguntas_respuestas_sub_seccion', widget).append(response);
            };

            ejecutarAjax(false, uri, data, null, null, null, null, null, success);

            $('input.subSeccionPregunta, input.subSeccionPonderacion', widget).val('');
            $('.ace-file-input', widget).find('a.remove').click();
            $('input.subSeccionPregunta, input.subSeccionPonderacion, .subSeccionPreguntaImagen', widget).attr('disabled', 'disabled');

            $(this).attr('disabled', 'disabled');

            numeroSubSeccionPregunta++;
        });
    });
</script>
