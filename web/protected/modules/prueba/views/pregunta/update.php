<?php
/* @var $this PreguntaController */
/* @var $model Pregunta */

$this->pageTitle = 'Actualización de Datos de Preguntas';

      $this->breadcrumbs=array(
	'Preguntas'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>