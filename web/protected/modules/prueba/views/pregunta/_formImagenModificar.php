<script>
    $("#botonGuardarPregunta").addClass("hide");
    $("#botonGuardarPreguntaSimple").addClass("hide");
    $("#botonGuardarPreguntaImagen").removeClass("hide");
    $("#botonGuardarPreguntaDomino").addClass("hide");
    
    $("#mensajeAlerta").html('<p>Todos los campos con <span class="required">*</span>son requeridos.</p>');
    $("#mensajeAlerta").addClass('infoDialogBox');
    $("#mensajeAlerta").removeClass('errorDialogBox');
    $("#mensajeForm").removeClass('successDialogBox');
    
    $("#botonGuardarPreguntaImagen").unbind('click');
    $("#botonGuardarPreguntaImagen").bind('click', function(){
        var id = $("#id").val();
        var tipoPrueba = $("#Pregunta_id").val();
        var pregunta_seccion_prueba_id = $("#Pregunta_seccion_prueba_id").val();
        var detallesSeccion = $("#detallesSeccion").val();
        var pregunta = $("#pregunta").val();
        var pregunta_ponderacion = $("#Pregunta_ponderacion").val();
        var respuesta = $("#respuesta").val();
        var tipo_pregunta = $("#tipo_pregunta").val();
        
        if(tipoPrueba == '' || pregunta_seccion_prueba_id == '' || detallesSeccion == ''){
            $("#mensajeAlerta").html('<p>Todos los campos con <span class="required">*</span>son requeridos.</p>');
            $("#mensajeAlerta").addClass('errorDialogBox');
            $("#mensajeAlerta").removeClass('infoDialogBox');
            $("#mensajeForm").removeClass('successDialogBox');
        }else{

            if(pregunta == ''){
                $("#mensajeForm").html('<p>Seleccione una imagen.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else if(pregunta_ponderacion == ''){
                $("#mensajeForm").html('<p>Seleccione la ponderacion para esta prueba.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else if(respuesta == ''){
                $("#mensajeForm").html('<p>Seleccione la respuesta.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else{
                $("#mensajeForm").html('<p>Formule la pregunta, seguido de marcar la respuesta correcta.</p>');
                $("#mensajeForm").addClass('infoDialogBox');
                $("#mensajeForm").removeClass('errorDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
                
                $("#loading").removeClass('hide');
                $("#contenidoPregunta1").addClass('hide');
                
            
                
                $.ajax({
                    url: "/prueba/pregunta/modificarPreguntaSimpleImagen",
                    data: 'id=' + id +
                          '&tipoPrueba=' + tipoPrueba +
                          '&pregunta_seccion_prueba_id=' + pregunta_seccion_prueba_id +
                          '&detallesSeccion=' + detallesSeccion +
                          '&pregunta=' + pregunta +
                          '&pregunta_ponderacion=' + pregunta_ponderacion +
                          '&respuesta=' + respuesta +
                          '&tipo_pregunta=' + tipo_pregunta,
                    type: 'POST',
                    success: function (data) {
                        $("#mensajeForm").html('<p>Pregunta modificada con éxito.</p>');
                        $("#mensajeForm").addClass('successDialogBox');
                        $("#mensajeForm").removeClass('infoDialogBox');
                        $("#loading").addClass('hide');
                        $("#contenidoPregunta1").removeClass('hide');
                        $('#pregunta-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    }
                });
            }
        }
        
//        console.log('RESPUESTA TEXT: ' + respuestaVacia);
//        console.log('RESPUESTAS TEXT CHECK: ' + respuestaCheckVacia);
        
    });
    
    $("#preguntaImagen").bind('change', function(){
//        alert();
    });
        
        
        
    $(document).ready(function() {

            $('#preguntaImagen').fileupload({
                url: '/prueba/pregunta/cargarImagen?t=i',
                acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
                dataType: 'json',
                singleFileUploads: true,
                autoUpload: true,
//                beforeSend : function (){
//                    formatoImagen();
//                },
                done: function (e, data) {
                    if (data.result.files[0].url) {
                        
                        var imagen = data.result.files[0].name;
                        var elemento = imagen.split('.');
                        console.log(elemento);
                        if(elemento[1] != 'undefined' || elemento[1] != 'jpg' || elemento[1] != 'jpeg' || elemento[1] != 'png'){
                            $("#mensajeForm").html('<p>Seleccione una imagen con el formato .jpg, .jpeg o .png.</p>');
                            $("#mensajeForm").removeClass('infoDialogBox');
                            $("#mensajeForm").addClass('errorDialogBox');
                            $("#mensajeForm").removeClass('successDialogBox');
                            $("#pregunta").val('');
                            $("#dataImage").attr('data-title', 'Seleccione una imagen...');
                            $("#preguntaImagen").attr('title', 'No se ha seleccionado ningún archivo.');
                        }
                        if(elemento[1] == 'jpg' || elemento[1] == 'jpeg' || elemento[1] == 'png'){
                            $("#mensajeForm").html('<p>Formule la pregunta, seguido de marcar la respuesta correcta.</p>');
                            $("#mensajeForm").addClass('infoDialogBox');
                            $("#mensajeForm").removeClass('errorDialogBox');
                            $("#mensajeForm").removeClass('successDialogBox');
    //                        $('#pregunta').val(data.result.files[0].url);
                            console.log(data.result.files[0].url);
                            console.log(data.result.files[0].name);
                            $("#dataImage").attr('data-title', data.result.files[0].name);
                            $("#preguntaImagen").attr('title', 'Imagen seleccionada: ' + data.result.files[0].name);
                            $("#pregunta").val(data.result.files[0].name);
                            $("#imagenLoad").attr('src', data.result.files[0].url);
    //                        $("#imagenView").html("<img src='" + data.result.files[0].url + "' width='200px'>");
    //                        $('#pregunta').text(data.result.files[0].name);
                        }
                    }
                    else if(data.result.files[0].error != ''){
                        $("#mensajeForm").html('<p>Ocurrio un error, verifique con mucho cuidado la imagen e intente nuevamente.</p>');
                        $("#mensajeForm").removeClass('infoDialogBox');
                        $("#mensajeForm").addClass('errorDialogBox');
                        $("#mensajeForm").removeClass('successDialogBox');
                        $("#pregunta").val('');
                        $("#dataImage").attr('data-title', 'Seleccione una imagen...');
                        $("#preguntaImagen").attr('title', 'No se ha seleccionado ningún archivo.');
                        console.log('Ocurrio un error, verifique con mucho cuidado la imagen e intente nuevamente.');
                    }
                },
                process: [
                {
                    action: 'load',
                    fileTypes: /(\.|\/)(jpe?g|png)$/i,
                    maxFileSize: 5000000 // 5MB
                },
                {
                    action: 'resize',
                    maxWidth: 1440,
                    maxHeight: 900
                },
                {
                    action: 'save'
                }
            ],
            })
        });
</script>
<div id="contenedor"> 
    <div class="">
        <div class="widget-box">
            <div class="widget-header">
                <h5>Pregunta de Selección Simple</h5>
                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-body-inner">
                    <div class="widget-main">
                        <div class="widget-main form">
                            <div class="infoDialogBox" id="mensajeForm"><p>Formule la pregunta, seguido de marcar la respuesta correcta.</p></div>
                            <div class="row">
                                <div class="row" id="step2">
                                    <?php
                                    if(isset($model->sub_seccion_prueba_id)){
                                        $tipo_seccion_id = 2;
                                        $detalle_seccion_id = $model->subSeccionPrueba->id;
                                    }
                                    else if(isset($model->seccion_prueba_id)){
                                        $tipo_seccion_id = 1;
                                        $detalle_seccion_id = $model->seccionPrueba->id;
                                    }
                                    ?>
                                    
                                    <input type="hidden" value="<?php echo $model->seccionPrueba->prueba->id; ?>" id="Pregunta_id">
                                    <input type="hidden" value="<?php echo $tipo_seccion_id; ?>" id="Pregunta_seccion_prueba_id">
                                    <input type="hidden" value="<?php echo $detalle_seccion_id; ?>" id="detallesSeccion">
                                    <input type="hidden" value="<?php echo $model->id; ?>" id="id">
                                    
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <img src="<?php echo Yii::app()->baseUrl . '/public/images/ajax-loader.gif'; ?>" width="75%" class="hide" id="loading">
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="contenidoPregunta1">
                                    <div class="col-md-4">
                                        <label>Seleccione la imagen <span class="required">*</span></label>
                                        <div class="ace-file-input">
                                            <input type="hidden" id="pregunta" value="<?php echo $model->nombre; ?>">
                                            <input type="file" id="preguntaImagen">
                                            <label data-title="Explorar" class="file-label">
                                                <span data-title="<?php echo $model->nombre; ?>" class="file-name" id="dataImage">
                                                    <i class="icon-upload-alt"></i></span>
                                            </label>
                                            <a href="#" class="remove"><i class="icon-remove"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Ponderación <span class="required">*</span></label>
                                        <select id="Pregunta_ponderacion" required="required" class="span-12">
                                            <option value="">Seleccione</option>
                                            <?php
                                            $contPonderacion = 1;
                                            while($contPonderacion != ($ponderacion + 1)){
                                                $selected = '';
                                                if($model->ponderacion == $ponderacion){
                                                    $selected = 'selected="selected"';
                                                }
                                                ?>
                                                <option value="<?php echo $contPonderacion; ?>" <?php echo $selected; ?>><?php echo $contPonderacion; ?></option>
                                                <?php
                                                $contPonderacion++;
                                            }
                                            ?>
                                        </select>
                                        <!--<input type="text" id="Pregunta_ponderacion" name="Pregunta[ponderacion]" required="required" class="span-12" prompt="Seleccione">-->
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Seleccione la Respuesta <span class="required">*</span></label>
                                        <select class="col-sm-12" id="respuesta">
                                            <option value="">Seleccione</option>
                                            <?php
                                            $respuesta = Respuesta::model()->findAll(array('condition' => 'pregunta_id=' . $model->id));
                                            while($cantidadRespuestas != 0){
                                                $selected = '';
                                                if($respuesta[0]['nombre'] == $cantidadRespuestas){
                                                    $selected = 'selected="selected"';
                                                }
                                                ?>
                                                <option value="<?php echo $cantidadRespuestas; ?>" <?php echo $selected; ?>><?php echo $cantidadRespuestas; ?></option>
                                                <?php
                                                $cantidadRespuestas--;
                                            }
                                            ?>
                                        </select>
                                        <input type="hidden" id="tipo_pregunta" value="<?php echo $tipo_pregunta; ?>" required="required">
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4"><br>
                                        <img width="250px" src="<?php echo Yii::app()->baseUrl . '/public/prueba/Imagen/' . $model->nombre; ?>" id="imagenLoad">
                                    </div>
                                    <div class="col-md-4"></div>
                                        
                                        <div id="imagenView"></div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<br>
<div class="row">

<!--    <div class="col-md-12" align="rigth">
        <a id="agregarCampo" class="btn btn-info" href="#">Agregar Pregunta</a>
    </div>-->
</div>

<!--<script src="/public/js/jquery-ui-1.10.3.custom.min.js">-->
