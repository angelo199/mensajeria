<?php
/* @var $this PreguntaController */
/* @var $model Pregunta */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'pregunta-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                        <?php
           if($model->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $model));
           else:
                ?>
                            <div class="infoDialogBox" id="mensajeAlerta"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    
                                                    <div class="col-md-6">
                                                        <?php echo $form->labelEx($model, 'Período Aperturado <span class="required">*</span>'); ?>
                                                        <?php
                                                        echo $form->dropDownList($model, 'id', CHtml::listData($periodosAperturados, 'id', 'nombre_clave'), array(
                                                            'ajax' => array(
                                                                'type' => 'GET',
                                                                'id'=>'Pregunta_id',
                                                                'update' => '#detallesSeccion',
                                                                'url' => CController::createUrl('pregunta/seleccionarSeccion'),
                                                            ),
                                                            'empty' => array('' => 'Seleccione'), 'class' => 'span-7', "required" => "required",
                                                            )
                                                        );
                                                        ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <?php echo $form->labelEx($model, 'Exámen de Postulación <span class="required">*</span>'); ?>
                                                        <?php
                                                        echo $form->dropDownList($model, 'seccion_prueba_id', CHtml::listData($listaSecciones, 'id', 'nombre'), array(
                                                            'empty' => 'Seleccione',
                                                            'id' => 'detallesSeccion',
                                                            'class' => 'span-7', "required" => "required",
                                                            'empty' => array('' => 'Seleccione'),
                                                            'ajax' => array(
                                                                'type' => 'GET',
                                                                'update' => '#detalles',
                                                                'url' => CController::createUrl('pregunta/seleccionarNombreSeccion'),
                                                            ),
                                                        ));
                                                        ?>
                                                        
                                                    </div>                                                    
                                                    
                                                    <div class="col-md-12">
                                                        <div id="detalles" class="hide"></div>
                                                    </div>
                                                    
                                                </div>

                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/prueba/seccion/edicion/id/".  base64_encode($model->seccion_prueba_id)); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>
                                    
                                    <div class="col-md-6 wizard-actions">
                                        <input type="button" class="btn btn-primary btn-next" id="botonGuardarPregunta" value="Guardar">
                                        <input type="button" class="btn btn-primary btn-next hide" id="botonGuardarPreguntaSimple" value="Guardar y Registrar Otra">
                                        <input type="button" class="btn btn-primary btn-next hide" id="botonGuardarPreguntaImagen" value="Guardar">
                                        <input type="button" class="btn btn-primary btn-next hide" id="botonGuardarPreguntaDomino" value="Guardar">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
            /**
             * Yii::app()->clientScript->registerScriptFile(
             *   Yii::app()->request->baseUrl . '/public/js/modules/PreguntaController/pregunta/form.js',CClientScript::POS_END
             *);
             */
        ?>
    </div>
</div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/prueba/pregunta.js', CClientScript::POS_END);
?>
