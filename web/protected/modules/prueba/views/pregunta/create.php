<?php

/* @var $this PreguntaController */
/* @var $model Pregunta */

$this->pageTitle = 'Registro de Preguntas y Respuestas';

$this->breadcrumbs = array(
    'Gestión de Exámenes de Postulación' => array('/../prueba'),
    'Exámenes de Postulación' => array('/../prueba/seccion'),
    'Registro de Preguntas y Respuestas',
);
?>

<?php $this->renderPartial('_form', array(
    'model' => $model,
    'modelPregunta' => $modelPregunta,
    'formType' => 'registro',
    'periodosAperturados' => $periodosAperturados,
    'listaSecciones' => $listaSecciones,
    'seccion' => $seccion
)); ?>


<?php

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/css/jquery-ui-1.10.3.custom.min.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery-ui-1.10.3.custom.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.upload/js/jquery.fileupload.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.upload/css/jquery.fileupload.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.upload/css/jquery.fileupload-ui.css');
//Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/js/jquery.upload/css/jquery.fileupload-ui.css');
?>
