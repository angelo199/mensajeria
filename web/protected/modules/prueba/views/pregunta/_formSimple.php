<?php
echo $info;
//clearstatcache();
?>
<script>
    $('#Pregunta_nombre').bind('keyup blur', function() {
        keyAlphaNum(this, true, true);// Valida que contenga letras y espacios
        makeUpper(this);//Convierte las letras en mayusculas
    });

    var cantidad_respuesta = $("#cantidad_respuesta").text();
    var i = 0, respuesta_id = '';
    for (i = 1; i <= cantidad_respuesta; i++) {
        if (cantidad_respuesta != i) {
            respuesta_id = respuesta_id + '#respuesta' + i + ', ';
        }
        if (cantidad_respuesta == i) {
            respuesta_id = respuesta_id + '#respuesta' + i;
        }
    }
    $(respuesta_id).bind('keyup blur', function() {
        keyAlphaNum(this, true, true);// Valida que contenga letras y espacios
        makeUpper(this);//Convierte las letras en mayusculas
    });
    $("#botonGuardarPregunta").addClass("hide");
    $("#botonGuardarPreguntaSimple").removeClass("hide");
    $("#botonGuardarPreguntaImagen").addClass("hide");
    $("#botonGuardarPreguntaDomino").addClass("hide");

    $("#mensajeAlerta").html('<p>Todos los campos con <span class="required">*</span>son requeridos.</p>');
    $("#mensajeAlerta").addClass('infoDialogBox');
    $("#mensajeAlerta").removeClass('errorDialogBox');
    $("#mensajeForm").removeClass('successDialogBox');

    $("#botonGuardarPreguntaSimple").unbind('click');
    $("#botonGuardarPreguntaSimple").bind('click', function() {
        var tipoPrueba = $("#Pregunta_id").val();
        var pregunta_seccion_prueba_id = $("#detallesSeccion").val();
        var detallesSeccion = $("#detallesSeccion").val();
        var pregunta_nombre = $.trim($("#Pregunta_nombre").val());
        var pregunta_ponderacion = $("#Pregunta_ponderacion").val();
        var cantidadRespuestas = $("#cantidadRespuestas").val();
        var tipo_pregunta = $("#tipo_pregunta").val();
        var respuestaVacia = 0;
        var respuestaCheckVacia = 0;
        var i = 0;

        if (tipoPrueba == '' || pregunta_seccion_prueba_id == '' || detallesSeccion == '') {
            $("#mensajeAlerta").html('<p>Todos los campos con <span class="required">*</span>son requeridos.</p>');
            $("#mensajeAlerta").addClass('errorDialogBox');
            $("#mensajeAlerta").removeClass('infoDialogBox');
            $("#mensajeForm").removeClass('successDialogBox');
        } else {
            /*Se identifica si el usuario le dio check a alguna respuesta*/
            for (i = 1; i <= cantidadRespuestas; i++) {
                if ($("#respuestaCorrecta" + i).is(":checked")) {
                    respuestaCheckVacia++;
                }
            }
            for (i = 1; i <= cantidadRespuestas; i++) {
                if ($("#respuesta" + i).val() == '') {
                    respuestaVacia++;
                }
            }

            if (pregunta_nombre == '') {
                $("#mensajeForm").html('<p>Formule una pregunta.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
                $("#Pregunta_nombre").val('');
                $("#Pregunta_nombre").focus();
            }
            else if (pregunta_ponderacion == '') {
                $("#mensajeForm").html('<p>Seleccione la ponderacion para esta prueba.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else if (respuestaVacia > 0) {
                $("#mensajeForm").html('<p>Todas las respuestas son obligatorias.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else if (respuestaCheckVacia != 1) {
                $("#mensajeForm").html('<p>Seleccione la respuesta correcta.</p>');
                $("#mensajeForm").addClass('errorDialogBox');
                $("#mensajeForm").removeClass('infoDialogBox');
                $("#mensajeForm").removeClass('successDialogBox');
            }
            else {
                var i = 0, j = 0, llave = 1, a = 0, b = 0;
                var cantidad_respuesta = $("#cantidad_respuesta").text();
                for (i = 1; i <= cantidad_respuesta; i++) {
                    for (j = i + 1; j <= cantidad_respuesta; j++) {
                        a = $("#respuesta" + i).val();
                        b = $("#respuesta" + j).val();
//                        if(a != 'undefined' || b != 'undefined'){
                        if (a == b) {
//                                console.log(a + ':' + i + '=' + b + ':' + j);
                            $("#mensajeForm").html('<p>Las respuestas no deben ser iguales.</p>');
                            $("#mensajeForm").addClass('errorDialogBox');
                            $("#mensajeForm").removeClass('infoDialogBox');
                            $("#mensajeForm").removeClass('successDialogBox');
                            llave = 0;
                            break;
                        }
//                        }
                    }
                }

                if (llave == 1) {
                    $("#mensajeForm").html('<p>Formule la pregunta, seguido de marcar la respuesta correcta.</p>');
                    $("#mensajeForm").addClass('infoDialogBox');
                    $("#mensajeForm").removeClass('errorDialogBox');
                    $("#mensajeForm").removeClass('successDialogBox');

                    $("#loading").removeClass('hide');
                    $("#contenidoPregunta").addClass('hide');
                    var dataRespuesta = '';
                    var respuestaCorrecta = '';

                    for (i = 1; i <= cantidadRespuestas; i++) {
                        if ($("#respuestaCorrecta" + i).is(":checked")) {
                            respuestaCorrecta = '&respuestaCorrecta=' + $("#respuesta" + i).val();
                        }
                        dataRespuesta = dataRespuesta + '&Respuesta[' + i + ']' + '=' + $("#respuesta" + i).val();
                    }

                    $.ajax({
                        url: "/prueba/pregunta/registrarPreguntaSimple",
                        data: 'tipoPrueba=' + tipoPrueba +
                                '&pregunta_seccion_prueba_id=' + pregunta_seccion_prueba_id +
                                '&detallesSeccion=' + detallesSeccion +
                                '&pregunta_nombre=' + pregunta_nombre +
                                '&pregunta_ponderacion=' + pregunta_ponderacion +
                                '&cantidadRespuestas=' + cantidadRespuestas +
                                '&tipo_pregunta=' + tipo_pregunta +
                                dataRespuesta + respuestaCorrecta,
                        type: 'POST',
                        success: function(data) {
                            $("#loading").addClass('hide');
                            $("#contenidoPregunta").removeClass('hide');
                            if (data == 2) {
                                $("#mensajeForm").html('<p>La pregunta esta repetida.</p>');
                                $("#mensajeForm").addClass('errorDialogBox');
                                $("#mensajeForm").removeClass('infoDialogBox');
                                $("#mensajeForm").removeClass('successDialogBox');
                            }
                            else {
                                $("#mensajeForm").html('<p>Pregunta agregada con éxito.</p>');
                                $("#mensajeForm").addClass('successDialogBox');
                                $("#mensajeForm").removeClass('infoDialogBox');
                                $("#Pregunta_nombre").val('');
                                $("#Pregunta_ponderacion").val('');
                                $("#cantidadPregunta").text(parseInt($("#cantidadPregunta").text()) + 1);
                                if (data == 1) {
                                    for (i = 1; i <= cantidadRespuestas; i++) {
                                        /*QUITAR EL CHECK*/
                                    }
                                    for (i = 1; i <= cantidadRespuestas; i++) {
                                        $("#respuesta" + i).val('');
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }

//        console.log('RESPUESTA TEXT: ' + respuestaVacia);
//        console.log('RESPUESTAS TEXT CHECK: ' + respuestaCheckVacia);

    });
</script>
<div id="contenedor">
    <div class="">
        <div class="widget-box">
            <div class="widget-header">
                <h5>Pregunta de Selección Simple</h5>
                <div class="widget-toolbar">
                    <a data-action="collapse" href="#">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-body-inner">
                    <div class="widget-main">
                        <div class="widget-main form">
                            <div class="infoDialogBox" id="mensajeForm"><p>Formule la pregunta, seguido de seleccionar la ponderaci&oacute;n y  de marcar la respuesta correcta.</p></div>
                            <div class="row">
                                <div class="row" id="step2">

                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <img src="<?php echo Yii::app()->baseUrl . '/public/images/ajax-loader.gif'; ?>" width="75%" class="hide" id="loading">
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="contenidoPregunta">
                                        <div class="col-md-8">
                                            <label>Indica La  Pregunta <span class="required">*</span></label>
                                            <input type="text" maxlength="150" id="Pregunta_nombre" name="Pregunta[nombre]" required="required" class="span-12" prompt="Seleccione">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Ponderación <span class="required">*</span></label>
                                            <select id="Pregunta_ponderacion" required="required" class="span-12">
                                                <option value="">Seleccione</option>
                                                <?php
                                                $contPonderacion = 1;
                                                while ($contPonderacion != ($ponderacion + 1)) {
                                                    ?>
                                                    <option value="<?php echo $contPonderacion; ?>"><?php echo $contPonderacion; ?></option>
                                                    <?php
                                                    $contPonderacion++;
                                                }
                                                ?>
                                            </select>
                                            <!--<input type="text" id="Pregunta_ponderacion" name="Pregunta[ponderacion]" required="required" class="span-12" prompt="Seleccione">-->
                                        </div>
                                        <input type="hidden" value="<?php echo $cantidadRespuestas; ?>" id="cantidadRespuestas">
                                        <?php
                                        $numeroRespuesta = 1;
                                        while ($cantidadRespuestas != 0) {
                                            ?>
                                            <div class="col-md-12">
                                                <div class="col-md-1" align="center">
                                                    <label><input type="radio" name="respuesta" id="respuestaCorrecta<?php echo $numeroRespuesta; ?>" required="required"></label>
                                                </div>
                                                <div class="col-md-11">
                                                    <input type="text" maxlength="150" id="respuesta<?php echo $numeroRespuesta; ?>" required="required" class="span-12" placeholder="<?php echo 'Respuesta número ' . $numeroRespuesta; ?>">
                                                </div>
                                            </div>
                                            <?php
                                            $numeroRespuesta++;
                                            $cantidadRespuestas--;
                                        }
                                        ?>
                                        <input type="hidden" id="tipo_pregunta" value="<?php echo $tipo_pregunta; ?>" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<br>
<div class="row">

    <!--    <div class="col-md-12" align="rigth">
            <a id="agregarCampo" class="btn btn-info" href="#">Agregar Pregunta</a>
        </div>-->
</div>
