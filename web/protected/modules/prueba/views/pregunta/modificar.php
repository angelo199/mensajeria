<div style="min-height: 500px;" id="main-container" class="row-fluid">
    <!-- <div class="col-xs-12">-->

    <div class="col-xs-12">
        <div class="row-fluid">

            <div class="tabbable">

                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#datosGenerales">Datos Generales</a></li>
                    <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
                </ul>

                <div class="tab-content">
                    <div id="datosGenerales" class="tab-pane active">
                        <div class="form">

                            <form method="post" action="/registroPrueba/pregunta/registro" id="pregunta-form" data-form-type="registro">
                                <div id="div-result">
                                    <div id="mensajeAlerta" class="infoDialogBox"><p>Consulta de Datos de la Sección <b><?php echo $model->seccionPrueba->nombre; ?></b></p></div>
                                </div>

                                <div id="div-datos-generales">

                                    <div class="widget-box">

                                        <div class="widget-header">
                                            <h5>Datos Generales</h5>

                                            <div class="widget-toolbar">
                                                <a href="#" data-action="collapse">
                                                    <i class="icon-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-body-inner">
                                                <div class="widget-main">
                                                    <div class="widget-main form">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <label>Periodo de la Prueba</label>
                                                                <select class="span-12" disabled="disabled">
                                                                    <option>
                                                                        <?php echo (is_object($model->seccionPrueba) && is_object($model->seccionPrueba->prueba))? $model->seccionPrueba->prueba->nombre_clave:null; ?>
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-md-6">
                                                                <label>Nivel de la Prueba</label>
                                                                <select class="span-7" disabled="disabled">
                                                                    <option>
                                                                        <?php echo (is_object($model->seccionPrueba) && is_object($model->seccionPrueba->nivelCargo))?$model->seccionPrueba->nivelCargo->nombre:null; ?>
                                                                    </option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label>Nombre de la Prueba</label>
                                                                <select class="span-7" disabled="disabled">
                                                                    <option>
                                                                        <?php echo is_object($model->seccionPrueba)?$model->seccionPrueba->nombre:null; ?>
                                                                    </option>
                                                                </select>
                                                            </div> 
                                                            <br/>
                                                            <br/>
                                                            <br/>
                                                            <br/>
                                                            <br/>
                                                            <br/>
                                                            <br/>
                                                            <div class="space-6"> </div>

                                                            <?php
                                                            $tipo_pregunta = is_object($model->seccionPrueba)?$model->seccionPrueba->tipo_pregunta:null;
                                                            if ($tipo_pregunta == 'T') {
                                                                $tipoPreguntaText = 'Selección Simple';
                                                            } else if ($tipo_pregunta == 'I') {
                                                                $tipoPreguntaText = 'Selección Simple (Imagen)';
                                                            } else if ($tipo_pregunta == 'D') {
                                                                $tipoPreguntaText = 'Domino';
                                                            } else {
                                                                $tipoPreguntaText = 'No posee';
                                                            }
                                                            ?>

                                                            <div class="col-md-12">
                                                                <div class="" id="detalles">
                                                                    <div class="infoDialogBox" id="detalles">
                                                                        <p>
                                                                            Datos de la Sección<br><br>
                                                                            <b>Nombre:</b> <?php echo is_object($model->seccionPrueba)?$model->seccionPrueba->nombre:null; ?><br>

                                                                            <b>Duración:</b> <?php echo is_object($model->seccionPrueba)?$model->seccionPrueba->duracion_seccion:null; ?><br>
                                                                            <b>Cantidad de Respuestas:</b> <?php echo (is_object($model->seccionPrueba))?$model->seccionPrueba->cantidad_respuesta:null; ?><br>
                                                                            <label class="hide" id="cantidad_respuesta">3</label>
                                                                            <b>Tipo de pregunta:</b> <?php echo $tipoPreguntaText; ?><br>
                                                                            <b>Instrucción de la Sección:</b> <?php echo (is_object($model->seccionPrueba))?$model->seccionPrueba->instruccion_seccion:null; ?><br>
                                                                            <b>Cantidad de preguntas formuladas: </b><label id='cantidadPregunta'><?php echo $cantidadPreguntas; ?></label>
                                                                        </p>
                                                                    </div>

                                                                    <?php 
                                                                    $cantidad_respuesta = (is_object($model->seccionPrueba))?$model->seccionPrueba->cantidad_respuesta:null;                                                                    
                                                                    if ($tipo_pregunta == 'T') {
                                                                        $this->renderPartial('_formSimpleModificar', array(
                                                                            'model' => $model,
                                                                            'ponderacion' => $ponderacionCantidad,
                                                                            'cantidadRespuestas' => $cantidad_respuesta,
                                                                            'tipo_pregunta' => $tipo_pregunta
                                                                        ));
                                                                    }
                                                                    if ($tipo_pregunta == 'I') {
                                                                        $this->renderPartial('_formImagenModificar', array(
                                                                            'model' => $model,
                                                                            'ponderacion' => $ponderacionCantidad,
                                                                            'cantidadRespuestas' => $cantidad_respuesta,
                                                                            'tipo_pregunta' => $tipo_pregunta
                                                                        ));
                                                                    }
                                                                    if ($tipo_pregunta == 'D') {
                                                                        $this->renderPartial('_formDominoModificar', array(
                                                                            'model' => $model,
                                                                            'ponderacion' => $ponderacionCantidad,
                                                                            'cantidadRespuestas' => $cantidad_respuesta,
                                                                            'tipo_pregunta' => $tipo_pregunta
                                                                        ));
                                                                    }
                                                                    ?>


                                                                    <br>
                                                                    <div class="row">

                                                                        <!--    <div class="col-md-12" align="rigth">
                                                                                <a id="agregarCampo" class="btn btn-info" href="#">Agregar Pregunta</a>
                                                                            </div>-->
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <a class="btn btn-danger" href="#" onclick="$('#dialogPantalla').dialog('close');" id="btnRegresar">
                                                    <i class="icon-arrow-left"></i>
                                                    Volver
                                                </a>
                                            </div>

                                            <div class="col-md-6 wizard-actions">
                                                <input type="button" class="btn btn-primary btn-next" id="botonGuardarPregunta" value="Guardar">
                                                <input type="button" class="btn btn-primary btn-next hide" id="botonGuardarPreguntaSimple" value="Guardar">
                                                <input type="button" class="btn btn-primary btn-next hide" id="botonGuardarPreguntaImagen" value="Guardar">
                                                <input type="button" class="btn btn-primary btn-next hide" id="botonGuardarPreguntaDomino" value="Guardar">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div><!-- form -->
                    </div>

                    <div id="otrosDatos" class="tab-pane">
                        <div class="alertDialogBox">
                            <p>
                                Próximamente: Esta área se encuentra en Desarrollo.
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="hide" id="resultDialog"></div>

        </div>
    </div>
    <!-- </div> -->
    <!-- /.col -->
</div>

