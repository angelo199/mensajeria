<?php
/* @var $this PruebaController */
/* @var $model Prueba */
if ($id_decod == '') {
    $this->breadcrumbs = array(
        'Gestión de Prueba' => array('/../prueba'),
        'Administración de Pruebas',
    );
    $this->pageTitle = 'Administración de Pruebas';
}
?>

<div id="msj_exitoso" class="successDialogBox hide">
    <p>

    </p>
</div>

<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Pruebas</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de tipo de pruebas.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/prueba/tipoPrueba/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <!--                        <a type="button" id='btnRegistrarNuevaPrueba' data-last="Finish" class="btn btn-success btn-next btn-sm">-->
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Prueba                       </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'prueba-grid',
                    'dataProvider' => $dataProvider,
                    'filter' => $model,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'summaryText' => 'Mostrando {start}-{end} de {count}',
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "function(){
                            $('.add-modificar').unbind('click');
                            $('.add-modificar').on('click',
                                    function(e) {
                                        e.preventDefault();
                                        var registro_id;
                                         registro_id=$(this).attr('data');

                                         modificar(registro_id);
                                    }
                            );

                           $('.add-inactivar').unbind('click');
                            $('.add-inactivar').on('click',
                                    function(e) {
                                        e.preventDefault();
                                        var registro_id;
                                         registro_id=$(this).attr('data');

                                         inactivar(registro_id);
                                    }
                            );

                            $('.add-activar').unbind('click');
                            $('.add-activar').on('click',
                                    function(e) {
                                        e.preventDefault();
                                        var registro_id;
                                         registro_id=$(this).attr('data');

                                         activar(registro_id);
                                    }
                            );

                            $('.add-consultar').unbind('click');
                            $('.add-consultar').on('click',
                                    function(e) {
                                        e.preventDefault();
                                        var registro_id;
                                         registro_id=$(this).attr('data');

                                         consultar(registro_id);
                                    }
                            );
                                $('#prueba_nombre_form').bind('keyup blur', function() {
                                    keyAlphaNum(this, true, true);// Valida que contenga números, letras y espacios
                                    makeUpper(this);//Convierte las letras en mayusculas
                                });

                                $('#nombre_prueba_grid').bind('keyup blur', function() {
                                    keyAlphaNum(this, true, true);// Valida que contenga números, letras y espacios
                                    makeUpper(this);//Convierte las letras en mayusculas
                                });

                            }",
                    'columns' => array(
                        array(
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                            'htmlOptions' => array(),
                            'filter' => CHtml::textField('Prueba[nombre]', null, array('id' => 'nombre_prueba_grid')),
                        //  'filter' => CHtml::textField('Prueba[nombre]', $model->nombre, array('title' => '')),
                        ),
                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'filter' => array(
                                'A' => 'Activo',
                                'I' => 'Inactivo'
                            ),
                            'value' => array($this, 'estatusPrueba'),
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acciones</center>',
                            'value' => array($this, 'getActionButtons'),
                            'htmlOptions' => array('nowrap' => 'nowrap', 'style' => 'width: 200px'),
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("prueba"); ?>" class="btn btn-danger">
        <i class="icon-arrow-left"></i>
        Volver
    </a>

    <?php
    $control = '2';
    $this->renderPartial('/_acciones', array('control' => $control));
    ?>
</div>
<div id="dialog_registrarNuevaPrueba" class="hide">
    <?php
    $this->renderPartial('_form', array(
        'model' => $model,
        'id_decod' => $id_decod,
        'variable' => $variable
    ));
    ?>
</div>

<div id="dialog_consultar" class="hide">
    <div id="msj_error_consulta" class="errorDialogBox hide">
        <p>

        </p>
    </div>
</div>

<div id="dialog_inactivacion" class="hide">
    <div id="pregunta_inactivar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Inactivar este Registro de Prueba?
        </p>
    </div>
    <div id="msj_error_inactivar" class="errorDialogBox hide">
        <p>

        </p>
    </div>
</div>

<div id="dialog_activacion" class="hide">
    <div id="pregunta_activar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Activar este Registro de Prueba?
        </p>
    </div>
    <div id="msj_error_activar" class="errorDialogBox hide">
        <p>

        </p>
    </div>
</div>
<div id="css_js">
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/prueba.js', CClientScript::POS_END);
    ?>
</div>
<script>
    //$("html, body").animate({scrollTop: 0}, "fast");
</script>