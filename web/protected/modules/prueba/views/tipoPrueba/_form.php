<?php
/* @var $this PruebaController */
/* @var $model Prueba */
if ($variable == 'M') {
    $this->breadcrumbs = array(
        'Gestión de Prueba' => array('/../prueba'),
        'Tipo de Pruebas' => array('/prueba/tipoPrueba'),
        'Modificar Tipo de Prueba'
    );
    $this->pageTitle = 'Modificación de tipo de Pruebas';
} elseif ($variable == 'R') {
    $this->breadcrumbs = array(
        'Gestión de Prueba' => array('/../prueba'),
        'Tipo de Pruebas' => array('/prueba/tipoPrueba'),
        'Registrar Tipo de Prueba'
    );
    $this->pageTitle = 'Registro de tipo de Pruebas';
}
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'prueba_form',
                            'enableAjaxValidation' => false,
                        ));
                        ?>


                        <div id="msj_exitoso" class="successDialogBox hide">
                            <p>

                            </p>
                        </div>
                        <div id="msj_error" class="errorDialogBox hide">
                            <p>

                            </p>
                        </div>

                        <div id="div-result">
                            <?php
                            if ($model->hasErrors()):
                                $this->renderPartial('//errorSumMsg', array('model' => $model));
                            else:
                                ?>
                                <div class="infoDialogBox">
                                    <p class="note">
                                    <ul>
                                        <li>Todos los campos con <span class="required">*</span> son requeridos.</li>
                                        <li><b>Prueba Cualitativa:</b> Son aquellas que no tienen ninguna puntuación o calificación.</li>
                                        <li><b>Prueba Cuantitativa:</b> Son aquellas que tienen puntuación o calificación final.</li>
                                    </ul>
                                    </p>
                                </div>
                            <?php
                            endif;
                            ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <input type="hidden" id="Prueba_id" name="Prueba[id]" value="<?php echo base64_encode($id_decod); ?>">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'nombre'); ?>
                                                            <?php echo $form->textField($model, 'nombre', array('size' => 60, 'maxlength' => 150, 'class' => 'span-12', 'id' => 'prueba_nombre_form')); ?>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'tipo_prueba'); ?>
                                                            <?php echo $form->dropDownList($model, 'tipo_prueba', array('1' => 'Prueba Cualitativa', '2' => 'Prueba Cuantitativa'), array('prompt' => '-SELECCIONE-', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12')); ?>
                                                        </div>
                                                    </div>

                                                    <div class="space-12"></div>
                                                    <div class="col-md-12">
                                                        <div id="instruccion_prueba" class="col-md-12 hide">
                                                            <label class="col-md-12">Instrucciones <span class="required">*</span></label>
                                                            <div id="instruccion_seccion1" class="wysiwyg-editor" style="border:1px solid #ccc;word-wrap:break-word; " contenteditable="true"  required>
                                                            </div>
                                                            <?php
                                                            if ($model->instruccion_prueba) {
                                                                $model->instruccion_prueba = CHtml::decode($model->instruccion_prueba);
                                                            }
                                                            echo $form->textArea($model, 'instruccion_prueba', array('id' => 'instruccion_seccion2', 'name' => 'Prueba[instruccion_prueba]', 'readonly' => 'true', 'class' => 'hide', 'hidden' => 'hidden'));
                                                            ?>
                                                            <?php //echo $form->labelEx($model, 'instruccion_prueba <span class="required">*</span>'); ?>
                                                            <?php //echo $form->textArea($model, 'instruccion_prueba', array('class' => 'span-12', 'id' => 'instruccion_prueba_form')); ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/prueba/tipoPrueba"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button id="btnTipoPrueba" class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div> 
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>

        <?php
        /**
         * Yii::app()->clientScript->registerScriptFile(
         *   Yii::app()->request->baseUrl . '/public/js/modules/PruebaController/prueba/form.js',CClientScript::POS_END
         * );
         */
        ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#prueba_nombre_form').bind('keyup blur', function() {
            keyAlphaNum(this, true, true);// true acepta la ñ y para que sea español
            makeUpper(this);
        });
        var tipo_prueba = $('#Prueba_tipo_prueba').val();
        if (tipo_prueba == 2) {
            $('#instruccion_prueba').removeClass('hide');
        }
//        $('#instruccion_prueba_form').bind('keyup blur', function() {
//            keyText(this, true);
//            makeUpper(this);//Convierte las letras en mayusculas
//        });

        $('#Prueba_tipo_prueba').change(function() {
            var tipo_prueba = $('#Prueba_tipo_prueba').val();
            if (tipo_prueba == 2) {
                $('#instruccion_prueba').removeClass('hide');
                $('#instruccion_prueba_form').val('');
            }
            else if (tipo_prueba == 1) {
                $('#instruccion_prueba').addClass('hide');
                $('#instruccion_prueba_form').val('');

            } else if (tipo_prueba == '') {
                $('#instruccion_prueba').addClass('hide');
                $('#instruccion_prueba_form').val('');
            }
        });

    });

</script>
<script src="/public/js/modules/prueba/jquery.ui.widget.js"></script>
<script src="/public/js/modules/prueba/jquery.iframe-transport.js"></script>
<!---------------------------------------------------------------------------------------->
<script src="/public/js/modules/prueba/prueba.js"></script>
<script src="/public/js/modules/catalogo/prueba.js"></script>
<script src="/public/js/me-elements.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/typeahead-bs2.min.js"></script>
<script src="/public/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/js/jquery.ui.touch-punch.min.js"></script>
<script src="/public/js/markdown/markdown.min.js"></script>
<script src="/public/js/markdown/bootstrap-markdown.min.js"></script>
<script src="/public/js/jquery.hotkeys.min.js"></script>
<script src="/public/js/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/bootbox.min.js"></script>
