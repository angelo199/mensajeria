<?php
/* @var $this NivelController */
/* @var $data Nivel */
?>
<br>

<div class="view" id="consultar_view">

    <div class="tabbable">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#datosGenerales">Datos Generales</a></li>
        </ul>

        <div class="tab-content">

            <div id="datosGenerales" class="tab-pane active">

                <div class="widget-main form">

                    <div class="row">

                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Nombre:</b></label>
                        </div>

                        <div class="col-md-6">
                            <label class="col-md-12" ><?php
                                echo CHtml::encode(($model->nombre != null) ? $model->nombre : "");
                                ?></label>
                        </div>

                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Prueba Cualitativa/Cuantitativa:</b></label>
                        </div>

                        <div class="col-md-6">
                            <label class="col-md-12" ><?php
                                echo CHtml::encode(($model->tipo_prueba != null) ? ($model->tipo_prueba == 1) ? 'Prueba Cualitativa' : 'Prueba Cuantitativa'  : "");
                                ?></label>
                        </div>
                        <?php if (isset($model->tipo_prueba) && $model->tipo_prueba == 2) { ?>
                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Instruciones:</b></label>
                            </div>

                            <div class="col-md-6">

                                <label class="col-md-12" >
                                    <div id="instruccion_seccion_cons" style='border: 1;background: #ffffff; overflow:auto;padding-right: 15px; padding-top: 15px; padding-left: 15px;
                                         padding-bottom: 15px;border-right: #6699CC 0px solid; border-top: #999999 0px solid;border-left: #6699CC 0px solid; border-bottom: #6699CC 0px solid;
                                         scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:300px; left: 28%; top: 300; width: 100%;word-wrap:break-word;'>
                                        <div class="col-md-12" style="border:1px solid #ccc; min-height:100px; word-wrap:break-word;">
                                            <?php echo $model->instruccion_prueba ?>
                                        </div>


                                    </div>
                                </label>
                            </div>
                        <?php } ?>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Creado por:</b></label>
                        </div>

                        <div class="col-md-6">
                            <label class="col-md-12" ><?php
                                echo CHtml::encode((is_object($model->usuarioIni) && $model->usuarioIni != null) ? $model->usuarioIni->nombre : "" . " " . ($model->usuarioIni != null && is_object($model->usuarioIni)) ? $model->usuarioIni->apellido : "");
                                ?></label>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Fecha de Creación:</b></label>
                        </div>

                        <div class="col-md-6">
                            <label class="col-md-12" ><?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime(($model->fecha_ini != null) ? $model->fecha_ini : "")));
                                ?></label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-12" ><b>Estatus:</b></label>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-12" >
                                <?php
                                if ($model->estatus == "A") {
                                    echo "Activo";
                                } else if ($model->estatus == "I") {
                                    echo "Inactivo";
                                }
                                ?>
                            </label>
                        </div>
                    </div>


                    <div class="row">
                        <?php if ($model->usuarioAct) { ?>
                            <div class="col-md-6">
                                <label class="col-md-12" ><b>Modificado por:</b></label>
                            </div>

                            <div class="col-md-6">
                                <label class="col-md-12" >
                                    <?php echo CHtml::encode(($model->usuarioAct != null) ? $model->usuarioAct->nombre : "" . " " . ($model->usuarioAct != null) ? $model->usuarioAct->apellido : ""); ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="row">
                        <?php if ($model->usuarioAct) { ?>
                            <div class="col-md-6">
                                <label class="col-md-12"><b>Fecha de Actualización:</b></label>
                            </div>

                            <div class="col-md-6">
                                <label class="col-md-12">
                                    <?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime(($model->fecha_act != null) ? $model->fecha_act : ""))); ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <?php if ($model->estatus == "I") { ?>
                            <div class="col-md-6">
                                <label class="col-md-12"><b>Inhabilitado el:</b></label>
                            </div>

                            <div class="col-md-6">
                                <label class="col-md-12"><?php echo CHtml::encode(date("d-m-Y H:i:s", strtotime(($model->fecha_elim != null) ? $model->fecha_elim : ""))); ?></label>
                            </div>

                            <?php
                        }
                        ?>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function() {
        //var instrucciones =<?php //echo ($model->instruccion_prueba != null) ? "'" . $model->instruccion_prueba . "'" : "'" . "'";      ?>;
        // $('#instruccion_seccion_cons').html(instrucciones.toString());
    });
</script>