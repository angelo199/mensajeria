<?php

/* @var $this SeccionController */
/* @var $model SeccionPrueba */
$this->breadcrumbs = array(
    'Gestión de Exámenes de Postulación' => array('/../prueba'),
    'Exámenes de Postulación' => array('/../prueba/seccion'),
    'Modificación de Datos',
);
$this->pageTitle = 'Modificación de Datos Generales';
?>
<?php

$this->renderPartial('_form', array(
    'model' => $model,
    'modelPregunta' => $modelPregunta,
    'dataProviderPreguntas' => $dataProviderPreguntas,
    'listaPeriodosAperturados' => $listaPeriodosAperturados,
    'apertura'=>$apertura,
    'apertura_nombre'=> $apertura_nombre,
    'aux' => $aux,
    'formType' => 'edicion',
    'cantidadPreguntas'=>$cantidadPreguntas,
    ));
?>
