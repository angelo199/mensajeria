<?php

/* @var $this SeccionController */
/* @var $model SeccionPrueba */
$this->breadcrumbs = array(
    'Gestión de Exámenes de Postulación' => array('/../prueba'),
    'Exámenes de Postulación' => array('/../prueba/seccion'),
    'Registro de Datos Generales',
);
$this->pageTitle = 'Registro de Datos Generales de Exámen de Postulación';
?>
<?php $this->renderPartial('_form', array('model' => $model, 'listaPeriodosAperturados' => $listaPeriodosAperturados, 'formType' => 'registro',)); ?>
