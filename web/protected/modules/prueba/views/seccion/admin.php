<?php
/* @var $this SeccionController */
/* @var $model SeccionPrueba */
$this->breadcrumbs = array(
    'Gestión de Exámenes de Postulación' => array('/../prueba'),
    'Exámenes de Postulación',
);
$this->pageTitle = 'Administración de Exámenes de Postulación';
?>

<div id="msj_exitoso" class="successDialogBox hide">
    <p>

    </p>
</div>



<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Exámenes de Postulación</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Exámenes de Postulación.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <?php $url = '/prueba/seccion/registro';
                              $url.= empty($apertura)?'':'/apertura/'.$apertura ?>
                        <a href="<?php echo $this->createUrl($url); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Examen </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'seccion-prueba-grid',
                    'dataProvider' => $dataProvider,
                    'filter' => $model,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'summaryText' => 'Mostrando {start}-{end} de {count}',
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "function(){

function verDialogo(message, title, style, clickCallback, reload, buttonsExtra) {
                if (!style)
                style = 'info';

    var buttonsDialog = [
        {
            html: 'Cerrar',
            'class': 'btn btn-xs',
            click: function() {
                $(this).dialog('close');
                if (clickCallback) {
                    clickCallback();
                }
                if (reload) {
                    window.location.reload();
                }
            }
        }
    ];

    if (buttonsExtra)
        buttonsDialog.push(buttonsExtra);

    displayDialogBox('dialog_error', style, message);

    var dialog = $('#dialog_error').removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title:  '<div class='+'widget-header widget-header-small'+'><h4 class='+'smaller'+'><i class='+'fa fa-exclamation-triangle'+'></i> ' + title + ' </h4></div>',
        title_html: true,
        buttons: buttonsDialog
    });
}

$('.editar-datos').unbind('click');
$('.editar-datos').on('click', function(e){
        e.preventDefault();

        var registro_id;
        registro_id= $(this).attr('data');
        var data = { id: registro_id };
        
    
        //return false;
       $.ajax({
                        url: '/prueba/seccion/verificarEstatus',
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(json) {
                            try{
                                var json = jQuery.parseJSON(json);
                               
                                if (json.statusCode == 'success') 
                                {
                                    var mensaje = '<b>Estimado Usuario: La Seccion Seleccionada ya ha sido utilizada para las pruebas Correspondientes, No puede editar este Registro</b>';
                                    var title = 'Edicion de Seccion';
                                    //return false;
                                    verDialogo(mensaje,title);
                                    //alert('no puede editar este registro');
                                }
                                if (json.statusCode == 'error') {
                                    //$('.editar-datos').unbind('click');
                                    //$('.editar-datos').click();
                                    var ruta=json.mensaje;
                                    //alert('puede pasar tranquilamente');
                                    location.href = ruta+'/prueba/seccion/edicion/id/' + registro_id;
                                    
                                }
                            }
                            catch(e){
                                
                            }
                        }
                    }); 
  
       //return false;    
});



                        $('#nombre').bind('keyup blur', function (){
                                keyAlphaNum(this,true,true);
                                makeUpper(this); //Convierte las palabras en mayusculas.
                         });


                         $.mask.definitions['H'] = '[0-1-2]';
        $.mask.definitions['Y'] = '[0|1|2|3]';
        $.mask.definitions['M'] = '[0|1|2|3|4|5]';
        $.mask.definitions['~'] = '[+-]';
        $('#duracion_seccion').mask('H9:M9:M9');



// PARTE QUE ACTUALIZA

    function refrescarGrid() {

    $('#seccion-prueba-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}


                              function inactivar(registro_id) {
    $('#pregunta_inactivar').removeClass('hide');
    $('#msj_error_inactivar').addClass('hide');
    $('#msj_error_inactivar p').html('');

    var dialogInactivar = $('#dialog_inactivacion').removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: '<div> Inactivar Registro de Sección de Prueba</div>',
        title_html: true,
        buttons: [
            {
                html: 'Cancelar',
                'class': 'btn btn-xs',
                click: function() {
                    $(this).dialog('close');
                }
            },
            {
                html: 'Inactivar',
                'class': 'btn btn-danger btn-xs',
                click: function() {
                    var data = {
                        id: registro_id
                    };
                    $.ajax({
                        url: '/prueba/seccion/inactivarSeccionPrueba',
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == 'success') {

                                    refrescarGrid();
                                    dialogInactivar.dialog('close');
                                    $('#msj_error_inactivar').addClass('hide');
                                    $('#msj_error_inactivar p').html('');
                                    $('#msj_exitoso').removeClass('hide');
                                    $('#msj_exitoso p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }

                                if (json.statusCode == 'error') {

                                    $('#pregunta_inactivar').addClass('hide');
                                    $('#msj_exitoso').addClass('hide');
                                    $('#msj_exitoso p').html('');
                                    $('#msj_error_inactivar').removeClass('hide');
                                    $('#msj_error_inactivar p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $('#dialog_inactivacion').show();
}

function activar(registro_id) {

    $('#pregunta_activar').removeClass('hide');
    $('#msj_error_activar').addClass('hide');
    $('#msj_error_activar p').html('');

    var dialogActivar = $('#dialog_activacion').removeClass('hide').dialog({
        modal: true,
        width: '600px',
        draggable: false,
        resizable: false,
        title: '<div> Activar Registro de la Sección de Prueba</div>',
        title_html: true,
        buttons: [
            {
                html: 'Cancelar',
                'class': 'btn btn-xs',
                click: function() {
                    $(this).dialog('close');
                }
            },
            {
                html: 'Activar',
                'class': 'btn btn-success btn-xs',
                click: function() {
                    var data = {
                        id: registro_id
                    }

                    $.ajax({
                        url: '/prueba/seccion/activarSeccionPrueba',
                        data: data,
                        dataType: 'html',
                        type: 'get',
                        success: function(resp, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);

                                if (json.statusCode == 'success') {

                                    refrescarGrid();
                                    dialogActivar.dialog('close');
                                    $('#msj_error_activar').addClass('hide');
                                    $('#msj_error_activar p').html('');
                                    $('#msj_exitoso').removeClass('hide');
                                    $('#msj_exitoso p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }

                                if (json.statusCode == 'error') {

                                    $('#pregunta_activar').addClass('hide');
                                    $('#msj_exitoso').addClass('hide');
                                    $('#msj_exitoso p').html('');
                                    $('#msj_error_activar').removeClass('hide');
                                    $('#msj_error_activar p').html(json.mensaje);
                                    $('html, body').animate({scrollTop: 0}, 'fast');
                                }
                            }
                            catch (e) {

                            }
                        }
                    })
                }
            }
        ]
    });
    $('#dialog_activacion').show();
}


$('.add-activar').unbind('click');
$('.add-activar').on('click', function(e) {

    e.preventDefault();

    var registro_id;
    registro_id = $(this).attr('data');

    activar(registro_id);

});


// FINAL QUE ACTUALIZA

$('.add-inactivar').unbind('click');
                                $('.add-inactivar').on('click', function(e) {
                                    e.preventDefault();

                                    var registro_id;
                                    registro_id = $(this).attr('data');

                                    inactivar(registro_id);
                                });
 }",
                    'columns' => array(
                        /* array(
                          'header' => '<center>Id</center>',
                          'name' => 'id',
                          'htmlOptions' => array(),
                          //'filter' => CHtml::textField('SeccionPrueba[id]', $model->id, array('title' => '',)),
                          ), */
                        array(
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                            'type' => 'raw',
                            'htmlOptions' => array(),
                            'filter' => CHtml::textField('SeccionPrueba[nombre]', null, array('id' => 'nombre')),
                            'value'  => '($data->estatus=="A") ? $data->nombre : "<strike>".$data->nombre."</strike>"'
                        //'filter' => CHtml::textField('SeccionPrueba[nombre]', $model->nombre, array('title' => '',)),
                        ),
                        array(
                            'header' => '<center>Período</center>',
                            'name' => 'prueba_id',
                            'value' => '(is_object($data->periodoApertura))?$data->periodoApertura->nombre_clave.": ".Helper::toAppDate($data->periodoApertura->fecha_inicio)." a ".Helper::toAppDate($data->periodoApertura->fecha_final):""',
                            'filter' => CHtml::listData($periodosAperturados , 'id', 'nombre_clave'),
                        ),
                        array(
                            'name' => 'duracion_seccion',
                            'type' => 'raw',
                            'filter' => CHtml::textField('SeccionPrueba[duracion_seccion]', null, array('id' => 'duracion_seccion')),
                            'value' => '($data->estatus=="A") ? $data->duracion_seccion : "<strike>".$data->duracion_seccion."<strike>"'
                        ),
                        array(
                            'header' => '<center>Nivel de la Prueba</center>',
                            'name' => 'nivel_cargo_id',
                            'type' => 'raw',
                            'value' => '(TalentoHumanoController::getTextData("nivelCargo", $data->nivel_cargo_id))',
                            'filter' => CHtml::listData(CNivelCargo::getData() , 'id', 'nombre'),
                        ),
                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            //'htmlOptions' => array(),
                            'filter' => array(
                                'A' => 'Activo',
                                'I' => 'Inactivo'
                            ),
                            'value' => array($this, 'estatusPrueba'),
                        //'filter' => CHtml::textField('SeccionPrueba[estatus]', $model->estatus, array('title' => '',)),
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acciones</center>',
                            'value' => array($this, 'getActionButtons'),
                            'htmlOptions' => array('nowrap' => 'nowrap'),
                        ),
                    ),
                ));
                ?>

                <div class="row">
                    <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("prueba"); ?>" class="btn btn-danger">
                        <i class="icon-arrow-left"></i>
                        Volver
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="css_js">
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/seccion.js', CClientScript::POS_END); ?>
</div>
<div id="dialog_inactivacion" class="hide">
    <div id="pregunta_inactivar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Inactivar este Registro de Sección de Prueba?
        </p>
    </div>
    <div id="msj_error_inactivar" class="errorDialogBox hide">
        <p>

        </p>
    </div>
</div>

<div id="dialog_activacion" class="hide">
    <div id="pregunta_activar" class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas Seguro(a) que Desea Activar este Registro de Sección de Prueba?
        </p>
    </div>
    <div id="msj_error_activar" class="errorDialogBox hide">
        <p>

        </p>
    </div>
</div>
<script>
    $("html, body").animate({scrollTop: 0}, "fast");
</script>
<div id="dialog_error" class="hide"></div>
