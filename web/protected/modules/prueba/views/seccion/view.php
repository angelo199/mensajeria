<?php
/* @var $this SeccionController */
/* @var $model SeccionPrueba */
/* @var $form CActiveForm */
$this->breadcrumbs = array(
    'Gestión de Prueba' => array('/../prueba'),
    'Sección Pruebas' => array('/../prueba/seccion'),
    'Actualización',
);
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'seccion-prueba-form',
                            'htmlOptions' => array('onSubmit' => 'return false;',), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation' => false,
                        ));
                        ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <?php echo CHtml::label('Tipo de Prueba', ''); ?><br>
                                                            <?php echo CHtml::textField($model->prueba, 'nombre', array('class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo CHtml::label('Nombre de la Sección', ''); ?><br>
                                                            <?php echo $form->textField($model, 'nombre', array('size' => 60, 'maxlength' => 150, 'class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                        </div>


                                                        <div class="col-md-4">
                                                            <?php echo CHtml::label('Duración de la Sección de Prueba', ''); ?>
                                                            <?php echo $form->textField($model, 'duracion_seccion', array('class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                        </div>



                                                    </div>

                                                    <div class="space-6"></div>
                                                    <?php /*
                                                      <div class="col-md-12">

                                                      <div class="col-md-4">
                                                      <?php echo $form->labelEx($model,'usuario_elim_id'); ?>
                                                      <?php echo $form->textField($model,'usuario_elim_id', array('class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                      </div>


                                                      <div class="col-md-4">
                                                      <?php echo $form->labelEx($model,'fecha_elim'); ?>
                                                      <?php echo $form->textField($model,'fecha_elim',array('size'=>6, 'maxlength'=>6, 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                      </div>


                                                      <div class="col-md-4">
                                                      <?php echo $form->labelEx($model,'estatus'); ?>
                                                      <?php echo $form->dropDownList($model, 'estatus', array('A'=>'Activo', 'I'=>'Inactivo', 'E'=>'Eliminado'), array('prompt'=>'- - -', 'class' => 'span-12', 'disabled'=>'disabled',)); ?>
                                                      </div>

                                                      </div> */ ?>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">



                                                        <div class="col-md-4">
                                                            <?php echo CHtml::label('Estatus', ''); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('prompt' => '- - -', 'class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo CHtml::label('¿Tiene Sub-secciones?', ''); ?>
                                                            <?php echo CHtml::textField('', $model->cantidad_sub_seccion != null ? 'Si' : 'No', array('class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                        </div>

                                                        <?php if (empty($model->cantidad_sub_seccion)) { ?>
                                                            <div class="col-md-4">
                                                                <?php echo $form->labelEx($model, 'tipo_pregunta'); ?><br>
                                                                <?php echo $form->dropDownList($model, 'tipo_pregunta', array('T' => 'Selección Simple', 'D' => 'Selección Simple (imagen)', 'I' => 'Domino', 'E' => 'Eliminado'), array('size' => 1, 'maxlength' => 1, 'class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-4">
                                                                <?php echo $form->labelEx($model, 'cantidad_sub_seccion'); ?>
                                                                <?php echo $form->textField($model, 'cantidad_sub_seccion', array('class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                            </div>
                                                        <?php } ?>

                                                    </div>

                                                    <div class="space-6"></div>

                                                    <div class="col-md-12">

                                                        <?php
                                                        if ($model->cantidad_sub_seccion == null) {
                                                            ?>


                                                            <div class="col-md-4">
                                                                <?php echo CHtml::label('Cantidad de Respuestas por Pregunta', ''); ?>
                                                                <?php echo $form->textField($model, 'cantidad_respuesta', array('class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                            </div>

                                                            <div class="col-md-8">

                                                                <label class="col-md-12">Instrucciones <span class="required">*</span></label>
                                                                <div id='instruccion_seccion' style='border: 1;background: #ffffff; overflow:auto;padding-right: 15px; padding-top: 15px; padding-left: 15px;
                                                                     padding-bottom: 15px;border-right: #cfd8dc 1px solid; border-top: #cfd8dc 1px solid;border-left: #cfd8dc 1px solid; border-bottom: #cfd8dc 1px solid;
                                                                     scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:100%; left: 28%; top: 300; width: 100%;word-wrap:break-word;'>
                                                                     <?php echo $model->instruccion_seccion ?>
                                                                     <?php //echo CHtml::textArea('instruccion_seccion', array('id' => 'instruccion_seccion', 'rows' => 2, 'cols' => 24, 'class' => 'span-12', 'disabled' => 'disabled',));  ?>
                                                                </div>
                                                            </div>

                                                        <?php } ?>




                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/prueba/seccion"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
<script>

    $(document).ready(function() {
//var instrucciones=<?php // echo ($model->instruccion_seccion != null) ? "'" . $model->instruccion_seccion . "'" : "'" . "'";    ?>;
//$('#instruccion_seccion').html(instrucciones);
    });
</script>