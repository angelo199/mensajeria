<?php
/* @var $this SeccionController */
/* @var $model SeccionPrueba */
/* @var $form CActiveForm */
?>
<div class="col-xs-12">
    <div class="row-fluid">
        <div class="tabbable">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <li><a href="#preguntas" data-toggle="tab">Preguntas y Respuestas</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'seccion-prueba-form',
                            'htmlOptions' => array('data-form-type' => $formType, 'name' => 'seccion-prueba-form'), // for inset effect
                            // Please note: When you enable ajax validation, make sure the corresponding
                            // controller action is handling ajax validation correctly.
                            // There is a call to performAjaxValidation() commented in generated controller code.
                            // See class documentation of CActiveForm for details on this.
                            'enableAjaxValidation' => false,
                        ));
                        ?>
                        <div id="div-result">
                            <?php
                            if ($model->hasErrors()):
                                $this->renderPartial('//errorSumMsg', array('model' => $model));
                            else:
                                ?>
                                <?php if (Yii::app()->user->hasFlash('exitoso')) { ?>
                                    <div class="successDialogBox"><p><?php echo Yii::app()->user->getFlash('exitoso'); ?></p></div>
                                <?php } elseif (Yii::app()->user->hasFlash('error')) { ?>
                                    <div class="errorDialogBox"><p><?php echo Yii::app()->user->getFlash('error'); ?></p></div>
                                <?php } else { ?>
                                    <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                                <?php } ?>
                            <?php
                            endif;
                            ?>

                            <div id="dialog_error" class="hide"></div>
                        </div>
                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'prueba_id'); ?>
                                                            <?php if(empty($apertura)): ?>
                                                                <?php echo $form->dropDownList($model, 'prueba_id', $listaPeriodosAperturados, array('prompt' => '-- Seleccione --', 'class' => 'span-12', "required" => "required",)); ?>
                                                            <?php else: ?>
                                                                <?php foreach($apertura_nombre as $nombre):  ?>
                                                            <input type="text" value="<?php echo $nombre; ?>" class="span-12" readonly="readOnly"  />
                                                                <?php endforeach; ?>
                                                                <input type="hidden" name="SeccionPrueba[prueba_id]" value="<?php echo base64_decode($apertura) ?>" class="span-12" />
                                                            <?php endif ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'nivel_cargo_id', array('class'=>'span-12')); ?>
                                                            <?php echo $form->dropDownList($model, 'nivel_cargo_id', CHtml::listData(CNivelCargo::getData(), 'id', 'nombre' ), array('prompt' => '-- Seleccione --', 'class' => 'span-12', "required" => "required",)); ?>
                                                        </div>
                                                    </div>
                                                    <div class="space-10"></div>
                                                    <div class="col-md-12">
                                                        <div class="col-md-12">
                                                            <?php echo $form->labelEx($model, 'nombre'); ?><br>
                                                            <?php echo $form->textField($model, 'nombre', array('size' => 60, 'maxlength' => 150, 'class' => 'span-12', "required" => "required",)); ?>
                                                        </div>
                                                    </div>
                                                    <br/><br/><br/><br/><br/><br/>
                                                    <div class="space-10"></div>
                                                    <div class="col-md-12">
                                                        
                                                        <div class="col-md-4" id="tipo_pregunta">
                                                                 <?php echo $form->labelEx($model, 'tipo_pregunta'); ?>
                                                                 <?php echo $form->dropDownList($model, 'tipo_pregunta', array('T' => 'Selección Simple'), array('empty' => '--Seleccione--', 'size' => 1, 'maxlength' => 1, 'class' => 'span-12', "required" => "required")); ?>
                                                        </div>

                                                        <div class="col-md-4" id="cantidad_respuesta">
                                                                <?php echo $form->labelEx($model, 'cantidad_respuesta'); ?>
                                                                 <?php if (isset($aux) && $model->tipo_pregunta == 'D') {
                                                                     $readOnly = 'readOnly';
                                                                 } else
                                                                     $readOnly = '';
                                                                 echo $form->textField($model, 'cantidad_respuesta', array('class' => 'span-12', 'maxlength' => 3, $readOnly => $readOnly));
                                                                 ?>
                                                        </div>
                                                        <div class="col-md-4" id="duracion_seccion">
                                                            <?php echo $form->labelEx($model, 'duracion_seccion'); ?>
                                                            <?php echo $form->textField($model, 'duracion_seccion', array('class' => 'span-12', "required" => "required", 'placeholder'=>'HH:MM:SS')); ?>
                                                        </div>
                                                    </div>
                                                    <div class="space-10"></div>
                                                    <div class="col-md-12">
                                                        <div  id="instruccion_seccion" class="col-md-12">
                                                            <label class="col-md-12"><center>Instrucciones de la Prueba <span class="required">*</span></center></label>
                                                            <div id="instruccion_seccion1" class="wysiwyg-editor" style="border:1px solid #ccc;word-wrap:break-word;" contenteditable="true"  required>
                                                            </div>
                                                            <?php
                                                            if ($model->instruccion_seccion) {
                                                                $model->instruccion_seccion = CHtml::decode($model->instruccion_seccion);
                                                            }
                                                            echo $form->textArea($model, 'instruccion_seccion', array('id' => 'instruccion_seccion2', 'name' => 'SeccionPrueba[instruccion_seccion2]', 'readonly' => 'true', 'class' => 'hide', 'hidden' => 'hidden'));
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php echo $form->hiddenField($model, 'id'); ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">

                                    <div class="col-md-6">
                                        <?php $link = '/prueba/seccion/admin'; $link.= empty($apertura)?'':'/apertura/'.$apertura; ?>
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl($link); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit" >
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>
                <div class="tab-pane" id="preguntas">
                    <?php if($formType=='registro'): ?>
                    <div class="infoDialogBox">
                        <p>
                            Debe primeramente Registrar los Datos Generales del Exámen de Postulación antes de realizar el Registro de Preguntas y Respuestas.
                        </p>
                    </div>
                    <?php else: ?>
                    <?php $this->renderPartial('_adminPreguntas', array(
                        'model' => $model,
                        'modelPregunta' => $modelPregunta,
                        'dataProviderPreguntas' => $dataProviderPreguntas,
                        'listaPeriodosAperturados' => $listaPeriodosAperturados,
                        'apertura'=>$apertura,
                        'apertura_nombre'=> $apertura_nombre,
                        'aux' => $aux,
                        'formType' => 'edicion'
                    ));
                    ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="resultDialog" class="hide"></div>
        <div id="css_js">
            <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/seccion.js', CClientScript::POS_END); ?>
        </div>
    </div>
</div>
<script src="/public/js/modules/prueba/jquery.ui.widget.js"></script>

<!---------------------------------------------------------------------------------------->
<script src="/public/js/modules/prueba/prueba.js"></script>
<script src="/public/js/lib/me-elements.min.js"></script>
<script src="/public/js/lib/bootstrap-wysiwyg.min.js"></script>
<script src="/public/js/lib/typeahead-bs2.min.js"></script>
<script src="/public/js/lib/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/public/js/lib/jquery.ui.touch-punch.min.js"></script>
<script src="/public/js/lib/markdown/markdown.min.js"></script>
<script src="/public/js/lib/markdown/bootstrap-markdown.min.js"></script>
<script src="/public/js/lib/jquery.hotkeys.min.js"></script>
<script src="/public/js/lib/bootbox.min.js"></script>
