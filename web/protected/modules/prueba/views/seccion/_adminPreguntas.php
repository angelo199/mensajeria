<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Preguntas</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Preguntas.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/prueba/pregunta/registro/per/".base64_encode($apertura).'/secc/'.base64_encode($model->id)); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Pregunta
                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

                <?php

                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'pregunta-grid',
                    'dataProvider' => $dataProviderPreguntas,
                    // 'filter' => $modelPregunta,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'summaryText' => 'Mostrando {start}-{end} de {count}',
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "
                function(){
                    $('#Pregunta_ponderacion').bind('keyup blur', function() {
                        keyNum(this, false);
                    });
                }",
                    'columns' => array(
                        array(
                            'header' => '<center>Sección</center>',
                            'name' => 'seccion_prueba_id',
                            'type' => 'raw',
                            'value' => '(is_object($data->seccionPrueba) && isset($data->seccionPrueba->nombre))? ($data->estatus=="A")? $data->seccionPrueba->nombre: "<strike>".$data->seccionPrueba->nombre."</strike>" : ""',
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                            'value' => array($this, 'nombrePregunta'),
                            'htmlOptions' => array('nowrap' => 'nowrap'),
                            'filter' => false,
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Ponderación</center>',
                            'name' => 'ponderacion',
                            'value' => '(isset($data->ponderacion))? ($data->estatus=="A")? $data->ponderacion: "<strike>".$data->ponderacion."</strike>" : ""',
                            'filter' => CHtml::textField('Pregunta[ponderacion_cgw]', null, array('maxlength' => 10)),
                            'htmlOptions' => array('nowrap' => 'nowrap'),
                        ),
                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'filter' => array(
                                'A' => 'Activo',
                                'I' => 'Inactivo'
                            ),
                            'value' => array($this, 'estatus'),
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acción</center>',
                            'value' => array($this, 'getActionButtonsPreguntas'),
                            'htmlOptions' => array('nowrap' => 'nowrap'),
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>


<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogPantallaConsultar" class="hide"></div>
<div id="dialogPantallaNotificacion" class="hide"></div>
<div id="dialogPantallaEliminar" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas seguro de Inactivar esta Pregunta?
        </p>
    </div>
</div>

<div id="dialogPantallaActivacion" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas seguro que desea Activar esta Pregunta?
        </p>
    </div>
</div>

<div id="dialog_error" class="hide"></div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/prueba/pregunta.js', CClientScript::POS_END);
?>
<script>
    $('#Pregunta_ponderacion_cgw').bind('keyup blur', function() {
        keyNum(this, false);
    });
</script>

<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/css/jquery-ui-1.10.3.custom.min.css');
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery-ui-1.10.3.custom.min.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.upload/js/jquery.fileupload.js', CClientScript::POS_END);
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.upload/css/jquery.fileupload.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.upload/css/jquery.fileupload-ui.css');
//Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/public/js/jquery.upload/css/jquery.fileupload-ui.css');
?>
