<div class="col-xs-12">

    <div class="row-fluid">

        <div id="reloj" class="col-md-12">
            <div class="col-md-6"></div>
            <div class="col-md-6 align-left">
                <div class="clock" style="padding-left: 20%; margin-bottom: 0em;"></div>
                <div class="message"></div>
            </div>
        </div>
        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#prueba_ini" data-toggle="tab"><?php echo $nombrePrueba ?></a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="prueba_ini">
                    <div class="form">

                        <form id="prueba_asp_form" method="post" action="/mostrarPruebaAsp">
                            <!--Mensaje de error. -->
                            <div id="msj_error" class="errorDialogBox hide">
                                <p>

                                </p>
                            </div>
                            <div class="panel panel-info hide" id="msj_culminacion_prueba">
                                <div class="panel-heading">
                                    <h2 class="panel-title center blue" style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                                        <b>
                                            <i>La Prueba Ha Culminado</i>
                                        </b>
                                    </h2>
                                </div>
                                <div class="panel-body">
                                    <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                                        <b>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            La Academia militar de venezuela les agradece por presentarse ante esta institución para optar por pertenecer y servir a este pais.
                                        </b>
                                    </p>
                                </div>
                            </div>
                            <!--Fin mensajes. -->
                            <?php
                            if (isset($datosPrueba)) {
                                $numero_respuesta = 0;
                                $cantidad_respuesta = 0;
                                $pregunta = 0;
                                $arregloSeccion = array();
                                foreach ($datosPrueba as $key => $value) {
                                    $y = $key + 1;

                                    if ($numero_respuesta == $cantidad_respuesta) {
                                        $numero_respuesta = 0;
                                    }
                                    if ($numero_respuesta == 0) {
                                        $pregunta = $pregunta + 1;
                                        $tipo_pregunta_seccion = $value['tipo_pregunta'];
                                        $tipo_pregunta_sub_seccion = $value['tipo_pregunta_sub_seccion'];
                                        if (isset($value['cantidad_respuesta']) && $value['cantidad_respuesta'] != null) {
                                            if ($tipo_pregunta_seccion == 'I') {
                                                $cantidad_respuesta = 1; //Esto indica que solo va mostrar un select para seleccionar una respuesta.
                                                $select_imagen = $value['cantidad_respuesta'];
                                            } else {
                                                $cantidad_respuesta = $value['cantidad_respuesta'];
                                            }
                                        } else {
                                            if ($tipo_pregunta_sub_seccion == 'I') {
                                                $cantidad_respuesta = 1; //Esto indica que solo va mostrar un select para seleccionar una respuesta.
                                                $select_imagen = $value['sub_cantidad_respuesta'];
                                            } else {
                                                $cantidad_respuesta = $value['sub_cantidad_respuesta'];
                                            }
                                        }
                                    }

                                    $nombre_seccion = $value['nombre_seccion'];
                                    $nombre_sub_seccion = $value['nombre_sub_seccion'];
                                    $seccion_id = $value['seccion_id'];
                                    $sub_seccion_id = $value['sub_seccion_id'];
                                    $pregunta_id = $value['pregunta_id'];
                                    if ($numero_respuesta == 0) {

                                        if ($value['sub_seccion_id'] != null) {
                                            $controlPreguntasNueva[] = array(
                                                'seccion_id' => $seccion_id,
                                                'nombre_seccion' => $nombre_seccion,
                                                'nombre_sub_seccion' => $nombre_sub_seccion,
                                                'sub_seccion_id' => $sub_seccion_id,
                                                'pregunta' => $pregunta,
                                                'pregunta_id' => $pregunta_id);
                                        } else {
                                            $sub_seccion_id = null;
                                            $nombre_sub_seccion = '';
                                            $controlPreguntasNueva[] = array(
                                                'seccion_id' => $seccion_id,
                                                'nombre_seccion' => $nombre_seccion,
                                                'nombre_sub_seccion' => $nombre_sub_seccion,
                                                'sub_seccion_id' => $sub_seccion_id,
                                                'pregunta' => $pregunta,
                                                'pregunta_id' => $pregunta_id);
                                        }

                                        //  var_dump($controlPreguntasNueva);
//                                        echo '<b>$controlPreguntasNueva</b><br>';
                                    }

                                    if ($numero_respuesta <= $cantidad_respuesta) {
                                        $numero_respuesta = $numero_respuesta + 1;
                                    }
                                }
                            }
                            ?>
                            <?php
                            if (isset($datosPrueba)) {
                                $numero_respuesta = 0;
                                $cantidad_respuesta = 0;
                                $pregunta = 0;
                                $i = 0;
                                $volver_pregunta = 0;
                                $select_imagen = 0;
                                $opcion = 1;
                                $tipo_pregunta_seccion = '';
                                $tipo_pregunta_sub_seccion = '';
                                //      var_dump($datosPrueba);
                                foreach ($datosPrueba as $key => $value) {
                                    $prueba_id = base64_encode($value['prueba_id']);
                                    $nombre_prueba = $value['nombre_prueba'];
                                    if ($numero_respuesta == $cantidad_respuesta) {
                                        $numero_respuesta = 0;
                                        $opcion = 1;
                                    }
                                    if ($numero_respuesta == 0) {
                                        $i = $i + 1;
                                    }
                                    if ($numero_respuesta == 0) {
                                        $pregunta = $pregunta + 1;
                                        $tipo_pregunta_seccion = $value['tipo_pregunta'];
                                        $tipo_pregunta_sub_seccion = $value['tipo_pregunta_sub_seccion'];
                                        if (isset($value['cantidad_respuesta']) && $value['cantidad_respuesta'] != null) {
                                            if ($tipo_pregunta_seccion == 'I') {
                                                $cantidad_respuesta = 1; //Esto indica que solo va mostrar un select para seleccionar una respuesta.
                                                $select_imagen = $value['cantidad_respuesta'];
                                            } else {
                                                $cantidad_respuesta = $value['cantidad_respuesta'];
                                            }
                                        } else {
                                            if ($tipo_pregunta_sub_seccion == 'I') {
                                                $cantidad_respuesta = 1; //Esto indica que solo va mostrar un select para seleccionar una respuesta.
                                                $select_imagen = $value['sub_cantidad_respuesta'];
                                            } else {
                                                $cantidad_respuesta = $value['sub_cantidad_respuesta'];
                                            }
                                        }
                                    }
                                    //    var_dump($tipo_pregunta_seccion . ' $tipo_pregunta_seccion');
                                    //    var_dump($tipo_pregunta_sub_seccion . ' $tipo_pregunta_sub_seccion');
                                    $resultado_primera_respuestaT = (isset($datosPreguntaRespondida[0])) ? (string) $datosPreguntaRespondida[0]['respuesta'] : "";
                                    ?>

                                    <?php
                                    if ($numero_respuesta == 0) {

                                        if ($pregunta > 1) {
                                            $hide_principal = 'hide';
                                        } else {
                                            $hide_principal = '';
                                        }
                                        ?>
                                        <div  id="div-mostrar-prueba_<?php echo $pregunta; ?>" class="mostrar <?php echo $hide_principal; ?>" data="<?php echo $pregunta; ?>">

                                            <div class="widget-box">
                                                <div class="widget-header">
                                                    <h5><b>Instrucciones de la <?php echo ($value['sub_seccion_id'] != null) ? 'sub-sección: ' . $value['nombre_sub_seccion'] : 'sección: ' . $value['nombre_seccion']; ?></b></h5>

                                                    <div class="widget-toolbar">
                                                        <a data-action="collapse" href="#">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="widget-body">
                                                    <div class="widget-body-inner">
                                                        <div class="widget-main">
                                                            <div class="widget-main form">
                                                                <div class="row">
                                                                    <div class="col-md-12 panel panel-info" style="word-wrap:break-word;">
                                                                        <p style="text-align: justify;font-size:12px;font-family: 'Helvetica';">
                                                                            <br>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <?php
                                                                            if ($value['sub_seccion_id'] != null) {
                                                                                echo $value['instruccion_sub_seccion'];
                                                                            } else {
                                                                                echo $value['instruccion_seccion'];
                                                                            }
                                                                            ?>
                                                                            <br>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="widget-box">
                                                <div class="widget-header">
                                                    <h5>Sección: <b><?php echo $value['nombre_seccion']; ?></b></h5>

                                                    <div class="widget-toolbar">
                                                        <a data-action="collapse" href="#">
                                                            <i class="icon-chevron-up"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="widget-body">
                                                    <div class="widget-body-inner">
                                                        <div class="widget-main">
                                                            <div class="widget-main form">
                                                                <div class="row">
                                                                    <div class="col-md-12">

                                                                        <div class="col-md-4">
                                                                            <div style='border: 1;background: #ffffff; overflow:auto;padding-right: 15px; padding-top: 15px; padding-left: 15px;
                                                                                 padding-bottom: 15px;border-right: #6699CC 0px solid; border-top: #999999 0px solid;border-left: #6699CC 0px solid; border-bottom: #6699CC 0px solid;
                                                                                 scrollbar-arrow-color : #999999; scrollbar-face-color : #666666;scrollbar-track-color :#3333333 ;height:300px; left: 28%; top: 300; width: 100%;word-wrap:break-word;'>
                                                                                <div class="list-group">
                                                                                    <a class="list-group-item disabled">
                                                                                        <h4 class="list-group-item-heading" style="text-align:center"><u>Información</u></h4>
                                                                                        <p class="list-group-item-text" style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                                                                                            Las Preguntas de color verde indican que ya fueron respondidas
                                                                                            y las que estan en negro no han sido respondidas.

                                                                                        </p>
                                                                                    </a>
                                                                                    <?php
                                                                                    for ($z = 0; $z < count($controlPreguntasNueva); $z++) {

                                                                                        //  foreach ($controlPreguntasNueva as $z => $datos) {
                                                                                        if ($z == 0) {
                                                                                            ?>
                                                                                            <a class="list-group-item active">
                                                                                                <?php echo $controlPreguntasNueva[$z]['nombre_seccion']; ?>
                                                                                            </a>
                                                                                            <?php
                                                                                            if (isset($resultadosGuardados[$z]) && $resultadosGuardados[$z]['id'] == $controlPreguntasNueva[$z]['pregunta_id']) {
                                                                                                $marcar = "style='color:green' listo='listo'";
                                                                                            } else {
                                                                                                $marcar = '';
                                                                                            }
                                                                                            ?>
                                                                                            <?php
                                                                                            if ($controlPreguntasNueva[$z]['sub_seccion_id'] != '') {
                                                                                                ?>
                                                                                                <a  class="list-group-item active">
                                                                                                    <?php echo $controlPreguntasNueva[$z]['nombre_sub_seccion']; ?>
                                                                                                </a>

                                                                                                <a class="list-group-item cantidadPregunta_<?php echo $pregunta; ?> pregControl preguntaControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" data="<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" <?php echo $marcar; ?> id="pregControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>">Pregunta <?php echo $controlPreguntasNueva[$z]['pregunta']; ?></a>
                                                                                            <?php } else { ?>
                                                                                                <a  class="list-group-item cantidadPregunta_<?php echo $pregunta; ?> pregControl preguntaControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" data="<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" <?php echo $marcar; ?> id="pregControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>">Pregunta <?php echo $controlPreguntasNueva[$z]['pregunta']; ?></a>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            ?>
                                                                                            <?php
                                                                                            if ($controlPreguntasNueva[$z]['seccion_id'] != $controlPreguntasNueva[$z - 1]['seccion_id']) {
                                                                                                ?>
                                                                                                <a class="list-group-item active">
                                                                                                    <?php
                                                                                                    echo $controlPreguntasNueva[$z]['nombre_seccion'];
                                                                                                    ?>
                                                                                                </a>
                                                                                            <?php } ?>
                                                                                            <?php
                                                                                            if (isset($resultadosGuardados[$z]) && $resultadosGuardados[$z]['id'] == $controlPreguntasNueva[$z]['pregunta_id']) {
                                                                                                $marcar = "style='color:green' listo='listo'";
                                                                                            } else {
                                                                                                $marcar = '';
                                                                                            }
                                                                                            ?>
                                                                                            <?php
                                                                                            if ($controlPreguntasNueva[$z]['sub_seccion_id'] != '') {
                                                                                                if ($controlPreguntasNueva[$z]['sub_seccion_id'] != $controlPreguntasNueva[$z - 1]['sub_seccion_id']) {
                                                                                                    ?>
                                                                                                    <a  class="list-group-item btn-info" style="color: #ffffff">
                                                                                                        <?php
                                                                                                        echo $controlPreguntasNueva[$z]['nombre_sub_seccion'];
                                                                                                        ?>
                                                                                                    </a>
                                                                                                <?php } ?>
                                                                                                <a class="list-group-item cantidadPregunta_<?php echo $pregunta; ?> pregControl preguntaControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" data="<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" <?php echo $marcar; ?>  id="pregControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>">Pregunta <?php echo $controlPreguntasNueva[$z]['pregunta']; ?></a>
                                                                                            <?php } else {
                                                                                                ?>
                                                                                                <a  class="list-group-item cantidadPregunta_<?php echo $pregunta; ?> pregControl preguntaControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" data="<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>" <?php echo $marcar; ?>  id="pregControl_<?php echo $controlPreguntasNueva[$z]['pregunta']; ?>">Pregunta <?php echo $controlPreguntasNueva[$z]['pregunta']; ?></a>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    <?php } ?>

                                                                    <!--Inserte esto-->
                                                                    <?php if ($value['sub_seccion_id'] != null && $numero_respuesta == 0) { ?>
                                                                        <?php if ($numero_respuesta == 0) { ?>
                                                                            <div class="col-md-8 widget-box">
                                                                                <div class="widget-header">
                                                                                    <h5>Sub-Sección: <b><?php echo $value['nombre_sub_seccion']; ?></b></h5>

                                                                                    <div class="widget-toolbar">
                                                                                        <a data-action="collapse" href="#">
                                                                                            <i class="icon-chevron-up"></i>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="widget-body">
                                                                                    <div class="widget-body-inner">
                                                                                        <div class="widget-main">
                                                                                            <div class="widget-main form">
                                                                                                <div class="row">
                                                                                                    <div class="col-md-12">
                                                                                                    <?php } ?>
                                                                                                    <!--Fin-->
                                                                                                <?php } ?>

                                                                                                <?php if ($value['sub_seccion_id'] != null) { ?>


                                                                                                    <?php if ($numero_respuesta == 0) { ?>
                                                                                                        <div id="pregunta_<?php echo $pregunta; ?>" class="col-md-12">
                                                                                                            <p class="align-center">
                                                                                                                <b>
                                                                                                                    <?php if ($tipo_pregunta_sub_seccion == 'T') { ?>
                                                                                                                        Pregunta <?php echo $pregunta; ?>)- <?php echo '¿' . $value['nombre_pregunta'] . '?'; ?>
                                                                                                                    <?php } ?>
                                                                                                                    <?php if ($tipo_pregunta_sub_seccion == 'D') { ?>
                                                                                                                        Pregunta <?php echo $pregunta; ?>)- <img style="width:180px;height:180px;" src="<?php echo "/public/prueba/Domino/" . $value['nombre_pregunta']; ?>" >
                                                                                                                    <?php } ?>
                                                                                                                    <?php if ($tipo_pregunta_sub_seccion == 'I') { ?>
                                                                                                                        Pregunta <?php echo $pregunta; ?>)- <img style="width:400px;height:200px;" src="<?php echo "/public/prueba/Imagen/" . $value['nombre_pregunta']; ?>" >
                                                                                                                    <?php } ?>
                                                                                                                    <input id="pregunta_prueba_<?php echo $i; ?>" type="hidden" value="<?php echo base64_encode($value['pregunta_id']); ?>">
                                                                                                                </b>
                                                                                                            </p>
                                                                                                        </div>
                                                                                                    <?php } ?>
                                                                                                    <div class="col-md-12"></div>
                                                                                                    <br>
                                                                                                    <br>
                                                                                                    <?php if ($numero_respuesta <= $cantidad_respuesta) { ?>
                                                                                                        <div id="respuesta" class="col-md-12 pull-left">
                                                                                                            <div class="col-md-3"></div>
                                                                                                            <div id="respuesta" marcarVerde="<?php echo $i; ?>" tipo_pregunta="<?php echo $tipo_pregunta_sub_seccion; ?>" class="col-md-6 marcarVerde respuesta_preg_<?php echo $i; ?>">
                                                                                                                <?php
                                                                                                                if ($tipo_pregunta_sub_seccion == 'T') {
                                                                                                                    $resultado_primera_respuestaT = (isset($datosPreguntaRespondida[0])) ? (string) $datosPreguntaRespondida[0]['respuesta'] : "";
                                                                                                                    ?>
                                                                                                                    <input id="respuesta_pregunta_<?php echo $i; ?>_<?php echo $numero_respuesta ?>" name="respuesta_pregunta" type="radio" value="<?php echo base64_encode($value['respuesta_id']); ?>">
                                                                                                                    <?php echo ' ' . $value['nombre_respuesta']; ?>
                                                                                                                <?php } ?>
                                                                                                                <?php
                                                                                                                if ($tipo_pregunta_sub_seccion == 'D') {

                                                                                                                    $resultado_primera_respuesta1 = (isset($datosPreguntaRespondida[0])) ? (string) $datosPreguntaRespondida[0]['respuesta'] : "";
                                                                                                                    $resultado_primera_respuesta2 = (isset($datosPreguntaRespondida[1])) ? (string) $datosPreguntaRespondida[1]['respuesta'] : "";
                                                                                                                    ?>
                                                                                                                    <select id="respuesta_pregunta_select_<?php echo $i; ?>_<?php echo $numero_respuesta ?>" name="respuesta_pregunta">
                                                                                                                        <option value="" selected>Seleccione <?php echo $opcion ?>ª opción </option>
                                                                                                                        <?php
                                                                                                                        $numero = 1;
                                                                                                                        while ($numero <= 7) :
                                                                                                                            ?>
                                                                                                                            <option value="<?= ($numero - 1) ?>"><?= ($numero - 1) ?></option>
                                                                                                                            <?php
                                                                                                                            $numero++;
                                                                                                                        endwhile;
                                                                                                                        ?>
                                                                                                                    </select><br><br>
                                                                                                                    <?php $opcion = $opcion + 1; ?>
                                                                                                                <?php } ?>
                                                                                                                <?php
                                                                                                                if ($tipo_pregunta_sub_seccion == 'I') {
                                                                                                                    $resultado_primera_respuestaI = (isset($datosPreguntaRespondida[0])) ? (string) $datosPreguntaRespondida[0]['respuesta'] : "";
                                                                                                                    ?>
                                                                                                                    <select id="respuesta_pregunta_imagen_<?php echo $i; ?>_<?php echo $numero_respuesta ?>" name="respuesta_pregunta">
                                                                                                                        <option value="" selected>Seleccione una opción </option>
                                                                                                                        <?php
                                                                                                                        $numero = 1;
                                                                                                                        while ($numero <= $select_imagen) :
                                                                                                                            ?>
                                                                                                                            <option value="<?= ($numero ) ?>"><?= ($numero) ?></option>
                                                                                                                            <?php
                                                                                                                            $numero++;
                                                                                                                        endwhile;
                                                                                                                        ?>
                                                                                                                    </select><br><br>
                                                                                                                <?php } ?>
                                                                                                            </div>


                                                                                                            <div class="col-md-3"></div>
                                                                                                        </div>
                                                                                                        <?php
                                                                                                        $numero_respuesta = $numero_respuesta + 1;
                                                                                                    }
                                                                                                    ?>

                                                                                                    <?php if ($numero_respuesta == $cantidad_respuesta) { ?>
                                                                                                        <!--  <div class="col-md-12"></div> -->
                                                                                                        <!-- <br> -->

                                                                                                        <div class="col-md-12 action-buttons">
                                                                                                            <div class="col-md-6 align-left">
                                                                                                                <?php
                                                                                                                if ($pregunta == 1) {
                                                                                                                    $hide = 'hide';
                                                                                                                } else {
                                                                                                                    $hide = '';
                                                                                                                }
                                                                                                                ?>
                                                                                                                <button id="volver_pregunta_sub_<?php echo $pregunta; ?>" class="btn btn-danger btn-danger volver_pregunta_sub <?php echo $hide; ?>" data_sub="<?php echo $pregunta; ?>" tipo_pregunta_sub="<?php echo $tipo_pregunta_sub_seccion; ?>" data-last="Finish" type="submit">
                                                                                                                    <i class="icon-backward icon-on-left"></i>
                                                                                                                    Regresar
                                                                                                                </button>
                                                                                                                <?php //}                    ?>
                                                                                                            </div>

                                                                                                            <div class="col-md-6 align-right">
                                                                                                                <button id="next_pregunta_sub_<?php echo $pregunta; ?>" class="btn btn-primary btn-next next_pregunta_sub" data_sub="<?php echo $pregunta; ?>" tipo_pregunta_sub="<?php echo $tipo_pregunta_sub_seccion; ?>" data-last="Finish" type="submit">
                                                                                                                    Siguiente
                                                                                                                    <i class="icon-forward icon-on-right"></i>
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                                <!-- <div class="space-6"></div>-->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <!--  </div> -->
                                                                    <?php } ?>

                                                                    <!--hhhhh-->
                                                                    <?php
                                                                    if ($numero_respuesta == $cantidad_respuesta) {
                                                                        ?>
                                                                    </div>


                                                                    <div class="space-6"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--                    </div>-->
                                            <?php
                                        }
                                        ?>
                                    <?php } else { ?>

                                        <?php if ($numero_respuesta == 0) { ?>
                                            <div id="pregunta_<?php echo $pregunta; ?>" class="col-md-8">
                                                <p class="align-center">
                                                    <b>
                                                        <?php if ($tipo_pregunta_seccion == 'T') { ?>
                                                            Pregunta <?php echo $pregunta; ?>)- <?php echo '¿' . $value['nombre_pregunta'] . '?'; ?>
                                                        <?php } ?>
                                                        <?php if ($tipo_pregunta_seccion == 'D') { ?>
                                                            Pregunta <?php echo $pregunta; ?>)- <img style="width:180px;height:180px;" src="<?php echo "/public/prueba/Domino/" . $value['nombre_pregunta']; ?>" >
                                                        <?php } ?>
                                                        <?php if ($tipo_pregunta_seccion == 'I') { ?>
                                                            Pregunta <?php echo $pregunta; ?>)- <img style="width:400px;height:200px;" src="<?php echo "/public/prueba/Imagen/" . $value['nombre_pregunta']; ?>" >
                                                        <?php } ?>
                                                        <input id="pregunta_prueba_<?php echo $i; ?>" type="hidden" value="<?php echo base64_encode($value['pregunta_id']); ?>">
                                                    </b>
                                                </p>
                                            </div>
                                        <?php } ?>
                                        <!--                                            <div class="col-md-8"></div>-->
                                        <br>
                                        <br>
                                        <?php if ($numero_respuesta <= $cantidad_respuesta) { ?>
                                            <div id="respuesta" class="col-md-8 pull-left">
                                                <div class="col-md-3"></div>
                                                <div marcarVerde="<?php echo $i; ?>" id="respuesta" tipo_pregunta="<?php echo $tipo_pregunta_seccion; ?>" class="col-md-6 marcarVerde respuesta_preg_<?php echo $i; ?>">
                                                    <?php
                                                    if ($tipo_pregunta_seccion == 'T') {
                                                        $resultado_primera_respuestaT = (isset($datosPreguntaRespondida[0])) ? (string) $datosPreguntaRespondida[0]['respuesta'] : "";
                                                        ?>
                                                        <input id="respuesta_pregunta_<?php echo $i; ?>_<?php echo $numero_respuesta ?>" name="respuesta_pregunta" type="radio" value="<?php echo base64_encode($value['respuesta_id']); ?>">
                                                        <?php echo ' ' . $value['nombre_respuesta']; ?>
                                                    <?php } ?>
                                                    <?php
                                                    if ($tipo_pregunta_seccion == 'D') {
                                                        $resultado_primera_respuesta1 = (isset($datosPreguntaRespondida[0])) ? (string) $datosPreguntaRespondida[0]['respuesta'] : "";
                                                        $resultado_primera_respuesta2 = (isset($datosPreguntaRespondida[1])) ? (string) $datosPreguntaRespondida[1]['respuesta'] : "";
                                                        ?>
                                                        <select id="respuesta_pregunta_select_<?php echo $i; ?>_<?php echo $numero_respuesta ?>" name="respuesta_pregunta">
                                                            <option value="" selected>Seleccione <?php echo $opcion ?>ª opción </option>
                                                            <?php
                                                            $numero = 1;
                                                            while ($numero <= 7) :
                                                                ?>
                                                                <option value="<?= ($numero - 1) ?>"><?= ($numero - 1) ?></option>
                                                                <?php
                                                                $numero++;
                                                            endwhile;
                                                            ?>
                                                        </select><br><br>
                                                        <?php $opcion = $opcion + 1; ?>
                                                    <?php } ?>
                                                    <?php
                                                    if ($tipo_pregunta_seccion == 'I') {
                                                        $resultado_primera_respuestaI = (isset($datosPreguntaRespondida[0])) ? (string) $datosPreguntaRespondida[0]['respuesta'] : "";
                                                        ?>
                                                        <select id="respuesta_pregunta_imagen_<?php echo $i; ?>_<?php echo $numero_respuesta ?>" name="respuesta_pregunta">
                                                            <option value="" selected>Seleccione una opción </option>
                                                            <?php
                                                            $numero = 1;
                                                            while ($numero <= $select_imagen) :
                                                                ?>
                                                                <option value="<?= ($numero ) ?>"><?= ($numero) ?></option>
                                                                <?php
                                                                $numero++;
                                                            endwhile;
                                                            ?>
                                                        </select><br><br>
                    <!-- <input id="respuesta_pregunta_imagen_<?php //echo $i;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ?>_<?php //echo $numero_respuesta                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ?>" name="respuesta_pregunta" type="text" placeholder="Ingrese el numero de la respuesta." >-->
                                                    <?php } ?>
                                                </div>
                                                <div class="col-md-3"></div>
                                            </div>
                                            <?php
                                            $numero_respuesta = $numero_respuesta + 1;
                                        }
                                        ?>
                                        <?php
                                        if ($numero_respuesta == $cantidad_respuesta) {
                                            ?>
                                            <div class="col-md-12"></div>
                                            <br>
                                            <div class="col-md-4"></div>
                                            <div class="col-md-8 action-buttons">
                                                <div class="col-md-6 align-left">
                                                    <?php
                                                    if ($pregunta == 1) {
                                                        $hide = 'hide';
                                                    } else {
                                                        $hide = '';
                                                    }
                                                    ?>
                                                    <button id="volver_pregunta_<?php echo $pregunta; ?>" class="btn btn-danger btn-danger volver_pregunta <?php echo $hide; ?>" data="<?php echo $pregunta; ?>" tipo_pregunta="<?php echo $tipo_pregunta_seccion; ?>" data-last="Finish" type="submit">
                                                        <i class="icon-backward icon-on-left"></i>
                                                        Regresar
                                                    </button>
                                                    <?php //}                    ?>
                                                </div>

                                                <div class="col-md-6 align-right">
                                                    <button id="next_pregunta_<?php echo $pregunta; ?>" class="btn btn-primary btn-next next_pregunta" data="<?php echo $pregunta; ?>" tipo_pregunta="<?php echo $tipo_pregunta_seccion; ?>" data-last="Finish" type="submit">
                                                        Siguiente
                                                        <i class="icon-forward icon-on-right"></i>
                                                    </button>
                                                </div>


                                            </div>

                                    </div>


                                    <div class="space-6"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>

            <?php } ?>
        <?php } ?>
    <?php } ?>
<?php } ?>


</form>
</div>
</div>
</div>
</div>
</div>
</div>

<div id="resultDialog" class="hide"></div>

<!--</div>
</div>-->
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="css_js">
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/aplicarPrueba/aplicar_prueba.js', CClientScript::POS_END);
    ?>
</div>
<div class="panel panel-warning hide" id="dialog_termino_prueba">
    <div class="panel-heading">
        <h2 class="panel-title center grey" style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
            <b>
                <i>Prueba: <?php echo $nombre_prueba; ?></i>
            </b>
        </h2>
    </div>
    <div class="panel-body">
        <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
            <b>
                La prueba que esta presentando ha llegado a su fin. &iquest;Estas Seguro(a) que Desea Culminar la prueba? Esto implica que no podra regresar, seleccione culminar para salir de la prueba o volver para verificar las respuestas que respondió.
            </b>
        </p>
        <div id="msj_falta">
            <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
                <b>
                </b>
            </p>
        </div>
    </div>
</div>
<!--<div id="dialog_termino_prueba" class="hide">
    <p class="bigger-110 bolder center grey">
        La prueba que esta presentando con el nombre: <?php //echo $nombre_prueba;                                                                                                                                                                                                                                       ?> ha llegado a su fin. &iquest;Estas Seguro(a) que Desea Culminar la prueba? Esto implica que no podra regresar, seleccione culminar para salir de la prueba o volver para verificar las respuestas que respondió.
    </p>
</div>-->

<!--</div>-->
<div class="panel panel-warning hide" id="dialog_termino">
    <div class="panel-heading">
        <h2 class="panel-title center grey" style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
            <b>
                <i>Prueba: <?php echo $nombre_prueba; ?></i>
            </b>
        </h2>
    </div>
    <div class="panel-body">
        <p style="text-align: justify;font-size:15px;font-family: 'Helvetica';">
            <b>
                La prueba que estaba presentando ha llegado a su fin, ya que el tiempo dispuesto para la resolución de la misma ha culminado.
            </b>
        </p>
    </div>
</div>
<link rel="stylesheet" href="/public/js/FlipClock-master/compiled/flipclock.css">

<script src="/public/js/FlipClock-master/compiled/flipclock.js"></script>
<script>
    var cant_pregunta =<?php echo (isset($cant_pregunta) && $cant_pregunta != 0) ? $cant_pregunta : '"' . '"'; ?>;
    var listo = 0;
    var msj_falta = '';
    var peticion = false;
    var pregunta_actual;
    $(document).ready(function() {

        document.onkeydown = function(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            // alert(tecla)
            if (tecla = 116)
                return false
        }
        function NoBack() {
            history.go(1)
        }

//Cronometro.
        var clock;
        var mensaje = '';
        var titulo = '';
        var tiempo_segundos =<?php echo (isset($tiempo_segundos) && $tiempo_segundos != '') ? $tiempo_segundos : '"' . '"'; ?>;
        // console.log(tiempo_segundos);
        var resultado = false;
        var clock = $('.clock').FlipClock(tiempo_segundos, {
            autoStart: false,
            countdown: true,
            callbacks: {
                interval: function() {
                    var time = clock.getTime().time;
                    if (time == 0) {
                        //Change element
                        clock.stop();
                        var contador = 1;
                        $('.mostrar').each(function() {
                            // console.log(contador);
                            if ($(this).is(":hidden")) {

                                //  console.log('oculato');
                                contador = parseInt(contador) + parseInt(1);
                            } else {

                                //   console.log('termino');
                                //  contador = parseInt(contador) + parseInt(1);
                                resultado = true;
                                return false;
                            }
                        });

                        if (resultado == true) {
                            //  console.log(resultado + ' resultado');
                            titulo = 'Información';
                            dialogo(mensaje, titulo);
                             $("#div-confirmar-envio-prueba").removeClass('hide')
                        }
                    }
                }
            }
        });

        clock.start();
        //Fin.


        $('.marcarVerde').unbind('click');
        $('.marcarVerde').change(function() {
            var preg = $(this).attr('marcarVerde');
            var tipo_pregunta = $(this).attr('tipo_pregunta');
            if (tipo_pregunta == 'T') {
                $(".preguntaControl_" + preg).css('color', 'green');
                $(".preguntaControl_" + preg).attr('listo', 'listo');
            }
            if (tipo_pregunta == 'I') {
                var imag = $('#respuesta_pregunta_imagen_' + preg + '_0').val();
                if (imag != '' && imag != 'undefined') {
                    $(".preguntaControl_" + preg).css('color', 'green');
                    $(".preguntaControl_" + preg).attr('listo', 'listo');
                } else {
                    $(".preguntaControl_" + preg).css('color', '#555');
                    $(".preguntaControl_" + preg).removeAttr('listo', 'listo');
                }
            }
            if (tipo_pregunta == 'D') {
                var primera = $('#respuesta_pregunta_select_' + preg + '_0').val();
                var segunda = $('#respuesta_pregunta_select_' + preg + '_1').val();
                if (primera != '' && segunda != '' && primera != 'undefined' && segunda != 'undefined') {
                    $(".preguntaControl_" + preg).css('color', 'green');
                    $(".preguntaControl_" + preg).attr('listo', 'listo');
                } else {
                    $(".preguntaControl_" + preg).css('color', '#555');
                    $(".preguntaControl_" + preg).removeAttr('listo', 'listo');
                }
            }

        });


        $('.pregControl').unbind('click');
        $('.pregControl').click(function(evt) {
            evt.preventDefault();
            var contador = 1;
            var preg = $(this).attr('data');
            $('.mostrar').each(function() {
                if ($(this).is(":hidden")) {
                    contador = parseInt(contador) + parseInt(1);
                } else {
                    pregunta_actual = $(this).attr('data');
                    resultado = true;
                    return false;
                }
            });
            if (resultado == true) {
                if (preg == pregunta_actual) {
                    return false
                } else {
                    $('#div-mostrar-prueba_' + preg).removeClass('hide');
                    $('#div-mostrar-prueba_' + contador).addClass('hide');
                }
                var x = 0;
                var prueba_id = <?php echo "'" . $prueba_id . "'"; ?>;
                var tipo_pregunta;

                //Mostrar las respuestas que ya fueron seleccionados por el aspirante.
                var respuesta_guardada;
                var respuesta_imag;
                var respuesta2;
                var respuesta1;
                var pregunta = $('#pregunta_prueba_' + preg).val()
                //   console.log(pregunta + ' pregunta');
                if ($('#next_pregunta_' + preg).length > 0) {
                    tipo_pregunta = $('#next_pregunta_' + preg).attr("tipo_pregunta");
                } else {
                    tipo_pregunta = $('#next_pregunta_sub_' + preg).attr("tipo_pregunta_sub");
                }
                if (tipo_pregunta == 'T') {
                    $.ajax({
                        url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaPregunta",
                        data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {
                            try {
                                var json = jQuery.parseJSON(resp3.responseText);
                                if (json.statusCode == "informacion") {
                                    respuesta_guardada = json.mensaje;
                                    x = 0;
                                    if (respuesta_guardada != false) {

                                        $('.respuesta_preg_' + preg).each(function() {
                                            var respuestas = $("#respuesta_pregunta_" + preg + '_' + x).val();
                                            if (respuestas == base64_encode(respuesta_guardada)) {
                                                $("#respuesta_pregunta_" + preg + '_' + x).removeAttr('checked', 'checked');
                                                $("#respuesta_pregunta_" + preg + '_' + x).attr('checked', 'checked');

                                            }

                                            x = parseInt(x) + parseInt(1);
                                        });
                                    }
                                }
                            }
                            catch (e) {
                                $("#msj_error").addClass('hide');
                                $("#msj_error p").html('');
                                $("html, body").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                }
                if (tipo_pregunta == 'I') {
                    $.ajax({
                        url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                        data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {
                            try {
                                var json = jQuery.parseJSON(resp3.responseText);
                                if (json.statusCode == "informacionDomino") {
                                    respuesta_imag = json.respuesta1;
                                    if (respuesta_imag != false) {
                                        x = 0;
                                        $('.respuesta_preg_' + preg).each(function() {
                                            $("#respuesta_pregunta_imagen_" + preg + '_' + x).find("option[value='" + respuesta_imag + "']").attr('selected', 'selected');
                                        });
                                    }
                                }
                            }
                            catch (e) {
                                $("#msj_error").addClass('hide');
                                $("#msj_error p").html('');
                                $("html, body").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                }
                if (tipo_pregunta == 'D') {
                    $.ajax({
                        url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                        data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                        dataType: 'html',
                        type: 'post',
                        success: function(resp, resp2, resp3) {
                            try {
                                var json = jQuery.parseJSON(resp3.responseText);
                                if (json.statusCode == "informacionDomino") {
                                    respuesta1 = json.respuesta1;
                                    respuesta2 = json.respuesta2;
                                    x = 0;
                                    if ("'" + respuesta1 + "'" != false && "'" + respuesta2 + "'" != false) {
                                        $('.respuesta_preg_' + preg).each(function() {
                                            if (x == 0) {
                                                $("#respuesta_pregunta_select_" + preg + '_' + x).find("option[value='" + respuesta1 + "']").attr('selected', 'selected');
                                            } else {
                                                $("#respuesta_pregunta_select_" + preg + '_' + x).find("option[value='" + respuesta2 + "']").attr('selected', 'selected');
                                            }
                                            x = parseInt(x) + parseInt(1);
                                        });
                                    }
                                }
                            }
                            catch (e) {
                                $("#msj_error").addClass('hide');
                                $("#msj_error p").html('');
                                $("html, body").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                }

            }
        });




        var x = 0;
        var respuesta_primera_preguntaT =<?php echo (isset($resultado_primera_respuestaT) && $resultado_primera_respuestaT != '') ? '"' . $resultado_primera_respuestaT . '"' : '"' . '"'; ?>;
        var respuesta_primera_preguntaI =<?php echo (isset($resultado_primera_respuestaI) && $resultado_primera_respuestaI != '') ? '"' . $resultado_primera_respuestaI . '"' : '"' . '"'; ?>;
        var respuesta_primera_pregunta1 =<?php echo (isset($resultado_primera_respuesta1) && $resultado_primera_respuesta1 != '') ? '"' . $resultado_primera_respuesta1 . '"' : '"' . '"'; ?>;
        var respuesta_primera_pregunta2 =<?php echo (isset($resultado_primera_respuesta2) && $resultado_primera_respuesta2 != '') ? '"' . $resultado_primera_respuesta2 . '"' : '"' . '"'; ?>;
        if (respuesta_primera_preguntaT != '') {
            $('.respuesta_preg_' + 1).each(function() {
                var respuestas = $("#respuesta_pregunta_" + 1 + '_' + x).val();
                if (base64_decode(respuestas) == respuesta_primera_preguntaT) {
                    $("#respuesta_pregunta_" + 1 + '_' + x).removeAttr('checked', 'checked');
                    $("#respuesta_pregunta_" + 1 + '_' + x).attr('checked', 'checked');
                }
                x = parseInt(x) + parseInt(1);
            });
        }
        if (respuesta_primera_preguntaI != '') {
            $('.respuesta_preg_' + 1).each(function() {
                $("#respuesta_pregunta_imagen_" + 1 + '_' + 0).find("option[value='" + respuesta_primera_preguntaI + "']").attr('selected', 'selected');
            });
        }

        if (respuesta_primera_pregunta1 != '' || respuesta_primera_pregunta2 != '') {
//            console.log(respuesta_primera_pregunta1 + ' respuesta_primera_pregunta1');
//            console.log(respuesta_primera_pregunta2 + ' respuesta_primera_pregunta2');
            $('.respuesta_preg_' + 1).each(function() {

                $("#respuesta_pregunta_select_" + 1 + '_' + 0).find("option[value='" + respuesta_primera_pregunta1 + "']").attr('selected', 'selected');
                $("#respuesta_pregunta_select_" + 1 + '_' + 1).find("option[value='" + respuesta_primera_pregunta2 + "']").attr('selected', 'selected');

            });
        }
        $('.next_pregunta').unbind('click');
        $('.next_pregunta').on('click', function(e) {
            e.preventDefault();
            peticion = true;
            var boton = $(this).attr("data");
            var next = parseInt(boton) + parseInt(1); //Sumo en js
            var x = 0;
            var y = 0;
            var z = 0;
            var mensaje = '';
            var valor_seleccionada = '';
            var pregunta_id = $('#pregunta_prueba_' + boton).val();
            var prueba_id = <?php echo "'" . $prueba_id . "'"; ?>;
            var termino_prueba;
            //     var respuesta_domino = new Array();
            var respuesta_domino1 = '';
            var respuesta_domino2 = '';
            var tipo_pregunta;
            var preg_imagen = '';
            if ($('#pregunta_prueba_' + next).length) {
                termino_prueba = 1;
            } else {
                termino_prueba = 0;
            }



            //Mostrar las respuestas que ya fueron seleccionados por el aspirante.
            //    console.log('#pregunta_prueba_' + next);
            var respuesta_guardada;
            var respuesta_imag;
            var respuesta2;
            var respuesta1;
            var pregunta = $('#pregunta_prueba_' + next).val()
            //   console.log(pregunta + ' pregunta');
            if ($('#next_pregunta_' + next).length > 0) {
                tipo_pregunta = $('#next_pregunta_' + next).attr("tipo_pregunta");
            } else {
                tipo_pregunta = $('#next_pregunta_sub_' + next).attr("tipo_pregunta_sub");
            }
            console.log(tipo_pregunta + ' tipo_pregunta');
            if (tipo_pregunta == 'T') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaPregunta",
                    data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacion") {
                                respuesta_guardada = json.mensaje;
                                x = 0;
                                if (respuesta_guardada != false) {

                                    $('.respuesta_preg_' + next).each(function() {
                                        var respuestas = $("#respuesta_pregunta_" + next + '_' + x).val();
                                        if (respuestas == base64_encode(respuesta_guardada)) {
                                            $("#respuesta_pregunta_" + next + '_' + x).removeAttr('checked', 'checked');
                                            $("#respuesta_pregunta_" + next + '_' + x).attr('checked', 'checked');
                                            $('#div-mostrar-prueba_' + boton).addClass('hide');
                                            $('#div-mostrar-prueba_' + next).removeClass('hide');
                                        }

                                        x = parseInt(x) + parseInt(1);
                                    });
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            if (tipo_pregunta == 'I') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta_imag = json.respuesta1;
                                //  console.log(respuesta_imag + ' respuesta_imag I')
                                if (respuesta_imag != false) {
                                    x = 0;
                                    $('.respuesta_preg_' + next).each(function() {
                                        $("#respuesta_pregunta_imagen_" + next + '_' + x).find("option[value='" + respuesta_imag + "']").attr('selected', 'selected');
                                    });
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            if (tipo_pregunta == 'D') {
                //  console.log(tipo_pregunta);
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta1 = json.respuesta1;
                                respuesta2 = json.respuesta2;
                                x = 0;
//                                console.log(respuesta1 +' respuesta1');
//                                         console.log(respuesta2 +' respuesta2');
//                                 console.log(respuesta1 != false);
//                                        console.log(respuesta2 != false);
                                if ("'" + respuesta1 + "'" != false && "'" + respuesta2 + "'" != false) {
                                    $('.respuesta_preg_' + next).each(function() {
//                                        console.log(respuesta1);
//                                        console.log(respuesta2);
                                        if (x == 0) {
                                            $("#respuesta_pregunta_select_" + next + '_' + x).find("option[value='" + respuesta1 + "']").attr('selected', 'selected');
                                        } else {
                                            $("#respuesta_pregunta_select_" + next + '_' + x).find("option[value='" + respuesta2 + "']").attr('selected', 'selected');
                                        }
                                        x = parseInt(x) + parseInt(1);
                                    });
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            //Fin

            //Variables.
            var x = 0;
            var y = 0;
            var z = 0;
            //Fin

            $('.respuesta_preg_' + boton).each(function() {
                if ($("#respuesta_pregunta_select_" + boton + '_' + y).length) {
                    respuesta_domino1 = $("#respuesta_pregunta_select_" + boton + '_' + 0).val();
                    respuesta_domino2 = $("#respuesta_pregunta_select_" + boton + '_' + 1).val();
                    //    respuesta_domino.push(base64_encode($("#respuesta_pregunta_select_" + boton + '_' + y).val()));
                    tipo_pregunta = 'D'; //Preguntas tipo domino.
                }
                // y = parseInt(y) + parseInt(1);


                if ($('#respuesta_pregunta_imagen_' + boton + '_' + z).length) {
                    preg_imagen = $("#respuesta_pregunta_imagen_" + boton + '_' + z).val();
                    tipo_pregunta = 'I'; //Preguntas tipo Imagen.
                }
                z = parseInt(z) + parseInt(1);
                if ($("#respuesta_pregunta_" + boton + '_' + x).is(':checked')) {
                    valor_seleccionada = $("#respuesta_pregunta_" + boton + '_' + x).val();
                    tipo_pregunta = 'T'; //Preguntas tipo simple.
                }
                x = parseInt(x) + parseInt(1);
            });
            if (preg_imagen != '' || valor_seleccionada != '' || respuesta_domino1 != '' || respuesta_domino2 != '') {
                if (peticion == true) {
                    //Verifico que la respuesta no haya sido contestada aun.
                    $.ajax({
                        url: "/aplicarPrueba/aplicarPrueba/verificarRespuestaPregunta",
                        data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta},
                        dataType: 'html',
                        type: 'post',
                        beforeSend: function() {
                            peticion = true;
                        },
                        afterSend: function() {
                            peticion = false;
                        },
                        success: function(resp, resp2, resp3) {
                            try {
                                var json = jQuery.parseJSON(resp3.responseText);
                                //Actualizo las respuestas de selección simple.
                                if (json.statusCode == "actualizo") {
                                    //Respuestas de la prueba domino.
                                    var domino1 = json.domino1; //id
                                    var domino2 = json.domino2; //id
                                    var respuesta1 = json.respuesta1;
                                    var respuesta2 = json.respuesta2;
                                    //fin
                                    //Respuesta de la prueba de texto
                                    var texto = json.texto;
                                    var texto_guardada = json.respuesta_texto;
                                    //fin
                                    //Respuesta de la prueba de imagen
                                    var imagen = json.imagen;
                                    var imagen_respuesta = json.respuesta_imagen;
                                    //fin
                                    //
                                    //Valido que la opcion seleccionada sea la misma que se guardo.
                                    if (texto != '') {
                                        if (base64_decode(valor_seleccionada) == texto_guardada) {
                                            if (termino_prueba == '1') {
                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                            } else if (termino_prueba == '0') {
                                                var c = 1;
                                                $('.cantidadPregunta_' + boton).each(function() {
                                                    var attr = $(this).attr('listo');
                                                    if (attr == 'listo') {
                                                        listo = c;
                                                        c++;
                                                    }
                                                });
                                                var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                if (faltan != 0) {
                                                    msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                } else {
                                                    msj_falta = '';
                                                }
                                                $('#msj_falta').html(msj_falta);
                                                var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                    modal: true,
                                                    width: '600px',
                                                    draggable: false,
                                                    resizable: false,
                                                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                    title_html: true,
                                                    buttons: [
                                                        {
                                                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                            "class": "btn btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                            }
                                                        },
                                                        {
                                                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                            "class": "btn btn-danger btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                $('#reloj').addClass('hide');
                                                                $('.cerrar').removeClass('hide');
                                                                $('#msj_culminacion_prueba').removeClass('hide');
                                                                actualizarAplicacionPrueba(prueba_id);
                                                                $("html, body").animate({scrollTop: 0}, "fast");
                                                                // $('#msj_culminacion_prueba p').html('');
                                                            }
                                                        }
                                                    ]
                                                });
                                                $("#dialog_termino_prueba").show();
                                            }
                                            return false;
                                        }
                                    }

                                    if (imagen != '') {
                                        if (preg_imagen == imagen_respuesta) {
                                            if (termino_prueba == '1') {
                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                            } else if (termino_prueba == '0') {
                                                var c = 1;
                                                $('.cantidadPregunta_' + boton).each(function() {
                                                    var attr = $(this).attr('listo');
                                                    if (attr == 'listo') {
                                                        listo = c;
                                                        c++;
                                                    }
                                                });
                                                var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                if (faltan != 0) {
                                                    msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                } else {
                                                    msj_falta = '';
                                                }
                                                $('#msj_falta').html(msj_falta);
                                                var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                    modal: true,
                                                    width: '600px',
                                                    draggable: false,
                                                    resizable: false,
                                                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                    title_html: true,
                                                    buttons: [
                                                        {
                                                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                            "class": "btn btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                            }
                                                        },
                                                        {
                                                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                            "class": "btn btn-danger btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                $('#reloj').addClass('hide');
                                                                $('.cerrar').removeClass('hide');
                                                                $('#msj_culminacion_prueba').removeClass('hide');
                                                                actualizarAplicacionPrueba(prueba_id);
                                                                $("html, body").animate({scrollTop: 0}, "fast");
                                                                // $('#msj_culminacion_prueba p').html('');
                                                            }
                                                        }
                                                    ]
                                                });
                                                $("#dialog_termino_prueba").show();
                                            }
                                            return false;
                                        }
                                    }

                                    if (respuesta1.toString() != '' || respuesta2.toString() != '') {

                                        if (respuesta1 == respuesta_domino1 && respuesta2 == respuesta_domino2) {
                                            if (termino_prueba == '1') {
                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                            } else if (termino_prueba == '0') {
                                                var c = 1;
                                                $('.cantidadPregunta_' + boton).each(function() {
                                                    var attr = $(this).attr('listo');
                                                    if (attr == 'listo') {
                                                        listo = c;
                                                        c++;
                                                    }
                                                });
                                                var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                if (faltan != 0) {
                                                    msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                } else {
                                                    msj_falta = '';
                                                }
                                                $('#msj_falta').html(msj_falta);
                                                var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                    modal: true,
                                                    width: '600px',
                                                    draggable: false,
                                                    resizable: false,
                                                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                    title_html: true,
                                                    buttons: [
                                                        {
                                                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                            "class": "btn btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                            }
                                                        },
                                                        {
                                                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                            "class": "btn btn-danger btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                $('#reloj').addClass('hide');
                                                                $('.cerrar').removeClass('hide');
                                                                $('#msj_culminacion_prueba').removeClass('hide');
                                                                actualizarAplicacionPrueba(prueba_id);
                                                                $("html, body").animate({scrollTop: 0}, "fast");
                                                                //  $('#msj_culminacion_prueba p').html('');
                                                            }
                                                        }
                                                    ]
                                                });
                                                $("#dialog_termino_prueba").show();
                                            }
                                            return false;
                                        }
                                    }
                                    //
                                    //Fin
                                    //
                                    //Actualizo la nueva opción seleccionada por el aspirante.
                                    $.ajax({
                                        url: "/aplicarPrueba/aplicarPrueba/actualizarRespuesta",
                                        data: {
                                            id_respuesta_domino1: domino1,
                                            id_respuesta_domino2: domino2,
                                            id_respuesta_texto: texto,
                                            id_respuesta_imagen: imagen,
                                            respuesta_seleccionada: valor_seleccionada,
                                            respuesta_domino1: respuesta_domino1,
                                            respuesta_domino2: respuesta_domino2,
                                            tipo_pregunta: tipo_pregunta,
                                            respuesta_imagen: preg_imagen
                                        },
                                        dataType: 'html',
                                        type: 'post',
                                        beforeSend: function() {
                                            peticion = true;
                                        },
                                        afterSend: function() {
                                            peticion = false;
                                        },
                                        success: function(resp, resp2, resp3) {
                                            try {
                                                var json = jQuery.parseJSON(resp3.responseText);
                                                if (json.statusCode == "error") {
                                                    $("#msj_error").removeClass('hide');
                                                    $("#msj_error p").html(json.mensaje);
                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                }
                                            }
                                            catch (e) {
                                                $("#msj_error").addClass('hide');
                                                $("#msj_error p").html('');
                                                if (termino_prueba == '1') {

                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                    $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                } else if (termino_prueba == '0') {
                                                    var c = 1;
                                                    $('.cantidadPregunta_' + boton).each(function() {
                                                        var attr = $(this).attr('listo');
                                                        if (attr == 'listo') {
                                                            listo = c;
                                                            c++;
                                                        }
                                                    });
                                                    var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                    if (faltan != 0) {
                                                        msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                    } else {
                                                        msj_falta = '';
                                                    }
                                                    $('#msj_falta').html(msj_falta);
                                                    var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                        modal: true,
                                                        width: '600px',
                                                        draggable: false,
                                                        resizable: false,
                                                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                        title_html: true,
                                                        buttons: [
                                                            {
                                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                                "class": "btn btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                                }
                                                            },
                                                            {
                                                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                                "class": "btn btn-danger btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    $('#reloj').addClass('hide');
                                                                    $('.cerrar').removeClass('hide');
                                                                    $('#msj_culminacion_prueba').removeClass('hide');
                                                                    actualizarAplicacionPrueba(prueba_id);
                                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                                    // $('#msj_culminacion_prueba p').html('');
                                                                }
                                                            }
                                                        ]
                                                    });
                                                    $("#dialog_termino_prueba").show();
                                                }
                                                // $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        }
                                    });
                                    //Fin
                                }
                                //Fin de actualizar selección simple.

                                //Inserto las respuestas de selección simple.
                                if (json.statusCode == "inserto") {
                                    //Inserto la respuesta seleccionada por el aspirante.
                                    $.ajax({
                                        url: "/aplicarPrueba/aplicarPrueba/registroRespuesta",
                                        data: {respuesta_seleccionada: valor_seleccionada, pregunta: pregunta_id, respuesta_domino1: respuesta_domino1, respuesta_domino2: respuesta_domino2, respuesta_imagen: preg_imagen, tipo_pregunta: tipo_pregunta},
                                        dataType: 'html',
                                        type: 'post',
                                        beforeSend: function() {
                                            peticion = true;
                                        },
                                        afterSend: function() {
                                            peticion = false;
                                        },
                                        success: function(resp, resp2, resp3) {
                                            try {
                                                var json = jQuery.parseJSON(resp3.responseText);
                                                if (json.statusCode == "error") {
                                                    $("#msj_error").removeClass('hide');
                                                    $("#msj_error p").html(json.mensaje);
                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                }
                                            }
                                            catch (e) {
                                                $("#msj_error").addClass('hide');
                                                $("#msj_error p").html('');

                                                if (termino_prueba == '1') {
                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                    $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                } else if (termino_prueba == '0') {
                                                    var c = 1;
                                                    $('.cantidadPregunta_' + boton).each(function() {
                                                        var attr = $(this).attr('listo');
                                                        if (attr == 'listo') {
                                                            listo = c;
                                                            c++;
                                                        }
                                                    });
                                                    var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                    if (faltan != 0) {
                                                        msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                    } else {
                                                        msj_falta = '';
                                                    }
                                                    $('#msj_falta').html(msj_falta);
                                                    var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                        modal: true,
                                                        width: '600px',
                                                        draggable: false,
                                                        resizable: false,
                                                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                        title_html: true,
                                                        buttons: [
                                                            {
                                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                                "class": "btn btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                                }
                                                            },
                                                            {
                                                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                                "class": "btn btn-danger btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    $('#reloj').addClass('hide');
                                                                    $('.cerrar').removeClass('hide');
                                                                    $('#msj_culminacion_prueba').removeClass('hide');
                                                                    actualizarAplicacionPrueba(prueba_id);
                                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                                    // $('#msj_culminacion_prueba p').html('');
                                                                }
                                                            }
                                                        ]
                                                    });
                                                    $("#dialog_termino_prueba").show();
                                                }
                                                // $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        }
                                    });
                                    //Fin.
                                }
                                //Fin de insertar seleccion simple.
                            }
                            catch (e) {
                                $("#msj_error").addClass('hide');
                                $("#msj_error p").html('');
                                $("html, body").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                    //Fin
                } else {
                    var mensaje = 'En este momento se esta procesando una petición, por favor espere unos segundos e intente guardar nuevamente.';
                    var title = 'Información';
                    dialogo_peticion(mensaje, title);
                }
            } else {
                if (termino_prueba == '1') {

                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                    $('#div-mostrar-prueba_' + next).removeClass('hide');
                } else if (termino_prueba == '0') {
                    var c = 1;
                    $('.cantidadPregunta_' + boton).each(function() {
                        var attr = $(this).attr('listo');
                        if (attr == 'listo') {
                            listo = c;
                            c++;
                        }
                    });
                    var faltan = parseInt(cant_pregunta) - parseInt(listo);
                    if (faltan != 0) {
                        msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                    } else {
                        msj_falta = '';
                    }
                    $('#msj_falta').html(msj_falta);
                    var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                        modal: true,
                        width: '600px',
                        draggable: false,
                        resizable: false,
                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                "class": "btn btn-xs",
                                click: function() {
                                    $(this).dialog("close");
                                    //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                }
                            },
                            {
                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                "class": "btn btn-danger btn-xs",
                                click: function() {
                                    $(this).dialog("close");
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#reloj').addClass('hide');
                                    $('.cerrar').removeClass('hide');
                                    $('#msj_culminacion_prueba').removeClass('hide');
                                    actualizarAplicacionPrueba(prueba_id);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                    // $('#msj_culminacion_prueba p').html('');
                                }
                            }
                        ]
                    });
                    $("#dialog_termino_prueba").show();
                }

            }


        });
        $('.next_pregunta_sub').unbind('click');
        $('.next_pregunta_sub').on('click', function(e) {
            e.preventDefault();
            peticion = true;
            var boton = $(this).attr("data_sub");
            var next = parseInt(boton) + parseInt(1); //Sumo en js
            var x = 0;
            var y = 0;
            var z = 0;
            var mensaje = '';
            var valor_seleccionada = '';
            var pregunta_id = $('#pregunta_prueba_' + boton).val();
            var prueba_id = <?php echo "'" . $prueba_id . "'"; ?>;
            var termino_prueba;
            //      var respuesta_domino = new Array();
            var tipo_pregunta;
            var preg_imagen = '';
            var respuesta_domino1 = '';
            var respuesta_domino2 = '';
            var tipo_pregunta;
            var preg_imagen = '';
            if ($('#pregunta_prueba_' + next).length) {
                termino_prueba = 1;
            } else {
                termino_prueba = 0;
            }



            //Mostrar las respuestas que ya fueron seleccionados por el aspirante.
            //  console.log('#pregunta_prueba_' + next);
            var respuesta_guardada;
            var respuesta_imag;
            var respuesta2;
            var respuesta1;
            var pregunta = $('#pregunta_prueba_' + next).val()
            //   console.log(pregunta + ' pregunta');
            if ($('#next_pregunta_sub_' + next).length > 0) {
                tipo_pregunta = $('#next_pregunta_sub_' + next).attr("tipo_pregunta_sub");
            } else {
                tipo_pregunta = $('#next_pregunta_' + next).attr("tipo_pregunta");
            }
            //  console.log(tipo_pregunta + ' tipo_pregunta');
            if (tipo_pregunta == 'T') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaPregunta",
                    data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacion") {
                                respuesta_guardada = json.mensaje;
                                x = 0;
                                if (respuesta_guardada != false) {

                                    $('.respuesta_preg_' + next).each(function() {
                                        var respuestas = $("#respuesta_pregunta_" + next + '_' + x).val();
                                        if (respuestas == base64_encode(respuesta_guardada)) {
                                            $("#respuesta_pregunta_" + next + '_' + x).removeAttr('checked', 'checked');
                                            $("#respuesta_pregunta_" + next + '_' + x).attr('checked', 'checked');
                                            $('#div-mostrar-prueba_' + boton).addClass('hide');
                                            $('#div-mostrar-prueba_' + next).removeClass('hide');
                                        }

                                        x = parseInt(x) + parseInt(1);
                                    });
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            if (tipo_pregunta == 'I') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta_imag = json.respuesta1;
                                //     console.log(respuesta_imag + ' respuesta_imag I')
                                if (respuesta_imag != false) {
                                    x = 0;
                                    $('.respuesta_preg_' + next).each(function() {
                                        $("#respuesta_pregunta_imagen_" + next + '_' + x).find("option[value='" + respuesta_imag + "']").attr('selected', 'selected');
                                    });
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            if (tipo_pregunta == 'D') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta1 = json.respuesta1;
                                respuesta2 = json.respuesta2;
                                x = 0;
                                if ("'" + respuesta1 + "'" != false && "'" + respuesta2 + "'" != false) {
                                    $('.respuesta_preg_' + next).each(function() {
                                        if (x == 0) {
                                            $("#respuesta_pregunta_select_" + next + '_' + x).find("option[value='" + respuesta1 + "']").attr('selected', 'selected');
                                        } else {
                                            $("#respuesta_pregunta_select_" + next + '_' + x).find("option[value='" + respuesta2 + "']").attr('selected', 'selected');
                                        }
                                        x = parseInt(x) + parseInt(1);
                                    });
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            //Fin

            //Variables.
            var x = 0;
            var y = 0;
            var z = 0;
            //Fin

            $('.respuesta_preg_' + boton).each(function() {
                if ($("#respuesta_pregunta_select_" + boton + '_' + y).length) {
                    respuesta_domino1 = $("#respuesta_pregunta_select_" + boton + '_' + 0).val();
                    respuesta_domino2 = $("#respuesta_pregunta_select_" + boton + '_' + 1).val();
                    //    respuesta_domino.push(base64_encode($("#respuesta_pregunta_select_" + boton + '_' + y).val()));
                    tipo_pregunta = 'D'; //Preguntas tipo domino.
                }
                // y = parseInt(y) + parseInt(1);


                if ($('#respuesta_pregunta_imagen_' + boton + '_' + z).length) {
                    preg_imagen = $("#respuesta_pregunta_imagen_" + boton + '_' + z).val();
                    tipo_pregunta = 'I'; //Preguntas tipo Imagen.
                }
                z = parseInt(z) + parseInt(1);
                if ($("#respuesta_pregunta_" + boton + '_' + x).is(':checked')) {
                    valor_seleccionada = $("#respuesta_pregunta_" + boton + '_' + x).val();
                    tipo_pregunta = 'T'; //Preguntas tipo simple.
                }
                x = parseInt(x) + parseInt(1);
            });

            if (preg_imagen != '' || valor_seleccionada != '' || respuesta_domino1 != '' || respuesta_domino2 != '') {
                if (peticion == true) {
                    //Verifico que la respuesta no haya sido contestada aun.
                    $.ajax({
                        url: "/aplicarPrueba/aplicarPrueba/verificarRespuestaPregunta",
                        data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta},
                        dataType: 'html',
                        type: 'post',
                        beforeSend: function() {
                            peticion = true;
                        },
                        afterSend: function() {
                            peticion = false;
                        },
                        success: function(resp, resp2, resp3) {
                            try {
                                var json = jQuery.parseJSON(resp3.responseText);
                                //Actualizo las respuestas de selección simple.
                                if (json.statusCode == "actualizo") {
                                    //Respuestas de la prueba domino.
                                    var domino1 = json.domino1; //id
                                    var domino2 = json.domino2; //id
                                    var respuesta1 = json.respuesta1;
                                    var respuesta2 = json.respuesta2;
                                    //fin
                                    //Respuesta de la prueba de texto
                                    var texto = json.texto;
                                    var texto_guardada = json.respuesta_texto;
                                    //fin
                                    //Respuesta de la prueba de imagen
                                    var imagen = json.imagen;
                                    var imagen_respuesta = json.respuesta_imagen;
                                    //fin
                                    //
                                    //Valido que la opcion seleccionada sea la misma que se guardo.
                                    if (texto != '') {
                                        if (base64_decode(valor_seleccionada) == texto_guardada) {
                                            if (termino_prueba == '1') {
                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                            } else if (termino_prueba == '0') {
                                                var c = 1;
                                                $('.cantidadPregunta_' + boton).each(function() {
                                                    var attr = $(this).attr('listo');
                                                    if (attr == 'listo') {
                                                        listo = c;
                                                        c++;
                                                    }
                                                });
                                                var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                if (faltan != 0) {
                                                    msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                } else {
                                                    msj_falta = '';
                                                }
                                                $('#msj_falta').html(msj_falta);
                                                var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                    modal: true,
                                                    width: '600px',
                                                    draggable: false,
                                                    resizable: false,
                                                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                    title_html: true,
                                                    buttons: [
                                                        {
                                                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                            "class": "btn btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                            }
                                                        },
                                                        {
                                                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                            "class": "btn btn-danger btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                $('#reloj').addClass('hide');
                                                                $('.cerrar').removeClass('hide');
                                                                $('#msj_culminacion_prueba').removeClass('hide');
                                                                actualizarAplicacionPrueba(prueba_id);
                                                                $("html, body").animate({scrollTop: 0}, "fast");
                                                                // $('#msj_culminacion_prueba p').html('');
                                                            }
                                                        }
                                                    ]
                                                });
                                                $("#dialog_termino_prueba").show();
                                            }
                                            return false;
                                        }
                                    }
                                    if (imagen != '') {
                                        if (preg_imagen == imagen_respuesta) {
                                            if (termino_prueba == '1') {
                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                            } else if (termino_prueba == '0') {
                                                var c = 1;
                                                $('.cantidadPregunta_' + boton).each(function() {
                                                    var attr = $(this).attr('listo');
                                                    if (attr == 'listo') {
                                                        listo = c;
                                                        c++;
                                                    }
                                                });
                                                var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                if (faltan != 0) {
                                                    msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                } else {
                                                    msj_falta = '';
                                                }
                                                $('#msj_falta').html(msj_falta);
                                                var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                    modal: true,
                                                    width: '600px',
                                                    draggable: false,
                                                    resizable: false,
                                                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                    title_html: true,
                                                    buttons: [
                                                        {
                                                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                            "class": "btn btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                            }
                                                        },
                                                        {
                                                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                            "class": "btn btn-danger btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                $('#reloj').addClass('hide');
                                                                $('.cerrar').removeClass('hide');
                                                                $('#msj_culminacion_prueba').removeClass('hide');
                                                                actualizarAplicacionPrueba(prueba_id);
                                                                $("html, body").animate({scrollTop: 0}, "fast");
                                                                //  $('#msj_culminacion_prueba p').html('');
                                                            }
                                                        }
                                                    ]
                                                });
                                                $("#dialog_termino_prueba").show();
                                            }
                                            return false;
                                        }
                                    }
                                    if (respuesta1.toString() != '' || respuesta2.toString() != '') {
                                        if (respuesta1 == respuesta_domino1 && respuesta2 == respuesta_domino2) {
                                            if (termino_prueba == '1') {
                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                            } else if (termino_prueba == '0') {
                                                var c = 1;
                                                $('.cantidadPregunta_' + boton).each(function() {
                                                    var attr = $(this).attr('listo');
                                                    if (attr == 'listo') {
                                                        listo = c;
                                                        c++;
                                                    }
                                                });
                                                var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                if (faltan != 0) {
                                                    msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                } else {
                                                    msj_falta = '';
                                                }
                                                $('#msj_falta').html(msj_falta);
                                                var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                    modal: true,
                                                    width: '600px',
                                                    draggable: false,
                                                    resizable: false,
                                                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                    title_html: true,
                                                    buttons: [
                                                        {
                                                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                            "class": "btn btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                            }
                                                        },
                                                        {
                                                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                            "class": "btn btn-danger btn-xs",
                                                            click: function() {
                                                                $(this).dialog("close");
                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                $('#reloj').addClass('hide');
                                                                $('.cerrar').removeClass('hide');
                                                                $('#msj_culminacion_prueba').removeClass('hide');
                                                                actualizarAplicacionPrueba(prueba_id);
                                                                $("html, body").animate({scrollTop: 0}, "fast");
                                                                //  $('#msj_culminacion_prueba p').html('');
                                                            }
                                                        }
                                                    ]
                                                });
                                                $("#dialog_termino_prueba").show();
                                            }
                                            return false;
                                        }
                                    }
                                    //Fin
                                    //
                                    //Actualizo la nueva opción seleccionada por el aspirante.
                                    $.ajax({
                                        url: "/aplicarPrueba/aplicarPrueba/actualizarRespuesta",
                                        data: {
                                            id_respuesta_domino1: domino1,
                                            id_respuesta_domino2: domino2,
                                            id_respuesta_texto: texto,
                                            id_respuesta_imagen: imagen,
                                            respuesta_seleccionada: valor_seleccionada,
                                            respuesta_domino1: respuesta_domino1,
                                            respuesta_domino2: respuesta_domino2,
                                            tipo_pregunta: tipo_pregunta,
                                            respuesta_imagen: preg_imagen
                                        },
                                        dataType: 'html',
                                        type: 'post',
                                        beforeSend: function() {
                                            peticion = true;
                                        },
                                        afterSend: function() {
                                            peticion = false;
                                        },
                                        success: function(resp, resp2, resp3) {
                                            try {
                                                var json = jQuery.parseJSON(resp3.responseText);
                                                if (json.statusCode == "error") {
                                                    $("#msj_error").removeClass('hide');
                                                    $("#msj_error p").html(json.mensaje);
                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                }
                                            }
                                            catch (e) {
                                                $("#msj_error").addClass('hide');
                                                $("#msj_error p").html('');
                                                if (termino_prueba == '1') {

                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                    $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                } else if (termino_prueba == '0') {
                                                    var c = 1;
                                                    $('.cantidadPregunta_' + boton).each(function() {
                                                        var attr = $(this).attr('listo');
                                                        if (attr == 'listo') {
                                                            listo = c;
                                                            c++;
                                                        }
                                                    });
                                                    var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                    if (faltan != 0) {
                                                        msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                    } else {
                                                        msj_falta = '';
                                                    }
                                                    $('#msj_falta').html(msj_falta);
                                                    var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                        modal: true,
                                                        width: '600px',
                                                        draggable: false,
                                                        resizable: false,
                                                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                        title_html: true,
                                                        buttons: [
                                                            {
                                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                                "class": "btn btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                                }
                                                            },
                                                            {
                                                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                                "class": "btn btn-danger btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    $('#reloj').addClass('hide');
                                                                    $('.cerrar').removeClass('hide');
                                                                    $('#msj_culminacion_prueba').removeClass('hide');
                                                                    actualizarAplicacionPrueba(prueba_id);
                                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                                    //  $('#msj_culminacion_prueba p').html('');
                                                                }
                                                            }
                                                        ]
                                                    });
                                                    $("#dialog_termino_prueba").show();
                                                }
                                                // $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        }
                                    });
                                    //Fin
                                }
                                //Fin de actualizar selección simple.

                                //Inserto las respuestas de selección simple.
                                if (json.statusCode == "inserto") {
                                    //Inserto la respuesta seleccionada por el aspirante.
                                    $.ajax({
                                        url: "/aplicarPrueba/aplicarPrueba/registroRespuesta",
                                        data: {respuesta_seleccionada: valor_seleccionada, pregunta: pregunta_id, respuesta_domino1: respuesta_domino1, respuesta_domino2: respuesta_domino2, respuesta_imagen: preg_imagen, tipo_pregunta: tipo_pregunta},
                                        dataType: 'html',
                                        type: 'post',
                                        beforeSend: function() {
                                            peticion = true;
                                        },
                                        afterSend: function() {
                                            peticion = false;
                                        },
                                        success: function(resp, resp2, resp3) {
                                            try {
                                                var json = jQuery.parseJSON(resp3.responseText);
                                                if (json.statusCode == "error") {
                                                    $("#msj_error").removeClass('hide');
                                                    $("#msj_error p").html(json.mensaje);
                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                }
                                            }
                                            catch (e) {
                                                $("#msj_error").addClass('hide');
                                                $("#msj_error p").html('');
                                                if (termino_prueba == '1') {
                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                    $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                } else if (termino_prueba == '0') {
                                                    var c = 1;
                                                    $('.cantidadPregunta_' + boton).each(function() {
                                                        var attr = $(this).attr('listo');
                                                        if (attr == 'listo') {
                                                            listo = c;
                                                            c++;
                                                        }
                                                    });
                                                    var faltan = parseInt(cant_pregunta) - parseInt(listo);
                                                    if (faltan != 0) {
                                                        msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                                                    } else {
                                                        msj_falta = '';
                                                    }
                                                    $('#msj_falta').html(msj_falta);
                                                    var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                                                        modal: true,
                                                        width: '600px',
                                                        draggable: false,
                                                        resizable: false,
                                                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                                                        title_html: true,
                                                        buttons: [
                                                            {
                                                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                                                "class": "btn btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                                                }
                                                            },
                                                            {
                                                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                                                "class": "btn btn-danger btn-xs",
                                                                click: function() {
                                                                    $(this).dialog("close");
                                                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                                                    $('#reloj').addClass('hide');
                                                                    $('.cerrar').removeClass('hide');
                                                                    $('#msj_culminacion_prueba').removeClass('hide');
                                                                    actualizarAplicacionPrueba(prueba_id);
                                                                    $("html, body").animate({scrollTop: 0}, "fast");
                                                                    //  $('#msj_culminacion_prueba p').html('');
                                                                }
                                                            }
                                                        ]
                                                    });
                                                    $("#dialog_termino_prueba").show();
                                                }
                                                // $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        }
                                    });
                                    //Fin.
                                }
                                //Fin de insertar seleccion simple.
                            }
                            catch (e) {
                                $("#msj_error").addClass('hide');
                                $("#msj_error p").html('');
                                $("html, body").animate({scrollTop: 0}, "fast");
                            }
                        }
                    });
                    //Fin
                } else {
                    var mensaje = 'En este momento se esta procesando una petición, por favor espere unos segundos e intente guardar nuevamente.';
                    var title = 'Información';
                    dialogo_peticion(mensaje, title);
                }
            } else {
                if (termino_prueba == '1') {

                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                    $('#div-mostrar-prueba_' + next).removeClass('hide');
                } else if (termino_prueba == '0') {
                    var c = 1;
                    $('.cantidadPregunta_' + boton).each(function() {
                        var attr = $(this).attr('listo');
                        if (attr == 'listo') {
                            listo = c;
                            c++;
                        }
                    });
                    var faltan = parseInt(cant_pregunta) - parseInt(listo);
                    if (faltan != 0) {
                        msj_falta = '<b>Se le recuerda que aún le faltan ' + faltan + ' preguntas por contestar</b>';
                    } else {
                        msj_falta = '';
                    }
                    $('#msj_falta').html(msj_falta);
                    var dialogTerminoPrueba = $("#dialog_termino_prueba").removeClass('hide').dialog({
                        modal: true,
                        width: '600px',
                        draggable: false,
                        resizable: false,
                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Información</h4></div>",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-remove bigger-110'></i>&nbsp; Volver",
                                "class": "btn btn-xs",
                                click: function() {
                                    $(this).dialog("close");
                                    //                                                                $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    //                                                $('#div-mostrar-prueba_' + next).removeClass('hide');
                                }
                            },
                            {
                                html: "<i class='icon-trash bigger-110'></i>&nbsp; Culminar",
                                "class": "btn btn-danger btn-xs",
                                click: function() {
                                    $(this).dialog("close");
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#reloj').addClass('hide');
                                    $('.cerrar').removeClass('hide');
                                    $('#msj_culminacion_prueba').removeClass('hide');
                                    actualizarAplicacionPrueba(prueba_id);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                    //  $('#msj_culminacion_prueba p').html('');
                                }
                            }
                        ]
                    });
                    $("#dialog_termino_prueba").show();
                }

            }

        });
        $('.volver_pregunta').unbind('click');
        $('.volver_pregunta').on('click', function(e) {
            e.preventDefault();
            var boton = $(this).attr("data");
            var volver = parseInt(boton) - parseInt(1); //Resto en js
            var pregunta_id = $('#pregunta_prueba_' + volver).val();
            var prueba_id = <?php echo "'" . $prueba_id . "'"; ?>;
            var respuesta_guardada;
            var x = 0;
            var tipo_pregunta;
            var respuesta1;
            var respuesta2;
            if ($('#volver_pregunta_' + volver).length > 0) {
                tipo_pregunta = $('#volver_pregunta_' + volver).attr("tipo_pregunta");
            } else {
                tipo_pregunta = $('#volver_pregunta_sub_' + volver).attr("tipo_pregunta_sub");
            }

            if (tipo_pregunta == 'T') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaPregunta",
                    data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacion") {
                                respuesta_guardada = json.mensaje;
                                if (respuesta_guardada != false) {

                                    $('.respuesta_preg_' + volver).each(function() {
                                        var respuestas = $("#respuesta_pregunta_" + volver + '_' + x).val();
                                        if (respuestas == base64_encode(respuesta_guardada)) {
                                            $("#respuesta_pregunta_" + volver + '_' + x).removeAttr('checked', 'checked');
                                            $("#respuesta_pregunta_" + volver + '_' + x).attr('checked', 'checked');
                                            $('#div-mostrar-prueba_' + boton).addClass('hide');
                                            $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                        }

                                        x = parseInt(x) + parseInt(1);
                                    });
                                } else {
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            } else {
                $('#div-mostrar-prueba_' + boton).addClass('hide');
                $('#div-mostrar-prueba_' + volver).removeClass('hide');
            }
            if (tipo_pregunta == 'I') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta1 = json.respuesta1;
                                if (respuesta1 != false) {
                                    $('.respuesta_preg_' + volver).each(function() {
                                        $("#respuesta_pregunta_imagen_" + volver + '_' + x).find("option[value='" + respuesta1 + "']").attr('selected', 'selected');
                                        x = parseInt(x) + parseInt(1);
                                    });
                                } else {
                                    //    console.log(' entre else');
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            if (tipo_pregunta == 'D') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta1 = json.respuesta1;
                                respuesta2 = json.respuesta2;
                                if (respuesta1 != false && respuesta2 != false) {
                                    $('.respuesta_preg_' + volver).each(function() {
                                        if (x == 0) {
                                            $("#respuesta_pregunta_select_" + volver + '_' + x).find("option[value='" + respuesta1 + "']").attr('selected', 'selected');
                                        } else {
                                            $("#respuesta_pregunta_select_" + volver + '_' + x).find("option[value='" + respuesta2 + "']").attr('selected', 'selected');
                                        }
                                        x = parseInt(x) + parseInt(1);
                                    });
                                } else {
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
        });
        $('.volver_pregunta_sub').unbind('click');
        $('.volver_pregunta_sub').on('click', function(e) {
            e.preventDefault();
            var boton = $(this).attr("data_sub");
            var volver = parseInt(boton) - parseInt(1); //Resto en js
            var tipo_pregunta_sub;
            var pregunta_id = $('#pregunta_prueba_' + volver).val();
            var prueba_id = <?php echo "'" . $prueba_id . "'"; ?>;
            var respuesta_guardada;
            var x = 0;
            var respuesta1;
            var respuesta2;
            if ($('#volver_pregunta_sub_' + volver).length > 0) {
                tipo_pregunta_sub = $('#volver_pregunta_sub_' + volver).attr("tipo_pregunta_sub");
            } else {
                tipo_pregunta_sub = $('#volver_pregunta_' + volver).attr("tipo_pregunta");
            }

            if (tipo_pregunta_sub == 'T') {

                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaPregunta",
                    data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta_sub},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacion") {
                                respuesta_guardada = json.mensaje;
                                if (respuesta_guardada != false) {
                                    $('.respuesta_preg_' + volver).each(function() {
                                        var respuestas = $("#respuesta_pregunta_" + volver + '_' + x).val();
                                        if (respuestas == base64_encode(respuesta_guardada)) {
                                            $("#respuesta_pregunta_" + volver + '_' + x).removeAttr('checked', 'checked');
                                            $("#respuesta_pregunta_" + volver + '_' + x).attr('checked', 'checked');
                                            $('#div-mostrar-prueba_' + boton).addClass('hide');
                                            $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                        }
                                        x = parseInt(x) + parseInt(1);
                                    });
                                } else {
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            } else {
                $('#div-mostrar-prueba_' + boton).addClass('hide');
                $('#div-mostrar-prueba_' + volver).removeClass('hide');
            }
            if (tipo_pregunta_sub == 'I') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta_sub},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta1 = json.respuesta1;
                                if (respuesta1 != false) {
                                    $('.respuesta_preg_' + volver).each(function() {
                                        $("#respuesta_pregunta_imagen_" + volver + '_' + x).find("option[value='" + respuesta1 + "']").attr('selected', 'selected');
                                        x = parseInt(x) + parseInt(1);
                                    });
                                } else {
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
            if (tipo_pregunta_sub == 'D') {
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/obtenerRepuestaDomino",
                    data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta_sub},
                    dataType: 'html',
                    type: 'post',
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            if (json.statusCode == "informacionDomino") {
                                respuesta1 = json.respuesta1;
                                respuesta2 = json.respuesta2;
                                if (respuesta1 != false && respuesta2 != false) {
                                    $('.respuesta_preg_' + volver).each(function() {
                                        if (x == 0) {
                                            $("#respuesta_pregunta_select_" + volver + '_' + x).find("option[value='" + respuesta1 + "']").attr('selected', 'selected');
                                        } else {
                                            $("#respuesta_pregunta_select_" + volver + '_' + x).find("option[value='" + respuesta2 + "']").attr('selected', 'selected');
                                        }
                                        x = parseInt(x) + parseInt(1);
                                    });
                                } else {
                                    $('#div-mostrar-prueba_' + boton).addClass('hide');
                                    $('#div-mostrar-prueba_' + volver).removeClass('hide');
                                }
                            }
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
            }
        });
        $('input:radio[name=respuesta_pregunta]').unbind('click');
        $('input:radio[name=respuesta_pregunta]').on('click', function(e) {
            $(this).attr('checked', 'checked');
        });
    });


    function guardarUltimaRespuesta(contador) {
        //Variables.
        peticion = true;
        var x = 0;
        var y = 0;
        var z = 0;
        var respuesta_domino1 = '';
        var respuesta_domino2 = '';
        var tipo_pregunta = '';
        var preg_imagen = '';
        var valor_seleccionada = '';
        var prueba_id = <?php echo "'" . $prueba_id . "'"; ?>;
        var pregunta_id = $('#pregunta_prueba_' + contador).val();
        var finalizar_prueba = true;
        //Fin

        $('.respuesta_preg_' + contador).each(function() {
            //   console.log("#respuesta_pregunta_select_" + contador + '_' + y);
            if ($("#respuesta_pregunta_select_" + contador + '_' + y).length) {
                respuesta_domino1 = $("#respuesta_pregunta_select_" + contador + '_' + 0).val();
                respuesta_domino2 = $("#respuesta_pregunta_select_" + contador + '_' + 1).val();
                tipo_pregunta = 'D'; //Preguntas tipo domino.
            }
            y = parseInt(y) + parseInt(1);
            if ($('#respuesta_pregunta_imagen_' + contador + '_' + z).length) {
                preg_imagen = $("#respuesta_pregunta_imagen_" + contador + '_' + z).val();
                tipo_pregunta = 'I'; //Preguntas tipo Imagen.
            }
            z = parseInt(z) + parseInt(1);
            if ($("#respuesta_pregunta_" + contador + '_' + x).is(':checked')) {
                valor_seleccionada = $("#respuesta_pregunta_" + contador + '_' + x).val();
                tipo_pregunta = 'T'; //Preguntas tipo simple.
            }
            x = parseInt(x) + parseInt(1);
        }
        );
//        console.log(respuesta_domino1);
//        console.log(respuesta_domino2);
        if (preg_imagen != '' || valor_seleccionada != '' || respuesta_domino1 != '' || respuesta_domino2 != '') {

            if (peticion == true) {
                //Verifico que la respuesta no haya sido contestada aun.
                $.ajax({
                    url: "/aplicarPrueba/aplicarPrueba/verificarRespuestaPregunta",
                    data: {prueba_id: prueba_id, pregunta: pregunta_id, tipo_pregunta: tipo_pregunta},
                    dataType: 'html',
                    type: 'post',
                    beforeSend: function() {
                        peticion = true;
                    },
                    afterSend: function() {
                        peticion = false;
                    },
                    success: function(resp, resp2, resp3) {
                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
                            //Actualizo las respuestas de selección simple.
                            if (json.statusCode == "actualizo") {
                                //Respuestas de la prueba domino.
                                var domino1 = json.domino1; //id
                                var domino2 = json.domino2; //id
                                var respuesta1 = json.respuesta1;
                                var respuesta2 = json.respuesta2;
                                //fin
                                //Respuesta de la prueba de texto
                                var texto = json.texto;
                                var texto_guardada = json.respuesta_texto;
                                //fin
                                //Respuesta de la prueba de imagen
                                var imagen = json.imagen;
                                var imagen_respuesta = json.respuesta_imagen;
                                //fin
                                //
                                //Valido que la opcion seleccionada sea la misma que se guardo.
                                if (texto != '') {
                                    if (base64_decode(valor_seleccionada) == texto_guardada) {
                                        $.ajax({
                                            url: "/aplicarPrueba/aplicarPrueba/guardarEstatusPruebaTerminada",
                                            data: {prueba_id: prueba_id},
                                            dataType: 'html',
                                            type: 'post',
                                            success: function(resp) {

                                            }
                                        });
                                        $('#div-mostrar-prueba_' + contador).addClass('hide');
                                        $('#reloj').addClass('hide');
                                        $('.cerrar').removeClass('hide');
                                        $('#msj_culminacion_prueba').removeClass('hide');
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                        //   $('#msj_culminacion_prueba p').html('');
                                        return false;
                                    }
                                }
                                if (imagen != '') {
                                    if (preg_imagen == imagen_respuesta) {
                                        $.ajax({
                                            url: "/aplicarPrueba/aplicarPrueba/guardarEstatusPruebaTerminada",
                                            data: {prueba_id: prueba_id},
                                            dataType: 'html',
                                            type: 'post',
                                            success: function(resp) {
                                            }
                                        });
                                        $('#div-mostrar-prueba_' + contador).addClass('hide');
                                        $('#reloj').addClass('hide');
                                        $('.cerrar').removeClass('hide');
                                        $('#msj_culminacion_prueba').removeClass('hide');
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                        //   $('#msj_culminacion_prueba p').html('');
                                        return false;
                                    }
                                }
                                if (respuesta1.toString() != '' || respuesta2.toString() != '') {
                                    if (respuesta1 == respuesta_domino1 && respuesta2 == respuesta_domino2) {
                                        $.ajax({
                                            url: "/aplicarPrueba/aplicarPrueba/guardarEstatusPruebaTerminada",
                                            data: {prueba_id: prueba_id},
                                            dataType: 'html',
                                            type: 'post',
                                            success: function(resp) {

                                            }
                                        });
                                        $('#div-mostrar-prueba_' + contador).addClass('hide');
                                        $('#reloj').addClass('hide');
                                        $('.cerrar').removeClass('hide');
                                        $('#msj_culminacion_prueba').removeClass('hide');
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                        // $('#msj_culminacion_prueba p').html('');
                                        return false;
                                    }
                                }
                                //Fin
                                //
                                //Actualizo la nueva opción seleccionada por el aspirante.
                                $.ajax({
                                    url: "/aplicarPrueba/aplicarPrueba/actualizarRespuesta",
                                    data: {
                                        id_respuesta_domino1: domino1,
                                        id_respuesta_domino2: domino2,
                                        id_respuesta_texto: texto,
                                        id_respuesta_imagen: imagen,
                                        respuesta_seleccionada: valor_seleccionada,
                                        respuesta_domino1: respuesta_domino1,
                                        respuesta_domino2: respuesta_domino2,
                                        tipo_pregunta: tipo_pregunta,
                                        respuesta_imagen: preg_imagen,
                                        finalizar_prueba: finalizar_prueba,
                                    },
                                    dataType: 'html',
                                    type: 'post',
                                    beforeSend: function() {
                                        peticion = true;
                                    },
                                    afterSend: function() {
                                        peticion = false;
                                    },
                                    success: function(resp, resp2, resp3) {
                                        try {
                                            var json = jQuery.parseJSON(resp3.responseText);
                                            if (json.statusCode == "error") {
                                                $("#msj_error").removeClass('hide');
                                                $("#msj_error p").html(json.mensaje);
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        }
                                        catch (e) {
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $('#div-mostrar-prueba_' + contador).addClass('hide');
                                            $('#reloj').addClass('hide');
                                            $('.cerrar').removeClass('hide');
                                            $('#msj_culminacion_prueba').removeClass('hide');
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                            // $('#msj_culminacion_prueba p').html('');
                                        }
                                    }
                                });
                                //Fin
                            }
                            //Fin de actualizar selección simple.

                            //Inserto las respuestas de selección simple.
                            if (json.statusCode == "inserto") {
                                //Inserto la respuesta seleccionada por el aspirante.
                                $.ajax({
                                    url: "/aplicarPrueba/aplicarPrueba/registroRespuesta",
                                    data: {
                                        respuesta_seleccionada: valor_seleccionada,
                                        pregunta: pregunta_id,
                                        respuesta_domino1: respuesta_domino1,
                                        respuesta_domino2: respuesta_domino2,
                                        respuesta_imagen: preg_imagen,
                                        tipo_pregunta: tipo_pregunta,
                                        finalizar_prueba: finalizar_prueba,
                                    },
                                    dataType: 'html',
                                    type: 'post',
                                    beforeSend: function() {
                                        peticion = true;
                                    },
                                    afterSend: function() {
                                        peticion = false;
                                    },
                                    success: function(resp, resp2, resp3) {
                                        try {
                                            var json = jQuery.parseJSON(resp3.responseText);
                                            if (json.statusCode == "error") {
                                                $("#msj_error").removeClass('hide');
                                                $("#msj_error p").html(json.mensaje);
                                                $("html, body").animate({scrollTop: 0}, "fast");
                                            }
                                        }
                                        catch (e) {
                                            $("#msj_error").addClass('hide');
                                            $("#msj_error p").html('');
                                            $('#div-mostrar-prueba_' + contador).addClass('hide');
                                            $('#reloj').addClass('hide');
                                            $('.cerrar').removeClass('hide');
                                            $('#msj_culminacion_prueba').removeClass('hide');
                                            $("html, body").animate({scrollTop: 0}, "fast");
                                            //  $('#msj_culminacion_prueba p').html('');
                                        }
                                    }
                                });
                                //Fin.
                            }
                            //Fin de insertar seleccion simple.
                        }
                        catch (e) {
                            $("#msj_error").addClass('hide');
                            $("#msj_error p").html('');
                            $("html, body").animate({scrollTop: 0}, "fast");
                        }
                    }
                });
                //Fin
            } else {
                var mensaje = 'En este momento se esta procesando una petición, por favor espere unos segundos e intente guardar nuevamente.';
                var title = 'Información';
                dialogo_peticion(mensaje, title);
            }
        } else {
            $.ajax({
                url: "/aplicarPrueba/aplicarPrueba/guardarEstatusPruebaTerminada",
                data: {prueba_id: prueba_id},
                dataType: 'html',
                type: 'post',
                success: function(resp) {
                    $('#div-mostrar-prueba_' + contador).addClass('hide');
                    $('#reloj').addClass('hide');
                    $('.cerrar').removeClass('hide');
                    $('#msj_culminacion_prueba').removeClass('hide');
                    $("html, body").animate({scrollTop: 0}, "fast");
                    //  $('#msj_culminacion_prueba p').html('');
                }
            });
        }
    }

    function actualizarAplicacionPrueba(prueba_id) {
        $.ajax({
            url: "/aplicarPrueba/aplicarPrueba/actualizarAplicacionPrueba",
            data: {prueba_id: prueba_id},
            dataType: 'html',
            type: 'post',
            success: function(resp) {

            }
        });
    }


    function dialogo_peticion(mensaje, title, redireccionar) {
        displayDialogBox('dialog_peticion', 'alert', mensaje);

        var dialog = $("#dialog_peticion").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-info-circle'></i> " + title + " </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                        if (redireccionar) {
                            window.location.reload();
                        }
                    }
                }
            ]
        });
    }
    function  dialogo(mensaje, titulo) {
        var dialogTermino = $("#dialog_termino").removeClass('hide').dialog({
            modal: true,
            width: '600px',
            draggable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i>" + titulo + " </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Aceptar",
                    "class": "btn btn-primary btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
            ]
        });
        $("#dialog_termino").show();
    }
</script>

