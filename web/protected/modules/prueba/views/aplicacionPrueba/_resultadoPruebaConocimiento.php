<?php
$this->pageTitle = 'Resultado de la Prueba de Conocimiento ' . $nombreTipoPeriodo;

/* @var $this AplicacionPruebaController */
/* @var $model SeccionPrueba */
/* @var $form CActiveForm */
?>

<div class="widget-box">
    <div class="widget-header">
        <h5> <?php echo $test['nombre_prueba']; ?> </h5>
        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-body-inner">
            <div class="widget-main">
                <div class="row">
                    <div id="div-result-prueba">
                        <div id="div-result-prueba-conocimiento">
                            <div class="<?php if($estatusPostulacion==EstatusPostulacion::BENEFICIADO): ?>successDialogBox<?php else: ?>alertDialogBox<?php endif; ?>">
                                <p>
                                    <?php echo CHtml::encode($mensajeUsuario); ?> <?php if($estatusPostulacion==EstatusPostulacion::BENEFICIADO): ?>Felicitaciones! usted ha superado satisfactoriamente la Prueba de Conocimiento.<?php else: ?>Lo sentimos, usted no ha superado la Prueba Conocimiento.<?php endif; ?>
                                </p>
                            </div>
                        </div>    
                        <?php //var_dump($modelAplicacion)//ld($test); ?>

                        <div class="tabbable tabs-left">
                            <ul id="div-prueba-conocimiento-preguntas" class="nav nav-tabs">
                                <?php
                                foreach ($test['preguntas'] as $i => $pregunta):
                                    $preguntasIdEncode[] =base64_encode($pregunta['pregunta_id']);
                                    $preguntasId[]=($pregunta['pregunta_id']);
                                    $preguntaId = $pregunta['pregunta_id'];
                                    $respuestaCorrecta = $pregunta['respuesta_correcta_id'];
                                    $respuestaUsuario = (isset($preguntasYRespuestas[$preguntaId]))?$preguntasYRespuestas[$preguntaId]:null;
                                    $esCorrecta = ($respuestaUsuario == $respuestaCorrecta);
                                    ?>
                                    <li class="<?php echo ($pregunta === reset($test['preguntas'])) ? 'active' : ''; ?>" title="<?php echo CHtml::encode($pregunta['nombre_pregunta']); ?>">
                                        <a <?php if($esCorrecta): ?>class="green"<?php else: ?>class="red"<?php endif; ?>
                                                                    id ="tab-pregunta-<?php echo $i ?>" href="#panel-pregunta-<?php echo $i ?>" data-toggle="tab">
                                            <i id="i-pregunta-<?php echo $i ?>" class="fa fa-check-square-o"></i>&nbsp
                                            <span  id="s-pregunta-<?php echo $i; ?>"> Pregunta <?php echo $i + 1; ?> </span>
                                        </a>
                                    </li>
                                <?php 
                                endforeach; 
                                ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($test['preguntas'] as $i => $pregunta):
                                    $preguntaId = $pregunta['pregunta_id'];
                                    $respuestaCorrecta = $pregunta['respuesta_correcta_id'];
                                    $respuestaUsuario = (isset($preguntasYRespuestas[$preguntaId]))?$preguntasYRespuestas[$preguntaId]:null;
                                    $esCorrecta = ($respuestaUsuario == $respuestaCorrecta);
                                    ?>
                                    <div class="tab-pane <?php echo ($pregunta === reset($test['preguntas'])) ? 'active' : ''; ?>" id ="panel-pregunta-<?php echo $i ?>">
                                        <div class="col-md-12 panel-pregunta">
                                            <h4 class="question"><?php echo $i + 1; ?>.- <?php echo CHtml::encode($pregunta['nombre_pregunta']); ?></h4>
                                            <ul>
                                                <?php

                                                foreach ($pregunta['opciones'] as $j => $opcion):
                                                    $letter = Helper::getLetter($j);
                                                    $opcionHtmlId = "preg-$i-opc-$j";
                                                    if(!is_null($respuestaUsuario)):
                                                        if($respuestaUsuario == $opcion['opcion_id']):
                                                    ?>
                                                    <li>
                                                        Usted Respondió:
                                                        <br>
                                                        <label <?php if($esCorrecta): ?>class="green"<?php else: ?>class="red"<?php endif; ?>
                                                                                        for="<?php echo $opcionHtmlId ?>">
                                                            <?php echo $opcion['nombre_opcion']; ?>
                                                        </label>
                                                        <br/>
                                                    </li>
                                                    <?php 
                                                        endif;
                                                    elseif($j==0):
                                                        ?>
                                                            <li>
                                                                <label class="red">
                                                                       Usted no ha respondido esta pregunta.
                                                                </label>
                                                            </li>
                                                    <?php
                                                    endIf;
                                                endforeach;
                                                ?>
                                            </ul>

                                        </div>

                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="space-4"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="row">

    <div class="col-md-6">
        <a class="btn btn-danger" href="/gestionHumana/postulacion/consulta/tipo/<?php echo base64_encode($nombreTipoPeriodo);?>/id/<?php echo base64_encode($postulacionId);?>">
            <i class="icon-arrow-left"></i>
            Volver
        </a>
    </div>
</div>
