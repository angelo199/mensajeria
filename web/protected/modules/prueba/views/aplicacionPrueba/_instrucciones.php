<?php

$this->pageTitle = 'Instrucciones de la Prueba '.$nombreTipoPeriodo;

  /* @var $this AplicacionPruebaController */
    /* @var $model SeccionPrueba */
    /* @var $form CActiveForm */

?>

<div id="div-aplicar-prueba">

    <div class="widget-box">

        <div class="widget-header">
            <h5>Instrucciones para el inicio de la Prueba de <?php echo $nombreTipoPeriodo; ?> </h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-body-inner">
                <div class="widget-main">
                    <div id="div-result">
                        <?php if ($result['resultado'] == 'error'): ?>
                            <div class="alertDialogBox">
                                <p> <?php echo $result['mensaje']; ?> </p>
                            </div>
                        <?php else: ?>
                            <div class="infoDialogBox">
                                <p class="note">
                                    <b> <?php echo $result['mensaje']; ?> Por favor lea detenidamente las instrucciones de la prueba para que sea culminada con éxito.</b>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="">
                                <div class="panel panel-info" style="word-wrap:break-word;">
                                    <p style="text-align: justify;font-size:14px;font-family: 'Helvetica';">
                                        <b>
                                            <br>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php echo ($model->instruccion_seccion); ?>
                                            <br>
                                            <br>
                                        </b>
                                    </p>
                                </div>
                                <div class="col-md-12 center">
                                    <a class="btn btn-primary btn-next btn-sm" href="/prueba/aplicacionPrueba/presentacion/pos/<?php echo base64_encode($postulacionId);?>/per/<?php echo base64_encode($aperturaPeriodoId);?>/tipo/<?php echo  base64_encode($nombreTipoPeriodo);?>/prueba/<?php echo  base64_encode($model->id);?>">
                                        Iniciar Prueba de Conocimiento &nbsp;
                                        <i class="fa fa-check"></i>
                                    </a>
                                </div>

                            </div>
                            <div class="space-6"></div>
                        <?php endif; ?>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>




