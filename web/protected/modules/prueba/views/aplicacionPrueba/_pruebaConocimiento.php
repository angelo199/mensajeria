<?php
$this->pageTitle = 'Prueba de Conocimiento ' . $nombreTipoPeriodo;

/* @var $this AplicacionPruebaController */
/* @var $model SeccionPrueba */
/* @var $form CActiveForm */
?>

<div id="div-aplicar-prueba">

    <div class="widget-box collapsed">

        <div class="widget-header">
            <h5>Información de la Prueba <?php echo $nombreTipoPeriodo; ?> </h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-down"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-body-inner">
                <div class="widget-main">
                    <div id="div-result">                        
                        <div class="infoDialogBox">
                            <p class="note">
                                <b><?php echo $test['nombre_prueba']; ?> </b> Duración de la Prueba: <b><?php echo $test['duracion']; ?></b> Hora de Inicio: <b><?php echo $modelAplicacion->hora_inicio_prueba; ?></b>  Hora Estimada de Culminación: <b><?php echo $modelAplicacion->hora_final_prueba; ?> </b>  Cargo al cual se Postula  <b> <?php echo is_object(($modelPost->cargoPostulado)) ? $modelPost->cargoPostulado->dencargo : ''; ?></b> 
                            </p>
                        </div>
                    </div>

                    <div class="space-6"></div>

                </div>
            </div>

        </div>
    </div>

</div>

<div class="widget-box">

    <div class="widget-header">
        <h5> Tiempo Restante </h5>

        <div class="widget-toolbar">
            <i href="#">
                <i class="icon-chevron-down"></i>
            </i>
        </div>
    </div>

    <div class="widget-body">
        <div class="widget-body-inner">
            <div class="widget-main">
                <div class="row" style="max-height: 80px;">
                    <div id="reloj" class="col-md-offset-8 col-md-4">
                        <div class="clock center"></div>
                        <div class="message"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="widget-box">
    <div class="widget-header">
        <h5> <?php echo $test['nombre_prueba']; ?> </h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <form id="prueba-conocimiento-form" method="PUT" action="/prueba/aplicacionPrueba/evaluarPrueba/pos/<?php echo base64_encode($postulacionId);?>/per/<?php echo base64_encode($aperturaPeriodoId);?>/tipo/<?php echo  base64_encode($nombreTipoPeriodo);?>/prueba/<?php echo  base64_encode($pruebaId); ?>">
        <div class="widget-body">
            <div class="widget-body-inner">
                <div class="widget-main">
                    <div class="row">
                        <div id="div-result-prueba">
                            <div id="div-result-prueba-conocimiento">
                                <div class="alertDialogBox">
                                    <p>
                                        - Evite actualizar o cerrar la página o no podrá presentar la prueba luego.
                                        <br/>
                                        - Una vez culminado el tiempo, se enviará automáticamente el formulario con las preguntas respondidas.
                                    </p>
                                </div>
                            </div>    
                            <?php //var_dump($modelAplicacion)//ld($test); ?>

                            <div class="tabbable tabs-left">
                                <ul id="div-prueba-conocimiento-preguntas" class="nav nav-tabs">
                                    <?php
                                    foreach ($test['preguntas'] as $i => $pregunta):
                                        $preguntasIdEncode[] =base64_encode($pregunta['pregunta_id']);
                                        $preguntasId[]=($pregunta['pregunta_id']);
                                        ?>
                                        <li class="<?php echo ($pregunta === reset($test['preguntas'])) ? 'active' : ''; ?>" title="<?php echo CHtml::encode($pregunta['nombre_pregunta']); ?>">
                                            <a id ="tab-pregunta-<?php echo $i ?>" href="#panel-pregunta-<?php echo $i ?>" data-toggle="tab">
                                                <i id="i-pregunta-<?php echo $i ?>" class="fa fa-square-o"></i>&nbsp
                                                <span  id="s-pregunta-<?php echo $i; ?>"> Pregunta <?php echo $i + 1; ?> </span>
                                            </a>
                                        </li>
                                        <?php endforeach; ?>
                                </ul>
                                <div class="tab-content">
                                    <?php foreach ($test['preguntas'] as $i => $pregunta):
                                        ?>
                                        <div class="tab-pane <?php echo ($pregunta === reset($test['preguntas'])) ? 'active' : ''; ?>" id ="panel-pregunta-<?php echo $i ?>">
                                            <div class="col-md-12 panel-pregunta">
                                                <h4 class="question"><?php echo $i + 1; ?>.- <?php echo CHtml::encode($pregunta['nombre_pregunta']); ?></h4>
                                                <ul>
                                                    <?php
                                                    foreach ($pregunta['opciones'] as $j => $opcion):
                                                        $letter = Helper::getLetter($j);
                                                        $opcionHtmlId = "preg-$i-opc-$j";
                                                        ?>
                                                        <li>
                                                            <b class="letter-opcion-pregunta"><?php echo $letter; ?></b>&nbsp;&nbsp;
                                                            <input type="radio" class="radio-opcion pregunta<?php echo $pregunta['pregunta_id'] ?>" name="respuestas[<?php echo $pregunta['pregunta_id'] ?>]" id="<?php echo $opcionHtmlId ?>" data-indice="<?php echo $i ?>" value="<?php echo $opcion['opcion_id'] ?>"  />
                                                            &nbsp;&nbsp;
                                                            <label for="<?php echo $opcionHtmlId ?>"><?php echo $opcion['nombre_opcion']; ?></label>
                                                            <br/>
                                                        </li>
                                                        <?php endforeach; ?>
                                                </ul>

                                            </div>

                                            <div class="space-6"></div>

                                            <hr>

                                                <?php if ($pregunta === end($test['preguntas'])): // ultimo elemento  ?>                                           
                                                <div class="col-md-6" align="left">
                                                    <button  type="button"class="btn btn-sm btn-danger btn-anterior-pregunta" data-indice="<?php echo $i ?>" >
                                                        Anterior &nbsp;
                                                        <i class="icon-arrow-left"></i>
                                                    </button>
                                                </div> 
                                                <div class="col-md-6" align="right">
                                                    <button type="button" class="btn btn-sm btn-success btn-next btnPruebaConocimientoa" id="btnPruebaConocimiento">
                                                        Finalizar Prueba &nbsp;
                                                        <i class="icon-save icon-on-right"></i>
                                                    </button> 
                                                </div>
                                                <button type="submit" class="btn btn-sm btn-primary btn-next hide"  data-last="Finish" id="btnSubmitPrueba" data-indice="<?php echo $i ?>">
                                                    Finalizar &nbsp;
                                                    <i class="icon-save icon-on-right"></i>
                                                </button>

                                                 <?php elseif ($pregunta === reset($test['preguntas'])): // primer elemento  ?>
                                                <div class="col-md-10" align="right"  >
                                                    <button type="button" class="btn btn-sm btn-primary btn-next btn-siguiente-pregunta" data-indice="<?php echo $i ?>">
                                                        Siguiente &nbsp;
                                                        <i class="icon-arrow-right"></i>
                                                    </button>
                                                </div> 
                                                <div class="col-md-1" align="right">
                                                    <button type="button" class="btn btn-sm btn-success btn-next btnPruebaConocimientoa" id="btnPruebaConocimiento">
                                                        Finalizar Prueba &nbsp;
                                                        <i class="icon-save icon-on-right"></i>
                                                    </button> 
                                                </div>
                                                <?php else: //  ?>
                                                <div class="col-md-6" align="left">
                                                    <button type="button" class="btn btn-sm btn-danger btn-anterior-pregunta" data-indice="<?php echo $i ?>">
                                                        Anterior &nbsp;
                                                        <i class="icon-arrow-left"></i>
                                                    </button>
                                                </div>
                                                <div class="col-md-4" align="right">
                                                    <button type="button" class="btn btn-sm btn-primary btn-next btn-siguiente-pregunta"  data-indice="<?php echo $i ?>">
                                                        Siguiente &nbsp;
                                                        <i class="icon-arrow-right"></i>
                                                    </button>
                                                </div>
                                                <div class="col-md-1" align="right">
                                                    <button type="button" class="btn btn-sm btn-success btn-next btnPruebaConocimientoa" id="btnPruebaConocimiento">
                                                        Finalizar Prueba &nbsp;
                                                        <i class="icon-save icon-on-right"></i>
                                                    </button> 
                                                </div>
                                                <?php endif; ?>
                                            <input type="hidden" value="<?php echo base64_encode(json_encode($preguntasId)); ?>" name="preguntas"/> 
                                            <input type="hidden" value="<?php echo base64_encode($modelAplicacion->id); ?>" name="aplicacion_prueba_id"/> 
                                            <input type="hidden" value="" id="finalizada_por" name="finalizada_por"/> 
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="space-4"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>     
</div>

<div id="div-confirmar-envio-prueba" class="hide">
    <div id="div-mensaje-confirmar-envio-prueba"></div>
    <form id="resultad-prueba-conocimiento-form" name="resultad-prueba-conocimiento-form" method="post" action="/prueba/aplicacionPrueba/mostrarResultado/pos/<?php echo base64_encode($postulacionId);?>/per/<?php echo base64_encode($aperturaPeriodoId);?>/tipo/<?php echo  base64_encode($nombreTipoPeriodo);?>/prueba/<?php echo  base64_encode($pruebaId); ?>">
        <input type="hidden" autocomplete="off" value="" id="Prueba_mensaje_sistema" name="Prueba[mensaje_sistema]" />
        <input type="hidden" autocomplete="off" value="" id="Prueba_mensaje_usuario" name="Prueba[mensaje_usuario]" />
        <input type="hidden" autocomplete="off" value="" id="Prueba_estatus" name="Prueba[estatus]" />
        <input type="hidden" autocomplete="off" value="" id="Prueba_seccion" name="Prueba[seccion]" />
        <input type="hidden" autocomplete="off" value="" id="Prueba_estatus_postulacion" name="Prueba[estatus_postulacion]" />
        <input type="hidden" autocomplete="off" value="<?php echo base64_encode($modelAplicacion->id); ?>" id="Prueba_aplicacion_prueba_id" name="Prueba[aplicacion_prueba_id]" />
        <button class="hide" type="submit" id="submit-mostrar-resultado-prueba" name="submit-mostrar-resultado-prueba">Mostrar Resultado</button>
    </form>
</div>



<script>
    var preguntasId = <?php echo json_encode($preguntasId); ?>;
    var cantidadPreguntas = preguntasId.length;
    var tiempoSegundos = <?php echo $tiempoSegundos ?>;
</script>

<?php
Yii::app()->clientScript->registerScriptFile(
        Yii::app()->request->baseUrl . '/public/js/modules/prueba/pruebaConocimiento.js', CClientScript::POS_END
);
?>


