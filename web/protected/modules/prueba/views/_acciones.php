<?php

$estatus = 'ACTIVO';
$columna = '<div class="btn-group dropup">
                        <button style="height:42px;" class="btn btn-primary" data-toggle="dropdown">
                            Acciones
                            <span class="icon-caret-up icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';
if ($control == '0') {
    if (Yii::app()->user->pbac('prueba.pregunta.read') or Yii::app()->user->pbac('prueba.pregunta.write') or Yii::app()->user->pbac('prueba.pregunta.admin')) {
        /* Registro de preguntas */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Registro de Preguntas</span>", "/prueba/pregunta", array("class" => "fa fa-file-text green", "title" => "Registro de Preguntas")) . '</li>';
    }
} elseif ($control == '1') {
    if (Yii::app()->user->pbac('prueba.pregunta.read') or Yii::app()->user->pbac('prueba.pregunta.write') or Yii::app()->user->pbac('prueba.pregunta.admin')) {
        /* Registro de preguntas */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Registro de Preguntas</span>", "/prueba/pregunta", array("class" => "fa fa-file-text green", "title" => "Registro de Preguntas")) . '</li>';
    }
} elseif ($control == '2') {
    if (Yii::app()->user->pbac('prueba.seccion.read') or Yii::app()->user->pbac('prueba.seccion.write') or Yii::app()->user->pbac('prueba.seccion.admin')) {
        /* Sección */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Sección</span>", "/prueba/seccion", array("class" => "fa fa-file-text blue", "title" => "Sección")) . '</li>';
    }
}

$columna .= '</ul></div>';
echo $columna;

