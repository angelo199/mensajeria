<?php
/* @var $this TipoPeriodoProcesoController */
/* @var $model TipoPeriodoProceso */

$this->pageTitle = 'Registro de Tipo Apertura Periodos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Tipo Apertura Periodos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>
