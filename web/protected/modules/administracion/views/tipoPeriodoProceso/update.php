<?php
/* @var $this TipoPeriodoProcesoController */
/* @var $model TipoPeriodoProceso */

$this->pageTitle = 'Actualización de Datos de Tipo Apertura Periodos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Tipo Apertura Periodos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>
