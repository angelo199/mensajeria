<?php
/* @var $this AperturaPeriodoController */
/* @var $model AperturaPeriodo */
/* @var $form CActiveForm */
?>
<div class="col-xs-12" style="z-index:0">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="form">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'apertura-periodo-form',
                                'htmlOptions' => array('data-form-type'=>$formType,), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-result">
                        <?php
           if($model->hasErrors()):
               $this->renderPartial('//errorSumMsg', array('model' => $model));
           elseif(Yii::app()->user->hasFlash('success')):               $this->renderPartial('//msgBox', array('class' => 'successDialogBox', 'message' => Yii::app()->user->getFlash('success')));
           else:
                ?>
                        <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
                <?php
                   endif;
               ?>
                        </div>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'tipo_apertura_id'); ?>
                                                            <?php if($formType=='edicion'): ?>
                                                            <?php echo CHtml::dropDownList('AperturaPeriodo[tipo_apertura_id]', $model->tipo_apertura_id, CHtml::listData($tipoPeriodoProceso, 'id', 'nombre'), array('prompt' => '- - -', 'class'=>'form-control','disabled'=>'true')); ?>
                                                            <?php else: ?>
                                                            <?php echo CHtml::dropDownList('AperturaPeriodo[tipo_apertura_id]', $model->tipo_apertura_id, CHtml::listData($tipoPeriodoProceso, 'id', 'nombre'), array('prompt' => '- - -', 'class'=>'form-control')); ?>
                                                            <?php endif ?>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_inicio'); ?>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-calendar bigger-100"></i>
                                                                </span>

                                                                <?php
                                                                if ($model->fecha_inicio != "") {
                                                                    $model->fecha_inicio = date("d-m-Y", strtotime($model->fecha_inicio));
                                                                } else {
                                                                    $model->fecha_inicio = "";
                                                                }

                                                                    echo $form->textField($model, 'fecha_inicio', array('size' => 30, 'maxlength' => 30, 'class' => 'form-control date-picker', 'required' => 'required', 'id' => 'fecha_inicio', 'readonly' => 'readonly'));
                                                                ?>


                                                            </div>
                                                        </div>                                                                                                                    
                                                        <div class="col-md-4">
                                                            <?php echo $form->labelEx($model,'fecha_final'); ?>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-calendar bigger-100"></i>
                                                                </span>

                                                                <?php
                                                                if ($model->fecha_inicio != "") {
                                                                    $model->fecha_inicio = date("d-m-Y", strtotime($model->fecha_final));
                                                                } else {
                                                                    $model->fecha_inicio = "";
                                                                }

                                                                    echo $form->textField($model, 'fecha_final', array('size' => 30, 'maxlength' => 30, 'class' => 'form-control date-picker', 'required' => 'required', 'id' => 'fecha_final', 'readonly' => 'readonly'));
                                                                ?>


                                                            </div>
                                                        </div>

                                                  </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label>Descripción: </label>
                                                        <?php echo $form->textField($model, 'periodo_nombre', array('class' => 'form-control', 'required' => 'required', 'id' => 'periodo_nombre'));?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/administracion/aperturaPeriodo"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                    <div class="col-md-6 wizard-actions">
                                        <button class="btn btn-primary btn-next" data-last="Finish" type="submit">
                                            Guardar
                                            <i class="icon-save icon-on-right"></i>
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

                <div class="tab-pane" id="otrosDatos">
                    <div class="alertDialogBox">
                        <p>
                            Próximamente: Esta área se encuentra en Desarrollo.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <div id="resultDialog" class="hide"></div>
        <script src="<?php echo Yii::app()->request->baseUrl ?>/public/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl ?>/public/js/modules/concursomerito/concursomerito.js"></script>
    </div>
</div>
