<?php
/* @var $this AperturaPeriodoController */
/* @var $model AperturaPeriodo */

$this->pageTitle = 'Actualización de los Datos de la Apertura';
      $this->breadcrumbs=array(
	'Apertura de Periodos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion','tipoPeriodoProceso'=>$tipoPeriodoProceso)); ?>
