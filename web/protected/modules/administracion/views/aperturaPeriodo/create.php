<?php
/* @var $this AperturaPeriodoController */
/* @var $model AperturaPeriodo */

$this->pageTitle = 'Registro de Apertura Periodos';
      $this->breadcrumbs=array(
	'Apertura de Periodos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro','tipoPeriodoProceso'=>$tipoPeriodoProceso)); ?>
