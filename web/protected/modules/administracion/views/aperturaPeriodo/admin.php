<?php

/* @var $this AperturaPeriodoController */
/* @var $model AperturaPeriodo */

$this->breadcrumbs=array(
	'Tipos de Procesos'=>array('lista'),
);
$this->pageTitle = 'Administración de Apertura de Procesos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Procesos de Ingreso y Clasificación</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de las Aperturas de Procesos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/administracion/aperturaPeriodo/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Apertura de Proceso</a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'apertura-periodo-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    $( '#AperturaPeriodo_fecha_inicio' ).datepicker({
                        defaultDate: '+1d',
                        minDate: '+1d',
                        dateFormat: 'yy-mm-dd',
                        changeMonth: true,
                        onClose: function( selectedDate ) {
                          $( '#AperturaPeriodo_fecha_inicio' ).datepicker( 'option', 'minDate', selectedDate );
                        }
                      });
                      $( '#AperturaPeriodo_fecha_final' ).datepicker({
                        defaultDate: '+1d',
                        changeMonth: true,      
                        dateFormat: 'yy-mm-dd',
                        onClose: function( selectedDate ) {
                          $( '#AperturaPeriodo_fecha_inicio' ).datepicker( 'option', 'maxDate', selectedDate );
                        }
                      });
                }",
	'columns'=>array(
        array(
            'header' => '<center>Descripción</center>',
            'name' => 'periodo_nombre',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('AperturaPeriodo[periodo_nombre]', $model->periodo_nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>Tipo de Proceso</center>',
            'name' => 'tipo_apertura_id',
            'htmlOptions' => array(),
            //'value' => '($data->tipo_apertura==0)?"Concurso Público":"Sistema de Mérito"',
            'value' => '(is_object($data->tipoPeriodoProceso))?$data->tipoPeriodoProceso->nombre:""',
            'filter' => CHtml::listData(CTipoPeriodoProceso::getData() , 'id', 'nombre'),
        ),
        array(
            'header' => '<center>Fecha de Apertura</center>',
            'name' => 'fecha_inicio',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('AperturaPeriodo[fecha_inicio]', $model->fecha_inicio, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha de Cierre</center>',
            'name' => 'fecha_final',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('AperturaPeriodo[fecha_final]', $model->fecha_final, array('title' => '',)),
        ),
        array(
            'header' => '<center>Nombre Clave</center>',
            'name' => 'nombre_clave',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('AperturaPeriodo[nombre_clave]', $model->nombre_clave, array('title' => '',)),
        ),            
        array(
            'header' => '<center>Estado de Periodo</center>',
            'name' => 'estatus',
            'value'=>'$data->estatus=="A"?"Activo":"Inactivo"',
            'htmlOptions' => array('style'=>'color:blue; text-align:center'),
            //'filter' => CHtml::textField('AperturaPeriodo[nombre_clave]', $model->nombre_clave, array('title' => '',)),
            'filter' => CHtml::listData($status , 'abreviatura', 'nombre'),
        ),
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/AperturaPeriodoController/apertura-periodo/admin.js', CClientScript::POS_END
     *);
     */
?>
<script src="/public/datepicker/js/bootstrap-datepicker.js"></script>
<script src="/public/js/modules/concursomerito/concursomerito.js"></script>
