<?php

/* @var $this AperturaPeriodoController */
/* @var $model AperturaPeriodo */

$this->breadcrumbs=array(
	'Apertura Periodos'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Apertura Periodos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Apertura Periodos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Apertura Periodos.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/aperturaPeriodo/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Apertura Periodos                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'apertura-periodo-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(
        array(
            'header' => '<center>id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AperturaPeriodo[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>tipo_apertura</center>',
            'name' => 'tipo_apertura',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AperturaPeriodo[tipo_apertura]', $model->tipo_apertura, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_inicio</center>',
            'name' => 'fecha_inicio',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AperturaPeriodo[fecha_inicio]', $model->fecha_inicio, array('title' => '',)),
        ),
        array(
            'header' => '<center>fecha_final</center>',
            'name' => 'fecha_final',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AperturaPeriodo[fecha_final]', $model->fecha_final, array('title' => '',)),
        ),
        array(
            'header' => '<center>nombre_clave</center>',
            'name' => 'nombre_clave',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AperturaPeriodo[nombre_clave]', $model->nombre_clave, array('title' => '',)),
        ),
        array(
            'header' => '<center>status</center>',
            'name' => 'status',
            'htmlOptions' => array(),
            //'filter' => CHtml::textField('AperturaPeriodo[status]', $model->status, array('title' => '',)),
        ),
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>