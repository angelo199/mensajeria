<?php

/* @var $this AutoridadController */
/* @var $model Autoridad */

$this->breadcrumbs=array(
        'Administración' => array('lista'),
	'Autoridades'=>array('lista'),
	'',
);
$this->pageTitle = 'Administración de Autoridades';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Autoridades</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Autoridades.
                            </p>
                        </div>
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/administracion/autoridad/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Autoridad
                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'autoridad-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    
                }",
	'columns'=>array(
        array(
            'header' => '<center>Id</center>',
            'name' => 'id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[id]', $model->id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Dependencia</center>',
            'name' => 'dependenciaNombre',
            'htmlOptions' => array(),
            'value' => '(is_object($data->dependencia))?$data->dependencia->nombre:""',
            'filter' => CHtml::textField('Autoridad[dependencia_id]', $model->dependencia_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Origen</center>',
            'name' => 'origen',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[origen]', $model->origen, array('title' => '',)),
        ),
        array(
            'header' => '<center>Cedula</center>',
            'name' => 'cedula',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[cedula]', $model->cedula, array('title' => '',)),
        ),
        array(
            'header' => '<center>Nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[nombre]', $model->nombre, array('title' => '',)),
        ),
        array(
            'header' => '<center>Apellido</center>',
            'name' => 'apellido',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[apellido]', $model->apellido, array('title' => '',)),
        ),
		/*
        array(
            'header' => '<center>imagen_firma</center>',
            'name' => 'imagen_firma',
            'htmlOptions' => array(),
                        'filter' => CHtml::textField('Autoridad[imagen_firma]', $model->imagen_firma, array('title' => '',)),
                    ),
        array(
            'header' => '<center>Fecha_ini</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[fecha_ini]', $model->fecha_ini, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha_act</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[fecha_act]', $model->fecha_act, array('title' => '',)),
        ),
        array(
            'header' => '<center>Usuario_ini_id</center>',
            'name' => 'usuario_ini_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[usuario_ini_id]', $model->usuario_ini_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Usuario_act_id</center>',
            'name' => 'usuario_act_id',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[usuario_act_id]', $model->usuario_act_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Autoridad[estatus]', $model->estatus, array('title' => '',)),
        ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<?php
    /**
     * Yii::app()->clientScript->registerScriptFile(
     *   Yii::app()->request->baseUrl . '/public/js/modules/miModulo/AutoridadController/autoridad/admin.js', CClientScript::POS_END
     *);
     */
?>
