<?php
/* @var $this AutoridadController */
/* @var $model Autoridad */

$this->pageTitle = 'Registro de Autoridads';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Autoridads'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'dependencias'=>$dependencias, 'formType'=>'registro')); ?>
