<?php
/* @var $this AutoridadController */
/* @var $model Autoridad */

$this->pageTitle = 'Actualización de Datos de Autoridads';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Autoridads'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion', 'dependencias'=>$dependencias, 'mensaje'=>$mensaje)); ?>