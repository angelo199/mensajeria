<?php

/**
 * @author
 *
 */
class CambiarPerfilController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de CambiarPerfilController',
        'write' => 'Creación y Modificación de CambiarPerfilController',
        'admin' => 'Administración Completa Para Cambio de Perfiles de Usuarios de Forma Masiva  de CambiarPerfilController',
        'label' => 'Peticiones de CambiarPerfilController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('index', 'porCedulas'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('index',),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array(),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $nivelGrupoUsuario = Yii::app()->user->level;

        $perfiles = CambiarPerfilUG::model()->getPerfiles($nivelGrupoUsuario);

        $this->render('index', array(
            'perfiles' => $perfiles,
        ));
    }

    public function actionPorCedulas(){

        $botónVolver = '<div class="col-md-12">
                <div class="space-6"></div>
            </div>
            <div class="col-md-12">
                <a id="btnRegresar" href="/userGroups/cambiarPerfil" class="btn btn-danger">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>';
        
        if($this->hasPut('perfil_id') && $this->hasPut('cedulas')){
            
            $perfilId = $this->getPut('perfil_id');
            $cedulasText = $this->getPut('cedulas');
            $cedulas = explode("\n", $cedulasText);

            if(count($cedulas)<=300){
            
                $userId = Yii::app()->user->id;
                $username = Yii::app()->user->name;
                $userLevel = Yii::app()->user->level;
                $ipAddress = Helper::getRealIP();
                $resultado = CambiarPerfilUG::model()->cambiarPerfilUsuariosByCedulas($perfilId, $cedulas, $username, $userId, $userLevel, $ipAddress);

                if($resultado){
                    $this->renderPartial('_resultadoCambioMasivoPerfil',array(
                        'resultado' => $resultado,
                    ));
                }
                else{
                    $this->renderPartial('//msgBox',array('class'=>'errorDialogBox','message'=>'APERR1002: No se han proveido los datos necesarios para efectuar esta acción.'));
                    echo $botónVolver;
                }
            }
            else {
                $this->renderPartial('//msgBox',array('class'=>'errorDialogBox','message'=>'APERR1001: Está permitido cambiar el perfil por lotes no mayores a 300 usuarios. Divida esta lista en lotes menores o iguales a 300 registros.'));
                echo $botónVolver;
            }
        }
        else{
            $this->renderPartial('//msgBox',array('class'=>'errorDialogBox','message'=>'APERR1000: No se han proveido los datos necesarios para efectuar esta acción.'));
            echo $botónVolver;
        }

    }

}
