<?php

/**
 * This is the model class for table "seguridad.usergroups_user".
 *
 * The followings are the available columns in table 'seguridad.usergroups_user':
 * @property string $id
 * @property string $group_id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $home
 * @property integer $status
 * @property string $question
 * @property string $answer
 * @property string $date_ini
 * @property string $activation_code
 * @property string $activation_time
 * @property string $last_login
 * @property string $ban
 * @property string $ban_reason
 * @property string $telefono
 * @property string $nombre
 * @property string $apellido
 * @property integer $cedula
 * @property string $direccion
 * @property integer $estado_id
 * @property string $user_ini_id
 * @property string $user_act_id
 * @property string $date_act
 * @property string $user_ban_id
 * @property string $last_ip_address
 * @property string $origen
 * @property string $clave_anterior
 * @property integer $cambio_clave
 * @property string $twitter
 * @property string $telefono_celular
 * @property string $presento_documento_identidad
 * @property string $foto
 * @property string $correo_enviado
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $userAct
 * @property UsergroupsUser[] $usergroupsUsers
 * @property UsergroupsUser $userBan
 * @property UsergroupsUser[] $usergroupsUsers1
 * @property Estado $estado
 * @property UsergroupsGroup $group
 * @property UsergroupsUser $userIni
 * @property UsergroupsUser[] $usergroupsUsers2
 * @property UsergroupsGroup[] $usergroupsGroups
 */
class UserGroupsRegistro extends UserGroupsUser
{
    
    public $digitos_cuenta_nomina;
    
    public static $cantidadDeDigitosRequeridosDeCuentaNomina = 4;
    
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        //return Yii::app()->db->tablePrefix.'usergroups_user';
        return 'seguridad.usergroups_user';
    }
    
    public function rules() {
        // load validation rules folder admin nombre
        Yii::import('userGroups.validation.*');
        return array(
            
            // rules for external registration
            array('password', 'match', 'pattern' => '/^[A-Za-z0-9\@\.\,\?\/\*\(\)\_\-\&\^\%\$\#\!\~]+$/', 'message' => 'Formato incorrecto de su clave', 'on' => 'registroExterno'), ///^[A-Za-z0-9]{4,}$/
            array('username, password, email, nombre, apellido, cedula, estado_id, group_id, telefono, date_ini, date_act, verifyCode, password_confirm', 'required', 'on' => 'registroExterno'),
            array('verifyCode', 'checkCaptcha', 'on' => 'registroExterno'),  
            array('email', 'email', 'message' => "El correo electrónico no posee un formato válido. Ej.: micuenta@dominio.me.gob.ve"),
            array('password', 'passwordStrength', 'on' => 'registroExterno'),
            array('password_confirm', 'compare', 'compareAttribute' => 'password', 'on' => 'registroExterno', 'message' => 'La Clave de Confirmación no coincide con la Clave Ingresada'), 
            // Integers
            array('status, cedula, estado_id, group_id', 'numerical', 'integerOnly' => true),
            // Range
            array('group_id', 'in', 'range'=>array(6, 7, 8, 9), 'on'=>'registroExterno', 'message'=>'Seleccioné el Grupo del Empleado al que pertenece'),
            array('origen', 'in', 'range'=>array('V', 'E' ,'O'), 'on'=>'registroExterno', 'message'=>'Seleccioné el Origen del Empleado al que pertenece'),
            //Length
            array('nombre, apellido', 'length','min'=>2, 'max' => 40),
            array('username, nombre, apellido', 'length', 'max' => 120),
            array('telefono', 'length', 'max' => 14),
            array('password', 'length', 'min'=>6, 'max'=>20),
            // uniques
            array('cedula', 'unique', 'message' => 'La Cédula ya se encuentra registrada. Si olvido su cuenta haga click en la seccion: "¿Olvidaste tu Clave?".', 'on' => 'registroExterno'),
            array('email', 'unique', 'message' => 'El correo electrónico ya se encuentra registrado.', 'on' => 'registroExterno'),
            array('digitos_cuenta_nomina', 'checkRequiredDigitosCuentaNomina', 'on'=>'registroExterno'),
            array('correo_enviado', 'in', 'range'=>array('S', 'N'),),
            array('email', 'checkNoPermitirCorreoHotmail')
        );
    }
    
    public function checkRequiredDigitosCuentaNomina($attribute, $params) {
        if($this->group_id != '9' && (strlen(Helper::onlyNumericString($this->digitos_cuenta_nomina))!=self::$cantidadDeDigitosRequeridosDeCuentaNomina || !is_numeric($this->digitos_cuenta_nomina))){
            $this->addError($attribute, 'Si usted es funcionario del MPPE, debe ingresar los '.self::$cantidadDeDigitosRequeridosDeCuentaNomina.' ultimos digitos de su Cuenta de Nomina.');
        }
    }

    public function checkNoPermitirCorreoHotmail($attribute, $params) {
        if((strpos($this->email, '@hotmail', 1)) OR (strpos($this->email, '@outlook', 1))){
            $this->addError($attribute, ' No esta Permitido Realizar el Registro con Correo Hotmail u Outlook, es Preferible que Utilice su Correo Institucional o en su Defecto un Correo Gmail.');
        }
    }

    public function setDataBeforeRegisterNewExternalUser(){
        $this->username = $this->cedula.self::generarLetraFromCedula($this->cedula);
        $this->status = 1; # En espera de activación.
        $this->activation_time = date('Y-m-d H:i:s');
        $this->activation_code = md5(uniqid().$this->username.$this->activation_time);
        $this->user_ini_id = 0;
        $this->user_act_id = 0;
        $this->creation_date = $this->activation_time;
        $this->date_ini = $this->activation_time;
        $this->date_act = $this->activation_time;
        $this->digitos_cuenta_nomina = (isset($this->digitos_cuenta_nomina))?$this->digitos_cuenta_nomina:null;
    }
    
    /**
     * 
     * @return array Array asociativo con el resultado del proceso de registro externo de usuarios
     */
    public function registroExternoUsuario() {
        
        $this->generatePasswordEncrypted();
        
        $origen = $this->origen; $cedula = $this->cedula; $nombre = $this->nombre; $apellido = $this->apellido;
        $telefono = $this->telefono; $password = $this->password; $email = $this->email; $groupId = $this->group_id;
        $estadoId = $this->estado_id; $digitos_cuenta_nomina = $this->digitos_cuenta_nomina; $username = $this->username;
        $status = $this->status; $activationCode = $this->activation_code; $activationTime = $this->activation_time;
        $userIniId = $this->user_ini_id; $userActId = $this->user_act_id; $dateIni = $this->date_ini; $dateAct = $this->date_act;
        
        $attributes = json_encode($this->attributes);
        
        $sql="SELECT * FROM seguridad.registro_externo_usuario(
            :origen::varchar,
            :cedula::int,
            :nombre::varchar,
            :apellido::varchar,
            :telefono::varchar,
            :password::varchar,
            :correo::varchar,
            :grupoId::int,
            :estadoId::int,
            :cuentaNomina::varchar,
            :usuario::varchar,
            :status::int,
            :claveActivacion::varchar,
            :fechaActivacion::TIMESTAMP,
            :usuarioIni::bigint,
            :usuarioAct::bigint,
            :fechaIni::TIMESTAMP,
            :fechaAct::TIMESTAMP,
            :data::TEXT
        ) AS f(codigo varchar, resultado varchar, mensaje varchar, user_id bigint, talento_humano_id bigint, seccion smallint);";
        
        $query = Yii::app()->db->createCommand($sql);

        $query->bindParam(':origen', $origen , PDO::PARAM_STR) ;
        $query->bindParam(':cedula', $cedula , PDO::PARAM_INT) ;
        $query->bindParam(':nombre', $nombre , PDO::PARAM_STR) ;
        $query->bindParam(':apellido', $apellido , PDO::PARAM_STR) ;
        $query->bindParam(':telefono', $telefono , PDO::PARAM_STR) ;
        $query->bindParam(':password', $password , PDO::PARAM_STR) ;
        $query->bindParam(':correo', $email , PDO::PARAM_STR) ;
        $query->bindParam(':grupoId', $groupId , PDO::PARAM_INT) ;
        $query->bindParam(':estadoId', $estadoId , PDO::PARAM_INT) ;
        $query->bindParam(':cuentaNomina', $digitos_cuenta_nomina , PDO::PARAM_STR) ;
        $query->bindParam(':usuario', $username , PDO::PARAM_STR) ;
        $query->bindParam(':status', $status , PDO::PARAM_INT) ;
        $query->bindParam(':claveActivacion', $activationCode , PDO::PARAM_STR) ;
        $query->bindParam(':fechaActivacion', $activationTime , PDO::PARAM_STR) ;
        $query->bindParam(':usuarioIni', $userIniId , PDO::PARAM_INT) ;
        $query->bindParam(':usuarioAct', $userActId , PDO::PARAM_INT) ;
        $query->bindParam(':fechaIni', $dateIni , PDO::PARAM_STR) ;
        $query->bindParam(':fechaAct', $dateAct , PDO::PARAM_STR) ;
        $query->bindParam(':data', $attributes , PDO::PARAM_STR) ;

        $result = $query->queryRow();

        if(array_key_exists('user_id', $result)){
            $this->id = $result['user_id'];
        }

        return $result;
    }
    
    /**
     *  Este método se encarga de efectuar el proceso de activación de cuenta de usuario registrado externamente y retorna el resultado en formato de array asociativo con las siguientes claves
     * 
     *  'codigo' => string,
     *  'resultado' => string,
     *  'mensaje' => string,
     *  'user_id' => int
     *
     * @param string $username Username del Uusario
     * @param string $codigoDeActivacion Código de Activación
     * @return array Array asociativo del resultado del proceso de activación de cuenta de usuario registrado externamente
     */
    public function activarRegistroExternoUsuario($codigoDeActivacion, $username, $fechaActual, $modulo, $ipAddress) {

        $sql="SELECT * FROM seguridad.activacion_registro_externo_usuario(
                                :activation_code_vi::CHARACTER VARYING,
                                :username_vi::CHARACTER VARYING,
                                :fecha_actual_vi::DATE,
                                :modulo_vi::CHARACTER VARYING,
                                :ipaddress_vi::CHARACTER VARYING
                            ) AS f(codigo varchar, resultado varchar, mensaje varchar, user_id bigint);";

        $query = Yii::app()->db->createCommand($sql);

        $query->bindParam(':activation_code_vi', $codigoDeActivacion , PDO::PARAM_STR);
        $query->bindParam(':username_vi', $username , PDO::PARAM_INT);
        $query->bindParam(':fecha_actual_vi', $fechaActual , PDO::PARAM_STR);
        $query->bindParam(':modulo_vi', $modulo , PDO::PARAM_STR);
        $query->bindParam(':ipaddress_vi', $ipAddress , PDO::PARAM_STR);

        $result = $query->queryRow();

        return $result;
    }

    public function generatePasswordEncrypted(){
        $this->password = md5($this->password . $this->getSalt());
        return $this->password;
    }
    
    public function getSalt() {
        // TODO when stop supporting php 5.2 use dateTime
        // turn the date_ini into the corresponding timestamp
        $this->creation_date = $this->date_ini;
        list($date, $time) = explode(' ', $this->creation_date);
        $date = explode('-', $date);
        $time = explode(':', $time);

        if (!is_null($this->date_ini)) {
            date_default_timezone_set('UTC');
            $timestamp = mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]);
        } else {
            $timestamp = '';
        }
        // create the salt
        $salt = $this->username . $timestamp;
        // add the additional salt if it's provided
        if (isset(Yii::app()->controller->module) && isset(Yii::app()->controller->module->salt))
            $salt .= Yii::app()->controller->module->salt;

        return $salt;
    }
    
    public function marcarCorreoNoEnviado($id){

        $result = 0;

        if(is_numeric($id)){
            $idInt = (int)$id;
            $sql = "UPDATE ".$this->tableName()." SET correo_enviado = 'N' WHERE id = :userId";
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $command->bindParam(":userId", $idInt, PDO::PARAM_INT);
            $result = $command->execute();

        }

        return $result;

    }
    
    /**
     * Returns the static model of the specified AR class.
     * @return UserGroupsUser the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
}
