<?php

/**
 * @author José Gabriel Gonzalez <jgonzalezp@me.gob.ve>
 * 
 * Clase de Estadísticas de Usuarios
 */
class UserStats extends UserGroupsUser{

    /**
     * Obtiene la estadísticas de los siguientes grupos de usuarios: ADMINISTRADOR_USUARIOS, ADMINISTRADOR_INGRESO_Y_CLASIFICACION, ANALISTA_INGRESO_NIVEL_CENTRAL, ANALISTA_INGRESO_ZONA_EDUCATIVA y ENTREVISTADOR
     * 
     * @return array
     */
   public function getUserStats(){

        $sql = "SELECT null AS id, 1 AS orden, 'Todos' AS grupo, COUNT(u.id) AS cantidad FROM ".$this->tableName()." u WHERE u.group_id IN (".UserGroups::ADMINISTRADOR_USUARIOS.", ".UserGroups::ADMINISTRADOR_INGRESO_Y_CLASIFICACION.", ".UserGroups::ANALISTA_INGRESO_NIVEL_CENTRAL.", ".UserGroups::ANALISTA_INGRESO_ZONA_EDUCATIVA.", ".UserGroups::ENTREVISTADOR.") 
                  UNION
                SELECT '".UserGroups::ADMINISTRADOR_USUARIOS."' AS id, 2 AS orden, 'Administradores de Usuario' AS grupo, COUNT(a.id) AS cantidad FROM ".$this->tableName()." a WHERE a.group_id = ".UserGroups::ADMINISTRADOR_USUARIOS."
                  UNION
                SELECT '".UserGroups::ADMINISTRADOR_INGRESO_Y_CLASIFICACION."' AS id, 3 AS orden, 'Administradores de Ingreso y Clasificación' AS grupo, COUNT(b.id) AS cantidad FROM ".$this->tableName()." b WHERE b.group_id = ".UserGroups::ADMINISTRADOR_INGRESO_Y_CLASIFICACION."
                  UNION
                SELECT '".UserGroups::ANALISTA_INGRESO_NIVEL_CENTRAL."' AS id, 4 AS orden, 'Analistas - Nivel Central' AS grupo, COUNT(d.id) AS cantidad FROM ".$this->tableName()." d WHERE d.group_id = ".UserGroups::ANALISTA_INGRESO_NIVEL_CENTRAL."
                  UNION
                SELECT '".UserGroups::ANALISTA_INGRESO_ZONA_EDUCATIVA."' AS id, 5 AS orden, 'Analistas - Zona Educativa' AS grupo, COUNT(c.id) AS cantidad FROM ".$this->tableName()." c WHERE c.group_id = ".UserGroups::ANALISTA_INGRESO_ZONA_EDUCATIVA."
                  UNION
                SELECT '".UserGroups::ENTREVISTADOR."' AS id, 6 AS orden, 'Entrevistadores MPPE' AS grupo, COUNT(c.id) AS cantidad FROM ".$this->tableName()." c WHERE c.group_id = ".UserGroups::ENTREVISTADOR."
                ORDER BY orden";
        $statement = Yii::app()->db->createCommand($sql);
        $resultado = $statement->queryAll(true);

        $todos = (int)$resultado[0]['cantidad'];
        $adminsUser = (int)$resultado[1]['cantidad'];
        $adminsIngreso = (int)$resultado[2]['cantidad'];
        $analistaCentral = (int)$resultado[3]['cantidad'];
        $analistaZona = (int)$resultado[4]['cantidad'];
        $entrevistadores = (int)$resultado[5]['cantidad'];

        $resultado[1]['porcentaje'] = ($todos>0)?($adminsUser*100/$todos):0;
        $resultado[2]['porcentaje'] = ($todos>0)?($adminsIngreso*100/$todos):0;
        $resultado[3]['porcentaje'] = ($todos>0)?($analistaCentral*100/$todos):0;
        $resultado[4]['porcentaje'] = ($todos>0)?($analistaZona*100/$todos):0;
        $resultado[5]['porcentaje'] = ($todos>0)?($entrevistadores*100/$todos):0;

        return $resultado;
   }
   
   /**
    * Retorna la lista de usuarios dado un Id del Grupo de Usuarios (ROL) al que pertenecen
    * 
    * @param int $groupId
    * @return array
    */
   public function getDataDetailsByGroupId($groupId){
       $resultado = null;
       if(is_numeric($groupId)){
            $sql = "SELECT 
                        e.nombre AS estado,
                        u.origen, 
                        u.cedula, 
                        u.username AS login, 
                        u.nombre, 
                        u.apellido, 
                        g.groupname AS grupo, 
                        g.description AS descripcion_grupo, 
                        s.text AS estatus, 
                        u.email, 
                        u.telefono AS telefono1, 
                        u.telefono_celular AS telefono2
                    FROM 
                        seguridad.usergroups_user u 
                        INNER JOIN seguridad.usergroups_group g ON u.group_id = g.id
                        INNER JOIN seguridad.usergroups_lookup s ON u.status = s.id
                        INNER JOIN catastro.estado e ON u.estado_id = e.id
                    WHERE u.group_id = :groupId";
            $statement = Yii::app()->db->createCommand($sql);
            $statement->bindParam(':groupId', $groupId);
            $resultado = $statement->queryAll(true);
       }
       return $resultado;
   }

   /**
    *
    * @param string $grouBy grupo, 
    */
   public function getUserStatsBy($grouBy){

        $resultado = null;
        if(in_array($grouBy, array('grupo', 'edad', 'profesion'))){
            if($grouBy == 'grupo'){
                $sql = 'SELECT g.groupname AS grupo, COUNT(u.id) AS cantidad FROM usergroups_group g LEFT JOIN ".$this->tableName()." u ON g.id = u.group_id WHERE g.id != 1 GROUP BY g.groupname';
            }
            elseif($grouBy == 'edad'){
                $sql = 'SELECT TIMESTAMPDIFF(YEAR, u.fecha_nacimiento, CURDATE()) AS edad, COUNT(u.id) AS cantidad FROM ".$this->tableName()." u WHERE u.group_id != 1 GROUP BY edad';
            }
            elseif($grouBy == 'profesion'){
                $sql = 'SELECT  p.nombre_es AS profesion_es, p.nombre_en AS profesion_en, COUNT(u.id) AS cantidad FROM profesion p LEFT JOIN ".$this->tableName()." u ON p.id = u.profesion_id GROUP BY p.nombre_es, p.nombre_en';
            }
            $statement = Yii::app()->db->createCommand($sql);
            $resultado = $statement->queryAll(true);
        }
        return $resultado;
   }

   /**
    * Returns the static model of the specified AR class.
    * @return UserStats the static model class
    */
   public static function model($className = __CLASS__) {
           return parent::model($className);
   }

}
