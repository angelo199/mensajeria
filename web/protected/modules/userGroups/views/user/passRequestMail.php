<div class="container">
    <?php echo CHtml::encode($model->nombre.' '.$model->apellido); ?> usted ha recibido este correo para el restablecimiento de su Clave de Acceso, para seguir con este procesos 
    debe hacer click en <b><a href="<?php echo Yii::app()->getParams()->hostName; ?>/login/restablecerClave/cod/<?php echo str_replace('/', '**', $codigoDeSeguridad); ?>">Restablecer Clave de Acceso</a></b>
    o acceder a la siguiente dirección e ingresar su nueva clave:
    <br/><br/>
    <?php echo Yii::app()->getParams()->hostName; ?>/login/restablecerClave/cod/<?php echo str_replace('/', '**', $codigoDeSeguridad); ?>
    <br/><br/>
    Su cuenta será inactivada mientras restablece su clave de acceso.
    <br/><br/>
    <b>Este correo ha sido enviado a la fecha <?php echo Helper::toAppDate($model->activation_time); ?> y sólo tiene validez por 12 horas, si usted no ha restablecido su clave en este tiempo deberá indicar una vez más que ha olvidado su clave y esperar un nuevo correo.</b>
</div>
