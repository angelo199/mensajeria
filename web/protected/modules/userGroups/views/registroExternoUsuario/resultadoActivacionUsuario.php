<?php
 /**
  * @var array resultado Array asociativo que contiene las siguiente claves 'codigo' => string, 'resultado' => string, 'mensaje' => string, 'user_id' => int
  */
?>
<div class="col-sm-10 col-sm-offset-1">
    <div class="login-container">

        <div class="space-6"></div>

        <div class="position-relative">

            <div id="signup-box" class="signup-box visible widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header blue lighter bigger">
                            <i class="icon-unlock green"></i>
                            Proceso de Activación de Cuenta de Usuario
                        </h4>

                        <div class="space-6"></div>

                        <fieldset>

                            <div id="result">

                                <div class="<?php if(isset($resultado['resultado'])): echo Helper::getCssClassBox($resultado['resultado']); else: echo 'errorDialogBox'; endif; ?>">
                                    <p>
                                        <?php 
                                            if(isset($resultado['resultado']) && $resultado['resultado']!='exito') echo '('.$resultado['codigo'].') ';
                                            if(isset($resultado['mensaje'])) echo $resultado['mensaje']; 
                                        ?>
                                    </p>
                                </div>

                            </div>

                            <div class="space"></div>

                            <div class="clearfix">
                                <a class="width-35 pull-right btn btn-sm btn-success" href="/login">
                                    Ir al Login
                                    &nbsp;
                                    <i class="icon-key"></i>
                                </a>
                            </div>

                            <div class="space-4"></div>
                        </fieldset>

                    </div><!-- /widget-main -->

                    <div class="toolbar clearfix">
                        <div>
                            <a href="/login" class="back-to-login-link">
                                <i class="icon-arrow-right"></i>
                                Ir al Login
                            </a>
                        </div>
                    </div>
                </div><!-- /widget-body -->
            </div><!-- /login-box -->

        </div><!-- /position-relative -->

    </div>

</div><!-- /.col -->
<?php
    if($resultado['resultado']=='exito' || trim($resultado['codigo'])=='ADB002'):
?>
<script type="text/javascript">
    var rutaActivacion = "/login";
    var watingTimeActivacion = 5;
    var idleTimeActivacion = 0;
    $(document).ready(function() {
        var idleIntervalActivacion = setInterval('timerIncrementActivacion()', 1000); // 60000 1 minute
    });
    function timerIncrementActivacion()
    {
        idleTimeActivacion = idleTimeActivacion + 1;
        if (idleTime > watingTimeActivacion) // minutes
        {
            window.location = rutaActivacion;
        }
    }
</script>
<?php endif; ?>

