<div class="container">
    El Proceso de Registro de Usuario se ha efectuado exitósamente con los siguientes datos: 
    <br/><br/>
    <b>C&eacute;dula:</b> <?php echo CHtml::encode($model->origen."-".$model->cedula); ?><br/>
    <b>Nombre(s) y Apellido(s):</b> <?php echo CHtml::encode($model->nombre.' '.$model->apellido); ?><br/>
    <b>Tel&eacute;fono:</b> <?php echo $model->telefono; ?><br/>
    <b>Correo Electr&oacute;nico:</b> <?php echo $model->email; ?><br/>
    <b>Estado:</b> <?php echo ucwords(strtolower($model->estado->nombre)); ?><br/>
    <hr>
    <b style="color: #990000;">Usuario:</b> <?php echo $model->username; ?><br/>
    <b style="color: #990000;">Clave:</b> <?php echo $clave; ?><br/>
    <hr>
    <br/>
    Para activar su Cuenta de Usuario, de hacer click en <a href="<?php echo Yii::app()->getParams()->hostName; ?>/registro/activarCuenta/cod/<?php echo str_replace('/', '**', $codigoDeSeguridad); ?>">Activar Cuenta</a>
    o acceda a la siguiente dirección:
    <br/><br/>
    <?php echo Yii::app()->getParams()->hostName; ?>/registro/activarCuenta/cod/<?php echo str_replace('/', '**', $codigoDeSeguridad); ?>
    <br/>
    Este correo es vigente por 7 días. Si no ha activado la cuenta con este link en 7 días deberá solicitar el reenvío del correo de activación.
</div>
