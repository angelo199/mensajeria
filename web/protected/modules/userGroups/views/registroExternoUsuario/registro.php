<div class="col-sm-10 col-sm-offset-1">

    <div class="login-container">

        <div class="space-6"></div>

        <div class="position-relative">

            <div id="result">
            </div>
            
            <div id="registre-box" class="login-box visible widget-box no-border">
                <div class="widget-body">
                        <div class="widget-main">
                            <h4 class="header red lighter bigger">
                                <i class="fa fa-user-plus red"></i>
                                Registro de Nuevo Usuario
                            </h4>
                            <div class="space-6"></div>
                            <!-- -->
                                <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'register-form',
                                    'enableAjaxValidation' => false,
                                    'focus' => array($model, 'cedula'),
                                ));
                                ?>
                                <fieldset>

                                    <!-- Origen y Cédula -->
                                    <div class="block clearfix">
                                        <div class="input-group col-md-12">
                                            <div class="input-group-btn">
                                                <button type="button" id="botonOrigen" title="Origen" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="margin-top: -33px; padding-top: 4px;">V&nbsp; &nbsp;<span class="caret"></span></button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a class="origenAcciones" data-value="V">V</a></li>
                                                    <li class="divider"></li>
                                                    <li><a class="origenAcciones" data-value="E">E</a></li>
                                                    <li class="divider"></li>
                                                    <li><a class="origenAcciones" data-value="P">P</a></li>
                                                </ul>
                                                <span style="width: 87.7%" class="input-icon input-icon-right">
                                                    <input type="hidden" value="V" id="UserGroupsRegistro_origen" name="UserGroupsRegistro[origen]" placeholder="Origen" class="input form-control hide" readonly="readonly" required="required"  />
                                                    <input type="text" autocomplete="off" class="input form-control span-12" required="required" placeholder="Cédula" name="UserGroupsRegistro[cedula]" id="UserGroupsRegistro_cedula" value="" x-moz-errormessage="Debe Ingresar su Número de Cédula" />
                                                    <i class="icon-credit-card"></i>
                                                </span>
                                            </div><!-- /btn-group -->
                                        </div><!-- /input-group -->    
                                    </div>
                                    <div class="space-6"></div>
                                    <div class="block clearfix">
                                        <!-- Nombre y Apellido -->
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="text" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Nombre" name="UserGroupsRegistro[nombre]" id="UserGroupsRegistro_nombre" value="" maxlength="40" x-moz-errormessage="Ingrese su Nombre" />
                                            <i class="icon-user"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="text" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Apellido" name="UserGroupsRegistro[apellido]" id="UserGroupsRegistro_apellido" value="" maxlength="40" x-moz-errormessage="Ingrese su Apellido">
                                            <i class="icon-user"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <!-- Teléfono y Correo Electrónico -->
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="text" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Teléfono" name="UserGroupsRegistro[telefono]" id="UserGroupsRegistro_telefono" value="" maxlength="14" x-moz-errormessage="Ingrese un número telefónico de contacto" />
                                            <i class="icon-phone"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="email" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Correo Electrónico" name="UserGroupsRegistro[email]" id="UserGroupsRegistro_email" value="" maxlength="120" x-moz-errormessage="Ingrese un Correo Electrónico Válido - Es neceario para la activación del Usuario en el Sistema">
                                            <i class="icon-envelope"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="email" autocomplete="off"  class="input form-control span-12" required="required" placeholder="Confirme su Correo Electrónico" name="confirmarEmail" id="confirmarEmail" value="" maxlength="120" x-moz-errormessage="Confirme su Correo Electrónico">
                                            <i class="icon-envelope"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <!-- Clave y  Confirmación de Clave-->
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="password" autocomplete="off"  class="input form-control space-3" required="required" placeholder="Clave" name="UserGroupsRegistro[password]" id="UserGroupsRegistro_password" value="" maxlength="20" title="Ingrese una Clave que contenga al menos 6 Caracteres" x-moz-errormessage="Ingrese una Clave que contenga al menos 6 Caracteres" />
                                            <i class="icon-key"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <!-- Clave y  Confirmación de Clave-->
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="password" autocomplete="off"  class="input form-control space-3" required="required" placeholder="Confirme su Clave" name="UserGroupsRegistro[password_confirm]" id="UserGroupsRegistro_password_confirm" value="" maxlength="20" title="Confirme su Clave" >
                                            <i class="icon-key"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <!-- Grupo de Usuario y Estado/Provincia -->
                                    <div class="block clearfix">
                                        <span  class="input-icon col-md-12" style="">
                                            <?php echo $form->dropDownList($model, 'group_id', CHtml::listData($gruposExternos, 'id', 'nombre'), array('prompt'=>' - Tipo de Personal - ', 'class'=>'input form-control', 'required'=>'required', 'title'=>'Seleccione el Grupo de Empleados al que Pertenece', 'x-moz-errormessage'=>'Seleccione su Condición Actual en el MPPE')); ?>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <!-- Grupo de Usuario y Estado/Provincia -->
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12" style="">
                                            <?php echo $form->dropDownList($model, 'estado_id', CHtml::listData($estados, 'id', 'nombre'), array('prompt'=>' - Estado donde Labora - ', 'class'=>'input form-control', 'required'=>'required', 'title'=>'Seleccione el Estado donde ejerció o ejerce como Funcionario del Ministerio', 'x-moz-errormessage'=>'Seleccione el Estado ejerce como Funcionario del Ministerio')); ?>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <!-- Cinco últimos números de la Cuenta de Nómina -->
                                    <div class="block clearfix">
                                        <span  class="input-icon input-icon-right col-md-12">
                                            <input type="text" autocomplete="off"  class="input form-control space-3" required="required" placeholder="Ultimos Cuatro Números de su Cuenta de Nómina MPPE" name="UserGroupsRegistro[digitos_cuenta_nomina]" id="UserGroupsRegistro_digitos_cuenta_nomina" title="Ingrese los últimos cinco números de su Cuenta de Nómina MPPE" x-moz-errormessage="Ingrese los últimos cinco números de su Cuenta de Nómina MPPE" value="">
                                            <i class="icon-credit-card"></i>
                                        </span>
                                    </div>
                                    <div class="space-6"></div>
                                    <input type="hidden" name="<?php echo $tokenName; ?>" id="<?php echo $tokenName; ?>" value="<?php echo $tokenValue; ?>" />
                                    <hr>
                                    <div class="block clearfix">
                                        <div class="col-xs-4" style="padding-left: 0px;">
                                            <a id="linkRefreshCaptchaRegistro" tabindex="-1" style="border-style: none;" title="Haga Click para obtener otra Imágen. El Código no es sensible a mayúsculas y minúsculas.">
                                                <img id="siimageRegistro" style="border: 1px solid #DDDDDD; margin-right: 15px" src="/login/captcha/sid/<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left" height="45" />
                                            </a>
                                        </div>
                                        <div class="col-xs-8" style="text-align: right; padding-right: 0px;">
                                            <span class="block input-icon input-icon-right">
                                                <?php echo $form->textField($model,'verifyCode', array( 'required'=>'required', 'style'=>'width: 100%;', 'maxlength'=>'10', 'required'=>'required', 'placeholder'=>'Ingrese el Código de la Imagen', 'title'=>'Ingrese el Código de la Imagen. El código no es sensible a mayúsculas y minúsculas.', 'autocomplete'=>'off')); ?>
                                                <i class="icon-qrcode"></i>
                                            </span>
                                        </div>
                                    </div>



                                    <div class="space"></div>

                                    <div class="clearfix">
                                        <button type="submit" class="width-35 pull-right btn btn-sm btn-danger" id="buttonSubmitId">
                                            Registrar
                                            &nbsp;
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <a title="Si ya posee un usuario activo puede ingresar al sistema a través del Login" href="/login" class="width-35 pull-left btn btn-sm btn-danger" id="buttonRegresarLogin">
                                            <i class="icon-key"></i>
                                            Ir al Login
                                        </a>
                                    </div>

                                    <div class="space-4"></div>

                                </fieldset>
                                <?php $this->endWidget(); ?>  
                            <!-- -->
                    </div> <!-- Widget Main-->
                    <div class="toolbar clearfix">
                        <div>
                            <a onclick="show_box('faqs-box');
                                    return false;" class="forgot-password-link">
                                <i class="icon-arrow-left"></i>
                                Preguntas Frecuentes
                            </a>
                        </div>

                        <div>
                            <a onclick="show_box('signup-box');
                                    return false;" class="user-signup-link">
                                ¿Olvidaste tu Clave?
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div> <!-- Widget Body -->
            </div>
            

            <div id="faqs-box" class="forgot-box widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">

                        <h4 class="header blue lighter bigger">
                            <i class="icon-key"></i>
                            Preguntas Frecuentes
                        </h4>

                        <div class="space-6"></div>
                        <p class="faqs-q">
                            <b>Estoy presentando un Error 503 en el Navegador</b>
                        </p>
                        <fieldset class="faqs-q">
                            <label class="block clearfix faqs-a">
                                <i>R: Puede que deba Configurar el Proxy en su Navegador para poder acceder a la Red interna del Ministerio del Poder Popular para La Educación.</i>
                            </label>
                        </fieldset>
                        <p class="faqs-q">
                            <b>¿Quién puede ingresar al Sistema?</b>
                        </p>
                        <fieldset>
                            <label class="block clearfix faqs-a">
                                <i>R: Al Sistema puede ingresar todo aquel al que se le haya dado permiso por parte del Ministerio del Poder Popular para la Educación.</i>
                            </label>
                        </fieldset>
                        <p class="faqs-q">
                            <b>¿Cómo Obtengo un Usuario para ingresar al Sistema?</b>
                        </p>
                        <fieldset>
                            <label class="block clearfix faqs-a">
                                <i>R: Debe Registrarse y Aceptar los Términos y Condiciones del Uso de esta Aplicación.</i>
                            </label>
                        </fieldset>
                        <p class="faqs-q">
                            <b>¿Qué navegador debo utilizar para ejecutar la Aplicación?</b>
                        </p>
                        <fieldset>
                            <label class="block clearfix faqs-a">
                                <i>R: Recomendamos el Uso de las últimas versiones de Mozilla Firefox, Chromium, Google Chrome o Cunaguaro en su versión 27 o superior.</i>
                            </label>
                        </fieldset>
                        <p class="faqs-q">
                            <b>¿Existe un dato de contacto?</b>
                        </p>
                        <fieldset>
                            <label class="block clearfix faqs-a">
                                <i>R: Puede enviar un correo con <b>los datos y especificaciones detalladas</b> del requerimiento a <a href="mailto:soporte_sistemas@me.gob.ve">soporte_sistemas@me.gob.ve</a>.</i>
                            </label>
                        </fieldset>
                    </div><!-- /widget-main -->

                    <div class="toolbar center">
                        <a href="#" onclick="show_box('registre-box');
                                return false;" class="back-to-login-link">
                            Volver al Registro
                            <i class="icon-arrow-right"></i>
                        </a>
                    </div>
                </div><!-- /widget-body -->
            </div><!-- /forgot-box -->

            <div id="signup-box" class="signup-box widget-box no-border">
                <div class="widget-body">
                    <div class="widget-main">
                        <h4 class="header green lighter bigger">
                            <i class="icon-group blue"></i>
                            ¿Olvidaste tu Clave?
                        </h4>

                        <div class="space-6"></div>
                        <p> Ingrese los datos necesarios para recuperar su clave: </p>

                        <!-- Formulario de Recupación de Clave -->
                        <?php $this->renderPartial('passRequest', array('modelPr' => $modelPr)); ?>
                    </div>

                    <div class="toolbar center">
                        <a href="#" onclick="show_box('registre-box');
                                return false;" class="back-to-login-link">
                            <i class="icon-arrow-left"></i>
                            Volver al Registro de Titular
                        </a>
                    </div>
                </div><!-- /widget-body -->

            </div><!-- /signup-box -->

        </div><!-- /position-relative -->

    </div>

</div><!-- /.col -->

<?php
// Librerías
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.maskedinput.min.js', CClientScript::POS_END );
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/lib/jquery.numeric.min.js', CClientScript::POS_END );
//  JS para el modulo de Registro de Usuarios Externos
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/userGroups/registroUsuario/registro.js',CClientScript::POS_END);    
?>
