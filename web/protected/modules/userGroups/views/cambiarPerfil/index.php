<?php
/* @var $this CambiarPerfilController */

$this->breadcrumbs=array(
        'Seguridad' => '#',
	'Cambiar Perfil',
);
?>
<div id="divResultCambiarPerfil">
    <div class="infoDialogBox"><p class="note">Todos los campos con <span class="required">*</span> son requeridos.</p></div>
</div>

<div class="widget-box">
    <div class="widget-header">
        <h5>Cambiar el Perfil de un grupo de usuarios por Cédula.</h5>
        <div class="widget-toolbar">
            <a href="#" id="ocultar" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="widget-body">
        <div class="widget-body-inner">
            <div class="widget-main">
                <div class="widget-main form">
                    <form name="formulario-perfil" id="formulario-perfil" action="/userGroups/cambiarPerfil/porCedulas" method="put">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="">
                                    <label for="perfil_id"> Perfil de Usuario </label><span class="required"> *</span>
                                    <?php echo CHtml::dropDownList( 'perfil_id', null ,CHtml::listData($perfiles, 'id', 'groupname'), array('prompt'=>'- - -', 'class' => 'span-12', "required"=>"required",)); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="space-6"></div>
                            </div>
                            <div id="div-cedulas">
                                <div class="col-md-12">
                                    <div class="">
                                        <label for="cedulas"> Cédulas  </label><span class="required"> *</span>
                                        <textarea required="required" class="col-md-12" id="cedulas" name="cedulas" rows="15" placeholder="Ingrese la lista de números de cédulas de las personas a las que desea realizar el cambio de pefil"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="space-6"></div>
                                </div>
                                <div class="col-md-12">
                                    <button id="btnGuardarPerfilesCedulas" class="btn btn-primary btn-next right" type="submit">
                                        Guardar
                                        <i class="icon-save icon-on-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="divResultado"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/userGroups/grupo/cambiarPerfilesMasivo.js',CClientScript::POS_END
);
?>
