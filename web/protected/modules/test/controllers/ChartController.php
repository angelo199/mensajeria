<?php

/**
 * Description of ChartController
 *
 * @author Familia González
 */
class ChartController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='make';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => '',
        'write' => '',
        'admin' => '',
        'label' => ''
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('allow', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionMake($type='columns'){
        if($type=='columns'){
            $this->render('chart-columns', array());
        }
        elseif($type=='columns3d'){
            $this->render('chart-columns-3d', array());
        }
        elseif($type=='map-ve'){
            $this->render('chart-map-ve', array());
        }
        elseif($type=='map-ve-guayana'){
            $this->render('chart-map-ve-guayana', array());
        }
        else{
            $this->render('chart-columns', array());
        }
    }
    
}
