<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EliminarCacheController
 *
 * @author pleiber
 */
class EliminarCacheController extends Controller{
    
    public $defaultAction = 'lista';
    public static $cacheIndex = 'TH:{talentoHumanoId}';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de Datos del Talento Humano',
        'write' => 'Creación y Modificación de Datos del Talento Humano',
        'admin' => 'eliminar cahe talento Humano',
        'label' => 'Eliminar Cache de  Talento Humano'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('ThSimple','borrarCacheConfig', 'TH'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('consulta', 'creacion', 'edicion', 'municipiosStandAlone', 'parroquiasStandAlone', 'registroDatosGenerales', 'registroDatosBancarios'),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('consulta', 'misDatos',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    /**Este metodo elimina la cache del talentoHumano  con el siguiente index TH:SIMPLE:{id} que se encuentra ubicado en el modelo TalentoHumano 
     * 
     * @param int $id    id del talento humano
     */
    public function actionThSimple($id){
        $control=true;
        $talentoHumanoModel= new TalentoHumano;
        $cacheIndexSimple = str_replace('{id}',$id,$talentoHumanoModel->cacheIndexSimple);
        Yii::app()->cache->delete($cacheIndexSimple);           
         // var_dump($cacheIndexSimple); die();
         $this->render('eliminarCache', array(
                'control' => $control,
            ));
    }
    /**Este metodo elimina la cache del talentoHumano  con el siguiente index TH:{talentoHumanoId} que se encuenta en el controlador TalentoHumanoController
     * 
     * @param int $id    id del talento humano
     */
     public function actionTH($id){
        $control=true;
        $cacheIndex= TalentoHumanoController::$cacheIndex;        
        $cacheIndex = str_replace('{talentoHumanoId}',$id,$cacheIndex);  
        Yii::app()->cache->delete($cacheIndex);           
         $this->render('eliminarCache', array(
                'control' => $control,
            ));
    }
    
 public function actionBorrarCacheConfig(){
     $control=true;
     $cacheIndex= Configuracion::$cacheIndexConfig;    
     Yii::app()->cache->delete($cacheIndex); 
      $this->render('eliminarCache', array(
                'control' => $control,
           ));
 }
   
}
