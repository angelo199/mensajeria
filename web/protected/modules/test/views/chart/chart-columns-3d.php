<style type="text/css">
    #container {
        height: 500px; 
        min-width: 800px; 
        max-width: 1240px;
        margin: 0 auto;
    }
</style>

<div id="container" style="height: 500px"></div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/highcharts/highcharts.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/highcharts/highcharts-3d.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/highcharts/modules/exporting.js"></script>

<script type="text/javascript">
    $(function () {
        $('#container').highcharts({
            chart: {
                type: 'column',
                margin: 75,
                options3d: {
                    enabled: true,
                    alpha: 5,
                    beta: 10,
                    depth: 70
                }
            },
            title: {
                text: 'Sistema de Mérito 2015'
            },
            subtitle: {
                text: 'Indicadores de Participantes en el Sistema de Mérito'
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 150,
                verticalAlign: 'top',
                y: 40,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor)
            },
            tooltip: {
                headerFormat: '<span style="font-size:15px"><b>{point.key}</b></span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            xAxis: {
                categories: [
                    'Amazonas',
                    'Anzoategui',
                    'Apure',
                    'Aragua',
                    'Barinas',
                    'Bolívar',
                    'Carabobo',
                    'Cojedes',
                    'Delta Amacuro',
                    'Distrito Capital',
                    'Falcón',
                    'Guarico',
                    'Lara',
                    'Mérida',
                    'Miranda',
                    'Monagas',
                    'Nueva Esparta',
                    'Portuguesa',
                    'Sucre',
                    'Tachira',
                    'Trujillo',
                    'Yaracuy',
                    'Zulia'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: null
                }
            },
            series: [{
                    name: 'Cargos Ofertados',
                    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6]

                }, {
                    name: 'Inscritos',
                    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3, 83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6]

                }, {
                    name: 'Postulados',
                    data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2, 48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3]

                }]
        });
    });
</script>
