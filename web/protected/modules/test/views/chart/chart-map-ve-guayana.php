<style type="text/css">
    #container-ve {
        height: 500px;
    }
    #container-ve-guayana-ezequiba {
        height: 500px;
    }
    .loading {
        margin-top: 10em;
        text-align: center;
        color: gray;
    }
</style>

<div class="col-md-12">
    <div class="col-md-6" id="container-ve"></div>
    <div class="col-md-6" id="container-ve-guayana-ezequiba"></div>
</div>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/highmaps/highmaps.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/highmaps/modules/exporting.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/highmaps/mapdata/countries/ve/ve-all.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/lib/highmaps/mapdata/countries/ve/ve-guayana-ezequiba.js"></script>

<script>
$(function () {

    // Prepare demo data
    var dataVe = [
        {
            "hc-key": "ve-3609",
            "value": 0
        },
        {
            "hc-key": "ve-dp",
            "value": 1
        },
        {
            "hc-key": "ve-ne",
            "value": 2
        },
        {
            "hc-key": "ve-su",
            "value": 3
        },
        {
            "hc-key": "ve-da",
            "value": 4
        },
        {
            "hc-key": "ve-bo",
            "value": 5
        },
        {
            "hc-key": "ve-ap",
            "value": 6
        },
        {
            "hc-key": "ve-ba",
            "value": 7
        },
        {
            "hc-key": "ve-me",
            "value": 8
        },
        {
            "hc-key": "ve-ta",
            "value": 9
        },
        {
            "hc-key": "ve-tr",
            "value": 10
        },
        {
            "hc-key": "ve-zu",
            "value": 11
        },
        {
            "hc-key": "ve-co",
            "value": 12
        },
        {
            "hc-key": "ve-po",
            "value": 13
        },
        {
            "hc-key": "ve-ca",
            "value": 14
        },
        {
            "hc-key": "ve-la",
            "value": 15
        },
        {
            "hc-key": "ve-ya",
            "value": 16
        },
        {
            "hc-key": "ve-fa",
            "value": 17
        },
        {
            "hc-key": "ve-am",
            "value": 18
        },
        {
            "hc-key": "ve-an",
            "value": 19
        },
        {
            "hc-key": "ve-ar",
            "value": 20
        },
        {
            "hc-key": "ve-213",
            "value": 21
        },
        {
            "hc-key": "ve-df",
            "value": 22
        },
        {
            "hc-key": "ve-gu",
            "value": 23
        },
        {
            "hc-key": "ve-mi",
            "value": 24
        },
        {
            "hc-key": "ve-mo",
            "value": 25
        }
    ];
    
    var dataGuayana = [
        {
            "hc-key": "gy-de",
            "value": 0
        },
        {
            "hc-key": "gy-pt",
            "value": 0
        },
        {
            "hc-key": "gy-ud",
            "value": 0
        },
        {
            "hc-key": "gy-pm",
            "value": 0
        },
        {
            "hc-key": "gy-ba",
            "value": 0
        },
        {
            "hc-key": "gy-cu",
            "value": 0
        }
    ];

    // Initiate the chart
    $('#container-ve').highcharts('Map', {

        title : {
            text : 'Venezuela'
        },

        subtitle : {
            text : '<a target="_blank" href="/public/js/lib/highmaps/mapdata/countries/ve/ve-all.js">Venezuela</a>',
            useHTML: true
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            min: 0
        },

        series : [{
            data : dataVe,
            mapData: Highcharts.maps['countries/ve/ve-all'],
            joinBy: 'hc-key',
            name: 'Participantes',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }]
    });
    
    $('#container-ve-guayana-ezequiba').highcharts('Map', {

        title : {
            text : 'Guayana Ezequiba'
        },

        subtitle : {
            text : '<a target="_blank" href="/public/js/lib/highmaps/mapdata/countries/ve/ve-guayana-ezequiba.js">Guayana Ezequiba</a>',
            useHTML: true
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        colorAxis: {
            min: 0
        },

        series : [{
            data : dataGuayana,
            mapData: Highcharts.maps['countries/ve/ve-guayana-ezequiba-all'],
            joinBy: 'hc-key',
            name: 'Participantes',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }]
    });
});

</script>
