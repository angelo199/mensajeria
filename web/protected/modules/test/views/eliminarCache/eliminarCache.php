<?php

$this->pageTitle = 'Eliminar Cahe';

 
?>

<div id="div-eliminar-prueba">

    <div class="widget-box">

        <div class="widget-header">
            <h5>Eliminar Cache </h5>

            <div class="widget-toolbar">
                <a data-action="collapse" href="#">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div class="widget-body-inner">
                <div class="widget-main">
                    <div id="div-result">
                        <?php if ($control): ?>
                            <div class="successDialogBox">
                                <p>  La cache ha sido eliminada exitosamente </p>
                            </div>
                        <?php else: ?>
                            <div class="alertDialogBox">
                                <p> No existe cache que debe eliminarse </p>
                            </div>
                        <?php endif; ?>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

