<?php
/* @var $this OrganismoResponsableConvenioController */
/* @var $model OrganismoResponsableConvenio */

$this->pageTitle = 'Actualización de Datos de Organismo Responsable Convenios';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Organismo Responsable Convenios'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>