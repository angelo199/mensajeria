<?php
/* @var $this OrganismoResponsableConvenioController */
/* @var $model OrganismoResponsableConvenio */

$this->pageTitle = 'Registro de Organismo Responsable Convenios';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Organismo Responsable Convenios'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>