<?php
/* @var $this OrganismoResponsableVenezuelaController */
/* @var $model OrganismoResponsableVenezuela */

$this->pageTitle = 'Registro de Organismo Responsable Venezuelas';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Organismo Responsable Venezuelas'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>