<?php
/* @var $this OrganismoResponsableVenezuelaController */
/* @var $model OrganismoResponsableVenezuela */

$this->pageTitle = 'Actualización de Datos de Organismo Responsable Venezuelas';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Organismo Responsable Venezuelas'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>