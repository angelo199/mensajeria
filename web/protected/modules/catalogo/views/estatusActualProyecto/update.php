<?php
/* @var $this EstatusActualProyectoController */
/* @var $model EstatusActualProyecto */

$this->pageTitle = 'Actualización de Datos de Estatus Actual Proyectos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Estatus Actual Proyectos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>