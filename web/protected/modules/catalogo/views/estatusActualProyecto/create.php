<?php
/* @var $this EstatusActualProyectoController */
/* @var $model EstatusActualProyecto */

$this->pageTitle = 'Registro de Estatus Actual Proyectos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Estatus Actual Proyectos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>