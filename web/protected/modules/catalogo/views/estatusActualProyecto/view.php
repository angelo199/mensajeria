<?php
/* @var $this EstatusActualProyectoController */
/* @var $model EstatusActualProyecto */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Estatus Actual Proyectos'=>array('lista'),
);
?>
<div class="col-xs-12">
    <div class="row-fluid">

        <div class="tabbable">

            <ul class="nav nav-tabs">
                <li class="active"><a href="#datosGenerales" data-toggle="tab">Vista de Datos Generales</a></li>
                <!--<li class="active"><a href="#otrosDatos" data-toggle="tab">Otros Datos Relacionados</a></li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="datosGenerales">
                    <div class="view">

                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'id'=>'estatus-actual-proyecto-form',
                                'htmlOptions' => array('onSubmit'=>'return false;',), // for inset effect
                                // Please note: When you enable ajax validation, make sure the corresponding
                                // controller action is handling ajax validation correctly.
                                // There is a call to performAjaxValidation() commented in generated controller code.
                                // See class documentation of CActiveForm for details on this.
                                'enableAjaxValidation'=>false,
                        )); ?>

                        <div id="div-datos-generales">

                            <div class="widget-box">

                                <div class="widget-header">
                                    <h5>Vista de Datos Generales</h5>

                                    <div class="widget-toolbar">
                                        <a data-action="collapse" href="#">
                                            <i class="icon-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="widget-body">
                                    <div class="widget-body-inner">
                                        <div class="widget-main">
                                            <div class="widget-main form">
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'nombre'); ?>
                                                            <?php echo $form->textField($model, 'nombre', array('style' => 'width:100%', 'maxlength' => 40, 'class' => 'span-12', 'disabled' => 'disabled',)); ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php echo $form->labelEx($model, 'estatus'); ?>
                                                            <?php echo $form->dropDownList($model, 'estatus', array('A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('prompt' => '- - -', 'class' => 'span-12', 'style' => 'width:100%', 'disabled' => 'disabled',)); ?>
                                                        </div>
                                                                <div class="row">
                                                        </div>


                                                        <div class="col-md-6">
                                                            <label class="col-md-12" for="nombre">Creado por</label>
                                                            <?php
                                                            echo
                                                            CHtml::textField('', $model->usuarioIni->nombre, array('style' => 'width:100%', 'class
' => 'col-xs-30 col-sm-30', 'required' => 'required','disabled'=>'disabled'));
                                                            ?>
                                                        </div>


                                                        <div class="col-md-6">
                                                            <label class="col-md-12" for="nombre">Fecha de creación</label>
                                                            <?php
                                                            echo
                                                            $form->textField($model, 'fecha_ini', array('style' => 'width:100%', 'maxlength' => 50, 'class
' => 'col-xs-30 col-sm-30', 'required' => 'required','disabled'=>'disabled'));
                                                            ?>
                                                        </div>
                                                        <div class="row">

                                                        </div>
                                                        <br>
                                                        <?php if (!empty($model->usuarioAct->nombre)) { ?>
                                                            <div class="col-md-6">
                                                                <label class="col-md-12" for="nombre">Actualizado por</label>
                                                                <?php
                                                                echo
                                                                CHtml::textField('', $model->usuarioAct->nombre, array('style' => 'width:100%', 'maxlength' => 50, 'class
' => 'col-xs-30 col-sm-30', 'required' => 'required','disabled'=>'disabled'));
                                                                ?>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label class="col-md-12" for="nombre">Fecha de Actualización</label>
                                                                <?php
                                                                echo
                                                                $form->textField($model, 'fecha_act', array('style' => 'width:100%', 'maxlength' => 50, 'class
' => 'col-xs-30 col-sm-30', 'required' => 'required','disabled'=>'disabled'));
                                                                ?>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/catalogo/estatusActualProyecto"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div><!-- form -->
                </div>

            </div>
        </div>

    </div>