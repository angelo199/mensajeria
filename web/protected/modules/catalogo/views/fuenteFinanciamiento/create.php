<?php
/* @var $this FuenteFinanciamientoController */
/* @var $model FuenteFinanciamiento */

$this->pageTitle = 'Registro de Fuente Financiamientos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Fuente Financiamientos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>