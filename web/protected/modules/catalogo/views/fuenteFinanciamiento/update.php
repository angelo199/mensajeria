<?php
/* @var $this FuenteFinanciamientoController */
/* @var $model FuenteFinanciamiento */

$this->pageTitle = 'Actualización de Datos de Fuente Financiamientos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Fuente Financiamientos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>