<?php
/* @var $this InstrumentoSuscritoController */
/* @var $model InstrumentoSuscrito */

$this->pageTitle = 'Actualización de Datos de Instrumento Suscritos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Instrumento Suscritos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>