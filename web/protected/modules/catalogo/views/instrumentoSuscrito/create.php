<?php
/* @var $this InstrumentoSuscritoController */
/* @var $model InstrumentoSuscrito */

$this->pageTitle = 'Registro de Instrumento Suscritos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Instrumento Suscritos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>