<?php
/* @var $this MemorandumController */
/* @var $model Memorandum */

$this->pageTitle = 'Actualización de Datos de Memorandums';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Memorandums'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>