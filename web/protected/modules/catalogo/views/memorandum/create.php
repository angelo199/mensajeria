<?php
/* @var $this MemorandumController */
/* @var $model Memorandum */

$this->pageTitle = 'Registro de Memorandums';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Memorandums'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>