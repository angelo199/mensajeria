<?php
/* @var $this ConvenioController */
/* @var $model Convenio */

$this->pageTitle = 'Actualización de Datos de Convenios';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Convenios'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>