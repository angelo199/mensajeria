<?php

/* @var $this ConvenioController */
/* @var $model Convenio */

$this->breadcrumbs=array(
        'Catálogo' => array('/catalogo'),
	'Convenios'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Convenios';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Convenios</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Convenios.
                            </p>
                        </div>
                    </div>
                    <div id="mensaje">
                    </div>

                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/convenio/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nuevo Convenios                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'convenio-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                    
                }",
	'columns'=>array(
        array(
            'header' => '<center>Nombre</center>',
            'name' => 'nombre',
            'htmlOptions' => array(),
            'filter' => CHtml::textField('Convenio[nombre]', $model->nombre, array('title' => '',)),
        ),

       array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
            'filter' => array('A'=>'Activo','I'=>'Eliminado'),
            'value' => array($this,'estatus'),
        ),
		/*
        array(
            'header' => '<center>estatus</center>',
            'name' => 'estatus',
            'htmlOptions' => array(),
                        'value' => 'strtr($data->estatus,array("A"=>"Activo", "I"=>"Inactivo", "E"=>"Eliminado"))',
            'filter' => array('A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'),
                    ),
		*/
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>

<div id="dialogo">
</div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<script>
 function eliminar(id) {
            $(document).ready(function() {
                $("#dialogo").html('<div class="infoDialogBox"> <p>Estas seguro que desea eliminar este registro </div> </p>');
                $("#dialogo").dialog({
                    width: 500,
                    height: 200,
                    show: "scale",
                    hide: "scale",
                    resizable: "false",
                    //position: "center",
                    modal: "true",
                    title: 'Convenio',
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                            "class": "btn btn-xs",
                            click: function() {
                            $(this).dialog("close");
                                return false;
                            }
                        },
                        {
                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                            "class": "btn btn-danger btn-xs",
                            click: function() {
                               Loading.show();
                                $.ajax({
                                    url: '/catalogo/convenio/eliminacion',
                                    data: {id: id},
                                    dataType: 'json',
                                    type: 'POST',
                                    success: function(resp)
                                    {
                                        $("#mensaje").removeClass('hide').html('<div class="successDialogBox"> <p>'+resp.mensaje+'</div> </p>');
                                        $('#convenio-grid').yiiGridView('update', {
                                        data: $(this).serialize(),

                                    });
                                    Loading.hide();
                                    }
                                });
                               $(this).dialog("close");
                                return false;
                            }
                        }
                    ]
                });
            });
            }



             function activar(id) {
            $(document).ready(function() {
                $("#dialogo").html('<div class="infoDialogBox"> <p>Estas seguro que desea activar este registro </div> </p>');
                $("#dialogo").dialog({
                    width: 500,
                    height: 200,
                    show: "scale",
                    hide: "scale",
                    resizable: "false",
                    //position: "center",
                    modal: "true",
                    title: 'Convenio',
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                            "class": "btn btn-xs",
                            click: function() {
                            $(this).dialog("close");
                                return false;
                            }
                        },
                        {
                            html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar",
                            "class": "btn btn-danger btn-xs",
                            click: function() {
                               Loading.show();
                                $.ajax({
                                    url: '/catalogo/convenio/activacion',
                                    data: {id: id},
                                    dataType: 'json',
                                    type: 'POST',
                                    success: function(resp)
                                    {
                                        $("#mensaje").removeClass('hide').html('<div class="successDialogBox"> <p>'+resp.mensaje+'</div> </p>');
                                        $('#convenio-grid').yiiGridView('update', {
                                        data: $(this).serialize(),

                                    });
                                    Loading.hide();
                                    }
                                });
                               $(this).dialog("close");
                                return false;
                            }
                        }
                    ]
                });
            });
            }


</script>