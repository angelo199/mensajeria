<?php
/* @var $this ConvenioController */
/* @var $model Convenio */

$this->pageTitle = 'Registro de Convenios';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Convenios'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>