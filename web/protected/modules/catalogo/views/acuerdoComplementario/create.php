<?php
/* @var $this AcuerdoComplementarioController */
/* @var $model AcuerdoComplementario */

$this->pageTitle = 'Registro de Acuerdo Complementarios';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Acuerdo Complementarios'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>