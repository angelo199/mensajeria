<?php
/* @var $this AcuerdoComplementarioController */
/* @var $model AcuerdoComplementario */

$this->pageTitle = 'Actualización de Datos de Acuerdo Complementarios';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Acuerdo Complementarios'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>