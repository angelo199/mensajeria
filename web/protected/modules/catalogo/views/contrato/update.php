<?php
/* @var $this ContratoController */
/* @var $model Contrato */

$this->pageTitle = 'Actualización de Datos de Contratos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Contratos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>