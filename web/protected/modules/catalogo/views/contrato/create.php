<?php
/* @var $this ContratoController */
/* @var $model Contrato */

$this->pageTitle = 'Registro de Contratos';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Contratos'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>