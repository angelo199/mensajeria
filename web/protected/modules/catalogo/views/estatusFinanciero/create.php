<?php
/* @var $this EstatusFinancieroController */
/* @var $model EstatusFinanciero */

$this->pageTitle = 'Registro de Estatus Financieros';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Estatus Financieros'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>