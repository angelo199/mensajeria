<?php
/* @var $this EstatusFinancieroController */
/* @var $model EstatusFinanciero */

$this->pageTitle = 'Actualización de Datos de Estatus Financieros';
      $this->breadcrumbs=array(
        'Mi Módulo' => array('#'),
	'Estatus Financieros'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>