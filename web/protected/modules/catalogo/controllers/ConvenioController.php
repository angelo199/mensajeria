<?php

class ConvenioController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $defaultAction='lista';

    /**
     * @return array action filters
     */
    public static $_permissionControl = array(
        'read' => 'Consulta de ConvenioController',
        'write' => 'Creación y Modificación de ConvenioController',
        'admin' => 'Administración Completa  de ConvenioController',
        'label' => 'Módulo de ConvenioController'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        // en este array colocar solo los action de consulta
        return array(
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion', 'eliminacion', 'activacion'),
                'pbac' => array('admin'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta', 'registro', 'edicion',),
                'pbac' => array('write'),
            ),
            array('allow',
                'actions' => array('lista', 'consulta',),
                'pbac' => array('read'),
            ),
            // este array siempre va asì para delimitar el acceso a todos los usuarios que no tienen permisologia de read o write sobre el modulo
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Lists all models.
     */
    public function actionLista()
    {
        $model=new Convenio('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Convenio')){
            $model->attributes=$this->getQuery('Convenio');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Convenio('search');
        $model->unsetAttributes();  // clear any default values
        if($this->hasQuery('Convenio')){
            $model->attributes=$this->getQuery('Convenio');
        }
        $dataProvider = $model->search();
        $this->render('admin',array(
            'model'=>$model,
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionConsulta($id)
    {
        $idDecoded = base64_decode($id);
        $model = $this->loadModel($idDecoded);
        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegistro()
    {
        $model=new Convenio;
        if($this->hasPost('Convenio'))
        {
            $model->attributes=$this->getPost('Convenio');
            $model->beforeInsert();
            if($model->save()){
                $this->registerLog('ESCRITURA', 'modulo.Convenio.registro', 'EXITOSO', 'El Registro de los datos de Convenio se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                Yii::app()->user->setFlash('success', 'El proceso de registro de los datos se ha efectuado exitosamente');
                $model=new Convenio;
                //$this->redirect(array('edicion','id'=>base64_encode($model->id),));
            }
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionEdicion($id)
    {
            $idDecoded = base64_decode($id);
            $model = $this->loadModel($idDecoded);
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if($this->hasPost('Convenio'))
            {
                $model->attributes=$this->getPost('Convenio');
                $model->beforeUpdate();
                if($model->save()){
                 $this->registerLog('ACTUALIZACION', 'modulo.Convenio.edicion', 'EXITOSO', 'La Actualización de los datos de Convenio se ha efectuado exitósamente. Data-> '.json_encode($model->attributes));
                 Yii::app()->user->setFlash('success', 'La actualización de los Datos se ha efectuado de forma exitosa');
                        //$this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La actualización de los Datos se ha efectuado de forma exitosa.'));
                        //Yii::app()->end();
                }
            }
            $this->render('update',array(
                    'model'=>$model,
            ));
    }

    /**
     * Logical Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
     public function actionEliminacion() {
        //$idDecoded = $this->getIdDecoded($id);
        $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        if ($model) {
            $model->beforeDelete();
            if ($model->save()) {
                $this->registerLog('ELIMINACION', 'modulo.AcuerdoComplementario.eliminacion', 'EXITOSO', 'La Eliminación de los datos de AcuerdoComplementario se ha efectuado exitósamente. Data-> ' . json_encode($model->attributes));
                $mensaje = "Se ha Eliminado el Registro de Forma Exitosa";
                //Yii::app()->user->setFlash('success', 'La eliminación del registro se ha efectuado de forma exitosa.');
                echo json_encode(array('statusCode' => 'exito', 'mensaje' => $mensaje));
                //$this->renderPartial('//msgBox', array('class'=>'successDialogBox', 'message'=>'La eliminación del registro se ha efectuado de forma exitosa.'));
                Yii::app()->end();
            }
        }
        //$this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }

       public function estatus($data) {
        if ($data['estatus'] == 'A') {
            $columna = "Activo";
        } else {
            $columna = "Eliminado";
        }
        return $columna;
    }


    /**
     * Activation of a particular model Logicaly Deleted.
     * If activation is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be activated
     */
    public function actionActivacion() {
         $id = $_REQUEST['id'];
        $model = $this->loadModel($id);
        if ($model) {
            $model->beforeActivate();
            if ($model->save()) {
               $this->registerLog('ELIMINACION', 'modulo.AcuerdoComplementario.eliminacion', 'EXITOSO', 'La Activación de los datos de AcuerdoComplementario se ha efectuado exitósamente. Data-> ' . json_encode($model->attributes));
                $mensaje = "Se ha Activado el Registro de Forma Exitosa";
                echo json_encode(array('statusCode' => 'exito', 'mensaje' => $mensaje));
                Yii::app()->end();
            }
        }
        //$this->redirect($this->hasPost('returnUrl') ? $this->getPost('returnUrl') : array('lista'));
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Convenio the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        if(is_numeric($id)){
            $model=Convenio::model()->findByPk($id);
            if($model===null){
                throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
        }
        else{
            return null;
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Convenio $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if($this->hasPost('ajax') && $this->getPost('ajax')==='convenio-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Retorna los botones o íconos de administración del modelo
     *
     * @param mixed $data
     *
     */
    public function getActionButtons($data) {
        $id_encoded = $data["id"];
        $id = base64_encode($id_encoded);
        $columna = '<div class="action-buttons">';
        if ($data['estatus'] == 'A') {
        $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in dark", "title" => "Ver datos", 'href' => '/catalogo/convenio/consulta/id/' . $id)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("class" => "fa icon-pencil blue", "title" => "Editar datos", 'href' => '/catalogo/convenio/edicion/id/' . $id)) . '&nbsp;&nbsp;';
        $columna .= CHtml::link("", "", array("onClick" => "eliminar($id_encoded)", "class" => "fa icon-trash red", "title" => "Convenio")) . '&nbsp;&nbsp;';
        }else{
            $columna .= CHtml::link("", "", array("class" => "fa icon-zoom-in dark", "title" => "Ver datos", 'href' => '/catalogo/convenio/consulta/id/' . $id)) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "", array("onClick" => "activar($id_encoded)", "class" => "fa fa-check", "title" => "Convenio"));
        }
        $columna .= '</div>';
        return $columna;
    }

}
